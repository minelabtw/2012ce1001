//                  賭錢比骰子大小遊戲                  //
//※如果三輪過後電腦與玩家所擁有的金錢相等時，為電腦獲勝//

#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int dice( int point ) // 隨機產生骰子點數的函式
{
    srand((unsigned)time(NULL));

    point = rand()%6 + 1;

    return point;
}

int main()
{
    int dice1, dice2, bet, cpu=100, money=100; // cpu為電腦的金錢，money為玩家金錢，bet為下注金額，dice1為電腦骰子點數，dice2為玩家骰子點數

    cout << endl << "Computer: I want to play a game. You have to make a choice." << endl << endl;

    for( int i = 1 ; i <= 3 ; i++ ) //執行三次迴圈
    {
        while(1) // 賭注金額的判斷
        {
            cout << "How much do you want to play in this game? ";
            cin >> bet;

            if( bet <= 0 ) // 賭注小於等於0元時重新輸入
                cout << "You can't input negative number or zero to your bet, please try again." << endl << endl;
            else if( bet > money ) // 賭注大於玩家所擁有的金額時重新輸入
                cout << "You don't have that much money, please try again." << endl << endl;
            else // 符合規定繼續執行接下來的程式
                break;
        }

        dice1 = dice(cpu);
        dice2 = rand()%6+1;
        cout << "Computer's dice point is: " << dice1 << endl;
        cout << "Your dice point is: " << dice2 << endl;

        if( dice1 < dice2 ) // 玩家點數大於電腦點數時
        {
            money += bet;
            cpu -= bet;
            cout << "Congratulation! You win. You left " << money << " dollars." << endl << endl;
        }
        if( dice1 > dice2 ) // 玩家點數小於電腦點數時
        {
            money -= bet;
            cpu += bet;
            cout << "Sorry, you lose. You left " << money << " dollars." << endl << endl;
        }
        if( dice1 == dice2 ) // 玩家點數等於電腦點數時
            cout << "Draw. You left " << money << " dollars." << endl << endl;

        if( money <= 0 ) // 如果中途玩家破產時結束程式
        {
            cout << "You don't have any money, you lose the game." << endl;
            return 0;
        }
        if( cpu <= 0 ) // 如果中途電腦破產時結束程式
        {
            cout << "Computer is bankrupted, you win the game" << endl;
            return 0;
        }
    }

    // 最後輸出玩家與電腦所擁有的錢，並比較誰比較多，多的方獲勝 //

    cout << "Computer remains " << cpu << " dollars." << endl;
    cout << "You remain " << money << " dollars." << endl;

    if( money > cpu )
        cout << "You win the game." << endl;
    else if( money < cpu )
        cout << "You lose the game." << endl;
    else
        cout << "Draw. You lose the game." << endl;

    return 0;
}

