#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int dice(int input=rand())//自訂一個函式,名稱為dice
{
    int point;
    point=input%6+1;
    return point;
}

int main()
{
    srand((unsigned)time(NULL));//使其數變為亂數
    int k=1,num1=100,num2=100;//num1為電腦,num2為玩家

    cout << "\nComputer: I want to play a game. You have to make a chioce.\n" << endl;

    while(k<=3)//以k為條件,讓迴圈跑三回合
    {

        int a=rand(),b=rand(),i=0,j=0,x=0;

        cout<<"How much do you want to play in this round? ";
        cin>>x;

        if(x<=0)
        {
            cout<<"You can't input negative or zero to your bet, please try again.\n"<<endl;
            k--;//不算一回合
        }
        else if(x>num2)
        {
            cout<<"You don't have that much money, please try again.\n"<<endl;
            k--;//不算一回合
        }


        else
        {
            cout<<"Compuerr's dice point is : "<<dice(a)<<endl;//電腦點數
            cout<<"Your dice point is : "<<dice(b)<<endl;//玩家點數

            i=dice(a);
            j=dice(b);

            if(i>j)//電腦贏玩家
            {
                num1=num1+x;
                num2=num2-x;

                if(num2>0)
                {
                    cout<<"Sorry,you lose.";
                    cout<<"You left "<<num2<<" dollars.\n"<<endl;
                }

                else if(num2<=0)//若玩家的錢小於0,則破產跳出
                {
                    num2=0;
                    cout<<"Sorry,you lose.";
                    cout<<"You left "<<num2<<" dollars.\n"<<endl;
                    cout<<"You don't have any money, you lose the game.\n";
                    return 0;
                }

            }

            else if(i<j)//電腦輸玩家
            {
                num1=num1-x;
                num2=num2+x;

                if(num1>0)
                {
                    cout<<"Congratulation! You win.";
                    cout<<"You left "<<num2<<" dollars.\n"<<endl;

                }

                else if(num1<=0)//若電腦的錢小於0,則玩家直接勝利
                {
                    num2=200;
                    cout<<"Congratulation! You win.";
                    cout<<"You left "<<num2<<" dollars.\n"<<endl;
                    cout<<"Computer is bankrupted,you win the game.\n";
                    return 0;
                }

            }

            else
            {
                cout<<"Draw. ";
                cout<<"You left "<<num2<<" dollars.\n"<<endl;
            }
        }

        k++;
    }

    cout<<"Computer remains "<<num1<<" dollars."<<endl;
    cout<<"You remain "<<num2<<" dollars."<<endl;
    if(num1>=num2)//由於電腦為莊家,當金錢相等時,由莊家勝出
    {
        cout<<"You lose the game.\n";
    }
    else
    {
        cout<<"You win the game.\n";
    }

    return 0;
}
