#include<iostream>
#include<time.h>
#include<cstdlib>
int dice(); //dice函式用來隨機產生骰子點數
using namespace std;
int main()
{
    srand((unsigned)time(NULL)); //建立亂數表
    int bet , bot = 100 , man = 100 , bot_point , man_point;
    //bet為玩家下注的賭金 , bot為電腦的錢 ,起始為100元 , man為玩家的錢 , 起始為100元 , bot_point為電腦擲出的骰子點數 , man_point為玩家擲出的骰子點數
    cout << "Computer: I want to play a game. You have to make a choice.\n\n";
    for(int i = 0 ; i < 3 ; i++) //遊戲進行三回合
    {
        cout << "How much do you want to play in this round? ";
        while(cin >> bet && (bet > man || bet <= 0)) //若賭金小於0或大於玩家身上的錢 , 則進入迴圈 , 要求玩家重新輸入
        {
            if(bet <= 0) //若賭金小於0 , 則輸出錯誤訊息
                cout << "You can't input negative number or zero to your bet, please try again.\n\n";
            else //若賭金大於玩家身上的錢 , 則輸出錯誤訊息
                cout << "You don't have that much money, please try again.\n\n";
            cout << "How much do you want to play in this round? ";
        }
        bot_point = dice(); //記錄電腦擲出的骰子點數
        man_point = dice(); //記錄玩家擲出的骰子點數
        cout << "Computer's dice point is: " << bot_point;
        cout << endl << "Your dice point is: " << man_point << endl;
        if(bot_point > man_point) //若電腦的骰子點數大於玩家的骰子點數 , 則玩家把賭金交給電腦
        {
            man -= bet;
            bot += bet;
            cout << "Sorry, you lose. You left " << man << " dollars.\n\n";
        }
        else if(bot_point < man_point) //若電腦的骰子點數小於玩家的骰子點數 , 則電腦把賭金交給玩家
        {
            man += bet;
            bot -= bet;
            if(man > 200) //若玩家的錢超過200元 , 則變成200元
                man = 200;
            cout << "Congratulation! You win. You left " << man << " dollars.\n\n";
        }
        else //若電腦的骰子點數等於玩家的骰子點數 , 則沒事
            cout << "Draw. You left " << man << " dollars.\n\n";
        if(man <= 0) //若玩家破產 , 輸出破產訊息 , 則結束程式
        {
            cout << "You don't have any money, you lose the game.\n";
            return -1;
        }
        else if(bot <= 0) //若電腦破產 , 輸出破產訊息 , 則結束程式
        {
            cout << "Computer is bankrupted, you win the game.\n";
            return -1;
        }
    }
    cout << "Computer remains " << bot << " dollars." << endl; //輸出電腦最後剩下的錢
    cout << "You remains " << man << " dollars." << endl; //輸出玩家最後剩下的錢
    if(man > bot) //若玩家的錢大於電腦的錢 , 則輸出獲勝訊息
        cout << "You win the game.\n";
    else if(man < bot) //若玩家的錢小於電腦的錢 , 則輸出失敗訊息
        cout << "You lose the game.\n";
    else //若玩家的錢等於電腦的錢 , 仍輸出失敗訊息
        cout << "Draw. You lose the game.\n";
	return 0;
}
int dice()
{
    int point = rand() % 6 + 1; //point產生隨機1~6的其中一個數字
    return point;
}
