#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int dice(int c)//自定義函式(取亂數1-6)
{
    srand((unsigned)time(NULL)+c);
    int a=rand()%6+1;
    return a;//
}
int main()
{
    int x=100,y=100,p;//設置雙方起始錢數
    cout<<"Computer: I want to play a game. You have to make a choice."<<endl<<endl;
    for(int c=1;c<=3;c++)//執行三次迴圈
    {
        cout<<"How much do you want to play in this round? ";
        while((cin>>p==false)||(p<=0)||(p>y))//判斷賭注是否合理
        {
            if(p>y)
                cout<<"You don't have that much money, please try again."<<endl<<endl<<endl;
            else
                cout<<"You can't input negative number or zero to your bet, please try again."<<endl<<endl<<endl;
            cout<<"How much do you want to play in this round? ";
        }
        int a=dice(a*2),b=dice(b*2);//呼叫函式
        cout<<"Computer's dice point is: "<<a<<endl;
        cout<<"Your dice point is: "<<b<<endl;
        if(a>b)//電腦勝
        {
            x=x+p;
            y=y-p;
            if(y<=0)//確認餘額
            {
                cout<<"Sorry, you lose. You left 0 dollars."<<endl<<endl<<"You don't have any money, you lose the game."<<endl<<endl;
                return 0;//電腦勝回傳0
            }
            cout<<"Sorry, you lose. You left "<<y<<" dollars."<<endl<<endl;
        }
        if(b>a)//玩家勝
        {
            x=x-p;
            y=y+p;
            if(x<=0)//確認餘額
            {
                cout<<"Congratulation! You win.You left 200 dollars."<<endl<<endl<<"Computer is bankrupted, you win the game."<<endl<<endl;
                return 0;//玩家勝回傳0
            }
            cout<<"Congratulation! You win.You left "<<y<<" dollars."<<endl<<endl;
        }
        if(a==b)//平手
            cout<<"Draw. You left "<<y<<" dollars."<<endl<<endl;
    }
    cout<<"Computer remains "<<x<<" dollars."<<endl;//三局結束，判斷餘額
    cout<<"You remain "<<y<<" dollars"<<endl;
    if(x<y)//玩家勝
        cout<<"You win the game."<<endl<<endl;
    if(x>y)//電腦勝
        cout<<"You lose the game."<<endl<<endl;
    if(x==y)//平手，判定電腦勝
        cout<<"Draw. You lose the game."<<endl<<endl;
    return 0;//回傳0
}

