//Midterm1 by J
//No additional functions, at last I don't think of it......
#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int dice()
{
    int pts ;
    srand((unsigned)time(NULL));

    pts=rand();     //random value

    return pts ;        //give the result
}

int main()
{
    cout << "Computer: I want to play a game. You have to make a choice." << endl << endl ;

    int cm=100 , pm=100 , ctr=1 ;       //cm as computer's $ , so is pm. Their original money is 100 ds.
    int cpt , ppt;      //2 points of dice
    int inp ;       //inp as inputP: for the use of this game's bill!

    for ( int i=3 ; i>0 ; i-- )     //Game start here
    {

        ppt=dice();
        cpt=dice();     //choose a random value
        ppt=(ppt*7)%6+1;
        cpt=(cpt*23)%6+1;       //my idea:random value is the same, so I change them by 2 prime & make them more "Random & Different"

        cout << "How much do you want to play in this round? ";
        while ( cin>>inp )      //check for regular input
        {
            if ( inp>0 && inp<=pm )
                break ;
            else if ( inp<=0 )      //negative bet
            {
                cout << "You can't input negative number or zero to your bet, please try again." << endl << endl ;
                cout << "How much do you want to play in this round? ";
            }
            else        //bet that is over your money
            {
                cout << "You don't have that much money, please try again." << endl << endl ;
                cout << "How much do you want to play in this round? ";
            }
        }

        cout << "Computer's dice point is: " << cpt << endl ;
        cout << "Your dice point is: " << ppt << endl ;

        if ( ppt>cpt )     //Judge the money change.
        {
            pm+=inp ;
            cm-=inp ;
        }
        else if ( ppt<cpt )
        {
            pm-=inp ;
            cm+=inp ;
        }


        if ( pm<=0 )        //break before 3 round, this is for player lose
        {
            cout << "Sorry, you lose. You left 0 dollars." << endl << endl;
            cout << "You don't have any money, you lose the game." ;

            return 0;
        }
        else if ( cm<=0 )       //for player win before 3 round.
        {
            cout << "Congratulation! You win. You left 200 dollars." << endl << endl;
            cout << "Computer is bankrupted, you win the game." ;

            return 0;
        }

        if ( ppt==cpt )     //Draw cout.
            cout << "Draw. You left " << pm << " dollars." << endl << endl ;
        else if ( ppt>cpt )
            cout << "Congratulation! You win. You left " << pm << " dollars." << endl << endl ;
        else        //player lose
            cout << "Sorry, you lose. You left " << pm << " dollars." << endl << endl ;

        if (ctr==3)
        {
            cout << "Computer remains " << cm << " dollars." << endl ;      //situation below is for playing all 3 round & nobody is bankrupt
            cout << "You remain " << pm << " dollars." << endl ;

            if ( pm>cm )        //3 situations below
                cout << "You win the game." << endl ;
            else if ( pm==cm )
                cout << "Draw. You lose the game." << endl ;
            else if ( pm<cm )
                cout << "You lose the game." << endl ;

            return 0 ;      //end program

        }

        ctr++ ;

    }

    return 0 ;      //end program
}
