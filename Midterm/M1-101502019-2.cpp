//the numbers which are return by dice() are always the same
#include <iostream>
#include <cstdlib>
#include <time.h>
using namespace std;

int main()
{
    int count = 0;
    srand((unsigned)time(NULL));

    int a = rand();
    int b = rand();

    cout << "Computer: I want to play a game. You have to make a choice." << endl;

    while (count <= 3)
    {

        cout << "\nComputer's dice point is: " << a % 6 + 1;
        cout << "\nYour dice point is: " << b % 6 + 1;

        if ((a % 6 + 1) > (b % 6 + 1))
            cout << "\nSorry, you lose.";
        else if ((a % 6 + 1) < (b % 6 + 1))
            cout << "\nCongratulation! You win.";
        else if ((a % 6 + 1) == (b % 6 + 1))
            cout << "\nDraw";

        count++;
    }
    return 0;
}
