#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

int dice()
{
    int t;
    srand ((unsigned)time(NULL));
    t=rand()%6+1;
    return t;
}

int main()
{

    int n,z=3,a=100,b=100,x,y;
    cout << "Computer: I want to play a game. You have to make a choice." << endl;
    do
    {
        srand ((unsigned)time(NULL)); //使用亂數表
        cout <<"How much do you want to play in this round?"<<" ";
        cin >>n;
        cout <<endl;
        while (n<=0) //賭金小於0 則在輸入一次
        {
            cout <<"You can't input negative number or zero to your bet,please try again."<<endl;
            cout <<endl;
            cout <<"How much do you want to play in this round?"<<" ";
            cin >>n;
        }
        while (n>a) //賭金大於財產 則在輸入一次
        {
            cout <<"You don't have that much money,please try again."<<endl;
            cout <<endl;
            cout <<"How much do you want to play in this round?"<<" ";
            cin >>n;
            cout <<endl;
        }
        cout <<"Computer's dice point is: "<<dice()<<endl;
        x=dice();
        cout <<"Your dice point is: "<<rand()%6+1<<endl;
        y=rand()%6+1;
        if (x>y)
        {
            if (a-n>0)
            {
                cout <<"Sorry,you lose.You left "<<a-n<<" dollars."<<endl;
                cout <<endl;
                a=a-n;
                b=b+n;
            }
            if (a-n==0)
            {
                cout <<"Sorry,you lose.You left 0 dollars.";
                return 0;
            }
        }
        if (x<y)
        {
            if (b-n>0)
            {
                cout <<"Congratulation! You win. You left "<<a+n<<" dollars."<<endl;
                cout <<endl;
                a=a+n;
                b=b-n;
            }
            if (b-n<=0)
            {
                cout <<"Computer is bankrupted, you win the game.";
                return 0;
            }

        }
        if (x==y)
        {
            cout <<"Draw. You left "<<a<<" dollars."<<endl;
            cout <<endl;
        }
        z--;
    }while (z!=0);
    if (a>b)
    {
        if (b<=0) //若電腦財產小於0 則視為0
        cout <<"Computer remains "<<0<<" dollars."<<endl;
        else
        cout <<"Computer remains "<<b<<" dollars."<<endl;
        if (a<200) //若玩家財產大於於200 則視為200
        cout <<"You remains "<<a<<" dollars."<<endl;
        else

        cout <<"You remains "<<200<<" dollars."<<endl;
        cout <<"You win the game."<<endl;
    }
    else if (a<b)
    {
        if (b<200) //若電腦財產大於200 則視為200
        cout <<"Computer remains "<<b<<" dollars."<<endl;
        else
        cout <<"Computer remains "<<200<<" dollars."<<endl;
        cout <<"You remains "<<a<<" dollars."<<endl;
        cout <<"You lose the game."<<endl;
    }
    else
    {
        cout <<"Computer remains "<<b<<" dollars."<<endl;
        cout <<"You remains "<<a<<" dollars."<<endl;
        cout <<"Draw. You lose the game."<<endl;
    }


    return 0;
}

