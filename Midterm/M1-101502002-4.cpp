#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;//簡化 std
int dice(int c)//輸出點數之函式
{
    srand((unsigned)time(NULL));//用時間打亂
    int a=rand();
    int b=a%6+1;//使b介於1~6
    return b;//回傳b
}
int main()
{
    srand((unsigned)time(NULL));
    cout<<"Computer: I want to play a game.  You have to make a choice."<<endl<<endl;
    int computer=100,user=100,ap,p;//computer和user為原始金額
    for(int i=1; i<=3 ;i++)//三次遊戲的迴圈
    {
        cout<<"How much do you want to play in this round? ";
        cin>>ap;//輸入賭金
        while(ap<=0 or ap>user)//錯誤輸入及重複輸入之迴圈
        {
            if(ap<=0)
                cout<<"You can't input negative number or zero to your bet, please try again."<<endl<<endl;
            else
                cout<<"You don't have that much money, please try again."<<endl<<endl;
            cout<<"How much do you want to play in this round? ";
            cin>>ap;
        }
        int a=dice(p);//運用dice函式
        int b=rand();
        cout<<"Computer's dice point is: "<<a%6+1<<endl;
        cout<<"Yout dice point is: "<<b%6+1<<endl;
        if((a%6+1)<(b%6+1))//使用者贏
        {
            user=user+ap;
            computer=computer-ap;//加減好賭金
            if(computer<=0)//電腦破產
            {
                cout<<"Computer is bankrupted, you win the game. "<<endl;
                return 0;
            }

            else
                cout<<"Congratulation! You win. You left "<<user<<" dollars."<<endl<<endl;
        }
        else if((a%6+1)>(b%6+1))//電腦贏
        {
            user=user-ap;
            computer=computer+ap;//加減好賭金

            if(user<=0)//使用者破產
            {
                cout<<"Sorry you lose. You left 0 dollars. "<<endl;
                return 0;
            }

            else
                cout<<"Sorry, you lose. You left "<<user<<" dollars."<<endl<<endl;
        }
        else//平手
            cout<<"Draw.  You left "<<user<<" dollars."<<endl<<endl;

    }//三次遊戲迴圈結束
    cout<<"Computer remains "<<computer<<" dollars. "<<endl;
    cout<<"You remain "<<user<<" dollars. "<<endl;
    if(computer>user)//電腦贏
        cout<<"You lose the game. "<<endl;
    else if(user>computer)//使用者贏
        cout<<"You win the game. "<<endl;
    else//平手
        cout<<"Draw. You lose the game."<<endl;

    return 0;
}
