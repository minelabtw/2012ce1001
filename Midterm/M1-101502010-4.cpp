#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int dice(int n)//骰子函數
{
    srand((unsigned)time(NULL));
    int a=rand();
    if(n==1)
        return (a%22)%6+1;
    else
        return (a%31)%6+1;
}
int main()
{
    cout<<"Computer: I want to play a game. You have to make a choice."<<endl<<endl;
    int n,count=1,ccash=100,pcash=100;//宣告變數，計數器預設為1，雙方玩家賭本預設為100
    while(count<=3)//至多比三次骰子
    {
        cout<<"How much do you want to play in this round?";//詢問賭金
        while(cin>>n)
        {
            if(n>pcash)//賭金不得超出賭本
                cout<<"You don't have that much money, please try again."<<endl;
            else if(n<=0)//賭金不得小於0
                cout<<"You can't input negative or zero to your bet, please try again."<<endl;
            else
                break;//若賭金符合條件則跳出迴圈
            cout<<endl<<"How much do you want to play in this round?";//再次詢問賭金
        }
        int com=dice(1);//電腦的骰子點數
        cout<<"Computer's dice point is: "<<com<<endl;
        int ply=dice(2);//玩家的骰子點數
        cout<<"Your dice point is: "<<ply<<endl;
        if(ply>com)
        {
            if(ccash-n>=0)//若玩家勝出且電腦無破產
            {
                pcash=pcash+n;//玩家獲得賭金
                ccash=ccash-n;//電腦失去賭金
            }
            else//若玩家勝出且電腦破產
            {
                pcash=200;//玩家剩餘金額為200
                ccash=0;//電腦剩餘金額為0
            }
            cout<<"Congratulation! You win. ";//輸出玩家勝利訊息
        }
        else if(ply<com)
        {
            if(pcash-n>=0)//若電腦勝出且玩家無破產
            {
                pcash=pcash-n;//電腦獲得賭金
                ccash=ccash+n;//玩家失去賭金
            }
            else//若電腦勝出且玩家破產
            {
                ccash=200;//電腦剩餘金額為200
                pcash=0;//玩家剩餘金額為0
            }
            cout<<"Sorry, you lose. ";//輸出玩家失敗訊息
        }
        else
            cout<<"Draw "<<endl;//輸出平手訊息(餘額不變動)
        cout<<"You left "<<pcash<<" dollars"<<endl<<endl;//輸出玩家餘下金額
        if(ccash==0||pcash==0)//若其中有一方破產
        {
            if(ccash==0)//若電腦破產
                cout<<"Computer is bankrupted, you win the game.";
            else//若玩家破產
                cout<<"You don't have any money, you lose the game";
            cout<<endl;
            return 0;//結束程式
        }
        if(count==3)//若已進行3回合
        {
            cout<<"Computer remain "<<ccash<<" dollars."<<endl;//輸出電腦餘額
            cout<<"You remain "<<pcash<<" dollars."<<endl;//輸出玩家餘額
            if(ccash>pcash)//若電腦餘額較多
                cout<<"You lose the game."<<endl;//輸出電腦獲勝
            else if(ccash<pcash)//若玩家餘額較多
                cout<<"You win the gane."<<endl;//輸出玩家獲勝
            else//若雙方餘額相同
                cout<<"Draw. You lose the game."<<endl;//輸出電腦獲勝
        }
        count++;//累加計數器(1回合結束)
    }
    return 0;//程式結束
}
