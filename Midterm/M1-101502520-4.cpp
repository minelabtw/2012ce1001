#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;


int Dice()//丟骰子的動作
{
    int point=1+rand()%6;
    return point;
}


int main()
{
    srand(time(0));

    int ComputerDice;
    int MyDice;

    int ComputerMoney=100;
    int MyMoney=100;
    int GameMoney;//賭金
    int Games=0;//用來計算回合數

    bool GameFinished=false; //用來判定需不需要輸出最終訊息


    cout << "Computer: I want to play a game. You have to make a choice." << endl<<endl;

    while(Games<3)
    {
        GameFinished=false;
        cout<<"How much do you want to play in this round? ";
        cin>>GameMoney;

        if(GameMoney<=0 || GameMoney>MyMoney)
        {
            if(GameMoney<=0)
                cout<<"You can't input negitive number or zero to your bet, please try again."<<endl<<endl;
            else
                cout<<"You don't have that much money, please try again."<<endl<<endl;
            continue;//輸入錯誤後，跳過這次While，再次回到While
        }

        //雙方開始丟骰子
        ComputerDice=Dice();
        MyDice=Dice();

        cout<<"Computer's dice point is: "<<ComputerDice<<endl;
        cout<<"Your dice point is: "<<MyDice<<endl;

        if(ComputerDice>MyDice)
        {
            MyMoney=MyMoney-GameMoney;
            ComputerMoney=ComputerMoney+GameMoney;

            if(ComputerMoney<0)
                ComputerMoney=0;
            else if(ComputerMoney>200)
                ComputerMoney=200;

            if(MyMoney<0)
                MyMoney=0;
            else if(MyMoney>200)
                MyMoney=200;

            cout<<"Sorry, you lose. "<<"You left "<<MyMoney<<" dollars."<<endl;
            if(MyMoney<=0)
            {
                cout<<endl<<"You don't have any money, you lose the game."<<endl;
                break;
            }

        }
        else if(ComputerDice<MyDice)
        {
            MyMoney=MyMoney+GameMoney;
            ComputerMoney=ComputerMoney-GameMoney;

            if(ComputerMoney<0)
                ComputerMoney=0;
            else if(ComputerMoney>200)
                ComputerMoney=200;

            if(MyMoney<0)
                MyMoney=0;
            else if(MyMoney>200)
                MyMoney=200;

            cout<<"Congratulation! You win. "<<"You left "<<MyMoney<<" dollars."<<endl;
            if(ComputerMoney<=0)
            {
                cout<<endl<<"Computer is bankrupted, you win the game."<<endl;
                break;
            }
        }
        else
        {
            cout<<"Draw. "<<"You left "<<MyMoney<<" dollars."<<endl;
        }

        Games++;
        GameFinished=true;
        cout<<endl;
    }

    if(GameFinished)
    {
        cout<<"Computer remains "<<ComputerMoney<<" dollars."<<endl;
        cout<<"You remains "<<MyMoney<<" dollars."<<endl;

        if(ComputerMoney>=MyMoney)
            cout<<"You lose the game."<<endl;
        else
            cout<<"You win the game."<<endl;
    }



    return 0;
}
