#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int dice(int g)     //創造亂數的函式
{
    srand((unsigned)time(NULL));    //打亂亂數表

    int a=rand(),c=rand(),b;     //設置兩亂數
    if(g==2)
        b=a%6+1;       //兩亂數皆調為1~6
    else
        b=c%6+1;
    return  b;        //回傳亂數值
}

int main()
{
    int p=0,n=100,c=100;
    cout<<"Computer : I want to play a game. You have make a choice.\n";
    while(p<3)      //最多執行3次
    {
        int m=0;

        while(m<=0||m>n)     //當輸入不符條件時進入迴圈
        {
            cout<<"How much do you want to play in this round?";
            cin>>m;
            if(m<=0||m>n)
            {
                if(m<=0)     //小於0的情況
                    cout<<"You can't input negative number or zero to your bet, please try again.\n\n";

                else         //大於自己錢的情況
                    cout<<"You don't have that much money, please try again.\n\n";
            }
        }
        cout<<"Computer's dice point is : "<<dice(1)<<"\n";   //輸出一亂數
        cout<<"Your dice point is : "<<dice(2)<<"\n";     //輸出另一亂數
        if( dice(2) > dice(1) )     //骰子贏的情況
        {
            n=n+m;        //獲得賭金
            c=c-m;
            if(n>200)     //超過200以200計
                n=200;
            cout<<"Congratulation! You win. You left "<<n<<" dollars.\n\n";
            if(c<=0)      //電腦破產的情形
            {
                cout<<"Computer is bankrupted, you win the game.\n";
                return 0;
            }
        }
        else if( dice(2) < dice(1) )    //骰子輸的情況
        {
            n=n-m;      //失去賭金
            c=c+m;
            cout<<"Sorry, you lose. You left "<<n<<" dollars.\n\n";
            if(n<=0)    //自己破產的情況
            {
                cout<<"You don't have any money, you lose the game.\n";
                return 0;
            }
        }
        else      //平手的情況
            cout<<"Draw. You left "<<n<<" dollars.\n\n";
        p=p+1;    //計數
    }
    if(c<0)       //若電腦賭金小於0則以0計
        c=0;
    if(c>200||n>200)   //若賭金超過200則以200計
    {
        if(c>200)
            c=200;
        else
            n=200;
    }
    cout<<"Computer remains "<<c<<" dollars.\n";    //輸出兩者剩餘的錢
    cout<<"You remain "<<n<<" dollars.\n";
    if(n>c)
        cout<<"You win the game.\n";     //贏得情況
    else if(n<c)
        cout<<"You lose the game.\n";    //輸的情況
    else
        cout<<"Draw. You lose the game.\n";    //平手卻還是輸的情況
    return 0;
}
