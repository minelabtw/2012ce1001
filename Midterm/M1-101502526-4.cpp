#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int dice(int point)  //變數函式
{
    point = rand ();
    point = point%6+1 ;  //取1~6
    return point;
}

int main()
{
    int my,com,count,a,b,mymoney=100,commoney=100,money;
    srand((unsigned)time(NULL));
    cout << "Computer : I want to play a game. You have to make a choice. " << endl << endl;
    for (count = 1; count <=3; count ++)  //三次的迴圈
    {
        cout << "How much do you want to play this round? " << endl ;
        cin >> money;
        if (money>mymoney)  //若超出值則重來一次
        {
            cout << "You don't have that much money, please try again." <<endl<< endl;
            count -- ;
            continue;
        }
        if (money<=0)      //若負數或0則重來一次
        {
            cout << "You can't input negative number or ezro to your bet, please try again." <<endl<<endl;
            count -- ;
            continue;
        }
        int a = dice(com);     //先存起來變數的兩個字
        int b = dice(my);
        cout << "Computer's dice point is: " << a << endl;
        cout << "Your dice point is: " <<  b << endl;
        if ( a > b)           //比大小且將雙方的錢+-上賭注
        {
            mymoney = mymoney - money;
            commoney = commoney + money;
            cout << "Sorry,  you lose."<< " You left " << mymoney << " dollars." << endl<< endl;
        }
        else if ( a < b)
        {
            mymoney = mymoney + money;
            commoney = commoney - money;
            cout << "Congratulation! You win."<< " You left " << mymoney << " dollars." <<endl<< endl;
        }
        else if ( a == b)
        {
            cout << "Draw."<< " You left " << mymoney << " dollars." << endl<< endl;
        }
        if (mymoney<=0)     //若沒錢則宣布破產且結束
        {
            cout << "You don't have any money, you lose the game."<<endl;
            return 0;
        }
        if (commoney<=0)   //電腦沒錢宣布獲勝且結束
        {
            cout << "Computer is bankrupted, you win the game."<<endl;
            return 0 ;
        }
    }
    cout << "Compuer remains " << commoney << " dollars." <<endl;
    cout << "You remains " << mymoney << " dollars" <<endl;
    if (commoney>=mymoney)                //判斷輸贏，平手算電腦贏
        cout << "You lose the game."<<endl;
    else
        cout << "Congratulation! You win. You left " << mymoney << " dollars" << endl;
    return 0;

}
