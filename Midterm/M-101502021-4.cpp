//賭博擲骰子遊戲
#include <iostream>
#include <cstdlib>
#include <time.h>
#include <iomanip>
using namespace std;

int dice(int A) //自訂函數
{
    int B;
    B=rand()%6+1;
    return B;
}


int main()
{
    srand((unsigned)time(NULL));
    int Cdice=rand(),Pdice=rand();
    int Cm=100,Pm=100;
    int input;
    int A=3;

    cout << "Computer: I want to play a game. You have to make a choice.\n" << endl;

    while(A>0)
    {
        cout << "How much do you want to play in this round? " ;
        cin >> input;
        while(input==false||input<=0||input>100)
        {
            cout << "You can't input negative number or zero to your bet,please try again.\n"<<endl;
            A++;
            cout << "How much fo you want to play in this round? " ;
            cin >> input;
        }

        cout << "Computer's dice point is: " << dice(Cdice)<< endl;
        cout << "Your dice point is: " << dice(Pdice) << endl ;

        if(Cdice<Pdice)
        {
            cout <<"Congratulation! You win. You left " << (Pm+input) << " dollars.\n"<<endl;
            Cm-=input ;
            Pm+=input ;

            if(Cm<=0)
            {

                cout << "Computer is bankrupted, you win the game."<<endl;
                return 0 ;
            }

            else if(Pm<=0)
            {
                cout << "Sorry, you lose. You left 0 dollars.\n" <<endl;
                return 0;
            }
        }

        else if(Cdice==Pdice)
        {
            cout << "Draw. You left " << Pm << " dollars.\n"<<endl;
        }

        else if (Pdice<Cdice)
        {
            cout << "Sorry, you lose. You left "<< (Pm-input) << " dollars."<<endl ;
            Cm+=input;
            Pm-=input;

            if(Cm<=0)
            {
                cout << "Congratulation! You win. You left 200 dollars.\n" <<endl;
                cout << "Computer is bankrupted, you win the game."<<endl;
                return 0 ;
            }

            else if(Pm<=0)
            {
                cout << "Sorry, you lose. You left 0 dollars.\n" <<endl;
                return 0;
            }
        }

        A--;
    }

    cout << "Computer remains " << Cm << " dollars."<<endl;
    cout << "You remain " << Pm << " dollars."<<endl ;

    if(Cm<Pm)
        cout << "You win the game."<<endl;

    else if (Cm>Pm)
        cout << "You lose the game."<<endl;

    else
        cout << "Draw. You lose the game." << endl;
    return 0 ;
}
