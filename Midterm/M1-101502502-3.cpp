#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int dice(int a)//製造出骰子點數
{
    a = a % 6 + 1;
    return a;
}

int main()
{
    cout << "Computer: I want to play a game. You have to make a choice.\n" << endl;

    int i = 1;
    srand((unsigned)time(NULL));//打散

    while(i <= 3)//玩三回合
    {
        int num1 = rand();//製造亂數
        int num2 = rand();//製造亂數

        cout << "Computer's dice point is: " << dice(num1) << endl;
        cout << "Your dice point is: " << dice(num2) << endl;

        if (dice(num1) > dice(num2))
            cout << "Sorry, you lose.\n" << endl;

        else if (dice(num1) < dice(num2))
            cout << "Congratulation! You win.\n" << endl;

        else if (dice(num1) == dice(num2))
            cout << "Draw\n" << endl;

        i++;
    }

    return 0;
}
