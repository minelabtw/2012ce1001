#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
int input,a,b,round=1,money1=100,money2=100;
//input為賭注 a b分別為玩家 電腦 骰子點數
//money1 money2 分別為 玩家 電腦 擁有的錢
//round 紀錄回合數
int dice()
{
    int d=(rand()%6+1);

    return d;
}
int main()
{
    srand((unsigned)time(NULL));
    cout << "Computer: I want to play a gmae. You have to make a choice.\n" << endl;
    while (round<=3)
    {
        while (1) //輸入值檢查 如果不符合條件則重跑一次迴圈
        {
            cout << "How much do you want to play in this round? ";
            cin >> input;
            if (input <=0)
            {
                cout << "You can't input negative number or zero to your bet, please try again.\n\n";
                continue;
            }
            if (input > money1)
            {
                cout << "You don't have that much money, please try again.\n\n";
                continue;
            }
            break;

        }
        //從這邊開始擲骰子
        b=dice();
        cout << "Computer's dice point is: " << b <<endl;
        a=dice();
        cout << "Your dice is: " << a << endl;
        //從這裡開始比對大小
        if (a>b) //玩家贏
        {
            if (money2-input<=0) //這段if是假設電腦輸的錢沒有賭注多
            {
                money1=money2+money1;
                money2=0;
            }
            else
            {
                money1=money1+input;
                money2=money2-input;
            }
            cout << "Congratulation! You win. You left " << money1 << " dollars." << endl;
        }
        if (a==b) //平手部分
        {
            cout << "Draw. You left " << money1 << " dollars." << endl;

        }
        if (a<b)//電腦贏
        {
            money1=money1-input;
            money2=money2+input;
            cout << "Sorry, You lose. You left " << money1 << " dollars." << endl;
        }
        cout << "\n\n";
        if (money1==0) //破產檢查
        {
            cout << "You don't have any money, you lose the game.\n\n";
            return 0;
        }
        if (money2==0)//破產檢查
        {
            cout << "Computer is bankrupted, you win the game.\n\n";
            return 0;
        }
        round++;
    }
    cout << "Computer remains " << money2 << " dollars." << endl;
    cout << "You remain " << money1 << " dollars." << endl;
    if (money1<=money2) //以下部分根據結果決定輸贏
    {
        if (money1==money2) //假設金錢相同則多加一段字串Draw
        cout << "Draw. ";
        cout << "You lose the game.\n\n" ;
    }
    if (money1>money2)
    cout << "You win the game.\n\n";
    return 0;
}
