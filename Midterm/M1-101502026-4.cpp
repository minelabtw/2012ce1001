#include <iostream> //include the header file "iostream"
#include <time.h> //include the header file "time.h"
#include <cstdlib> //include the header file "cstdlib"

using namespace std;

int dice() //this is the function to produce random dice numbers
{
    int x=1+rand()%6;

    return x;
}

int main()
{
    srand(time(NULL));

    cout << "Computer: I want to play a game. You have to make a choice." << endl << endl;

    int x,pay1=100,pay2=100; //pay1 is player's money,pay2 is computer's money
    for(int k=1;k<=3;k++)
    {
        int i=dice(),j=dice();
        cout << "How much do you want to play in this round? ";
        cin >> x;

        if(x<=0) //if x entered is smaller than or equal to 0, print out an error message
        {
            cout << "You can't input negative number or zero to your bet, please try again." << endl << endl;
            k--;
            continue;
        }
        if(x>pay1) //if x entered is greater than the money the player owns, print out an error message
        {
            cout << "You don't have that much money, please try again." << endl << endl;
            k--;
            continue;
        }
        if(x>0&&x<=100) //if x entered is greater than 0 or smaller than or equal to 100, the bet begins
        {
            cout << "Computer's dice point is: " << i << endl;
            cout << "Your dice point is: " << j << endl;
            if(i>j)
            {
                pay1-=x; //if the player loses, his money loses
                pay2+=x; //if the player loses, computer's money adds
                cout << "Sorry, you lose. You left " << pay1 << " dollars." << endl << endl;
                if(pay1<=0) //if the player's bankrupted, print out the message.
                {
                   // cout << endl;
                    cout << "You don't have any money, you lose the game." << endl;
                    return 0;
                }
            }
            if(i<j)
            {
                pay1+=x; //if the player wins, his money adds
                pay2-=x; //if the player wins, computer's money loses
                cout << "Congratulation! You win. You left " << pay1 << " dollars." << endl << endl;
                if(pay2<=0) //if the computer's bankrupted, print out the message.
                {
                    cout << "Computer is bankrupted, you win the game." << endl;
                    return 0;
                }
            }
            if(i==j) //if it's draw, the player and the computer keep their own money
                cout << "Draw. You left " << pay1 << " dollars." << endl << endl;
        }
        if(k==3) //the last bet
        {
            cout << "Computer remains " << pay2 << " dollars." << endl;
            cout << "You remain " << pay1 << " dollars." << endl;
            if(pay2>pay1)
                cout << "You lose the game." << endl;
            if(pay2==pay1)
                cout << "Draw. You lose the game." << endl;
            if(pay2<pay1)
                cout << "You win the game." << endl;
        }
    }
    return 0;
}
