#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int dice1();
int dice2();


int main()
{
    int a,b,cmon=100,ymon=100,m;

    cout << "Computer: I want to play a game. You have to make a choice." << endl << endl <<endl;

    for ( int t=3 ; t>0 ; t--)
    {
        cout << "How much do you want to play in this round? ";
        cin  >> m;
        while((m>ymon)||(m<=0))
        {
            if(m>ymon)
            {
                while(m>ymon)
                {
                    cout << "you don't have that much money, please try again." << endl << endl << endl;
                    cout << "How much do you want to play in this round? ";
                    cin  >> m;
                }


            }

            if(m<=0)
            {
                while(m<=0)
                {
                    cout << "You can't input negative number or zero to your bet, please try again." << endl << endl << endl;
                    cout << "How much do you want to play in this round? ";
                    cin  >> m;
                }

            }
        }

        cout << "computer's dice point is: " << dice1() << endl;
        cout << "your dice point is: " << dice2() << endl;

        if(dice1()==dice2())//比大小
        {
            cout << "Draw. You left " << ymon << " dollars" << endl << endl << endl;
        }
        else if(dice1()<dice2())
        {
            cmon=cmon-m;
            ymon=ymon+m;
            if(cmon<0)//維持總金額為200
            {
                cmon=0;
            }
            if(ymon<0)
            {
                ymon=0;
            }
            if(cmon>200)
            {
                cmon=200;
            }
            if(ymon>200)
            {
                ymon=200;
            }
            cout << "congratulation! You win. You left " << ymon << " dollars" << endl << endl << endl;

        }
        else if(dice1()>dice2())
        {
            cmon=cmon+m;
            ymon=ymon-m;

            if(cmon<0)//維持總金額為200
            {
                cmon=0;
            }
            if(ymon<0)
            {
                ymon=0;
            }
            if(cmon>200)
            {
                cmon=200;
            }
            if(ymon>200)
            {
                ymon=200;
            }
            cout << "Sorry, you lose. You left " << ymon << " dollars" << endl << endl << endl;
        }
        if(cmon<=0)//中途結束
        {
            cout << "Computer is bankrupted, you win the game.";
            return 0;
        }
        if(ymon<=0)//中途結束
        {
            cout << "You don't have any money, you lose the game";
            return 0;
        }



    }

    if(cmon>ymon)//正常結束
    {
        cout << "Computer remains " << cmon << " dollars." << endl;
        cout << "you remains " << ymon << " dollars." << endl;
        cout << "You lose the game" << endl;
    }
    if(ymon>cmon)//正常結束
    {
        cout << "Computer remains " << cmon << " dollars." << endl;
        cout << "you remains " << ymon << " dollars." << endl;
        cout << "You win the game." << endl;
    }
    if(cmon==ymon)//正常結束
    {
        cout << "Computer remains " << cmon << " dollars." << endl;
        cout << "you remains " << ymon << " dollars." << endl;
        cout << "You lose the game" << endl;
    }
    return 0;
}

int dice1()
{
    srand((unsigned)time(NULL));//亂數

    int point1=rand(),point2=rand(),k;
    k=(point1%6)+1;
    return k;

}
int dice2()
{
    srand((unsigned)time(NULL));//亂數

    int point1=rand(),point2=rand(),l;
    l=(point2%6)+1;
    return l;

}
