#include <iostream>

using namespace std;

bool b[1000010] = {0};
int tb[1000010] = {0};

int f91(int);       // function of f91()

int main()
{
    int n;
    int table[110]={0};
    for(int i=1;i<=100;i++) // pre calculate f91(n) , n<=100
        table[i] = f91(i);

    while(cin >> n)
    {
        if(n<=0)    // end of input
            break;

        cout << "f91(" << n << ") = " ;
        if(n<=100)  // have calculated
            cout << table[n];
        else        // by define, print n-10
            cout << n-10;
        cout << endl;
    }
    return 0;
}

int f91(int n)// function of f91()
{
    if(n<=100)
        return f91(f91(n+11));
    else
        return n-10;
}
