#include <iostream>
#include <cstdlib>
#include <time.h>
using namespace std;

int dice()
{
    int point1=rand()%6+1;
    return point1;
}//隨機產生點數

int main()
{
    srand((unsigned)time(NULL));
    int dice();
    int n1,n2=3,yours=100,cpus=100;//設定一開始有的錢和3回合的記數
    cout<<"Computer: I want to play a game. You have to make a choice."<<endl;
    while(n2>0&&yours>0&&cpus>0)//3回合結束和你和電腦錢歸0會跳出這迴圈
    {
        cout<<"How much do you want to play in this round? ";
        cin>>n1;
        int d1=dice(),d2=dice();
        if (n1<=0)
            cout<<"You can't input negative number or zero to your bet,please try again."<<endl<<endl;
        else if (n1>yours)
            cout<<"You don't have that much money,please try again."<<endl<<endl;
        else//點數比大小和算錢
        {
            cout<<"Computer's dice point is: "<<d1<<endl;
            cout<<"Your dice point is: "<<d2<<endl;
            if (d1>d2)
            {
                cpus=cpus+n1;
                yours=yours-n1;
                cout<<"sorry,you lose.You left "<<yours<<" dollars."<<endl<<endl;
                n2--;
            }
            else if (d1==d2)
            {
                cout<<"Draw.You left "<<yours<<" dollars."<<endl<<endl;
                n2--;
            }
            else
            {
                cpus=cpus-n1;
                yours=yours+n1;
                cout<<"Congratulation! You win.You left "<<yours<<" dollars."<<endl<<endl;
                n2--;
            }
        }
    }
    if(yours<=0)//你破產情形
        cout<<"You don't have any money,you lose the game.";
    else if (cpus<=0)//電腦破產情形
        cout<<"computer is bankrupted,you win the game.";
    else//正常3回合情形
    {
        cout<<"Computer remains "<<cpus<<" dollars"<<endl;
        cout<<"You remains "<<yours<<" dollars"<<endl;
        if (cpus>yours)
            cout<<"You lose the game.";
        else if (cpus==yours)
            cout<<"Draw. You lose the game.";
        else
            cout<<"You win the game.";
    }

    return 0;
}
