#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int dice()
{

    int a=rand();
    return a%6+1;
}

int main()
{
    srand((unsigned)time(NULL));
    cout << "Computer: I want to play a game. You have to make a choice." << endl << endl;

    int play_monsum=100,com_monsum=100,money;

    for(int a=1; a<4 ; a++)
    {
        cout << "How much do you want to play in this round? ";
        cin >> money;

            while(money <= 0)
            {
                cout << "You can't input negative or zero to your bet, please try again."<<endl<<endl;
                cout << "How much do you want to play in this round? ";
                cin >> money;
            }
            while(money > play_monsum)
            {
                cout << "You don't have that much money, please try again."<<endl<<endl;
                cout << "How much do you want to play in this round? ";
                cin >> money;
            }

        int com_dice=dice();
        int play_dice=dice();

        cout << "Compputer's dice point is: " << com_dice << endl;
        cout << "Your dice point is: " << play_dice << endl;

        if(com_dice > play_dice)
        {
            play_monsum=play_monsum-money;
            com_monsum=com_monsum+money;
            if(play_monsum<0)
            {
                play_monsum=0;
            }
            cout << "Sorry, you lose. You left " << play_monsum << " dollars." << endl << endl << endl;

        }
        else if(com_dice < play_dice)
        {
            play_monsum=play_monsum+money;
            com_monsum=com_monsum-money;
            if(play_monsum>200)
            {
                play_monsum=200;
            }
            cout << "Congratlation! You win. You left " << play_monsum << " dollars." << endl << endl << endl;

        }
        else
        {
            cout << "Draw. You left " << play_monsum << " dollars." << endl << endl;
        }

        if(play_monsum<=0)
        {
                cout << "Player is bankrupted, computer win the game."<<endl;
                return -1;
        }
        else if(com_monsum<=0)
        {
                cout << "Computer is bankrupted, you win the game."<<endl;
                return -1;
        }

    }

    cout << "Computer remains " << com_monsum << " dollars." << endl;
    cout << "You remain " << play_monsum << " dollars." << endl;

    if(play_monsum > com_monsum)
    {
        cout << "You win the game." << endl << endl;
    }
    else if(play_monsum = com_monsum)
    {
        cout << "Draw. You lose the game." << endl << endl;
    }
    else if(play_monsum == com_monsum)
    {
        cout << "You lose the game." << endl << endl;
    }


system("pause");
return 0;
}

