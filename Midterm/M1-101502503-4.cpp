#include<iostream>
#include<cstdlib>
#include<ctime>
using namespace std;

int dice(int);

int main()
{
    int i=1,max=100,cmax=100; //宣告整數
    cout<<"Computer: I want to play a game. You have to make a choice."<<endl<<endl;
    srand((unsigned)time(NULL));//使每次亂數不同
    for(i=1;i<=3;i++)//讓遊戲進行三回合
    {
        int a,b,c=0,m=0,dollar;//宣告整數
        cout<<"How much do you want to play in this round? ";
        cin>>dollar;//輸入賭金
        while(dollar<=0)//如果賭金<=0,再輸一次
        {
            cout<<"You can't input negative number or zero to your bet , please try again."<<endl<<endl;
            cout<<"How much do you want to play in this round? ";
            cin>>dollar;
        }//end while
        while(dollar>max)//若賭金比所持金額大,再輸一次
        {
            cout<<"You don't have that too much money, please try again."<<endl<<endl;
            cout<<"How much do you want to play in this round? ";
            cin>>dollar;
        }//end while
        c=dice(a);//放入函數,c為電腦點數
        m=dice(b);//放入函數,m為玩家點數
        cout<<"Computer's dice point is: "<<c<<endl;
        cout<<"Your dice point is: "<<m<<endl;

        if( c>m ) //如果電腦點數>玩家點數
        {
            max=max-dollar;//玩家持有金額=玩家持有金額-賭金
            cmax=cmax+dollar;//電腦持有金額=電腦持有金額+賭金
            cout<<"Sorry, you lose. You left "<<max<<" dollars."<<endl;
        }//end if
        else if( m>c ) //如果玩家點數>電腦點數
        {
            max=max+dollar;//玩家持有金額=玩家持有金額+賭金
            cmax=cmax-dollar;//電腦持有金額=電腦持有金額-賭金
            cout<<"Congratulation! You win. You left "<<max<<" dollars."<<endl;
        }//end else if
        else //如果平手
            cout<<"Draw. You left "<<max<<" dollars."<<endl;
        cout<<endl;
        if(max<=0) //玩家破產,結束程式
        {
            cout<<"You don't have any money, you lose the game.";
            return -1 ;
        }// end if
        else if (cmax<=0)//電腦破產,結束程式
        {
            cout<<"Computer is bankrupted, you win the game.";
            return -1 ;
        }//end else if
    }
    if(max>200)//如果玩家金額超過200,化為200
        max=200;
    if(cmax>200)//如果電腦金額超過200,化為200
        cmax=200;
    cout<<"Computer remains "<<cmax<<" dollars."<<endl;//輸出電腦持有金額
    cout<<"You remain "<<max<<" dollars."<<endl;//輸出玩家持有金額
    if(cmax>max)//如果電腦金額>玩家金額,電腦贏
        cout<<"You lose the game."<<endl;
    else if (max>cmax)//如果玩家金額>電腦金額,玩家贏
        cout<<"You win the game."<<endl;
    else if(max=cmax)//如果玩家金額與電腦金額相同,電腦贏
        cout<<"Draw. You lose the game."<<endl;
    return 0;
}// end main
int dice(int x)
{
    int a = rand()%6+1 , b= rand()%6+1; //宣告整數,a和b為1~6之間的亂數
    return a , b;//回傳a和b
} // end dice()
