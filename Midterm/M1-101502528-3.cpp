#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int dice(int x) //此為運算骰子點數的函式
{
    int point;
    point=(x%6)+1;

    return point;
}

int main()
{
    srand((unsigned)time(NULL));
    cout << "Computer: I want to play a game. You have to make a choice." << endl << endl;

    for (int time=3;time>=1;time--) //因只玩三場,所以設time=3並每場減1
    {
        int a=rand(),b=rand(),com,you;
        com=dice(a);
        you=dice(b);

        cout << "Computer's dice point is: " << com << endl;
        cout << "Your dice point is: " << you << endl;

        if(you>com) //玩家點數大於電腦則獲勝
            cout << "Congratulation! You win." << endl << endl;

        else if (you==com) //玩家點數等於電腦則平手
            cout << "Draw" << endl << endl;

        else if (you<com) //玩家點數小於電腦則輸
            cout << "Sorry, you lose." << endl << endl;
    }
    return 0;
}
