#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int dice()
    {
        int a=rand();
        return a%6+1;
    }

int main()
{
    srand((unsigned)time(NULL));
    int com_cash=100,com_dice;
    int player_cash=100,player_dice,player_money;//cash=本金;money=賭金
    int count=3;
    cout << "Computer: I want to play a game. You have to make a choice." << endl;
    while(count!=0)
    {
        cout<<"How much do you want to play in this round ? ";
        if( cin>>player_money && player_money>0 && player_money<=player_cash)//判斷輸入
        {
            com_dice=dice();//執行亂數
            player_dice=dice();
            cout<<"Computer's dice point is : "<<com_dice<<endl;
            cout<<"Your's dice point is : "<<player_dice<<endl;
            //三種case
            if(com_dice>player_dice)//lose
            {
                player_cash=player_cash-player_money;
                com_cash=com_cash+player_money;
                if(player_cash<=0)//輸到破產
                {
                    cout<<"Sorry, you lose. You left 0 dollars. "<<endl;
                    player_cash=0;
                    break;
                }
                else//輸但沒破產
                cout<<"Sorry, you lose. You left "<<player_cash<<" dollars"<<endl;
            }

            else if(com_dice<player_dice)//win
            {
                player_cash=player_cash+player_money;
                com_cash=com_cash-player_money;
                cout<<"Congratulation! You win. You left "<<player_cash<<" dollars"<<endl;
            }
            else//draw
            {
                cout<<"Draw. You left "<<player_cash<<" dollars"<<endl;
            }
            count--;
            cout<<endl;
    }
    else if(player_cash<player_money)//不夠錢做賭注
        cout<<"You don't have that much money, please try again."<<endl<<endl;
    else//輸入為負數或零
        cout<<"You can't input negative number or zero to your bet, please try again."<<endl<<endl;
    }

    if(com_cash>200)//調整賭金範圍介於0~200
        com_cash=200;
    if(player_cash>200)
        player_cash=200;
    if(com_cash<0)
        com_cash=0;
    if(player_cash<0)
        player_cash=0;

    cout<<endl<<"Computer remains "<<com_cash<<" dollars"<<endl;//輸出勝負結果
    cout<<"You remains "<<player_cash<<" dollars"<<endl;
    if(com_cash>=player_cash)
        cout<<"You lose the game."<<endl;
    else
        cout<<"You win the game."<<endl;

    return 0;
}
