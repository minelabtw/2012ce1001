// M1-
#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int dice(int x)
{
    int computers,your;
    srand((unsigned)time(NULL));
    cout << "Computer: I want to play a game. You have to make a choice." << endl << endl ;

    for ( int i=0; i<3 ; i++ )
    {
        int a = rand();
        int b = rand();
        computers = a%6+1;
        your = b%6+1;
        cout << "Computer's dice point is: " << computers << endl;
        cout << "Your dice point is: " << your << endl;
        if ( computers > your )
            cout << "Sorry, you lose." << endl << endl;
        else if (computers < your)
            cout << "Congratulation! You win." << endl << endl;
        else if (computers == your )
            cout << "Draw" << endl << endl;
    }
    return 0;
}

int main()
{
    dice(0);
    return 0;
}
