#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int dice (int) // 自定義函數dice
{
    srand((unsigned)time(NULL));
    int point = rand();
    return point;
}

int main()
{

    cout << "Computer: I want to play a game. You have to make a choice" << endl << endl;

    while (1) //重複輸入
    {
        cout << "How much do you want to play in this round? ";

        int money;
        cin >> money;

        if ( money < 1 ) //不可輸入0或負數
            cout << "You can't input negative number or zero to your bet, please try again." << endl << endl;
        else if ( money > 100 ) //不可超過一開始的金額100
            cout << "You don't have that much money, please try again." << endl << endl;
        else
            break;
    }
    srand((unsigned)time(NULL)); // 隨機產生數字
    int a = rand();
    int b = rand();
    int buffer1 = rand() , buffer2 = rand() ,buffer3 = rand();

    for( int i = 1; i <= 3; i++ ) // 執行三回合
    {
        cout << "Computer's dice point is: " << a%6 + 1 << endl;
        cout << "Your dice point is: " << b%6 + 1 << endl;

        if ( a%6 + 1 > b%6 + 1 )
            cout << "Sorry, you lose.";
        else if ( a%6 + 1 < b%6 + 1 )
            cout << "Congratulation! You won.";
        else
            cout << "Draw";
        cout << endl << endl;

        a = buffer1; //讓a與b的數值不會儲存到下回合
        b = buffer2;
        buffer1 = buffer2;
        buffer1 = buffer3;
    }
    return 0;
}











