#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;
int main()
{
    srand((unsigned)time(NULL));//錯亂rand()
    int count=3;
    while(count>0)
    {
        int a=rand()%6+1; //a隨機 在1~6之間
        int b=rand()%6+1; //b隨機 在1~6之間
        cout << "Computer's dice point is: " << a <<endl;
        cout << "Your dice point is: " << b <<endl;
        if (a>b)//輸的條件
            cout << "Sorry, you lose."<<endl<<endl;
        else if (a<b)//贏的條件
            cout << "Congratulation! You win."<<endl<<endl;
        else//平手
            cout << "Draw"<<endl<<endl;

        count--;
    }
    return 0;
}
