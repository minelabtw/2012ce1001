#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int main()
{
    int money;
    srand((unsigned)time(NULL));

    cout << "Computer: I want to play a game. You have to make a choice." << endl;
    cout << endl;
    cout << endl;

    while(money>0)
    {
        cout << "How much do you want to play in this round? ";
        cin >> money;
        if(money<=0)
        {
            cout << "You can't input negative number or zero to your bet, please try again." << endl;
        }
        else if(money>100)
        {
            cout << "You don't have that much money, please try again." << endl;
        }

        int a = rand();//讓隨機數放入a
        cout << "Computer's dice point is: " << a%6+1 << endl;//讓隨機數a的範圍控制在1~6
        int b = rand();//讓隨機數放入b
        cout << "Your dice point is: " << b%6+1 << endl;//讓隨機數b的範圍控制在1~6

        if(a%6+1>b%6+1)//若電腦的數字大於玩家
        {
            money=100-money;
            cout << "Sorry, you lose. You left " << money <<" dollars." << endl;
            cout << endl;
            cout << endl;
        }
        else if(a%6+1==b%6+1)//若電腦的數字與玩家相同
        {
            cout << "Draw. You left " << 100 << " dollars." << endl;
            cout << endl;
            cout << endl;
        }
        else if(a%6+1<b%6+1)//若電腦的數字小於玩家
        {
            money=100-money;
            cout << "Congratulation! You win. You left " << money << " dollars." << endl;
            cout << endl;
            cout << endl;
        }
    }
    return 0;
}
