//Midterm-100502201-4
//A program to play dice game
//User have 100 dollars at first, com is vice versa, see who runs out of money
#include<iostream>
#include<ctime> //Time function is used to change parameters in rand every sec
#include<cstdlib> //Used for rand
using namespace std;

int dice() //Self-definition function, range from 1 to 6
{
    int a;
    a=rand();
    a=a%6+1;
    return a;
}

int main()
{
    int x,y,cou=0,com=100,user=100,gam; //Initialization of every int
    srand((unsigned)time(NULL)); //Random numbers
    cout<<"Computer: I want to play a game. You have to make a choice.\n"<<"\n";
    while (cou<3) //We'll play three times
    {
        cout<<"How much do you want to play in this round? ";
        while(cin>>gam)
        {
            while (gam<=0) //Negative or zero condition
            {
                cout<<"You can't input negative number or zero to your bet, please try again.\n\n";
                cout<<"How much do you want to play in this round? ";
                break;
            }
            while (gam>user)
            {
                cout<<"You don't have that much money, please try again.\n\n";
                cout<<"How much do you want to play in this round? ";
                break;
            }
            if (gam<=user&&gam>0)
                break;
        }
        x=dice(); //Com's dice
        cout<<"Computer's dice point is: "<<x<<endl;
        y=dice(); //User's dice
        cout<<"Your dice point is: "<<y<<endl;
        if(x==y) //Draw
            cout<<"Draw. You left "<<user<<" dollars.\n\n";
        else if(x>y) //Com wins
        {
            cout<<"Sorry, you lose. You left "<<user-gam<<" dollars.\n\n";
            user=user-gam;
            com=com+gam;
        }
        else //User wins
        {
            cout<<"Congratulation! You win. You left "<<user+gam<<" dollars.\n\n";
            com=com-gam;
            user=user+gam;
        }
        //Bankrupted during game
        if(user==0)
        {
            cout<<"You don't have any money, you lose the game.";
            return 0;
        }
        if(com==0)
        {
            cout<<"Computer is bankrupted, you win the game.";
            return 0;
        }
        cou++;
    }
    //Result and judge that who wins or loses
    cout<<"Computer remains "<<com<<" dollars."<<endl;
    cout<<"You remain "<<user<<" dollars."<<endl;
    if (com>user)
        cout<<"You lose the game.";
    else if (com==user)
        cout<<"Draw. You lose the game.";
    else
        cout<<"You win the game.";
    return 0;
}
