#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int main()
{

    int times = 1;
    srand((unsigned)time(NULL));

    cout << "Computer: I want to play a game. You have to make a choice."<<endl<<endl<<endl;
    while ( times <= 3 )
    {
        int x,y;
        int a = rand(),b = rand();
        x = a % 6 + 1;
        y = b % 6 + 1;

        if ( x > y )
        {
            cout << "Computer's dice point is:" << x <<endl;
            cout << "Your dice point is: " << y<<endl;
            cout << "Sorry, you lose."<<endl<<endl<<endl;

        }
        if ( x < y )
        {
            cout << "Computer's dice point is:" << x<<endl;
            cout << "Your dice point is: " << y<<endl;
            cout << "Congratulation! You win."<<endl<<endl<<endl;
        }
        if ( x == y )
        {
            cout << "Computer's dice point is:" << x<<endl;
            cout << "Your dice point is: " << y<<endl;
            cout << "Draw"<<endl<<endl<<endl;
        }
        times ++;
    }
}
