#include<iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;


int dice();
bool betCheck(int playerMoney, int bet);
bool game(int &comMoney, int &playerMoney);
void moneyAdjust(int &n);
void judge(int comMoney, int playMoney);


int main()
{
    //initial
    srand(time(NULL));
    int comMoney=100,playerMoney=100;
    cout<<"Computer: I want to play a game. You have to make a choice.\n";


    //game
    for(int round=0;round<3;round++)
        if(!game(comMoney,playerMoney))
            break;
    judge(comMoney,playerMoney);


    //end
    cout<<endl;
    return 0;
}




bool betCheck(int playerMoney,int bet)
{
    if(bet<=0)
    {
        cout<<"You can't input negative number or zero to your bet, please try again.\n";
        return false;
    }
    else if(bet>playerMoney)
    {
        cout<<"You don't have that much money, please try again.\n";
        return false;
    }

    //bet is OK.
    return true;
}


int dice()
{
    int point=rand()%6+1;
    return point;
}


bool game(int &comMoney, int &playerMoney)
{
    //input bet
    int bet;
    do
    {
        cout<<"\nHow much do you want to play in this round? ";
        cin>>bet;
    }while(!betCheck(playerMoney,bet));


    //dice
    int comDice=dice();
    int playerDice=dice();


    //game
    cout<<"Computer's dice point is: "<<comDice<<endl;
    cout<<"Your dice point is: "<<playerDice<<endl;
    if(comDice>playerDice)
    {
        cout<<"Sorry, you lose. ";
        comMoney+=bet;
        playerMoney-=bet;
    }
    else if(comDice<playerDice)
    {
        cout<<"Congratulation! You win. ";
        comMoney-=bet;
        playerMoney+=bet;
    }
    else
    {
        cout<<"Draw. ";
    }
    moneyAdjust(playerMoney);
    cout<<"You left "<<playerMoney<<" dollars.\n";


    if(playerMoney<200&&playerMoney>0)//continue game
        return true;
    else//game over
        return false;
}


void moneyAdjust(int &n)
{
    if(n>200)
        n=200;
    else if(n<0)
        n=0;
}


void judge(int comMoney, int playerMoney)
{
    cout<<endl;
    if(playerMoney==0)
        cout<<"You don't have any money, you lose the game.";
    else if(playerMoney==200)
        cout<<"Computer is bankrupted, you win the game.";
    else
    {
        cout<<"Computer remains "<<comMoney<<" dollars.\n";
        cout<<"You remain "<<playerMoney<<" dollars.\n";
        if(playerMoney>comMoney)
            cout<<"You win the game.";
        else
            cout<<"You lose the game.";
    }
}
