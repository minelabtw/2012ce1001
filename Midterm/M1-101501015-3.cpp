#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int dice1(int);//建立自定義函示
int dice2(int);
int main()
{
    cout<<"Computer: I want to play a game . You have to make a choice ."<<endl;
    for(int i=1;i<=3;i++)//執行三次
    {
        srand((unsigned)time(NULL));
        cout<<"Computer's dice point is :"<<dice1(i)<<endl;//呼叫定義函示
        cout<<"Your dice point is :"<<dice2(i)<<endl;
        if(dice1(i)>dice2(i))//若小於電腦
            cout<<"Sorry,you lose. "<<endl;
        if(dice1(i)<dice2(i))//若大於電腦
            cout<<"Congratulation! You win ."<<endl;
        if(dice1(i)==dice2(i))//若等於電腦
            cout<<"Draw"<<endl;
        cout<<endl;
    }
    return 0;
}
int dice1(int y1)
{
    srand((unsigned)time(NULL));
    y1=rand();//使用亂數
    y1=y1%6+1;//使之介於1和6之間
    return y1;
}
int dice2(int y2)
{
    y2=rand();
    y2=y2%6+1;
    return y2;
}
