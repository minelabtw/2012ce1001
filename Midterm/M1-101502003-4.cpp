#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;//可以省略std::

int dice (int x)//自訂函式 (電腦的骰子)
{
    srand((unsigned)time(NULL)); //隨機取數
    x=rand()%6+1; //%6+1剛好會是1~6=骰子的數字
    return x;
}
int dice1 (int y) //自訂函式(玩家的骰子)
{
    srand((unsigned)time); //隨機取數
    y=rand()%6+1;//%6+1剛好會是1~6=骰子的數字
    return y;
}

int main()
{
    int play,com,count=1,cprice=100,pprice=100,choice;
    //設變數play=玩家點數,com=電腦點數,cprice=電腦一開始籌碼,pprice=玩家一開始籌碼
    //choice=玩家要下的籌碼,count=完的次數
    cout << "Computer: I want to play a game. You have to make a choice. " << endl<<endl;
    //輸出文字
//////////////////////////////迴圈開始////////////////////////////////////////
    while(count <=3)//如果玩小於3次進入迴圈
    {
        cout << "How much do you want to play in this round? " ;//輸出文字
        cin>>choice;//輸入籌碼
        if (choice<=0) //如果籌碼小於等於0,輸出下列文字,並且重新輸入籌碼
        {
            cout << "You can't input negative number or zero to your bet, please try again." << endl;
        }
        else if (choice>100) //如果籌碼大於100,輸出下列文字並重新輸入籌碼
        {
            cout << "You don't have that much money, please try again." << endl;
        }
        else//如果籌碼範圍輸入對,進入以下
        {
            cout << "Computer's dice point is : " << dice(com) << endl;//輸出電腦點數
            cout << "Your dice point is : " << dice1(play)<<endl;//輸出玩家點數
            if(dice(com)>dice1(play))//如果電腦>玩家
            {
                pprice=pprice-choice;//你的錢
                cprice=cprice+choice;//電腦的錢
            ////如果超過200或<0時修正////
                if (cprice>200)
                    cprice=200;
                if(pprice<0)
                    pprice=0;
            ///////////////////////////
                cout << "Sorry, you lose. You left " << pprice << " dollars." << endl<<endl;
                //輸出文字
                if(pprice==0)//如果你沒錢了,輸出下列文字並結束程式
                {
                    cout << "You don't have any money, you lose the game."<<endl;
                    return 0;
                }

            }
            else if (dice(com)<dice1(play)) //如果電腦<玩家,進入以下
            {
                pprice=pprice+choice;//你的錢
                cprice=cprice-choice;//電腦的錢
            ////如果超過200或<0時修正////
                if (cprice<0)
                    cprice=0;
                if (pprice>200)
                    pprice=200;
            ///////////////////////////
                cout << "Congratulation! You win. You left " << pprice << " dollars." << endl<<endl;
                //輸出文字
                if (pprice==200)//如果電腦沒錢了,輸出下列文字並結束程式
                {
                    cout << "Computer is bankrupted, you win the game." <<endl;
                    return 0;
                }
            }
            else //平手的話輸出下列文字.並重新賭注
            {
                cout << "Draw. You left " << pprice << " dollars." << endl<<endl;
            }
            count++; //count+1,進行下一輪
        }
    }
/////////////////////////////////迴圈結束//////////////////////////////////////
    cout << endl <<"Computer remains "<<cprice << " dollars."<<endl;//輸出電腦剩餘的錢
    cout << "You remain " << pprice << " dollars." << endl;//輸出玩家剩餘的錢
    if(cprice<pprice) //如果玩家>電腦,則輸出下列文字
        cout << "You win the game." << endl;
    else if (cprice>pprice)//如果玩家<電腦,輸出下列文字
        cout << "You lose the game." << endl;
    else //如果玩家=電腦,輸出下列文字
        cout <<"Draw. You lose the game." << endl;
    return 0;
}
