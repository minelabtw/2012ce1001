#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int main()
{
    cout << "Computer: I want to play a game. You have to make choice" << endl << endl;
    srand((unsigned)time(NULL));                  //以時間為亂數.
    for(int i = 1; i <= 3; i++)                   //做三次.
    {
        int a = rand();                           //a為亂數.
        int b = rand();                           //b為亂數
        int c, d;
        c = a % 6 + 1;                            //骰子算法.
        d = b % 6 + 1;
        cout << "Computer's dice point: " << c << endl;
        cout << "Your dice point is: " << d << endl;
        if( c > d)                                //電腦贏.
        {
            cout << "Sorry,you lose." << endl << endl;
        }
         if( c < d)                               //玩家贏.
        {
            cout << "Congratulation! You win." << endl << endl;
        }
         if( c == d)                              //平手.
        {
            cout << "Draw" << endl << endl;
        }
    }

    return 0;
}
