#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int dice(int);//自訂一函式
int main()
{
    srand((unsigned)time(NULL));//打亂變數
    int a,b,i,j=100,k,x,y,z;
    int e=3;//回合數
    cout<<endl;
    cout<<"Computer: I want to play a game. You have to make a choice.";
    cout<<endl;
    cout<<endl;
    while(e>=1)//進行三回合
    {
        do
        {
            cout<<"How much do you want to play in this round? ";
            cin>>i;
            if(i<=0||i>j)//進行偵錯，如果輸入的金額超過或為負數
            {
                if(i<=0)
                    cout<<"You can't input negative number or zero to your bet, please try again.";
                else
                {
                    cout<<"You don't have that much money, please try again.";
                }
                cout<<endl<<endl;
            }
        }while(i<=0||i>j);

        x=dice(a);//將變數傳入函式進行亂數產生
        y=dice(b);
        cout<<"Computer's dice point is: "<<x<<endl;
        cout<<"Your dice point is: "<<y<<endl;
        if(x>y)//電腦贏的話，扣錢
        {
            j=j-i;
            cout<<"Sorry, you lose. You left "<<j<<" dollars."<<endl<<endl;
            if(j<=0)//破產
            {
                cout<<"You don't have any money, you lose the game.";
                return 0;
            }
        }
        if(x<y)//玩家贏
        {
            j=j+i;//加錢
            if(j<200)
            {
                cout<<"Congratulation! You win. You left "<<j<<" dollars."<<endl<<endl;
            }
            if(j>=200)//破產，如果超過200則輸出200
            {
                cout<<"Congratulation! You win. You left 200 dollars."<<endl<<endl;
                cout<<"Computer is bankrupted, you win the game.";
                return 0;
            }
        }
        if(x==y)//平手
        {
            cout<<"Draw. You left "<<j<<" dollars."<<endl<<endl;
        }
        e--;
    }
    if(j<200||j>0)//如果三回合沒有人破產
    {
        cout<<"Computer remains "<<200-j<<" dollars."<<endl;
        cout<<"You remain "<<j<<" dollars."<<endl;
        if(j>100)//玩家錢比較多
        {
            cout<<"You win the game"<<endl;
        }
        if(j<100||j==100)//平手或玩家錢比較少
        {
            if(j==100)
                cout<<"Draw. You lose the game."<<endl;
            else
                cout<<"You lose the game."<<endl;
        }
    }
    return 0;//回傳
}

int dice(int f)//自訂一函式
{
    int h;
    f=rand();
    h=f%6+1;//產生1~6的亂數
    return h;
}

