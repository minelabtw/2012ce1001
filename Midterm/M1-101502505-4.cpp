#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int dice()
{
    int j;
    j = rand()%6 + 1;
    return j;
}

int main()
{
    int mon;
    cout << endl;
    cout << "Computer: I want to play a game. You have to make a choice." << endl;
    cout << endl;
    while( cin != false )
    {
        cout << endl;
        cout << "How much do you want to play in this round? ";
        cin >> mon;
        cout << endl;

        int a,b,m1 = 100, m2 = 100;
        a = dice();
        b = dice();

        if ( mon == 0 || mon < 0)//輸入為負數或0
            cout << "You can't input negative number or zero to your bet, please try again." << endl;

        else if ( mon > 100)//輸入過大
            cout << "You don't have that much money, please try again." << endl;

        else if( mon > 0 && mon <= 100 )
        {
            for(int i = 1; i <= 3 && m1 > 0 && m2 > 0; i++)
            {
                cout << "Computer's dice point is: " << a << endl;
                cout << "Your dice point is: " << b << endl;

                if( a == b )//平手
                {
                    cout << "Draw" << endl;
                    m1 = m1 - mon;
                    m2 = m2 + mon;
                }

                else if( a > b)//玩家輸
                {
                    cout << "Sorry, you lose." << endl;
                    m1 = m1 - mon;
                    m2 = m2 + mon;
                }

                else if( a < b )//玩家贏
                {
                    cout << "Congratulation! You win." << endl;
                    m1 = m1 + mon;
                    m2 = m2 - mon;
                }

                if ( m1 > 0)//玩家仍有籌碼
                {
                    cout << "You left " << m1 <<" dollars." << endl;
                    cout << "How much do you want to play in this round? ";
                    cin >> mon;
                    cout << endl;
                }

                else if ( m1 <= 0)//玩家沒有籌碼
                {
                    cout << "You left 0 dollars." << endl;
                    cout << endl;
                    cout << "You don't have any money, you lose the game." << endl;
                    break;
                }

                else if( m1 >= 200)//玩家全贏
                {
                    cout << "You left 200 dollars." << endl;
                    cout << endl;
                    cout << "Computer is bankrupted, you win the game.";
                    break;
                }
            }
/////////////////3回合結束後//////////////////////
            if( m1 < m2 )//玩家輸
            {
                cout << "You remain " << m1 << " dollars." <<endl;
                cout << "You lose the game." <<endl;
                break;
            }
            if(m1 == m2)//平手玩家輸
            {
                cout << "Computer remain " << m2 << " dollars." <<endl;
                cout << "You remain " << m1 << " dollars." <<endl;
                cout << "Draw. You lose the game." <<endl;
                break;
            }
            else if (m1 > m2)//玩家贏
            {
                cout << "Computer remain " << m2 << " dollars." <<endl;
                cout << "You remain " << m1 << " dollars." <<endl;
                cout << "You win the game." <<endl;
                break;
            }
        }
    }
    return 0;
}
