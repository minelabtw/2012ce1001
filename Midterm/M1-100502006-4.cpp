#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int dice();//function prototype

int main()
{
    int MC=100,MU=100;//initialize bets
    cout<<"Computer: I want to play a game. You have to make a choice."<<endl;
    cout<<endl;
    srand(time(NULL));//set seed for rand()
    for(int i=0;i<3;i++)//rounds counts
    {
        int a,b,Inp;
        do
        {
            cout<<"How much do you want to play in this round? ";
            cin>>Inp;
            if(Inp<=0)
            {
                cout<<"You can't input negative or zero to your bet, please try again."<<endl;
                cout<<endl;
            }
            else if(Inp>MU)
            {
                cout<<"You don't have that much money, please try again."<<endl<<endl;
            }
            else
            {
                MU-=Inp;//collect bets
                MC-=Inp;//collect bets
                break;
            }
        }while(1);
        a=dice();//do the dice
        b=dice();//do the dice
        cout<<"Computer's dice point is: "<<a<<endl;
        cout<<"Your dice point is: "<<b<<endl;
        if(a>b)
        {
            cout<<"Sorry, You lose.";
            MC+=2*Inp;//give the bets
        }
        else if(a==b)
        {
            cout<<"Draw.";
            MU+=Inp;//return the bets
            MC+=Inp;//return the bets
        }
        else
        {
            cout<<"Congratulation! You win.";
            MU+=2*Inp;//give the bets
        }
        cout<<" You left "<<MU<<" dollars."<<endl<<endl;//static how much you left
        if(MU==0)//if you have no money, the game is over
        {
            cout<<"You don't have any money, you lose the game."<<endl;
            return 0;
        }
        else if(MC<=0)//if computer have no money, the game is over
        {
            MC=0;
            cout<<"Computer is bankrupted, you win the game."<<endl;
            return 0;
        }
    }
    cout<<"Computer remains "<<MC<<" dollars."<<endl;
    cout<<"You remain "<<MU<<" dollars."<<endl;
    if(MC>MU)//campare the bets remained after three rounds
    {
        cout<<"You lose the game."<<endl;
    }
    else if(MC<MU)
    {
        cout<<"You win the game."<<endl;
    }
    else
    {
        cout<<"Draw. You lose the game."<<endl;
    }
    return 0;
}

int dice()//function of rolling a dice
{
    return rand()%6+1;
}
