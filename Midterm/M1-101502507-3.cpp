//Play the dice with computer
#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int dice();

int main(int)
{
    dice();
}

int dice()
{
    int i;
    srand((unsigned)time(NULL));

    cout << "Computer: I want to play a game. You have to make a choice." << endl << "\n";

    for( i = 1 ; i <= 3 ; i++ )//play three times and end the game
    {
        int a = rand() % 6 + 1;//a and b are both random numbers, and they are between 1 and 6
        int b = rand() % 6 + 1;

        cout << "Computer's dice point is: " << a << endl;//cout Computer and players' dice point
        cout << "Your's dice point is: " << b << endl;

        if( a > b )//Computer's dice point is bigger than player's dice point
        {
            cout << "Sorry, you lose." << endl;
            cout << "\n";
        }

        if( a == b )//Computer's dice point is as same as player's dice point
        {
            cout << "Draw" << endl;
            cout << "\n";
        }

        if( a < b )//Computer's dice point is smaller than player's dice point
        {
            cout << "Congratulation! You win." << endl;
            cout << "\n";
        }
    }

    return 0;
}
