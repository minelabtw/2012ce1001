#include <iostream>
#include <cstdlib>
#include <time.h>
using namespace std;
int dice (int);//定義涵式

int main()
{
    srand((unsigned)time(NULL));//產生亂數種子
    int n=2,a,b,money,money1=100,money2=100;
    cout<<"Computer: I want to play a game. You have to make a choice.";
    cout<<endl;
    cout<<"\nHow much do you want to play in this round? ";
    cin>>money;
    while(n>0)//三回合
    {
        a=dice(a),b=dice(b);

        if(money ==0||money<0||money>100)//判斷輸入是否正確
        {
            if(money>100)
                cout<<"You don't have that much money, please try again."<<endl;
            else
                cout<<"You can't input negative number or zero to your bet, please try again."<<endl;
            return 0;
        }
        cout<<"Computer's dice point is: "<<a<<endl;
        cout<<"Your dice point is: "<<b<<endl;
        if(a>b)//電腦贏
        {
            money1=money1-money;
            money2=money2+money;
            if(money1==0)
            {
                cout<<"Sorry, you lose. You left 0 dollars.";
                return 0;
            }
            cout<<"Sorry, you lose. You left " <<money1<< " dollars."<<endl;

        }
        else if(a<b)//我們贏
        {
            money2=money2-money;
            money1=money1+money;
            if(money2==0)
            {
                cout<<"Congratulation! You win. You left 200 dollars.";
                return 0;
            }
            cout<<"Congratulation! You win. You left "<<money1<<"dollars."<<endl;
        }
        else if(a==b)
        {
            cout<<"Draw. You left" <<money1<< " dollars."<<endl;
        }

        cout<<"\nHow much do you want to play in this round? ";
        cin>>money;//重複輸入
        n--;//遞減次數
    }
    if(money1>200)
    {
        money1=200;
    }
    if(money2>200)
    {
        money2=200;
    }
    cout<<"Computer remains "<<money2<<"dollars."<<endl;
    cout<<"You remain "<<money1<<"dollars."<<endl;
    if(money1>money2)//63~68輸出最後勝負
        cout<<"You win the game."<<endl;
    else if(money<money2)
        cout<<"You lose the game."<<endl;
    else if(money==money2)
        cout<<"Draw. You lose the game."<<endl;

    return 0;
}

int dice(int)//內容
{

    int a=rand()%6+1;

    return a;//回傳a值
}
