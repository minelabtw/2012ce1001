#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int dice(int);

int main()
{
    cout<<"Computer: I want to play a game. You have to make a choice."<<endl; // first question
    int a=1,b,d=100,e,f;
    while(a<=3) // situations
    {
        cout<<"How much do you want to play in this round? ";
        cin>>b;
        if(b<=0)//error contection
            cout<<"You can't input negative number or zero to your bet, please try again."<<endl;
        else if(b>d)// error contection
            cout<<"You don't have that much money, please try again."<<endl;
        cout<<"Computer's dice point is: "<<dice(a)<<endl;
        cout<<"Your dice point is: "<<(dice(a)*100%6)+1<<endl;
        e=dice(a);
        f=(dice(a)*100%6+1);
        if(e>=f)//the resault
        {
            cout<<"Sorry, you lose.You left "<<d-b<<" dollars"<<endl;
            b=b-d;
        }
        else if(e<f)//the resault
             {
                 cout<<"Congratulations! You win. You left "<<d+b<<" dollars"<<endl;
                 d=d-b;
             }
        if(b<=0)// the player is bankrupted
            return 0;
        a++;
    }
    return 0;
}

int dice(int)
{
    srand((unsigned)time(NULL));
    int e=rand(),d=rand();
    d=d%6+1;
    return d;
}
