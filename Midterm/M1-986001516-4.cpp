#include <iostream>
#include <time.h>
#include <cstdlib> /* Head file including */
using namespace std;

int dice() /* function to do dice-rolling (question 1-3) */
{
    int point;
    point = rand()%6+1;
    return point;
}

int main()
{
    int c_p,p_p,c_m=100,p_m=100,bet; /* all the variable, computer's and player's money(c_m,p_m), computer's and player's point(c_p,p_p), player's bet(bet) */
    srand((unsigned)time(NULL)); /* use time function to generate a random-like numbers */
    cout << "Computer: I want to play a game. You have to make a choice.\n\n"; /* question 1-1 */
    for (int i=1;i<=3;i++) /* 3 rounds, so the for loop would operate for 3 times (question 1-2) */
    {
        for (int j=0;j>=0;j++) /* this loop would not break until user input a number that they can afford. */
        {
            cout << "How much do you want to play in this round? ";
            cin >> bet;
            if (bet<=0)
            cout << "You can't input negative number or zero to your bet, please try again.\n\n";
            else if (bet>p_m)
            cout << "You don't have that much money, please try again.\n\n";
            else
            break;
        }
        c_p=dice();
        p_p=dice();
        cout << "Computer's dice point is: " << c_p << endl;
        cout << "Your dice point is: " << p_p << endl; /* after user input their bet, then generate a random dice number for both player and computer */
        if (c_p>p_p)
        {
            c_m+=bet;
            p_m-=bet;
            if (c_m<=0)
            {
                c_m=0;
                p_m=200;
            }
            if (p_m<=0)
            {
                c_m=200;
                p_m=0;
            } /* the limitation of value is 0 to 200 */
            cout << "Sorry, you lose. You left " << p_m << " dollars.\n";
        }
        else if (c_p<p_p)
        {
            c_m-=bet;
            p_m+=bet;
            if (c_m<=0)
            {
                c_m=0;
                p_m=200;
            }
            if (p_m<=0)
            {
                c_m=200;
                p_m=0;
            } /* the limitation of value is 0 to 200 */
            cout << "Congratulation! You win. You left " << p_m << " dollars.\n";
        }
        else
        {
            if (c_m<=0)
            {
                c_m=0;
                p_m=200;
            }
            if (p_m<=0)
            {
                c_m=200;
                p_m=0;
            } /* the limitation of value is 0 to 200 */
            cout << "Draw. You left " << p_m << " dollars.\n";
        } /* line 35 to line 50 will compare computer's point to  player's point, then change their money */
        if (p_m==0)
        {
            cout << "\nYou don't have any money, you lose the game.\n";
            return -1;
        }
        if (p_m==200)
        {
            cout << "\nComputer is bankrupted, you win the game.\n";
            return -1;
        } /* line 51 to line 60, if computer or player has no money, then over this game. */
        cout << endl;
    }
    if (p_m>c_m) /* after playing, compare computer's money and player's, then display who is the winner. */
    {
        cout << "Computer remains " << c_m << " dollars.\n";
        cout << "You remain " << p_m << " dollars.\n";
        cout << "You win the game.\n\n\n";
    }
    else
    {
        cout << "Computer remains " << c_m << " dollars.\n";
        cout << "You remain " << p_m << " dollars.\n";
        cout << "You lose the game.\n";
    }
    return 0;
}
