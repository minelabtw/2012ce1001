#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int dice()      //自訂函數
{
    int x = rand();
    x = x % 6 +1;

    return x;
}


int main()
{

    srand((unsigned)time(NULL));  // 打亂數

    cout << "Computer: I want  to play a game . You have to make a choice."
         << endl << endl;

    int i,compoint,manpoint;
    int commoney = 100;
    int manmoney = 100;
    int bet;
    for( i = 0 ; i < 3 ; i++)
    {
        cout << endl;
        cout << "How much do you want to play in this round? ";
        cin >> bet;
        while( bet <= 0 || bet > manmoney)    // 檢測錯誤  不合規則重新輸入
        {
            if(bet > manmoney)
            {
                cout << "You don't have that much money, plese try again."
                     << endl << endl;
                cout << "How much do you want to play in this round? ";
                cin >> bet;
            }
            else if(bet <= 0)
            {
                 cout << "You can't input negative number or zero to your bet,please try again."
                      << endl << endl;
                 cout << "How much do you want to play in this round? ";
                 cin >> bet;
            }
        }

        compoint = dice();         // 使用dice函數
        manpoint = dice();

        cout << "Computer's dice point is: "<< compoint << endl;
        cout << "Your dice point is: " << manpoint << endl;

        if(compoint<manpoint)//輸了扣錢
        {
        commoney -= bet;
        manmoney += bet;
        cout << "Congratulation! You win. " << "You left " << manmoney << " dollars.";
        }
        else if(compoint>manpoint)//贏了加錢
        {
        commoney += bet;
        manmoney -= bet;
        cout << "Sorry, you lose. " << "You left " << manmoney << " dollars.";
        }
        else     //平手不變
        {
        cout << "Draw." << "You left " << manmoney << " dollars.";
        }
        cout << endl;



        if(manmoney<=0)     //中途結束
        {
            cout << endl;
            cout << "you don't have any money, you lose the game. ";
            commoney = 200;
            manmoney = 0;
            break;
        }
        else if(commoney<=0)
        {
            cout << endl;
            cout << "Computer is bankrupted, you win the game.";
            break;
        }

    }

    // 三局後  檢查剩餘金錢  判定勝負
    cout << "Computer remains " << commoney << " dollars." << endl;
    cout << "You remain s " << manmoney << " dallars." << endl;

    if(commoney>=manmoney)
    {
        cout << "You lose the game.";
    }
    else
    {
        cout << "You win the game.";
    }

    return 0;
}
