#include <iostream>
#include <time.h>
#include <cstdlib>
using std::cout;
using std::cin;
using std::endl;
int dice (int); // dice function prototype
int main()
{
    srand((unsigned)time(NULL)); // time for the random number
    int loop = 1;
    int money, counter = 100, counter2 = 100;
    cout << "Computer: I want to play a game. You have to make a choice.\n" << endl;
    while (loop <= 3) // 3 rounds for the game
    {
        int a, b;
        a = dice (a); // computer's dice number
        b = dice (b); // your dice number
        do
        {
            cout << "How much do you want to play in this round? ";
            cin >> money; // how much money to bet
            if (money <= 0)
                cout << "You can't input negative number or zero to your bet, please try again.\n" << endl;
            else if (money > counter)
                cout << "You don't have that much money, please try again.\n" << endl;
        }while (money <= 0 || money > counter); // ends until bet money is bigger than zero and smaller than counter
        cout << "Computer's dice point is: " << a << endl;
        cout << "Your dice point is: " << b << endl;
        if (a > b) // if computer's dice is bigger, you lose
            {
                cout << "Sorry, you lose. You left " << counter - money << " dollars\n" << endl;
                counter = counter - money; // you lose money
                counter2 = counter2 + money; // computer wins money
            }
        else if (a < b) // if computer's dice is smaller, you win
            {
                cout << "Congratulation! You win. You left " << counter + money << " dollars\n" << endl;
                counter = counter + money; // you win money
                counter2 = counter2 - money; // computer lose money
            }
        else // if both have the same dice, draw
            {
                cout << "Draw. You left " << counter << " dollars\n" << endl;
                counter = counter; // stay intact
                counter2 = counter2; // stay intact
            }
        if (counter == 0) // if you don't have money, you lose
        {
            cout << "Sorry, you lose. You left 0 dollars." << endl;
            return 0;
        }
        else if (counter2 <= 0) // if computer doesn't have money, computer lose
        {
            cout << "Computer is bankrupted, you win the game." << endl;
            return 0;
        }
        loop ++; // round + 1 til 3 rounds
    }
    cout << "Computer remains " << counter2 << " dollars." << endl; // demostrate how much money does the computer left
    cout << "You remain " << counter << " dollars." << endl; // demostrate how much money do you left
    if (counter > counter2) // if your left money is bigger than computer's, you win
        cout << "You win the game." << endl;
    else if (counter < counter2) // if your left money is smaller, you lose
        cout << "You lose the game." << endl;
    else if (counter == counter2) // if both have the same money, you lose
        cout << "Draw. You lose the game." << endl;
    return 0;
}
int dice(int x) // definition of dice function
{
    x = rand();
    x = x % 6 + 1;
    return x;
}
