#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int dice()//generate dice number function
{
    int point =rand()%6+1;
    return point;
}
int main()
{
    srand((unsigned)time(NULL));
    int comdice=dice();//generate comdice
    int youdice=dice();//generate youdice
    int betdollar,youdollar=100,comdollar=100;
    cout<<"Computer: I want to play a game. You have to make a choice."<<endl<<endl;

    for(int i=1;i<=3;i++)//play three times
    {
        do
        {
            cout <<"How much do you want to play in this round? ";
            cin >>betdollar;
            if(betdollar<=0)  //if betdollar<=0,print error message,try again
               cout<<"You can't input negative number or zero to your bet, please try again."<<endl<<endl;
            else if(betdollar>youdollar)//if betdollar>youdollar,print error message,try again
               cout<<"You don't have that much money, please try again."<<endl<<endl;
        }while(betdollar<=0 || betdollar>youdollar);


        cout<<"Computer's dice point is: "<<comdice<<endl; //print comdice
        cout<<"Your dice point is: "<<youdice<<endl;//print youdice
        if(youdice<comdice)
        {
            youdollar =youdollar-betdollar;//count youdollar
            comdollar =comdollar+betdollar;//count comdollar
            if(youdollar<0)//if youdollar <0,youdollar=0
                youdollar=0;
            cout<<"Sorry, you lose. You left "<<youdollar<<" dollars." <<endl;
            if(youdollar==0) //if youdollar=0,game is over
            {
                cout<<endl<<"You don't have any money, you lose the game."<<endl;
                return 0;
            }

        }
        else if(youdice>comdice)
        {
            youdollar =youdollar+betdollar;
            comdollar =comdollar-betdollar;
            if(youdollar>200)//if youdollar>200,youdollar=200
                youdollar=200;
            cout<<"Congratulation! You win. You left "<<youdollar<<" dollars."<<endl;
            if(comdollar<=0)//if comdollar<=0,game is over
            {
                cout<<endl<<"Computer is bankrupted, you win the game."<<endl;
                return 0;
            }
        }
        else if(youdice=comdice)
            cout<<"Draw. You left "<<youdollar<<" dollars."<<endl;
        cout<<endl;
    }
    if(comdollar<0)
       comdollar=0;
    else if(comdollar>200)
       comdollar=200;
    if(youdollar<0)
       youdollar=0;
    else if(youdollar>200)
       youdollar=200;
    //////print game result////
    cout<<endl<<"Computer remains "<<comdollar<<" dollars."<<endl;
    cout<<"You remain "<<youdollar<<" dollars."<<endl;
    if(youdollar<=comdollar)
        cout<<"You lose the game."<<endl;
    else if(youdollar>comdollar)
        cout<<"You win the game."<<endl;
    return 0;
}
