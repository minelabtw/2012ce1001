#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int dice() // function of dice
{
    int point = rand()%6 +1;
    return point;
}

int main()
{
    int round = 1 , yourdice , computerdice , bet , yourmoney = 100 , computermoney = 100;

    srand((unsigned)time(NULL));

    cout << "Computer: Iwant to play a game. You have to make a choice." << endl << endl;

    while(round <= 3)//play three rounds.
    {
        yourdice = dice() ;
        computerdice = dice() ;

        if(yourmoney*computermoney<=0)
        {
            if(yourmoney > computermoney)
            {
                cout << "Computer remain " << computermoney << " dollars." << endl;
                cout << "You remain " << yourmoney << " dolars." << endl;
                cout << "You win the game." << endl;
                return 0;
            }
            else
            {
                cout << "Computer remain " << computermoney << " dollars." << endl;
                cout << "You remain " << yourmoney << " dolars." << endl;
                cout << "You lose the game." << endl;
                return 0;
            }
        }

        cout << "How much do you want to play in this round? ";
        cin >> bet;

        if(bet > yourmoney)//check the input value is correct or not.
        {
            cout << "You don't have that much money." << endl;
        }
        else if(bet <= 0)
        {
            cout << "You can't input negative nimber or zero to your bet, please try again.";
        }
        else
        {
            cout << "Computer's dice point is: " << computerdice << endl;
            cout << "Your dice point is: " << yourdice << endl;

            if(yourdice < computerdice)
            {
                cout << "Sorry, you lose. ";
                yourmoney = yourmoney - bet;
                computermoney = computermoney + bet;
                if(yourmoney >200)//one can hold at most 200 dollars.
                {
                    yourmoney = 200 ;
                }
                if(computermoney <0)
                {
                    computermoney = 0 ;
                }
                cout << "You left " << yourmoney << " dollars." << endl << endl;
                round++;
            }
            else if(yourdice == computerdice)
            {
                cout << "Draw. You left " << yourmoney << " dollars." << endl << endl;
                round++;
            }
            else if(yourdice > computerdice)
            {
                cout << "Congratulation! You win. ";
                yourmoney = yourmoney + bet;
                computermoney = computermoney - bet;
                if(yourmoney >200)//one can hole atamost $ 200.
                {
                    yourmoney = 200 ;
                }
                if(computermoney <0)
                {
                    computermoney = 0 ;
                }
                cout << "You left " << yourmoney << " dollars." << endl << endl;
                round++;
            }
        }

        if(yourmoney < 0)
        {
            cout << "Sorry you are bankrupted, computer win the game";
            return 0;
        }
        else if(computermoney < 0)
        {
            cout << "Congratulation! You win.you left 200 dollars." << endl;
            cout << "Computer is bankrupted, you win the game";
            return 0;
        }

    }

    if(yourmoney >200)
    {
        yourmoney = 200 ;
    }
    if(computermoney <0)
    {
        computermoney = 0 ;
    }

    if(yourmoney > computermoney)
    {
        cout << "Computer remain " << computermoney << " dollars." << endl;
        cout << "You remain " << yourmoney << " dolars." << endl;
        cout << "You win the game." << endl;
    }
    else
    {
        cout << "Computer remain " << computermoney << " dollars." << endl;
        cout << "You remain " << yourmoney << " dolars." << endl;
        cout << "You lose the game." << endl;
    }
    return 0;
}
