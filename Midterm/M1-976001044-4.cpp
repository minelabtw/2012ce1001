#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int dice();
int main()
{
    int DicePlayer=0, DiceComputer=0, RunTimes=1;
    int ComputerMoney=100, PlayerMoney=100, GameMoney=100;

    srand((unsigned)time(NULL));

    cout << "Computer : I want to play a game. You have to make a choice." << endl << endl;

    while ( RunTimes <= 3 )
    {
        cout << "How much do you want to play in this round? ";
        cin >> GameMoney;
        while ( (GameMoney <= 0) || (GameMoney > PlayerMoney) )
        {
            if ( GameMoney <= 0 )
            {
                cout << "You can't ipnut negative number or zero to your bet, please try again." << endl << endl;
            }

            if ( GameMoney > PlayerMoney )
            {
                cout << "You don't have that much money, please try again." << endl << endl;
            }
            cout << "How much do you want to play in this round? ";
            cin >> GameMoney;
        }

        DiceComputer = dice();
        cout << "Computer's dice point is: " << DiceComputer << endl;
        DicePlayer = dice();
        cout << "Your dice point is: " << DicePlayer << endl;

        if ( DiceComputer < DicePlayer )
        {
            PlayerMoney = PlayerMoney + GameMoney;
            ComputerMoney = ComputerMoney - GameMoney;
            cout << "Congratulation! You win. ";
            cout << "You left " << PlayerMoney  << " dollars." << endl << endl;
        }
        else if ( DiceComputer > DicePlayer )
        {
            PlayerMoney = PlayerMoney - GameMoney;
            ComputerMoney = ComputerMoney + GameMoney;
            cout << "Sorry, you lose. ";
            cout << "You left " << PlayerMoney  << " dollars." << endl << endl;
        }
        else
        {
            cout << "Draw. ";
            cout << "You left " << PlayerMoney  << " dollars." << endl << endl;
        }
        RunTimes++;

        if ( (PlayerMoney <= 0) || (ComputerMoney <= 0) )
        {
            if ( PlayerMoney <= 0 )
            {
                cout << "You don't have any money, you lose the game." << endl;
            }

            if ( ComputerMoney <= 0 )
            {
                cout << "Computer is bankrupted, you win the game." << endl;
            }
            system("pause");
            return 0;
        }
    }

    if ( PlayerMoney < 0 )
    {
        PlayerMoney = 0;
    }
    else if ( PlayerMoney > 200 )
    {
        PlayerMoney = 200;
    }

    if ( ComputerMoney < 0 )
    {
        ComputerMoney = 0;
    }
    else if ( ComputerMoney > 200 )
    {
        ComputerMoney = 200;
    }


    cout << "Computer remains " << ComputerMoney << " dollars." << endl;
    cout << "You remains " << PlayerMoney << " dollars." << endl;

    if ( ComputerMoney > PlayerMoney )
    {
        cout << "You lose the game." << endl << endl;
    }
    else if ( ComputerMoney < PlayerMoney )
    {
        cout << "You win the game." << endl << endl;
    }
    else if ( ComputerMoney == PlayerMoney )
    {
        cout << "Draw. You lose the game." << endl << endl;
    }


    system("pause");
    return 0;
}

int dice()
{
    return rand() % 6 + 1;
}
