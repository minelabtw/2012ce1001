#include<iostream>
#include<cstdlib>
#include<ctime>
int dice();
using namespace std;
int main()
{
    cout<<"Computer: I want to play a game. You have to make a choice."<<'\n'<<endl;//一開始
    int pla=100,com=100,x;

    for(x=0;x<3;x++)//跑三次
    {
        srand((unsigned)time(NULL));//取亂數 必須把srand方在外面 因為如果放在裡面 因為執行時間太短 導致rand跑出來的東西一樣
        int c=dice();
        int p=dice(),mon;
        cout<<"How much do you want to play in this round? ";
        cin>>mon;
        if(mon<=0)//為判斷小於等於0
        {
            cout<<"You can't input negative number or zero to your bet, please try again."<<'\n'<<endl;
            x--;
            continue;
        }
        else if(mon>pla)//為判斷是否輸入大於籌碼
        {
            cout<<"You don't have that much money, please try again."<<'\n'<<endl;
            x--;
            continue;
        }
        cout<<"Computer's dice point is: "<<c<<endl;
        cout<<"Your dice point is: "<<p<<endl;

        if(c>p)//比骰子點數 電腦大於玩家
        {
            pla-=mon;//加錢減錢
            com+=mon;
            if(com>200)//無法收超過200籌碼  也無法變負數
            {
                pla=0;
                com=200;
            }
            cout<<"Sorry, you lose. You left "<<pla<<" dollars."<<'\n'<<endl;//輸出結果
            if(pla<=0)//判斷中途破產
            {
               cout<<"You don't have any money, you lose the game."<<endl;
               break;
            }
            else if(com<=0)//判斷中途破產
            {
                cout<<"Computer is bankrupted, you win the game."<<endl;
                break;
            }
        }
        else if(c<p)//當玩家點數大於電腦
        {
            pla+=mon;//加錢減錢
            com-=mon;
            if(pla>200)//無法收超過200籌碼  也無法變負數
            {
                pla=200;
                com=0;
            }
            cout<<"Congratulation! You win. You left "<<pla<<" dollars."<<'\n'<<endl;//輸出結果
            if(pla<=0)//判斷中途破產
            {
               cout<<"You don't have any money, you lose the game."<<endl;
               break;
            }
            else if(com<=0)//判斷中途破產
            {
                cout<<"Computer is bankrupted, you win the game."<<endl;
                break;
            }
        }
        else//平手狀況 因為無變化此局不用考慮破產問題
            cout<<"Dral. You left "<<pla<<" dollars."<<'\n'<<endl;

    }
    if(x==3)//當全部跑完 x會等於3 以下為判斷最終勝負
    {
        cout<<"Computer remains "<<com<<" dollars."<<endl;
        cout<<"You remain "<<pla<<" dollars."<<endl;
        if(com>pla)
            cout<<"You lose the game."<<endl;
        else if(com==pla)
            cout<<"Draw. You lose the game."<<endl;
        else
            cout<<"You win the game."<<endl;

    }


    return 0;
}

int dice()//亂數
{
    int point;
    point=rand()%6+1;
    return point;
}
