#include <iostream>

using namespace std;

int main()
{
    int n;
    bool firstline = true;  //is the first line?
    while(cin >> n)
    {
        int i;              // i is the layer in question
        int remain;
        int dir = 0;        // the direction of the loop
        for(i=1;i*(i+1)/2<n;i++);   // count the layer
        i--;                //the full layer above it
        if(i%2==0)
            dir = 1;// count to up
        else
            dir = 2;// count to down
        remain = (n-(i*(i+1)/2));   // count remain of layer

        /////////// handle the end of line ///////////
        if(firstline)
            firstline = false;
        else
            cout << endl;

        /////////// ouput //////////////////
        cout << "TERN " << n << " IS ";
        if(dir==1)
            cout << i+1+1-remain << "/" << remain; // reposition the answer
        else
            cout << remain << "/" << i+1+1-remain; // reposition the answer

    }
    return 0;
}
