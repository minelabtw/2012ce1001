#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    cout << "Comeputer: I want to play a game. You have to make a choice." << endl;//先輸出此字串
    cout << endl;//空一行
    srand((unsigned)time(NULL));//隨機選數字
    for(int i=1;i<=3;i++)//此迴圈運行3次
    {
        int a=rand(),b=rand(),c=a%6+1,d=b%6+1;//設定變數

        cout << "Computer's dice point is: " << c << endl;//電腦的骰子結果
        cout << "Yout dice point is: " << d << endl;//玩家的骰子結果

        if(c>d)//電腦贏的輸出結果
        {
            cout << "Sorry, you lose." << endl;
            cout << endl;
        }

        else if(c<d)//玩家贏的輸出結果
        {
            cout << "Congratulation! You win." << endl;
            cout << endl;
        }

        else if(c==d)//平手的輸出結果
        {
            cout << "Draw" << endl;
            cout << endl;
        }
    }

    return 0;
}

