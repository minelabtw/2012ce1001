#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int dice(int);

int main()
{
    srand((unsigned)time(NULL));
    int x,y,n=3;
    cout << "Computer: I want to play a game. You have to make a choice.\n"<<endl;
    do//使遊戲做3次
    {
        x=dice(x);//代表電腦的點數
        y=dice(y);//代表玩家的點數
        cout << "Computer's dice point is : "<<x<<endl;
        cout << "Your dice point is : "<<y<<endl;
        if(x>y)//輸的局面
            cout << "Sorry, you lose.\n\n";
        if(x<y)//贏的局面
            cout << "Congratulation! You win.\n\n";
        if(x==y)//平手的局面
            cout << "Draw\n\n";
        n--;
    }while(n>0);
}
int dice(int)//自訂函數
{
    int x=rand()%6+1;//使輸出的數為1到6
    return x;
}
