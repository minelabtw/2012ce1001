#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int dice()
{
    int a = rand()%6+1;
    return a;
}

int main()
{
    cout << "Computer: I want to play a game. You have to make a choice." << endl;
    srand((unsigned)time(NULL));
    int time = 1;
    int Computer = 100;
    int Player = 100;
    int money;

    while ( time <= 3)
    {
        int x = dice();
        int y = dice();

        cout << "\nHow much do you want to play in this round? ";
        cin >> money;
        cout << "Computer's dice point is:" << x << endl;
        cout << "Your dice number is :" << y << endl;

        if ( x > y )
        {
            Player = Player - money;
            Computer = Computer + money;
            cout << "Sorry, you lose. You left " << Player << "dollars." << endl;
        }
        else if ( y > x)
        {
            Player = Player + money;
            Computer = Computer - money;
            cout << "Congradulation!You win. You left " << Player << "dollars." << endl;
        }
        else
        {
            cout << "Draw. You left " << Player << "dollars." << endl;
        }

        time++;
    }
    cout << "Computer remain " << Computer << "dollars" << endl;
    cout << "Player remain " << Player << "dollars" << endl;

    if(Computer>Player)
        cout << "You lose the game.";
    else if(Computer<Player)
        cout << "You win the game.";
    else
        cout << "Draw.You lose the game.";
}


