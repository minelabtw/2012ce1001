#include <iostream>

using namespace std;

int main()
{
    int binary,total,mod; //宣告變數
    int count = 1;
    cout << "Enter a binary number : ";
    //假如輸入非數字則錯誤
    if ( cin >> binary == false )
    {
        cout << "Invalid input";
        return 0;
    }
    //假如輸入小於0則錯誤
    if ( binary < 0 )
    {
        cout << "Input cannot be negative";
        return 0;
    }
    /*若輸入不論計算與否都大於等於0則進入迴圈
      假如/10的餘數非0或1則非2進位
      餘數=輸入/10
      總值=原本+上餘數*count(預設1)
      之後count*2
      輸入的值/2
    */
    for (total =0; binary>=1 ;)
    {
        if ( binary % 10 != 1 && binary % 10 !=0)
        {
            cout << "Can't be binary input";
            return 0;
        }
    mod = binary % 10;
    total = total + mod * count;
    count = count * 2 ;
    binary = binary / 10;
    }
    cout << "Decimal is: " << total;//輸出總值
    return 0;
}
