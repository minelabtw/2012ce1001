#include <iostream>
#include <math.h>

using namespace std;

bool is_prime(int);

int main()
{
    int L, U;
    char check;

    ///////////////////////// input L //////////////////////////////
    cout << "Enter the lower bound : ";
    if(!(cin >> L))                                 // 錯誤檢查 非法字元為首的輸入
    {
        cout << "Invalid input.\n";
        return -1;
    }

    check = cin.get();                              //檢查輸入流是否剩餘非法字元

    if(check!='\n' && check!=0 && check!=' ')       //換行符和EOF和空白不算
    {
        cout << "Invalid input.\n";
        return -1;
    }
    cin.putback(check);                             //還有資料等待輸入 還原輸入流

    if(L<0)                                         //負數檢查
    {
        cout << "Input cannot be negative.\n";
        return -1;
    }

    ///////////////////////// input U ////////////////////////
    cout << "Enter the upper bound : ";
    if(!(cin >> U))                                 // 錯誤檢查 非法字元為首的輸入
    {
        cout << "Invalid input.\n";
        return -1;
    }

    check = cin.get();                              //檢查輸入流是否剩餘非法字元
    if(check!='\n' && check!=0 && check!=' ')       //換行符和EOF和空白不算
    {
        cout << "Invalid input.\n";
        return -1;
    }

    if(U<0)                                         //負數檢查
    {
        cout << "Input cannot be negative.\n";
        return -1;
    }
    if(U<L)                                         //檢查U和L大小順去序
    {
        cout << "Upper bound must be greater than lower bound.\n";
        return -1;
    }

    ////////////////////// End of input //////////////////////////////////
    for(int i=L; i<=U; i++)
        if(is_prime(i))                             //檢查區間中的每一個數
            cout << i << " ";

    return 0;
}

bool is_prime(int n)
{
    if(n==0 || n==1)                                //0 1 都不是質數
        return false;
    for(int i=2;i<=sqrt(n);i++)                     //sqrt(n)取根號n
        if(n%i==0)
            return false;                           //若能被整除 即不是質數 直接回傳
    return true;                                    //都不能被<=跟號n的數整除 即為質數
}
