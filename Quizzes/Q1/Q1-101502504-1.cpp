//101502504
#include <iostream>
#include <iomanip>

using namespace std;//program uses

int main()
{
    int a,b=1,c=0,decimal=0;//variable declaration
    cout << "Enter a binary number: ";//prompt user to data

    if ( cin >> a == false || a < 0 )//if a is not a number and a < 0
    {
        if ( a < 0 )//a < 0
            cout << "Input cannot be negative." << endl;
        else//a is not a number
            cout << "Invalid input." << endl;
        return -1;
    }//end if

    else
    {
        for (;a>0;)
        {
            if ( a % 10 > 1 )//if a is not a binary number
            {
                cout << "Can't be binary input." << endl;
                return -1;
            }//end if

            else//if a is a binary number
            {
                c = (a%10)*b;
                decimal += c;
                a = a/10;
                b = b*2;
            }
        }
            cout << "Decimal is: " << decimal << endl;//show the decimal
    }//end else
    return 0;
}
