#include <iostream>

using namespace std;

int main()
{
    int a,b,c,d;//設定變數
    c=a;
    d=1;
    cout << "Enter the lower bound : ";//輸出文字
    if (cin >> a==false || a <0) //如果a不是整數或小於零出現下列文字
    {
        if (a < 0)
            cout << "Input cannot be negative." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }
    cout << "Enter the upper bound : "; //輸出文字
    if (cin >> b==false || b <0) //如果b不是整數或小於零出現下列文字
    {
        if (b < 0)
            cout << "Input cannot be negative." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }
    if (a>b) //如果a>b出現下列文字
    {
        cout << "Upper bound must be greater than lower bound." << endl;
        return -1;
    }
    while (a<=b) //在a~b的範圍找出有幾個數字
    {
       a = a + 1;
       while (d<a) //判斷a~b範圍內有幾個是質數
       {
           d =d+1;
           if (a%d!=0)
            cout << d << " ";
       }

    }


    return 0;

}
