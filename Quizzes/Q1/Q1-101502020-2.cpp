//Q1-2 by J
#include <iostream>
#include <cmath>
using namespace std ;
int main()
{
    int l=0 , u=0 , cr1=0 , cr2=0  ;
    // 4 variables used in this program:l as lower bound , u as upper bound , cr as checker
    // cr1 to represent a number between u&l ; cr2 to check whether cr1 is a prime or not

    cout << "Enter the lower bound : " ;        //these are the check for lower bound
    if ( cin>>l==false )
    {
        cout << "Invalid input." ;
        return -1 ;
    }
    else if ( l<0 )
    {
        cout << "Input cannot be negative." ;
        return -1 ;
    }

    cout << "Enter the upper bound : " ;        //these are the check for upper bound
    if ( cin>>u==false )
    {
        cout << "Invalid input." ;
        return -1 ;
    }
    else if ( u<0 )
    {
        cout << "Input cannot be negative." ;
        return -1 ;
    }
    else if ( u<l )     //additional check for upper < lower
    {
        cout << "Upper bound must be greater than lower bound." ;
        return -1 ;
    }

    while ( l<=u )      //the way to check that a number is a prime or not : square root of the number and check the integers lower than it.
    {
        cr1=l ;
        cr2=pow(cr1,0.5) ;
        while ( cr2>1 )
        {
            if ( cr1%cr2!=0 )       //for checking that whether a number is a prime or not
                cr2-- ;
            else        //cause it's already have a number that can divide cr1 , we don't have to check anymore.
                break ;
        }
        if ( cr2==1 )
        {
            cout << cr1 << " " ;
        }
        l++ ;
    }

    return 0 ;

}
