#include <iostream>
#include <cmath>
using namespace std;

int main()
{

    int big,small;
    bool Prime=true;
    cout << "Enter the lower bound : ";
    cin >> small;
    if(small<0)
    {
        cout <<"Input cannot be negative."<<endl;   //如果輸入為負數,告知為Input cannot be negative.,並結束程式
        return -1;
    }
    else if(cin == false)
    {
        cout <<"Invalid input."<<endl;              //如果輸入為字元,告知為Invalid input.,並結束程式
        return -1;
    }
    cout << "Enter the upper bound : ";
    cin >> big;
    if (big<0)
    {
        cout <<"Input cannot be negative."<<endl;    //如果輸入為負數,告知為Input cannot be negative.,並結束程式
        return -1;
    }
    else if(cin == false)
    {
        cout <<"Invalid input."<<endl;               //如果輸入為字元,告知為Invalid input.,並結束程式
        return -1;
    }
    else if(small>big)
       /*如果輸入的small的值大於big的值,告知Upper bound must be greater than lower bound.
       並結束程式*/
    {
        cout <<"Upper bound must be greater than lower bound."<<endl;
        return -1;
    }
    for(;small<=big;small++)             //從small到big間找出質數並印出
    {
        for(int i=2;i<=sqrt(small);i++)
        {
            if(small%i==0)
            {
                Prime =false;
                break;
            }
        }
        if(Prime==true)
            cout << small<<" ";
        else if(Prime ==false)
            Prime =true;
    }

    return 0;
}
