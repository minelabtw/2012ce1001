#include<iostream>
#include<cmath>

using namespace std;

int main()
{
    int number1,number2,a=2,b=0,c=0;//設置變數
    cout << "Enter the lower bound : ";//第一行顯示字串
    cin >> number1;//第一個輸入變數
    cout << "Enter the upper bound : ";//第二行顯示字串
    cin >> number2;//第二個輸入變數

    if(number1>number2)//錯誤偵測，當輸入變數1大於輸入變數二時
        cout << "Upper bound nust be greater than lower bound.";//則顯示此字串

    if(number1<0)//錯誤偵測，當number1小於0時
        cout << "Input cannot be negative.";//則顯示此字串
    if(number2<0)//錯誤偵測，當number2小於0時
        cout << "Input cannot be negative.";//則顯示此字串

    if(cin==false)//錯誤偵測，輸入變數非數字時
        cout << "Invalid input.";//則顯示此字串

    while(number1<=number2)//迴圈停止條件為當number1小於number2時
    {
        while(a<number1)//此為判斷是否為質數之迴圈，停止條件為當a小於number1
        {
            b=number1%a;//b為輸入變數除以a之餘數
            a++;//a為從2開始往上加之除數

            if(b=0)//每當b等於0
                c++;//就累計一次c
        }

        if(c=0)//當c等於0，即可判斷此數為質數
            cout << number1 << " ";//這時可輸出此質數

        number1++;//number1繼續往上加繼續做判斷
    }

    return 0;
}
