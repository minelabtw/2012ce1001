#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int binary,x=0,y=0,a=0,b=1;//設定變數
    cout << "Enter a binary number : ";//第一行顯示字串
    cin >> binary;//輸入第一個變數

    if(cin==false)//如果輸入的變數非數字
        cout << "Invalid input." << endl;//則顯示此字串

    if(binary<0)//若輸入變數為負
        cout << "Input cannot be negative" << endl;//則顯示此字串

    if(binary>0)
    {
        while (binary=0)//迴圈限制條件為變數的位數除到=0
        {
            x=binary%10;//x為binary除以10之餘數
            binary==binary/10;//binary持續減少位數

            if(x>1)//若餘數x大於1
                y++;//累計變數y

            a+=(x*b);//a由餘數乘上b之總和，轉變為十進位
            b*=2;//b由1開始一直乘2，為二進位之計算方式
        }

        if(y>0)//當累計值y大於0，即判斷此數非二進位
            cout << "Cant't be binary input" << endl;//則顯示此字串

        if(y=0)//累計值y等於0，即判斷此數為二進位
            cout << "Decimal is : "<<a<<endl;//計算十進位完成，輸出此字串
    }
    return 0;
}
