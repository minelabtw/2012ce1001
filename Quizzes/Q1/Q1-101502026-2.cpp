#include <iostream>

using namespace std;

int main()
{
    int x,y,z,i=2;
    cout << "Enter the lower bound : ";
    while(cin >> x)
    {
        if(x<0)
        {
            cout << "Input cannot be negative." << endl;
            return 0;
        }
        else
        {
            cout << "Enter the upper bound : ";
            {
                while(cin >> y)
                {
                    if(y<0)
                    {
                        cout << "Input cannot be negative." << endl;
                        return 0;
                    }
                    else if(y<x)
                    {
                        cout << "Upper bound must be greater than lower bound." <<endl;
                        return 0;
                    }
                    else
                    {
                        for(x;x<=y;x++)
                        {
                            while(i<x)
                            {
                                z=x%i;
                                i++;
                                if(z==0)
                                cout << x << " ";
                            }
                        }
                    }
                }
            }
        }
    }
    cout << "Invalid input." << endl;
    return 0;
}
