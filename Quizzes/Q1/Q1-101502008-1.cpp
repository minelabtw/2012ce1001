#include <iostream>

using namespace std;

int main()
{
    int input_num=0;
    int count=1;
    int result=0;
    cout<<"Enter a binary number: ";
    cin>>input_num;
    if (input_num==0)//違法輸入
    {
        cout<<"Invalid input.";
        return 0;
    }
    if (input_num<0)//小於零違法
    {
        cout<<"Input cannot be nagative.";
        return 0;
    }
    for(;input_num>=1;input_num/=10)//以2進位算法
    {
        int n=input_num%10;
        if (n==1)
        {
            result+=count;

        }

        if (n>1)//不屬於2進位違法
        {
            cout<<"Can't be binary input.";
            return 0;
        }
        count*=2;
    }
    cout<<"Decimal is : "<<result<<"\n";//輸出
    return 0;
}
