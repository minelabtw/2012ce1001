#include<iostream>
using namespace std;
int main()
{
    int binary,remainer,i=1,total=0;
    cout<<"Enter a binary number: "<<endl;//輸入一個二進位數
    cin>>binary;
    if (binary==false)//若為字元
    {
        cout<<"Invalid input. "<<endl;
        return -1;
    }
    else if (binary<0)//若為負數
    {
        cout<<"Input can't be binary .";
        return -1;
    }
    else
    {
        while (binary>0)
        {
            remainer=binary%10;//求取各個位數
            if (remainer!=1 && remainer!=0)
            {
                cout<<"Can't be binary input .";
                return -1;
            }
            else{
                total=total+remainer*i;//加總
                i=i*2;
                binary=binary/10;
                }
        }
        cout<<"Decimal is : "<<total<<endl;//輸出十進位
        return 0;
    }

}
