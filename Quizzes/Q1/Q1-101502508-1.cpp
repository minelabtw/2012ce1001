//我的回圈內有問題，如果是二進位會跑出結果可是是無限迴圈，另外非二進位的則會輸出0，其他ok

#include<iostream>

using namespace std ;

int main()
{
    int number , sum = 0 , a = 1;

    cout << "Enter a binary number :" ;

    if ( cin >> number == false ) // 如果輸入的為英文字母
        {
            cout << "Invalid input." <<endl ;
            return -1 ;
        }

    if ( number < 0 ) //如果輸入為負數
        {
            cout << "Input cannot be negative." << endl ;
            return -1 ;
        }

    if ( number%10 > 1) //如果輸入為非二進位的整數，且尾數不為0或1
        {
            cout << "Can't be binary input." << endl ;
            return -1 ;
        }

    while ( number%10 == 0 || number%10 == 1 ) // 判別尾數有0有1的二進位或十進位整數
        {
            if ( number%10 > 1 ) //由非0和1組成的整數
                {
                    cout << "Can't be binary input." << endl ;
                    return -1 ;
                }

            sum = sum + number%10 * a ;
            number = number / 10 ;
            a = a * 2 ;

            cout << "Decimal is :" << sum << endl ;
        }

    return 0 ;

}
