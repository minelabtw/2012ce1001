#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int num1;
    int i=0;
    int sum=0;

    cout << "Enter a binary number: ";

    if ( cin >> num1 == false )
    {
        cout << "Invalid input." << endl;
        return 0;
    }//if end

    else if ( num1 < 0 )
    {
        cout << "Input cannot be negative." << endl;
        return 0;
    }//else if end

    else if ( num1 % 10 > 1 )
    {
        cout << "Can't be binary input." << endl;
        return 0;
    }//else if end

    else if ( num1 % 10 == 1 || num1 % 10 == 0 )
    {
        while ( num1 > 0 )
        {
            i=0;
            sum += num1 * 2^i;


        }//while end

        //while loop can't figure out the right answer....

        cout << sum << endl;
    }//else end
    return 0;
}//main end
