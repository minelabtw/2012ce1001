#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int binary, base = 1, split, decimal = 0;
    cout << "Enter a binary number: ";
    cin >> binary;
    if (( cin == false ) || ( binary < 0 )) // input a number making a decision
        {
            if ( cin == false ) // if the input number is a character
                cout << "Invalid input." << endl; // appears this message
            else // if the input number is negative
                cout << "Input cannot be negative." << endl; // appears this message
            return -1;
        }
    if ( binary > 0 ) // if the input number is a number
    {
        while ( binary >= 0 ) // while the number is greater than 0
        {
            split = binary % 10; // take the first number of the input number
            if ( split != 1 && split != 0 ) // if the taken numer isn't 1 or 0
            {
                cout << "Can't be binary input." << endl; // appears this message
                return -1;
            }
            binary = binary / 10; // take the second number
            split = split * base; // multiply the taken number by the base
            base = base * 2; // the base is binary so multiply by 2
            decimal = decimal + split; // add the number that is multiplied to the decimals
            if ( binary == 0 ) // if there is no more number to take
                break; // ends
        }
        cout << "Decimal is: " << decimal << endl; // appears the all added number
    }
    return 0;
}
