#include <iostream>

using namespace std;

int main()
{
    int A ;
    int B=1 ;
    int C ;
    int sum=0 ;

    cout << "Enter a binary number: " ;

    if (cin >> A==false ) //如果A不是數字
    {
        cout << "Invalid input." << endl ;
        return 0 ;
    }

    if (A<0)
    {
        cout << "Input cannot be negative." << endl ;
        return 0 ;
    }

    while (A>=0)
    {
        C = A % 10 ; // C是A/10的餘數

        if (C==1)
        {
            sum = sum + B ;
        }

        if (C!=0 && C!=1) // 如果C不等於0且不等於1
        {
            cout << "Can't be binary input." << endl ;
            return 0 ;
        }

        B=B*2;
        A=A/10;

    }
    cout << "Decimal is: " << sum << endl ; // 不知道為什麼都不能顯示，但如果把第45行移到第43行的話，就可以顯示出sum，只是一直迴圈的cout


    return 0 ;
}
