#include<iostream>

using namespace std;

int main ()
{
    int binary, b=0 ,c=1 ,decimal=0 ; //各類宣告

    cout <<"Enter a binary number: ";
    cin >> binary ;

    if ( binary < 0) // 如果ninary<0 執行下列動作
        cout << "Input can't be negative."<<endl;//輸出結果

    while ( binary>=0 ) //如果binary>=0 ,進入while迴圈
    {

        b = binary % 10 ; // b等於binary除以10的餘數

        //問題:輸入的數是二進位也會進入這個if
        if ( b != 0 || b != 1 )// 如果b除以10不等於0或1,進行以下動作
        {
            cout<< "Can't be binary input."<<endl;//輸出結果
            return -1 ;//結束程式
        }

        else// 如果b等於0或1
        {
            c = c * 2; // c等於c乘以2
            decimal = b * c ; // decimal等於b乘以c
            decimal += decimal ; // decimal加decimal等於decimal
            cout << "Decimal is: "<< decimal << endl ;//輸出結果
        }


    }

    return 0 ;//結束程式
}
