#include <iostream>
using namespace std;

int main()
{
    int binary,decimal = 0,a = 1,test = 0,save;

    cout << "Enter a binary number: ";
    cin >> binary;
    save = binary;

    if ( cin == false ) //偵測字元錯誤
    {
        cout << "Invalid input." << endl;
        return -1;
    }
    if ( binary < 0 ) //偵測負數之錯誤
        {
            cout << "Input cannot be negative." << endl;
            return -1;
        }
    else //若無以上錯誤則進行下列判斷
    {
        while ( binary % 10 != 1 && binary % 10 != 0 ) //判斷輸入使否為二進位數
        {
            binary = binary / 10;
            test = 1;
            break;
        }
        binary = save;
        if ( test == 1 ) //test=1表示輸入非二進位數
            cout << "Can't be binary input." << endl;

        else
        {
            while ( binary != 0 ) //將輸入的二進位數轉換成十進位數
            {
                decimal = decimal + ( binary % 10 ) * a;
                a = a * 2;
                binary = binary / 10;
            }
            cout << "Decimal is: " << decimal << endl;
        }
    }

    return 0;
}
