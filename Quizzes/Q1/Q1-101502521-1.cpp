#include <iostream>

using namespace std;

int solve(int);

int main()
{
    int n;

    cout << "Enter a binary number: ";

    if((cin >> n)==false)               // 錯誤檢查 非法字元為首的輸入
    {
        cout << "Invalid input.\n";
        return -1;
    }

    char check = cin.get();             //檢查輸入流是否剩餘非法字元
    if(check!='\n' && check!=0 && check!=' ')         //換行符和EOF和空白不算
    {
        cout << "Invalid input.\n";
        return -1;
    }

    if(n<0)
    {
        cout << "Input cannot be negative.\n";
        return -1;
    }

    int ans = solve(n);                 //轉換

    if(ans==-1)                         //判斷錯誤
    {
        cout << "Can't be binary input.\n";
        return -1;
    }
    else
        cout << "Decimal is: " << ans << endl;

    return 0;
}

int solve(int n)
{
    int now = 1;                        //2的冪次
    int sum = 0;

    while(n>0)
    {
        int t = (n%10);
        if(t!=1 && t!=0)                //非1或0 不是二進位數
            return -1;                  //-1表示錯誤
        sum += now*t;
        n /= 10;
        now *= 2;
    }

    return sum;
}
