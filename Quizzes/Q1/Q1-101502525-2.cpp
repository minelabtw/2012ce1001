#include<iostream>
#include<stdlib.h>
#include<limits>
#include<math.h>
using namespace std;


bool input(int &in);


int main()
{
    //initial
    int L=0,U=0;
    bool prime;


    //input
    cout<<"Enter the lower bound : ";
    if(!input(L))
        return -1;
    cout<<"Enter the upper bound : ";
    if(!input(U))
        return -1;


    //exception
    if(L>U)
    {
        cout<<"Upper bound must be greater than lower bound.\n";
        return -1;
    }
    if(L<2)//resist 0 and 1
        L=2;
    //exception above


    //compute
    for(;L<=U;L++)
    {
        prime=true;
        for(int i=2;i<=sqrt(L);i++)
            if(L%i==0)//not prime
            {
                prime=false;
                break;
            }
        //output prime number
        if(prime)
            cout<<L<<'\t';
    }


    //end
    cout<<endl;
    system("pause");
    return 0;

}




bool input(int &in)
{
    cin>>in;

    cin.clear();//reset cin
    cin.ignore(numeric_limits<streamsize>::max(),'\n');//clear cin buffer

    if(cin.gcount()!=1)//not number
    {
        cout<<"Invalid input.\n";
        return false;
    }
    if(in<0)//negative number
    {
        cout<<"Input cannot be negative.\n";
        return false;
    }

    //no error
    return true;
}
