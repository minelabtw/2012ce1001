#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    int a,b;
    cout<<"Enter the lower bound : ";
    if(cin>>a==false||a<0)//判斷輸入是否為負數或非數字字元
    {
        if(a<0)
        {
            cout<<"Input cannot be negative."<<endl;
            return 0;
        }
        else
        {
            cout<<"Invalid input."<<endl;
            return 0;
        }
    }
    cout<<"Enter the upper bound : ";
    if(cin>>b==false||b<0)//判斷輸入是否為負數或非數字字元
    {
        if(b<0)
        {
            cout<<"Input can't be negative."<<endl;
            return 0;
        }
        else
        {
            cout<<"Invalid input."<<endl;
            return 0;
        }
    }
    if(a>b)//當後面小於前面則錯誤
    {
        cout<<"Upper bound must be greater than lower bound."<<endl;
        return 0;
    }
    int x;
    for(x=a;x<=b;x++)//從輸入的第一個數跑到第二個輸入的數
    {
        if(x==1)//1另外判斷
            continue;
        int out=0;
        for(int y=2;y<=sqrt((double)x);y++)//能夠整除就是非質數
            if(x%y==0)
            {
                out=1;
                break;
            }
        if(out==0)//判斷後為質數則輸出其值
            cout<<x<<" ";
    }
    return 0;
}
