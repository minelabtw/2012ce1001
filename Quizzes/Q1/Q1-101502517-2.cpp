#include<iostream>
using namespace std;
int main()
{
    int a,b,d;
    cout<<"Enter the lower bound : ";
    if((cin>>a==false)||(a<0))//若輸入為負或字串，
    {
        if(a<0)//若為負
            cout<<"Input cannot be negative."<<endl;
        else//若為字串
            cout<<"Invalid input."<<endl;
        return -1;
    }
    if(a==0)//判定a是否為0，0非正整數，不合題意
    {
        cout <<"Input cannot be 0."<<endl;
        return -1;
    }
    cout<<"Enter the upper bound : ";
    if((cin>>b==false)||(b<0))//若輸入為負或字串，
    {
        if(b<0)//若為負
            cout<<"Input cannot be negative."<<endl;
        else//若為字串
            cout<<"Invalid input."<<endl;
        return -1;
    }
    if(b==0)//判定b是否為0，0非正整數，不合題意
    {
        cout <<"Input cannot be 0."<<endl;
        return -1;
    }
    if(a>b)//若a大於b
    {
        cout<<"Upper bound must be greater than lower bound."<<endl;
        return -1;
    }
    while(a<=b)
    {
        d=0;//把判斷數d歸零
        for(int c=2;c<a;c++)
        {
            if(a%c==0)//若餘數為0，非質數
                d=a;//非質數則讓判斷數d不為零
        }
        if(d==0)//若d=0，必為質數
        {
            if(a==1)//避開1，因為1非質數
            {}
            else
                cout <<a<<" ";//輸出a
        }
        a++;
    }
    cout<<endl;
    return 0;
}
