# include <iostream>
# include <iomanip>
using namespace std;

int main()
{
    int binary,c,sum;//c為2的次方的值
    sum=0,c=1;
    cout << "Enter a binary number: ";
    if((cin>>binary==false)||(binary<0))//排除字元及負數
       {
           if(binary<0)
            {
                cout << "Input cannot be negative.";
                return 0;
            }
            else
            {
                cout << "Invalid input.";
                return 0;
            }
       }
    while(binary>0)
    {
        if((binary%10)>=2)//排除非2進位
        {
            cout << "Can't be binary input";
            return 0;
        }

        sum=sum+(binary%10)*c;
        c=c*2;
        binary=binary/10;

    }

    cout << "Decimal is: " << sum << endl;
    return 0;

}
