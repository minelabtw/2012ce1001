//輸出在L~U之間所有質數
#include <iostream>

using namespace std;

int main()
{
    int L,U,i,j;//i為L~U之間之某數 j為L~U之間每一個數 用來驗證i是否為質數

    cout << "Enter the lower bound :";
    cin >> L;//輸入下界
    cout << "Enter the upper bound :";
    cin >> U;//輸入上界

    for( i = L ; i <= U ; i++)//i必須在L~U之間
    {
        /*
        if(j = 2 ; j <= U ; j++)//j從2開始當i的除數
        {
            i%j=0;
            return 1;//若可整除 則結束程式

            }//if end*/

            if(i%j!=0) cout << i + " ";//若為質數則輸出

    }//for end

  return 0;

}//main end


