#include<iostream>
using namespace std;
int main()
{
    int input,con=1,sum=0;//宣告變數
    cout<<"Entrer a binary number : ";
    if (cin>>input==false)//若輸入為字元之錯誤輸出
    {
        cout<<"Invalid input."<<endl;
        return -1;
    }
    else if (input<0)//若輸入為負數之錯誤輸出
    {
        cout<<"Input cannot be negative."<<endl;
        return -1;
    }
    else
    {
        while(input!=0)
        {
            if(input%10>1)//若輸入為非0或1之錯誤輸出
            {
                cout<<"Can't be binary input"<<endl;
                return -1;
            }

            sum=sum+(input%10)*con;//取其末位數做運算
            input=input/10;//將末位數刪除
            con*=2;//將計數器*2
        }
    }
    cout<<"Decimal is : "<<sum<<endl;//輸出結果
    return 0;
}
