#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int binary;
    cout <<"Enter an integer: ";
    cin >> binary;

    if(cin == false || binary <0)
    {
        if(binary<0)               //如果輸入為負數,告知為Input cannot be negative.,並結束程式
        {
            cout << "Input cannot be negative."<<endl;
            return -1;
        }
        else                       //如果輸入為字元,告知為Invalid input.,並結束程式
        {
            cout <<"Invalid input."<<endl;
            return -1;
        }
    }

    int y=0,arybin[32],base=1,decimal=0;
    while(binary>0)
    {
        arybin[y]=binary%10;        /*逐項檢查並分開儲存,因為是二進位,只有0或是1的可能,
                                      告知為Can't be binary input.並結束程式*/
        if(arybin[y]!=0 && arybin[y]!=1)
        {
            cout <<"Can't be binary input."<<endl;
            return -1;
        }
        y=y+1;
        binary=binary/10;
    }
    for(int i=0;i<=y-1;i++)         //將二進位換成十進位
    {
        decimal=decimal+arybin[i]*base;
        base=base*2;
    }
    cout << "Decimal is: " <<decimal<< endl;
    return 0;
}
