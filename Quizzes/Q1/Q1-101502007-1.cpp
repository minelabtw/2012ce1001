#include <iostream>

using namespace std;

int main()
{
    int num,sum=0,n=1;
    cout <<"Enter a binary number ";
    cin >>num;
    while (num>0)
    {
        sum=sum+num%10*n;
        n=n*2;
        num=num/10;
    }
    cout <<"Decimal is: "<<sum;
    return 0;
}
