#include <iostream>
#include <math.h>

using namespace std ;

main ()
{
    int LB , UB ;
    double checker;

    cout << "Enter the lower bound : " ;
    cin >> LB ;
    cout << "Enter the upper bound : " ;
    cin >> UB ;
    checker = LB ;

    if(LB < 0 || UB < 0) //check the invalid input
    {
        cout << "imput cannot be negative.";
        return 0 ;
    }
    else if(LB > UB)
    {
        cout << "Upper bound must be greater than lower bound.";
        return 0 ;
    }
    else
    {
            int i = 2 , j = 0;
            while(LB <= UB)
            {
                checker = LB ;
                i=2;
                j=0;
                while(i <= sqrt(checker))//check each number is prime or not
                {
                    if(LB%i==0)
                    {
                        j++;//if j == 0, means LB is prime.
                        i++;
                    }
                    else
                    {
                        i++;
                    }
                }
                if(j == 0 && LB >= 2)
                {
                    cout << LB << " ";
                }
                LB++;
            }

        return 0;
    }
}
