//將二進為轉換為十進位
#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int binary;
    int sum = 0;//轉換為十進位之數
    int i;//2的次方數

    cout << "Enter a binary number: ";
    cin >> binary;//使用者輸入二進位數

    if(binary < 0)
    {
        cout << "Input cannot be negative." << endl;//輸入不可為負數
        return 1;//結束程式

    }//if end

    /*這段為偵錯 但會使程式錯誤 所以隱藏
    else if(cin >> binary == false)
    {
        cout << " Invalid input. "<< endl;
        return 1;//結束程式
    }

    else( binary % 10 != 0 && binary % 10 != 1 )
    {
        cout << " Can't be binary input. "<< endl;
        return 1;//結束程式
    }*/

    for( i=0 ; binary != 0 ; i++)
    {
      sum = (binary % 10)*pow(2,i);
      sum += sum;
      binary = binary/10;

    }//for end

    cout << "Decimal is: " << sum << endl;//輸出換算後之值

    return 0;

}//main end
