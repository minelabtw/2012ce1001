#include<iostream>
#include<cmath>
using namespace std;
int check(int n)//製作檢查質數之函數，若為質數則回傳1，否則回傳0
{
    int i,flag=1;//flag(旗幟)預設為1
    if (n==1)
        flag=0;//1為非質數
    for(i=2;i<=sqrt(float(n));i++)
    {
        if(n%i==0)
            flag=0;//若有非質數者，旗幟變為0
    }
    return flag;
}
int main()
{
    int low,upp;
    cout<<"Enter the lower bound : ";
    if((cin>> low)==false || low<0)//輸入lower bound
    {
        if(low<0)//若為負數之錯誤輸出
            cout<<"Input cannot be negative."<<endl;
        else
            cout<<"Invalid input."<<endl;//若為字元之錯誤輸出
        return -1;
    }
    cout<<"Enter the upper bound : ";
    if((cin>> upp)==false || upp<0)//輸入upper bound
    {
        if(upp<0)//若為負數之錯誤輸出
            cout<<"Input cannot be negative."<<endl;
        else
            cout<<"Invalid input."<<endl;//若為字元之錯誤輸出
        return -1;
    }
    if(low>upp)
    {
        cout<<"Upper bound must be greater than lower bound."<<endl;
        return -1;//upper bound<lower bound 之錯誤輸出
    }
    for(int i=low;i<=upp;i++)//設定整數 i 介於lower bound 及upper bound 之間
    {
        if(check(i)==1)//檢查 i 是否為質數
            cout<<i<<" ";//輸出結果
    }
    return 0;
}
