#include <iostream>
using namespace std;
int main()
{
    int a,b,c,d=1,e=0;//宣告變數
    cout <<"Enter a binary number: ";
    if(cin>>a==false||a<0)//當a的型態或是a小於0時，輸出錯誤訊息
    {
        if(a<0)//如果a小於0，輸出nput cannot be negative.
            cout<<"Input cannot be negative.";
        else//否則輸出Invalid input
            cout<<"Invalid input.";
        return -1;//回傳
    }
    b=a;
    for( ;a>0;a=a/10)//當a大於0時，進入迴圈
    {
        if(a%10>1)//如果a除10之餘數大於1，輸出錯誤訊息Can't be binary input.
        {
            cout<<"Can't be binary input.";
            return -1;//回傳
        }
    }
    for(;b>0;b=b/10)//當b大於0時，進入迴圈
    {
        c=b%10*d;//將b除10之餘數*d,d=1
        d=2*d;//每次將d*2
        e=e+c;//將算出來之值存在e裡頭
    }
    cout << "Decimal is: "<<e<<endl;//將結果e印出
    return 0;//回傳
}
