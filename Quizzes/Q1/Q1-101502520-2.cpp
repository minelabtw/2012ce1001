#include <iostream>

using namespace std;

int main()
{
    int lower,upper;

    cout<<"Enter the lower bound : ";
    if(cin>>lower==false)//首先檢查是否輸入數字
        cout<<"Invalid input.";
    else if(lower<0)//在檢查是否是負數
        cout<<"Input cannot be negative.";
    else//第一個輸入檢查完才可以輸入第二個，並做跟第一個一樣的檢查
    {
        cout<<"Enter the upper bound : ";
        if(cin>>upper==false)
            cout<<"Invalid input.";
        else if(upper<0)
            cout<<"Input cannot be negative.";
        else//兩個都個別符合輸入規則後才檢查大小問題
            if(lower>upper)
                cout<<"Upper bound must be greater then lower bound.";
            else//輸入都沒有問題之後開始逐一做質數檢查
                for(int i=lower;i<=upper;i++)
                {
                    bool flag1=true;//宣告一個旗子來標示是否為質數
                    if(i==1)
                        flag1=false;
                    for(int j=2;j<i;j++)//從2開始，一個一個看看可不可以整除
                            if(i%j==0)
                            {
                                flag1=false;
                                break;//如果發現有東西可以整除他就標記為不是質數並跳出迴圈
                            }
                    if(flag1)//確定是質數就輸出
                        cout<<i<<" ";//
                }
    }
    return 0;
}
