#include<iostream>
#include<cmath> //Enable sqrt
using namespace std;
int main()
{
    int a,b,i,j; //"a" is lower bound, "b" is upper bound
    cout<<"Enter the lower bound : ";
    cin>>a;
    if (a<0)
    {
        cout<<"Input cannot be negative";
        return 0;
    }
    cout<<"Enter the upper bound : ";
    cin>>b;
    if (b<0)
    {
        cout<<"Input cannot be negative";
        return 0;
    }
    else if (a>b)
    {
        cout<<"Upper bound must be greater than lower bound.";
        return 0;
    }
    //Tell the input is valid or not
    else
    {
        for(i=a;i<=b;i++)
        {
            for(j=2;j<sqrt(i);j++)
            {
                if(i%j==0)
                break;
                else
                cout<<i<<" ";//@TA:Can't calculate correctly, don't know the equation
            }
        }
    }
    return 0;
}
