#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int x,y,z=0,i=0; //declare four variables,and "z" and "i" starts from 0

    cout << "Enter a binary number: ";
    while(cin>>x) //set a loop,of which the condition to enter is entering an integer
    {
        if(x<0)
        {
            cout << "Input cannot be negative." <<endl; //if the number entered is smaller than 0,then print out an error message
            return 0;
        }
        else
        {
            while(x>=1)
            {
                y=x%10;
                if(y==0)
                {
                    z=z+y*pow(2,i);//this is the formula to convert the binary number entered into a decimal number
                    x=x/10;
                    i++;
                }
                else if(y==1)
                {
                    z=z+y*pow(2,i);//this is the formula to convert the binary number entered into a decimal number
                    x=x/10;
                    i++;
                }
                else
                {
                    cout << "Can't be binary input.";
                    return 0;
                }
            }
            cout << "Decimal is: " << z << endl;
            return 0;
        }
    }
    cout << "Invalid input.";
    return 0;
}
