#include <iostream>
#include <math.h>
using namespace std;
int main()
{
    int L , U , c;//c用來判斷某個數字是否為質數

    cout << "Enter the lower bound : ";
    if(cin >> L == false)//判斷輸入是否為字元,是的話則輸出錯誤訊息和結束程式
    {
        cout << "Invalid input." << endl;
        return -1;
    }
    else if(L < 0)//判斷輸入是否為負數,是的話則輸出錯誤訊息和結束程式
    {
        cout << "Input cannot be negative." << endl;
        return -1;
    }

    cout << "Enter the upper bound : ";
    if(cin >> U == false)//判斷輸入是否為字元,是的話則輸出錯誤訊息和結束程式
    {
        cout << "Invalid input." << endl;
        return -1;
    }
    else if(U < 0)//判斷輸入是否為負數,是的話則輸出錯誤訊息和結束程式
    {
        cout << "Input cannot be negative." << endl;
        return -1;
    }

    if(L > U)//判斷L是否大於U
        cout << "Upper bound must be greater than lower bound." << endl;
    else
    {
        for(int i = L ; i <= U ; i++)
        {
            c = 1;//假設i為質數,則c等於1
            for(int j = 2 ; j <= sqrt(double(i)) ; j++)//判斷質數的方法:從2到i的平方根中,如果沒有數字能整除i的話,則i是質數
            {
                if(i % j == 0)//判斷i是否能被j整除
                {
                    c = 0;//i不是質數,則c等於0,並跳出迴圈
                    break;
                }
            }
            if(c == 1 && i != 1)//若i不是1且c為1的話,則i為質數,並輸出i
                cout << i << " ";
        }
        cout << endl;
    }
    return 0;
}
