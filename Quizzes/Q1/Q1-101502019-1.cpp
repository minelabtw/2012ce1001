#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int num,n=0,sum=0;

    cout << "Enter a binary number: ";
    while (cin >> num)//to divide invalid input
    {
        if(num >= 0)//to divide negative number
        {
            if (num%10 <= 1)//to divide binary input
            {
               while(num >= 1)//to transfer the numbers
                {
                    sum = sum + num%10 * pow(2,n);
                    num = num/10;
                    n++;
                }
                cout << "Decimal is: " << sum;
                return 0;
            }
            else
            cout << "Can't be binary input.";
            return 0;
        }
        else
        cout <<"Input can't be negative.";
        return 0;
    }
    cout << "Invalid input.";
    return 0;
}
