#include <iostream>
using namespace std;

int main()
{
    int lb,ub,a;
    cout << "Enter the lower bound : ";
    cin >> lb;
    cout << "Enter the upper bound : ";
    cin >> ub;

    while ( lb <= ub )
    {
        for ( a = 2 ; a < lb ; a++ )
        {
            if ( lb % a == 0 )
                cout << lb << " ";
            break;
        }
        lb = lb++;
    }

    return 0;
}
