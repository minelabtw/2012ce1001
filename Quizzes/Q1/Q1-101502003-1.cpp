#include <iostream>

using namespace std;//下面可以省略std::

int main()
{
    int a,b,c,d; //設定變數,a是輸入值,b是總和,c是2的倍數,d是a/10的餘數
    c=1; //設一開始c=2的零次方=1
    b=0;//總和一開始為零
    cout << "Enter a binary number : "; // 輸入文字
    if (cin>>a == false || a<0) //判斷輸入是否為整數或小於零
    {
        if (a<0) //小於零的話輸出下面文字
            cout << "Input cannot be negative." << endl;
        else //字元輸入的話輸出下面文字
            cout << "Invalid input." << endl;
        return -1;
    }

    while (a>0) //當a>0時,執行以下
    {
        d = a%10; //先算出餘數
        if (d==1) //如果餘數等於1,就乘以2的n次方
        {
            b = b + d * c;
            c = c *2;
            a = (a-1)/10; //a-1後繼續算下一個位數的餘數

        }
        else if (d==0) //如果餘數等於零,a直接除以10算下一個數
        {
            a = a / 10;
            c = c*2;
        }

        else //餘數不為零或一時,輸出下面文字
        {
            cout << "Can't be binary input." << endl;
            return -1;
        }

    }

    cout << "Decimal is : " << b << endl; //輸出總和
    return 0;
}
