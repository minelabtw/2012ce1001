#include <iostream>
using namespace std;

// 程式由此開始
int main()
{
    int a,b,buffer; //假設變數
    int c = 2;

    cout << "Enter the lower bound : "; // 輸出訊息

    if ( cin >> a == false || a < 0 ) // 如果a不是整數或是a小於0，則顯示錯誤訊息，結束並回報
    {
        if ( a < 0 )
            cout << "Input cannot be nagative .";
        else
            cout << "Invalid input .";
            return -1;
    }

    cout << "Enter the upper bound : "; // 輸出訊息
    if ( cin >> b == false || b < 0 ) // 如果b不是整數或是b小於0，則顯示錯誤訊息，結束並回報
    {
        if ( b < 0 )
            cout << "Input cannot be nagative .";
        else
            cout << "Invalid input .";
            return -1;
    }

    while ( a > b ) // 當a大於b的時候，顯示錯誤訊息，結束並回報
    {
        cout << "Upper bound must be greater than lower bound .";
        return -1;
    }

    for ( ; a <= b; a++ ) // 如果a小於等於b且a遞增時，進入迴圈
    {
        for ( ; c <= b; c++ ) // 如果c小於等於b且c遞增時，進入迴圈
            if ( a > c && a % c == 0 )
                buffer = c;
            else
                cout << c << " ";      // 無法將非質數的數字剃除
    }
    return 0; // 結束並回報
}
