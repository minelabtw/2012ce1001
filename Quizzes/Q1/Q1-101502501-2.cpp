//不知道怎麼顯示出L和U之間的質數
//判斷時會顯示出不對的文字

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int L,U,number;//宣告各數及其值

    cout << "Enter the lower bound : ";//輸出文字
    cin >> L;

    cout << "Enter the upper bound : ";//輸出文字
    cin >> U;

    if ( L>U )//判斷L是否大於U
        cout << "Upper bound must be greater than lower bound."<< endl;

    else if ( L<0 )//判斷L是否小於0
            cout << "Input cannot be negative." << endl;
    else if ( U<0 )//判斷U是否小於0
            cout << "Input cannot be negative." << endl;

    else if ( cin >> L == false || U == false )
            cout << "Invalid input." << endl;

    while ( number > sqrt(double(L)) && number < sqrt(double(U)) )
    {
        cout << number << " " << endl;
    }

    return 0;

}
