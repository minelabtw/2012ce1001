#include <iostream>
using namespace std;

int main()
{
    int Binary, Decimal = 0;
    cout << "Enter a binary number: ";

    if( cin >> Binary == false || Binary < 0 )
    {
        if( Binary < 0 )
            cout << "Input cannot be negative." << endl;
        else
            cout << "Invalid input." << endl;
    }

    if ( Binary > 0 )
    {
        for( int i = 1 ; i > 0 ; i *= 2 )
        {
            Decimal += Binary % 10 * i;
            Binary = Binary / 10;
        }
        cout << "Decimal is: " << Decimal << endl;
    }
    else
        cout << "Can't be binary input." << endl;
    return 0;
}


