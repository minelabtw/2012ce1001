#include <iostream>
using namespace std;

// 程式由此開始執行
int main()
{
    int a,c; // 假設變數
    int d = 1;
    int sum = 0;

    cout << "Enter a binary number: "; // 輸出訊息

    if ( cin >> a == false || a < 0 )  // 如果a不為整數或是a小於0，則輸出錯誤訊息，結束並回報
    {
        if ( a < 0 )
            cout << "Input cannot be negative .";
        else
            cout << "Invalid input .";
            return -1;
    }

    int b = a;
    for ( ; a > 0; a = a / 10 ) // 如果a大於0且a等於a除於10的話，進入迴圈
    {
        if ( a % 10 > 1 ) // 如果a除於10的餘數大於1，輸出錯誤訊息，結束並回報
        {
            cout << "Can't be binary input .";
            return -1;
        }

    }

    for ( ; b > 0; b = b / 10 ) // 如果符合條件：b大於0且b等於b除於10的話，進入迴圈
    {
        c = b % 10 * d; // 運算公式
        d = d * 2;
        sum = sum + c;
    }

    cout << "Decimal is : " << sum << endl; // 輸出運算結果
    return 0; // 結束並回報
}
