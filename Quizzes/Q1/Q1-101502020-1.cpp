//Q1-1 by J
#include <iostream>
#include <cmath>
using namespace std ;
int main()
{
    int inp=0 , ctr=0 , rst=0 ;
    //3 variables used in this programs : inp as input, ctr as counter(for loop) , rst for final result

    cout << "Enter a binary number: ";

    if ( cin>>inp==false )      //this if statement check for character input
    {
        cout << "Invalid input." ;
        return -1 ;
    }

    if ( inp<0 )        //this statement check for negative number
    {
        cout << "Input cannot be negative.";
        return -1 ;
    }

    while ( inp>0 )
    {
        if ( inp%10!=0 && inp%10!=1 )       //3rd check for digits are not 0 or 1
        {
            cout << "Can't be binary input." ;
            return -1 ;
        }
        rst=rst+inp%10*pow(2,ctr) ;
        ctr+=1 ;
        inp/=10 ;
    }
    cout << "Decimal is: " << rst ;
    return 0 ;
}
