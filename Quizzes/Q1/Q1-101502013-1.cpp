#include <iostream>

using namespace std;

int main()
{
    //a1輸入值 a2待加數 answer答案 c1次方計數器 c2迴圈計數器
    int a1=0,a2=1,answer=0,c1=0,c2=1;
    cout << "Enter an binary number: " ;
    if (cin >> a1 )                     //偵錯 非整數
    ;
    else
    {
        cout << "Invalid input." << endl;
        return -1;
    }
    if (a1<0)                             //偵錯  負數
    {
        cout << "Input can't be negative." << endl;
        return -1;
    }
    if (a1 % 10 ==1 || a1 % 10 == 0)        //偵錯 非0.1輸入
    ;
    else
    {
        cout << "Can't be binary input." << endl;
        return -1;
    }


    while (a1/10>0 || a1==1)
    {
        if (a1%10==1)           //若尾數為1則進行下列運算
        {
            while (c2<=c1)          //c1決定這是第幾位數 乘以2的次方數
            {
                a2=a2*2;
                c2++;
            }
            answer=answer+a2; //加上答案 運算後重設a2 c2 值
            a2=1;
            c2=1;
            a1=a1/10;       //a1/10因a1型態為整數 不會有小數點問題
            c1++;
        }
        else
        {
            a1=a1/10;
            c1++;
        }
    }
    cout << "Decimal is : " << answer << endl;
    return 0;
}
