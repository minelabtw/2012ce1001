#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int num,endnum,power=0,sum=0;
    bool flag1=true;//宣告一個旗子決定是否輸出結果
    cout<<"Enter a binary number: ";

    if(cin>>num==false)
    {//檢查輸入是否是數字，如果不是就不輸出結果，而輸出錯誤訊息
        cout<<"Invalid input.";
        flag1=false;
    }

    else if(num<0)
    {//之後再檢查是否是負數，如果是負數就不輸出結果，而輸出錯誤訊息
        cout<<"Input cannot be negative.";
        flag1=false;
    }

    else
        while(num!=0)//當num沒辦法再除以10之後就結束
            if(num%10==0 || num%10==1)
            {
                endnum=num%10;//儲存尾數(個位數)
                sum=sum+endnum*pow(2,power);
                power++;
                num=num/10;
            }
            else
            {//如果尾數不是0或1就輸出錯誤訊息
                cout<<"Can't be binary input.";
                flag1=false;
                break;
            }

    if(flag1)
        cout<<"Decimal is : "<<sum;
    return 0;
}
