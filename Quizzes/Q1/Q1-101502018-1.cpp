#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int a,b,c=0,i=0;
    cout <<"Enter a binary number: ";
    while(cin >> a)
    {
        if(a<0)
        {
            cout <<"Input cannot be negative."<<endl;//讓a<0的數不能被計算
            return 0;
        }
        for(a;a>=0;i++)
        {
            b=a%10;
             if(b!=1 && b!=0)
            {
                cout <<"Can't be binary input."<<endl;//讓不是2進位的數不能被計算
                return 0;
            }
            if(b==0)
                b=a/10;
            if(b==1)
                b=a/10;
            c=c+b*pow(2,i);//2進位轉換成10進位
        }
        cout <<"Decimal is: "<<c<<endl;
        return 0;
    }
    cout <<"Invalid input."<<endl;
    return 0;
}
