#include <iostream>
using namespace std;

int main()
{
    int number,number2,i=1,sum=0,k=0,t;

    //輸入
    cout<<"Enter a binary number : ";
    cin>>number;
    number2 = number;


    if(number < 0)
    {//輸入小於0時,顯示錯誤訊息
        cout<<"Input cannot be negative .";
        return -1;
    }
    if(!cin)
    {//輸入錯誤時,顯示錯誤訊息
        cout<<"Invalid input . ";
        return -1;
    }
    for(;number2 > 0;number2=number2/10)
    {//輸入不是二進位數字時,顯示錯誤訊息
        t=number2 %10;
        if(t>1)
        {
            cout<<"Can't be a binary input . ";
            return -1 ;
        }
    }

    for(;number>0;i = i*2)
    {//將二進位轉為十進位
        k = number%10*i;
        sum = sum + k;
        number = number / 10;
    }
    //輸出
    cout<<"Decimal is : "<<sum;
    return 0 ;
}
