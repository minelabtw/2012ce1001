#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int l,u,i=1,t,sum=0;

    //輸入
    cout<<"Enter the lower bound : ";
    cin>>l;

    if(l<0)
    {//輸入小於0,顯示錯誤訊息
        cout<<"Input cannot be negative . ";
        return -1 ;
    }
    if(!cin)
    {//輸入錯誤時,顯示錯誤訊息
        cout<<"Invalid input . ";
        return -1 ;
    }
    //輸入
    cout<<"Enter the upper bound : ";
    cin>>u;

    if(l>u)
    {//前項大於後項時,顯示錯誤訊息
        cout<<"Upper bound must be greater than lower bound . ";
        return -1 ;
    }
    if(!cin)
    {//輸入錯誤時,顯示錯誤訊息
        cout<<"Invalid input . ";
        return -1 ;
    }
    //檢查前項和後項間的所有質數
    for(;l>u;l++)
    {
        for(;i>l;i++)
        {
            t = l % i ;
            if(t==0)
            {
                sum = sum + 1 ;
            }
        }
        if(sum==2)
        {
            cout<<l<<" ";
            sum=0;
        }
    }//寫不出來,執行時不會跑出數字...
    return 0 ;
}
