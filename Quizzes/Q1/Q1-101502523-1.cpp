#include <iostream>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int binary,decimal,n;
    n = 0;
    decimal = 0;

    cout << "Enter a binary number: ";
    cin >> binary;

    if( binary < 0 )//負數偵錯
    {
        cout << "Input cannot be negative." << endl;
        return -1;
    }
    if( !cin )//符號偵錯
    {
        cout << "Invalid input." << endl;
        return -1;
    }

    while( binary > 0 )//二進位轉十進位
    {
        if( binary % 10 == 1 )//二進位轉十進位
        {
            decimal = decimal + pow(2,n);
            binary = binary / 10;
            n++;
        }
        if( binary % 10 == 0 )//二進位轉十進位
        {
            binary = binary / 10;
            n++;
        }
        if( binary % 10 > 1 )//二進位偵錯
        {
            cout << "Can't be binary input." << endl;
            return -1;
        }
        if( binary == 0 ) break;//binary為0時跳出迴圈
    }
    cout << "Decimal is " << decimal << endl;

    return 0;
}
