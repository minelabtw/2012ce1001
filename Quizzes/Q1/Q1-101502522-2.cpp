#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int L , U , i ;

    cout << "Enter the lower bound : ";
    cin >> L;

    cout << "Enter the upper bound : ";
    cin >> U;

    if (L > U)
        cout << "Upper bound must be greater than lower bound.";


    return 0;
}
