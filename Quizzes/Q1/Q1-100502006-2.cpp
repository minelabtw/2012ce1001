#include <iostream>

using namespace std;

int main()
{
    int a,b;
    bool ip=true;
    cout<<"Enter the lower bound : ";
    if(cin>>a==false||a<0)//輸入並判斷
    {
        if(a<0)//判斷正負
        {
            cout<<"Input cannot be negative.";
            return -1;
        }
        else
        {
            cout<<"Invalid input.";
            return -1;
        }
    }
    cout<<"Enter the upper bound : ";
    if(cin>>b==false||b<0)//輸入並判斷
    {
        if(b<0)//判斷正負
        {
            cout<<"Input cannot be negative.";
            return -1;
        }
        else
        {
            cout<<"Invalid input.";
            return -1;
        }
    }
    else if(a>b)//判斷大小
    {
        cout<<"Upper bound must be greater than lower bound";
        return -1;
    }
    else//運算並輸出
    {
        for(int i=a;i<=b;i++)
        {
           if(i!=0&&i!=1)
           {
               for(int j=2;j<i;j++)
               {
                   if(i%j==0)
                   {
                       ip=false;
                       break;
                   }
               }
           }
           else
           {
               ip=false;
           }
           if(ip)
           {
               cout<<i<<" ";
           }
           ip=true;
        }
    }
    return 0;
}
