#include <iostream>
#include <math.h>
using namespace std;

int main ()
{
    int binary,integer,answer=0,count=0,i=1;

    cout << "Enter a binary number: ";
    cin >> binary;

    if (binary<0) //錯誤偵錯 確定輸入大於0
        cout << "Input cannot be negative.";
    else if (cin==false) //錯誤偵錯 確認輸入非字元
        cout << "Invalid input.";
    else
    {
        while (binary>=1)
        {
            integer=binary%10;

            if ( (integer!=0) && (integer!=1) ) //錯誤偵錯 確認是2進位
            {
                cout << "Can't be binary input.";
                return -1;
            }
            else
                answer=answer+integer*i;//2進位轉10進位

            binary=binary/10;
            i=i*2;
        }
        cout << "Decimal is: " << answer << endl;
    }
    return 0;
}
