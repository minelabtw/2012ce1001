#include<iostream>
#include<stdlib.h>
using namespace std;


string checkinput(string str);


int main()
{
    //initial
    int decimal=0,pow=1;
    string binary="";


    //input
    cout<<"Enter a binary number: ";
    cin>>binary;


    //exception
    if(checkinput(binary)!="")
    {
        cout<<checkinput(binary);
        return -1;
    }


    //compute
    for(int i=binary.length()-1;i>=0;i--)
    {
        decimal+=(binary[i]-48)*pow;
        pow*=2;
    }


    //output
    cout<<"Decimal is: "<<decimal<<endl;


    //end
    system("pause");
    return 0;
}




string checkinput(string str)
{
    if(str[0]=='-')
    {
        for(int i=1;i<str.length();i++)
            if(str[i]>'9'||str[i]<'0')//not number input
                return "Invalid input.\n";

        //negative number
        return "Input cannot be negative.\n";
    }

    for(int i=0;i<str.length();i++)
        if(str[i]>'9'||str[i]<'0')//not number input
            return "Invalid input.\n";

    for(int i=0;i<str.length();i++)
        if(str[i]!='1'&&str[i]!='0')//not binary input
            return "Can't be binary input.\n";

    //no error
    return "";
}
