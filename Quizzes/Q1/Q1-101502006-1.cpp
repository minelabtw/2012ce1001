#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
int bin,sum=0;
int temp=1;
cout << "Enter a binary number: " ;

    while (cin >> bin == false) // 錯誤偵測 非數字
    {
        cout <<"Invalid input.";
        return 0;
    }
    if (bin < 0)
    {
        cout << "Input canot be negative."; // 錯誤偵測 負數
        return 0;
    }

        while (bin > 0) // 二進位轉十進位
        {
            if (bin%10 >=2) // 錯誤偵測 非二進位
            {
                cout << "Cant be binary input.";
                return 0;
            }
            sum = sum + (bin%10)*temp;
            temp= temp * 2;
            bin = bin / 10;
        }
        cout <<"Decimal: "<< sum;

return 0;
}
