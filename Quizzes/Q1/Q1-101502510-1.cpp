#include <iostream>
using namespace std;
int main()
{
    int a,b=0,c=0;
    cout<<"Enter a binary number: ";
    while(cin>>a)//輸入一個數值a
    {
        if(a<0)//如果a小於零則顯示"Input cannot be negative."
        {
            cout<<"Input cannot be negative.";
            return 0;
        }
        for( int i=1; a>=1; i*=2 )
        {
            b=(a%10)*i;//b等於a除以10的餘數乘以i
            c+=b;//c等於c+b
            a=a/10;//a等於a/10
        }

        if(a%10==0||a%10==1)//若a除以10餘0或1則顯示c
            cout<<"Decimal is: "<<c;

        else//若a除以10不餘0或1則顯示"Can't be binary input."
            cout<<"Can't be binary input.";
        return 0;
    }
    cout<<"Invalid input.";
    return 0;
}
