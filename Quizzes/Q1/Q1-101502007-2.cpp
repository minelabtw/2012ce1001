#include <iostream>

using namespace std;

int main()
{
    int num1,num2,sum=0,c;
    cout << "Enter the lower bound : " ;
    cin >>num1;
    cout << "Enter the upper bound : " ;
    cin >>num2;
     if (num1<0 || num2<0)
    {
        cout <<"Input cannot be negative.  ";
        return -1;
    }
    for (int x=num1;x<=num2;x++)
    {
        for (int n=1;n<=x;n++)
        {
            if (x%n==0)
            {
            c=x/n;
            sum+=c;
            }
        }
        if (sum==x+1)
        cout <<x<<'\t';
        sum=0;
    }
    if (num1>num2)
    {
        cout <<"Upper bound must be great than lower bound ";
        return -1;
    }

    return 0;
}
