#include <iostream>
using namespace std;
int main()
{
    int binary , decimal , sum = 0 , count = 1;//decimal紀錄binary的每一位數 , sum紀錄binary轉十進位後的結果 , count紀錄每一位數需要乘的基數

    cout << "Enter a binary number: ";
    if(cin >> binary == false)//判斷輸入是否為字元,是的話則輸出錯誤訊息
        cout << "Invalid input." << endl;
    else if(binary < 0)//判斷輸入是否為負數,是的話則輸出錯誤訊息
        cout << "Input cannot be negative." << endl;
    else
    {
        while(binary > 0)//判斷binary的每一位數是否有被轉換成十進位
        {
            decimal = binary % 10;//把binary的每一位數存入decimal
            if(decimal > 2)//如果那一位數有超過2,則不是二進位數,並結束程式
            {
                cout << "Can't be binary input." << endl;
                return -1;
            }
            binary /= 10;//binary前進一位數
            sum += decimal * count;//將二進位轉十進位的結果存入sum
            count *= 2;//當binary前進一位數時,則count需乘二
        }
        cout << "Decimal is: " << sum << endl;//輸出轉換後的答案
    }
    return 0;
}
