#include<iostream>
using namespace std;
int main()
{
    int a,b=0;
    cout<<"Enter a binary number:  ";
    if((cin>>a==false)||(a<0))//若輸入為負或字串，
    {
        if(a<0)//若為負
            cout<<"Input cannot be negative."<<endl;
        else//若為字串
            cout<<"Invalid input."<<endl;
        return -1;
    }
    for(int c=1;a>0;a=a/10)
    {
        if(a%10>1)//判定a是否為2進位數
        {
            cout <<"Can't be binary input."<<endl;
            return -1;//若不是，回傳-1
        }
        b=b+a%10*c;
        c=c*2;
    }
    cout <<"Decimal is:  "<<b<<endl;//若是2進位，改成十進位輸出
    return 0;//回傳0
}
