#include<iostream>

using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form arrPtr[start] to arrPtr[end] will be sorted
    int i, j, temp, *Ptr1, *Ptr2;

    for(i=start ; i <= end ; i++)//put number from small to large.
    {
        for(j=i+1 ; j <= end ; j++)
        {
            if(arrPtr[i] > arrPtr[j])
            {
                temp = arrPtr[j];
                arrPtr[j] = arrPtr[i];
                arrPtr[i] = temp;
            }
        }
    }
}
int main()
{
    int inputArr[1000], n, i;

    while(cin >> n)// enter n number.
    {
        /////input begin//////

        for(int i=0 ; i<n ; i++)//enter number into inputArr[].
        {
            cin >> inputArr[i];
        }

        /////input end///////

        sort( &inputArr[0], 0, n-1 );

        /////output begin/////

        for(i=0; i<n ; i++)//cout number from little to large.
        {
            cout << inputArr[i] << " ";
        }

        cout << endl;

        /////output end///////
    }

    return 0;
}
