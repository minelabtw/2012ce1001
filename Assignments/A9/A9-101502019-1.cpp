#include<iostream>

using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for(int i = start;i < end;i++)
    {
        for( int j = i + 1;j < end;j++)
        {
          if(arrPtr[i] > arrPtr[j])
            {
                int buff;
                buff = arrPtr[i];
                arrPtr[i] = arrPtr[j];
                arrPtr[j] = buff;
            }
        }
    }
}

int main()
{
    int inputArr[1000],n;

    while(cin >> n)
    {
        /////input begin//////
        for(int i = 0;i < n;i++ )
            cin >> inputArr[i];
        /////input end///////

        sort(inputArr,0,n);

        /////output begin/////
        for(int i = 0;i < n;i++)
            cout << inputArr[i] << " ";
        /////output end///////
    }
    return 0;
}
