#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{

    for( int i=start ; i<=end ; i++ )//從頭比到尾
    {
        for( int j=start ; j<end ; j++ )//交換數字
        {
            if(arrPtr[j]>arrPtr[j+1])
            {
                int buff=arrPtr[j];
                arrPtr[j]=arrPtr[j+1];
                arrPtr[j+1]=buff;
            }
        }
    }
}

int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for( int i=0; i<n ;i++ )
        {
            cin >> inputArr[i];
        }
        /////input end///////

        sort( inputArr , 0 , n-1 );

        /////output begin/////
        for( int i=0; i<n; i++ )
        {
            cout << inputArr[i]<<" " ;
        }
        cout << endl;
        /////output end///////

    }
    return 0;
}
