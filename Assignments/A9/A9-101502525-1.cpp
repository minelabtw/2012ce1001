#include<iostream>
using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{
    //from inputArr[start] to inputArr[end] will be sorted
    if(start>=end)
        return;
    int left=start,right=end,tmp=arrPtr[end];//take end as pivot
    for(;left<right;)
    {
        while(arrPtr[left]<=tmp&&left<right)//find the data bigger than pivot
            left++;
        arrPtr[right]=arrPtr[left];//move to right side
        while(tmp<=arrPtr[right]&&left<right)//find the data smaller than pivot
            right--;
        arrPtr[left]=arrPtr[right];//move to left side
    }
    arrPtr[right]=tmp;//put pivot back
    sort(arrPtr,start,left-1);//recursion left side
    sort(arrPtr,right+1,end);//recursion right side
}

int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for(int i=0;i<n&&cin>>inputArr[i];i++);
        /////input end///////
        sort(inputArr,0,n-1);
        /////output begin/////
        for(int i=0;i<n;i++)
            cout<<inputArr[i]<<' ';
        cout<<endl;
        /////output end///////
    }
    return 0;
}
