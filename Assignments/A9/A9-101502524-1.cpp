#include<iostream>
#include<iomanip>
using namespace std;

void sort(int*arrPtr,int start,int end)
{
    int save;
    for(;start<=end;start++)
    {//將每個數字排序
        int moveItem=start;
        save = arrPtr[start];
        while((moveItem>0)&&(arrPtr[moveItem-1]>save))
        {
            arrPtr[moveItem] = arrPtr[moveItem-1];
            moveItem--;
        }
        arrPtr[moveItem] = save;
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        for(int a=0;a<n;a++)
        {
            cin>>inputArr[a];
        }

        sort(inputArr,0,n-1);//轉換輸入的數字順序，由小到大

        for(int a=0;a<n;a++)
        {
            cout<<inputArr[a]<<" ";
        }
        cout<<endl;
    }
    return 0;
}
