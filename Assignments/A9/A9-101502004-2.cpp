#include<iostream>
#include<string>
using namespace std;
void filter(string *word,string ban)
{
    for(int i=0;i<(*word).length()-ban.length();i++)//先將要檢查的字串跟不想出現的字串對齊
    {
        bool match=true;//判斷是不是要替換的字
        for(int j=0;j<ban.length();j++)//對齊後開始檢查範圍內的字是不是符合
        {
            if((*word)[i+j]!=ban[j])//要是範圍內有字不是，那就跳出來
            {
                match=false;
                break;
            }
        }
        if(match)//符合條件者即帶換掉字串陣列裡的內容
        {
            for(int k=i;k<i+ban.length();k++)
                (*word)[k]='*';
        }
    }
}

int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl;
    }
    return 0;
}
