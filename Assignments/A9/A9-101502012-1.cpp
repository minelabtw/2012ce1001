#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    int buffer;
    for (int j=start;j<end;j++)
    {
        for (int k=j;k<(end-1);k++)
        {
            if (arrPtr[j]>arrPtr[k+1]) //Bubble sort交換法
                {
                buffer=arrPtr[j];
                arrPtr[j]=arrPtr[k+1];
                arrPtr[k+1]=buffer;
                }
        }
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        for(int i=0;i<n;i++)
        {
            cin >> inputArr[i];
        }
        sort( inputArr , 0 , n ); //陣列就是指標 因此不用加&

        for(int j=0;j<n;j++)
        {
            cout << inputArr[j] << " ";
        }
        cout << endl;
    }
    return 0;
}
