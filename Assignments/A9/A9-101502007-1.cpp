#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                        //form inputArr[start] to inputArr[end] will be sorted
    int s;
    for (int i=start;i<end-1;i++)
    {
        for (int j=i+1;j<end;j++) //與此項之後的所有項比大小,若大於則交換
        {
            if (arrPtr[i]>arrPtr[j])
            {
               s=arrPtr[j];
               arrPtr[j]=arrPtr[i];
               arrPtr[i]=s;
            }
        }
    }
}

int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for (int e=0;e<n;e++)
        cin >>inputArr[e];
        /////input end///////

        sort( inputArr , 0 , n );

        /////output begin/////
        for (int f=0;f<n;f++)
        cout <<inputArr[f]<<" ";
        /////output end///////
    }
    return 0;
}
