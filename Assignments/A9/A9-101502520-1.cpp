#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{//Bubble Sort                          //form inputArr[start] to inputArr[end] will be sorted
    for(int i=start;i<end-1;i++)
        for(int j=start;j<end-1;j++)
        {
            if(arrPtr[j]>arrPtr[j+1])
                swap(arrPtr[j],arrPtr[j+1]);
        }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        for(int i=0; i<n; i++)
            cin>>inputArr[i];
        sort(inputArr, 0, n);
        for(int i=0; i<n; i++)
            cout<<inputArr[i]<<" ";
        cout<<endl;
    }
    return 0;
}
