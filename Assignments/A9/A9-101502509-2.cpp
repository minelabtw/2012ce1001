#include<iostream>
#include<string>
using namespace std;
void filter(string *c,string b)
{
    for(int i=0;i<(*c).length();i++)  //check each item of chat
    {
        if((*c)[i]==b[0])  //check if chat[i] is equal to bannedWord[0],
                           //if satisfied,check whole bannedWord from chat[i]
        {
           bool check=true;  //initialize the check is true
           for(int j=0;j<b.length();j++,i++)
           {
              if((*c)[i]!=b[j])
              {
                 check=false;
                 i=i-1;  //if it is not bannedWord,go to chat[i],and break
                 break;
              }
           }
           if(check==true)  //if have bannedWord,cover them with "*"
           {
              for(int k=i-1,end=i-b.length();k>=end;k--)
                 (*c)[k]='*';
              i=i-1;   //go to chat[i] and continue check
           }
        }
    }
}
int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);   //input chat
        getline(cin,bannedWord);  //input bannedWord
        filter(&chat,bannedWord); //filter bannedWord
        cout << chat << endl << endl;  //output chat after filter
    }
    return 0;
}
