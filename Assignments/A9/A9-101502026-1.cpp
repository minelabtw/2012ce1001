#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for(int i=0;i<end;i++)
        for(int j=i;j+1<end;j++)
            if(arrPtr[j]>arrPtr[j+1])
            {
                int change=arrPtr[j];
                arrPtr[j]=arrPtr[j+1];
                arrPtr[j+1]=change;
            }
}

int main()
{
    int n,k=0;
    while(cin >> n)
    {
        int inputArr[1000];
        for(int i=0;i<n;i++)
            cin >> inputArr[i];

        for(int i=0;i<n;i++)
            for(int j=i;j<n;j++)
                sort(inputArr,0,n); //pass the whole array "inputArr" to the function "sort"

        for(int i=0;i<n;i++)
            cout << inputArr[i] << " ";
        cout << endl;
    }
    return 0;
}
