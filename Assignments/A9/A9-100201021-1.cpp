#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    int a;
        for(int i=start;i<end;i++)              //loop...
            for(int j=start;j<end;j++)          //
                if(*(arrPtr+j) > *(arrPtr+j+1)) //inputArr[a] > inputArr[a+1] 則兩者交換
                {
                    a = *(arrPtr+j);
                    *(arrPtr+j) = *(arrPtr+j+1);
                    *(arrPtr+j+1) = a;
                }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        if(n<1000)
        {
            /////input begin//////
            for(int i=0;i<n;i++)
                cin >> inputArr[i];
            /////input end///////

            int *inputArrPtr = inputArr;    //陣列inputArr的地址
            sort(inputArrPtr,0,n);          //小至大排列inputArr
            /////output begin/////
            for(int i=0;i<n;i++)
                cout << inputArr[i] << " ";
            cout << endl;
            /////output end///////
        }
    }
    return 0;
}
