#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{
    for(int c=end;c>0;c--)//泡泡排序法
    {
        for(int count=0,buff;count<c;count++)
        {
            if((arrPtr)[count]>(arrPtr)[count+1])
            {
                buff=(arrPtr)[count];
                (arrPtr)[count]=(arrPtr)[count+1];
                (arrPtr)[count+1]=buff;
            }
        }
    }
}
int main()
{
    int n;//如果我以助教的範例形式，會產生錯誤
    while(cin >> n)
    {
        int inputArr[1000];
        /////input begin//////
        for(int count=0;count<n;count++)
        {
            cin>>inputArr[count];
        }
        /////input end///////

        sort(inputArr  ,0 ,n  );

        /////output begin/////
        for(int count=0;count<n;count++)
        {
            cout<<inputArr[count]<<" ";
        }
        cout<<endl;
        /////output end///////
    }
    return 0;
}
