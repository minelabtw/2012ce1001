#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)
{
    for (start ; start<end ; start++) //將數字由小到大排序
    {
        for (int compare=start+1 ; compare<end ; compare++)
        {
            if (arrPtr[start]>arrPtr[compare])
            {
                int buffer;
                buffer=arrPtr[start];
                arrPtr[start]=arrPtr[compare];
                arrPtr[compare]=buffer;
            }
        }
    }
}

int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        int start=0;
        /////input begin//////
        for (int x=0 ; x<n ; x++)
            cin >> inputArr[x];
        /////input end////////

        sort (inputArr,start,n);

         /////output begin/////
        for (int x=0 ; x<n ; x++)
            cout << inputArr[x] << ' ';
        cout << endl;
        /////output end////////
    }

    return 0;
}
