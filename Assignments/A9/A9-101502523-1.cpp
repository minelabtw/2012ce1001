#include<iostream>
using namespace std;
void sort( int *arrPtr, int start, int end )//from low to high
{                                           //form inputArr[start] to inputArr[end] will be sorted
    int insert;
    for( ; start <= end ; start++ )
    {
        insert = arrPtr[start];
        int moveItem = start;

        while( ( moveItem > 0 ) && ( arrPtr[ moveItem - 1 ] > insert ) )
        {
            arrPtr[ moveItem ] = arrPtr[ moveItem - 1 ];
            moveItem--;
        }
        arrPtr[ moveItem ] = insert;
    }
}

int main()
{
    int inputArr[1000], n;
    while( cin >> n )
    {
        for( int i = 0 ; i < n ; i++ )
            cin >> inputArr[i];

        sort( inputArr , 0 , n-1 );

        for( int i = 0 ; i < n ; i++ )
            cout << inputArr[i] << ' ';

        cout << endl;
    }
    return 0;
}

