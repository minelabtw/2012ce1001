#include<iostream>
#include<string>
using namespace std;

void filter( string *chatPtr , string bannedWord )
{
    string bannedstart(bannedWord.length(),'*');//宣告一字串'*'長度為bannedWord的長度
    while (1)
    {
        if ((*chatPtr).find(bannedWord) != -1 )//找出是否有相同字串，並傳回位置。若是找不到相同字串 會傳回-1
        //.replace:取代chat中有與bannedWord相同的字串
        //.find:回傳bannedWord在chatPtr的位置
        (*chatPtr).replace((*chatPtr).find(bannedWord), bannedWord.length(), bannedstart);
        else break;
    }
}

int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl  ;
    }
    return 0;
}
