//A9-100502201-1
//Since arrPtr is an address, no need to add pointer or address symbol to answers
//To shorten code length, define SWAP function below
#include<iostream>
#define SWAP(x,y){int t;t=x;x=y;y=t;}
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for (start;start<end-1;start++)
    {
        int m=start;
        for (int n=start+1;n<end;n++)
            if(arrPtr[n]<arrPtr[m])
                m=n;
        if (start!=m)
            SWAP(arrPtr[start],arrPtr[m])
    }
}

int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        //input
        for (int i=0;i<n;i++)
            cin>>inputArr[i];
        int j=0;
        sort( inputArr , j , n);
        //output
        for (int k=0;k<n;k++)
            cout<<inputArr[k]<<" ";
        cout<<endl;
    }
    return 0;
}
