#include<iostream>
using namespace std;
void swap(int *aPtr , int *bPtr) //swap函式用來交換兩個數字
{
    int buffer = *aPtr;
    *aPtr = *bPtr;
    *bPtr = buffer;
}
void sort(int *arrPtr , int start , int end)//from low to high
{                                           //from inputArr[start] to inputArr[end] will be sorted
    for(int j = start ; j < end ; j++)      //sort函式使用泡沫排序法把數字由小排到大
    {
        for(int k = j + 1 ; k < end ; k++)
        {
            if(*(arrPtr + j) > *(arrPtr + k))
                swap(&*(arrPtr + j) , &*(arrPtr + k));
        }
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for(int i = 0 ; i < n ; i++)
            cin >> inputArr[i];
        /////input end///////

        sort( inputArr , 0 , n );

        /////output begin/////
        for(int i = 0 ;i < n ;i++)
            cout << inputArr[i] << " ";
        cout << endl;
        /////output end///////
    }
    return 0;
}
