#include <iostream>
#include <string>//head files
using namespace std;

void filter(string *chat,string bannedWord)//this function will replace the string that users' input by the word they want to ban
{
    int loc_chat,len_ban;
    string star;
    len_ban=bannedWord.size();//know the length of bannedWord
    for (int i=0;i<len_ban;i++)//a string that assemble by * and as long as the bannedWord
    {
        star=star+"*";
    }
    for (int i=1;i>0;i++)//ban the word of the users' input
    {
        loc_chat=(*chat).find(bannedWord,0);
        if (loc_chat!=-1)
        {
            (*chat)=(*chat).replace(loc_chat,len_ban,star);
        }
        else//if there is no coincide word in chat, loc_chat will be -1, then break the loop
        break;
    }
}

int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl;
    }
    return 0;
}
