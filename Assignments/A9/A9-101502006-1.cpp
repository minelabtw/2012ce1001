#include<iostream>
using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for (int i=end;i>start;i--)
    {
        for (int j=start;j<i;j++)
        {
            if (arrPtr[j]>arrPtr[j+1])
            {
                ////// to exchange //////
                int tmp;
                tmp=arrPtr[j];
                arrPtr[j]=arrPtr[j+1];
                arrPtr[j+1]=tmp;
                /////////////////////////
            }
        }
    }
}

int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for (int i=0;i<n;i++)
        {
            cin >> inputArr[i];
        }
        /////input end///////

        sort( inputArr , 0 , n );

        /////output begin/////
        for (int i=0;i<n;i++)
        {
            cout << inputArr[i] << " ";
        }
        cout << endl;
        /////output begin/////
    }
    return 0;
}
