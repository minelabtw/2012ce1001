#include <iostream>

using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    int buffer;
    for (int i=0;i<end;i++)
    {
        for (int j=i;arrPtr[j]<arrPtr[j-1] && j>0;j--)//從arrPtr[0]開始檢查 若比前一數字小則換位
        {
            buffer=arrPtr[j];
            arrPtr[j]=arrPtr[j-1];
            arrPtr[j-1]=buffer;
        }
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for (int i=0;i<n;i++)
            cin >> inputArr[i];
        /////input end///////

        sort(inputArr,0,n);

        /////output begin/////
        for (int i=0;i<n;i++)
            cout << inputArr[i] << " " ;
        cout << endl;
        /////output end///////

    }
    return 0;
}
