//資工系四年B班_965002086_張凱閔 
/*
計算機實習_HW_9-1
 
簡單的排序程式：由小排到大 
輸入： 
先輸入一個正整數 n 代表後面有 n 個整數的輸入所形成的數列。(n<1000) 
要做重複輸入，但不必做程式結束的條件。
 
輸出： 
由小排到大的數列。 
必須照著以下格式：
#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    ......
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        ......
        /////input end///////

        sort( ... , ... , ... );

        /////output begin/////
        ......
        /////output end///////
    }
    return 0;
}

範例輸出結果：
 5             <----輸入
-1 5 7 -7 -6  <----輸入
-7 -6 -1 5 7  <----輸出
8
-10 45 98 -76 2 0 7 18
-76 -10 0 2 7 18 45 98 
*/

#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high by selection sort
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for (int i=start; i<end-1; i++)
    {
        for (int j=i+1; j<end; j++)
        {
            if (arrPtr[i]>arrPtr[j])
            {
               int temp;
               temp=arrPtr[i];
               arrPtr[i]=arrPtr[j];
               arrPtr[j]=temp;
            }
        }
               
    }
}

int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for (int i=0; i<n; i++)
        cin>>inputArr[i];

        sort( inputArr , 0 , n );

        //output begin
        for (int i=0; i<n; i++)
        cout<<inputArr[i]<<" ";
        cout<<endl;
        //output end
    }
    return 0;
}
