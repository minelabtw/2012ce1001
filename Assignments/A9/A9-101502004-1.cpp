#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    int buffer;
    for(int i=start;i<=end;i++)//有幾個數就運算幾次，氣泡排序法
    {
        for(int j=start;j<=end-1;j++)//從第一個開始算，算到最後一個數字前
        {
            if(*(arrPtr+j)>*(arrPtr+(j+1)))//要是前面的數字比較大就交換
            {
                buffer=*(arrPtr+(j+1));
                *(arrPtr+(j+1))=*(arrPtr+j);
                *(arrPtr+j)=buffer;
            }
        }
    }
}
int main()
{
    int inputArr[1000],n=0;
    while(cin >> n)
    {
        /////input begin//////
        for(int i=0;i<n;i++)
            cin >> inputArr[i];
        /////input end////////

        sort(&inputArr[0],0,n-1);

        /////output begin/////
        for(int i=0;i<n;i++)
            cout << inputArr[i] << " ";
        /////output end///////
    }
    return 0;
}
