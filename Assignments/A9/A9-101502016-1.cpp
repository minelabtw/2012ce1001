#include<iostream>

using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high

{ //form inputArr[start] to inputArr[end] will be sorted

    int buffer;

    for ( start=0 ; start<end ; start++ )
        for (  int next=start+1 ; next<end ; next++ )
            if ( arrPtr[start] > arrPtr[next] )
                {
                    buffer = arrPtr[start];
                    arrPtr[start] = arrPtr[next];
                    arrPtr[next] = buffer;
                }

}

int main()

{

int inputArr[1000],n;


while(cin >> n)

{

/////input begin//////


    for ( int i=0 ; i<n ; i++)
        cin >> inputArr[i];


/////input end///////



    sort( inputArr , 0 , n );



/////output begin/////

    for ( int i=0 ; i<n ; i++)
        cout << inputArr[i] << " ";

    cout << endl;

/////output end///////

}

return 0;

}
