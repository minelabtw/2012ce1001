#include <iostream>
#include <algorithm>

using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{
    sort(arrPtr + start, arrPtr + end);//form inputArr[start] to inputArr[end] will be sorted
}

int main()
{
    int inputArr[1000],n;

    while(cin >> n)
    {
        /////input begin//////

        for(int i = 0 ; i < n ; i++)
        {
            cin >> inputArr[i];
        }

        /////input end///////


        sort(inputArr , 0 , n);


        /////output begin/////

        for(int i = 0 ; i < n ; i++)
        {
            cout << inputArr[i] << " ";
        }

        cout << endl;

        /////output end///////
    }

    return 0;
}
