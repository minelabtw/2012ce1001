#include<iostream>
#include<string>
#include<cctype>
using namespace std;
void filter(string *chatptrs, string word)
{
    for(int i = 0; i < (*chatptrs).length(); i++) // for loop to check the sentence
    {
        for (int j = 0; j < word.length(); j++) // for loop to check the word to ban
        {
            while ((*chatptrs)[i] == word[j] && j < word.length()) // check the word to ban on the sentence
            {
                i++; // check the second letter of the word
                j++;
            }
            if ((*chatptrs)[i] != word[j] && j < word.length()) // if is not the word, break and continue looking
                break;
            else
            {
                for (int k = i - 1, z = i - word.length(); k >= z; k--) // if it's the word, banned it
                    (*chatptrs)[k] = '*';
            }
        }
    }
}
int main()
{
    string chat,bannedWord;
    while(true) // while loop
    {
        getline(cin,chat); // input a word or sentence or some character
        getline(cin,bannedWord); // character wanted to ban
        filter(&chat,bannedWord); // process to ban
        cout << chat << endl << endl; // output the baneed form
    }
    return 0;
}
