#include<iostream>

using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high

{ //form inputArr[start] to inputArr[end] will be sorted
    for(int x=start+1;x<=end;x++)
        for(int y=start;y<=end-x+start;y++)     //bubble sort
            if(arrPtr[y]>arrPtr[y+1])
            {
                int z=arrPtr[y];
                arrPtr[y]=arrPtr[y+1];
                arrPtr[y+1]=z;
            }
}

int main()

{

    int inputArr[1000],n;

    while(cin >> n)

    {

    /////input begin//////

        for(int x=0;x<n;x++)
            cin>>inputArr[x];

    /////input end///////



        sort( inputArr , 0 , n-1 );// call function;



    /////output begin/////

        for(int x=0;x<n;x++)
            cout<<inputArr[x]<<' ';
        cout<<endl;

    /////output end///////

    }

    return 0;

}
