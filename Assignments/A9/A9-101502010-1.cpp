#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for(int i=end;i>start;i--)
        for(int j=end;j>end-i;j--)
            if(*(arrPtr+j)<*(arrPtr+j-1))//put the smallest number to the front
            {
                int c=*(arrPtr+j);
                *(arrPtr+j)=*(arrPtr+j-1);
                *(arrPtr+j-1)=c;
            }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for(int i=0;i<n;i++)
            cin>>inputArr[i];
        /////input end///////

        sort( &(inputArr[0]) , 0 , n-1 );

        /////output begin/////
        for(int i=0;i<n;i++)
            cout<<inputArr[i]<<" ";
        /////output end///////
        cout<<endl;
    }
    return 0;
}
