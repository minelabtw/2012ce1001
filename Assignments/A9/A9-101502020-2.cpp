//A9-2 by J
#include<iostream>
#include<string>
using namespace std;
void filter( string *inp , string ban )
{
    int ctr=0 ;     //here I use a counter to filt

    for ( int i=0 ; i<( (*inp).length()-(ban).length() ) ; i++ )        //this loop check for all the input
    {
        for ( int j=0 ; j<(ban).length() ; j++ )        //this loop check whether a continuous string should be banned or not
        {
            if ( (*inp)[i+j]==ban[j] )
                ctr++;      //if it's the same, ctr+1
        }

        if ( (ban).length()==ctr )      //it means that all words are the same with banned word, i.e. it shall be banned
        {
            for (int k=0 ; k<(ban).length() ; k++ )     //this loop is going to ban this word
            {
                (*inp)[i+k]=42 ;        //*'s ASCII code is 42.
            }
        }
        ctr=0 ;     //reset the ctr for next check.
    }
}
int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl  ;
    }
    return 0;
}
