#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for(int i=0;i<end+1;i++)//bubble sort
    {
        for(int j=i;j<end+1;j++)
        {
            if(arrPtr[i]>arrPtr[j])
            {
                int buffer;
                buffer=arrPtr[i];
                arrPtr[i]=arrPtr[j];
                arrPtr[j]=buffer;
            }
        }
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        for(int i=0;i<n;i++)//input
        {
            cin>>inputArr[i];
        }
        sort(inputArr,0,n-1);//sort
        for(int i=0;i<n;i++)//output
        {
            cout<<inputArr[i]<<" ";
        }
        cout<<endl;
    }
    return 0;
}
