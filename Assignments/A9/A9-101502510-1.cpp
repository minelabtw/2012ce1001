#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{

    int buffer;

    while(start<end)//form inputArr[start] to inputArr[end] will be sorted
    {
        for(int k=start; k<end; k++)
        {
            if(arrPtr[start]>arrPtr[k])
            {
                buffer=arrPtr[start];
                arrPtr[start]=arrPtr[k];
                arrPtr[k]=buffer;
            }
        }
        start++;
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for(int i=0; i<n; i++)
        {
            cin>>inputArr[i];
        }
        /////input end///////

        sort( inputArr , 0 , n );

        /////output begin/////
        for(int i=0; i<n; i++)
        {
            cout<<inputArr[i]<<" ";
        }
        cout<<endl;
        /////output end///////
    }
    return 0;
}
