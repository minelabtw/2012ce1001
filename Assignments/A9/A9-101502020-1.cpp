//A9-1 by J
#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    int buff ;
    int hold ;

    for (int i=0 ; i<=end ; i++ )       //check for all data
    {
        buff=i ;
        for ( int j=i ; j<=end ; j++ )      //check for the present datum is the min datum & check for the laters after present datum.
        {
            if ( arrPtr[j]<arrPtr[i] )        //if the present datum isn't the min, change them.
            {
                hold=arrPtr[i] ;
                arrPtr[i]=arrPtr[j] ;
                arrPtr[j]=hold ;
            }
        }
    }

}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        if ( n<=0 || n>=1000 )
            return 0 ;      //input required by problem: positive integer & n<1000

        for ( int i=0 ; i<n ; i++ )
            cin >> inputArr[i] ;

        /////input end///////

        sort( inputArr , 0 , n-1 );

        /////output begin/////

        for ( int i=0 ; i<n ; i++ )
            cout << inputArr[i] << " " ;

        cout << endl ;

        /////output end///////
    }
    return 0;
}
