#include <iostream>
#include <string>
using namespace std;

void filter( string *c , string ban )
{
    for( int i = 0 ; i < (*c).length() ; i++ )
        if( (*c).substr( i, ban.size() ) == ban )   //to find the string that match the ban words.
            for( int j = i ; j <= i+ban.size()-1 ; j++ )   //to ban the words.
                (*c)[j] = 42;  //ASCII: "*" is value 42.
}

int main()
{
    string chat, bannedWord;
    while( true )
    {
        getline( cin, chat );
        getline( cin, bannedWord );
        filter( &chat, bannedWord );
        cout << chat << endl << endl  ;
    }
    return 0;
}
