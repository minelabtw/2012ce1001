#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for(start;start<end;start++)        //從0跑到最後一位.
    {
        for(int i=start+1;i<end;i++)    //要start的下一項.
        {
            if(arrPtr[start]>arrPtr[i]) //比較start的下一項.
            {
                int a;
                a=arrPtr[start];
                arrPtr[start]=arrPtr[i];
                arrPtr[i]=a;
            }
        }
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        for(int i=0;i<n;i++)        //輸入.
        {
            cin>>inputArr[i];
        }

        sort(inputArr, 0, n);

        for(int i=0;i<n;i++)
        {
            cout<<inputArr[i]<<" ";
        }
    }
    return 0;
}
