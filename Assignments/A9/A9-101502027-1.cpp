#include<iostream>

using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{  //form inputArr[start] to inputArr[end] will be sorted
    int buffer;
    for (int i=start;i<=end;i++)// 一個個去比較
    {
        for (int j=start;j<=end;j++)
        {
            if (arrPtr[i]<arrPtr[j])
            {
                buffer=arrPtr[i];
                arrPtr[i]=arrPtr[j];
                arrPtr[j]=buffer;
            }//換順序
        }
    }
}

int main()

{

    int inputArr[1000],n;

    while(cin >> n)

    {
        /////input begin//////

        for (int i=0;i<n;i++)
            cin >> inputArr[i];

        /////input end///////

        sort(inputArr, 0 , n-1 );

        /////output begin/////

        for (int i=0;i<n;i++)
            cout<< inputArr[i]<<" ";
        cout<<endl;
        /////output end///////

    }

    return 0;

}

