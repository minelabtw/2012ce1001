#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for(int i=end;i>=0;i--)             //By bubble sort
        for(int j=start;j<i;j++)
            if(arrPtr[j+1]<arrPtr[j])
                swap(arrPtr[j+1], arrPtr[j]);
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for(int i=0;i<n;i++)
            cin >> inputArr[i];
        /////input end///////

        sort( inputArr , 0 , n-1 );

        /////output begin/////
        for(int i=0;i<n;i++)
            cout << inputArr[i] << " ";
        cout << endl;
        /////output end///////
    }
    return 0;
}
