//資工系四年B班_965002086_張凱閔 
/*
計算機實習_HW_9-2
 
你架設了一個聊天室，可是卻有使用者頻繁的在聊天室裡使用不雅字眼，所以你希望可以寫一個程式將不想看到的字給屏蔽住。
 
輸入： 
第一行代表某位使用者的發言。 
第二行則是你想要遮住的字眼。
 
記得做重複輸入，而不需要做程式結束的判斷。
 
輸出：
 
屏蔽後的結果。
 
必須照以下格式：
#include<iostream>
#include<string>
using namespace std;
void filter( ... , ... )
{
    ......
}
int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl  ;
    }
    return 0;
}


範例輸出結果：
 What the fuck!!!  <----輸入
fuck              <----輸入
What the ****!!!  <----輸出

It is a dog dog dog.
dog
It is a *** *** ***.
*/


#include<iostream>
#include<cstring>
using namespace std;

void filter(string *chat1,string bannedWord)
{
  string SS;
  //chat is original string
  //bannedWord is "want be replced words"
  //ss is instead string
  SS="";
  
  unsigned int i, pos =0; 
  unsigned len=bannedWord.size();//Length of want be replaced word
  for (i=1; i<=len;i++)
  SS+="*";//replace total "*"
  
  while ((i=chat1->find(bannedWord, pos))!=string::npos) //Seaech the matched words
  {
        chat1->replace(i,len,SS);//Replace found words by "*"
        pos = i + len + 1;
  }
}

int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl  ;
    }
    return 0;
}
