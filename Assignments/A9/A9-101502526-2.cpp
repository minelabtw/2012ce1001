#include<iostream>
#include<string>
using namespace std;

void filter(string *chat ,string bannedWord )
{
    int count = 0, i = 0 , j = 0 ,a ;
    for ( ; i < (*chat).length() ; i++ )              //此迴圈為對話第一個字搜尋到最後一個
    {
        if ( (*chat).at(i) == bannedWord.at(j) )      //若其中對話的任一字符合遮蔽的第一字則進一步判斷
        {                                             //也就是計算值+1且判斷遮蔽的第二字
            count += 1 ;
            j ++ ;
        }
        else                                          //若途中不符合則遮蔽字從頭判斷且計算值歸零
        {
            count = 0;
            j = 0;
        }
        if (count == bannedWord.length())             //若計算值達到遮蔽字的字數
        {
            for ( int k = (i-(bannedWord.length())+1) , a = k; k < a+bannedWord.length() ; k++)
                (*chat).at(k) = '*';                  //則將對話的字改變為星號直到該遮蔽的字數
            count = 0;
            j = 0;
        }
    }

}

int main()
{
    string chat,bannedWord;     //丟進函式且輸出
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl  ;
    }
    return 0;
}

