#include<iostream>
using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{ //form inputArr[start] to inputArr[end] will be sorted
    int buffer = 0;
    int sortFlag = 1;//if the sort id done, the sortFlag will == 0.
    while(sortFlag != 0)
    {
        sortFlag = 0;
        for(int i = 0 ; i < end ; i++)
        {
            if(arrPtr[i] > arrPtr[i+1])
            {
                buffer = arrPtr[i];
                arrPtr[i] = arrPtr[i+1];
                arrPtr[i+1] = buffer;
                sortFlag++;
            }
        }
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for(int i = 0 ; i<n ; i++)
        {
            cin >> inputArr[i];
        }
        /////input end///////

        sort( inputArr , 0 , n );

        /////output begin/////
        for(int i = 0 ; i<n ; i++)
        {
            cout << inputArr[i] << " " ;
        }
        cout << endl;
        /////output end///////
    }
    return 0;
}
