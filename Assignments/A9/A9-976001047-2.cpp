#include<iostream>
#include<string>

using namespace std;

void filter( string *chat , string bannedWord )
{
    string a;
    a=*chat;
    int n=a.find(bannedWord);          //n is the number which find the banned word in string a.
    int m=bannedWord.length();         //m is the length of string bannedWord.
    while(n < string::npos)            //if n is not the end of string, continue.
    {
        for(int i=0;i<m;i++)
        {
            a.replace(n+i,1,"*");       //replace the nth character to "*" for m times.
        }
        *chat=a;                       //return string to chat in main.
        n=a.find(bannedWord,n++);      //find the next banned word.
    }

}
int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);             // input string chat
        getline(cin,bannedWord);       // input string bannWord
        filter(&chat,bannedWord);
        cout << chat << endl << endl  ;
    }
    return 0;
}
