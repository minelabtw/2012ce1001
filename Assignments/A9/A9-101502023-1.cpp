#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    int buffer;
    buffer = arrPtr[start];
    arrPtr[start] = arrPtr[end];
    arrPtr[end] = buffer;
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for(int i=0;i<n;i++)
            cin>>inputArr[i];
        /////input end///////

        /////output begin/////
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++)
                if(inputArr[i]<inputArr[j])
                    sort(inputArr,i,j);
        /////output end///////
        for(int i=0;i<n;i++)
            cout<<inputArr[i]<<" ";
    }
    return 0;
}
