#include<iostream>
#include<string>
using namespace std;

void filter(string *chat,string bannedWord)
{
    string tmp(bannedWord.length(),'*');
    while(chat->find(bannedWord)!=string::npos)//while bannedWord can be found
        chat->replace(chat->find(bannedWord),bannedWord.length(),tmp);//replace the part with tmp
}

int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl ;
    }
    return 0;
}
