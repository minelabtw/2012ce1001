#include<iostream>
#include<string>
using namespace std;
void filter(string *chatPtr , string bannedWord)
{
    int test=0,ban=0;
    for (int n=0 ; n<(*chatPtr).length() ; n++) //將輸入之字母一一做檢查
    {
        if ((*chatPtr).at(n)==bannedWord.at(ban)) //若字母相同則讓test加1
        {
            test++;
            ban++;
        }
        else //若字母不同則讓test歸0
        {
            test=0;
            ban=0;
        }
        if (test==bannedWord.length()) //與欲禁止的字串相同時將其轉為星號
        {
            for (int d=n-test+1 ; d<=n ; d++)
                (*chatPtr).at(d)='*';
            test=0;
            ban=0;
        }
    }
}
int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl  ;
    }

    return 0;
}
