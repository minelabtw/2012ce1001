#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{
    //form inputArr[start] to inputArr[end] will be sorted
    for(int i=0; i<end+1; i++)
        for(int j=i+1; j<end+1; j++)
            if(arrPtr[i]>arrPtr[j])
            {
                int buffer;
                buffer=arrPtr[i];
                arrPtr[i]=arrPtr[j];
                arrPtr[j]=buffer;
            }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for(int i=0; i<n; i++)
            cin>>inputArr[i];
        /////input end///////

        sort( inputArr,0,n-1 );//陣列不用&號,直接指標第一個位置
        /////output begin/////
        for(int i=0; i<n; i++)
            cout<<inputArr[i]<<" ";
        cout<<endl;
        /////output end///////
    }
    return 0;
}
