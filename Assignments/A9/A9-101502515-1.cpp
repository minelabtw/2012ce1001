#include<iostream>
using namespace std;

//泡泡排序
 void sort(int *arrPtr,int start,int end)//from low to high
{                                           //form inputArr[start] to inputArr[end] will be sorted
    int i=0,j=0;
    for(i=start;i<end;i++)
    {
        for(j=end-1;j>i;j--)
        {
            int temp;
            if(*(arrPtr+j)<*(arrPtr+j-1))
            {
                temp=*(arrPtr+j);
                *(arrPtr+j)=*(arrPtr+j-1);
                *(arrPtr+j-1)=temp;
            }
        }
    }
}
int main()
{
    int inputArr[1000],n;
    int *ptr = inputArr;//設定指標
    while(cin >> n)
    {
        for(int i = 0; i < n; i++){//輸入
        cin>>inputArr[i];
        }
        sort(ptr,0,n);//排序
        for(int i = 0; i < n; i++){//輸出
        cout<<*(ptr+i)<<" ";
        }
        cout<<endl;
    }
    return 0;
}
