#include<iostream>
#define SWAP(x,y,t)((t)=(x),(x)=(y),(y)=(t))
using namespace std;

//from low to high
//form inputArr[start] to inputArr[end] will be sorted
void BubbleSort(int *arrPtr,int start,int end)
{
    int temp=0;

    // using the Bubble Sort to sort the input datas
    for ( int i=0 ; i < end-1 ; i++ )
    {
        for ( int j=i+1 ; j < end ; j++ )
        {
            if ( arrPtr[i] > arrPtr[j] )
            {
                // swap index i&j of array arrPtr
                // which SWAP() is define at the label 2
                SWAP(arrPtr[i],arrPtr[j],temp);
            }
        }
    }
}

int main()
{
    int inputArr[1000],n;

    while(cin >> n)
    {
        /////input begin//////
        for ( int i=0 ; i < n ; i++ )
        {
            cin >> inputArr[i];
        }
        /////input end///////

        BubbleSort(inputArr,0,n);

        /////output begin/////
        for ( int i=0 ; i < n ; i++ )
        {
            cout << inputArr[i] << " ";
        }
        cout << endl;
        /////output end///////
    }

    return 0;
}
