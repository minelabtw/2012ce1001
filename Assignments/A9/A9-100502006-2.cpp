#include<iostream>
#include<string>
using namespace std;
void filter(string *chatPtr,string bWord)//function to change the banded word into '*'s.
{
    for(int i=0;i<chatPtr->length()-bWord.length()+1;i++)
    {
        bool isBandWord=true;
        for(int j=i,k=0;k<bWord.length();j++,k++)//check that if the word is matched to the banded word.
        {
            if((*chatPtr)[j]!=bWord[k])
            {
                isBandWord=false;
                break;
            }
        }
        if(isBandWord)//if it is mathed, change it into '*'s.
        {
            for(int j=i,k=0;k<bWord.length();j++,k++)
            {
                (*chatPtr)[j]='*';
            }
        }
    }
}
int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);//user input
        getline(cin,bannedWord);//input banded words
        filter(&chat,bannedWord);//filter user input with the banded words
        cout << chat << endl << endl  ;
    }
    return 0;
}
