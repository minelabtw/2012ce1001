#include<iostream>
#include<string>

using namespace std;

void filter( string *chat, string bannedWord )
{
    int i, j, k, m;
    string str1 = *chat;

    for(i=0 ; bannedWord[i] ; i++)  k = i+1;//test the length of bannedWord.

    j=0;// j : count the same word between chat and bannedWord.

    for(i=0 ; str1[i] ; i++)
    {
       if(str1[i] == bannedWord[j]) j++;
       else j=0;

       if(j==k)//the same word's number = bannedWord's number.
       {
          for(m=i ; m >= i-k+1 ; m--) str1[m]= '*';//use * to replace words.
          j=0;
       }
    }
    *chat = str1;//return *chat.
}

int main()
{
    string chat, bannedWord;

    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl;
    }
    return 0;
}
