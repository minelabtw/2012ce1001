#include<iostream>
using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{
    int sum=0;                            //form inputArr[start] to inputArr[end] will be sorted
    for (int j = start ; j <= end ; j ++ )          //每次都從第一個開始且計算次數
    {
        for (int i = start ; i <= end ; i++)        //將第一個依序比較直到最大的在後，之後倒數第二、第三以此類推
        {
            if ( arrPtr[i] > arrPtr[i+1])
            {
                sum = arrPtr[i];
                arrPtr[i] = arrPtr[i+1];
                arrPtr[i+1] = sum;
            }
        }
    }
}

int main()
{
    int inputArr[1000],n;
    while(cin >> n)                            //重複輸入
    {   /////input begin//////
        for (int i = 0; i < n; i++ )           //輸入要排序的值
            cin >> inputArr[i];
        /////input end///////
        sort( inputArr , 0 , n );              //丟進函式排值
        /////output begin/////
        for (int i = 0; i < n; i++ )           //輸出
            cout << inputArr[i] << " ";
        /////output end///////
    }
    return 0;
}
