#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
   for(int back=1;back<=end;back++)
   {
       int insert=arrPtr[back];   //store the current element you want to sort
       int changeitem=back;       //initialize the place of the current element
       while(changeitem>start && arrPtr[changeitem-1]>insert)  //search right place of the current element
       {
            arrPtr[changeitem]=arrPtr[changeitem-1];
            changeitem--;
       }
       arrPtr[changeitem]=insert;  //insert the current element into the place
   }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for(int i=0;i<n;i++)
            cin >> inputArr[i];
        /////input end///////

        sort(inputArr,0,n-1);

        /////output begin/////
        for(int i=0;i<n;i++)
            cout << inputArr[i]<<" ";
        /////output end///////
        cout<<endl;
    }
    return 0;
}
