#include <iostream>

using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    int a,b,c;

    for(a=0;a<end-1;a++)                //從第一項向後檢查
        for(b=a+1;b<end;b++)            //與第2,3...項比大小
            if(arrPtr[a]>arrPtr[b])     //前面大則交換
            {
                c=arrPtr[b];
                arrPtr[b]=arrPtr[a];
                arrPtr[a]=c;
            }
}

int main()
{
    int inputArr[1000],n,m;
    while(cin >> n)
    {
        for(m=0;m<n;m++)
        {
            cin>>inputArr[m];          //輸入陣列
        }

        sort(&inputArr[0],0,n);        //帶入函式

        for(m=0;m<n;m++)
        {
            cout<<inputArr[m]<<" ";    //輸出陣列
        }
        cout<<"\n";
    }
    return 0;
}
