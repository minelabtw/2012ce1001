#include <iostream>
#include <string>
using namespace std;

void filter(string *chatPtr,string bannedWordPtr)
{
    int m=0;
    for(int a=0;a<(*chatPtr).length();a++)
    {
        for(int b=0;b<(bannedWordPtr).length();b++)
        {//將輸入的字和要禁止的字做比較
            if((*chatPtr)[a+b]!=(bannedWordPtr)[b])
            {
                break;
            }
            m++;
            if(m==(bannedWordPtr).length())
            {//如果連續找到和bannedword相同的詞,則改變為 * 字
                for(int k=0;k<(bannedWordPtr).length();k++)
                {
                    (*chatPtr)[a+k]='*';
                }
            }

        }
        m=0;
    }
}

int main()
{
    string chat ,bannedWord;
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl  ;
    }
    return 0;
}
