#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for ( int index=0 ; index<end ; index++ )
    {
        int buffer ;
        start=0 ;

        while ( start<index )
        {
            if ( arrPtr[start] > arrPtr[index] )//change their position
            {
                buffer = arrPtr[start] ;
                arrPtr[start] = arrPtr[index]  ;
                arrPtr[index] = buffer ;
            }
            start++;
        }
    }
}
int main()
{
    int inputArr[1000], n, a ;
    while(cin >> n)
    {
        /////input begin//////
        for ( a=0 ; a<n ; a++ )
            cin >> inputArr[a] ;

        /////input end///////

        sort( inputArr,0,n );

        /////output begin/////
        for ( a=0 ; a<n ; a++ )
            cout << inputArr[a] << " " ;

        cout << endl ;
        /////output end///////
    }
    return 0;
}
