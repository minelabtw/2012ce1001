#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{
    //form inputArr[start] to inputArr[end] will be sorted

    int buffer;
    buffer = arrPtr[start];
    arrPtr[start] = arrPtr[end];
    arrPtr[end] = buffer;
}

int main()
{
    int inputArr[1000],n;
    while(cin >> n)//重複輸入
    {
        for( int i = 0; i < n; i++ )//輸入n個數字
            cin >> inputArr[i];

        for ( int i = 0; i < n; i++ )//排大小
        {
            for ( int j = i + 1; j < n; j++ )//讓第i項和+1之後的數字相比
            {
                if ( inputArr[i] > inputArr[j] )
                    sort( inputArr, i, j );//由小排到大
            }
        }

        /////output begin/////
        for ( int i = 0; i < n; i++ )//輸出由小排到大的n個數字
            cout << inputArr[i] << " ";

        cout << endl;
        /////output end///////
    }
    return 0;
}
