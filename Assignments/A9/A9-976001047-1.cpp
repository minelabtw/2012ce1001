#include<iostream>

using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{
    int a=0;
    for(int j=end;j>0;j--)              //the loop for bubble sort
    {
        for (int i=0;i<=end;i++)
        {
            if (*(arrPtr+i)>*(arrPtr+i+1))
            {
                a=*(arrPtr+i+1);
                *(arrPtr+i+1)=*(arrPtr+i);
                *(arrPtr+i)=a;
            }
        }
    }
}

int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        for (int i=0;i<n;i++)             //input an array has n element.
        {
            int a=0;
            cin >> a;
            inputArr[i]=a;
        }

        sort(&inputArr[0],1, n-1);

        for (int i=0;i<n;i++)             //out put the anwser
        {
            cout << inputArr[i]<<" ";

        }
        cout <<endl;
    }
    return 0;
}
