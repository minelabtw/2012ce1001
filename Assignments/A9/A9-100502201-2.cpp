//A9-100502201-2
//Use filter to hide unappropriate words in chat room
//Since "chat" is type of string, just set "chat" as an address and use pointer to change its value  through address
#include<iostream>
#include<string>
using namespace std;
void filter( string *chat , string bannedWord )
{
    int clength=(*chat).length();
    int blength=bannedWord.length();
    int i=0,num=0; //num is a number to count how many consecutive bannedWord are being detected
    while ((*chat)[i]!='\0')
    {
        int j=0; //Every time we end the inner loop, return j's value to 0 to compare bannedWord again
        while (bannedWord[j]==(*chat)[i])
        {
            num++;
            if(bannedWord[j+1]=='\0'&&num==blength) //If bannedWord array is null and ensure the same words in *chat, change the word to stars
                for(int k=i-blength+1;k<=i;k++)
                    (*chat)[k]='*';
            j++;
            i++;
        }
        num=0; //If the comparison fail, reinitialize num
        i++;
    }
}
int main()
{
    string chat,bannedWord;
    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);
        cout << chat << endl << endl  ;
    }
    return 0;
}
