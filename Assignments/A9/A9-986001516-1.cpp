#include <iostream>
#include <cstdlib>//head files
using namespace std;

void sort(int *arrPtr,int start,int end)//function used to arrange the input array from min to max
{
    int temp=0;
    for (int i=0;i<=end;i++)
    {
        for (int j=end;j>=i+1;j--)//this double loop will check from the last elementin the array
        {
            if  (*(arrPtr+j)<*(arrPtr+j-1))
            {
                temp=*(arrPtr+j);
                *(arrPtr+j)=*(arrPtr+j-1);
                *(arrPtr+j-1)=temp;
            }
        }
    }
}

int main()
{
    int inputArr[1000],n;
    while (cin>>n)
    {
        for (int j=0;j<n;j++)//for users to input all the element of the array
        {
            cin >> inputArr[j];
        }
        sort(inputArr,0,n-1);//call the function to arrange
        for (int j=0;j<n;j++)//display the result
        {
            cout << inputArr[j] << " ";
        }
        cout << endl;
    }
    return 0;
}
