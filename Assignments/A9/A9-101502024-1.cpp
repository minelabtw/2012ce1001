#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)
{
    int buffer;

    for (int i = 0;i <= end;i++ )//Compare the numbers  in the array one by one.
    {
        for(int j = i+1;j <= end ;j++)
        {
            if ( arrPtr[i]>arrPtr[j] )//To change the sort of  two numbers.
            {
                buffer = arrPtr[i];
                arrPtr[i] = arrPtr[j];
                arrPtr[j] = buffer;
            }
        }
    }
}
int main()
{
    int inputArr[1000],n;

    while(cin >> n)
    {
        for (int i = 0;i < n;i++)//Input the array.
            cin >> inputArr[i];
        sort(inputArr,0,n-1);
        for (int i = 0;i < n;i++)//Output the changed array.
            cout << inputArr[i] << " ";
    }
    return 0;
}
