#include<iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for(int t=1;t<end;t++)//總共排幾位
    {
        for(int j=0;j<end;j++)//比大小
        {
            if(arrPtr[j]>arrPtr[j+1])//把較大之數字調到後面
            {
                int buffer=arrPtr[j];
                arrPtr[j]=arrPtr[j+1];
                arrPtr[j+1]=buffer;
            }
        }
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        for(int i=0;i<n;i++)
        {
            cin>>inputArr[i];
        }
        /////input end///////
        sort( inputArr , 0 , n );

        /////output begin/////
        for(int i=0;i<n;i++)
        {
            cout<<inputArr[i]<<" ";
        }
        /////output end///////
    }
    return 0;
}
