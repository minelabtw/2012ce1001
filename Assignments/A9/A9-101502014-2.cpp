#include<iostream>
#include<string>
using namespace std;
void filter( string *chatPtr , string *bannedWord )
{
    int ban = 0; //ban用來判斷是否有要遮蔽的字眼
    for(int i = 0 ; i < (*chatPtr).length() ; i++)
    {
        if((*chatPtr)[i] == (*bannedWord)[0]) //若發言裡的某個字元和遮蔽字的第一個字元一樣的話 , 則判斷是否要遮蔽
        {
            ban = 1; //先假設要遮蔽的話 , ban值等於1
            for(int j = 0 ; j < (*bannedWord).length() ; j++)
            {
                if((*chatPtr)[i + j] != (*bannedWord)[j]) //若後來的字元與遮蔽字不同 , 則不需遮蔽 , ban值改為1 , 並跳出迴圈
                {
                    ban = 0;
                    break;
                }

            }
        }
        if(ban) //若確定要遮蔽的話 , 則把發言中的遮蔽字改為星號 , ban值初始為0
        {
            for(int k = i ; k < i + (*bannedWord).length() ; k++)
                (*chatPtr)[k] = '*';
            ban = 0;
        }
    }
}
int main()
{
    string chat , bannedWord;
    while(true)
    {
        getline(cin , chat);
        getline(cin , bannedWord);
        filter(&chat , &bannedWord);
        cout << chat << endl << endl;
    }
    return 0;
}
