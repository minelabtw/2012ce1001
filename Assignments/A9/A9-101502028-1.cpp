#include <iostream>
using namespace std;
void sort(int *arrPtr,int start,int end)// from low to high
{                                       // form inputArr[start] to inputArr[end] will be sorted
    for (int nextnum = 1; nextnum < start; nextnum ++) // for loop to take the next number
    {
        end = arrPtr[nextnum]; // sort the taken number
        int movenum = nextnum; // set the taken number to move
        while ((movenum > 0) && (arrPtr[movenum - 1] > end)) // move the taken number comparing it with its rights number
        {
            arrPtr[movenum] = arrPtr[movenum - 1];
            movenum --;
        }
        arrPtr[movenum] = end; // put the taken number on its space
    }
}

int main ()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        /////input begin//////
        int space;
        for (int i = 0; i < n; i++) // input array number
        {
            cin >> inputArr[i];
        }
        /////input end///////

        sort(inputArr, n, space); // apply the function

        /////output begin/////
        for (int i = 0; i < n; i++)
        {
            cout << inputArr[i] << " "; // output the sorted number
        }
        cout << endl << endl;
        /////output end///////
    }
    return 0;
}
