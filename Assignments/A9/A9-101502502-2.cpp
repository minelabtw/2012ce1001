#include<iostream>
#include<string>

using namespace std;

void filter( string *p , string b )//將p裡的字串b用'*'替換掉
{
    size_t found;

    found = p->find(b);//在p裡面找字串b的position

    while(found != string::npos)
    {
        p->replace( found , b.length() , b.length() , '*' );//將p裡面的字串b以'*'代替
        found = p->find(b, found + b.length());//從剛剛找到的字串b後繼續尋找有無字串b的position
    }

}

int main()
{
    string chat,bannedWord;

    while(true)
    {
        getline(cin, chat);
        getline(cin, bannedWord);//輸入要遮蔽的字串

        filter(&chat, bannedWord);

        cout << chat << endl << endl ;
    }

    return 0;
}
