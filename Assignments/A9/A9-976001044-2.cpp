#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

void filter(string *OutputString, string bannedWord)
{
    int len=bannedWord.length(), index=0;
    string Block(len,'*');

    // get the index of bannedWord in the string
    index = (*OutputString).find(bannedWord);

    if ( index != string::npos )
    {
        // Block the bannedWord
        (*OutputString).replace(index,len,Block);

        // check if there is more bannedWord in the string
        while ( true )
        {
            // get the index again, if there are more bannedWord
            index = (*OutputString).find(bannedWord,index+len);
            if ( index == string::npos )
            {
                return;
            }
            else
            {
                // Block the bannedWord
                (*OutputString).replace(index,len,Block);
            }
        }
    }
    else
    {
        return;
    }
}

int main()
{
    string chat,bannedWord;

    while(true)
    {
        getline(cin,chat);
        getline(cin,bannedWord);
        filter(&chat,bannedWord);

        cout << chat << endl << endl  ;
    }

    return 0;
}

// Test data:
// what the fuck ffuck fuckk fffuckkk fuckfuckfuck fcuk is going on!!
// Block : fuck
