#include<iostream>

using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    for(start;start<end;start++) // let the order be correct
    {
        for(int j=start+1;j<end;j++) // arrange the order
        {
            int buffer;
            if(arrPtr[start]>arrPtr[j])
            {
                buffer=arrPtr[start];
                arrPtr[start]=arrPtr[j];
                arrPtr[j]=buffer;
            }
        }
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n) // set the number of the integers
    {
        for(int d=0;d<n;d++)
        {
            cin>>inputArr[d]; // input the integers
        }
        sort(inputArr,0,n);
        for(int b=0;b<n;b++) // output the integers in orders
        {
            cout<<inputArr[b]<<" ";
        }
        cout<<endl;
    }
    return 0;
}
