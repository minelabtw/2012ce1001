#include<iostream>
#define SIZE 256
using namespace std;

void Swap(int *tempPtr1, int *tempPtr2)
{
    int temp;
    temp = *tempPtr1;
    *tempPtr1 = *tempPtr2;
    *tempPtr2 = temp;
}

int Partition(int *arrPtr, int start, int end)
{
    int temp;
    int i;
    int j;
    temp = arrPtr[end];
    i = start - 1;
    for(j = start; j <= end - 1; j++)
    {
        if(arrPtr[j] <= temp)
        {
            i++;
            Swap(&arrPtr[i], &arrPtr[j]);
        }
    }
    Swap(&arrPtr[i + 1], &arrPtr[end]);
    return i + 1;
}

void sort(int *arrPtr,int start,int end)//from low to high
{                                       //form inputArr[start] to inputArr[end] will be sorted
    int mid;
    if(start < end)
    {
        mid = Partition(arrPtr, start, end);
        sort(arrPtr, start, mid - 1);
        sort(arrPtr, mid + 1, end);
    }
}
int main()
{
    int inputArr[1000],n;
    while(cin >> n)
    {
        int i, num[SIZE];
        char buf[SIZE*4], *ptr;

        fgets(buf, SIZE, stdin);

        ptr = buf;
        for (n=0; n<SIZE; n++)
        {
            num[n] = strtol(ptr, &ptr, 10);
            if (*ptr == '\n')
            {
                if (n)
                    n++;
                    break;
            }
        }
        sort( num, 0 , n-1 );

        for ( i=0; i<n; i++)
        cout << num[i] << " ";

        return 0;
    }
    return 0;
}
