#include <iomanip>
#include<iostream>
using namespace std;

void sort(int *arrPtr,int start,int end)//from low to high
{ //form inputArr[start] to inputArr[end] will be sorted
    int buffer=0;
    for( int i=start; i<=end; i++ )
    {
        for( int j=start; j<=end; j++ )//start to end
            if ( arrPtr[j] > arrPtr[i] )//compare fome 0 to n-1
            {
                buffer=arrPtr[j];
                arrPtr[j]=arrPtr[i];
                arrPtr[i]=buffer;
            }
    }
}

int main()
{
    int inputArr[1000],n;
    while( cin>>n )//user input
    {
        int input;
        for ( int i=0; i<n; i++)//initiable by user
        {
            cin >> input;
            inputArr[i] = input;
        }

        sort( inputArr, 0, n-1 );

        for( int i=0;i<n; i++ )//output the answer
            cout << inputArr[i] << " ";
        cout << endl;
    }
    return 0;
}
