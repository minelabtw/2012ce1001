#include<iostream>
#include<fstream>
#include<cstdlib>
using namespace std;
class MRTJudgeSystem
{
public:
    MRTJudgeSystem(const char*);
    int judge();
private:
    int first[100000],least[100000];
    int n;
};
MRTJudgeSystem::MRTJudgeSystem(const char *FileName)
{
    n=0;
    ifstream inRecordFile(FileName,ios::in);
    if(!inRecordFile)
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    int First;
    int Least;
    while(inRecordFile>>First>>Least)//將record.txt內的資料傳入
    {
        first[n]=First;
        least[n]=Least;
        n++;
    }
}

int MRTJudgeSystem::judge()
{
    int buffer;
    int x=0,max=0;
    for(int i=0;i<=86400;i++)//從1秒算到86400秒
    {
        for(int j=0;j<n;j++)
        {
            if(first[j]==i)//計算該秒進站人數
                x++;
            if(least[j]==i)//計算該秒出站人數
                x--;
            if(x>max)//求最大值
                max=x;
        }
    }
    return max;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
