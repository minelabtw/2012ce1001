#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;

class MRTJudgeSystem
{
private:
    fstream file;

public:

    MRTJudgeSystem(string file_name)//Ū��
    {
        file.open(file_name.c_str(), ios::in);
        if(!file)//�Y�ɮ�Ū���X��
        {
            cerr << "File could not be opened." << endl;
            exit(1);
        }
    }
    int judge()
    {
        int peoplein,peopleout,people[86400];
        int n=0;
        int peoplemax=0;

        if (file.is_open())
        {
            for(int i=0;i<86400;i++ )
            {
                people[i]=0;
            }
            while(file>>peoplein>>peopleout)
            {
                people[peoplein]++;
                people[peopleout]--;
            }
            for(int j=0;j<86400;j++)
            {
                n=n+people[j];
                if(n>peoplemax )
                {
                peoplemax=n;
                }
            }
        }
        return peoplemax;
    }

};

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
