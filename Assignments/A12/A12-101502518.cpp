#include<iostream>
#include<string>
#include<math.h>
#include<fstream>
#include<cstdlib>
#include<vector>
#include<algorithm>

using namespace std;

class MRTJudgeSystem
{
    public:
        MRTJudgeSystem(string);
        int judge();
    private:
        vector<int>time;
        int in,out;
};

 MRTJudgeSystem::MRTJudgeSystem(string a)
{
    ifstream account(a.c_str(),ios::in);

    while(account>>in>>out)
    {
        time.push_back (in);
        time.push_back (-out);
    }
}

bool cmp(const int& i, const int& j)//比較絕對值
{
    return abs(i) < abs(j);
}

int MRTJudgeSystem::judge()
{

    sort(time.begin(),time.end(),cmp);

    int n=0,n_max=0;
    for(int i=0;i<time.size();++i)//進則+1;出則-1
    {
        if (time[i]>=0)
            n++;
        else
            n--;

        n_max = max(n_max,n);
    }
    return n_max;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
