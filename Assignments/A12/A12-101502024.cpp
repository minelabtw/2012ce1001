#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;

class MRTJudgeSystem
{
    private:
        vector<int>a;
        vector<int>b;
        int max;
        int y;
        int x;
        int p;
    public:
        MRTJudgeSystem(string);
        int judge();
};
int MRTJudgeSystem::judge()//return the biggest number.
{
    return max;
}
MRTJudgeSystem::MRTJudgeSystem(string h)
{
    ifstream in(h.c_str(),ios::in);//read the file.
    while(in>>x>>y)
    {
        a.push_back(x);
        b.push_back(y);
    }
    for(int i=0;i<a.size();i++)//sort the file.
    {
       for(int j=0;j<a.size()-1;j++)
       {
           if(a[i]>a[j])
           {
               p=a[i];//to change the position.
               a[i]=a[j];
               a[j]=p;
           }
           if(b[i]>b[j])
           {
               p=b[i];
               b[i]=b[j];
               b[j]=p;
           }
       }
    }
    max=0;//set max to zero.
    x=0;
    for(int i=0;i<=86400;i++)
    {
        for(int j=0;j<a.size();j++)//count the biggest number.
        {
            if(a[j]==i)
            {
                x++;
            }
            if(b[j]==i)
            {
                x--;
            }
            if(x>max)
            {
                max=x;//make sure the program has the maxium number.
            }
        }
    }
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
