#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;
class MRTJudgeSystem
{
    public:
    MRTJudgeSystem(string);
    int judge();
    private:
    string address;
    int max,in,leave;
    vector<int>invector;
    vector<int>leavevector;
};
MRTJudgeSystem::MRTJudgeSystem(string address)//儲存檔案之進出入時間
{
    ifstream inclientfile(address.c_str(),ios::in);
    while(inclientfile>>in>>leave)
    {
        invector.push_back(in);
        leavevector.push_back(leave);
    }
}
int MRTJudgeSystem::judge()
{
    int mantimes,max=0;//mantimes是有多少人
    vector<int>mantimesvector;//用來儲存個時間各有多少人
    for(int timer=0;timer<86400;timer++)
    {//時間的for
        mantimes=0;
        for(int vectortimer=0;vectortimer<invector.size();vectortimer++)
        {//各個人進出入的時間
            if(invector[vectortimer]<=timer && leavevector[vectortimer]>=timer)
            {
                mantimes++;
            }
        }
        mantimesvector.push_back(mantimes);
    }
    for(int count=0;count<86400;count++)
    {//找最大
        if(max<mantimesvector[count])
        {
            max=mantimesvector[count];
        }
    }
    return max;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
