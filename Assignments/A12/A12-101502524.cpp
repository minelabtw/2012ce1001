#include<iostream>
#include<iomanip>
#include<fstream>
#include<string>
#include<cmath>
#include<cstdlib>
using namespace std;

class MRTJudgeSystem
{
    public:
        MRTJudgeSystem(char []);
        int judge();
    private:
        int x,k;
        int q[110000];

};

MRTJudgeSystem::MRTJudgeSystem(char a[])
{
    x=0,k=0;
    ifstream time(a,ios::in);
    if(!time)
    {
        cerr << "error";
        exit(1);
    }
    while(time>>q[x])//讀取
    {
        x++;//記錄X
    }
    time.close();
}

int MRTJudgeSystem::judge()
{
    int save=0;
    for(int a=1;a<x;a+=2)//將奇數項轉為負值
    {
        q[a]=-1 * q[a];
    }
    for(int a=0;a<x-1;a++)//排序
    {
        for(int b=a+1;b<x;b++)
        {
            int buffer;
            if(abs(q[a])>abs(q[b]))
            {
                buffer=q[a];
                q[a]=q[b];
                q[b]=buffer;
            }
        }
    }
    for(int a=0;a<x;a++)//計算最大人數
    {
        if(q[a]>=0)
        {
            k = k + 1;
        }
        else
        {
            k = k - 1;
        }
        if(k>save)
        {
            save=k;
        }
    }
    return save;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
