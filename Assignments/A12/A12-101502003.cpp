#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string enter)
    {
        array3[86401]={};
        ifstream peoplefile(enter.c_str(),ios::in);
        if( !peoplefile )//讀檔如果沒讀到
        {
            cerr << "Saving Error!!" << endl;
            exit(1);
        }
        int i=0;
        while (peoplefile >> instation >> outstation)
        {//把進站時間存入array,出站時間存入array2
            array[i]=instation;
            array2[i]=outstation;
            i++;
        }
    }
    int judge()
    {
        int max=0;
        for (int k=0;k<100000;k++)
        {//計算每個人進入的時間,有一人進入array3[時間]就+1,來算時間點有幾個人在裡面
            for(int j=array[k];j<array2[k];j++)
            {
                array3[k]++;
            }
        }
        for (int j=0;j<86401;j++)
        {
            if(max<array[j])//求最大值
            {
                max=array[j];
            }
        }
        return max;
    }
private:
    int array[100000],array2[100000],array3[86401];
    int instation,outstation;
    string peoplefile;


};
int main()
{
    MRTJudgeSystem MRTJudgeSys("record5.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
