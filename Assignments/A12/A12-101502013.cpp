#include <iostream>
#include <string.h>
#include <fstream>
#include <stdlib.h>
#include <algorithm>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(char*);
    int judge();
private:
    int size,buffer,counter,answer;
    char *filename;
};
MRTJudgeSystem::MRTJudgeSystem(char *input_filename)
{
    filename=input_filename;
    ifstream inrecord(filename,ios::in);
    if (!inrecord )
    {
        cout << "file can't be opened." << endl;
        exit(-1);
    }
}
int MRTJudgeSystem::judge()
{
    size=0;
    ifstream inrecord(filename,ios::in);
    while (inrecord >> buffer >> buffer)//先取得資料數 (一行為一筆資料)
        size++;
    int time_in[size],time_out[size];
    inrecord.clear();
    inrecord.seekg(0,ios::beg); //再從頭讀取
    for (int i=0;i<size;i++)
        inrecord >> time_in[i] >> time_out[i];
    answer=0,buffer=0,counter=0;
    sort(time_in,time_in+size);
    sort(time_out,time_out+size);
    for (int i=1;i<size;i++)//時間點由第二個進站時間開始逐次跳到下一個進站時間 因為第一個進站時間不可能有人出站
    {
        //如果出站時間剛好在兩次進站時間內 則加入出站人數 buffer記錄出站人數
        for (int j=buffer;time_out[j]<time_in[i];j++)
            buffer++;
        if (answer<(i+1-buffer))//出站人數-進站人數為答案
            answer=(i+1-buffer);
    }

    return answer;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
