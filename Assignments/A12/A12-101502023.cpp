#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;

class MRTJudgeSystem
{
public:
        MRTJudgeSystem(string);
        int judge();

private:
        vector<int> a;
        vector<int> b;
        int x;
        int y;
        int z;
        int p;
};
MRTJudgeSystem::MRTJudgeSystem(string h)
{
    ifstream in(h.c_str(),ios::in);//read the file information
    while(in>>x>>y)
    {
        a.push_back(x);
        b.push_back(y);
    }
    for(int i=0;i<a.size();i++)//compare the sort
    {
       for(int j=0;j<a.size()-1;j++)
       {
           if(a[i]>a[j])
           {
               p=a[i];
               a[i]=a[j];
               a[j]=p;
           }
           if(b[i]>b[j])
           {
               p=b[i];
               b[i]=b[j];
               b[j]=p;
           }
       }
    }
    x=0;
    z=0;

    for(int i=0;i<=86400;i++)
    {
        for(int j=0;j<a.size();j++)//calculate the maximum number
        {
            if(a[j]==i)
            {
                x++;
            }
            if(b[j]==i)
            {
                x--;
            }
            if(x>z)
            {
                z=x;
            }
        }
    }
}

int MRTJudgeSystem::judge()//return the maximum number
{
    return z;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
