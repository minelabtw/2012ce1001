#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;

class MRTJudgeSystem
{
    public:
        MRTJudgeSystem(string );
        int judge();
    private:
        int count;//用來計算有幾行(幾個人進出站)
        int in,out;//進站,出站
        int inArr[60000],outArr[60000],sameTime[86401];//進站的陣列,出站的陣列,第幾秒的陣列
};

MRTJudgeSystem::MRTJudgeSystem( string filename )
{
    for(int i = 0; i < 86401; i++)//讓sameTime裡的每一項為0
    {
        sameTime[i]=0;
    }

    ifstream inRecordFile( filename.c_str(), ios::in );//讀檔
    if ( !inRecordFile )//偵錯
    {
        cerr << "File could not be opened" << endl;
        exit( 1 );
    }

    count = 0;//用來計算是第幾個陣列
    while (inRecordFile >> in >> out)//把進站和出站分別存進陣列中
    {
        inArr[count] = in;
        outArr[count] = out;
        count++;
    }
}
int MRTJudgeSystem::judge()
{
    int big = 0;//設最大值為0(排大小用)

    for ( int i=0; i<count; i++)
        for ( int j = inArr[i]; j <= outArr[i]; j++ )//讓那個人在站裡的每一個秒數(sameTime)都加1
            sameTime[j]++;

    for ( int i=0; i<86401; i++)
        if (sameTime[i]>big)//看哪個時間人最多
            big=sameTime[i];

    return big;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;

    return 0;
}
