#include<iostream>
#include <fstream>
#include <cstdlib>
#include<string>
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);//建構子
    int judge();
    ofstream outrecordFile;//存資料
    ifstream inrecordFile;//讀資料
private:
    int n;
    int a,b;
};

MRTJudgeSystem::MRTJudgeSystem(string a)
{
    n=0;
    outrecordFile.open("record.txt",ios::app);

    if(!outrecordFile)//偵錯
    {
        cerr<<"file could not be found"<<endl;
        exit(1);
    }
    inrecordFile.open("record.txt",ios::in);

    if(!inrecordFile)//偵錯
    {
        cerr<<"file could not be found"<<endl;
        exit(1);
    }
}

int MRTJudgeSystem::judge()
{

    int w[86401]={0};

    inrecordFile.clear();//清除
    inrecordFile.seekg(0);//重頭讀
    int n=0,nm=0;

    while(inrecordFile>>a>>b)
    {
        w[a]++;//用矩陣將讀到進入的資料，並增加
        w[b]--;//離開的資料並減
    }

    for(int m=0;m<86400;m++)
    {
        n=n+w[m];//將資料從0開始跑，看哪一個時間點的人最多
        if(n>nm)
        {
            nm=n;
        }
    }
    return nm;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
