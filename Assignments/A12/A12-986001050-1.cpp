#include<iostream>
#include<string>
#include<fstream>
#include<cmath>
#include <sstream>
using namespace std;

class MRTJudgeSystem
{
private:
    int passenger_max;
    fstream filein;
public:
    MRTJudgeSystem(string filename)
    {
        filein.open(filename.c_str(), ios::in);
    }

    int judge()
    {
        char *buffer = new char[12];//最多8+8+2=12位數
        int intotime,exittime;
        int passenger=0;
        int time[86401]={0};//每一秒一格
        while ( filein.getline(buffer,12) ) //讀出file裡之資料，一次一行，輸至buffer中
            {
                stringstream ss(buffer);//將buffer，輸進stringstream ss中
                ss >> intotime;         //再將ss輸出至int intotime
                ss >> exittime;         //再將ss輸出至int exittime
                time[intotime]++;       //進站加一
                time[exittime]--;       //出站減一
                for (int i=0; i<=86400; i++)    //矩陣相加，找出最大值
                {
                    passenger+=time[i];
                    passenger_max=max(passenger_max, passenger);
                }
            }
        filein.close();
        return passenger_max;
    }
};
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
