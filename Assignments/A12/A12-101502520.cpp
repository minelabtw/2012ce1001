#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cmath>
using namespace std;
bool cmp(int i,int j)
    {
        return abs(i) < abs(j);
    }
class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string target)
    {
        fstream loadfile(target.c_str(),ios::in);
        while(loadfile>>tempin>>tempout)
        {
            timestream.push_back(+tempin);
            timestream.push_back(-tempout);
        }
        sort(timestream.begin(),timestream.end(),cmp);

    }
    int judge()
    {
        for(int i=0;i<timestream.size();i++)
        {
            if (timestream[i] >= 0)
                people++;
            else
                people--;
            //cout<<timestream[i]<<endl;
            maxpeople = max(maxpeople, people);
        }
        return maxpeople;
    }
private:
    vector<int> timestream;
    int tempin,tempout,people=0,maxpeople=0;
};
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
