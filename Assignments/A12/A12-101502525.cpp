#include<iostream>
#include<fstream>
#include<vector>
#include<cstdlib>
using namespace std;


class MRTJudgeSystem
{
    public:
        MRTJudgeSystem(const char *filename)
        {
            ifstream in(filename);
            if(!in)
            {
                cout<<"Open file error!";
                exit(-1);
            }
            while(in>>tmp.in>>tmp.out)//read file
                passenger.push_back(tmp);

            for(int i=0;i<86401;i++)//initialize
                time[i]=0;

            for(int i=0;i<passenger.size();i++)//sort
            {
                time[passenger[i].in]++;
                time[passenger[i].out+1]--;
            }
        }
        int judge()
        {
            int sum=0,max=0;
            for(int i=0;i<86400;i++)//find max
            {
                sum+=time[i];
                if(sum>max)
                    max=sum;
            }
            return max;
        }
    private:
        struct PASSENGER
        {
            int in;
            int out;
        }tmp;
        vector<PASSENGER> passenger;
        int time[86401];
};


int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
