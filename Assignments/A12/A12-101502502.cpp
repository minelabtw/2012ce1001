#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();

private:
    int mCount;
    int mEnterArray[50000];
    int mOutArray[50000];
    int mBothArray[50000];
    int InOrOut[50000];
};

MRTJudgeSystem::MRTJudgeSystem(string filename)
{
    mCount = 0;
    int enter;
    int out;

    ifstream inClientFile(filename.data() , ios::in);

    while(!inClientFile.eof())
    {
        inClientFile >> enter >> out;//讀檔

        mEnterArray[mCount] = enter;//把進站資料存入mEnterArray裡
        mOutArray[mCount] = out;//把出站資料存入mOutArray裡

        mCount++;
    }
}

int MRTJudgeSystem::judge()
{
    int buffer;
    int count = 0;
    int MaxCount = 0;

    for(int i = 0 ; i < (mCount * 2 - 1) ; i++)//把所有的時間點存到mBothArray裡，再把進、出站資料對應到InOrOut陣列裡
    {
        if(i < mCount)
        {
            mBothArray[i] = mEnterArray[i];

            InOrOut[i] = 1;//如果是進站為1
        }

        else
        {
            mBothArray[i] = mOutArray[i-mCount];

            InOrOut[i] = -1;//如果是出站則為-1
        }
    }

    for(int i = 0 ; i < (mCount * 2 - 1) ; i++)//排順序，包含代表進出站的1和-1
    {
        for(int j = i + 1 ; j < (mCount * 2 - 1) ; j++)
        {
            if(mBothArray[i] > mBothArray[j])
            {
                buffer = mBothArray[j];
                mBothArray[j] = mBothArray[i];
                mBothArray[i] = buffer;

                buffer = InOrOut[i];
                InOrOut[i] = InOrOut[j];
                InOrOut[j] = buffer;
            }
        }
    }

    for(int i = 0 ; i < (mCount * 2 - 1) ; i++)
    {
        if(InOrOut[i] == 1)//進站+1
        {
            count = count + 1;
        }

        else if(InOrOut[i] == -1)//出站-1
        {
            count = count - 1;
        }

        if(count > MaxCount)//儲存最大值
            MaxCount = count;
    }

    return MaxCount - 1;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
