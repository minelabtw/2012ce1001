#include<iostream>
#include<fstream>
#include<string>
#include<cstdlib>
using namespace std;
class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    fstream record;
};
MRTJudgeSystem::MRTJudgeSystem(string file)
{
    record.open(file.c_str(), ios::in);
    if( !record )//無法開啟
        {
            cerr << "File could not be opened" << endl;
            exit(1);
        }
}
int MRTJudgeSystem::judge()
{
    int filearray[86401]={},enter,leave,max = 0;

    while(record >> enter >> leave)
    {
        for(int i = enter ; i <= leave ; i++)
            filearray[i]++;
    }

    for(int j = 0 ; j < 86401 ; j++)
    {
        if(filearray[j] > max)
            max = filearray[j];
    }
    return max;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
