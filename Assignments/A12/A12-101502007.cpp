#include <iostream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <algorithm>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    int x;
    int y;
    int z;
    vector<int>a;
    vector<int>b;
};
MRTJudgeSystem::MRTJudgeSystem(string s)
{
    ifstream in(s.c_str(),ios::in);
    while (in>>x>>y)
    {
        a.push_back(x);
        b.push_back(y);
    }
    sort (a.begin(),a.end());
    sort (b.begin(),b.end());
    z=0;
    int u=0,v=0,c=0;
    for (int g=0;g<=86400;g++)
    {
        if (a[u]==g) //有人進站總數加一
        {
            u++;
            c++;
        }
        if (b[v]==g) //有人出站總數減一
        {
            v++;
            c--;
        }
        if (c>z)
        z=c;
    }
}

int MRTJudgeSystem::judge()
{
    return z;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
