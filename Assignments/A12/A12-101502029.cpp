#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;

class MRTJudgeSystem
{
    private:
    fstream file;
    int intime,outtime;
    int time[86401];

    public:
    MRTJudgeSystem (string file_name)
    {
        for(int i=0; i<=86400; i++)

            time[i]=0;
            MRTJudgeSys(file_name);
    }
    void MRTJudgeSys(string file_name)
        {
            file.open(file_name.c_str(), ios::in );
            if( !file )
            {
                cerr << "File could not be opened" << endl;
                exit(1);
            }
        }
    int judge()
    {
        int sum=0,max_sum=0;
        if(file.is_open())//打開資料檔
        {
            file.seekg(0, ios::beg);
            while (file >> intime >> outtime)//將資料檔裡的數據讀出來
            {
                    time[intime]++;//若數據為進入時間，人數加一
                    time[outtime]--;//若數據為離開時間，人數減一
            }
        }
        for(int i=0; i<=86400; i++)
        {
            sum=sum+time[i];//將進入與離開的人數總和
            if(sum>max_sum)//比較最後總人數
            {
                max_sum=sum;
            }
        }
        return max_sum;
    }
};
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
