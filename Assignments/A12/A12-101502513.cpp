#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem( string );
    int judge();
private:
    int x;
    int arrive, leave;
    int ArriveTime[100000];
    int LeaveTime[100000];
};

MRTJudgeSystem::MRTJudgeSystem( string file )
{
    ifstream RecordFile( file.c_str(), ios::in );

    if( !RecordFile )
    {
        cerr << "File could not be opened." << endl;
        exit( 1 );
    }

    x = 0;

    while( RecordFile >> arrive >> leave )
    {
        ArriveTime[x] = arrive;
        LeaveTime[x] = leave;
        x++;
    }
}

int MRTJudgeSystem::judge()
{
    int passenger = 0, max_passenger = 0;

    for( int i = 0; i <= 86400; i++ ) //there are 86400 seconds in one day
    {
        for( int j = 0; j < x; j++ ) //there are x data
        {
            //If someone enters the MRT station, the numbers of passengers increase.
            if( i == ArriveTime[j] )
                passenger++;
            //If someone exits the MRT station, the numbers of passengers decrease.
            if( i == LeaveTime[j] )
                passenger--;
        }
        if( passenger >= max_passenger ) //record the max numbers of passengers
            max_passenger = passenger;
    }

    return max_passenger;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
