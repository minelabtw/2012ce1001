#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string) ;
    int judge();
private:
    ifstream inaccountFile;
};

MRTJudgeSystem::MRTJudgeSystem(string record)
{
    inaccountFile.open(record.c_str() , ios::in);//開啟檔案
    if(!inaccountFile)
    {
        cerr <<"File could not be opened" << endl;
        exit(1);
    }
}

int MRTJudgeSystem::judge()
{
    int people[86401]={};//宣告86401個陣列且全都是0
    int arrival,leave;

    while(inaccountFile >> arrival >> leave)//讀檔案
    {
        for(int k=arrival;k<=leave;k++)//在進站時間內的陣列全部加1
        {
                people[k]++;
        }

    }
    int max=people[0];//先把max的初始值設為第0秒的人數
    for(int i=0;i<=86400;i++)
    {
       if(people[i]>max)//若大於max就取代他
           max=people[i];
    }
    return max;//回傳max

}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
