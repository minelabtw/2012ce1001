#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
using namespace std;
class MRTJudgeSystem
{
    public:
        MRTJudgeSystem(string);
        int judge();
    private:
        int arrive[100000],leave[100000],i;
};
MRTJudgeSystem::MRTJudgeSystem(string filename)
{
    i=0;
    ifstream File ( filename.c_str() ,ios::in );
    if (!File) //如果無法打開txt檔的錯誤訊息
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }
    while(!File.eof())
    {
        File >> arrive[i] >> leave[i];
        i++;
    }
    i=i-1; //計算總長度
}
int MRTJudgeSystem::judge()
{
    int truemax=0,max=0,time,maxtime=0;
    for (int k=0;k<=i;k++)
    {
        if (maxtime<=leave[k])
            maxtime=leave[k];//找出最後離站的時間 減少程式計算時間
    }
    for (time=0;time<=maxtime;time++)
    {
        for (int j=0;j<=i;j++)
        {
            if (time==arrive[j])
                max++;
            if (time>leave[j])
            {
                leave[j]=86400;//使離站時間不會重複減到max
                max--;
            }
        }
        if (truemax<max)
            truemax=max;
    }
    return truemax;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
