#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    int array[86401];
};

MRTJudgeSystem::MRTJudgeSystem(string str)
{
    ifstream file(str.c_str(),ios::in);
    int a,b;

    array = {};

    while( file >> a >> b )
    {
        for ( int i=a ; i<=b ; i++)
        {
            array[i]++;
        }
    }
}

int MRTJudgeSystem::judge()
{
    int max=0;

    for (int i=0 ; i<=86400 ; i++)
    {
        if ( max < array[i])
            max = array[i];
    }

    return max;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
