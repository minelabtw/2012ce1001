#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <algorithm>
//需要等一段時間~~need some time to wait.
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    int arr[50000];
    int lea[50000];
    int arrive;
    int leave;
    ifstream record;
};
MRTJudgeSystem::MRTJudgeSystem(string)
{
    record.open("record.txt",ios::in);

    if( !record ) //fix error
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }
    record.close();
}

int MRTJudgeSystem::judge()
{
    int n = 0, max_n =0;
    record.open("record.txt",ios::in);

    for(int i=0;i<50000;i++)
    {
        record >> arrive>> leave;
        if(!record)
        {
            arr[i]=0;
            lea[i]=0;
        }
        else
        {
            arr[i]=arrive;
            lea[i]=leave;
        }
    }

    sort(arr,arr+50000);
    sort(lea,lea+50000);

    for (int time=1; time<=86400; time++)
    {
        for(int i=0;i<50000;i++)
        {
            if(arr[i]<=time)//不能打arr[i]>0，會有錯誤答案
                n++;

            if(lea[i]<time)//不能打lea[i]>0，會有錯誤答案
                n--;

        }
        //cout << n <<endl;
        if(n>max_n)
        {
            max_n = n;
        }
       // max_n = max(max_n, n);

        n=0;
    }
    return max_n ;

}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
