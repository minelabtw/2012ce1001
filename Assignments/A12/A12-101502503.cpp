#include<iostream>
#include<cstdlib>
#include<fstream>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();

private:
    int arrayarrival[60000];//陣列 存乘客的進站時間
    int arrayleave[60000];//陣列 存乘客的出站時間
    int arrival , leave;//記是本中的進站時間及出站時間
    int n;//總共n筆資料
    int time[86400];//存每秒有多少人數的陣列
    int big;//存最大數
    int j;

};
MRTJudgeSystem::MRTJudgeSystem(string filename)
{
    n=0;
    ifstream loadtimefile(filename.c_str(),ios::in);

    if(!loadtimefile)//檢查檔案是否開啟
    {
        cerr<<"File could not be open" <<endl;
        exit(1);
    }
    while( loadtimefile >> arrival >> leave )//讀檔,將進站與出站時間丟入陣列
    {
        arrayarrival[n] = arrival;
        arrayleave[n] = leave;
        n++;
    }
}

int MRTJudgeSystem::judge()
{
    big=0;
    for(int i=0;i<86400;i++)//初始化time陣列
    {
        time[i]=0;
    }
    j=0;
    while(j<=n)//將每個人從進入到出來的時間人數+1
    {
        for(int k=arrayarrival[j];k<=arrayleave[j];k++)
            time[k]++;
        j++;
    }
    for(int i=0;i<86400;i++)//找出最大人數
    {
        if(time[i]>big)
            big = time[i];
    }
    return big;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record1.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
