#include<iostream>
#include<fstream>
#include<cmath>
#include<cstdlib>
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(char []);
    int judge();
private:
    int data[100000];
    int n,count;
};

MRTJudgeSystem::MRTJudgeSystem(char a[]) //將讀取的資料存進data這個陣列
{
    n=0;
    ifstream time(a,ios::in);
    if (!time)
    {
        cerr << "error";
        exit(1);
    }
    while (time >> data[n])
        n++;
    time.close();
}

int MRTJudgeSystem::judge()
{
    int max=0,count=0;
    for (int t=1 ; t<n ; t+=2) //將下車時間用負號標記
        data[t]*=-1;
    for (int i=0 ; i<n-1 ; i++) //將上車及下車時間做排序
    {
        for (int j=i+1 ; j<n ; j++)
        {
            int buffer;
            if (abs(data[i])>abs(data[j]))
            {
                buffer=data[i];
                data[i]=data[j];
                data[j]=buffer;
            }
        }
    }

    for (int i=0 ; i<n ; i++) //讀到正時表示上車, 負時則表示下車
    {
        if (data[i]>=0)
            count+=1;
        else
            count-=1;
        if (count>max)
            max=count;
    }
    return max;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
