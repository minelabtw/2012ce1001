#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

class MRTJudgeSystem
{
  public:
    MRTJudgeSystem( char[] );
    int judge();
  private:
    fstream Time;
    vector<int> saveArrivalTime;
    vector<int> saveLeaveTime;
};

MRTJudgeSystem::MRTJudgeSystem( char name[] )//load the file
{
    Time.open( name, ios::in );
}

int MRTJudgeSystem::judge()
{
    int arrival, leave, counter=0, max=0;
    while( Time >> arrival >> leave )//load the data from file, use two vector to save arrived time and leaved time.
    {
        saveArrivalTime.push_back(arrival);
        saveLeaveTime.push_back(leave);
    }

    sort(saveArrivalTime.begin(),saveArrivalTime.end());
    sort(saveLeaveTime.begin(),saveLeaveTime.end());

    for( int i=0,j=0,k=0 ; i <= 86400 ; i++ )   //to find the max of the flow
    {
        while( j < saveArrivalTime.size() && saveArrivalTime[j] == i )
        {
            j++;
            counter++;
            if( counter > max )
                max = counter;
        }
        while( k < saveLeaveTime.size() && saveLeaveTime[k] == i )
        {
            k++;
            counter--;
        }
    }
    return max;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
