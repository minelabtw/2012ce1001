#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <algorithm>

using namespace std;

struct PEOPLE
{
    int in;
    int out;
};

bool cmp(PEOPLE a, PEOPLE b)
{
    if(a.in==b.in)
        return a.out < b.out;
    return a.in < b.in;
}

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string file);
    int judge();

private:
    PEOPLE people[100010];
    int n;
};

MRTJudgeSystem::MRTJudgeSystem(string file)
{
    n=0;
    ifstream fin(file.c_str());
    if(!fin)
    {
        cout << "Can't open the file!\n";
        exit(-1);
    }
    int x, y;
    while(fin>>x>>y)
    {
        people[n].in = x;
        people[n].out = y;
        n++;
    }
}

int MRTJudgeSystem::judge()
{
    sort(people, people+n, cmp);
    int pp[100010] = {0};
    int now = 0;
    for(int i=0;i<n;i++)
    {
        bool flag = false;
        for(int j=0;j<now;j++)
            if(pp[j]<people[i].in)
            {
                pp[j] = people[i].out;
                flag = true;
                break;
            }
        if(!flag)
            pp[now++] = people[i].out;
    }
    return now;

}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
