#include<iostream>
#include<fstream>
#include<string>
#include<cstdlib>

using namespace std ;

class MRTJudgeSystem
{
    public:
        MRTJudgeSystem(string) ;
        int judge() ;
        int SortNumber() ;
        int CountNumber() ;

    private:
        int total ;
        int in[86400] ;
        int out[86400] ;
        int TotalNow ;
        int maximum ;

};

MRTJudgeSystem::MRTJudgeSystem(string record)//input data
{
    ifstream inMRTJudgeSystem("record.txt",ios::in) ;

    int Entrance, Exit ;
    total = 0 ;

    if (!inMRTJudgeSystem)
    {
        cerr << "File could not be opened" << endl ;
        exit( 1 ) ;
    }

    while ( inMRTJudgeSystem >> Entrance >> Exit )//put value into the array
    {
        in[total] = Entrance ;
        out[total] = Exit ;

        total++ ;
    }
}

int MRTJudgeSystem::judge()
{
    ////////////////////sort data////////////////////

    int buffer ;

    for( int a=0 ; a<total ; a++ )
    {
        for ( int b=0 ; b<total ; b++ )
        {
            if ( in[a] < in[b] )
            {
                buffer = in[a] ;
                in[a] = in[b] ;
                in[b] = buffer ;
            }
        }
    }

    for( int a=0 ; a<total ; a++ )
    {
        for ( int b=0 ; b<total ; b++ )
        {
            if ( out[a] < out[b] )
            {
                buffer = out[a] ;
                out[a] = out[b] ;
                out[b] = buffer ;
            }
        }
    }

    ////////////////////count number and find maximum////////////////////

    TotalNow=0 ;
    maximum=0 ;

    int a=0 , b=0 ;

    for ( int t=in[0] ; t<=out[total-1] ; t++ )
    {
        if ( t==in[a]&&a<total )//confirm that every data will be considered
        {
            TotalNow++ ;
            a++ ;
        }

        if ( t==out[b]&&b<total )
        {
            TotalNow-- ;
            b++ ;
        }

        if ( TotalNow>maximum )//find maximum
            maximum=TotalNow ;
    }

    return maximum ;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
