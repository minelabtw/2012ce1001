#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    ifstream loadingdate;
    int now,max;//now為當前人數
};
MRTJudgeSystem::MRTJudgeSystem(string file)
{
    now=max=0;//initial
    loadingdate.open(file.c_str());//load file
    if(!loadingdate)
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
}
int MRTJudgeSystem::judge()
{
    int a[86401]= {0};//array of second
    int manin,manout;
    while(loadingdate>>manin>>manout)
    {
        a[manin]++;//increase
        a[manout]--;//decrease
    }
    loadingdate.seekg(0);
    for (int i=0; i<86401; i++)//隨著時間變化
    {
        now+=a[i];//加上人數變化輛
        if(now>max)//紀錄最大值
            max=now;
    }
    return max;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge()<< endl;
    return 0;
}
