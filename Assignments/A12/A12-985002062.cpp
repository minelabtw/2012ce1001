#include<iostream>
#include<fstream>
#include<vector>
#include<cstdlib>
#include <algorithm>
#include <cmath>
using namespace std;
bool cmp(const int& i, const int& j)
    {
        return abs(i) < abs(j);
    }

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string filename)
    {
        ifstream MRTFile(filename.c_str(),ios::in);
        if( !MRTFile )
        {
            cout << "Loading Error!!" << endl;
           // return false;
        }
    }

    struct Guest {int arrival, leave;} g[50000];

    int judge()
    {
        int n = 0, max_n = 0,count=0;

        ifstream infile("record.txt");//計算資料行數
        string s;
        while(getline(infile,s))
        {
            count++;
         //   cout<<count<<endl;
           // cout<<s<<endl;
        }


        ifstream MRTFile("record.txt",ios::in);//開檔
        vector<int> time;
        for (int i=0; i<count; ++i)
        {
            MRTFile>>g[i].arrival>>g[i].leave;
            time.push_back(+g[i].arrival);
            time.push_back(-g[i].leave);
        }

        sort(time.begin(), time.end(),cmp);//去絕對值做排序


//        for(vector<int>::iterator it = time.begin();it != time.end();it++)
//        {
//          //  if(*it!=0)
//                cout << *it << " ";
//        }


        for (int i=0; i<time.size(); ++i)
        {
            if (time[i] >= 0)
                n++;
            else
                n--;

            max_n = max(max_n, n);
        }
     //   cout<<max_n<<endl;

        return max_n;
    }




};

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;



    return 0;
}
