#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;


class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string filename)
    {
        File.open(filename.c_str(),ios::in);//讀檔案
        if(!File)//若無法讀，輸出錯誤訊息
        {
            cerr <<"File could not be opened"<< endl;
            exit(1);
        }
    }
    int judge()//計算最大人數
    {
        int start,end;
        int time[86401]={};//紀錄每個時間各有多少人的陣列，0~86400秒，共86401項
        while(File>>start>>end)//一行一行讀取資料，start為進入時間，end為出站時間
        {
            for(int i=start;i<=end;i++)//在站內的每一秒皆加一
                time[i]++;
        }
        int max=time[0];
        for(int i=1;i<=86400;i++)//取得最大值的迴圈
        {
            if(time[i]>max)//若大於max則將此值給予max
                max=time[i];
        }
        return max;//傳回最大人數
    }
private:
    ifstream File;

};
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
