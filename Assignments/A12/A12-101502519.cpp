#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <math.h>

using namespace std;
bool cmp(const int& i, const int& j)    //絕對值的運算方法
{
    return abs(i) < abs(j);
}

class MRTJudgeSystem
{
    public:
        int x,y;
        vector<int>c;

        MRTJudgeSystem(string file)
        {
            fstream inFile(file.c_str(),ios::in);   //讀檔

            while(inFile>>x>>y)       //進站讀正，出站讀負
            {
                c.push_back(x);
                c.push_back(-y);
            }
        }

    int judge()
    {
        int o=0,p=0;

        sort(c.begin(),c.end(),cmp);   //排序

        for(int n=0;n<c.size();n++)   //進站+1，出站-1，找最大值
        {
            if(c[n]>=0)
                o++;
            else
                o--;
            if(o>=p)
                p=o;
        }
        return p;  //回傳最大值
    }
};

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
