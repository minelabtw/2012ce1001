#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>  //為了使用C++內建的排序函式sort
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    int a[100000] ,b[100000];
};
MRTJudgeSystem::MRTJudgeSystem(string) //初始化清空陣列，讀資料
{
    for(int i=0 ; i<100000 ;i++)
    {
        a[i]=-1;
        b[i]=-1;
    }
    ifstream file("record.txt", ios::in );
    for(int i=0 ; i<100000 ; i++)
        file >> a[i] >> b[i];
}
int MRTJudgeSystem::judge() //先將record資料排序,再找出人數最大值
{
    int n;
    for(n=0 ; n<100000 ; n++)
        if(a[n]==-1)
            break;
    sort(a,a+n);
    sort(b,b+n);

    int people=0,max=0;
    for(int i=0,j=0,k=0; i <=86400 ; i++) //從0秒跑到86400秒
    {
        while(j<n && a[j]==i)  //將進站陣列a[j]跑到最後一項a[n],若有人有進站,
        {                      //將people+1並將j+1繼續往下找,若找到最大值則用max紀錄
            people++;
            j++;
            if(people>max)
                max=people;
        }
        while(k<n && b[k]==i)  //將出站陣列b[k]跑到最後一項b[n],若有人有出站,
        {                      //將people-1並將k+1繼續往下找
            people--;
            k++;
        }
    }
    return max;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}

