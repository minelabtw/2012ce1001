#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem( string ) ;
    int judge();
private:
    ifstream inaccountFile;
};

MRTJudgeSystem::MRTJudgeSystem( string record )
{
    inaccountFile.open( record.c_str() , ios::in );   //開檔

    if( !inaccountFile )   //若開檔錯誤則輸出訊息
    {
        cerr << "The file is not found !!!" << endl;
        exit(1);
    }
}

int MRTJudgeSystem::judge()
{
    int people[86401]={};   //0~86400共86401筆
    int in,out;

    while( inaccountFile >> in >> out )   //讀檔案
    {
        for( int i = in ; i <= out ; i++ )   //從進入到出來之中每一秒的時間陣列都加一
            people[i]++;
    }

    int max = people[0];

    for( int i=0 ; i <= 86400 ; i++ )   //找出人數最多的時間陣列
    {
       if( people[i] > max )
           max = people[i];
    }

    return max;   //輸出最大值
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");   //constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
