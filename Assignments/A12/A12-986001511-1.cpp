#include<iostream>
#include<fstream>
#include<sstream>
#include<cstdlib>

using namespace std;

class MRTJudgeSystem
{
public:
       MRTJudgeSystem(string f)
       {  filename=f;  }

       int judge()
       {
           ifstream inFile(filename.c_str(),ios::in);
           int arrive,leave,arr[50000],lea[50000],time[86400]={0},max=0,k=0;
           while(inFile>>arrive>>leave)
           {
               arr[k]=arrive;
               lea[k]=leave;
               k=k+1;
           }

           for(int n=0; n<=k-1; n++)
           {
               int t1=arr[n],t2=lea[n];
               for(int tt=t1; tt<=t2; tt++)
               {  time[tt]=time[tt]+1;   }
           }

           for(int n=0; n<=86399; n++)
           {  if(time[n]>max)
                {max=time[n];}
           }

           inFile.close();
           return max;
}

private:
       string filename;
};


int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
