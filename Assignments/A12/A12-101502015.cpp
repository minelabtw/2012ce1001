#include<iostream>
#include<string>
#include<cstdlib>
#include<fstream>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    void MRTJudgeSys(string);
    int judge();
private:
    int array[86401];
    int in,out;
};

MRTJudgeSystem::MRTJudgeSystem(string record)//初始化陣列
{
    for(int i=0;i<=86400;i++)
        array[i]=0;
    MRTJudgeSys(record);
}
void MRTJudgeSystem::MRTJudgeSys(string record)//將資料放進陣列
{
    ifstream inClientFile(record.c_str(),ios::out);

    while(inClientFile>>in>>out)
    {
        for(int j=in;j<=out;j++)
        {
            array[j]++;
        }
    }
}

int MRTJudgeSystem::judge()//比大小並輸出答案
{
    for(int i=0;i<86400;i++)
    {
        if(array[i]>array[i+1])
        {
            array[i+1]=array[i];
        }
    }
    return array[86400];
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//呼叫函式
    cout << MRTJudgeSys.judge() << endl;//回傳答案
    return 0;
}
