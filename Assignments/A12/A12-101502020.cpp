//A12 by J
//This time I want to do one program which is more efficient than my E10.
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
using namespace std ;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem( string ) ;      //constructor
    int judge() ;
private:
    int ToP[86400] ;       //ToP is short for time of passangers, i.e. at that time t, ToP[t]=passanger number.
    int data[100000] ;      //data array for storing data in txt file
    int DtNum ;     //DtNum for data number: how much data(get in& get out) did this txt provide.
    string tgt ;        //tgt is short for target, i.e. the target TXT data
    int max ;       //the maximun passanger number
};

MRTJudgeSystem::MRTJudgeSystem( string str )        //all data needed to be reseted reset in constructor
{
    for ( int i=0 ; i<100000 ; i++ )
    {
        ToP[i]=data[i]=0 ;     //reset 2 arrays
    }
    DtNum=0 ;       //reset datanumber
    tgt=str ;
    max=0 ;
}

int MRTJudgeSystem::judge()     //major functions build here.
{
    ifstream InputText( tgt.c_str() , ios::in ) ;       //assign data position
    if (!InputText)      //error message.
    {
        cerr << "File could not be opened" << endl;
        exit( 1 );
    }

    while ( InputText>>data[2*DtNum]>>data[2*DtNum+1] )     //while loop input all data from txt to data array
    {
        for ( int j=data[2*DtNum]; j<data[2*DtNum+1] ; j++ )
            ToP[j]++ ;
        DtNum++ ;       //input all the data.
    }

    for ( int k=0 ; k<86400 ; k++ )        //this loop find the maximum passanger number.
    {
        if (ToP[k]>max)
            max=ToP[k] ;
    }

    return max ;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
