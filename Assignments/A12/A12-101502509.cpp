#include <iostream>
#include <fstream>
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(const char *); //default  constructor
    int judge();            //get max people at a instant time in a day
private:
    int userarrival[50000];  //passenger arrival time,limit 50000
    int userleave[50000];    //passenger leave time,limit 50000
    int cdex;                //passenger numbers
    int time[86400];         //change rate of passenger numbers at a instant time
};
MRTJudgeSystem::MRTJudgeSystem(const char *readFile)
{
    ifstream inFile(readFile,ios::in);  //read record.txt
    int index=0;
    while(inFile>>userarrival[index]>>userleave[index])  //storage every passenger data
    {
        index++;
    }
    cdex=index;  //record passenger numbers
    for(int i=0;i<86400;i++)
    {
        time[i]=0;      //initialize timearray
    }
}

int MRTJudgeSystem::judge()
{
    int maxpeople=0;   //default max people at a instant time in a day is 0
    int people=0;      //default people at a instant time is 0
    for(int j=0;j<cdex;j++)
    {
        time[userarrival[j]]++;
        time[userleave[j]]--;
    }
    for(int i=0;i<86400;i++)
    {
        people+=time[i];
        if(people>maxpeople)
           maxpeople=people;
    }
    return maxpeople;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
