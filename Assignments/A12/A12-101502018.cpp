#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;
class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    vector<int> m;
    vector<int> n;
    int max;
    int buffer;
    int x;
    int y;
    int a;
    int b;
};
MRTJudgeSystem::MRTJudgeSystem(string z)
{
    ifstream in(z.c_str(),ios::in);
    max=0;
    while(in >>x >>y)
    {
        m.push_back(x);//進站
        n.push_back(y);//出站
    }
    sort(m.begin(),m.end());//排序
    sort(n.begin(),n.end());//排序
    b=0;
    for(int i=0;i<=86400;i++)
    {
        for(int j=0;j<m.size();j++)
        {
            if(m[j]==i)//進站加1
                b++;
            if(n[j]==i)//出站減1
                b--;
            if(b>max)
                max=b;//最大值
        }
    }
}
int MRTJudgeSystem::judge()
{
    return max;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
