#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

class MRTJudgeSystem
{
private:
    string filename;                //另設一個檔名.
    int intime, outime, max;        //進,出的時間跟最多人的時間.
    int people[86401];              //時間.
public:
    MRTJudgeSystem(string file)
    {
        filename=file;              //檔名共通.
        ifstream inClientFile(file.c_str(), ios::in);       //讀檔.
        if(!inClientFile)
        {
            cerr << "File could not be opened" << endl;
            exit( 1 );
        }
        for(int p=0;p<86401;p++)                          //先將每個時間的人數歸零.
        {
            people[p]=0;
        }
    }
    int judge()
    {
        max = 0;
        ifstream inClientFile(filename.c_str(),ios::in);        //讀檔.
        if(!inClientFile)
        {
            cerr << "File could not be opened" << endl;
            exit( 1 );
        }
        while(inClientFile >> intime >> outime)             //將每一秒在捷運的人做統計
        {
            for(int i = intime; i <= outime; i++)
            {
                people[i]++;
            }

        }
        for(int o = 0; o <= 86400; o++)             //找出人最多的時間.
        {
            if(people[o] >= max)
            {
                max = people[o];
            }
        }
        return max;
    }
};
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
return 0;
}
