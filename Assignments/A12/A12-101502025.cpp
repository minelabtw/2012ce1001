#include <iostream>
#include<fstream>
#include<cstdlib>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(const char *record)
    {
        ifstream in(record,ios::in);
        if(!in)
        {
            cerr<<"File could not be opened"<<endl;
            exit(1);
        }
        for(int s=0;s<86400;s++)
        {
            x[s][0]=0; // time to get in
            x[s][1]=0; // time to get out
            x3[s]=0; // the moment
        }
        int a,b,c=0;
        while(in>>a>>b) // get the data
        {
            x[c][0]=a;
            x[c][1]=b;
            c++;
        }
    }
    int judge()
    {
        for(int j=0;j<50000;j++) // check every data
        {
            for(int i=x[j][0];i<=x[j][1];i++) // calculate the resaults
            {
                x3[i]++;
            }
        }
        int max; // the answer
        for(int k=0;k<86400;k++) // get the real maxinum
        {
            for(int r=k+1;r<86400;r++)
            if(x3[r]>max)
            {
                max=x3[r];
            }
        }
        return max;
    }
private:
    int x[50000][2],x3[86400]; // x for each data,x3 for every moment
};

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
