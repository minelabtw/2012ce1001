#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    int max;
    string entertime;
    string leavetime;
    int array[86400];
    int s_entertime;
    int s_leavetime;
};

MRTJudgeSystem::MRTJudgeSystem(string str)
{
    ifstream inClientfile( str.c_str(),ios::in);
    for(int i=0;i<86400;i++) //use the loop to initialize the array[86400]
        array[i]=0;
}

int MRTJudgeSystem::judge()
{
    ifstream inClientfile( "record.txt",ios::in);
    while(inClientfile >> entertime >> leavetime)
    {
        istringstream is1(entertime); // transform string entertime into int form
        istringstream is2(leavetime); //transform string leavetime into int form
        is1 >> s_entertime;
        is2 >> s_leavetime;
        for(int k=s_entertime;k<=s_leavetime;k++)
            array[k]++;
    }
    max=0;
    for(int k=0;k<86400;k++)
        if(array[k]>max)
            max=array[k];
    return max;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
