#include<iostream>
#include<string>
#include<fstream>
#include<vector>
#include <algorithm>
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(char []);
    int judge();
private:
    fstream data;
    vector <int> in;
    vector <int> out;
};

MRTJudgeSystem::MRTJudgeSystem(char filename[])   //open file in reading statement.
{
    data.open(filename,ios::in);

}

int MRTJudgeSystem::judge()
{
    int in_max=0;
    int out_max=0;
    int n=0,n_max=0;
    int a=0;
    int b=0;
    if(data.is_open())         //to confirm the file is be open
    {
        int i=1;
        while(!data.eof())     //if read to the end of file,end the loop.
        {
            in.push_back(i);
            out.push_back(i);
            data>>a;
            in[i-1]=a;
            data>>b;
            out[i-1]=b;
            i++;
        }
        in.pop_back();      //the loop will read d the last data one more,
        out.pop_back();     //so have to clean the last data.
        sort(in.begin(),in.end());   //sort data.
        sort(out.begin(),out.end());

        in_max=in[(in.size())-1];
        out_max=out[(out.size())-1];

        a=0;
        b=0;
        for(int t=0;t<=max(in_max,out_max);t++) //t is like the time line,
        {                                       //if the time is same as in[a] ,make n +1,
            if(t==in[a])                        //if the time is same as out[b] ,make n -1
            {
                a++;
                n++;
            }
            if(t==out[b])
            {
                b++;
                n--;
            }
            n_max=max(n,n_max);                //to take maximum of n .
        }
    }
    return n_max;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
