#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
#include<algorithm>
using namespace std;
bool cmp(const int &x , const int &y) //cmp為自定sort函式排序的標準
{
    return (abs(x) < abs(y));
}
class MRTJudgeSystem
{
    public:
        MRTJudgeSystem(string name)
        {
            target = name;
        }
        int judge()
        {
            max = 0; //max記錄搭乘的最大人數
            size = 0; //size記錄文件有幾筆出站或入站時間
            sum = 0; //sum記錄某個時間點有多少人在站內
            ifstream file(target.c_str() , ios::in); //讀入文件
            while(file >> begin >> end) //讀入文件中的資料
            {
                time[size] = +begin; //將入站時間預設為正整數
                size++;
                time[size] = -end; //將出站時間預設為負整數
                size++;
            }
            sort(time , time + size , cmp); //time陣列進行排序
            for(int k = 0 ; k < size ; k++) //比較每個時間點的站內人數 , 並找出站內的最高人數
            {
                if(time[k] >= 0)
                    sum++;
                else
                    sum--;
                if(max < sum)
                    max = sum;
            }
            return max;
        }
    private:
        string target;
        int time[100000];
        int begin;
        int end;
        int max;
        int size;
        int sum;
};
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
