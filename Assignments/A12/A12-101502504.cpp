#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;

class MRTJudgeSystem
{
    public:
        MRTJudgeSystem(string);
        int judge();
    private:
        int amount,instation, outstation;
        int count[86401],inarray[60000],outarray[60000];
};

MRTJudgeSystem::MRTJudgeSystem(string filename)//loadfile and initial
{
    for( int i=0; i<86401; i++ )
        count[i]=0;

    ifstream recordfile(filename.c_str(),ios::in);
    if( !recordfile )
    {
        cerr << "File couldn't be opened" << endl;
        exit(1);
    }
    amount=0;
    while( recordfile >> instation >> outstation )
    {
        inarray[amount]=instation;//a person go into station
        outarray[amount]=outstation;//a person is out station
        amount++;
    }
}

int MRTJudgeSystem::judge()
{
    int big=0;//find the most amount

    for( int i=0; i<amount; i++ )
        for ( int j=inarray[i]; j<=outarray[i]; j++ )
            count[j]++;//find which second has the most people

    for( int i=0; i<86401; i++ )
        if( count[i]>big )
            big=count[i];

    return big;//output answer
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record1.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
