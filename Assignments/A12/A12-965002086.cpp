/* ----   10 MRT judge system   -----
operation system: Win7, Dev-C++ 4.9.9.2

965002086 張凱閔
資工系四年B班


捷運公司正在考慮是否要加開班次，以減少人車壅擠的問題。所以需要你幫忙寫個程式幫助捷運公司

統計同時搭乘的最高人數，來幫助判斷。

輸入：

record.txt中記載了每位乘客的進出站時間，範圍在0~86400。且每位乘客的進出站時間都不會相同

。(即某一時間點不會有一位以上的乘客進出站)

record.txt中編排方式為：乘客A進站時間+空白+乘客A出站時間+換行，依此類推。詳見所附的 record.txt。

輸出：最大搭乘人數。

格式：

提示：

http://www.csie.ntnu.edu.tw/~u91029/Incremental.html 中的宴會中訪客數目的極大值。

record4 : 261

record5 : 136

record6 : 501

*/
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>

using namespace std;

bool cmp(const int& i, const int& j);

class MRTJudgeSystem
{
    public:
    int arrival, leave;
    vector<int> time;
    MRTJudgeSystem(string );
    int judge();
};

bool cmp(const int& i, const int& j)
  {
       return abs(i) < abs(j);
       }

 MRTJudgeSystem::MRTJudgeSystem(string inputname )
{

     ifstream inFile;
     inFile.open(inputname.c_str());
     while(!inFile.eof())
     {
     inFile>>arrival;
     time.push_back(+arrival);
     inFile>>leave;
     time.push_back(-leave);
     }
     inFile.close();

     cout << inputname.substr(0,6)<<" : " ;
     sort(time.begin(), time.end(), cmp);
}

int MRTJudgeSystem::judge()
{
        int n = 0, max_n = 0;
        for (int i=0; i<time.size(); ++i)
        {
            if (time[i] >= 0)
                n++;
            else
                n--;

            max_n = max(max_n, n);
        }

        return max_n;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;

    system("pause");
    return 0;

}


