#include<iostream>
#include<cstdlib>
#include<fstream>
#include<string>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string filename)
    {
        txtfile.open(filename.c_str(),ios::in); //讀檔 : filename
        if(!txtfile)                    //讀檔失敗，直接結束程式
            exit(1);
        for(int i=0;i<86401;i++)        //a[]={}
            a[i]=0;
        max = 0;
    }
    int judge()
    {
        while(txtfile >> in >> out)
        {
            for(int i=in;i<out;i++)     //計算人數
                a[i]++;
        }
        for(int i=0;i<86401;i++)        //取最大a[i]為max
            if(a[i]>max)
                max = a[i];
        return max;
    }
private:
    ifstream txtfile;   //宣告讀檔
    int a[86401],in,out,max;    //a=時間軸(a[1]=1s時的人數)
};
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
