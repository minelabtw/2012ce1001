#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;
class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    ifstream readData;
};
MRTJudgeSystem::MRTJudgeSystem(string fileName)//開檔
{
    readData.open(fileName.c_str(),ios::in);//.c_str():把字串轉為字元陣列
    if(!readData)
    {
        cerr<<"檔案無法開啟!"<<endl;
        exit(1);
    }
}
int MRTJudgeSystem::judge()
{
   int array[86401]={};//因為是0~86400有86401個數
   int log1,log2;
   while(readData>>log1>>log2)//讀檔
   {
       for(int i=log1;i<=log2;i++)
        {

            array[i]++;
        }
   }
   int max=0;
   for(int j=0;j<=86400;j++)//比較
    {
        if(array[j]>max)
            max=array[j];

    }
    return max;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
