#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
using namespace std;
class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    int time[86401];//存進站及出站的各個時間
};
MRTJudgeSystem::MRTJudgeSystem(string File)
{
    ifstream RecordFile("record.txt",ios::in);
    if(!RecordFile)
    {
        cerr<<"Damn"<<endl;
        exit(1);
    }
    int arrival,leave;

    for(int s=0;s<=86400;s++)
        time[s]=0;//歸零

    while( RecordFile >> arrival >> leave )
    {
        for ( int i = arrival ; i <= leave ; i++)
        {//時間跑到一次代表那個時間的人數加一
            time[i]++;
        }
    }
}
int MRTJudgeSystem::judge()
{
    int max=0;
    for (int second=0 ; second<=86400 ; second++)
    {//判斷哪個時間的人最多
        if ( max < time[second])
            max = time[second];
    }
    return max;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
