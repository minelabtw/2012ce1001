#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;
class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    ifstream readFile;
};
MRTJudgeSystem::MRTJudgeSystem(string fileName)
{
    readFile.open(fileName.c_str(),ios::in);//open file
    if(!readFile)
    {
        cerr<<"File could not be open."<<endl;
        exit(1);
    }
}
int MRTJudgeSystem::judge()
{
    int time[86401]={},enterTime=0,leaveTime=0,max=0;
    while(readFile>>enterTime>>leaveTime)//read file
    {
        for(int i=enterTime;i<=leaveTime;i++)//plus all members of array in the interval
        {
            time[i]++;
        }
    }
    for(int j=0;j<=86400;j++)//find the maximum
    {
        if(time[j]>max)
        {
            max=time[j];
        }
    }
    return max;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
