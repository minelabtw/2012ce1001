#include<iostream>
#include<fstream>
#include<cmath>
#include<cstdlib>
using namespace std;

class MRTJudgeSystem//宣告

{
    public:
        MRTJudgeSystem(char []);//讀檔
        int judge();//判斷
    private:
        fstream times;
        int  tt[200000];//儲存資料
        int x,max; //X用來記錄有幾筆資料 MAX為最多人數



};

MRTJudgeSystem::MRTJudgeSystem(char a[])//讀檔案
{
    max=0;
    x=0;
    times.open(a,ios::in);
    while(times>>tt[x])x++;
    times.close();
}

int MRTJudgeSystem::judge()
{
    for(int y=1;y<x;y+=2)//每個乘以-1 區分入站出站
        tt[y]*=-1;

    for(int i=0;i<x-1;i++)//依照數值大小排序
        for(int j=i+1;j<x;j++)
        {
            if(abs(tt[i])>abs(tt[j]))
            {
                int sw=tt[i];
                tt[i]=tt[j];
                tt[j]=sw;
            }
            else if(abs(tt[i])==abs(tt[j])&&tt[i]<tt[j])//入站先算!!
            {
                int sw=tt[i];
                tt[i]=tt[j];
                tt[j]=sw;
            }
        }
    int temp=0;
    for(int y=0;y<x;y++) //統計 MAX更新
    {
        if(tt[y]>=0)
            temp++;
        else
            temp--;
        if(temp>max)
            max=temp;
    }
    return max;

}

int main()

{

MRTJudgeSystem MRTJudgeSys("record.txt");

cout << MRTJudgeSys.judge() << endl;

return 0;

}
