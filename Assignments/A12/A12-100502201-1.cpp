//A12-100502201-1
//A program to judge the maximum number of people in MRT
//Set time as a main judgement to see how many people are in MRT at a time
#include <vector>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <algorithm> //Use "max" function
using namespace std;

class ioTime //To get the time of getIn and getOut and put them into vector
{
    private:
        int in;
        int out;
    public:
        ioTime(int _in,int _out)
        {
            in=_in;
            out=-_out;
        }
        int getIn()
        {
            return in;
        }
        int getOut()
        {
            return out;
        }
};

class MRTJudgeSystem
{
    private:
        vector<int> timelist;
        int max_n; //The result we want
    public:
        MRTJudgeSystem() //Default constructor
        {

        }
        MRTJudgeSystem(string filename)
        {
            ifstream fin(filename.c_str(),ios::in);
            if(!fin)
            {
                cout<<"Fail to load."<<endl;
                return ;
            }
            int ins ,ous;
            while (fin>>ins>>ous) //Load data and put them into vector
            {
                ioTime io(ins,ous);
                timelist.push_back(io.getIn());
                timelist.push_back(io.getOut());
            }
            fin.close();

            int n = 0;
            max_n = 0;
            for (int i=0; i<=86400; i++) //Set time as a main loop to judge the maximum number
            {
                for (int j=0;j<timelist.size();) //Convert vector to array
                {
                    if (timelist[j]==i) //When "j" is even, it's the time of arriving
                        n++;
                    if(timelist[j+1]==-i) //When "j" is odd, it's the time of leaving
                        n--;
                    max_n = max(max_n, n); //Record the max of n
                    j=j+2;
                }
            }
        }
        int judge()
        {
            return max_n;
        }
};
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
