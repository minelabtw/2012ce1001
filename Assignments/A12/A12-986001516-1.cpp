#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    string file_name;
};

MRTJudgeSystem::MRTJudgeSystem(string f)
{
    file_name=f;
    ifstream file(f.c_str(),ios::in);
    if (!file)
    {
        cout << "File not found!!" << endl;
    }
    file.close();
}

int MRTJudgeSystem::judge()
{
    ifstream file(file_name.c_str(),ios::in);
    int arrival,leave;
    vector<int> time_data(86400);
    for (int i=0;i<=86399;i++)
    {
        time_data[i]=0;
    }
    while(file>>arrival>>leave)
    {
        for (int i=arrival-1;i<=leave-1;i++)
        {
            time_data[i]+=1;
        }
    }
    sort(time_data.begin(), time_data.end());
    file.close();
    return time_data[86399];
}


int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
