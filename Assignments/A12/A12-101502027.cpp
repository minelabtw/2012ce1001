#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <cmath>
using namespace std;

class MRTJudgeSystem
{
public:
        MRTJudgeSystem(string);
        int judge();
        static bool cmp(const int& ,const int& );

private:
        vector<int> x;
        vector<int> y;
        int a;
        int b;
        int p;
        int gus;
        int max_gus;
};

MRTJudgeSystem::MRTJudgeSystem(string z)
{
    gus=0;
    max_gus=0;//初始化
    ifstream in(z.c_str(),ios::in);
    while(in>>a>>b)
    {
        x.push_back(+a);
        x.push_back(-b);
    }//存資料進入陣列中
    sort(x.begin(), x.end(), cmp);//排列
}
bool MRTJudgeSystem::cmp(const int& i, const int& j)
{
    return abs(i) < abs(j);
}//忽略正負排列
int MRTJudgeSystem::judge()
{
    for(int j=0;j<x.size();j++)
    {
            if(x[j]>=0)
                gus++;
            if(x[j]<0)
                gus--;
            if(gus>max_gus)
                max_gus=gus;//找臨界值
    }
    return max_gus;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
