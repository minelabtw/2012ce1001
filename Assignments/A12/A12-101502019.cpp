#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    int people[86401];
    int arraysize;
};

MRTJudgeSystem::MRTJudgeSystem(string file)
{
    ifstream inClientFile( file.c_str(),ios::in);
    if (!inClientFile)
    {
        cerr << "File could not be opened" << endl;
        exit( 1 );
    }
    int people[86401]={};
}

int MRTJudgeSystem::judge()
{

    int intime,outime,max = 0;
    ifstream inClientFile("record.txt",ios::in);
    if (!inClientFile)
    {
        cerr << "File could not be opened" << endl;
        exit( 1 );
    }
    while (inClientFile >> intime >> outime)//read data from file
    {
        for (int i = intime; i <= outime; i++)
        {
            people[i]++;
        }
    }
    for (int j = 0; j < 84458; j++)//access max
    {
        if (people[j] >= max)
        {
            max = people[j];
        }
    }
    return max;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
