#include<iostream>
#include<vector>
#include<fstream>
#include<string>
using namespace std;
class MRTJudgeSystem//製作class
{
    public:
        MRTJudgeSystem(string);
        int judge();
    private:
        vector<int> intime;
        vector<int> outtime;
        vector<int> people;
};
MRTJudgeSystem::MRTJudgeSystem(string txt)//讀檔
{
    ifstream list(txt.c_str());
    int timein,timeout;
    while(list>>timein>>timeout)
    {
        intime.push_back(timein);
        outtime.push_back(timeout);
    }
}
int MRTJudgeSystem::judge()//計算人次
{
    int current=0,maxmum=0;
    for(int i=0;i<86401;i++)
        people.push_back(0);
    for(int i=0;i<intime.size();i++)
    {
        for(int j=intime[i];j<=outtime[i];j++)
            people[j]++;
    }
    for(int i=1;i<=86400;i++)
    {
        if(people[i]>maxmum)
            maxmum=people[i];
    }
    return maxmum;
}
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
