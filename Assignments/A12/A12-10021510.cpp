#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    int flux[86400];
    ifstream datafile;
};

MRTJudgeSystem::MRTJudgeSystem(string filename)
{
    datafile.open(filename.c_str(),ios::in);

    if( !datafile )//check the file is loaded or not.
    {
        cout << filename + " is not found!" << endl;
        exit(1);
    }

    for(int i = 0 ; i < 86400 ; i++)
    {
        flux[i] = 0;//initialize array
    }
}

int MRTJudgeSystem::judge()
{
    int gointime , goouttime;
    int passanger = 0;
    int maxpassanger = 0;
    while(datafile >> gointime >> goouttime)
    {
        flux[goouttime]--;//when someone go out.
        flux[gointime]++;//when someone go in.
    }

    for(int time = 0 ; time < 86400 ; time++)
    {
        passanger += flux[time];//passanger is the number at the moment.

        if(passanger > maxpassanger)
            maxpassanger = passanger;
    }

    return maxpassanger;
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
