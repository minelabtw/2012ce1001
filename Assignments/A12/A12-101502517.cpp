#include<iostream>
#include<fstream>
#include<string>
#include<cstdlib>
using namespace std;
class MRTJudgeSystem
{
    public:
        MRTJudgeSystem(string);
        int judge();
   private:
        int max;
        int now;
        ifstream rec;
};
MRTJudgeSystem::MRTJudgeSystem(string a)//constructor
{
    rec.open(a.c_str());
    if(!rec)//OPEN check
    {
        cout<<"Error!!!"<<endl<<endl;
        exit(1);
    }
    max=0;
    now=0;
}
int MRTJudgeSystem::judge()
{
    int a[86401]={0};//陣列儲存人數變化
    int e,o;
    while(rec>>e>>o)//讀取
    {
        a[e]++;//進+1
        a[o]--;//出-1
    }
    for(int i=0;i<=86400;i++)//比出最大值
    {
        now=now+a[i];
        if(now>max)
        {
            max=now;
        }
    }
    return max;//回傳
}


int main()

{

    MRTJudgeSystem MRTJudgeSys("record.txt");

    cout << MRTJudgeSys.judge() << endl;

    return 0;

}
