#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;

class MRTJudgeSystem
{
public:
        MRTJudgeSystem(string);
        int judge();
private:
        vector<int> a;
        vector<int> b;
        int x;
        int y;
        int p;
        int array[86401];//有0~86400秒的時間
};

MRTJudgeSystem::MRTJudgeSystem(string h)
{
    p=0;
    ifstream in(h.c_str(),ios::in);//讀出檔案資料
    for(int i=0;i<=86400;i++)//將陣列裡都變為0
    {
        array[i]=0;

    }
    while(in>>x>>y)//將每個時間都變為一個點，時間點在車站內就加一
    {
        for(int i=x;i<=y;i++)
        {
            array[i]++;
        }

    }
    for(int i=0;i<=86400;i++)
    {
        if(array[i]>p)
        {
            p=array[i];
        }
    }
}

int MRTJudgeSystem::judge()//回傳最大人數
{
    return p;
}



int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
