#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;
class MRTJudgeSystem // MTREJudgeSystem class definition
{
    public: // public data
        MRTJudgeSystem(string file) // constructor
        {
            for (int z = 0; z <= 86400; z++) // set all arrays to 0
                people[z] = 0;
            MRTJudgeSys(file); // set MTRJudgeSys to the same file
        }
        void MRTJudgeSys(string file) // function to load the file
        {
            data.open (file.c_str(), ios::in); // load file
            if (!data)
            {
                cerr << "File could not be opened" << endl;
                exit(1);
            }
        }
        int judge() // judge function
        {
            if (data.is_open()) // open the file
            {
                data.seekg(0, ios::beg); // start with the begining
                while (data >> arrive >> leave) // take out the file's data
                {
                    for (int i = arrive; i <= leave; i++) // count people
                    {
                        people[i]++;
                    }
                }
            }
            for (int j = 0; j < 86400; j++) // sort
            {
                if (people[j] > people[j + 1]) // find the highest number of people
                    people[j + 1] = people[j];
            }
            return people[86400]; // return the highest number of people
        }
    private: // private data
        fstream data;
        int arrive, leave;
        int people[86401];
};
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
