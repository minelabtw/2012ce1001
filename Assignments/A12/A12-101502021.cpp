#include <iostream>
#include <fstream>
#include <cstdlib>
#include <sstream>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    string time_arrival;
    string time_leave;
    int time[86400];
    int T_arrival; //將入站時間的字串轉換為整數型態.
    int T_leave; //將出站時間的字串轉換為整數型態.

};

MRTJudgeSystem::MRTJudgeSystem(string str)
{
    ifstream inClientFile(str.c_str(), ios::in);

    if(!inClientFile)
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }

    //將陣列86400個元素都歸0.
    for(int i=0 ; i<86400 ; i++)
        time[i]=0;
}

int MRTJudgeSystem::judge()
{
    ifstream inClientFile("record.txt", ios::in);

    while (inClientFile >> time_arrival >> time_leave)
    {
        //用來將字串轉換為整數型態.
        istringstream is1(time_arrival);
        istringstream is2(time_leave);
        is1>>T_arrival;
        is2>>T_leave;

        for(int j=T_arrival ; j<T_leave ; j++)
            time[j]++;

    }

    int max=0;

    for(int i=0 ; i<86400 ; i++)
    {
        if(time[i]>=max)
            max=time[i];
    }

    return max;
}


int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
