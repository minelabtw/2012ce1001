#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);
    int judge();
private:
    bool real;
    int max, count,time;
    int a[100000],l[100000];
};

MRTJudgeSystem::MRTJudgeSystem(string record)
{
    real = false;
    max = 0;
    count = 0;
    time = 0;
    ifstream Record("record.txt",ios::in);     //匯入檔案
    for (int i = 0; i< 100000 ; i++)
        Record >> a[i] >> l[i];
}

int MRTJudgeSystem::judge()                          //判斷時間若進入時間相等於目前時間則
{                                                    //代表裡面人數+1若出去時間相等則人數-1
    for (int time = 0 ; time < 86400 ; time ++)
    {
        for ( int i = 0; i< 60000 ; i++)
        {
            if ( a[i] == time)
                count ++ ;
            if ( l[i] == time)
                count -- ;
        }
        if ( count > max )                           //保存最大值
            max = count;
    }
    return max;                                      //回傳最大值
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
