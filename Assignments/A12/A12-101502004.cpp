#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;

class MRTJudgeSystem
{
public:
    MRTJudgeSystem(string);//constructor
    int judge();
private:
    int begin;
    int leave;
};

int Savedatabegin[100000];//儲存進站時間
int Savedataleave[100000];//儲存出站時間
int data=0;//記錄共幾筆資料

MRTJudgeSystem::MRTJudgeSystem(string str)//constructor裡面讀取資料
{
    ifstream MRTJudgeSys("record.txt",ios::in);
    while(MRTJudgeSys >> begin >> leave)
    {
        Savedatabegin[data]=begin;
        Savedataleave[data]=leave;
        data++;
    }
}

int MRTJudgeSystem::judge()
{
    int countbegin=0,countleave=0;
    int number=0;
    int max=0;
    for(int i=0;i<data;i++)//以每次的出站時間當作一個紀錄點
    {
        for(int j=0;j<data;j++)
        {
            if(Savedataleave[i]>Savedatabegin[j])//計算在紀錄點之前的所有進站時間
            {
                countbegin++;
            }
            if(Savedataleave[i]>Savedataleave[j])//計算在紀錄點之前的所有出站時間
            {
                countleave++;
            }
        }
        number=countbegin-countleave;//相減之後即為紀錄點當下的人數
        if(number>max)//找出最大值
        {
            max=number;
        }
        number=0;
        countbegin=0,countleave=0;
    }
    return max;//return
}

int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");//constructor for loading data from record.txt
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
