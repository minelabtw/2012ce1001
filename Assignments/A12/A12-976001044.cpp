#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <fstream>
using namespace std;

// 自定義的比較規則函數
bool cmp(const int& i, const int& j)
{
    return abs(i) < abs(j);
}

class MRTJudgeSystem
{
    private :
        string FileName;
        vector<int> RecordList;

    public :
        // 由此建構元初始化主程式傳入、所要判別的紀錄檔之檔案名稱
        MRTJudgeSystem(string id)  // customize constructor
        {
            FileName = id;//cout << "constructor has been call ..." << endl;//system("pause");
        }
        /*
        MRTJudgeSystem()  // default constructor
        {
            //cout << "constructor has been call ..." << endl;//system("pause");
        }

        ~MRTJudgeSystem() // default deconstructor
        {
            //cout << "deconstructor has been call ..." << endl;//system("pause");
        }
        */
        int judge()
        {
            int index=0, counter=0, GuestMaxNum=0, num1=0, num2=0;
            ifstream recordfile(FileName.c_str(), ios::in);            // 開啟目標檔案，設定為唯獨

            if ( !recordfile.is_open() )
            {
                cerr << FileName << " open fail !!" << endl;
                exit(-1);
            }

            while ( recordfile >> num1 >> num2 )                       // 由檔案物件 recordfile 抓取資料至中介變數
            {
                RecordList.push_back(+num1);
                RecordList.push_back(-num2);                           // 將訪客拜訪時間為正、離開時間為負 push 至陣列 RecordList 中
            }

            // 使用STL內建的排序函數，加上自定義的比較規則函數，對 RecordList 陣列進行排序
            sort(RecordList.begin(), RecordList.end(), cmp);
            recordfile.close();                                        // 目標檔案使用完畢，關閉檔案

            // 計算訪客最大滯留人數之演算法
            // 規則為簡單計數器，有人拜訪則滯留人數+1，有人離開則滯留人數-1
            for ( int i=0 ; i < RecordList.size() ; i++ )
            {
                if ( RecordList[i] >= 0 )
                    counter++;
                else
                    counter--;

                if ( GuestMaxNum < counter )                           // 若計數器之值大於最大滯留數目，則刷新最大數目
                    GuestMaxNum = counter;
            }
            return GuestMaxNum;
        }
};

/*
// 測試用
int main()
{
    MRTJudgeSystem MRTJudgeSys1("record4.txt"), MRTJudgeSys2("record5.txt"), MRTJudgeSys3("record6.txt");
    cout << MRTJudgeSys1.judge() << endl;
    cout << MRTJudgeSys2.judge() << endl;
    cout << MRTJudgeSys3.judge() << endl;
    return 0;
}
*/

// 原主程式
int main()
{
    MRTJudgeSystem MRTJudgeSys("record.txt");
    cout << MRTJudgeSys.judge() << endl;
    return 0;
}
