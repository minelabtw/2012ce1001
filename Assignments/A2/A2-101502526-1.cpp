#include <iostream>

using namespace std;

int main()
{
    int number1;//設定值1為整數
    int number2;//設定值2為整數
    int number3;//設定值3為整數
    int i;//設定暫存的數為整數
    cout << "Enter the integer 1 : " ;//要求輸入值1
    cin  >> number1 ;//輸入值1
    cout << "Enter the integer 2 : " ;//要求輸入值2
    cin  >> number2 ;//輸入值2
    cout << "Enter the integer 3 : " ;//要求輸入值3
    cin  >> number3 ;//輸入值3
    /*假設值1大於值2，則值1的值等於暫存的值，
      然後值1的值等於值2的值，值2的值等於暫存的值*/
    if (number1>number2)
    {
        i=number1;
        number1=number2;
        number2=i;
    }
    /*假設值2大於值3，則值2的值等於暫存的值，
      然後值2的值等於值3的值，值3的值等於暫存的值*/
    if (number2>number3)
    {
        i=number2;
        number2=number3;
        number3=i;
    }
    /*假設值1大於值2，則值1的值等於暫存的值，
      然後值1的值等於值2的值，值2的值等於暫存的值*/
    if (number1>number2)
    {
        i=number1;
        number1=number2;
        number2=i;
    }
    cout<<"The sort of the 3 integers : "<<number1<<" "<<number2<<" "<<number3;//輸出三個值
    return 0;
}
