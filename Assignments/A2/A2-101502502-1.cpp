#include <iostream>
using namespace std;

int main()
{
    int num1;
    int num2;
    int num3;
    int buffer; // give a space to store the interger

    cout << "Enter the integer 1: ";
    cin >> num1;

    cout << "Enter the integer 2: ";
    cin >> num2;

    cout << "Enter the integer 3: ";
    cin >> num3;

    if ( num1 > num2 ) // if num1 is greater than num2, exchange num1 and num2
    {
        buffer = num1;
        num1 = num2;
        num2 = buffer;
    }


    if ( num1 > num3 ) // if num1 is greater than num3, exchange num1 and num3
    {
        buffer = num1;
        num1 = num3;
        num3 = buffer;
    }


     if ( num2 > num3 ) // if num2 is greater than num3, exchange num2 and num3
    {
        buffer = num2;
        num2 = num3;
        num3 = buffer;
    }


    cout << "The sort of the 3 intergers : ";
    cout << num1 << " " << num2 << " " << num3 << endl;


    return 0;
}
