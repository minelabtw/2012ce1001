#include <iostream>
using namespace std;
int main()
{
    int startnum,endnum,dividenum,sum=0;
    cout<<"Enter the start : ";
    cin>>startnum;
    cout<<"Enter the end : ";
    cin>>endnum;
    cout<<"Enter the divider : ";
    while(cin>>dividenum)
        if(dividenum!=0)
        {//避開除以0的情況
            for(int i=startnum;i<=endnum;i++)
                if(i%dividenum==0)
                    sum=sum+i;
            break;//算完後跳出while
        }
        else//若是除以0，則重新輸入
            cout<<"Enter the divider : ";
    cout<<"Sum : "<<sum<<endl;
    return 0;
}
