#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2,integer3,buffer,i;

    cout << "Enter the integer 1: ";
    cin >> integer1;
    cout << "Enter the integer 2: ";
    cin >> integer2;
    cout << "Enter the integer 3: ";
    cin >> integer3;

    for (i=0;i<2;i++) //run the function twice.
    {
        if ( integer1 > integer2 ) //if integer1 > integer2,exchange the value one another.
        {
            buffer = integer1;
            integer1 = integer2;
            integer2 = buffer;
        }
        if ( integer2 > integer3 ) //if integer2 > integer3,exchange the value one another.
        {
            buffer = integer2;
            integer2 = integer3;
            integer3 = buffer;
        }
    }
    cout << "The sort of the 3 integers : ";
    cout << integer1 << " " << integer2 << " " << integer3 << endl; //that means the smallest is integer1,and the biggest is integer3.
    return 0;
}
