#include <iostream>

using namespace std;

int main()
{
    int start,end,divider,sum;
    sum = 0;

    cout << "Enter the start : ";
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;
    cout << "Enter the divider : ";
    cin >> divider;

    for ( ; start<=end ; start++ ) //當"start"不能被"divider"整除時
    {                              //自動加到能被"divider"整除為止
        if ( start%divider==0 )    //在此能被"divider"整除之數字將做加總
            sum = sum + start;
    }
    cout << "Sum is : " << sum << endl;

    return 0;
}
