//plus all the integers between start and end which can be divisible by divider.
#include <iostream>
using namespace std;

int main(){

    int start;
    int end;
    int divider;
    int sum = 0;//plus all the integers between start and end which can be divisible by divider
    int i;//number between the start and the end

    //input start,end entered by user
    cout << "Enter the start : ";
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;

    //the start number has to smaller than the end number
    if( start > end )
    cout << "*** Input Error ***\n" << "The start has to smaller than the end." << endl;

    else{//the start number is smaller than the end number

        //input divider entered by user
        cout << "Enter the divider : ";//let user enter the divider
        cin >> divider;

        //if the number between start and the end can be divisible by divider, plus all of them
        for( i = start; i <= end ; i++ ){

            if( i % divider == 0 ){
                sum += i;
            }//if end

        }

            cout << "Sum : " << sum << endl;//output the sum

    }//else end

    return 0;
}//main end
