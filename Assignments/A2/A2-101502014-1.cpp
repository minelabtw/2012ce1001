#include<iostream>
using namespace std;
void exchange(int &a,int &b) // a function of exchanging two numbers
{
    int temp = a;
    a = b;
    b = temp;
}
int main()
{
    int integer1,integer2,integer3;
    cout << "Enter the integer 1: ";
    cin >> integer1;
    cout << "Enter the integer 2: ";
    cin >> integer2;
    cout << "Enter the integer 3: ";
    cin >> integer3;
    if( integer1 > integer2 )//if integer1 is greater than integer2, exchange integer1 and integer2
        exchange(integer1,integer2);
    if( integer1 > integer3 )//if integer1 is greater than integer3, exchange integer1 and integer3
        exchange(integer1,integer3);
    if( integer2 > integer3 )//if integer2 is greater than integer3, exchange integer2 and integer3
        exchange(integer2,integer3);
    cout << "The sort of the 3 integers : " << integer1 << " " << integer2 << " " << integer3;
}

