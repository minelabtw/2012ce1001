#include <iostream>
using namespace std;
int main()
{
    int start,end,divider,sum,number;

    cout << "Enter the start : ";
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;
    cout << "Enter the divider : ";
    cin >> divider;

    sum = 0;//定一開始sum為0
    number = start;//定number介於start與end之間,此訂number初值為start

    if( start < end )//檢查number是否小於end
    {
        while ( number <= end )//當number <= end時進入while迴圈
    {
        if (number % divider == 0)//當number被divider整除時
           sum = sum + number;//加總
           number++;//number持續加1直至number = end
    }
       cout << "Sum : " << sum << endl;
    }

    else//當start大於end時,執行以下
       cout << " Warning! Start need to smaller than end. ";

   return 0;
}
