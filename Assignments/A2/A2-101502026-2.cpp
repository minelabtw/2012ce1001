#include <iostream>

using namespace std;

int main()
{
    int x,y,z,sum,i=0; //declare five variables to be integers,and variable"i" starts from zero

    cout << "Enter the start : ";
    cin >> x; //enter the first variable
    cout << "Enter the end : ";
    cin >> y; //enter the second variable
    cout << "Enter the divider : ";
    cin >> z; //enter the third variable(divider)

    while ( x<=y ) //set a loop,of which the condition is that x is smaller than or equal to y
    {
        sum = x%z; //sum is the value of the remainder of x/z
        if( sum == 0 )
        {
        i = i+x; //to sum up all the exact divisors
        }
        x++; //the value of x increases,and then the variable enters the loop again
    }
    cout << "Sum : " << i; //print out the result(the sum of all the exact divisors between x and y,which contain x and y) on the screen
    return 0;
}
