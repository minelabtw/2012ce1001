#include <iostream>
using namespace std;

// function main begins program execution
int main()
{
    int a,b,c,buffer; // 假設三個變數和一個暫存數

    cout << "Enter the integer 1: "; // display message
    cin >> a;
    cout << "Enter the integer 2: ";
    cin >> b;
    cout << "Enter the integer 3: ";
    cin >> c;

    if ( a > b ) // 比較兩數大小，小的提前
    {
        buffer = a;
        a = b;
        b = buffer;
    }
    if ( a > c ) // 比較兩數大小，小的提前
    {
        buffer = a;
        a = c;
        c = buffer;
    }
    if ( b > c ) // 比較兩數大小，小的提前
    {
        buffer = b;
        b = c;
        c = buffer;
    }

    cout << "The sort of the 3 integers : "; // 輸出(The sort of the 3 integers :)在螢幕上
    cout << a << " " << b << " " << c << endl; // 依序由小到大輸出比較之後的結果

    return 0; // indicate that program ended successfully
} // end function main
