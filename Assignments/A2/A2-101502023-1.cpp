#include <iostream>
using namespace std;

int main()
{
     int a,b,c,i;

     cout << "Enter the integer 1: ";
     cin >> a;
     cout << "Enter the integer 2: ";
     cin >> b;
     cout << "Enter the integer 3: ";
     cin >> c;

     if( a > b )//if a is greater than b,exchange a and b
     {
           i = a;
           a = b;
           b = i;
     }

     if( a > c )//if a is greater than c,exchange a and c
     {
           i = a;
           a = c;
           c = i;
     }
     if( b > c )//if b is greater than c,exchange b and c
     {
           i = b;
           b = c;
           c = i;
     }

     cout << "The sort of 3 integers : ";
     cout << a << " " << b << " " << c << endl;

     return 0;
}
