#include<iostream>

using namespace std ;

int main(){
    int start,end,divider,sum;

    cout << "Enter the start :";
    cin  >> start ;
    cout << "Enter the end :";
    cin  >> end ;
    cout << "Enter the divider :";
    cin  >> divider;
    sum = 0; //  sum總和起始值為0 

    for( int i =start ; i<=end ; i++){
         if( i%divider == 0 ){
             sum += i;  // 當 i 可被 divider 除 加進 sum
         }
    }

    cout << sum;

    return 0;
}
