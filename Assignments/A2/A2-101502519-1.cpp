#include <iostream>

using namespace std;

int main()
{
    int i1,i2,i3,b;

    cout<<"Enter the integer 1: ";
    cin>>i1;
    cout<<"Enter the integer 2: ";
    cin>>i2;
    cout<<"Enter the integer 3: ";
    cin>>i3;

    if(i1>i2)    //讓i2比i1大
    {
        b=i2;
        i2=i1;
        i1=b;
    }

    if(i2>i3)    //讓i3比i2大
    {
        b=i3;
        i3=i2;
        i2=b;
    }

    if(i1>i2)    //若i2又比i1大  再交換
    {
        b=i2;
        i2=i1;
        i1=b;
    }

    cout<<"The sort of the 3 integers : ";
    cout<<i1<<"  "<<i2<<"  "<<i3<<endl;
    return 0;
}
