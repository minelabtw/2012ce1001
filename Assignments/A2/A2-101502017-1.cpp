#include <iostream>

using namespace std;

int main()
{
    int a,b,c,x;

    cout << "Enter the integer 1: " ;
    cin >>a;
    cout << "Enter the integer 2: " ;
    cin >>b;
    cout << "Enter the integer 3: " ;
    cin >>c;

    for(int i=1;i<3;i++)//3個數字只需執行兩次
     {
        if(a>b)//否則b>a,條件成立使a,b交換
        {
            x=a;
            a=b;
            b=x;
        }
        if(b>c)//否則c>b,條件成立使b,c交換
        {
            x=c;
            c=b;
            b=x;
        }
     }
    cout <<"The sort of the 3 integers : "<<a<<" "<<b<<" "<<c;
    return 0;
}
