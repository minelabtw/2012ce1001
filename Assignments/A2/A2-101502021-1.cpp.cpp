#include <iostream>

using namespace std;

int main()
{
    int interger1,interger2,interger3 ; // number entered by user
    int buffer;  // be used to be in place of number

    cout << "Enter the interger1: " ; // prompt for input
    cin >> interger1 ; // input number1
    cout << "Enter the interger2: " ; // prompt for input
    cin >> interger2 ; // input number2
    cout << "Enter the interger3: " ; // prompt for input
    cin >> interger3 ; // input number3

    if (interger1 > interger2) // the condition1
    {
        buffer = interger1 ;
        interger1 = interger2 ;
        interger2 = buffer ;
    } // end if
    // be used to change interger1 to be interger2

    if (interger1 > interger3) // the condition2
    {
        buffer = interger1 ;
        interger1 = interger3 ;
        interger3 = buffer ;
    } // end if
    // be used to change interger1 to be integer3

    if (interger2 > interger3) // the condition3
    {
        buffer = interger2 ;
        interger2 = interger3 ;
        interger3 = buffer ;
    } // end if
    // be used to change interger2 to be interger3

    cout << "The sort of 3 intergers : " ; // prompt for sort of 3 intergers
    cout << interger1 << " " << interger2 << " " << interger3 ; // display the sort of 3 intergers


    return 0 ;
} // end main
