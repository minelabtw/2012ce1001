#include<iostream>
using namespace std;

int main()
{
    int start,end,divider,sum=0;

    cout << "Enter the start : ";
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;
    cout << "Enter the divider : ";
    cin >> divider;

    for( int i=start ; i<=end ; i++ )//設一變數i,會在start和end之間變動,幅度為+1
       {
           if(i%divider==0)//若有能被divider整除的數則加總到sum裡頭
              sum = sum + i;
       }
    cout << "Sum : " << sum << endl;//最後顯示sum的結果

    return 0;

}
