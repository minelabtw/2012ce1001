#include<iostream>
#include<limits>
using namespace std;


void input(int &in);
void swap(int &a,int &b);
void bubblesort(int *array,int len);


int main()
{
    int integer[3]={0};


    //input
    for(int i=0;i<3;i++)
    {
        cout<<"Enter the integer "<<i+1<<": ";
        input(integer[i]);
    }


    bubblesort(integer,3);


    //output
    cout<<"The sort of the 3 integers : ";
    for(int i=0;i<3;i++)
        cout<<integer[i]<<' ';


    return 0;
}




void input(int &in)
{
    bool error;
    do
    {
        cin>>in;

        //input exception
        cin.clear();//clear error message;
        cin.ignore(numeric_limits<streamsize>::max(),'\n');//clear input buffer
        error=0;
        if(cin.gcount()!= 1)//not integer
        {
            error=1;
            cout<<"Error, please enter a number : ";
        }
    }while(error);
}

void swap(int &a,int &b)
{
    int tmp;
    tmp=a;
    a=b;
    b=tmp;
}

void bubblesort(int *array,int len)
{
    for(int i=0;i<len-1;i++)
        for(int j=0;j<len-1-i;j++)//put the greater one backward
            if(array[j]>array[j+1])
                swap(array[j],array[j+1]);
}
