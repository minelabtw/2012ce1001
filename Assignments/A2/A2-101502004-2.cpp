#include <iostream>

using namespace std;

int main()
{
    int start,end,i,divider,sum;//設定變數

    cout << "Enter the start : ";//第一行要顯示的字串
    cin >> start;//第一行裡要輸入的第一個變數

    cout << "Enter the end : ";//第二行要顯示的字串
    cin >> end;//第二行裡要輸入的第二個變數

    cout << "Enter the divider : ";//第三行要顯示的字串
    cin >> divider;//第三行裡要輸入的第三個變數

    i = start-1;//i是要丟入迴圈的起始值
    sum = 0;//sum是總結，以0當起始值
    while (i <= end-1)//要開始迴圈了，i的條件是要小於end
    {
        i += 1;//先找出start到end裡面到底有哪幾個數
        if (0 == i%divider)//用if做選擇，條件是要能夠被divider整除
        {
            sum += i;//如果if的條件成立，結果要把這些數i加起來進去sum這個新的變數
        }
    }

    cout << "Sum : " << sum << endl;//最後就是進行sum輸出

    return 0;
}
