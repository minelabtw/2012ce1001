#include <iostream>

using namespace std;

int main()
{
    int s,e,d,i;

    cout << "Enter the start : ";
    cin >> s;
    cout << "\nEnter the end : ";
    cin >> e;
    cout << "\nEnter the divider : ";
    cin >> d;

    int sum = 0;//sum is begin from 0
    for(i=s;i<=e;i++)//i is between s and e
    {
        if (i%d==0)//to choose the i which should be add on
        sum=sum+i;//add i
    }
    cout << "\nSum : " << sum;

    return 0;
}
