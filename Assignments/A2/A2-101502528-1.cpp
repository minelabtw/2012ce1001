#include <iostream>
using namespace std;

int main()
{
    int integer1,integer2,integer3,buffer;

    cout << "Enter the integer 1: ";
    cin >> integer1;
    cout << "Enter the integer 2: ";
    cin >> integer2;
    cout << "Enter the integer 3: ";
    cin >> integer3;

    if( integer1 > integer2 ) // 若integer1大於integer2時,兩數交換位置
    {
        buffer = integer1;
        integer1 = integer2;
        integer2 = buffer;
    }

    if ( integer2 > integer3 ) if ( integer3 > integer1 ) // 若integer3介於integer1和integer2時,integer3和integer2交換位置
    {
        buffer = integer3;
        integer3 = integer2;
        integer2 = buffer;
    }

    if ( integer1 > integer3 ) // 若integer3小於integer1時,integer3換至integer1,integer1換至integer2,integer2換至integer3
    {
        buffer = integer3;
        integer3 = integer2;
        integer2 = integer1;
        integer1 = buffer;
    }

    cout << "The sort of 3 integers : ";
    cout << integer1 << " " << integer2 << " " << integer3 << endl;

    return 0;

}
