#include <iostream>

using namespace std;
int main()
{
    int x,y,z,i; //declare four variables to be integers
    cout << "Enter the integer 1: ";
    cin >> x; //enter the first variable
    cout << "Enter the integer 2: ";
    cin >> y; //enter the second variable
    cout << "Enter the integer 3: ";
    cin >> z; //enter the third variable

    if( x > y ) //if the first variable is greater than the second one,then exchange them
    {
        i = x;
        x = y;
        y = i;
    }
    if( x > z ) //if the first variable is greater than the third one,then exchange them
    {
        i = x;
        x = z;
        z = i;
    }
    if( y > z ) //if the second variable is greater than the third one,then exchange them
    {
        i = y;
        y = z;
        z = i;
    }
    cout << "The sort of the 3 integers : ";
    cout << x << " " << y << " " << z << endl; //print out the result on the screen
    return 0;
}
