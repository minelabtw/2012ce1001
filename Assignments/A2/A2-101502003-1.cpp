#include <iostream>

using namespace std; //下面可以省略std::

int main()
{
    int a,b,c,d; //四個數
    cout << "Enter the integer 1: "; //輸出文字
    cin >> a;
    cout << "Enter the integer 2: "; //輸出文字
    cin >> b;
    cout << "Enter the integer 3: "; //輸出文字
    cin >> c;
    if ( a>b ) //假設a>b
     { d=a;   //把a暫存在d
         if ( b>c ) // a>b>c的狀況
            {
              a=c;
              c=d;
            }
         else if (a>c) //a>c>b的狀況
             {
              a=b;
              b=c;
              c=d;
             }

         else //c>a>b的狀況
             {
              a=b;
              b=d;
             }
     }
     else //假設b>a
     {
         { d=b; //把b暫存到d
            if ( b>c ) //b>c>a的狀況
              {
                b=c;
                c=d;
              }
            else if (c>b) //c>b>a的狀況
               {
                b=a;
                a=d;
               }
         }
         if (a>c) //b>a>c的狀況
             {
               d=b;
               b=a;
               a=d;
             }

     }
    cout << "The sort of the 3 integers : "; //輸出文字
    cout << a << " " << b << " " << c << endl;
    return 0;
}
