#include <iostream>

using namespace std;

int main()
{
    int integer1,integer2,integer3,buffer;//設定變數

    cout << "Enter the integer 1: ";//第一行顯示的東西
    cin >> integer1;//第一個數字要輸入在這裡

    cout << "Enter the integer 2: ";//第二行要顯示的東西
    cin >> integer2;//第二個數字要輸入在這裡

    cout << "Enter the integer 3: ";//第三行要顯示的東西
    cin >> integer3;//第三個數字要輸入在這裡

    if ( integer1 > integer2 )//先比第一個跟第二個數字，如果一比二大的話
    {
        buffer = integer1;//buffer是個緩衝暫存第一個數字
        integer1 = integer2;//於是第一個變數的位置就空了下來，就可以放入第二個數字
        integer2 = buffer;//第二個變數也空出了位置，接著就可以把原本放在buffer的第一個數字放進去，就完成了交換
    }

    if ( integer1 > integer3 )//接下來因為要把第三個變數加進去，所以先比較第一個變數跟第三個變數，若是一比三大的話
    {
        buffer = integer1;
        integer1 = integer3;
        integer3 = buffer;//與上面同樣的方法做交換
    }

    if ( integer2 > integer3 )//但還需要跟第二個變數做比較，若是第二個變數大於第三個變數的話
    {
        buffer = integer2;
        integer2 = integer3;
        integer3 = buffer;//一樣做交換的動作
    }

    cout << "The sort of the 3 integers : ";//輸出要顯示的字串
    cout << integer1 << " " << integer2 << " " << integer3 << endl;//預設定是按照變數一、變數二、變數三，這樣的順序排列

    return 0;
}
