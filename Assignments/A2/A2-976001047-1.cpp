#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer1,integer2,integer3,buffer;  

    cout << "Enter the integer 1: ";
    cin >> integer1;
    cout << "Enter the integer 2: ";
    cin >> integer2;
    cout << "Enter the integer 3: ";
    cin >> integer3;                  // enter three integer by user

    if( integer1 > integer2 )         //if integer1 > integer2 
    {                                 
        buffer = integer1;            //transfer their number
        integer1 = integer2;
        integer2 = buffer;
       
       if (integer2 > integer3)       //if integer2(be transfered) > integer3
       {
          integer2 = integer3;        //transfer their number make integer2 < integer3 
          integer3 = buffer;
          
          if (integer1 > integer2)    // compair the new integer2 (original integer3) and integer1
          {                           // if new integer2 > integer3. tranfer them 
             buffer = integer1;
             integer1 = integer2;
             integer2 = buffer;
          }    
       } 
    } 
    else                            // at first time integer1 < integer2
      if (integer2 > integer3)      // so we just compere integer2 and integer3
       {                            // if integer2 > integer3 . tranfer them   
          buffer = integer2;
          integer2 = integer3;
          integer3 = buffer;         
       } 
                     
    cout << "The sort of the 3 integers : ";   
    cout << integer1 << " " << integer2 << " " << integer3 <<endl;  // print the answer we need 

    system("PAUSE");                       // keep these word on screen until key any word
    return 0;                              // end program

}
