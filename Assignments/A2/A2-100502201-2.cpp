#include<iostream>
using std::cout;
using std::cin;
using std::endl;
//Shorten the code length

int main()
{
    int a,b,c,d;
    d=0; //Initialize sum as zero
    cout<<"Enter the start : ";
    cin>>a;
    cout<<"Enter the end : ";
    cin>>b;
    cout<<"Enter the divider : ";
    cin>>c;
    //a,b,c,d use for start,end,divider and sum

    if(a<=b)
    {
        while(a%c!=0)
        {
            a++;
        }
        //Find the nearest integer from start that can be divided by the divider
        while(a<=b)
        {
            d+=a;
            a+=c;
        }
        //Add the divider to the integer we find and calculate the sum
            cout<<"Sum : "<<d;
    }
    else
    {
        cout<<"Start should be smaller than End."<<endl; //Satisfy the given condition
    }

    return 0;
}
