#include<iostream> 

using namespace std;

int main()
{
    int num1,num2,num3,A;                   // 設定三個變數以及一個備用 
    
    cout << "please input 1st integer : ";  // 提示輸入者輸入變數 
    cin >> num1;
    
    cout << "please input 2nd integer : ";
    cin >> num2;     
    
    cout << "please input 3th integer : ";
    cin >> num3;
    
    while (num1 > num2)                      // 只要當第一個數大於第二個數，則將第一個數移到備用空間，且全部平移往前 
    {   A=num1;
        num1=num2;
        num2=num3;
        num3=A;     }
        
    while (num2 > num3)                      //  要當第二個數大於第三個數，則將第二個數移到備用空間，且全部平移往前
    {   A=num2;
        num2=num3;
        num3=A;     }
                
    cout << "after sorting , the three numbers are : " << endl;
    cout << num1 << " " << num2 << " " << num3 << endl;    // 最後由小排到大的結果 

    system("pause");                          // 系統中止 
    return 0;    
    
    }
