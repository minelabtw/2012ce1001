#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    int stnum,ednum,dinum,item,sum = 0;
    cout << " Enter the start : ";  
    cin >> stnum;                    // 輸入start
    cout << " Enter the end : ";    
    cin >> ednum;                    //輸入end
    
    while(stnum > ednum)                //偵測start的值是否大於end的值，如果大於的話，重新輸入
     {
        cout << "start的值不能大於end的值，請重新輸入：\n";
        cout << " Enter the start : ";  
        cin >> stnum;                    // 輸入start
        cout << " Enter the end : ";    
        cin >> ednum;                    //輸入end   
     }

    cout <<" Enter the divider : "; 
    cin >> dinum;                    //輸入divider
     
    if(stnum % dinum != 0 )          //如果start不能被divider整除，找到第一個在範圍內可以被 divider整除的數
	 stnum = stnum - (stnum % dinum) + dinum;
        
    if(ednum % dinum != 0 )          //如果end不能被divider整除，找到最後一個在範圍內可以被 divider整除的數
	 ednum = ednum - (ednum % dinum);
        
    item = (ednum-stnum)/dinum+1;    //在範圍內找到可被divider整除的數，形成一個公差為divider的等差數列,並加總
    sum = (stnum+ednum)*item/2;    
	 
    cout <<" Sum : "<< sum;
      
    system("pause");
    return 0;
}

