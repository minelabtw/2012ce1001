//101502503
#include <iostream>// allows program to output data to the screen

using namespace std;

// funtion main begins program execution
int main()

{
    int start, end, divider, total ; // variable declarations

    total = 0 ; // initialize total

    cout << "Enter the start : " ;
    cin >> start ; // input start
    cout << "Enter the end : " ;
    cin >> end ; // input end
    cout << "Enter the divider : " ;
    cin >> divider ; // input divider

    // for statement header includes initialization
    // loop-continuation condition and incremant
    for ( int counter = start ; counter <= end ; counter++ )
    {
        if ( counter % divider == 0)
    {
        total += counter ;
    } //end if
    } //end for

    cout << "Sum : " << total << endl ; // display result

    return 0 ; // indicate that program ended successfully
} // end main
