#include <iostream>
using namespace std;

int main()
{
    int start , end , divider , number , sum;
    sum = 0;

    cout << "Enter the start : ";
    cin >> start;

    cout << "Enter the end : ";
    cin >> end;

    if (start >= end ) // To prevent an error value
        cout << "Your value is wrong!";
    else
        cout << "Enter the divider : ";
        cin >> divider;

    number = start;

    while (number <= end) // Calculating the sum of the number that can be divisible
    {
        if (number % divider == 0)
            sum = number + sum;

        number = number + 1;
    }

    cout << "Sum : " << sum << endl;

    return 0;
}
