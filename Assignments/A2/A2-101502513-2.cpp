#include <iostream>
using namespace std;

int main()
{
    int start,end,divider,sum=0;

    cout << "Enter the start : ";
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;

    if( start < end ) //if start is smaller than end
    {
        cout << "Enter the divider : ";
        cin >> divider;

        //number loop from start to end
        for( int number = start ; number <= end ; number++ )
            {
                //number must be divided by divider equals 0
                if( number % divider == 0 )
                sum += number;
            }
        cout << "Sum : " << sum << endl;
    }
    else
        cout << "Error!!" << endl;//when start is greater than end, show "Error!!"

    return 0;
}
