#include<iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int x,y,z,t;
    cout << "Enter the start : ";//輸入一個數字為開始
    cin >> x;
    cout << "Enter the end : ";//輸入一個數字為結束
    cin >> y;
    cout << "Enter the divider : ";//輸入一個數字為除數
    cin >> z;
    t = 0;//初始總和
    while ( x <= y )//在x與y之間做循環
    {
        if ( x % z == 0)//x能被z整除
        t = t + x;//x能被z整除的總和
        x = x + 1;
    }
    cout << "sum : " << t << endl;
    return 0;
}
