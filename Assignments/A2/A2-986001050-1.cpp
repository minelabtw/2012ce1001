#include<iostream>
using namespace std;
int a,b,c,buffer1;  //宣告變數
int main()
{   //輸入三個整數為 a、b、c
    cout << "Enter the integer 1:" ;
    cin >> a;
    cout << "Enter the integer 2:" ;
    cin >> b;
    cout << "Enter the integer 3:" ;
    cin >> c;

    if (a>b)    //判斷是否第一數大於第二數
    {
        if (a>c)    //判斷第一數是否大於第三數
        {
            if (c>b)    //判斷第三數是否大於第二數 結果為 => a>c>b
            {
                buffer1=c;  //將c、b的值對調(因為a>c>b)
                c=b;
                b=buffer1;

                buffer1=a;  //將a、c的值對調(因為a>c>b)
                a=c;
                c=buffer1;
            }
            else        //若第三數小於第二數 結果為 => a>b>c
            {
                buffer1=a;  //將a、c的值對調(因為a>b>c)
                a=c;
                c=buffer1;
            }
        }
        else        //若第一數小於第三數 結果為 => c>a>b
        {
            buffer1=a; //a、b的值對調(因為c>a>b)
            a=b;
            b=buffer1;
        }
    }
    else    //判斷第二數是否大於第三數
    {
        if (c>a)    //判斷第三數是否大於第一數
        {
            if (b>c)    //判斷第二數是否大於第三數 結果為 => b>c>a
            {
                buffer1=b;  //b、c的值對調(因為b>c>a)
                b=c;
                c=buffer1;
            }
            //若第二數小於第三數 結果為 =>c>b>a 不須改順序
        }
        else        //若第三數小於第一數 結果為=> b>a>c
        {
            buffer1=a;      //a、b的值對調(因為b>a>c)
            a=b;
            b=buffer1;

            buffer1=a;      //a、c的值對調(因為b>a>c)
            a=c;
            c=buffer1;
        }
    }
    cout << "The sort of the 3 integers : "; //將順序由小到大的三個整數印出
    cout << a <<" "<< b<<" "<< c;
    return 0;
}
