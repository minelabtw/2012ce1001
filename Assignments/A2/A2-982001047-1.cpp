#include<iostream>

using namespace std ;

int main()
{
    int a,b,c,temp;

    cout << "Enter the integer 1:";
    cin >> a ;
    cout << "Enter the integer 2:";
    cin >> b ;
    cout << "Enter the integer 3:";
    cin >> c ;
    
    if( a > b ){  // 前2項排序
        temp = a ;
        a = b ;
        b = temp ;     
    }

    if( b > c ){  // 後2項排序
        temp = b ;
        b = c ;
        c = temp ;
    }

    if( a > b ){  // 前2項排序
        temp = a ;
        a = b ;
        b = temp ;
    }
    
    cout << "The sort of the 3 integers :" << a << " " << b << " " << c ;
 
    
    return 0;
}
