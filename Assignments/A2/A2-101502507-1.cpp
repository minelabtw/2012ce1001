//Sort 3 integers which entered by user from smallest to greatest.
#include <iostream>
using namespace std;

int main(){
    int integer1,integer2,integer3;
    cout << "Enter the integer 1: ";
    cin >> integer1;
    cout << "Enter the integer 2: ";
    cin >> integer2;
    cout << "Enter the integer 3: ";
    cin >> integer3;
    cout << "The sort of the 3 integers : ";

    //discuss all the situations
    if( integer1 > integer2 ){
        if( integer3 > integer1 )
        cout << integer2 << " " << integer1 << " " << integer3;
        else if( integer3 > integer2 )
        cout << integer2 << " " << integer3 << " " << integer1;
        else
        cout << integer3 << " " << integer2 << " " << integer1;
    }//if end

    else{//integer1 < integer2
        if( integer3 > integer2 )
        cout << integer1 << " " << integer2 << " " << integer3;
        else if( integer3 > integer1 )
        cout << integer1 << " " << integer3 << " " << integer2;
        else
        cout << integer3 << " " << integer1 << " " << integer2;
    }//else end

    return 0;
}
