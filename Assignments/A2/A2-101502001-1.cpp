#include <iostream>

using namespace std;

int main()
{
    int integer1,integer2,integer3,buffer;//buffer是暫存數
    cout << "Enter the integer 1: ";
    cin >> integer1;//放入integer1
    cout << "Enter the integer 2: ";
    cin >> integer2;//放入integer2
    cout << "Enter the integer 3: ";
    cin >> integer3;//放入integer3

    if( integer1 > integer2 )//if integer1 is greater than integer2, exchange integer1 and integer2
        {
        buffer = integer1;
        integer1 = integer2;
        integer2 = buffer;
        }

    if( integer1 > integer3 )//if integer1 is greater than integer3, exchange integer3 and integer1
        {
         buffer = integer1;
         integer1 = integer3;
         integer3 = buffer;
        }

    if( integer2 > integer3 )//if integer2 is greater than integer3, exchange integer2 and integer3
        {
         buffer = integer2;
         integer2 = integer3;
         integer3 = buffer;
        }

        cout << "The sort of the 3 integers : ";
        cout << integer1 << " " << integer2 << " " << integer3 << endl;
    return 0;
}
