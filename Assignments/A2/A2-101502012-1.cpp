#include <iostream>
using namespace std;

int main()
{
    int integer1,integer2,integer3,buffer; //interger1~3 are enter by user. buffer is empty.

    cout << "Enter the integer 1: ";
    cin >> integer1;
    cout << "Enter the integer 2: ";
    cin >> integer2;
    cout << "Enter the integer 3: ";
    cin >> integer3;

    if( integer1 > integer3 ) // If interger1>interger3, changed two of them
    {
        buffer = integer1;
        integer1 = integer3;
        integer3 = buffer;
    }
    if( integer1 > integer2 ) // If interger1>interger2, changed two of them
    {
        buffer = integer1;
        integer1 = integer2;
        integer2 = buffer;
    }
    if( integer2 > integer3 ) // If interger2>interger3, changed two of them
    {
        buffer = integer2;
        integer2 = integer3;
        integer3 = buffer;
    }
    //All of things above make integer1<integer2<integer3.
    cout << "The sort of the 3 integers : ";
    cout << integer1 << " " << integer2 << " " << integer3 << endl;

    return 0;
} //end main
