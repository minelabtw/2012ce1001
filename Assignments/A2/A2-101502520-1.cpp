#include <iostream>
using namespace std;
void exchange(int& a,int& b)//swap函式、使用參考宣告int&
{
    int buffer;
    buffer=a;
    a=b;
    b=buffer;
}
int main()
{
    int interger1,interger2,interger3;
    cout<<"Enter the integer 1: ";
    cin>>interger1;
    cout<<"Enter the integer 2: ";
    cin>>interger2;
    cout<<"Enter the integer 3: ";
    cin>>interger3;
    if(interger1>interger2)//判斷是否交換第一與第二
        exchange(interger1,interger2);
    if(interger2>interger3)//判斷是否交換第二與第三
        exchange(interger2,interger3);
    if(interger1>interger2)//判斷是否再交換第一與第二
        exchange(interger1,interger2);
    cout << "The sort of the 3 integers : "<<interger1<<" "<<interger2<<" "<<interger3<< endl;
    return 0;
}
