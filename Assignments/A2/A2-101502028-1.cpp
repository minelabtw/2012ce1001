#include <iostream>
using namespace std;
int main()
{
    int i1, i2, i3, buffer;
        cout << "Enter the integer 1: ";
        cin >> i1;
        cout << "Enter the integer 2: ";
        cin >> i2;
        cout << "Enter the integer 3: ";
        cin >> i3;

    if ( i1 > i2 ) // if i1 is bigger than i2
    {
        buffer = i1;
        i1 = i2; // then i1 change the place with i2
        i2 = buffer; // this makes i1 bigger than i2
    }

    if ( i1 > i3 ) // if i1 is bigger than i2
    {
        buffer = i1;
        i1 = i3; // then i1 change the place with i3
        i3 = buffer; // this makes i1 the biggest integer so it goes in the last
    }

    if ( i2 > i3 ) // if i2 is bigger than i3
    {
        buffer = i2;
        i2 = i3; // then i2 change the place with i3
        i3 = buffer; // this makes i2 bigger than i3 so it goes in the middle and i3 the first
    }

    cout << "The sort of the 3 integers : ";
    cout << i1 << " " << i2 << " " << i3 << endl;

    return 0;
}
