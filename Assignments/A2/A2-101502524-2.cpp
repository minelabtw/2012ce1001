#include <iostream>
using namespace std;
int main()
{
    int start,end,divider,sum;
    sum=0;

    //input
    cout<<"Enter the start : ";
    cin>>start;
    cout<<"Enter the end : ";
    cin>>end;
    cout<<"Enter the divider : ";
    cin>>divider;

    //exception
    if(start>end)
    {//exchange start with end
        int t;
        t=start;
        start=end;
        end=t;
    }

    for( ;start<=end;start++)//check every number from start to end
    {
        if(start%divider==0)//add all the number that can be divided by divider
        {
            sum=sum + start;
        }
    }

    cout<<"Sum : "<<sum<<endl;

    return 0;
}
