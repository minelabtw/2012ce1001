#include <iostream>
using namespace std;
int main()
{
  int integer1,integer2,integer3,buffer;

  cout << "Enter the integer 1: ";//輸入數字1
  cin >> integer1;
  cout << "Enter the integer 2: ";//輸入數字2
  cin >> integer2;
  cout << "Enter the integer 3: ";//輸入數字3
  cin >> integer3;

  if ( integer3 > integer2 )//判別int1,int2大小
  {
    buffer = integer3;//條件符合時交換int1,int3字串
    integer3 = integer2;
    integer2 = buffer;
  }
  if ( integer3 > integer1 )//判別int1,int3大小
  {
    buffer = integer3;//條件符合時交換int1,int3字串
    integer3 = integer1;
    integer1 = buffer;
  }
  if ( integer2 > integer1 )//判別int2,int3大小
  {
    buffer = integer2;//條件符合時交換int1,int3字串
    integer2 = integer1;
    integer1 = buffer;
  }/*14、20、26 if 內的位置調換必須要把34行integer的順序變換
     否則執行結果會發生順序混亂 */

  cout << "The sort of the 3 integers : " << integer3 << " " << integer2 << " " <<integer1;
  return 0;
}
