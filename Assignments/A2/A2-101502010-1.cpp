#include <iostream>
using namespace std;
int main()
{
    int integer1,integer2,integer3,buffer;//宣告變數

    cout << "Enter the integer 1: ";
    cin >> integer1;//輸入整數一
    cout << "Enter the integer 2: ";
    cin >> integer2;//輸入整數二
    cout << "Enter the integer 3: ";
    cin >> integer3;//輸入整數三

    if( integer1 > integer2 )//比較整數一跟二
    {
        buffer = integer1;
        integer1 = integer2;
        integer2 = buffer;//將較小的數置換到前面
    }
    if( integer2 > integer3 )//比較整數二跟三
    {
        buffer = integer2;
        integer2 = integer3;
        integer3 = buffer;//將較小的數置換到前面
    }
    if( integer1 > integer2 )//再次比較整數一跟二
    {
        buffer = integer1;
        integer1 = integer2;
        integer2 = buffer;//將較小的數置換到前面
    }


    cout << "The sort of the 3 integers : ";
    cout << integer1 << " " << integer2 <<  " " << integer3 << endl;//輸出結果

    return 0;

}

