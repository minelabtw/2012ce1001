#include <iostream>
//a2-2
using namespace std;

int main ()
{
    int start ;
    int end , divider ;
    double sum =0 ;
    //use "double" to avoid overflow

    cout << "Enter the start : ";
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;
    cout << "Enter the divider : ";
    cin >> divider;

    if(start%divider != 0)
    {
        start=(start+divider-(start%divider));
    }
    end=end-(end%divider);
    //redefine the "start" and "end" to use arithmetic series formula
    cout << "Sum : " << 0.5*(start+end)*(1+(end-start)/divider);
    //"0.5*(start+end)*(1+(end-start)/divider)" is the arithmetic series formula

    return 0;
}
