#include <iostream>
//A2-1
using namespace std;

int main()
{
    int integer1,integer2,integer3,buffer;//buffer is used to swich the numbers

    //input variables
    cout << "Enter the integer 1: ";
    cin >> integer1;
    cout << "Enter the integer 2: ";
    cin >> integer2;
    cout << "Enter the integer 3: ";
    cin >> integer3;

    //use while loop to do the "swich"
    while (integer1 > integer2 || integer2 > integer3)
    //make sure that the order is correct
    {
        if( integer1 > integer2 )//swich integer1 and integer2
        {
            buffer = integer1;
            integer1 = integer2;
            integer2 = buffer;
        }

        if( integer2 > integer3 )//swich integer2 and integer3
        {
            buffer = integer2;
            integer2 = integer3;
            integer3 = buffer;
        }
    }

        //print the result
        cout << "The sort of the 3 integers : ";
        cout << integer1 << " " << integer2 << " " << integer3 << endl;

        return 0;

}
