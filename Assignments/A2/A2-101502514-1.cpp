#include <iostream>
using namespace std;
int main()
{
    int i,j,k,buffer;

    cout << "Enter the integer 1: ";
    cin >> i ; // input  integer1
    cout << "Enter the integer 2: ";
    cin >> j ; // input  integer2
    cout << "Enter the integer 3: ";
    cin >> k ; // input  integer3

    if( i>j )// i大於j時，i與j互換
    {
        buffer=i;
        i=j;
        j=buffer;
    }
    if( i>k )// i大於k時，i與k互換
    {
        buffer=i;
        i=k;
        k=buffer;
    }
    if( j>k )// j大於k時，j與k互換
    {
        buffer=j;
        j=k;
        k=buffer;
    }
    cout << "The sort of the 3 integers : ";
    cout << i << " " << j << " " << k << endl;
    return 0;
}
