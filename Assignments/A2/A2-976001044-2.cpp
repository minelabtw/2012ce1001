// Assignment #2-2
// A2-976001044-2.cpp 
#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    // Initialization phase
    // Declare four integer start, end, divider and temp
    // And another integer sum which is assigned initial value zero
    int start=0, end=0, divider=0, temp=0;
    int sum=0;
    
    // Input phase
    // Which is according to assignment require
    cout << "Enter the start : ";
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;
    cout << "Enter the divider : ";
    cin >> divider;
    
    // 判斷 Input data 大小錯置之情況，若判斷式成立， 則將兩者之值交換 
    // 因後面迴圈設定上的關係，必須確保變數 start 值為小、變數 end 值為大 
    if ( end < start )
    {
         temp = end;
         end = start;
         start = temp;
    }
    
    // Functional phase
    // 藉由 for loop 和 if 判斷式，篩選輸入範圍內所有符合條件的值，並將其加總
    // 篩選方式為：除以 divider 之餘數為零 ( i.e. 整除 ) 
    // sum 變數已給定初始值為零，故判斷式成立，即可直接作加總之動作 
    for ( int i=start ; i <= end ; i++ )
    {
        if ( (i % divider) == 0 )
        {
             sum = sum + i;
        }
    }
    
    // Output phase
    // Which is according to assignment require
    cout << "Sum : " << sum << endl;
    system("pause");
    return 0;
}
