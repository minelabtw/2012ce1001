#include <iostream>
using namespace std;
int main()
{
    int integer1,integer2,integer3,buffer;

    cout << "Enter the integer 1: ";
    cin >> integer1;
    cout << "Enter the integer 2: ";
    cin >> integer2;
    cout << "Enter the integer 3: ";
    cin >> integer3;

    if( integer2 > integer3 )//當integer2大於integer3時,交換兩者位置
    {
        buffer = integer2;
        integer2 = integer3;
        integer3 = buffer;
    }

    if( integer1 > integer2 )//當integer1大於integer2時,交換兩者位置
    {
        buffer = integer1;
        integer1 = integer2;
        integer2 = buffer;
    }

    if( integer2 > integer3 )//再此比較integer2及integer3,當integer2大於integer3時,交換兩者位置
    {
        buffer = integer2;
        integer2 = integer3;
        integer3 = buffer;
    }

    cout << "The sort of the 3 integers : ";
    cout << integer1 << " " << integer2 << " " << integer3 << endl;

    return 0;
}
