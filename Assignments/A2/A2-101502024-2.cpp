#include <iostream>
using namespace std;

int main()
{
    int start,end,divider,n1,total=0;//Declare the integers that will be used in the program and make total = 0.

   cout << "Enter the start : ";//Show line.
   cin >> start;//Input start.
   cout << "Enter the end : ";//Show line.
   cin >> end;//Input end.
   cout << "Enter the divider : ";//Show line.
   cin >> divider;//Input divider.

   while(start <= end)//Run the program repeatdly when the volume of start is still between the original start and end.
   {
       n1 = start % divider;

       if( n1 == 0 )//Judge if start can be divided by the divider or not.
       {
           total = total + start;//Add the volume of start that can be divided by divider to total.
       }
start++;//Start will be increased by one everytime the "while" part of the program is completed.
   }
std::cout << "Sum : " << total << endl;//Show line and the result.
return 0;
}
