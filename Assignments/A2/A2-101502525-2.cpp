#include<iostream>
#include<limits>
using namespace std;


void input(int &in);
void swap(int &a,int &b);


int main()
{
    int start=0,end=0,divider=0;


    //input
    cout<<"Enter the start : ";
    input(start);
    cout<<"Enter the end : ";
    input(end);
    cout<<"Enter the divider : ";
    input(divider);
    //exception
    if(start>end)
        swap(start,end);
    while(divider==0)
    {
        cout<<"Error, please enter other number : ";
        input(divider);
    }


    //find the first and the last number which is divisible by divider
    if(start%divider!=0)
        start+=-start%divider+divider;
    if(end%divider!=0)
        end-=end%divider;


    //output arithmetic series
    cout<<"Sum : "<<(start+end)*((end-start)/divider+1)/2;


    return 0;
}




void input(int &in)
{
    bool error;
    do
    {
        cin>>in;

        //input exception
        cin.clear();//clear error message;
        cin.ignore(numeric_limits<streamsize>::max(),'\n');//clear input buffer
        error=0;
        if(cin.gcount()!= 1)//not integer
        {
            error=1;
            cout<<"Error, please enter a number : ";
        }
    }while(error);
}

void swap(int &a,int &b)
{
    int tmp;
    tmp=a;
    a=b;
    b=tmp;
}
