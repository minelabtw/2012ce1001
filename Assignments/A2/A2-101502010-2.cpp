#include <iostream>
using namespace std;
int main()
{
    int start,end,divider,sum=0;//宣告變數
    cout << "Enter the start : ";
    cin >> start;//輸入開頭
    cout << "Enter the end : ";
    cin >> end;//輸入結尾
    cout << "Enter the divider : ";
    cin >> divider;//輸入除數
    for (start;start<=end;start++)//設定由開頭至結尾的迴圈
    {
        if (start%divider==0)
            sum+=start;//若整除則加總
    }
    cout << "Sum: " << sum << endl;//輸出結果
    return 0;
}
