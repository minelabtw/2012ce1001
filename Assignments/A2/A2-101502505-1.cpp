#include <iostream>//允許程序在屏幕上輸入和輸出
using namespace std;//使用名稱空間std下的名稱
int main()
{
    int a,b,c;//宣告a,b,c為三整數
    cout << "Enter the integer 1: ";//顯示到畫面中
    cin >> a;//輸入第一個數
    cout << "Enter the integer 2: ";//顯示到畫面中
    cin >> b;//輸入第二個數
    cout << "Enter the integer 3: ";//顯示到畫面中
    cin >> c;//輸入第三個數

    if(a > b){//當a>b
        if(b > c)//此時a>b且b>c
            cout << "The sort of the 3 integers : " << c << " " << b << " " << a <<endl;//顯示由左到右數字逐漸變大
        else if(a > c)//此時a>b且c>b又a>c
            cout << "The sort of the 3 integers : " << b << " " << c << " " << a <<endl;//顯示由左到右數字逐漸變大
        else//此時a>b且c>a
            cout << "The sort of the 3 integers : " << b << " " << a << " " << c <<endl;//顯示由左到右數字逐漸變大
    }
    else {if(c > b)//此時b>a且c>b
            cout << "The sort of the 3 integers : " << a << " " << b << " " << c <<endl;//顯示由左到右數字逐漸變大
          else if(c > a)//此時b>a且b>c又c>a
            cout << "The sort of the 3 integers : " << a << " " << c << " " << b <<endl;//顯示由左到右數字逐漸變大
          else//此時b>a且a>c
            cout << "The sort of the 3 integers : " << c << " " << a << " " << b <<endl;//顯示由左到右數字逐漸變大
    }
    return 0;//回傳0表示程式正常結束
}
