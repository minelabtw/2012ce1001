#include<iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int a,b,c,buffer;

    cout<<"Enter the integer1 : ";
    cin>>a;
    cout<<"Enter the integer2 : ";
    cin>>b;
    cout<<"Enter the integer 3 : ";
    cin>>c;

    if(a>b)//如果a大於b就將a,b位置互換.
    {
        buffer=a;
        a=b;
        b=buffer;
    }
    if(a>c)//如果a大於c就將a,c位置互換.
    {
        buffer=a;
        a=c;
        c=buffer;
    }
    if(b>c)//如果b大於c就將b,c位置互換.
    {
        buffer=b;
        b=c;
        c=buffer;
    }
    cout<<"The sort of the 3 integers : ";
    cout<<a<<" "<<b<<" "<<c<<endl;

    return 0;
}
