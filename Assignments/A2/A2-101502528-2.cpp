#include <iostream>
using namespace std;

int main()
{
    int start,end,divider,sum;

    cout << "Enter the start : ";
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;
    cout << "Enter the divider : ";
    cin >> divider;

    sum = 0;

    while ( start <= end ) // 當一個介於start和end之間的整數若能被divider整除則加進去sum
    {
        if ( start % divider == 0 )
        sum = sum + start;
        start = start++;
    }

    cout << "Sum : " << sum << endl;

    return 0;

}
