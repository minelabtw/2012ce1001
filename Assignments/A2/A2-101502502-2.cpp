#include <iostream>
using namespace std;
int main()
{
    int num1;
    int num2;
    int divider;
    int sum;

    cout << "Enter the start : ";
    cin >> num1;

    cout << "Enter the end : ";
    cin >> num2;

    cout << "Enter the divider : ";
    cin >> divider;

    if ( num1 % divider == 0 && num2 % divider == 0 )
    {
        sum = ( num2 - num1 + divider )*( num1 + num2 )/( divider*2 ); // by math we can get the conclusion
        cout << "Sum : " << sum << endl;
    } // if end

    else
    {
        sum = ( num2 - ( num2%divider ) - num1 - divider + ( num1%divider ) + divider )*( num1 + divider - (num1%divider) +  num2 - (num2%divider) )/(divider*2);
        // adjust the modulus of num1 and num2, then put it into formula
        cout << "Sum : " << sum << endl;
    } // else end


    return 0;
} // main end
