#include <iostream>
using namespace std;
int start,end,divider,a;

int main()
{   //輸入三個整數 start、end、divider
    cout << "Enter the start : ";
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;
    cout << "Enter the divider : ";
    cin >> divider;
    if (start<end)  //預設 start 小於 end
    {
        for (int i=start ; i<=end ; i++)    //for loop 當i>end 的時候停止迴圈。
        {
            if (i%divider == 0)     //當此數能夠被divider整除時
            {
                a=a+i;              //把符合以上條件之數加起來
            }
        }
    cout << "Sum : " << a <<endl;   //印出此和值
    }
    else    //若是start 大於 end 則印出please enter the start bigger than the end.
        cout << "\n!!! please enter the start bigger than the end." ;
    return 0;
}
