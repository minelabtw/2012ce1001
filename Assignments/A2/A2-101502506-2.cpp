#include <iostream>

using namespace std;
int main()
{
    int start,end,divider; //宣告3個變數
    cout << " Enter the start :";
    cin  >> start; //使用者輸入start
    cout << " Enter the end :";
    cin  >> end; //使用者輸入end
    cout << " Enter the divider :";
    cin  >> divider; //使用者輸入divider
    int sum =0; //宣告sum的初始值=0

    for(int a=start;a<=end;a++)//宣告a的範圍從start到end
    {
        if(a%divider==0)
        sum+=a; //若a可以整除divider,把a全部加起來
    } //end for
    cout << " sum : " << sum << endl; //輸出結果
    return 0;
} //end main
