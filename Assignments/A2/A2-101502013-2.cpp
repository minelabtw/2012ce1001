#include <iostream>

using namespace std;

int main()
{
    int i1,i2,i3,sum=0,counter;
    cout << "Enter the start: " ;
    cin >> i1;
    counter=i1;
    cout << "Enter the end: " ;
    cin >> i2;
    while (i2<i1)   //判別end大小
    {
        cout << "請輸入大於start的整數" << endl;
        cout << "Enter the end: " << endl;
        cin >> i2;
    }
    cout << "Enter the divider: " ;
    cin >> i3;
    while (i3<i1 || i3>i2)   //判別divider大小
    {
        cout << "請輸入大於start且小於end的整數" << endl;
        cout << "Enter the divider: " << endl;
        cin >> i3;
    }
    while (counter <=i2)
    {
        if (counter % i3 == 0) //如果整數被整除則將該整數加入sum
            sum = sum + counter;
        counter++;
    }
    cout << "Sum : " << sum << endl;

    return 0;
}
