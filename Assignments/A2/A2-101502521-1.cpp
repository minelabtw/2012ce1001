#include <iostream>

using namespace std;

void swap(int*, int*);

int main()
{
    int a, b, c;
    cout << "Enter the integer 1: ";
    cin >> a;
    cout << "Enter the integer 2: ";
    cin >> b;
    cout << "Enter the integer 3: ";
    cin >> c;
    if(a>b) swap(a, b);
    if(b>c) swap(b, c);
    // above 2 line let c = max{a, b, c}
    if(a>b) swap(a, b); // let a = min{a, b, c}
    cout << "The sort of the 3 integers : " << a << " " << b << " " << c << endl;
    return 0;
}

void swap(int* a, int* b) // exchange a and b
{
    int t = *a;
    *a = *b;
    *b = t;
}
