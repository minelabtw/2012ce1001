#include<iostream>
using namespace std;//讓下面不用打std
int main()
{
    int start,end,divider,total=0;//設變數
    cout<<"Enter the start : ";//螢幕出現文字
    cin>>start;//讓使用者輸入start值
    cout<<"Enter the end : ";//螢幕出現文字
    cin>>end;//使用者輸入end值
    cout<<"Enter the divider : ";//螢幕出現文字
    cin>>divider;//使用者輸入divider值

    int product=start;//迴圈初始值
    while (product<=end)//迴圈判斷式
    {

        if ( product % divider == 0)//如果除以divider等於零
                total=total+product;//就把他加total

        product=product+1;//迴圈改變值

    }
    cout<<"Sum : "<<total<<endl;//螢幕顯示結果

    return 0;
}
