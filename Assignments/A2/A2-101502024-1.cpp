#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int integer1,integer2,integer3,buffer,buffer2;//The integers that will be used in the program.

    cout << "Enter the integer 1: ";//Show line.
    cin >> integer1;//Input integer 1.
    cout << "Enter the integer 2: ";//Show line.
    cin >> integer2;//Input integer 2.
    cout << "Enter the integer 3: ";//Show line.
    cin >> integer3;//Input integer 3.

    if( integer1 > integer2 )//Change the sequence of the two integers if integer 1 is bigger than integer 2.
    {
        buffer = integer1;
        integer1 = integer2;
        integer2 = buffer;
    }
    if( integer3 < integer1 )//Change the sequence of the two integers if integer 1 is bigger than integer 3.
    {
        buffer = integer1;
        integer1 = integer3;
        integer3 = buffer;
    }
    if( integer2 > integer3 )//Change the sequence of the two integers if integer 2 is bigger than integer 3.
    {
        buffer= integer2;
        integer2 = integer3;
        integer3 = buffer;
    }
    cout << "The sort of the 3 integers : ";//Show line.
    cout << integer1 << " " << integer2 << " " << integer3 << endl;//Output the result of the program.

    return 0;
}
