#include <iostream>

using namespace std;

int main()
{
    int start,end,divider,sum=0;
    cout << "Enter the start : ";
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;
    cout << "Enter the divider : ";
    cin >> divider;

    while (start <= end)   //using while to let start +1
    {
        if (start % divider == 0)  //if start can be divided then put it in sum
        {sum = sum + start;}

    start = start + 1;
    }

    cout << "Sum : " << sum << endl;  //show the sum
    return 0;
}
