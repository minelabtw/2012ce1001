#include <iostream>
using namespace std ;
int main()

{
    int integer1,integer2,integer3,buffer;//假設變數

    cout << "Enter the integer 1: ";//第一行顯示字串
    cin >> integer1;//輸入第一行所需變數
    cout << "Enter the integer 2: ";//第二行顯示字串
    cin >> integer2;//輸入第二行所需變數
    cout << "Enter the integer 3: ";//第三行顯示字串
    cin >> integer3;//輸入第三行所需變數

    if( integer1 > integer2 )//判斷int1、int2大小
    {
      buffer=integer1;//交換int1和int2的值
      integer1=integer2;
      integer2=buffer;
    }

    if( integer1 > integer3 )//交換int1和int3的值
    {
        buffer=integer1;
        integer1=integer3;
        integer3=buffer;
    }

    if( integer2 > integer3 )//交換int2和int3的值
    {
        buffer=integer2;
        integer2=integer3;
        integer3=buffer;


    }

    cout << "The sort of the 3 integers : ";//輸出
    cout << integer1 << " " << integer2 << " " << integer3 << endl;

    return 0;
}
