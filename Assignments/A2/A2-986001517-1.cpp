#include <iostream>
int main()
{
    int integer1,integer2,integer3,buffer;
    std::cout << "Enter the integer 1: ";
    std::cin >> integer1;
    std::cout << "Enter the integer 2: ";
    std::cin >> integer2;
    std::cout << "Enter the integer 3: ";
    std::cin >> integer3;
    if( integer1 > integer2 ) 
    {
        buffer = integer1;
        integer1 = integer2;
        integer2 = buffer;  
     }
    if( integer2 > integer3 )
    {
        buffer = integer2;
        integer2 = integer3;
        integer3 = buffer;   
     }
     std::cout << "The sort of the 3 integers : ";
    std::cout << integer1 << " " << integer2 << " " << integer3 << " " << std::endl;
    system("pause");
    return 0;
 }
