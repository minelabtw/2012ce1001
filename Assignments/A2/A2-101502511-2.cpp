#include <iostream>
using namespace std;
int main()
{
    int start,end,divider,a1,an,n;

    cout << "Enter the start : ";/*輸入變數*/
    cin >> start;
    cout << "Enter the end : ";
    cin >> end;
    cout << "Enter the divider : ";
    cin >> divider;

    if ( start < end ) //判斷start是否小於end
    {
        if ( start % divider == 0 )//判斷start是否被divider整除
            a1 = start;
        else
        {
            a1 = start + ( divider - (start%divider) );//計算大於start且最接近能夠被divider整除的數
        }
        an = end - ( end%divider );//計算小於end且最接近能夠被divider整除的數
        n = ( (an-a1) / divider ) + 1;//項數
        cout << "Sum : " << ( (a1+an)*n )/2 <<endl;//輸出結果
    }
    else
        cout << "錯誤!!start必須小於end"<<endl;
    return 0;
}
