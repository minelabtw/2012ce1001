#include <iostream>

using namespace std;

int main()
{
    int start,end,divider ; // numbers entered by user
    int sum=0 ; // initialize sum

    // total of those which can be divisible
    int A ; // the result of start % divider
    cout << "Enter the start : " ; // prompt for input
    cin >> start ; // input the number
    cout << "Enter the end : " ; // prompt for input
    cin >> end ; // input the number
    cout << "Enter the divider : " ; // prompt for input
    cin >> divider ; // input the number

    while (start <= end) // while the number of start <= the number of end
    {
        A = start % divider ; // define that A is the remainder of srart/divider
        if (A==0) // if the result of A is 0
        {
            sum += start; // add start to sum
        } //end if
        start++ ; // add 1 to start
    } // end while

    cout << "Sum : " << sum ; // prompt for the value of sum
    return 0 ;
} // end main
