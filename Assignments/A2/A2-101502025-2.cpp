#include <iostream>

using namespace std;

int main()
{
    int num1,num2,num3,sum;// num1=the "start",num2=the "end",num3=the "divider"
    sum=0;// let the original sum = 0

    cout<<"Enter the start : ";
    cin>>num1;// input the start

    cout<<"Enter the end : ";
    cin>>num2;// input the end

    cout<<"Enter the divider : ";
    cin>>num3;// input the divider

    while(num1<=num2)// try all the numbers between start and end
    {
      if(num1%num3==0)// if the number could be divided by the divider
        {
         sum=sum+num1;// add the number to sum
        }
      else
        sum=sum+0;//if the number do not fit the bill,do not add it to the sum
     num1=num1+1;//try the next number
    }
    cout<<"Sum : "<<sum<<endl;// output the sum
    return 0;
}
