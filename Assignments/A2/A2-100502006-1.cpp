#include <iostream>//含入標頭檔 iostream
#include <string>//含入標頭檔 string
#include <stdio.h>//含入標頭檔 stdio.h
#include <stdlib.h>//含入標頭檔 stdlib.h

using namespace std;//使用名稱空間 std

class CIntSort//定義類別
{
    public://可供一般的敘述或 function以 物件.資料成員 方式取用
        string strInteger[3];//陣列宣告，型別: string
        int iInteger[3];//陣列宣告，型別: int
        int iBuffer;//整數宣告，用以暫存
        void IntegerInput();//輸入函式宣告，型別: void
        void IntegerSort();//排序函式宣告，型別: void
        void IntegerOutput();//輸出函式宣告，型別: void
    private://僅可供 member function 直接取用
        bool CheckInput(string str);//檢查輸入函式宣告，型別: bool
};

bool CIntSort::CheckInput(string str)//定義檢查輸入函式
{
    {
            if(str.c_str()[0]!=45)//判斷首字是否為負號
             {
                for(int j = 0; j < str.length(); j++)//逐一檢查字串所有字元是否為數字
                {
                   if( str.c_str()[j] < '0' || str.c_str()[j] > '9' )
                   {
                       cout<<"Input error!"<<endl;
                       return true;//回傳 true
                   }
                }
                return false;//回傳 false
             }
             else
             {
               for(int j = 1; j < str.length(); j++)//逐一檢查字串第一字元後所有字元是否為數字
                {
                   if( str.c_str()[j] < '0' || str.c_str()[j] > '9' )
                   {
                       cout<<"Input error!"<<endl;
                       return true;//回傳 true
                   }
                }
               return false;//回傳 false
             }
        }
}

void CIntSort::IntegerInput()//定義輸入函式
{
    for(int i=0;i<3;i++)//輸入三個整數
    {
        do
        {
            cout << "Enter the integer "<<i+1<<": ";
            cin >> strInteger[i];//儲存輸入為字串型別
        }while(CheckInput(strInteger[i]));//作輸入檢查判斷
        iInteger[i]=atoi(strInteger[i].c_str());//將字串轉換為整數儲存
    }
}

void CIntSort::IntegerSort()//定義排序函式
{
     if(iInteger[0]>iInteger[1])//判斷第一個與第二個整數大小，適時交換以排序
    {
        iBuffer=iInteger[0];
        iInteger[0]=iInteger[1];
        iInteger[1]=iBuffer;
    }

    if(iInteger[1]>iInteger[2])//判斷第二個與第三個整數大小，適時交換以排序
    {
        iBuffer=iInteger[1];
        iInteger[1]=iInteger[2];
        iInteger[2]=iBuffer;

        if(iInteger[0]>iInteger[1])//判斷第一個與第二個整數大小，適時交換以排序
        {
            iBuffer=iInteger[0];
            iInteger[0]=iInteger[1];
            iInteger[1]=iBuffer;
        }
    }
}

void CIntSort::IntegerOutput()//定義輸出函式
{
    cout << "The sort of the 3 integers : ";
    cout << iInteger[0] << " " << iInteger[1] << " " << iInteger[2] <<endl;
}

int main()//主程式 型別: int
{
    CIntSort IntSort;//類別CIntSort 宣告
    IntSort.IntegerInput();//呼叫類別中輸入函式
    IntSort.IntegerSort();//呼叫類別中排序函式
    IntSort.IntegerOutput();//呼叫類別中輸出函式

    return 0;//回傳 0
}
