#include<iostream>
#include <stdlib.h>

using namespace std;

int main(){

	 int num1,num2,num3,rem;    //宣告變數
	 int sum=0;

	 cout << "Enter the start : ";  //輸入起始值
	 cin  >> num1;
 	 cout << "Enter the end : ";    //輸入終值
 	 cin  >> num2;
	 cout << "Enter the divider : ";  //輸入除數
     cin  >> num3;

	 while(num1<=num2){        //起始值若不大於終值則繼續動作

		 rem=num1%num3;        //起始值對除數求餘數
		 
		 if(rem==0)            //若餘數為零，則把當時的數值做相加
		   sum=sum+num1;
		   
		 num1++;               //讓起始值累加,直到終值
	 }

	 cout<<"sum : "<<sum<<endl;  //印出總和


	 system("pause");
	 return 0;
}
