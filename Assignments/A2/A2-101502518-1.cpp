#include <iostream>
using namespace std;

int main()
{
    int i1,i2,i3,x;

    cout << "Enter the integer 1: ";
    cin >> i1;
    cout << "Enter the integer 2: ";
    cin >> i2;
    cout << "Enter the integer 3: ";
    cin >> i3;

    if( i1 > i2 )
    {
        x = i1;
        i1 = i2;
        i2 = x;
    }
    if( i1 > i3 )
    {
        x = i1;
        i1 = i3;
        i3 = x;
    }
    if( i2 > i3 )
    {
        x = i2;
        i2 = i3;
        i3 = x;
    }


    cout << "The sort of the 3 integers : ";
    cout << i1 << " " << i2 <<" "<< i3 <<endl;

    return 0;

}
