#include<iostream>
using namespace std;
int main()
{
    int a,b,c;
    cout<<"Enter the start : ";
    cin>>a;
    cout<<"Enter the end : ";
    cin>>b;
    cout<<"Enter the divider : ";
    cin>>c;
    //迴圈我已明瞭 但是題目沒說一定要用迴圈 直接運算所花的時間比較少
    if(a%c==0)
        a=a/c;//if用來考慮整除問題
    else
        a=a/c+1;
    b=b/c;
    cout<<"Sum : "<<(a+b)*(b-a+1)/2*c;//梯型公式
    return 0;
}
