// Assignment #2-1
// A2-976001044-1.cpp 
#include <iostream>
#include <cstdlib>
#define N 3 
// ↑定義一個N的值，此數值將對應主程式內的輸入資料量 (此處設定為3) 
//   透過更改此處的數值可輕易調整"要排序的數列"的大小
//   由於主程式內的 Input/Output & Sorting 的部分皆透過 N 設定
//   因此更改此一數值即可對應任何數量的整數數列的大小排序(由小而大) 
using namespace std;

int main()
{
    // Declare input as a integer arrray with size N
    // And set all element initial value to zero
    int input[N]={0};
    // Declare temp as a integer to temporary storage the data 
    // which is use to sort and given the initial value zero
    int temp=0;  
    
    // Input data phase
    // Using for loop and set the number of input data to N
    for ( int i=0 ; i < N ; i++ )
    {
        cout << "Enter the integer " << i+1 << ": " ;
        cin >> input[i];
    }
    
    // Functional phase ( Sorting ) 
    // 迴圈內宣告兩個迴圈控制變數 i & j
    // 將陣列內的元素倆倆互相比較，將數值為低者，調換順序至陣列位置之前方 
    // 調換順序之法如下
    //   1. 將"大"者之值 assign 給變數 temp 
    //   2. 將"小"者之值 assign 原本為"大"者 (其原本之值已保存，故此處直接覆蓋)
    //   3. 將變數 temp 保留之值 assign 回原本為"小"者
    //   4. 此時原本為"大"者之值為小，而原本為"小"者之值為大，完成順序調換 
    for ( int i=0 ; i < N-1 ; i++ )
    {
        for ( int j=i+1 ; j < N ; j++ )
        {
            if ( input[i] > input[j] )
            {
                 temp = input[i];              // step 1.
                 input[i] = input[j];          // step 2.
                 input[j] = temp;              // step 3.
            }
        }
    }
    
    // Output data phase
    // Using for loop and set the number of output data to N,
    // Which the data are after sorting and order form small to large
    cout << "The sort of the 3 integers : ";
    for ( int i=0 ; i < N ; i++ )
    {
        cout << input[i] << " ";
    }
    cout << endl;
    
    system("pause");
    return 0;
}
