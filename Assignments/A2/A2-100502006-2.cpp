#include <iostream>//含入標頭檔 iostream
#include <string>//含入標頭檔 string
#include <stdio.h>//含入標頭檔 stdio.h
#include <stdlib.h>//含入標頭檔 stdlib.h

using namespace std;//使用名稱空間 std

class CDivisibleSum//定義類別
{
    public://可供一般的敘述或 function以 物件.資料成員 方式取用
        string strStart;//變數宣告，型別: string
        string strEnd;//變數宣告，型別: string
        string strDivider;//變數宣告，型別: string
        int iStart;//變數宣告，型別: int
        int iEnd;//變數宣告，型別: int
        int iDivider;//變數宣告，型別: int
        int iSum;//變數宣告，型別: int
        void Input();//輸入函式宣告，型別: void
        void DivisibleSum();//運算總和函式宣告，型別: void
        void Output();//輸出函式宣告，型別: void
    private://僅可供 member function 直接取用
        bool CheckInput(string str);//檢查輸入函式宣告，型別: bool
        bool StartEndCompare(int iS,int iE);//起終點大小比較函式宣告，型別:bool
};

void CDivisibleSum::Input()//定義輸入函式
{
    do
    {
        cout << "Enter the start : ";
        cin >> strStart;//儲存輸入為字串型別
    }while(CheckInput(strStart));//作輸入檢查判斷
    iStart=atoi(strStart.c_str());//將字串轉換為整數儲存

    do
    {
        do
        {
            cout << "Enter the end : ";
            cin >> strEnd;//儲存輸入為字串型別
        }while(CheckInput(strEnd));//作輸入檢查判斷
        iEnd=atoi(strEnd.c_str());//將字串轉換為整數儲存
    }while(StartEndCompare(iStart,iEnd));//作起點終點大小比較

    do
    {
        cout << "Enter the divider : ";
        cin >> strDivider;//儲存輸入為字串型別
    }while(CheckInput(strDivider));//作輸入檢查判斷
    iDivider=atoi(strDivider.c_str());//將字串轉換為整數儲存
}

void CDivisibleSum::DivisibleSum()//定義運算總和函式
{
    iSum=0;//先把總和設為 0
    for(iStart;iStart<=iEnd;iStart++)//從起點開始至終點
    {
        if(iStart%iDivider==0)//判斷是否能被除數整除
        {
            iSum+= iStart;//將其加入總和
        }
    }
}

void CDivisibleSum::Output()//定義輸出函式
{
    cout<<"Sum : "<<iSum;
}

bool CDivisibleSum::CheckInput(string str)//定義檢查輸入函式
{
    {
            if(str.c_str()[0]!=45)//判斷首字是否為負號
             {
                for(int j = 0; j < str.length(); j++)//逐一檢查字串所有字元是否為數字
                {
                   if( str.c_str()[j] < '0' || str.c_str()[j] > '9' )
                   {
                       cout<<"Input error!"<<endl;
                       return true;//回傳 true
                   }
                }
                return false;//回傳 false
             }
             else
             {
               for(int j = 1; j < str.length(); j++)//逐一檢查字串第一字元後所有字元是否為數字
                {
                   if( str.c_str()[j] < '0' || str.c_str()[j] > '9' )
                   {
                       cout<<"Input error!"<<endl;
                       return true;//回傳 true
                   }
                }
               return false;//回傳 false
             }
        }
}

bool CDivisibleSum::StartEndCompare(int iS,int iE)//定義起終點大小比較函式
{
    if(iS>=iE)//判斷起終點大小
    {
        cout<<"\"End\" should be greater than \"Start\" !"<<endl;
        return true;//回傳 true
    }
    else
    {
        return false;//回傳 false
    }
}

int main()//主程式 型別: int
{
    CDivisibleSum DivSum;//類別CDivisibleSum 宣告
    DivSum.Input();//呼叫類別中輸入函式
    DivSum.DivisibleSum();//呼叫類別中運算總和函式
    DivSum.Output();//呼叫類別中輸出函式

    return 0;//回傳 0
}
