#include <iostream>

using namespace std;

int main()
{
    int t;
    int x;
    int y;
    int z;

    cout << "Enter the integer 1: ";
    cin >> x;
    cout << "\nEnter the integer 2: ";
    cin >> y;
    cout << "\nEnter the integer 3: ";
    cin >> z;

    if(x>y)//If x is greater than y,the two number will be exchanged.
    {//start if (x>y)
            t = x;
            x = y;
            y = t;
    }//end if (x>y)
    if(x>z)//If x is greater than z,the two number will be exchanged.
    {//start if (x>z)
            t = x;
            x = z;
            z = t;
    }//end if (x>z)
    if(y>z)//If y is greater than z,the two number will be exchanged.
    {//start if (y>z)
            t = y;
            y = z;
            z = t;
    }//end if (y>z)
    cout << "\nThe sort of the 3 integers : " <<x<< " " <<y<< " " <<z<<endl;

    return 0;
}
