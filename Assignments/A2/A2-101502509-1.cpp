#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    int num[3],tmp;
    cout << " Enter the integer 1: ";
    cin >> num[0];                      // 輸入integer 1
    cout << " Enter the integer 2: ";
    cin >> num[1];                      // 輸入integer 2
    cout << " Enter the integer 3: ";
    cin >> num[2];                      // 輸入integer 3

    if(num[0]>num[1])                   //如果integer 1 比 integer 2 大，交換integer 1 and integer 2位置
     {
       tmp = num[1];
       num[1] = num[0];
       num[0] = tmp;
     }
    if(num[0]>num[2])                   //接著如果integer 1 比 integer 3 大，將integer 3 移到最前面
     {
       tmp = num[2];
       num[2] = num[1];
       num[1] = num[0];
       num[0] = tmp;
     }
    if(num[1]>num[2])                   //最後如果integer 2 比 integer 3 大，交換integer 2 and integer 3位置
        {
         tmp = num[2];
         num[2] = num[1];
         num[1] = tmp;
        }
    cout <<" The sort of the 3 integers: ";
    for(int i=0;i<=2;i++)               //印出排序後的結果
    cout << num[i] <<" ";

    system("pause");
    return 0;
}

