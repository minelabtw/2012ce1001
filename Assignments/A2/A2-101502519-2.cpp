#include <iostream>

using namespace std;

int main()
{
    int s,e,d,a,sum=0;

    cout<<"Enter the start : ";
    cin>>s;
    cout<<"Enter the end : ";
    cin>>e;
    cout<<"Enter the divider : ";
    cin>>d;

    a=d;     //a用來儲存d的倍數

    while(a<s)      //取得大於起始值得最小倍數
    {
        a=a+d;
    }

    while(a<=e)     //將所有介於s和e之間的d的倍數相加
    {
        sum=sum+a;
        a=a+d;
    }

    cout<<"Sum : "<<sum;


    return 0;
}
