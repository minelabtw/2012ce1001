//Assignment2-2 by J
#include <iostream>
using namespace std;

//the above instructions are for the use of cout cin

int main()
{
    int st=0 , ed=0 , dv=0 , sum=0 ;

    //anouncing all the variables used in this program , st as the starting number . ed as the end .dv as the divider .sum is simply the sum we want to find

    cout << "Enter the start: " ;
    cin >> st ;
    cout << "Enter the end: " ;
    cin >> ed ;
    cout << "Enter the divider: " ;
    cin >> dv ;

    //setting cout functions and how to input all the variables

    while ( st <= ed )
    {
        if ( st%dv==0)
        {
            sum+=st ;
        }
        st+=1;
    }

    //describing how my loop works

    cout << "The sum is " << sum ;
    return 0 ;

    //showing the result and turn off the program

}
