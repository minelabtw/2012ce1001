//101502503
#include <iostream> // allows program to output data to the screen

using namespace std;

// funtion main begins program execution
int main()

{
    int integer1, integer2, integer3, buffer; // variable declarations

    cout << " Enter the integer 1: " ;
    cin >> integer1 ; // input integer1
    cout << " Enter the integer 2: " ;
    cin >> integer2 ; // input integer2
    cout << " Enter the integer 3: " ;
    cin >> integer3 ; // input integer3

    if ( integer1 > integer2 ) //if integer1 is greater than integer2, exchange integer1 and integer2
    {
        buffer = integer1 ;
        integer1 = integer2 ;
        integer2 = buffer ;
    } // end if

    if ( integer1 > integer3 ) //if integer1 is greater than integer3, exchange integer1 and integer3
    {
        buffer = integer1 ;
        integer1 = integer3 ;
        integer3 = buffer ;
    } // end if

    if ( integer2 > integer3 ) //if integer2 is greater than integer3, exchange integer2 and integer3
    {
        buffer = integer2 ;
        integer2 = integer3 ;
        integer3 = buffer ;
    } // end if

    cout << " The sort of the 3 integers : " ;
    cout << integer1 << " " << integer2 << " " << integer3 << endl ; // display result

    return 0 ; // indicate that program ended successfully
} // end main
