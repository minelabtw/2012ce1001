#include <iostream>
using namespace std;
int main()
{
    int a,b,c,buffer;

    cout << "Enter the integer 1: ";
    cin >> a;
    cout << "Enter the integer 2: ";
    cin >> b;
    cout << "Enter the integer 3: ";
    cin >> c;

    if(a>b)//If a>b , exchange a and b
    {
        buffer=a;
        a=b;
        b=buffer;
    }

    if(a>c)//If a>c , exchange a and c
    {
        buffer=a;
        a=c;
        c=buffer;
    }
    if(b>c)//If b>c , exchange b and c
    {
        buffer=b;
        b=c;
        c=buffer;
    }


    cout << "The sort of the 3 integers : ";
    cout << a << " " << b << " " << c << endl;

    return 0;

}
