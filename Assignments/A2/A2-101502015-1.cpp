#include<iostream>
using namespace std;
int main()
   {
       int integer1,integer2,integer3,buffer1,buffer2,buffer3;

       cout<<"Enter the integer1 ";
       cin>>integer1;
       cout<<"Enter the integer2 ";
       cin>>integer2;
       cout<<"Enter the integer3 ";
       cin>>integer3;

       if (integer1>integer2)//if integer1 is larger than integer2
        {
            buffer1=integer1;//let the buffer1 transfer to buffer1 which is a process
            integer1=integer2;//let the integer1 transfer to integer2
            integer2=buffer1;//let the integer2 transfer to buffer1 so the value of integer2 will be integer1
        }
       if (integer1>integer3)//just the same as the above
        {
            buffer2=integer1;
            integer1=integer3;
            integer3=buffer2;
        }

       if (integer2>integer3)
        {
            buffer3=integer2;
            integer2=integer3;
            integer3=buffer3;
        }
       cout<<integer1<<" "<<integer2<<" "<<integer3<<endl;//output the answer

       return 0;

   }
