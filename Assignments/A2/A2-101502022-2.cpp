#include <iostream>

using namespace std;

int main()
{
    int start,end,divider,num,sum=0;

    cout<<"Enter the start:";
    cin>>start;
    cout<<"Enter the end:";
    cin>>end;
    cout<<"Enter the divider";
    cin>>divider;

    while(start<=end)//we can use the loop to run the "if" again and again to find our answer.
    {
        num=start%divider;
        if(num==0)// as the num ==0, we can know that "start" can be divisible by "divider".
        {sum=sum+start;}

     start++;// "start" can be increased until it's same as "end".
    }

    cout<<"Sum : "<<sum;
    return 0;
}
