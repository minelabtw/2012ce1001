#include <iostream>
using namespace std;
int main()
{
    int i1,i2,i3,b;

    cout << "Enter the integer 1: ";
    cin >> i1;//輸入一個整數
    cout << "Enter the integer 2: ";
    cin >> i2;//輸入一個整數
    cout << "Enter the integer 3: ";
    cin >> i3;//輸入一個整數

    if( i1 > i2 )//if i1 > i2,exchange i1 and i2
    {
        b = i1;
        i1 = i2;
        i2 = b;
    }
    if( i1 > i3)//if i1 > i3,exchange i1 and i3
    {
        b = i1;
        i1 = i3;
        i3 = b;
    }
    if( i2 > i3)//if i2 > i3,exchange i2 and i3
    {
        b = i2;
        i2 = i3;
        i3 = b;
    }

    cout << "The sort of the 3 integers : ";//這三個數的大小排序
    cout << i1 << " " << i2 << " " << i3 << endl;

    return 0;
}
