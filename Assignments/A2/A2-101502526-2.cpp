#include <iostream>

using namespace std;

int main()
{
    int start;//設定起始值為整數
    int end;//設定結束值為整數
    int divider;//設定除的數為整數
    int total=0;//設定起始總數=0
    cout << "Enter the start : " ;//輸出要求輸入起始值
    cin>>start;//輸入起始值
    cout << "Enter the end : " ;//輸出要求輸入結束值
    cin>>end;//輸入結束值
    cout << "Enter the divider : " ;//輸出要求輸入除的數
    cin>>divider;//輸入除的數
    /*設定一個迴圈，假使起始值小於等於末值則進行
      進行內容為假設起始值除以所要除的數的餘數等於0
      則總值等於原本總值+上符合條件的數
      然後起始值+1，直到起始值超過結束值*/
    for(;start<=end;start++)
    {
        if (start%divider==0)
        total=total+start;
    }
    cout<<"Sum : "<<total;//輸出總值
}
