#include <iostream>
using namespace std;

// function main begins program execution
int main()
{
    int a,b,c; //假設三個變數
    int sum = 0;

    cout << "Enter the start : "; // display message
    cin >>  a ;
    cout << "Enter the end : ";
    cin >>  b ;

    while ( a > b ) // 如果a>b(違背預設值)的話，顯示錯誤並重新輸入變數
        {
            cout << "Error!" << endl;
            cout << "Enter the start : ";
            cin >>  a ;
            cout << "Enter the end : ";
            cin >>  b ;
        }

    cout << "Enter the divider : "; // display message
    cin >>  c ;


    for ( ; a <= b; a++) // 判斷條件，a為一個每次加一的遞增數，且如果a<=b就執行程式

        if ( a % c == 0 ) // 如果c除a的餘數是0
        {
           sum += a; // 把所有符合條件的a相加
        }


    cout << "Sum : " << sum << endl; // 輸出運算結果在螢幕上
    return 0;  // indicate that program ended successfully
} // end function main
