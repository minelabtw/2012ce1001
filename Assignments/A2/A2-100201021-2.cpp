#include <iostream>

using namespace std;

int main()
{
    int s,e,d,t;  //
    cout << "Enter the start : ";
    cin >> s;
    cout << "Enter the end : ";
    cin >> e;
    cout << "Enter the divider : ";
    cin >> d;
    t = s%d;
    if(t != 0)          //  大於s最小d的倍數
    t = d-t;
    s += t;
    t = e%d;            //  小於e最大d的倍數
    e -= t;
    t = (e-s)/d + 1;                // 公式
    t *= (s+e)/2;
    cout << "Sum : " << t << endl;
    return 0;
}
