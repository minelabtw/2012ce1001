//101502504
#include <iostream>//allows program to output data to the screen
using std::cout;//program uses cout
using std::cin;//program uses cin
using std::endl;//program uses endl

int main()//function main begins program exection
{
    int integer1,integer2,integer3,buffer;//variable declarations

    cout << "Enter the integer 1: ";//prompt user to data
    cin >> integer1;//read from user into integer1
    cout << "Enter the integer 2: ";//prompt user to data
    cin >> integer2;//read from user into integer2
    cout << "Enter the integer 3: ";//prompt user to data
    cin >> integer3;//read from user into integer3

    //processing phase
    if ( integer1 > integer2 )
    {   buffer = integer1;////put the value of integer1 into buffer
        integer1 = integer2;//put the value of integer2 into integer1
        integer2 = buffer;//put the value of buffer into integer2
    }//end if
    //processing phase
    if (integer1 > integer3)
    {   buffer = integer1;//put the value of integer1 into buffer
        integer1 = integer3;//put the value of integer3 into integer1
        integer3 = buffer;//put the value of buffer into integer3
    }//end if
    //processing phase
    if (integer2 > integer3)
    {   buffer = integer2;//put the value of integer2 into buffer
        integer2 = integer3;//put the value of integer3 into integer2
        integer3 = buffer;//put the value of buffer into integer3
    }//end if

    cout << "The sort of the 3 integers : ";//prompt for input
    cout << integer1 << " " << integer2 <<  " " << integer3 << endl;
    //show the integer from small to large
    return 0;//indicate that program ended successfully
}//end function main
