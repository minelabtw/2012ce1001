#include <iostream>
using namespace std;//簡化std
int main()
{
    int integer1,integer2,integer3,buffer;//設四個變數

    cout << "Enter the integer 1: ";//輸出文字
    cin >> integer1;//使用者輸入文字
    cout << "Enter the integer 2: ";//輸出文字
    cin >> integer2;//使用者輸入文字
    cout << "Enter the integer 3: ";//輸出文字
    cin >> integer3;//使用者輸入文字

    if (integer1>integer2)//如果int1大於int2
    {
        buffer=integer2;//讓int1和int2交換位置
        integer2=integer1;
        integer1=buffer;

    }
    if (integer1>integer3)//如果int1大於int3
    {
        buffer=integer3;//讓int1和int3交換位置
        integer3=integer1;
        integer1=buffer;

    }
    if (integer2>integer3)//如果int2大於int3
    {
        buffer=integer3;//讓int2和int3交換位置
        integer3=integer2;
        integer2=buffer;
    }
    cout << "The sort of the 3 integers : "<< integer1 << " " << integer2 << " " << integer3 << endl;//輸出文字

    return 0;
    }
