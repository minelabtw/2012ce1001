#include <iostream>
using namespace std;
int main()
{
    int start, end, divider, total = 0;
        cout << "Enter the start : ";
        cin >> start;
        cout << "Enter the end : ";
        cin >> end;
        cout << "Enter the divider : ";
        cin >> divider;

    while ( start <= end ) // start counting every number from the start to the end
        {
            if ( start % divider == 0 ) // if the remainder is equal to 0
                total = total + start; // then add all the start number to get the total
                start = start + 1; // and then the start have to plus 1 until the remainder of the last number is equal to 0
        }
        cout << "Sum : " << total << endl; // print out to the screen the total

    return 0;
}
