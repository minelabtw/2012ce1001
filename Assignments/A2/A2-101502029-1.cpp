#include<iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int x,y,z,b;

    cout << "Enter the integer 1: ";
    cin >> x;
    cout << "Enter the integer 2: ";
    cin >> y;
    cout << "Enter the integer 3: ";
    cin >> z;

    if( x > y )// 如果x>y,x與y交換
    {
        b = x;
        x = y;
        y = b;
    }
    if( x > z )// 如果x>z,x與z交換
    {
        b = x;
        x = z;
        z = b;
    }
    if( y > z)// 如果y>z,y與z交換
    {
        b = y;
        y = z;
        z = b;
    }
    cout << "The sort of the 3 integers : ";
    cout << x << " " << y << " " << z << endl;

    return 0;

}
