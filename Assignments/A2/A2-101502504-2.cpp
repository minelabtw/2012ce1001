//101502504
#include <iostream>//allows program to output data to the screen
using std::cout;//program uses cout
using std::cin;//program uses cin
using std::endl;//program uses endl

int main()//function main begins program exection
{
    int start,end,divider,sum=0;//variable declarations; initialize sum

    cout << "Enter the start : ";//prompt user to data
    cin >> start;//read from user into start
    cout << "Enter the end : ";//prompt user to data
    cin >> end;//read from user into end

    if (start>end)//if start>end , it shouldn't be continue
    {cout << "Please try again !! \nEnd must bigger than start " << endl;}

    else
    {   cout << "Enter the divider : ";//prompt user to data
        cin >> divider;//read from user into divider

        //processing phase
        while((end-start)>=0)//loop until end < start
        {
            if(start%divider==0)
            {sum+=start;}//find number that between start and end can be divisible by divider
            start++;//increment control variable by start
        }//end while

        cout << "Sum : " << sum << endl;//display sum; end line
    }
    return 0;//indicate that program ended successfully
}//end function main
