#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int start;                // declare variable and set their initial value 
    int end;
    int divider;          
    int sum =0;
    int x=0;              
    
    cout << "Enter the start: ";    // enter start,end,divider there integer by user
    cin >> start;
    cout << "Enter the end: ";
    cin >> end;
    cout << "Enter the divider: ";
    cin >> divider;                
    
    while ( start <= end )         // set the loop , let it stop when start = end
    {
       if (start % divider == 0)   // if the number can be divided by divider clearly
       {                           // we take it add in the sum
           x = start;              
           sum = sum + x;
       }
       
       start = start + 1;         //the loop's increment
    }     
    
    cout << "Sum :" << sum << endl;  // print the answer
   
    system("PAUSE");                // keep these word on screen until key any word
    return 0;                       //end program
}
 
