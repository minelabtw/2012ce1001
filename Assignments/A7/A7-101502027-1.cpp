#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;


//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;


    return 1;
}
 int main()
 {
    int n1,n2,n3,f;
    while(cin>>n1>>n2)//重複輸入
    {
        int p=0,Xp=0;//記有幾個質數幾個非質數
        while (n1<=n2)
        {
            n3=n1*(n1+1)+41;
            if (isPrime(n3)==0)
                Xp++;
            if (isPrime(n3)==1)
                p++;
            n1++;
        }
        f=Xp+p;//總和數

        cout<<fixed<<setprecision(2)<<p/f*100<<endl;
    }
}
