/*******************************************/
/*                                         */
/*  Rerunable, input non-integer to quit.  */
/*                                         */
/*******************************************/

#include<iostream>
using namespace std;

void absolute(int &);//function to transfer negative to positive
int reverse(int);//function to reverse the number
int GCD(int,int);//function to find the GCD

int main()
{
    int n;//to store the input number
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
void absolute(int &x)
{
    if(x<0)
    {
        x*=-1;
    }
}
int reverse(int y)
{
    int s=0;//to store the reverse number
    if(y!=0)
    {
        while(y%10==0)//eliminate not need 0s
        {
            y/=10;
        }
        while(y!=0)
        {
            s=s*10+y%10;
            y/=10;
        }
     }
     return s;
}
int GCD(int z,int w)
{
    int u;//to be a buffer
    while(w!=0)
    {
        u=z;
        z=w;
        w=u%z;
    }
    return z;
}
