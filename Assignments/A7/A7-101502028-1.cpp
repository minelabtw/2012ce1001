#include <iostream>
#include <cmath>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;
int isPrime (int); // isPrime function prototype
int formula (int); // formula function prototype
int main()
{
    int a, b, c = 0, d;
    double e;
    do // do while loop til break
    {
        cin >> a >> b;
        if (cin == false) // finish program with a character
            break;
        else
        {
            for ( int i = a; i <= b; i++) // loop from first number to second number
            {
                d = formula(i); // find prime number using the given formula
                if (isPrime(d) == 1) // find the rest prime number
                    c++; // count the times that the prime number appears
            }
            e = (double) c / ( b - a + 1) * 100; // calculate the percentage
            cout << setprecision (2) << fixed << e << endl; // output the percentage with 2 decimals
            c = 0; // zero the counter
        }
    }while(true);
    return 0; // ends program
}

int isPrime (int n) // isPrime function
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int formula (int x) // formula funtion
{
    return ( x * x ) + x + 41; // given formula from the assignment
}
