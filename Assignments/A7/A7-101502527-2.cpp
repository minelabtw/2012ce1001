#include<iostream>
using namespace std;
int reverse(int);
void absolute(int &);
int GCD(int,int);
int c(int);
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout<< GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

void absolute(int &x)
{
    if(x<0)
        x*=-1;
}

int reverse(int x)
{
    int v=c(x);//知道10的幾次方
    if(v!=0)
        return x%10*v+reverse(x/10);//遞迴 每除一次則乘v 往下用遞迴到0
    else return 0;//x到0則回傳0
}

int GCD(int a,int b)//最大公因數
{
    int x;
    if(b>a)//如果a比較小則交換
    {
        x=a;
        a=b;
        b=x;
    }
    while(a!=0&&b!=0)//輾轉相除法 0則跳出
    {
        a%=b;
        x=a;
        a=b;
        b=x;
    }
    return a;
}

int c(int x)//自定函式 找最接近的10的幾次方
{
    int y;
    for(y=1;x!=0;y*=10)
        x/=10;
    return y/10;
}
