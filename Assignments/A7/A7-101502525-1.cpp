//Enter not integer to exit

#include<iostream>
#include<iomanip>
#include<cmath>
using namespace std;


int isPrime(int n);
int equation(int n);
double percent(int a,int b);


int main()
{
    //initial
    int a,b;


    //input & compute & output
    while(cin>>a>>b)//inputcheck
        cout<<fixed<<setprecision(2)<<percent(a,b)<<endl;


    //end
    return 0;
}




int equation(int n)
{
    return n*n+n+41;
}


double percent(int a,int b)
{
    int count=0;
    for(int n=a;n<=b;n++)
        if(isPrime(equation(n)))
            count++;
    return (double)count/(b-a+1)*100;//a~b, total b-a+1
}


//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
