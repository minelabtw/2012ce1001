#include <iostream>
using namespace std;
void absolute(int &num) //absolute函式用來取絕對值
{
    if(num < 0)
        num = -num;
}
int GCD(int a , int b) //GCD函式用來找出a跟b的最大公因數
{
    int c;
    while(c > 0)
    {
        c = a % b;
        a = b;
        b = c;
    }
    return a;
}
int reverse(int integer) //reverse函式用來反轉數字
{
    int sum = 0;
    while(integer > 0)
    {
        sum *= 10;
        sum += integer % 10;
        integer /= 10;
    }
    return sum;
}
int main()
{
    int n;
    while(cin >> n)
    {
        absolute(n);
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
