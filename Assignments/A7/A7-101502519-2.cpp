#include <iostream>

using namespace std;

void absolute(int & a)
{
    if(a<0)             //取正
        a=a*(-1);
}

int reverse(int b)
{
    int n=-1,o=b,p,l,r=0;

    while(o>=1)         //計算位數
    {
        o=o/10;
        n=n+1;
    }
    while(b>=1)
    {
        p=b%10;
        b=b/10;
        l=n;       //儲存位數
        for(l=l;l>=1;l--)   //最小位數字乘最大位數的10次方，變成第一位數，以此類推
            p=p*10;
        r=r+p;     //將每一位數字相加
        n=n-1;     //位數遞減
    }
    return r;
}

int GCD(int a,int b)
{
    int c,d,e=0;
    if(b<a)                //若b<a則交換數字
    {
        c=a;
        a=b;
        b=c;
    }
    for(c=1;c<=a;c++)     //從1開始執行到較小的數
    {
        if(a%c==0&&b%c==0)      //找公因數
            d=c;
        if(d>e)            //存較大的公因數
            e=d;
    }
    return e;
}

int main()
{
    int n;
    while(cin>>n)           //重複執行
    {
        absolute(n);         //呼叫函式
        cout << GCD(n,reverse(n)) << endl << endl;         //呼叫函式
    }
    return 0;
}
