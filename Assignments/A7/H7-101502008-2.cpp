#include <iostream>

using namespace std;
void absolute(int &x)//call by reference 永遠改變x值
{
    if(x<0)
     x=-x;
    if(x>=0)
     x=x;
}
int reverse(int x)
{
    int output=0;
    for(int m;x>0;x/=10)//取出各個位數並倒反
    {
        m=x%10;
        output+=m;
        output*=10;
    }
    return output/=10;
}
int GCD(int x,int y)
{
    do//輾轉相除法
    {
        if(x>y)
        {
            int buff;
            buff=x;
            x=y;
            y=buff;
        }
        if(y%x==0)//整除就跳出
        {
            break;
        }
        y=y%x;

    }while(1);
    return x;
}
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
