#include <iostream>
#include <math.h>//利用絕對值

using namespace std;
void absolute(int &n)//絕對值自定義函式
{
    n = fabs(n);
}
int reverse(int n)//反轉自定義函式
{
    int a,buf,x=1,sum=0;
    buf = n;
    while( n >= 10)//計算幾位數
    {
        n/=10;
        x++;
    }
    for(int i=1;i<=x;i++)//數字顛倒
    {
        a = buf % 10;
        buf = buf / 10;
        sum = (sum + a) * 10;
    }
    sum /= 10;//把個位數多乘的10消掉
    return sum;
}
int GCD(int a,int b)
{
    int c;//we dont assume that a is greater than b, because they will exchange each other automatically.
    while( b != 0 )//輾轉相除法
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//絕對值自定義函式
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

