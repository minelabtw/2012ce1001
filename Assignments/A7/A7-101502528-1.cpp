#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

int isPrime(int n) //若是質數則回傳1,若不是質數則回傳0
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int euler(int n) //讓數字做這個Euler發現的運算式
{
    return n*n+n+41;
}

int main()
{
    int start,end;
    double prime,count;
    while (cin>>start>>end && start>=0 && start<=10000 && end>=0 && end<=10000) //輸入介於0到10000的兩數進行以下運算
    {
        prime=0;
        count=0;

        for (int n=start ; n<=end ; n++) //計算輸入的兩數經由euler計算後之間的質數個數
        {
            prime=prime+isPrime(euler(n));
            count++;
        }

        cout << fixed << setprecision(2) << prime/count*100 << endl; //算出並且輸出其中質數的比例
    }

    return 0;
}
