#include<iostream>
#include<cmath>
#include<iomanip>

using std::cin;
using std::cout;
using std::endl;
using namespace std;

int reverse(int n);
int GCD(int i,int j);
int absolute(int &n);  //point the x in main

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

int reverse(int n)                  //this function is input a number and output a reverse number
{                                   // ex: input 3245 => output 5423
    int x=0;
    int y=0;
    double z=0;
    double total=0;

    z=n;
    y=int(log10(z));
    z=y;

    while (n>10)                    //the loop is to make the number reverse
    {
        x = n%10;
        total =total+x*pow(10,z);
        n=n/10;
        z=z-1;
    }

    total = total+n;


    return total;
}

int GCD(int i,int j)             //function to fine the large fator of i,j
{
    int k=0;
    while(j!=0)
    {
        k=i%j;
        i=j;
        j=k;
    }
    return i;
}

int absolute (int &n)           //take n to posstive, &n is point the n in main
{
    n=abs(n);
    return n;
}
