#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    int n1, n2;

    while( cin >> n1 && cin >> n2 )
    {
        int counter=0;
        double percentage=0, interval;
        interval = n2 - n1 + 1;

        for( int i = n1 ; i <= n2 ; i++ )
        {
            int Euler=0;
            Euler = i*i + i + 41;       //公式：n*n + n + 41

            if( isPrime(Euler) == 1 )       //如果結果是質數，計算器counter++
                counter++;
        }

        percentage = counter*100 / interval;        //換算成百分比
        cout << fixed << setprecision(2) << percentage << endl;
    }
    return 0;
}
