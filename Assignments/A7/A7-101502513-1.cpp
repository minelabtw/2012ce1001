#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if( flag == false )
    {
        p[1] = 2;
        p[2] = 3;
        p[3] = 5;
        for(int i = 5,j = 4, s = 0, t = 4; j <= 10000; j++, s = 0, t++ )
        {
            if( t % 2 == 0 )
                i = i + 2;
            else if( t % 2 == 1 )
                i = i + 4;
            for( int k = 1; p[k] <= sqrt(i); k++ )
                if( i % p[k] == 0 )
                    s = 1;
            if( s == 0 )
                p[j] = i;
            else
                j--;
        }
        flag = true;
    }
    if( n == 0 || n == 1 )
        return 0;
    for( int i = 1, l = sqrt(n); p[i] <= l; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    int a, b, count = 0;

    while( cin >> a >> b )
    {
        if( a >= 0 && a <= b && b <= 10000)
        {
            count = 0; //initialize count

            for( int n = a; n <= b; n++ ) //find prime numbers within the range of a to b
            {
                int sum = 0;

                sum = n * n + n + 41;

                if( isPrime( sum ) == 1 )
                    count++; //if the sum is a prime number, count plus 1
            }
            //output the prime numbers account for proportion of all numbers between a and b
            cout << fixed << setprecision(2) << (double)count / (b - a + 1) * 100 << endl;
        }
        else
            break;
    }

    return 0;
}
