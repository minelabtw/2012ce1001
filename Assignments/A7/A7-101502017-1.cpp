#include<iostream>
#include<cmath>
#include<iomanip>

using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
int main()
{
    int a,b,y;
    double x=0,count=0;

    while(cin >>a>>b && a>=0 && a<=10000 && b>=0 && b<=10000)
    {
        for(int n=a;n<=b;n++)
        {
            count++;//總次數
            y=n*n+n+41;//尤拉公式
            if(isPrime(y)==1)
            {
                x++;//質數個數
            }
        }
        cout << fixed << setprecision( 2 ) << 100*(x / count)<<endl;
        x=0;//回歸值
        count=0;//回歸值
    }
}
