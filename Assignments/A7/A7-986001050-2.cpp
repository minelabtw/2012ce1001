#include<iostream>
using namespace std;

void absolute(int &);
int reverse(int);
int GCD(int,int);

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

void absolute(int &n)//to absolute n call by reference
{
    if (n<0)
    n=-n;
}

int reverse(int n)//to reverse number n
{
    int output=0;
    while(n!=0)
    {
        output=output*10+n%10;
        n/=10;
    }
    return output;
}

int GCD(int a,int b)//to calculate the GCD
{
    int c;
    while( b != 0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}
