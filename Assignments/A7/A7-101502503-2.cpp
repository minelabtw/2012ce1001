#include<iostream>

using namespace std;

int reverse(int x)//自訂函式,將數字翻轉
{
    int a=0,b=0;
    while(x>0)
    {
        a=x%10;
        x=x/10;
        b=b*10+a;
    }
    return b;
}

int  GCD(int x, int y) //自訂函式,找GCD
{
    int c ;
    while(y!=0)
    {
        c=x%y;
        x=y;
        y=c;
    }
    return x;
}

void absolute(int &x) //自訂函式,將負數取絕對值
{
    if(x<0)
        x=-x;
}

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
