#include<iostream>
using namespace std;

void absolute(int &);
int reverse(int);
int GCD(int,int);

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

void absolute(int &number)//call for reference,負數轉正數
{
    if(number<0)
        number*=-1;
}

int reverse(int m)//數字翻轉
{
    int a,number=0;
    while(m>0)
    {
        a=m%10;
        m/=10;
        number=number*10+a;//數字往前進位
    }
    return number;
}

int GCD(int x,int y)//最大公因數
{
    int max;
    for(int i=1;i<=x;i++)//從1開始進行運算
    {
        if(x%i==0 && y%i==0)//當i為公因數時
            max=i;//存為max值，一路到最後即為最大公因數
    }
    return max;
}
