#include<iostream>
#include<cstdlib>
#include<cmath>
using namespace std;

void absolute( int &a )
{
    a = abs(a); //let it be positive permanently
}

int reverse( int input )
{
    int output = 0, x = 0;

    for( int y = input; y >= 10; y /= 10 )
        x++; //calculate the power
    for( x; x >= 0; x-- )
    {
        output += input % 10 * pow( 10, x ); //let number be reverse
        input /= 10;
    }
    return output;
}

int GCD( int num1, int num2 )
{
    int a;
    //if num1 is greater than num2, they will exchange each other automatically
    while( num2 != 0 )
    {
        a = num1 % num2;
        num1 = num2;
        num2 = a;
    }
    return num1;
}

int main()
{
    int n;

    while( cin >> n )
    {
        absolute(n); //let n be positive.
        cout << GCD( n,reverse(n) ) << endl << endl;
    }

    return 0;
}
