#include<iostream>
#include <cmath>
using namespace std;
int absolute(int n)//使負數變成正數
{
    if(n<0)
        n=n*-1;
    return n;
}
int reverse(int n)//讓數字倒過來
{
    if(n<0)
        n=n*-1;
    int a=0,b=0,c;
    c=n;
    while(c!=0)//計算有幾位數
    {
        c=c/10;
        b++;
    }
    while(b>0)
    {
        a=(n%10)*pow(10,b-1)+a;
        n=n/10;
        b--;
    }
    return a;
}
int GCD(int n,int m)//輾轉相除法
{
    int a;
    while(m!=0)
    {
        a=n%m;
        n=m;
        m=a;
    }
    return n;
}
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

