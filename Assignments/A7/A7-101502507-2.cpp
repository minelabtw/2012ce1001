#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

//自訂函式GCD().reverse().absolute()
int GCD(int, int);
int reverse(int);
int absolute(int);

int GCD(int n, int m)
{
    int temp;

    if(n < m)//若n<m.則將m.n交換
    {
       temp = n;
       n = m;
       m = temp;
    }

    while(m)//求m.n最大公因數
    {
        temp = n%m;
        n = m;
        m = temp;
    }

    return n;//n為最大公因數
}

int reverse (int n)//翻轉輸入的數字
{
    int m = 0;

    while(n)
    {
        m = m*10 + n%10;
        n /= 10;
    }

    return m;
}

int absolute(int n)//若輸入為負則取絕對值
{
    if(n<0)
    n = -n;
    return n;
}

int main()
{
    int n;

    while(cin>>n)
    {
        n = absolute(n);//let n be positive.
        cout << GCD(n, reverse(n)) << "\n" << endl;
    }
    return 0;
}
