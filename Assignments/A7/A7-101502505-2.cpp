#include<iostream>
using namespace std;

int absolute(int n)
{
    int a;
    if(n<0)
        a = (-1)*n;
    else
        a = n;
    return a;
}
int reverse(int input)
{
    int output=0;
    while(input!=0)
    {
        output=output*10+input%10;
        input/=10;
    }
    return output;
}
int GCD(int a,int b)
{
    int c;
    while( c != 0 )
    {
        c = b % a;
        b = a;
        a = c;
    }
    return b;
}
int main()
{
    int n;
    while(cin>>n)
    {
        n = absolute(n);
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
