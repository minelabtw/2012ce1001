#include<iostream>
using namespace std;

void absolute(int &x)//使負數轉換為正數
{
    if(x<0)
        x=x*(-1);
}

int reverse(int x)//產生輸入值之反轉數
{
    int sum=0,a;

    while(x>0)
    {
        a=x%10;
        sum=sum*10+a;
        x=x/10;
    }
    return sum;
}

int GCD(int i,int j)//使輸入值與其反轉數做輾轉相除求得最大公因數
{
    int a;

    if(i%j==0)
    {
        return j;
    }
    else
    {
        while(i%j!=0)
        {
            if(j>i)
            {
                a=i;
                i=j;
                j=a;
            }
            i=i-j;
        }
        return j;
    }
}

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
