#include<iostream>
#include<cmath>
using namespace std;

int n;
int reverse(int);
int GCD(int, int);
void absolute(int &x);

int main()
{
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
int reverse(int n)
{
    long int a,b;
    int i,i2=0,c=0;
    b=n;
    a=n;
    for (i=0;b>0;i++) //先記錄input數字的指數位
        b=b/10;
    b=n;
    while (i>=0)
    {
        a=a/pow(10,i-1);//利用int無法儲存小數位取出頭數 ex: 123/10^2=1.23     int a 此時只能儲存1
        c=c+a*pow(10,i2); //將頭數從個位數開始排列 ex: 123 1排在個位 2排在十位 3排在百位
        a=b-a*pow(10,i-1);//將a還原並去除頭數 ex: 123-100 = 23
        b=a;
        i--;
        i2++;

    }
    return c;
    }

int GCD(int a,int b)
{
    int c;
    //我搬來助教寫的 輾轉相除法
    while( b != 0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}
void absolute(int &x) //直接傳入指標 改變傳入值
{
    if (x<0)
    x=-x*1;
}
