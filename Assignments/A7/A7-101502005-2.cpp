#include<iostream>
using namespace std;
void absolute(int &n)
{
    if(n<0)
        n=-n;

}
int GCD(int a,int b)
{
    int c;
    //we dont assume that a is greater than b, because they will exchange each other automatically.
    while( b != 0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}
int reverse(int n)
{
    int p=0,q;
    while(n>0)
    {   //////讓數字反轉//////
        q=n%10;
        p=p*10+q;
        n=n/10;
    }
    return p;
}
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

