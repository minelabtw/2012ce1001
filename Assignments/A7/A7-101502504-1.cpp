
#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int prime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}//end prime

int main()
{
    int input1, input2, a=0;
    while ( cin >> input1 >> input2 )//user input
    {
        float total=0, percent=0;
        a=input1;//avoid the value of input1 change

        while ( a<=input2 )
        {
            total += prime(pow(a,2)+a+41);//calculate how many prime number
            a++;
        }

        percent = total/(input2-input1+1)*100;//find the percentage
        cout << fixed << setprecision(2) << percent << endl;
    }
}
