#include<iostream>
using namespace std;


int absolute(int &n);
int reverse(int n);
int GCD(int a, int b);


int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}




int absolute(int &n)
{
    if(n<0)
        n=-n;
}


int reverse(int n)
{
    int sum=0;
    for(;n>0;n/=10)
        sum=sum*10+n%10;
    return sum;
}


int GCD(int a, int b)
{
    if(a%b==0)
        return b;
    return GCD(b,a%b);//recursion
}
