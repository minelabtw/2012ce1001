//A7-100502201-1
//Apply the formula to judge whether the number it produce is prime or not
//Use self-definition function to solve the problem
#include<iostream>
#include<cmath>
#include<iomanip> //Used for setprecision
using namespace std;
//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
int main()
{
    int a,b,tmp;
    while(cin>>a>>b&&a>=0&&b>=0&&a<=10000&&b<=10000)//Meet all requirements
    {
        if (a>b)//Exchange a and b when a>b
        {
            tmp=a;
            a=b;
            b=tmp;
        }
        int count=0;//The number of prime numbers (in line 59)
        tmp=a;
        for (;tmp<=b;tmp++)
        {
            int x=tmp;
            int y=x*x+x+41;
            count=count+isPrime(y);
        }
        double num=b-a+1; //Make result be a floating number
        cout<<fixed<<setprecision(2)<<count*100/num<<endl;
    }
    return 0;
}
