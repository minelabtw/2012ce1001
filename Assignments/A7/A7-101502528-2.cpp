#include<iostream>
using namespace std;

int reverse(int);
int GCD(int,int);
void absolute(int &);

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

int reverse(int x) //將數字倒轉之函式
{
    int y=0;

    if (x>0)
    {
        for (; x>=1 ; x=x/10)
            y=y*10+x%10;
    }
    return y;
}

int GCD(int n , int rn) //找最大公因數之函式
{
    int a;
    while( rn != 0 )
    {
        a = n%rn;
        n = rn;
        rn = a;
    }
    return n;
}

void absolute(int &n)
{
    if (n<0)
        n=n*(-1);

}
