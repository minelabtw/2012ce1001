#include <iostream>

using namespace std;

int absolute(int k)//能取絕對值的函式
{
    int number;
    if(k<0)//若k為負數
    {
        number=0-k;//number是k的絕對值,number變為正數
    }
    else if(k>0)//若k為正數
    {
        number=k;//以k代入number
    }
    return number;//回傳number
}

int reverse(int input)//能翻轉數字的函式
{
    int output=0;
    while(input!=0)//當input不為0時才能進入迴圈
    {
        output=output*10+input%10;
        input=input/10;
    }
    return output;//回傳output
}

int GCD(int a,int b)//能取出兩數字最大公因數的函式
{
    int c;
    while(a!=0)//當a不等於0時才能進入迴圈
    {
        c=b%a;
        b=a;
        a=c;
    }
    return b;//回傳b
}

int main()
{
    int n;
    while(cin>>n)//輸入n
    {
        n=absolute(n);//以n代入absolute()的函式
        cout << GCD(n,reverse(n)) << endl << endl;//輸出n與n的翻轉數的最大公因數
    }
    return 0;
}
