#include<iostream>
using namespace std;
//Make the absolute value of n
void absolute(int& n)
{
    if(n>0)
        n=n;
    else
        n=-n;
}
//Calculate the reverse number
int reverse(int n)
{
    int k=0;
    while(n!=0)
    {
        k=10*k+n%10;
        n/=10;
    }
    return k;
}
//Calculate the GCD
int GCD(int a,int b)
{
    while(a!=0)
    {
        int c;
        if(a>b)
        {
            c=a;
            a=b;
            b=c;
        }
        b=b-a;
    }
    if(b==0)
        return 1;
    else
        return b;
}
//Main code
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;//Output the GCD about n and the reverse
    }
    return 0;
}
//End code
