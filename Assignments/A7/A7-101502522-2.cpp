#include<iostream>
using namespace std;
void absolute(int &);
int GCD(int,int);
int reverse(int);
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }

    return 0;
}

void absolute(int &n) //絕對值函式
{
    if (n < 0)
        n=-n;
}

int GCD(int a,int b)//最大公因數函式
{
    int c;
    //假設a>b，讓a、b能自動換位置
    while( b != 0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}

int reverse (int number) //顛轉數字函式
{
    int r=0;
    while ( number > 0)
    {
        r =r*10;
        r = number%10+r;
        number = number/10;
    }
    return r;
}
