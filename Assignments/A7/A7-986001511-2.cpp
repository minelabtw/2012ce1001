#include<iostream>
#include<cmath>
#include<iomanip>
#include<cstdlib>

using namespace std;

// subroutine for absolute
int absolute(int n)
{return (int)sqrt(n*n);}
// subroutine for reverse
int reverse(int input)
{
    int output=0;
    while(input!=0)
    {
        output=output*10+input%10;
        input/=10;
    }
    return output;
}
// subroutine for GCD
int GCD(int num1,int num2)
{
    int result;
    
    for(int i=1; i<=num1; i++)
    {
        if( (num1%i==0)&&(num2%i==0) )
          result=i;
    }
    return result;
}
// main routine
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
return 0;
}

