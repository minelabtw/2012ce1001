#include<iostream>
using namespace std;

int reverse(int n1);
int GCD(int n1,int n2);
int absolute(int &n1);
int main()                                              //小明寫的.
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
int reverse(int n1)                                     //函式reverse,將n反轉.
{
    int a=0,b=0;
    while(n1>0)
    {
        a=n1%10;
        b=b*10+a;
        n1=n1/10;
        if(n1<=0)
        {
            break;
        }
    }
    return b;
}
int GCD(int n1,int n2)                                  //函示GCD,求最大公因數.
{
    int c=0;
    while(n2!=0)
    {
        c=n1%n2;
        n1=n2;
        n2=c;
    }
    return n1;
}
int absolute(int &n1)                                   //函示absolute,若n是負數將它乘於-1,變正數.
{
    if(n1<0)
    {
        n1=n1*-1;
    }
    return n1;
}
