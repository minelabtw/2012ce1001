
#include<iostream>
using namespace std;

void absolute( int &k )//Let input be positive when calculate
{
    if ( k < 0 )
        k = -k;
}//end absolute

int reverse( int t )//reverse input
{
    int rt=0;
    while ( t!=0 )
    {
        rt = rt*10 + t%10;
        t /= 10;
    }
    return rt;
}//end reverse

int GCD( int a,int b )//find GCD of input and the reverse(input)
{
    int c=0;
    while( b != 0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}//end GCD

int main()
{
    int n;
    while( cin >> n )//user input
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;//cout the answer
    }
}
