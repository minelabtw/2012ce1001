#include <iostream>
#include <cmath>
#include <iomanip>
#include <cstdio>
using namespace std;

int isPrime(int n)//判斷是否為質數 0非質數 1即質數
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    int a,b,c;//c用來修正a,b大小之間的關係
    int sum=0;//計算質數的個數
    int total;//代表a,b之間的數字總量
    double answer;//為了使答案能顯示小數點後面的數字

    while(cin >> a >> b)
    {
        sum=0;
        total = b-a+1;

        if(a>b)
        {
            c=a;
            a=b;
            b=c;
        }
        if (a<=b)
        {
            for (int n=a ; n<=b ; n++)
            {
                int x=isPrime(n+n*n+41);//x用來代表自訂函數回傳的值

                if (x==0)
                    continue;
                else if (x==1)
                    sum++;
            }
            answer = sum/(double)total*100;
            printf("%.2f\n", answer);

        }
    }

    return 0;
}
