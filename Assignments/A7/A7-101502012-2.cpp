#include<iostream>
#include<cmath>
using namespace std;
void absolute(int&);
int reverse(int);
int GCD(int,int);
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
void absolute(int& x) //絕對值 用call by ref.使它值改變
{
    if (x<0)
        x=x*-1;
}
int GCD(int x,int y)//最大公因數函式
{
    int buffer,max;
    if (x>y)  // 一定讓y>=x
        {
        buffer=x;
        x=y;
        y=buffer;
        }
    for (int i=1;i<=x;i++)
        {
            if ((0==y%i)&&(0==x%i)) //確保i可整除x,y
                max=i;
        }
    return max;
}
int reverse(int y)
{
    int count=-1,buffer,answer=0,z=1; //z來判斷y是否是正數
    if (y<0)//小於零先使值便正值
    {
        z=0;
        y=y*-1;
    }
    buffer=y;//暫存原來的值
    while (y>0)
    {
        y=y/10;
        count++;
    }//count用來計算y是幾位數
    while (buffer>0)
    {
        if (buffer>0)
        answer=answer+(buffer%10)*pow(10,count);
        count--;
        buffer=buffer/10;
    }
    if (z==0) //當y是負數 因為前面轉成正數 後面再把他轉回負數
        answer=answer*-1;
    return answer;
}//end reverse
