#include<iostream>
using namespace std;

void absolute(int &);//宣告自定義函式
int GCD(int,int);
int reverse(int);
int k;

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.

        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

void absolute(int &y)//讓n隨著y一起改變
{
    if(y<0)
        y = -y;//若y小於0,y變號
}

int reverse(int z)//使的z翻轉
{
    int x=0;
    while(z!=0)
        {
            k=z%10;
            z=z/10;
            x= x*10+k;
        }
    return x;
}
int GCD(int a,int b)//求a,b的最大公因數
{
    int c;
    //we dont assume that a is greater than b, because they will exchange each other automatically.
    while( b != 0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}
