#include<iostream>

using namespace std;

void absolute(int& n) // reference of n
{
    if(n<0) n=-n;
}

int GCD(int a, int b)
{
    while((a%=b)!=0&&(b%=a)!=0); // ����۰��k
    return a+b;
}

int reverse(int n)
{
    int rtn = 0;
    while(n>0)
    {
        rtn*=10;
        rtn+=n%10;
        n/=10;
    }
    return rtn;
}

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
