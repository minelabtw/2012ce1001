#include<iostream>
#include<cmath>
#include<iomanip>// all the head file may be needed
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    double max,min,pri=0,temp,r;//declare all the variables
    for (int t=1;t>=1;t++)//this loop can let the user reuse this program until they key in a min that less than 0 or a max that more than 10000
    {
        cin >> min >> max;
        if (min<0||max>10000)
        return -1;// to exit this program, you must key in a number less than 0 or more than 10000
        for (int j=min;j<=max;j++)//this loop will test all the number between your min and max, and get the amount of the prime number
        {
            temp=j*j+j+41;//use the Euler's function to profuce a new number
            if (isPrime(temp)==1)//test if it is a prime number
            pri=pri+1;
        }
        r=(pri/(max-min+1))*100;//calculate the proportion of prime numbers in all the number
        cout << fixed << setprecision(2) << r << endl;
        pri=0;
    }
    return 0;
}
