#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4; j<=10000; j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1; p[k]<=sqrt(i); k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    int a,b,test;
    double number=0,prime=0,percent;

    while(cin>>a>>b)
    {
        number=0,prime=0;//To reset the number and the prime.
        while(a<=b)
        {
            test=a*a+a+41;
            a++;
            number++;//To compute there were how many numbers between the two numbers.
            if(isPrime(test)==1)
            {
                prime++;//To compute there were how many primes between the two numbers.
            }
        }
        percent=(prime*100)/number;//To coumpute the percent.
        cout<<fixed<<setprecision(2)<<percent<<endl;

    }
    return 0;
}
