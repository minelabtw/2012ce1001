#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    double a,b,n;//設定變數，a,b為輸入值，n是包括a與b中間有多少數
    while(0<=a && 0<=b)//只要不輸入錯誤即進入迴圈
    {
        cin >> a;
        cin >> b;
        n=b-a+1;

        double number,j=0;//設定變數，number是將數字丟進尤拉公式產生的值，j是繼續有多少質數
        for(int i=a;i<=b;i++)
        {
            number=pow(i,2)+i+41;
            if(isPrime(number)==1)
                j++;
        }

        double output=j*100/n;//輸出結果進行比例運算
        cout << fixed << setprecision(2) << output << endl;
    }
}
