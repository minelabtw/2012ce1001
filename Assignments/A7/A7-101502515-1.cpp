//輸入負數 就會跳出
#include<iostream>
#include<cmath>
#include<iomanip>
#include<math.h>
using namespace std;
int isPrime(int n)//指定使用的函式
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
int eu(int a)//轉換為EU算法 A為第A個
{
    return (a*a)+a+41;
}

int main()
{
   int in1,in2;
   int sum=0;//prime
   int count=0;//all
   float parcent;
   for(;;){
       cin>>in1>>in2;
   if(in1<0||in2<0)
   break;
   for(int n = 0;;n++)
   {
       if(n>in2)//優先判斷跳出
       break;
       if(n>=in1&&n<=in2)//是否符合區域
       {
           count+=1;
           sum+=isPrime(eu(n));//判斷是否值數
       }
   }
   parcent=((double)sum/(double)count)*100;
    cout<<fixed <<setprecision(2)<<parcent<<endl;
    sum=0;
    count=0;//reset
   }
   return 0;
}
