#include<iostream>
#include<cmath>
#include<iomanip>
#include <iomanip>
#include <stdlib.h>
using namespace std;
int isPrime(int n);
int main()
{
    int a,b,count=0;
    cin>>a>>b;
    for(int i=a;i<=b;i++)
         if(isPrime(i*i+i+41))
              count++;
    cout<<fixed<<setprecision(2)<< 100*((float)count/(float)(b-a+1));
  //  system("pause");
}













//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
        
    float l = sqrt(n);    
    for( int i = 1 ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}




