#include<iostream>
using namespace std;

int reverse(int);
int absolute(int &);
int GCD(int,int);

int reverse(int x)
{
    int buff = 0,add = 0;//buff is the latest number of x in each round
    while(x > 0)
    {
        buff = x%10;
        add = add*10+buff;
        x = x/10;
    }
    return add;
}

int absolute(int &x)
{
    if (x < 0)
        x = -x;
    return x;
}

int GCD(int a,int b)
{
    int buff2 = 0;

    while (b != 0)
    {
//////輾轉相除法 ///////
        buff2 = a%b;
        a = b;
        b = buff2;
//////輾轉相除法 ///////
    }
    return a;
}

int main()
{
    int n = 0;
    while(cin >> n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
