#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for(int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
int main()
{
    int p[10001]={0},a,n,m;
    p[0]=1;
    for(a=1;a<=10000;a++)
        p[a]=isPrime(a*a+a+41);
    while(scanf("%d %d",&n,&m)==2)
    {
        double s=m-n+1,pp=0;
        for(a=n;a<=m;a++)
            pp+=p[a];
        int ss=pp*100000/s;
        if(ss%10>=5) ss=ss+10;
            printf("%d.%02d\n",ss/1000,ss%1000/10);
    }
    return 0;
}
