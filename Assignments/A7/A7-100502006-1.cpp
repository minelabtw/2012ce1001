/**********************************/
/*                                */
/*  Rerunable, input -1 to quit.  */
/*                                */
/**********************************/

#include<iostream>
#include<cmath>
#include<iomanip>

using namespace std;

int isPrime(int);//function to determine if n is a prime number

int main()
{
    while(1)
    {
        float a,b,iCounts=0;
        cin>>a;
        if(a==-1)//if input is -1, end the program
        {
            return 0;
        }
        cin>>b;
        for(float i=a;i<=b;i++)
        {
            int l=i*i+i+41;
            if(isPrime(l)==1)
            {
                iCounts++;
            }
        }
        cout<<fixed<<setprecision(2)<<iCounts*100/(b-a+1)<<endl;
    }
    return 0;
}
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
