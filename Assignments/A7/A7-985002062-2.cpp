#include<iostream>
#include <algorithm>
#include <cmath>
using namespace std;
int absolute(int *n);
int reverse(int n);
int GCD(int n,int m);

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(&n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}


int absolute(int *n)
{
    *n=abs(*n);
    return 0;
}

int reverse(int input)
{
    int output=0;
    while(input!=0)
    {
        output=output*10+input%10;
        input/=10;
    }
    return output;
}


int GCD(int a,int b)
{
    int temp;
    while(a%b!=0)
    {
        temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}


