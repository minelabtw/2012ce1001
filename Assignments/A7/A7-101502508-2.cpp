#include<iostream>
using namespace std;

int absolute ( int &x )//use function definition to absolute the value
{
    if ( x >= 0 )
        x = x ;

    if ( x < 0 )
        x = x * (-1) ;

    return x ;
}

int reverse ( int x )//use function definition to reverse the value
{
    int result=0 ;

    while( x > 0 )
    {
        result = result*10 + x % 10 ;
        x = x / 10 ;
    }

    return result ;
}

int GCD ( int x, int y )//to find GCD
{
    int buffer, factor ;

    if ( x < y )//make sure x always is larger than y
    {
        buffer = x ;
        x = y ;
        y = buffer ;
    }

    factor = x ;

    while ( factor > 0 )
    {
        if ( x % factor == 0 && y % factor == 0 )//find GCD
            break ;

        factor-- ;
    }

    return factor ;
}

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}


