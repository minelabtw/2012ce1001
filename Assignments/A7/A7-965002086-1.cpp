// 計算機實習 HW 7-1
// 資工系四年B班_965002086_張凱閔
//
/*
輸出在某一個範圍中，這個公式可以產生質數的百分比。

輸入：輸入的每一行測試資料有2個整數a,b（ 0 <= a <= b <= 10000）。不須做錯誤偵測。

輸出：
1. 輸出在 a,b 之間所有的數n（a <= n <= b）為質數的百分比。
2. 輸出精確到小數點2位
*/

//程式結束 輸入 0 0

#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

int isPrime(int n);

int main()
{

 int a,b,totalprime;
 float percentage;
 b=1;// let while loop run at least once
 while (a!=0 || b!=0)// if want stop input a=0 and b=0
 {
    cin>>a>>b;// read data
    totalprime=0; //set init counter=0
    for (int i=a; i<=b;i++)
        totalprime += isPrime(i*i+i+41);// count prime number
    percentage=100*float(totalprime)/float(b-a+1); //type change
    cout<< fixed<<setprecision(2)<<percentage<<endl;// set output format
 }
 return 0;
}

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
