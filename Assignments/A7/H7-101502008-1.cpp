#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
int main()
{
    while(1)//重複輸入
    {
        int input_low,input_up,buff;
        double persent,count=0,longth;
        cin>>input_low>>input_up;
        if(input_low>input_up)//避免上下顛倒
        {
            buff=input_low;
            input_low=input_up;
            input_up=buff;
        }
        longth=input_up-input_low+1;//分母
        for(int counter=0;input_low<=input_up;input_low++)
        {
            counter=(input_low*input_low)+input_low+41;//公式計算後的結果

            count+=isPrime(counter);//質數計數器
        }
        persent=(count/longth)*100;//算%數
        cout<<setprecision(4)<<persent<<endl;//輸出
    }
    return 0;
}
