#include<iostream>
using namespace std;
int absolute(int &a)//若輸入值為負 則轉正
{
    if(a<0)
        a=a*(-1);
}
int reverse(int a)//將輸入值翻轉
{
    int b=0;
    for(;a>0;a=a/10)
    {
        b=b*10+a%10;
    }
    return b;
}
int GCD(int a,int b)//以輾轉相除法取最大公因數
{
    int c;
    while(a>0)
    {
        a=a%b;
        if(a==0)
            return b;//得其最大公因數
        c=a;
        a=b;
        b=c;
    }
}
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}


