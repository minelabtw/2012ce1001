#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    double x,v;
    int a,b,r;
    while (cin >>a>>b) //若a>b則調換
    {
        int y=0;
        if (a>b)
        {
            x=a;
            a=b;
            b=x;
        }
        for (int t=a;t<=b;t++)
        {
            r=t*t+t+41;
            if (isPrime(r)==1)
            {
                y++; //每偵測到一個質數 則加1
            }
        }
        v=double(y)/(b-a+1);
        cout <<fixed<<setprecision(2)<<v*100<<endl;
    }
}
