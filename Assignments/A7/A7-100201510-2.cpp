#include<iostream>
using namespace std;

int absolute(int x)
{
    if(x >= 0)
        return x;
    else if(x < 0)
        return -x;
}

int GCD(int a ,int b)//Find GCD by Euclidean algorithm.
{
    a = absolute(a);
    b = absolute(b);

    while(a*b != 0)
    {
        if(a <= b)
        {
            b=b%a;
        }
        else
        {
            a=a%b;
        }
    }

    if(a != 0)
        return a;
    else
        return b;
}

int reverse(int x)//this function is same as A6
{
    int xreverse = 0;
    while(x != 0)
    {
        xreverse =10*xreverse + x%10;
        x /= 10;
    }
    return xreverse;
}


int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

