#include<iostream>
using namespace std;

void absolute(int& i)
{
    if (i < 0)
        i = -i;//直接將負數轉成正的，並直接改那個數字(call by reference)
}

int GCD(int num1, int num2)
{
    //找出兩數的最大公因數並輸出
    int a;

    while( num2 != 0 )
    {
        a = num1 % num2;
        num1 = num2;
        num2 = a;
    }

    return num1;
}

int reverse(int j)
{
    //將數反轉
    int mid;
    int rev = 0;

    while(j != 0)
    {
        mid = j % 10;
        j = j / 10;
        rev = mid + rev * 10;
    }

    return rev;
}

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.

        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

