#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
//如果是質數，回傳值1，如果不是質數，回傳值0
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
//如果傳入的值為0，帶入方程式為41，所以回傳41，其餘回傳方程式所算的值
int f(int e)
{
    if(e==1)
        return 41;
    else
        return e*e+e+41;
}
int main()
{
    int a,b;
    cout<<endl;
    do
    {
        double c=0;
        double d=0;
        cin>>a>>b;//輸入兩數
        for(;a<=b;a++)
        {
            int g=f(a);//將兩數範圍間的數帶入方程式
            if(isPrime(g)==1)
            {
                c++;//是質數
            }
            d++;//總數
        }
        cout<<fixed<<setprecision(2)<<c/d*100<<endl;//輸出是質數的百分比
    }while(1);

    return 0;
}
