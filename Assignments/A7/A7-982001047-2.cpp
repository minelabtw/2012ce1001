#include<iostream>
using namespace std;

int GCD(int a,int b)
{
    int c;
    //we dont assume that a is greater than b, because they will exchange each other automatically.
    while( b != 0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}

int reverse(int input)// this is A6-1
{
    int output=0;
    while(input!=0)
    {
        output=output*10+input%10;
        input/=10;
    }
    return output;
}

void absolute( int &n) // if n is negative , change n to positive
{
    if(n<0)
    n *= -1;
}

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
