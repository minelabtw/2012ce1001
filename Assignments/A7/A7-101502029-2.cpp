#include<iostream>
#include<stdlib.h>
#include<math.h>
using namespace std;

int reverse( int n )//為自定義函式，將輸入的數字變為反轉數
{
    int r=0;
    while( n != 0 )//當n不等於0，則進入迴圈
    {
        r = r * 10 + ( n % 10 );
        n = n / 10;
    }
    return r;
}

int absolute( int &n )//為自定義函式，將負數絕對值轉為正數
{
    return n=abs(n);
}

int GCD( int num1 , int num2 )//為自定義函式，找出兩數之間的最大公因數
{
    int gcd;
    while( num2 != 0 )
    {
        gcd = num1 % num2;
        num1 = num2;
        num2 = gcd;
    }
    return num1;
}
int main()
{
    int n;

    while(cin>>n)
    {
        absolute(n);
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
