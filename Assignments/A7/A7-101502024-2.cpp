#include<iostream>
using namespace std;
int reverse(int input);//Newly-defined formula.
int absolute(int &);
int GCD(int x,int y);

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
int reverse(int input)//http://ce1001.minelab.tw/
{
    int output=0;
    while(input!=0)
    {
        output=output*10+input%10;
        input/=10;
    }
    return output;
}
int absolute(int &n)
{
    if (n < 0)
    {
        n =-n;//Make the number always a positive number.
    }
}
int GCD(int x,int y)//http://ce1001.minelab.tw/
{
    int z;
    while( y != 0 )
    {
        z = x%y;
        x = y;
        y = z;
    }
    return x;
}
