#include<iostream>
#include<cmath>
#include<iomanip>

using namespace std;

int isPrime(int n)//自定義函式用來分辨質數
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{

    int n1,n2;
    while(cin>>n1>>n2)//輸入兩個數
    {
        double count=0,t;
        for(int i=n1;i<=n2;i++)//使用所有介於兩數之間
        {
            int n=i*i+i+41;
            if(isPrime(n)==1)//若餘一則為質數
                count++;
        }
        cout<<fixed<<setprecision(2)<<count*100/(n2-n1+1)<<endl;//輸出答案
    }
    return 0;
}
