#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

int isPrime(int n)                                          //如果n是質數就return 1,不是就return 0.
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
int function(int i1);
int main()
{
    int a,b,c=0;                                            //c代表質數總和.
    double percent;                                         //讓percent有小數點.
    while(true)                                             //可以一直做.
    {
        cin>>a;
        cin>>b;
        if(cin==false)                                      //輸入非數字字元,結束程式.
        {
            break;
        }
        for(int i=a;i<=b;i++)                               //將a到b的數字,帶入.
        {
            int n=function(i);
            if(isPrime(n)==1)
            {
                c++;
            }
        }
        percent=(double)c/(b-a+1)*100;                      //算出percent.
        cout<< setprecision(2)<< fixed<<percent<<endl;
        c=0;
    }
        return 0;
}
int function(int i1)                                        //函式function.
{
    i1=i1*i1+i1+41;
    return i1;
}
