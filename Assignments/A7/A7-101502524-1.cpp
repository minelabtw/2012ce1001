#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

int isPrime(int n)//偵測質數的函式
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    while(1)
    {
        double a,b,y=0,n=0,j;
        cin>>a;
        cin>>b;
        for(;a<=b;a++)//找出質數和非質數的比值
        {
            j=a*a+a+41;
            if(isPrime(j)==0)
            {
                n=n+1;
            }
            if(isPrime(j)==1)
            {
                y=y+1;
            }
        }
        cout<<fixed<<setprecision(2)<<y*100/(y+n)<<endl;
    }
    return 0;
}
