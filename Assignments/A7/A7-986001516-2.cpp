#include<iostream>
#include<cmath>
using namespace std;

int absolute(int &n)//to make the input be positive, use the method "call by refrence"
{
    if(n<0)
    n=-n;
}

int reverse(int n)//to get the reverse number of the input
{
    int temp=n,i=0;
    while(temp>0)
    {
        temp=temp/10;
        i=i+1;
    }
    for (i;i>0;i--)
    {
        temp=temp+(n%10)*pow(10,i-1);
        n=n/10;
    }
    return temp;
}

int GCD(int m, int n)//to find the greatest common divisor, use the method "Euclidean algorithm"
{
    int temp;
    while(temp>0)
    {
        temp=n%m;
        n=m;
        m=temp;
    }
    return n;
}

int main()
{
    int n;
    while(cin>>n)
    {
        if(n==0)
        break;//add these two line to let the user exit by keying in 0
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
