#include<iostream>
#include<iomanip>
#include<cmath>
int isPrime(int);
using namespace std;
int main()
{
    int a,b;
    while(cin>>a>>b)//重複輸入
    {
        int i=0;
        for(int x=a;x<=b;x++)//迴圈跑輸入的兩個數
            if(isPrime(x*x+x+41))
                i++;
        cout<<fixed<<setprecision(2);//設定兩位數輸出
        cout<<(double)i/(double)(b-a+1)*100<<endl;//轉換成浮點數運算輸出
    }

    return 0;
}

int isPrime(int n)//判斷質數 複製貼上bj4...
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
