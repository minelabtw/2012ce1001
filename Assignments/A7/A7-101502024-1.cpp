#include <iostream>
#include<cmath>
#include<iomanip>

using namespace std;
int isPrime(int n);

int main()
{
    double count0=0,count1=0;
    int top,bot,hue;

    while (cin >> bot >> top)
    {
        for ( int i = bot;i <= top;i++)
        {
            hue = i * i + i + 41;//The formula provided by the question.
            if (isPrime(hue) == 1)
            {
                count1++;//Add 1 to count1.
            }
            if (isPrime(hue) == 0)
            {
                count0++;//Add 1 to count0.
            }
            if (isPrime(hue) == 1)
            {
                count0++;
            }
        }
        cout << fixed << setprecision(2) << count1 * 100 / count0 << endl;
        count0 = 0;//Reset the count.
        count1 = 0;
    }
   return 0;
}
//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
