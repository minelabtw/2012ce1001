//if input is character ,the loop will break//
#include <iostream>
#include <cstdlib>
using namespace std;

void absolute(int &);
int reverse(int);
int GCD(int,int);

int main()
{
   int n;
   while(cin >>n)
   {
      absolute(n);//let n be postive.
      cout<<GCD(n,reverse(n))<<endl<<endl; //print the greatest common divisor of n and reverse(n)
   }
   return 0;
}

void absolute(int &x)   //define absolute function
{
   x=abs(x);
}

int reverse(int y)      //define reverse function
{
   int revy=0;
   while(y>0)
   {
      revy=10*revy+y%10;
      y/=10;
   }
   return revy;
}

int GCD(int a,int b)    //find the greatest common divisor between a and b
{
   int r=1;
   while(r!=0)
   {
      r=a%b;
      a=b;
      b=r;
   }
   return a;
}

