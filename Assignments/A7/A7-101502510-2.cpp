#include<iostream>
using namespace std;

//If  x<0 let x be positive.
void absolute(int& x)
{
    if(x<0)
    {
        x=-x;
    }
}

//To reverse n which you enter.
int reverse(int y)
{
    int change=0;

    while(y>0)
    {
        change=(change*10)+(y%10);
        y/=10;
    }
    return change;
}

//To computer the GCD
int GCD(int a,int b)
{
    int max=0;

    for(int i=1; i<=a; i++)
    {
        if(a%i==0&&b%i==0)
        {
            if(i>max)
                max=i;
        }
    }
    return max;
}

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//Let n be positive.
        cout<<GCD(n,reverse(n))<<endl<<endl;
    }
    return 0;
}
