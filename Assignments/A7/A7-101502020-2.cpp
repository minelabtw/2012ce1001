//A7-2 by J
#include <iostream>
using namespace std;

int GCD(int a,int b)        //I want to d it in recursion
{
    if ( b==0 )
        return a ;      //end point for Euclidean algorithm: a is the GCD.

    return GCD (b,a%b) ;        //for the next algorithm
}

int reverse(int inp)        //old problem: reversing a number!
{
    int deg=1 , oup=0 ;

    while ( inp/deg>10 )
        deg*=10;

    while ( deg>0 )
    {
        oup+=(inp%10*deg);
        deg/=10 ;
        inp/=10 ;
    }

    return oup ;
}

void absolute(int &n)       //use "call by reference" to change n into positive
{
    if (n<0)
        n=n*-1 ;
}

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
