#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

int a,b,n,x;
float primecounter;
int isPrime(int);//宣告自定義函式
float counter=0;

int main()
{
   cin >> a >> b ;
   for(int n=a;n<=b;n++)
   {
       counter = counter + 1;
       x=n*n+n+41;

       if(isPrime(x)==1)
            primecounter+= isPrime(x);//把isPrime(x)的次數全部加總起來存在primecounter裡
   }

   cout << fixed << setprecision(2) << primecounter/counter*100;//顯示到小數點後兩位

   return 0;

}

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
