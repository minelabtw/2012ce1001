#include<iostream>
#include<cmath>
using namespace std;

int reverse(int);
void absolute(int &);
int GCD(int,int);
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

void absolute(int &c)
{
    c = sqrt(c*c);      //let n = |n|
}

int reverse(int n)      //re_n = reverse(n)
{
    int re_n=0;
    while(n != 0)
    {
        re_n *= 10;
        re_n += n%10;
        n /= 10;
    }
    return re_n;
}

int GCD(int n1,int n2)
{
    int i,gcd;
    if(n1>n2)           //let n1<=n2
        i = n1,n1=n2,n2=i;
    for(i=1;n1>=i;i++)
    {
        if(n1%i==0 && n2%i == 0)
        {
            gcd = i;
        }
    }
    return gcd;
}
