#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false) // first call to create table of prime
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    int a, b;
    while(cin >> a >> b)
    {
        double sum = 0;
        for(int i=a;i<=b;i++)
            if(isPrime(i*i+i+41))
                sum++;
        sum = sum/(double)(b-a+1) /*+ 1e-9*/; // +1e-9 can fixed the precision error
        cout << setprecision(2) << fixed << sum*100 << endl;
    }
    return 0;
}
