#include <iostream>

using namespace std;

int absolute(int&); // let the number be positive
int GCD(int,int); // calculate the DCD
int reverse(int); // reverse the number

int main()
{
    int n; // input the number
    while(cin>>n)
    {
        absolute(n);
        cout << GCD(n,reverse(absolute(n)))<< endl << endl;
    }
    return 0;
}

int absolute(int &a)
{
    if(a<0)
        a=a*(-1);
    else
        a=a;
    return a;
}

int reverse(int b)
{
    int c=0;
    while(b!=0)
    {
        c=c*10+b%10;
        b/=10;
    }
    return c;
}

int GCD(int k,int l)
{
    int d=1,e,f=1;
    if(k>l)
    {
        e=k;
        k=l;
        l=e;
    }
    for(d;d<=k;d++)
    {
        if(l%d==0&&k%d==0)
            f=d;
        else
            f=f;
    }
    return f;
}
