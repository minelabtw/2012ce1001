#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
int isPrime( int );

int main()
{
    int a,b,c,d=0;
    float s;
    while(cin>>a>>b)
    {
        int c1=0,n=0,d=b-a+1;
        for(;a<=b;a++)
        {
            n=a*a+a+41;
            c=isPrime(n);
            if(c==1)
            {
                c1++;
            }
        }
        s=static_cast<double>(c1)/(d);
        cout<<setprecision(2)<<fixed<<s*100<<endl;
    }
}
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
