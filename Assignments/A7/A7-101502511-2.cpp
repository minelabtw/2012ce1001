#include <iostream>
#include <cmath>
using namespace std;

int reverse(int);
int GCD(int,int);
void absolute(double &);

int main()
{
    double n;
    while(cin>>n)//重複輸入
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

int GCD(int a,int b)//公因式的涵式
{
    int c;
    while( b != 0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}
int reverse (int number1)//數顛倒的涵式
{
    int sum=0;
    while(number1 >0)
    {
        sum+=number1%10;
        sum*=10;
        number1/=10;
    }
    return sum/10;
}
void absolute (double &n)//call by reference ,絕對值
{
   n=abs(n);
}
