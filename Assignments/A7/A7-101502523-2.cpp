#include <iostream>

using namespace std;

void absolute(int&);
int reverse(int);
int GCD(int,int);

int main()
{
    int n;
    while( cin >> n )
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

void absolute(int &n)   //let n be positive.
{
    if( n < 0 )
        n = n * -1;
}

int reverse(int n)  //reverse input "n".
{
    int n2 = 0;
    while( n > 0 )
    {
        n2 = n % 10 + n2 * 10;
        n /= 10;
    }

    return n2;  //return the reversal.
}

int GCD(int a, int b)   //find the GCD(n,n2)
{
    int c;

    while( b != 0 )     //if b=0, break the loop.
    {
        c = a%b;
        a = b;
        b = c;
    }

    return a;
}

