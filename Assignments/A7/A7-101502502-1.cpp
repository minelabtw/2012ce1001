#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    int num1;
    int num2;
    int cal;
    double percent;

    while(1)
    {
        int sum = 0;

        cin >> num1 >> num2;

        for(int n = num1 ; n <= num2 ; n++)
        {
            cal = n * n + n + 41;

            if(isPrime(cal) == 1)//計算有多少質數
                sum++;
        }

        percent = (double)sum / (num2 - num1 + 1) * 100;//算出在範圍內的質數百分率

        cout << fixed << setprecision( 2 ) << percent << endl;//取到小數第二位

    }

    return 0;
}
