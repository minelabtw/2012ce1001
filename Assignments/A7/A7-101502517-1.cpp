#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;
//If n is prime number, return 1. If not, return 0.
int isPrime(int n)//判斷是否質數
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)//列出1-10000的質數
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)//0 1 不是質數
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;//非質數 回傳0

    return 1;//是質數 回傳1
}
int main()
{
    while(1)
    {
        int a,b;
        float c=0;
        cin>>a>>b;
        for(int i=a;i<=b;i++)//自a開始檢查到b
        {
            c=c+isPrime(i*i+i+41);//計算質數出現次數
        }
        cout<<fixed << setprecision( 2 ) <<(c/(b-a+1))*100<<endl;//輸出%數
    }
    return 0;
}

