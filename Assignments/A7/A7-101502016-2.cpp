#include <iostream>

using namespace std;

int absolute( int a)//使其絕對值化
{
    if (a<0)
        a *= -1;
    else
        a=a;
    return a;
}

int GCD(int m,int n)//求最大公因數
{
    int i=0;
    while (n!=0)//利用輾轉相除的概念,當餘數為零時則得出最大公因數
    {
        i=m%n;
        m=n;
        n=i;
    }
    return m;
}

int reverse(int input)
{
    int output=0;
    while(input!=0)
    {
        output = output*10 + input%10;
        input /= 10;
    }
    return output;
}



int main()
{
    int n;
    while(cin>>n)
    {
        n=absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;

    return 0;
}
