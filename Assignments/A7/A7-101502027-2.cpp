#include<iostream>
#include<cmath>
using namespace std;

void absolute(int &);
int GCD(int,int);
int reverse(int );

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
int reverse(int input)
{
    int output=0;
    while (input>=1)//反轉數字
    {
        output=(output+input%10)*10;
        input=input/10;
    }
    output=output/10;//重複上面迴圈會多一位,所以除掉
    return output;
}
void absolute(int &n)
{
    if (n<0)
        n=-n;
}
int GCD(int n1,int n2)
{
    int n3;
    while( n2 != 0 )//輾轉相除
    {
        n3 = n1%n2;
        n1 = n2;
        n2 = n3;
    }
    return n1;
}
