#include <iostream>
#include<iomanip>
#include<cmath>
using namespace std;

int isPrime(int n)
{
    //If n is prime number, return 1. If not, return 0.
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4; j<=10000; j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1; p[k]<=sqrt(i); k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;
    return 1;
}

int main()
{
    int i,j;

    while(cin>>i>>j)
    {
        float sum1=0;//介於I與J之間，符合公式的數是多少，初始值為0
        float sum2=0;//介於I與J之間，符合公式的數是多少，初始值為0

        while(i<=j)//迴圈跑J-I+1次
        {
            int a=i*i+i+41;//尤拉的公式

            if(isPrime(a)==1)//質數
                sum1++;
            if(isPrime(a)==0)//非質數
                sum2++;
            i++;
        }

        float total=sum1/(sum1+sum2)*100;//算是質數的機率

        cout<<fixed<<setprecision(2)<<total<<endl;
    }
}
