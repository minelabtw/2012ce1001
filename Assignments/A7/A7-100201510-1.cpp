//enter negetive number to end this program.
#include<iostream>
#include<cmath>
#include<iomanip>
#include<cstdio>

using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    int a = 0 , b = 1 ;// a and b are number to be check later, where 0 <= a <= b <= 10000
    int primeflag = 0; //primeflag will denote the number of primes.

    while(!(a == b || b > 10000))
    {
        primeflag = 0;
        cin >> a >> b ;

        if(a < 0 || b < 0)
            break;

        for(int i = a ; i <= b ; i++)
        {
            if(isPrime(i*i + i + 41) == 1) //check each number between a and b.
                primeflag++;// count how many prime are there.
        }
        printf("%3.2f\n", (100.0*primeflag)/(b-a+1));//輸出到小數點後第二位
    }
    return 0;
}
