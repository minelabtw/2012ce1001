#include <iostream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
using namespace std;
int isPrime(int);
int Eulerformula(int);

int main()
{
    int Upperbound=0, Lowerbound=0, PrimeNumber=0;
    float result=0;  //The percentage of prime numbers
    bool flag=false;

    while ( cin >> Lowerbound >> Upperbound && Upperbound>=0 && Lowerbound>=0 )
    {
        for ( int i = Lowerbound ; i <= Upperbound ; i++ )
        {
            flag = isPrime(Eulerformula(i));
            if ( flag )
            {
                PrimeNumber++;
            }
        }
        result = float(PrimeNumber) / (Upperbound - Lowerbound + 1) * 100;
        cout << setprecision(2) << fixed << result << endl;

        PrimeNumber=0;
    }



    system("pause");
    return 0;
}

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int Eulerformula(int x)
{
    return x*x + x + 41;
}
