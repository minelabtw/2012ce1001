#include <iostream>
#include<cmath>
#include<iomanip>
using namespace std;
int isPrime(int);
int main()
{
    while(1)//判斷a~b之間所有的數字代入n*n+n+41是否為質數
    {
        int a,b;
        int n=0;
        cin >> a >> b;
        for (int i = a; i <= b ; i++)
        {
            if (isPrime(i*i+i+41)== 1)
                n++;
        }
        //精確至小數點後2位
        cout << fixed << setprecision(2) <<(double)n/(b-a+1)*100 << endl;
    }
    return 0;
}

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
