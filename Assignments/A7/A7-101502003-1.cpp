#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    int down,up,sum,c;//設變數down=比較小的輸入值,up=比較大的輸入值
    while (cin>>down>>up) //如果輸入數字進入以下迴圈
    {
        sum=0;
        c=up-down+1; //算a~b之間總共有幾個數
        while (down<=up)
        {
            if (isPrime(down*down+down+41)==1)//質數判斷
            {
                sum++;//sum+1
            }
            down++;//a+1再進入迴圈
        }
        cout << fixed<< setprecision(2)<< (double)(sum)/c*100<<endl;//輸出文字 數字計算到小數第二位
    }
    return 0;
}
