#include<iostream>
using namespace std;

void absolute(int &k)//取絕對值
{
    if(k<0)
    {
        k = (-1)*k;
    }
}
int reverse(int j)//將數字反轉的函式
{
    int t,r=0;
    t = j;
    int n=1;
    for(;t>0;t=t/10)
    {
        n = n*10;
    }
    for(;j>0;n=n/10)
    {
        r = r + (j%10)*n/10;
        j = j/10;
    }
    return r;
}
int GCD(int a, int b )//找出最大公因數的函式
{
    int save=1;
    for(int h=1;h<=a;h++)
    {
        if(a%h==0)
        {
            if(b%h==0)
            {
                if(h>=save)
                {
                    save =h;
                }
            }
        }
    }
    return save;
}

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
