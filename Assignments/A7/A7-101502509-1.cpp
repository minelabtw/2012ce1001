//When input is character,the loop will break//
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int isPrime(int);
int equation(int);

int main()
{
    int start,end,primeitem=0,item;  //primeitem will count total prime numbers x^2+x+41 that n between start and end
    double ratio;
    while(cin>>start>>end)
    {
		item=end-start+1;       //calculate total items between start and end
		for(int i=start;i<=end;i++)
		{
		    if(isPrime((equation(i)))==1)
		        primeitem++;
		}
		ratio=((double)primeitem/item)*100;   //calculate the percent of primeitem/item
		cout<<setprecision(2)<<fixed<<ratio<<endl;
        primeitem=0;    //initial primeitem
    }
    return 0;
}

int equation(int x)  //define f(x)=x*x+x+41
{
    return x*x+x+41;
}

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
