#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//Call by reference 將main接下來的n都替換掉
void absolute(int &n)
{
    if (n<0)
        n=-n;
}

//最大公因式
int GCD(int a,int b)
{
    int c;

    while (b!=0)
    {
        c=a%b;
        a=b;
        b=c;
    }

    return a;
}

//將數字反轉
int reverse(int replace)
{
    int sum=0;

    while(replace>0)
    {
        sum*=10;
        sum+=(replace%10);
        replace/=10;
    }

    return sum;
}

int main()
{
    int int_interger;

    while (cin>>int_interger)
    {
        absolute(int_interger);
        cout << GCD(int_interger,reverse(int_interger)) << endl << endl ;
    }
}








