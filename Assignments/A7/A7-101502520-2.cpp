#include <iostream>
#include <cmath>
using namespace std;
int GCD(int a,int b)
{
    if(a==0)
        return 0;
    int GCD_result=1;//預設公因數是1
    //確保a比較小，方便迴圈計算
    if(a>b)
        swap(a,b);

    //從2開始，如果a,b都可以整除就是公因數
    for(int i=2;i<=a;i++)
    {
        if(a%i==0 && b%i==0)
            GCD_result=i;
    }
    return GCD_result;
}
int absolute(int &num)
{
    if(num<0)
        num=num*(-1);
}
int reverse(int ReverseTarget)
{
    int remider,temp=ReverseTarget,result=0,counter=0;
    while(temp)
    {//先計算需要的次方數
        temp=temp/10;
        counter++;
    }
    while(ReverseTarget)
    {
        remider=ReverseTarget%10;
        result=result+remider*pow(10,counter-1);
        counter--;
        ReverseTarget=ReverseTarget/10;
    }
    return result;
}
int main()
{
    int n;
    while(cin>>n)
    {
            absolute(n);//let n be positive.
            cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
