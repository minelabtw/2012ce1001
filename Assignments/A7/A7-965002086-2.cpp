// 計算機實習 HW 7-2
// 資工系四年B班_965002086_張凱閔
//
/*
運算 n 與 n 的反轉數的最大公因數的程式

輸入：
1. 一連串的整數 n 。( 不用考慮 n 為 0 的情形 )
2. 不須做錯誤偵測。

輸出：
 1. 若 n 為負數，則先對 n 取絕對值。
 2. 輸出 n 與 n 的反轉數的最大公因數。
 3. 每一行輸出都要多空一行。
*/



#include<iostream>
#include <stdlib.h>

using namespace std;

int reverse(unsigned int x)
 {
     int output;
	 output=0;
	 while(x)
     {
		output *= 10;//將欲輸出之十進位數左移一位，後面補0	;如輸入數字為0，則不移位。
        output += x % 10;//輸入數字之最左邊一位找出，加到上一列(補加個位數)
        x /= 10;//輸入數字減少之最左一位數移走
     }
//        int *outputptr;
//        outputptr=&output;
	 return output;
}


int GCD(int m, int n) // GCD function
 {
     int r; // 餘數
     while (n != 0) // 不等
     {
         r = m % n; // 模數運算
         m = n;
         n = r;
     }
     return m; // 回傳GCD
 }


void absolute(int &x)
 {
    x=abs(x);
 }


int main()
 {
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
 }
