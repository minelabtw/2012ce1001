#include<iostream>
#include<cmath>
#include<iomanip>

using namespace std;

int isPrime(int);

int isPrime(int n)//If n is prime number, return 1. If not, return 0.
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }

    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;
    return 1;
}

int main()
{
    int a, b, count, i;

    while(1)
    {
        cin >> a >> b;//輸入上下界

        count = 0;

        for(i=a;i<=b;i++)
        {
            count += isPrime(i*i+i+41);//若數字代入n2+n+41為質數.則計數count+1
        }

        cout << fixed << setprecision(2) << (float(count)/(b-a+1))*100 << endl;//算出質數百分比並取到小數第二位
    }

    return 0;
}
