#include <iostream>
#include <cmath>
#include <iomanip>
#include <cstdlib>
using namespace std;
int absolute (int &); // absolute function prototype
int GCD(int, int); // GDC function prototype
int reverse (int); // reverse function prototype
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl; // find GCD of input number to reverse input number
    }
    return 0;
}

int absolute (int &x) // absolute function definition
{
    return x = abs (x);
}

int GCD (int x, int y) // GDC function definition
{
    int greater;
    while (y != 0) // find the GCD
    {
        greater = x % y;
        x = y;
        y = greater;
    }
    return x;
}

int reverse(int x) // reverse function definition
{
    int y = 0; // new inversed number
    while (x != 0)
    {
        y = y * 10 + (x % 10);
        x = x / 10;
    }
    return y;
}
