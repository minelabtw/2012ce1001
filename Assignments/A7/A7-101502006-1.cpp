#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    int a,b;
    while(cin>>a>>b)
    {
        int pcounter=0; //used for record the prime times.
        int npcounter=0;//used for record the not prime times.
        for (int n=a;n<=b;n++) //to loop from a to b.
        {
            int x=n*n+n+41;

            if (isPrime(x)==1)
                pcounter++;
            if (isPrime(x)==0)
                npcounter++;
        }
        //////////// calculate the result ///////////
        double y=(pcounter+npcounter);
        double z=(pcounter/y)*100;
        //////////// calculate the result ///////////

        cout << fixed <<setprecision(2)<< z << endl;
    }
    return 0;
}
