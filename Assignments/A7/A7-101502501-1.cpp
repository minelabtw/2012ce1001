#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    double a,b,d,e;
    while ( 1 )
    {
        int c=0;//定c=0,c用來判斷有多少個質數
        cin >> a >> b;//輸入a,b

        for( int i = a ; i <= b ; i++ )//把a和b之間的所有數帶入公式以判斷質數
        {
            d=i*i+i+41;
            if( isPrime( d )==1 )//如果為質數,c加一
                c++;
        }
        e = c/(b-a+1) * 100;//e為計算在某一個範圍中,這個公式d可以產生質數的百分比
        cout << fixed << setprecision( 2 ) << e << endl;//輸出到小數點後第二位
    }

    return 0;
}
