#include<iostream>
#include <cstdlib>
#include <algorithm>
using namespace std;

int absolute(int x)
{
    x=abs(x);
    return x;
}
int reverse(int n)
{
	int temp = n;
	int sum = 0;
	while (temp)
	{
		sum*=10;
		sum += temp%10;
		temp/=10;
	}
	return sum;
}
int GCD(int n,int m) //自訂的函數gcd(),傳回最大公因數
{
    int g;
    while(m!=0)
    {
        g=n%m;
        n=m;
        m=g;
    }
    return n;
}

int main()
{
    int n;
    while(cin>>n)
    {
        n=absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
