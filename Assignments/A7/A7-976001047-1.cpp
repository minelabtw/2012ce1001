#include<iostream>
#include<cmath>
#include<iomanip>

using std::cin;
using std::cout;
using std::endl;
using namespace std;

int isPrime(int n);

int main()                //main function for input two number
{                         //calculate the number between these two input in function n^2+n+41
    int a=0;              //the answer maybe prime or not
    int b=0;              //in end , calculate the ratio of prime
    int n=0;

    while ((cin>>a>>b != false)  && a,b!=-1)                //enter -1 -1 to end program
    {
        float x=0;
        float y=0;
        double z=0;

        for (int i=a ; i<=b ; i++)
        {
            n=i*i+i+41;                       //function n^2+n+21

            if(isPrime(n)==1)                 //function isPrime is to judgment the answer is prime or not
            {
               x=x+1;
            }

            y=y+1;
        }

        z = x/y*100;                        //calculate the ratio of prime

        cout << fixed << setprecision( 2 )<<z<<endl;
    }
    return 0;
}


//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
