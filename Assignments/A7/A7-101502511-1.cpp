#include<iostream>
#include<cmath>
#include<iomanip>

using namespace std;
int isPrime(int);//定義涵式

int main()
{
   int number1,number2;//start and end
   double percent,dn,sum=0,formula;
   while(cin>>number1>>number2)
   {
      sum=0;//初始化
      dn=number2-number1+1;//項數
      for(int i=number1;i<=number2;i++)
      {
         formula=i*i+i+41;
         sum+=isPrime(formula);//計算質數個數
      }
      percent=(sum/dn)*100;//百分比
      cout<<fixed<<setprecision(2)<<percent<<"%"<<endl;
   }

    return 0;
}

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)//內容
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}
