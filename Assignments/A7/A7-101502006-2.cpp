#include<iostream>
using namespace std;

////// let a be positive. //////
void absolute(int &a)
{
    if (a<0)
        a=-a;
}

////// find the GCD of a and b. //////
int GCD(int a,int b)
{
    int c;

    while( b!=0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}

////// reverse the number. //////
int reverse(int a)
{
    int c=0;
    for (int x=a;x>0;x/=10)
    {
        int b=x%10;
        c=c*10+b;
    }
    return c;
}

/////// original function ////////
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
