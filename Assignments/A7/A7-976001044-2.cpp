#include <iostream>
#include <cmath>
#include <cstdlib>

using namespace std;
int GCD(int,int);
int reverse(int);
int pow10_Integer(int);
void absolute(int *);

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(&n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}

int GCD(int InputNumber_1, int InputNumber_2)
{
    int remainder=1;

    while ( InputNumber_2 != 0 )
    {
          remainder = InputNumber_1 % InputNumber_2;
          if ( remainder == 0 )
          {
               break;
          }
          InputNumber_1 = InputNumber_2;
          InputNumber_2 = remainder;
    }

    return InputNumber_2;
}

int reverse(int n)
{
    int Boundary=0, Quotient=0, Remainder=0, result=0;
    bool ZeroFlag=false;

    // Boundary = log(n), which is positive integer,
    // Ues to set the Highest significant digit
    Boundary = int(log10(double(n)));

    // Under the Decimal system, Divided by 10 in each loop
    // And let the Quotient replace the input number
    for ( int i=1 ; i <= Boundary+1 ; i++ )
    {
        Quotient = n / 10;
        Remainder = n % 10;

        if ( Remainder != 0 )
        {
            ZeroFlag = true;
        }

        if ( ZeroFlag )
        {
            result += Remainder * int(pow10_Integer(Boundary-i+1));
            //cout << Remainder;
        }

        n = Quotient;
    }
    //cout << result << endl;
    return result;
}

void absolute(int *n)
{
    if ( *n < 0 )
    {
        *n = -*n;
    }
    return;
}

int pow10_Integer(int n)
{
    int result=1;
    for ( int i=1 ; i <= n ; i++ )
    {
        result *= 10;
    }
    return result;
}
