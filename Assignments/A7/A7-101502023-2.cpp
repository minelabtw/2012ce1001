#include<iostream>
using namespace std;
int GCD(int,int);
int reverse(int);
void absolute(int &n)
{
    if(n<0)
        n=-n;//let n be positive.
}
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
int GCD(int a,int b)
{
    int c;
    //we dont assume that a is greater than b, because they will exchange each other automatically.
    while( b != 0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}
int reverse(int n)
{
    int b,c=n%10,d=0;
    while(n>=1)
    {
         b=n/10;
         c=n%10;
         d=d*10+c;
         n=b;//using b to replace a
    }
    return d;
}
