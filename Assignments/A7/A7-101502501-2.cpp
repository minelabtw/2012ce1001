#include<iostream>
using namespace std;
int absolute(int& i)//自訂函式,讓 n 為正數
{
    if ( i < 0 )
        i=-i;
    return i;
}
int GCD(int a,int b)//自訂函式,計算最大公因數
{
    int c;

    while( b != 0 )
    {
        c = a%b;
        a = b;
        b = c;
    }
    return a;
}
int reverse(int x)//自訂函式,翻轉輸入的數字
{
    int n = 0, b = 1, c = 0;

    while (  b > 0 )
    {
        b = x / 10;//讓迴圈能夠跑幾次
        n = x % 10;//計算餘數
        c = 10 * c + n;//利用餘數乘10再加上餘數來反轉
        x = x / 10;//讓數字少一位
    }
    return c;
}
int main()//小明的函式,輸入及輸出
{
    int n;
    while(cin>>n)
    {
        absolute(n);//讓n為正
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
