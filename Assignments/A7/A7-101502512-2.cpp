#include <iostream>
#include<cmath>
#include<cstdlib>
using namespace std;

int reverse(int x)//把輸入的整數顛倒
{
    int sum=0;
    int b=x/10;
    int i=0;

    while(b!=0)//找出I的值
    {
        b/=10;
        i++;
    }
    while(i>=0)//迴圈跑I+1次
    {
        sum += x % 10 *pow(10,i);
        x/=10;
        i--;
    }
    return sum;
}

int GCD(int a,int b)//算出最大公因數
{
    int c;
    while(b!=0)//輾轉相除法
    {
        c=a%b;
        a=b;
        b=c;
    }
    return a;
}

void absolute(int &n)//使輸入的數恆偽正
{
    n=abs(n);
}

int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}
