//A7-100502201-2
//Total 3 self-definition functions
//Only abs(n) needs to call by reference 'cause n needs to be altered
#include<iostream>
using namespace std;

int absolute(int &n)
{
    if (n<0)
    {
        n=-n;
        return n;
    }
    else
        return n;
}
int reverse(int n) //Return a value which is reversed
{
    int a=0;
    while (n!=0)
    {
        a=a*10+n%10;
        n/=10;
    }
    return a;
}
int GCD(int a,int b) //Return a value of two numbers' GCD
{
    int c;
    while (b!=0)
    {
        c=a%b;
        a=b;
        b=c;
    }
    return a;
}
int main()
{
    int n;
    while (cin>>n)
    {
        absolute(n);//let n be positive.
        cout<<GCD(n,reverse(n))<<endl<<endl;
    }
    return 0;
}
