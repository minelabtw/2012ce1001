#include <iostream>
#include<cmath>
#include<iomanip>

using namespace std;
//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    while(1)             //重複執行
    {
        double a,b,c,m=0,n=0;
        cin>>a;
        cin>>b;

        if(a>b)                //若a>b則交換數字
        {
            c=a;
            a=b;
            b=c;
        }

        for(a=a;a<=b;a++)          //計算總數字量與質數量
        {
            c=a*a+a+41;
            n=n+1;
            if(isPrime(c)==1)
                m=m+1;
        }

        cout<<fixed<<setprecision(2)<<m*100/n<<"\n";   //輸出比值
    }

    return 0;
}

