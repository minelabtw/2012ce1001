#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//If n is prime number, return 1. If not, return 0.
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for(int i=5,j=4,s=0,t=4;j<=10000;j++,s=0,t++)
        {
            if(t%2==0)
                i=i+2;
            else if(t%2==1)
                i=i+4;
            for(int k=1;p[k]<=sqrt(i);k++)
                if(i%p[k]==0)
                    s=1;
            if(s==0)
                p[j]=i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;

    return 1;
}

int main()
{
    double n1,n2;
    for (;cin >> n1 >> n2;)                         //重複輸入
    {
        double count = 0.00;
        double amount = n2 - n1 + 1 ;               //計算2數中有幾個數字
        int Prime[100000] = {};                     //宣告一個陣列來儲存isPrime的回傳值
        for (int i = 0 ; i < amount;i++,n1++)       //將值存入陣列
            Prime[i] = n1;
        for (int j=0 ; j<amount ; j++)              //此迴圈計算回傳值中有幾個是1(質數)
        {
            if (isPrime(Prime[j]*Prime[j]+Prime[j]+41)==1)
                count +=1 ;
        }
        cout << fixed <<setprecision(2) << (count/amount)*100 << endl ;  //輸出結果且為小數後兩位
    }
    return 0;
}

