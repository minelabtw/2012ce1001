#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

//如果n是質數就return 1，如果不是就return 0
int isPrime(int n)
{
    static int p[10001];
    static bool flag = false;
    if(flag==false)
    {
        p[1]=2;
        p[2]=3;
        p[3]=5;
        for( int i=5 , j=4 , s=0 , t=4 ; j<=10000 ; j++ , s=0 , t++ )
        {
            if( t % 2 == 0 )
                i = i + 2;
            else if( t % 2 == 1 )
                i = i + 4;
            for( int k = 1 ; p[k] <= sqrt(i) ; k++ )
                if( i % p[k] == 0)
                    s = 1;
            if( s == 0 )
                p[j] = i;
            else
                j--;
        }
        flag = true;
    }
    if(n==0||n==1)
        return 0;
    for( int i = 1 , l = sqrt(n) ; p[i] <= l ; i++ )
        if( n % p[i] == 0 )
            return 0;
    return 1;
}

int Prime(int m)//自定義函式，套入公式0到39都會變質數
{
   return m*m+m+41;
}

int main()
{
    int num1,num2,count=0;
    double per;
    while(1)
    {
        cin >> num1 >> num2;
        if( cin == false )
        break;
        for( int i=num1 ; i<=num2 ; i++ )//計算有幾個質數
        {
            int n = Prime(i);
            if( isPrime(n) == 1 )
            count++;
        }
        per = (double)count / ( num2 - num1 + 1 ) * 100;
        cout << setprecision(2)<< fixed << per << endl;//計算到小數點後第2位
        count=0;
    }
    return 0;
}


