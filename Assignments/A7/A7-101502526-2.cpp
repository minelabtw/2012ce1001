#include<iostream>
#include<cmath>
#include<iomanip>

using namespace std;

int absolute(int& x)  //先將數變為正數
{
    if (x < 0)
        x *= -1;
    return x;
}

                      /*將數倒過來
                        先計算有幾位數，
                        當值>0時，
                        總值等於原本的+上最後一個數乘上10的(位數-1)次方，
                        之後值/10，持續迴圈
                        之後回傳總值                                  */

int reverse(int x)
{
    int total=0,count=0,y=x;
    for (; x/10 > 0 ;)
    {
        count ++;
        x/=10;
    }
    for (int site = count; y > 0; site -- )
    {
        total = total + (y%10) * pow(10,site) ;
        y /= 10;
    }
    return total ;
}
                      /*判斷最大公因數
                        若前面數較大則調換位子，
                        之後利用輾轉相除法求到兩數的餘數為0
                        回傳餘數0之前的餘數                    */
int GCD(int x,int y)
{
    int space,mod;
    if (x>y)
    {
        space = y;
        y = x;
        x = space;
    }
    for (;y%x!=0;)
    {
        mod = y%x;
        y=x;
        x=mod;
    }
    return mod;
}
                                        // 先絕對值，後求最大公因數
int main()
{
    int n;
    while(cin>>n)
    {
        absolute(n);//let n be positive.
        cout << GCD(n,reverse(n)) << endl << endl;
    }
    return 0;
}


