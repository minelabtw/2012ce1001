#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
#include<iomanip>
using namespace std;

class accountSystem
{
    public:
        void loadDataFromFile(string); //讀取account
        bool logIn(string,string);//登入
        bool signUp(string,string);//註冊
        void SaveDataToFile(string);//更新並結束程式
    private:
    string username,password;
    string q[10000],w[10000];
    int k;
};
accountSystem::accountSystem()
{
    username=' ';
    password=' ';
    k=0;
}
void accountSystem::loadDataFromFile()//讀取
{
    ifstream file("account.txt",ios::in);
    if(!file)
    {
        cout<<"ERROR."<<endl;
        exit(1);
    }
    for(int a =0;!file.eof();a++)
    {
        file>>q[a]>>w[a];
        k=a;
    }
}
bool accountSystem::logIn(string user,string pass)
{
    for(int a =0;a<10000;a++)
    {//檢查帳號密碼是否正確，正確則回傳true
        if(username==q[a])
        {
            if(password==w[a])
            {
                return true;
            }
        }
    }
}
bool accountSystem::singUp(string user,string pass)
{
    for{int a=0;a<10000;a++)
    {//如果已經有用過的帳號，則回傳false
        if(username==q[i])
        {
            return false;
        }
    }
    else//沒用過的帳號的話，將他存取
    q[k]=username;
    w[k]=password;
    k = k + 1;
    return true;
}
void accountSystem::SaveDataToFile()
{
    ofstream outfile("account.txt", ios::out);
    for(int a=0;a<10000;a++)
    {//將輸入的字串存取到檔案裡
        outfile>>q[a]>>" ">>w[a]>>endl;
    }
}


int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
