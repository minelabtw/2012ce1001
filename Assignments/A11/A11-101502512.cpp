#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
using namespace std;
//you must design one or more class to complete this program
class accountSystem
{
public:
    accountSystem();
    void loadDataFromFile(string);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(string);
private:
    string account;
    string code;
};
accountSystem::accountSystem()//constructor
{
    account='0';
    code='0';
}
void accountSystem::loadDataFromFile(string file)
{
    ifstream inAccountFile("account.txt",ios::in);
    if(!inAccountFile)//檢查檔案是否存在
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
}
void accountSystem::SaveDataToFile(string file)
{
    exit(1);
}
bool accountSystem::logIn(string username,string password)
{
    ofstream outAccountFile("account.txt",ios::app);
    ifstream inAccountFile("account.txt",ios::in);
    bool check=false;
    while(inAccountFile>>account>>code)//帳號密碼是否一樣
    {
        if(username.compare(account) == 0 and password.compare(code) == 0)//一樣
        {
            check=true;
            break;
        }
    }
    return check;
}

bool accountSystem::signUp(string username,string password)
{
    ofstream outAccountFile("account.txt",ios::app);
    ifstream inAccountFile("account.txt",ios::in);
    bool check=true;
    while(inAccountFile>>account>>code)//是否重複
    {
        if(username.compare(account) == 0)//重複
        {
            check=false;
            break;
        }
    }
    if(check)//沒重複，儲存
    {
        outAccountFile<<username<<' '<<password<<endl;
    }
    return check;
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
