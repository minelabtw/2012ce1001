#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;
class accountSystem
{
public:
    accountSystem();//初始化各項數值
    void loadDataFromFile(string); //讀account文件
    bool logIn(string,string);//登入系統
    bool signUp(string,string);//註冊系統
    void SaveDataToFile(string);//更新文件並結束程式
private:
    string username,password,u[100000],p[100000]; //帳號,密碼,儲存帳號的陣列,儲存密碼的陣列
    int n; //記錄從陣列第幾個開始覆寫文件
};
accountSystem::accountSystem()
{
    username=' ',password=' ';
    n=0;
}
void accountSystem::loadDataFromFile(string)
{
    ifstream inClientFile("account.txt", ios::in);
    for(int i=0 ; !inClientFile.eof() ; i++) // 標記檔案讀完
    {
        inClientFile >> u[i] >> p[i];
        n = i; //將i的值存入n中
    }
}
bool accountSystem::logIn(string username,string password)
{
    for(int i=0 ; i < 100000 ; i++) //若帳號相同則繼續檢查密碼，若皆相同則回傳true，代表登入成功
    {
        if(username==u[i])
            if(password==p[i])
                return true;
    }
}
bool accountSystem::signUp(string username,string password)
{
    bool issame=false; //一開始先用issame紀錄帳號不重覆
    for ( int i=0 ; i < 100000 ; i++) //檢查帳號有無重覆，若重覆則讓issame改為true,代表帳號重覆
        if(username==u[i])
            issame =true;
    if (issame==true) //帳號重覆時就回傳false
        return false;
    else //若沒重覆就將帳號密碼存入u,p陣列裡,並將n值+1給下一次用,並回傳true
        u[n]=username;
        p[n]=password;
        n=n+1;
        return true;
}
void accountSystem::SaveDataToFile(string)
{
    ofstream outClientFile("account.txt", ios::out);
    for(int i=0 ; i<100000 ; i++) //輸出帳號密碼的字串到account文件上
        outClientFile << u[i] << ' '<< p[i] << endl;
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
