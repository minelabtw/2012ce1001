#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
using namespace std;
//you must design one or more class to complete this program
class  accountSystem
{
    public:
        accountSystem();
        void loadDataFromFile(string);
        bool logIn(string,string);
        bool signUp(string,string);
        void SaveDataToFile(string);
    private:
        string accountb[10000];
        string passwordb[10000];
        int i;
};
accountSystem::accountSystem()
{
    i=0;//初始i
}
void accountSystem::loadDataFromFile(string filename)
{
    ifstream accountFile ( filename.c_str() , ios::in );
    if (!accountFile) //如果無法打開txt檔的錯誤訊息
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }
    while ( !accountFile.eof() ) //把所有的帳號密碼存下來
    {
        accountFile >> accountb[i] >> passwordb[i];
        i++;
    }
    i--;//最後i會多一 所以要扣掉
}
bool accountSystem::logIn(string account,string password)
{
    for (int j=0;j<=i;j++)
    {
        if (account==accountb[j]) //先比對帳號 無帳號則不更動check
        {
            if (password==passwordb[j]) //帳號比對完後直接比對密碼
            {
                return true;

            }
            else
            {
                return false;
            }
        }
    }
}
bool accountSystem::signUp(string account,string password)
{
    for (int j=0;j<=i;j++)
    {
        if (accountb[j]==account) //用來比較帳號是否重複
        {
            return false;
        }
    }
    i=i+1;
    accountb[i]=account;
    passwordb[i]=password;
    return true;
}
void accountSystem::SaveDataToFile(string filename)
{
    ofstream accountFile( filename.c_str() , ios::out );
    if (!accountFile) //如果無法打開txt檔的錯誤訊息
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }
    for(int k=0;k<=i;k++)
    {
        if(k<i)
            accountFile << accountb[k] << " " << passwordb[k] << endl;
        else
            accountFile << accountb[k] << " " << passwordb[k];//最後一列不可有換行 不然下次讀會把空白當作帳號密碼
    }

}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
