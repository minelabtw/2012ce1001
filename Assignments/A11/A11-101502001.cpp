#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

class accountSystem
{
private:
    string u,p;
public:
    void loadDataFromFile()
    {
        ofstream outClientFile("account.txt",ios::out);//建立一個txt檔

        if(!outClientFile)//若讀不到檔則輸出錯誤訊息並結束程式
        {
            cerr << "File could not be opened" << endl;
            exit(1);
        }
    }
    void logIn(string u,string p)
    {

    }
    void signUp(string u,string p)
    {
        int i=0;
        string a[1000];
        while(cin >> u >> p)
        {
            a[i]=u;//username代入a陣列
            for(int j=0;j<i;j++)//迴圈,若i=0則無法進入
            {
                if(i>0&&a[i]!=a[j])
                {
                    break;//每次的username都與前幾次的username做比較,若不等於則註冊成功
                }
                if(i==0&&a[0]==u)//i=0則跳入此,註冊成功
                {
                    break;
                }
            }
            i++;
        }
    }
    void SaveDataToFile()
    {
        ifstream inClientFile("account.txt",ios::in);//建立一個txt檔

        if(!inClientFile)//若讀不到檔則輸出錯誤訊息並結束程式
        {
            cerr << "File could not be opened" << endl;
            exit(1);
        }
    }
};

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
