#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class accountSystem
{
    public:
        int n;
        string a[10000];
        string b[2];
    int loadDataFromFile(string file)
    {
        fstream inFile;
        inFile.open(file.c_str(),ios::in);   //讀檔

        for(n=0;n<=10000;n=n+2)   //讀入帳密
        {
            if(inFile.eof())
                break;

            inFile>>a[n]>>a[n+1];
        }
    }

    bool logIn(string name, string pass)
    {
        for(int m=0;m<n;m=m+2)              //依次比對
        {
            if(name==a[m]&&pass==a[m+1])          //正確帳密判斷
                return true;

            else if(m==n-2)
                return false;

        }
    }

    bool signUp(string name , string pass)
    {
        for(int o=0;o<=n-2;o=o+2)
        {
            if(name==a[o])             //帳號相同時
                return false;
            else if(n==o+2)      //新帳號
            {
                a[n]=name;
                a[n+1]=pass;
                n=n+2;
                return true;
            }
        }
    }

    void SaveDataToFile(string file1)
    {
        fstream fp;                 //開啟txt檔
        fp.open(file1.c_str(), ios::out);
        if(!fp)                     //如果開啟檔案失敗
        {
            cout<<"Fail to open file."<<endl;
        }
        for(int m=0;m<=n;m=m+2)
            fp<<a[m]<<" "<<a[m+1]<<"\n";   //輸入

        fp.close();
    }
};

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
