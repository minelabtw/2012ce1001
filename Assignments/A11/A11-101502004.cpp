#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;

class accountSystem
{
public:
    accountSystem();
    void loadDataFromFile(string);
    void SaveDataToFile(string);
    bool logIn(string,string);
    bool signUp(string,string);
private:
    string username;
    string password;
};

string countUsernameData[10000];//儲存帳號的陣列
string countPasswordData[10000];//儲存密碼的陣列
int i=0;//紀錄有幾筆資料

accountSystem::accountSystem(){}//constructor

void accountSystem::loadDataFromFile(string)//讀檔的function
{
    string username;
    string password;
    ifstream loadDataFromFile("account.txt",ios::in);
    while(loadDataFromFile >> username >> password)//一直讀資料讀到結束
    {
        countUsernameData[i]=username;//然後一個個放進帳號陣列
        countPasswordData[i]=password;//跟密碼陣列裡
        i++;//順便紀錄資料編號
    }
}

void accountSystem::SaveDataToFile(string)//存檔function
{
    ofstream SaveDataToFile("account.txt",ios::out);
    for(int j=0;j<i;j++)//把陣列內的元素一個個存進去
    {
        SaveDataToFile << countUsernameData[j] << ' ' << countPasswordData[j] << endl;
    }
}

bool accountSystem::logIn(string username,string password)//登入fuction
{
    int number=0;//標記第幾個帳號
    for(int j=0;j<i;j++)//比對帳號
    {
        if(username.compare(countUsernameData[j])==0)
        {
            number=j;//比對成功後記錄是第幾筆
            break;
        }
    }
    if(password.compare(countPasswordData[number])==0)//再比較那筆資料的密碼
        return true;//正確就return true
}

bool accountSystem::signUp(string username,string password)//註冊fuction
{
    int check=0;//確認是否將所有資料都檢查過一次
    for(int j=0;j<i;j++)//開始從第一筆帳號資料檢查
    {
        if(username.compare(countUsernameData[j])==0)//一出現相符的即break
            break;
        check++;//紀錄已經檢查幾筆了
    }
    if(check==i)//要是全部檢查完，就把新的帳號密碼傳進陣列裡
    {
        countUsernameData[i]=username;
        countPasswordData[i]=password;
        i++;//記錄一筆新資料
        return true;//回傳確認註冊成功
    }
    else//途中break，表示有相同沒檢查完
        return false;//回傳false表示有帳號重複
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
