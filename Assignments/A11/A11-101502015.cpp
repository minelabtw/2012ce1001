#include<iostream>
#include<string>
#include<cstdlib>
#include<fstream>

using namespace std;

class accountSystem
{
public:
    accountSystem();
    void loadDataFromFile(string);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(string);

private:
    int t;
    bool check;
    string n[100],p[100];
    string name1,name,password1,password;
};

accountSystem::accountSystem()
{
    t=0;
}

void accountSystem::loadDataFromFile(string account)
{
    ifstream inClientFile(account.c_str(),ios::out);
    while(inClientFile>>n[t]>>p[t])//check if there is any account is right
    {
        t++;
    }
}

bool accountSystem::logIn(string username,string password)
{
    ifstream inClientFile("account.txt",ios::out);
    bool check=false;
    for(int i=0;i<t;i++)//check if account and password are all correct
    {
        if(n[i]==username && p[i]==password)
        {
            check=true;
            return check;
        }
    }
    return check;
}

bool accountSystem::signUp(string username,string password)
{
    check=true;
    for(int i=0;i<t;i++)
    {
        if(n[i]==username)
        {
            check=false;
            return check;
        }
    }
    if(check==true)//if there is no repeat
    {
        n[t]=username;
        p[t]=password;
        t++;
        return check;
    }
}

void accountSystem::SaveDataToFile(string account)
{
    ofstream OutClientFile(account.c_str(),ios::out);
    for(int i=0;i<t;i++)
    {
        OutClientFile<<n[i]<<" "<<p[i]<<endl;
    }
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
