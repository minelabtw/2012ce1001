#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;

class accountSystem
{
public:
    accountSystem();
    void loadDataFromFile(const char *);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(const char *);
    ofstream outClientFile;

private:
    int i;
    string str[1000];
    string uname[100],pword[100];
};
accountSystem::accountSystem()
{
    i = 0;
}
void accountSystem::loadDataFromFile(const char *)
{
    ofstream outClientFile("account.txt", ios::app);
    if (!outClientFile)
    {
        cerr << "File could not be opened" << endl;
        exit( 1 );
    }
}
bool accountSystem::logIn(string username,string password)
{
    ifstream inClientFile("account.txt", ios::in);
    i = 0;
    bool check = 0;
    while(inClientFile >> uname[i] >> pword[i])
    {
        if (username == uname[i] && password == pword[i])
        {
            check = 1;
            break;
        }
        i++;
    }
    if (check == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool accountSystem::signUp(string username,string password)
{
    ifstream inClientFile("account.txt", ios::in);
    ofstream outClientFile("account.txt", ios::app);
    i = 0;
    bool check = 0;
    while(inClientFile >> uname[i] >> pword[i])
    {
        if (username == uname[i])
        {
            check = 1;
            break;
        }
        i++;
    }
    if (check != 1)
    {
        outClientFile << username << " " << password << endl;
        return true;
    }
    else
    {
        return false;
    }
}
void accountSystem::SaveDataToFile(const char *)
{
    ofstream outClientFile("account.txt", ios::app);
}
int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
