#include <iostream>
#include <string>
#include <fstream>
using namespace std;

class accountSystem
{
public:
    void loadDataFromFile(string);
    int logIn(string, string);
    int signUp(string, string);
    void SaveDataToFile(string);
private:
    char data[500];
    fstream file_read;
    ofstream file_app;
    string ID;
};

void accountSystem::loadDataFromFile(string)
{
    file_read.open("account.txt",ios::in);
}

int accountSystem::logIn(string un, string pw)
{
    file_read.read(data,500);
    string check(data),pw_c;
    if (check.find(un,0)!=-1)
    {
        if (check.find(un,0)==0||check[check.find(un,0)-1]=='\n')
        {
            pw_c.assign(check,check.find(un,0)+un.length()+1,pw.length());
            if (pw_c==pw)
            return 1;
            else
            return 0;
        }
        else
        return 0;
    }
    else
    return 0;
}

int accountSystem::signUp(string un, string pw)
{
    file_app.open("account.txt",ios::app);
    file_read.read(data,500);
    string check(data);
    if (check.find(un,0)==-1)
    {
        ID=un+' '+pw+'\n';
        file_app << ID;
        file_app.close();
        file_read.close();
        file_read.open("account.txt",ios::in);
        return 1;
    }
    else
    {
        if (check.find(un,0)!=0)
        {
            if (check[check.find(un,0)+un.length()]==' '&&check[check.find(un,0)-1]=='\n')
            return 0;
            else
            {
                ID=un+' '+pw+'\n';
                file_app << ID;
                file_app.close();
                file_read.close();
                file_read.open("account.txt",ios::in);
                return 1;
            }
        }
        else
        {
            if (check[check.find(un,0)+un.length()]==' ')
            return 0;
            else
            {
                ID=un+' '+pw+'\n';
                file_app << ID;
                file_app.close();
                file_read.close();
                file_read.open("account.txt",ios::in);
                return 1;
            }
        }
    }
}

void accountSystem::SaveDataToFile(string)
{
    file_read.close();
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
