// have some bugs that i couldn't fix
#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;
string array[9999] = {}; // set an array
class accountSystem // accountSustem class definition
{
  public: // public data
        accountSystem(); // constructor
        void loadDataFromFile(string); // function
        bool logIn(string, string);
        bool signUp(string, string);
        void SaveDataToFile(string);
    private: // private data
        string u, p;
        string file;
};

accountSystem::accountSystem() // constructor
{
    u = " ";
    p = " ";
    file = " ";
}

void accountSystem::loadDataFromFile(string f) // load the file
{
    file = f;
    ifstream inAccountFile(file.c_str(), ios::in);
    if (!inAccountFile)
    {
        cerr << "File could not be opened" << endl;
        exit (1);
    }
}

bool accountSystem::logIn(string username, string password) // definition
{
    ifstream inAccountFile(file.c_str(), ios::in); // load file
    u = username;
    p = password;
    string a, b;
    while (inAccountFile >> a >> b) // take out the username and pass to compare
    {
        if (u.compare(a) == 0 && p.compare(b) == 0) // compare the input user, pass and file's user, pass
            return true; // if it's equal, return true
        else
            return false; // else false
    }
    inAccountFile.seekg(0); // move to the begening of the file
}

bool accountSystem::signUp(string username, string password) // definition
{
    ifstream inAccountFile(file.c_str(), ios::in); // load file
    u = username;
    p = password;
    string a, b;
    while (inAccountFile >> a >> b) // compare input user and falie's user
    {
        if (u.compare(a) == 0) // if there's same user
            return false; // return false
    }
    return true; // if there's not same user, return true
    inAccountFile.seekg(0); // move to the begining of the file
}

void accountSystem::SaveDataToFile(string f) // definition
{
    ofstream saveAccountFile(f.c_str(), ios::app); // move to the end of the file
    if (!saveAccountFile)
    {
        cerr << "File could not be opened" << endl;
        exit (1);
    }
    saveAccountFile << endl << u << ' ' << p; // save the new input user and pass
    saveAccountFile.close(); // close the file
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
