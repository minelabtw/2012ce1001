//you must design one or more class to complete this program
#include<iostream>
#include<string>
#include<fstream>

using namespace std ;

class accountSystem
{
    public:
        void loadDataFromFile(  string filename )//讀取account.txt
        {
            read.open( "account.txt" , ios::in );
            write.open( "account.txt" , ios::app );
        }

        bool logIn( string name , string word)//檢察帳號與密碼
        {
            while( read >> namecheck >> wordcheck)
            {

                if( namecheck == name && wordcheck == word)
                    return true;
            }
            return false;
        }

        bool signUp( string name , string word)//檢查帳號  重覆則錯誤
        {
            while( read >> namecheck )            //無重複則輸入新帳號與密碼
            {

                if( namecheck == name)
                    return false;
            }
            write << name << " " << word << endl;
            return true;

        }
        void SaveDataToFile( string filename )//關閉account.txt
        {
            read.close();
            write.close();
        }

    private:
        ifstream read;
        ofstream write;
        string namecheck,wordcheck;



};

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
