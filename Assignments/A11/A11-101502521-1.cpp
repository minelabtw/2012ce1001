#include <iostream>
#include <string.h>
#include <fstream>
#include <vector>
#include <stdlib.h>

using namespace std;

class accountSystem
{
public:
    void loadDataFromFile(string filename);
    void SaveDataToFile(string filename);
    bool logIn(string username, string password);
    bool signUp(string username, string password);

private:
    int old_data_num=0;
    vector<string> usnlist;
    vector<string> pswlist;
};

void accountSystem::loadDataFromFile(string filename)
{
    ifstream fin(filename.c_str());
    if(!fin)
    {
        cout << "Can't open file!\n";
        exit(-1);
    }
    string t1, t2;
    while(fin >> t1 >> t2)
    {
        usnlist.push_back(t1);
        pswlist.push_back(t2);
    }
    old_data_num = usnlist.size();
    fin.close();
}

void accountSystem::SaveDataToFile(string filename)
{
    ofstream fout(filename.c_str(), ios::app);
    for(int i=old_data_num;i<usnlist.size();i++)
        fout << usnlist[i] << " " << pswlist[i] << endl;
    fout.close();
}

bool accountSystem::logIn(string username, string password)
{
    for(int i=0;i<usnlist.size();i++)
        if(username==usnlist[i] && password==pswlist[i])
            return true;
    return false;
}

bool accountSystem::signUp(string username, string password)
{
    for(int i=0;i<usnlist.size();i++)
        if(username == usnlist[i])
            return false;
    usnlist.push_back(username);
    pswlist.push_back(password);
    return true;
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
