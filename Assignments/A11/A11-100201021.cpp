#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
using namespace std;

class accountSystem
{
public:
    accountSystem();
    void loadDataFromFile(string file);
    bool logIn(string name,string pass);
    bool signUp(string name,string pass);
    void SaveDataToFile(string file);
private:
    string user[100];   //帳號資料
    int c;              //c 記錄資料數
};

accountSystem::accountSystem()
{
    c=0;
}

void accountSystem::loadDataFromFile(string file)
{
    ofstream Txt("account.txt",ios::out);
    if(!Txt)
    {
        cerr << "no File" << endl;
        exit(1);
    }
}

bool accountSystem::logIn(string name,string pass)
{
    ifstream Txt("account.txt",ios::in);
    string C_name,C_pass;
    Txt.seekg(0);
    while( !Txt.eof() )         //判斷至檔案結束
    {
        Txt >> C_name >> C_pass;
        if(name == C_name && C_pass == pass)    //完全相等 >> 登入成功
        {
            return true;
            break;
        }
    }
    return false;           //全部不相符 >> 登入失敗
}
bool accountSystem::signUp(string name,string pass)
{
    ofstream Txt("account.txt",ios::app);
    for(int i = 0;i<c;i++)      //判斷重複
        if(name == user[i])
        {
            return false;
            break;              //登錄失敗 (false)
        }
    user[c] = name;
    Txt << user[c] << " " << pass << endl;  //寫入
    c++;                                    //資料數+1
    return true;
}
void accountSystem::SaveDataToFile(string file)
{
    ofstream Txt("account.txt",ios::out);
    Txt.clear();                            //
}
int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
