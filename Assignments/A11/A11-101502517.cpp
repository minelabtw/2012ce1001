#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;
///////////////全域宣告//////////////////
const int size=100;
int k=1,y=1;
string aa[size],pp[size];
ofstream oacc;
ifstream iacc;
///////////////////////////
class accountSystem//定義CLASS
{
    public:
        accountSystem();
        void loadDataFromFile(string);
        int logIn(string,string);
        int signUp(string,string);
        void SaveDataToFile(string);

    private:


};
accountSystem::accountSystem()//建構子
{

}

void accountSystem::loadDataFromFile(string t)//讀檔
{
    oacc.open("account.txt",ios::app);
    iacc.open("account.txt");
    if(!oacc || !iacc)//讀檔成功否
    {
        cout<<"ERROR!!"<<endl<<endl;
        exit(1);
    }

    int i=1;
    while(iacc>>aa[i]>>pp[i])//取出資料
    {
        i++;
        y++;
    }
}
int accountSystem::logIn(string a,string p)//登入
{

    for(int i=1;i<size;i++)//比對
    {
        if(a==aa[i] && p==pp[i])
            return 1;
    }

    return 0;
}
int accountSystem::signUp(string a,string p)//註冊
{
    for(int i=1;i<size;i++)//先確認帳號有否相同
    {
        if(a==aa[i])
            return 0;
    }
    ///////不相同時，寫入陣列//////
    k=y;
    aa[k]=a;
    pp[k]=p;
    k=k+1;
    return 1;



}
void accountSystem::SaveDataToFile(string t)//存檔
{
    for(int i=y;i<k;i++)//將新資料寫入TXT
    {
        oacc<<aa[i]<<" "<<pp[i]<<"\n";
    }
}
int main()

{

    string response,username,password;

    accountSystem AccountSystem;

    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt

    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

    while( cin>>response )

    {

        if(response.compare("1") == 0)

        {

            cout << "Username : ";

            cin >> username;

            cout << "Password : ";

            cin >> password;

            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"

                cout << "Login succeed!" << endl;

            else

                cout << "Login failed!" << endl;

        }

        else if(response.compare("2") == 0)

        {

            cout << "Username : ";

            cin >> username;

            cout << "Password : ";

            cin >> password;

            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"

            cout << "Sign up succeed!" << endl;

            else

            cout << "Detected same username. Return to menu." << endl;

        }

        else if(response.compare("3") == 0)

        {

            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt

            break;

        }

        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

    }

    return 0;

}

