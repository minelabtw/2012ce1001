#include<iostream>
#include<string>
#include<cstring>
#include<fstream>
#include<sstream>

using namespace std;

class accountSystem
{
public:
    void loadDataFromFile(string)
    { ofile=fopen("account.txt","a+"); }

    int logIn(string input_name, string input_pass)
    {
        ifstream inFile("account.txt");
        string line;
        while(getline(inFile,line))
        {
           istringstream ss(line);
           string name, pass;
           ss>>name;
           ss>>pass;
           if((input_name==name)&&(input_pass==pass)){return 1;}
        }
        return 0;
    }

    int signUp(string input_name, string input_pass)
    {
        ifstream inFile("account.txt");
        string line;
        int lon=0,ok=0;

        while(getline(inFile,line))
        {
           istringstream ss(line);
           string name, pass;
           ss>>name;
           ss>>pass;

           lon=lon+1;
           if(input_name!=name)
           { ok=ok+1;}
           else {continue;}
        }
        if(lon==ok)
        {
           fprintf(ofile,"\n%s %s",input_name.c_str(),input_pass.c_str());
           fclose(ofile);
           ofile=fopen("account.txt","a+");
           return 1;
        }
        else {return 0;}
    }

    void SaveDataToFile(string input)
    {fclose(ofile);}

private:
    FILE *ofile;
};

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
