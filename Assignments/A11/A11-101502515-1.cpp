#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

class accountSystem{
private:
    vector<string> accvec;
    vector<string> pasvec;
public:
    void loadDataFromFile(const char * fname){//開啟檔案的讀寫&存入private
        string accbuf;//用來暫存
        string pasbuf;
        ifstream tfile(fname);
        if(tfile==0){
            ofstream tfile1(fname);//如果沒有fname的txt創造txt
            tfile1.close();
        }
        while(!tfile.eof()){
            if(tfile>>accbuf==false)
            break;
            //tfile>>accbuf;
            accvec.push_back(accbuf);
            tfile>>pasbuf;
            pasvec.push_back(pasbuf);
        }
        tfile.close();//關閉
    }
    bool logIn(string acc,string pas){//登入
        for(int i=0;i<accvec.size();i++){
            if(acc==accvec[i])//帳號符合
            if(pas==pasvec[i])//接著檢查密碼
                return true;//密碼也正確
        }/////檢查完畢
        return false;
    }
    bool signUp(string acc,string pas){//註冊
        for(int i=0;i<accvec.size();i++){//檢查重複帳號
            if(acc==accvec[i])//重複
            return false;
        }//完成
        accvec.push_back(acc);
        pasvec.push_back(pas);
        return true;
    }
    void SaveDataToFile(const char * fname)
    {
        ofstream tfile(fname);//打開
        for(int i=0;i<accvec.size();i++){//存檔
            tfile<<accvec[i]<<" ";
            tfile<<pasvec[i]<<endl;
        }
    }
    void show()
    {
        for(int i=0;i<accvec.size();i++){//存檔
            cout<<accvec[i]<<" ";
            cout<<pasvec[i]<<endl;
        }
    }
};

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)//當response是1的時候
        {
            AccountSystem.show();
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
