#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;
class list
{
public:
    list(string =" ");  //default constructor
    void setdata(string ); //set data
    string getdata();  //return data
private:
    string cdata;   //storage data
};//end class list
list::list(string x) //initialize cdata to " "
{
    cdata=x;
}
void list::setdata(string word)
{
    cdata=word;
}
string list::getdata()
{
    return cdata;
}

class accountSystem
{
public:
    void setIndex(int );
    int getIndex();
    void loadDataFromFile(char *);
    bool logIn(string ,string );
    bool signUp(string ,string );
    void SaveDataToFile(char *);

private:
   list sysuser[1000];  //limit 1000 username
   list syspw[1000];    //limit 1000 password
   int cdex;            //accountSystem numbers
};//end class accountSystem
void accountSystem::setIndex(int x)  //set cdex value
{
    cdex=x;
}
int accountSystem::getIndex()  //return cdex value
{
    return cdex;
}
void accountSystem::loadDataFromFile(char *file)
{
    ifstream inFile(file,ios::in);  //read account.txt
    string str1,str2;
    int index=0;
    while(inFile>>str1>>str2)  //storage every account data
    {
        sysuser[index].setdata(str1);
        syspw[index].setdata(str2);
        index++;
    }
    setIndex(index);  //record account number
}

bool accountSystem::logIn(string testuser,string testpw)
{
    for(int j=0;j<getIndex();j++)  //check username and password if exist,
    {                               //if exist logIn succeed,or logIn failed
        if(sysuser[j].getdata()==testuser && syspw[j].getdata()==testpw)
            return true;
    }
    return false;
}

bool accountSystem::signUp(string testuser,string testpw)
{
    for(int j=0;j<getIndex();j++)  //check username if exist
    {                               //if exist signUp failed
        if(sysuser[j].getdata()==testuser)
            return false;
    }
    sysuser[getIndex()].setdata(testuser);  //signUp succeed,add new account data into list
    syspw[getIndex()].setdata(testpw);
    setIndex(getIndex()+1);   //account number++
    return true;
}

void accountSystem::SaveDataToFile(char *file)
{
    ofstream outFile(file,ios::out);   //open account.txt
    for(int j=0;j<getIndex();j++)     //save every account data into file
        outFile<<sysuser[j].getdata()<<" "<<syspw[j].getdata()<<" "<<endl;
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
