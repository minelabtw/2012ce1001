//you must design one or more class to complete this program
#include<iostream>
#include<string>
#include<fstream>
#include<vector>
using namespace std;

class accountSystem
{
    public:
    void loadDataFromFile(string);//如果直接打"account.txt"就只能讀此檔
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(string);//如果直接打"account.txt"就只能讀此檔

    private:
    vector<string> usernames;//帳號
    vector<string> passwords;//密碼
};

//讀入
void accountSystem::loadDataFromFile(string address)//如果直接打"account.txt"就只能讀此檔
{
    ifstream fout(address.c_str());//fout是自己取的
    string username;
    string password;

    while(fout>>username>>password)
    {
        usernames.push_back(username);
        passwords.push_back(password);
    }
}

bool accountSystem::logIn(string username,string password)
{
    bool flag=false;
    for(int i=0;i<usernames.size();i++)//偵測是否重複
    {
        if(username==usernames[i]&&password==passwords[i])
        {
            return true;
        }
    }
    if(flag==false)
    {
        return false;
    }

}
bool accountSystem::signUp(string username,string password)
{
    int check=0;
    ofstream fout("account.txt");
    for(int i=0;i<usernames.size();i++)//偵測是否重複
    {
        if(username==usernames[i])
        {
        return false;
        }
        else
        {
            check=check+1;
        }
    }

    if(check==usernames.size())
    {
        usernames.push_back(username);
        passwords.push_back(password);
    }
    return true;
}

void accountSystem::SaveDataToFile(string address)
{
    for(int i=0;i<usernames.size();i++)
    {
        ofstream fout(address.c_str());
        fout<<usernames[i]<<" "<<passwords[i];
    }

}



int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
