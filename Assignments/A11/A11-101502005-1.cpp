#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;
class accountSystem
{
public:
    void loadDataFromFile(char []);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(string);
private:
    ifstream loadFile;//讀檔不會建立檔案
    ofstream signFile;//寫檔

};
void accountSystem::loadDataFromFile(char loadData[])
{   //所以必須先檔來建立檔案才能讀檔



    signFile.open(loadData,ios::app);//附加在原本有的東西之後
    if(!signFile)
    {
        cerr<<"檔案無法被建造!"<<endl;
        exit(1);
    }
    loadFile.open(loadData,ios::in);//先寫再讀
    if(!loadFile)
    {
        cerr<<"檔案讀取失敗!"<<endl;
        exit(1);
    }


}
bool accountSystem::logIn(string logIn1,string logIn2)
{
    string logIn3="",logIn4="";//存取檔案讀取資料，""為初始空字串
    loadFile.clear();//如果獨到end of file，將標籤剔除
    loadFile.seekg(0);//把標籤位置移回原始處，()中指檔案偏移量
    while(loadFile>>logIn3>>logIn4)//從檔案中讀取資料
    {
        if(logIn1==logIn3&&logIn2==logIn4)
            return true;
    }
    return false;
}
bool accountSystem::signUp(string signUp1,string signUp2)
{
    string signUp3="",signUp4="";//初始空字串
    loadFile.clear();//如果獨到end of file，將標籤剔除
    loadFile.seekg(0);
    while(loadFile>>signUp3>>signUp4)
    {
        if(signUp1==signUp3)
            return false;
    }
    signFile<<signUp1<<' '<<signUp2<<endl;
    return true;
}
void accountSystem::SaveDataToFile(string SaveData)
{
    loadFile.close();
    signFile.close();
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
