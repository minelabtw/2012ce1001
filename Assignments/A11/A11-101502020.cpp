//A11 by J
//Damn A11 which spend me 2 days.
#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;

string a[2]={ } ;       //this global a[] will be used for checking!
string b[50000]={ } ;
int i=0 ;

class accountSystem
{
public:
    accountSystem() ;       //constructor
    void loadDataFromFile( string ) ;
    bool logIn( string , string ) ;
    bool signUp( string , string ) ;
    void SaveDataToFile( string ) ;
private:
    string file ;       //for passing file name
    string iur ;
    string ipw ;
};

accountSystem::accountSystem()
{
    //an empty constructor
}

void accountSystem:: loadDataFromFile( string str )
{
    file=str ;
    ifstream inAccountFile( file.c_str() , ios::in );
    if (!inAccountFile)
    {
        cerr << "File could not be opened" << endl;
        exit (1);
    }       //check for file accessable
}

bool accountSystem::logIn( string user , string pw )
{
    bool tft=false ;
    ifstream inAccountFile( file.c_str() , ios::in );
    while ( inAccountFile >> a[0] >> a[1] )
    {
        if ( a[0].compare(user)==0 && a[1].compare(pw)==0  )
            tft=true ;      //there exists a pair of username & password suit for input
    }

    if (tft==true)
        return true ;
    else
        return false ;
}

bool accountSystem::signUp( string user , string pw )
{
    bool TF=false ;
    ifstream inAccountFile( file.c_str() , ios::in );
    while ( inAccountFile>>a[0]>>a[1] )
    {
        if ( user.compare(a[0])==0 )
            TF=true ;      //there exists the same user name.
    }
    if ( TF==true )
        return false ;      //exist same user name
    else
    {
        b[i]=user ; b[i+1]= pw ; i+=2 ;
        return true ;       //no same user name, sign up this account
    }

}

void accountSystem::SaveDataToFile( string str )
{
    ofstream outAccountFile ( str.c_str() , ios::app ) ;      //set txt file status.

    if ( !outAccountFile )      //error message
    {
        cerr << "File could not be opened." << endl ;
        exit(1) ;
    }
    for (int j=0 ; j<=i ; j+=2 )
    {
        outAccountFile << endl << b[j] << ' ' << b[j+1] ;
    }
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
