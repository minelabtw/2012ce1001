#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;

//you must design one or more class to complete this program
class accountSystem
{
public:
    void loadDataFromFile(string);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(string);

private:
    ofstream outaccountFile;
    ifstream inaccountFile;
};
void accountSystem::loadDataFromFile(string a)
{
    outaccountFile.open( a.c_str() , ios::app );//開啓檔案
    if(!outaccountFile)
    {
        cerr << "File could not be created" << endl;
        exit(1);
    }
     inaccountFile.open(a.c_str() , ios::in);
     if(!inaccountFile)
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }

}
bool accountSystem::logIn(string b,string c)
{
    string account1,password1;
    inaccountFile.clear();//清除想比對的資料
    inaccountFile.seekg(0);//將標籤移到最前面
    while(inaccountFile >> account1 >> password1 )//讀檔案
    {
        if(b==account1&&c==password1)//若帳號密碼都符合
        return true;
    }
    return false;
}
bool accountSystem::signUp(string d,string e)
{
    string account1,password1;
    inaccountFile.clear();//清除想比對的資料
    inaccountFile.seekg(0);//將標籤移到最前面
    while(inaccountFile >> account1 >> password1 )
    {
        if(d==account1)//若帳號符合
        return false;
    }
    outaccountFile << d << ' ' << e << endl;
    return true;
}
void accountSystem::SaveDataToFile(string k)
{
    inaccountFile.close();
    outaccountFile.close();//先儲存再關檔案
}


int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
