#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

//you must design one or more class to complete this program

class accountSystem
{
public:
    accountSystem()
    {

    }

    void loadDataFromFile(string direction)
    {
        address=direction;
        fstream CheckAccount(address.c_str(),ios::in);

        while(getline(CheckAccount,tempLine))
            CheckName.push_back(tempLine);
    }

    bool logIn(string UserName,string PassWord)
    {
        for(int i=0;i<CheckName.size();i++)//如果帳號密碼都符合就可以標記可登入
            if(CheckName[i].compare(0,UserName.size(),UserName)==0 &&
               CheckName[i].compare(UserName.size()+1,PassWord.size(),PassWord)==0)
            {
                return true;
            }
    }

    bool signUp(string UserName,string PassWord)
    {
        for(int i=0;i<CheckName.size();i++)//檢查輸入的帳號名稱與之前有沒有重複
            if(CheckName[i].compare(0,UserName.size(),UserName)==0)
            {
                return false;
            }

        ofstream createAccount(address.c_str(), ios::app);

        if(createAccount.is_open())
        {
            createAccount<<UserName<<" ";
            createAccount<<PassWord<<"\n";
            CheckName.push_back(UserName+" "+PassWord);//再把註冊好的存起來，用於下次檢查
        }
        return true;
    }

    void SaveDataToFile(string direction)
    {

    }
private:
    vector<string> CheckName;
    string tempLine,address;
};

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
