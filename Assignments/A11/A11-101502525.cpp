#include<iostream>
#include<fstream>
#include<vector>
using namespace std;


class accountSystem
{
    public:
        void loadDataFromFile(const char *filename)
        {
            ifstream in(filename);
            for(accountNumber=0;in>>tmp.username>>tmp.password;accountNumber++)//load data and count number
                account.push_back(tmp);
        }
        bool logIn(string username, string password)
        {
            for(int i=0;i<account.size();i++)
                if(account[i].username==username&&account[i].password==password)//correct username & password
                    return true;
            return false;
        }
        bool signUp(string username, string password)
        {
            for(int i=0;i<account.size();i++)
                if(account[i].username==username)//same account exists
                    return false;
            tmp.username=username;
            tmp.password=password;
            account.push_back(tmp);//signup new account
            return true;
        }
        void SaveDataToFile(const char *filename)
        {
            ofstream out(filename, ios::app);//add new data at the end of file
            for(int i=accountNumber;i<account.size();i++)//only write new data
                out<<endl<<account[i].username<<' '<<account[i].password;
        }
    private:
        struct ACCOUNT
        {
            string username;
            string password;
        }tmp;
        vector<ACCOUNT> account;
        int accountNumber;
};


int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
