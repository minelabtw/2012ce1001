#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <iomanip>
using namespace std;

class accountSystem
{
public:
    void loadDataFromFile(string);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(string);
    int x;//去控制陣列的存取
private:
    string buffer1[1000];//to store username
    string buffer2[1000];//to store password

};

void accountSystem::loadDataFromFile(string str)
{
    ifstream infile( "account.txt" , ios::in );
    string a,b;

    if(!infile)
    {
        cout << "File could not be opened" << endl;
        exit(1);
    }
    else
    {
        int i=0;

        while ( infile >> a >> b)
        {
            buffer1[i] = a;
            buffer2[i] = b;
            i++;
        }
    }

}



bool accountSystem::logIn(string username,string password)
{
    bool check=false;

    for (int i=0 ; i<=1000 ; i++)
    {
        if (username == buffer1[i])//user correct
        {
            if (password == buffer2[i])
                return true;
            else
                return false;
        }
        else
            continue;

    }

    if (check == false)
        return false;

}

bool accountSystem::signUp(string username , string password)//store user to string[]
{
    bool check=true;

    for (int i=0 ; i<=1000 ; i++)
    {
        if ( buffer1[i] == username )
        {
            check = false;
            break;
        }
    }

    if (check == true)
    {
        buffer1[x] = username;
        buffer2[x] = password;
        x++;
        return true;
    }
    else if (check == false)
        return false;

}

void accountSystem::SaveDataToFile(string str)
{
    ofstream outfile( "account.txt" , ios::app );

    for (int i=0 ; i<x ; i++)
    {
        outfile << buffer1[i] << ' ' << buffer2[i] << endl;
    }

}

int main()

{

    string response,username,password;

    accountSystem AccountSystem;

    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt

    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

    while( cin>>response )

    {

        if(response.compare("1") == 0)

        {

            cout << "Username : ";

            cin >> username;

            cout << "Password : ";

            cin >> password;

            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"

                    cout << "Login succeed!" << endl;

            else

                    cout << "Login failed!" << endl;

        }

        else if(response.compare("2") == 0)

        {

            cout << "Username : ";

            cin >> username;

            cout << "Password : ";

            cin >> password;

            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"

                    cout << "Sign up succeed!" << endl;

            else

                    cout << "Detected same username. Return to menu." << endl;

        }

        else if(response.compare("3") == 0)

        {

            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt

            break;

        }

        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

        }

        return 0;

}
