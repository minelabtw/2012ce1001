#include <iostream>
#include <fstream>
#include <string>
using namespace std;

class accountSystem
{
    private :
        fstream AccountFile, BufferFile;
        string DataBase, DataTemp;
        string account, password;
    public :
        void loadDataFromFile(string FileName)
        {
            DataBase = FileName;                                                         // 將引數字串設定至類別私有變數
            DataTemp = "buffer.txt";                                                     // 設定暫存檔案的檔案名稱
            return;
        }
        bool logIn(string UserInputAccount, string UserInputPassword)
        {
            bool flag=false;
            AccountFile.open(DataBase.c_str(),ios::in);                                  // 登入動作只需讀取資料，將檔案設為唯讀
            while ( !AccountFile.eof() )                                                 // 進入資料檔案重複搜尋至檔案結束(end of file)
            {
                AccountFile >> account >> password;                                      // 擷取出資料庫內的資料至類別中的私有變數
                if ( (UserInputAccount == account) && (UserInputPassword == password) )  // 在資料庫內搜尋到相符的帳號密碼
                {
                    flag = true;                                                         // 將登入標籤直設為真，代表登入成功
                    break;                                                               // 因為登入成功，故直接跳出搜尋迴圈
                }
            }
            AccountFile.clear();AccountFile.close();                                     // 清除並關閉資料檔案
            return flag;
        }
        bool signUp(string UserInputAccount, string UserInputPassword)
        {
            bool flag=true;
            BufferFile.open(DataTemp.c_str(),ios::out | ios::app);                       // 註冊程序需寫入資料，將暫存檔案設為可寫入
            AccountFile.open(DataBase.c_str(),ios::in);                                  // 搜尋資料時不需要更改資料庫，將此檔案設為唯讀
            while ( !AccountFile.eof() )
            {
                AccountFile >> account >> password;                                      // 擷取出資料庫內的資料至類別中的私有變數
                if ( UserInputAccount == account )                                       // 若在資料庫內搜尋到相同的帳號字串
                {
                    flag = false;                                                        // 表示該帳號已經有人使用，標籤值設為否
                    break;                                                               // 表示註冊失敗，並直接跳出蒐尋迴圈
                }
            }
            if ( flag )                                                                  // 若註冊成功
            {
                BufferFile << UserInputAccount << " " << UserInputPassword << endl;      // 將註冊資料輸出到暫存檔案
                SaveDataToFile(DataBase);                                                // 呼叫資料儲存函式，將暫存檔內之資料更新至資料庫
            }
            BufferFile.clear();BufferFile.close();
            AccountFile.clear();AccountFile.close();
            return flag;
        }
        void SaveDataToFile(string Destination)
        {
            string temp=account;                                                         // 將最後一個account的值擷取至temp變數，用來避免EOF造成的"多一次"的動作
            BufferFile.open(DataTemp.c_str(),ios::in);                                   // 開啟系統內暫存的資料檔案
            AccountFile.open(Destination.c_str(),ios::out | ios::app);                   // 開啟要更新的目的資料檔案
            if ( BufferFile.is_open() )                                                  // 若暫存的資料檔案開啟成功(或者是該檔案存在)，則執行更新的動作
            {
                while ( !BufferFile.eof() )
                {
                    BufferFile >> account >> password;
                    if ( account != temp )                                               // 避免因為EOF的判斷而多執行一次資料儲存的動作
                    {
                        AccountFile << account << " " << password << endl;
                        temp = account;
                    }
                }
            }
            BufferFile.clear();BufferFile.close();
            AccountFile.clear();AccountFile.close();
            remove(DataTemp.c_str());                                                    // 檔案清除並關閉後，刪除不必要的暫存檔
            return;
        }
};


int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)         // response == 1 , login
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)    // response == 2 , sign up
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)    // response == 3 , save
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
