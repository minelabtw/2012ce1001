//you must design one or more class to complete this program
#include <iostream>
#include <string>
#include <fstream>//file stream
#include <cstdlib>
using namespace std;

class accountSystem
{
public:
    accountSystem();
    void outAccount();//the initial in account.txt
    void loadDataFromFile(char*);//in the account.txt
    void compare(string);//compare login, sign up, or exit
    bool logIn(string,string);//login
    bool signUp(string,string);//sign up
    void SaveDataToFile(char*);//exit
    string account123[100];
    //~accountSystem();
private:
    string account[100],code[100];//save account and code
    int count,mount;
};
accountSystem::accountSystem()
{
    mount=count=0;
}

void accountSystem::loadDataFromFile(char *txt)
{   //save the initial to the array
    ifstream inaccount(txt,ios::in);
    if(!inaccount)
    {
        cerr << "File couldn't be opened." << endl;
        exit(1);
    }
    for ( int i=0; i<100 ; i++ )
    {
        inaccount >> account[i] >> code[i];

        if( account[i]== "" )
        {
            mount=i;//mount=how many accounts
            break;
        }
    }
}

void accountSystem::compare(string com)
{   //determine the user want to login , sign up ,or exit
    if(com=="1")
        com="0";
    if(com=="2")
        com="0";
    if(com=="3")
        com="0";
}

bool accountSystem::logIn( string inaccount, string incode )
{
    for( int i=0; i<=mount+count; i++ )
    {
        if( inaccount==account[i] && incode==code[i] )
            return true;//login success
    }
    return false;//fail
}

bool accountSystem::signUp( string inaccount, string incode )
{
    for( int i=0; i<=mount+count; i++ )
    {
        if( inaccount==account[i] )
            return false;//sign up the same account
    }

    account[mount+count]=inaccount;//save to the array account
    code[mount+count]=incode;//save to the array code
    count++;//count=sign up++
    return true;//sign up success
}

void accountSystem::SaveDataToFile(char *txt)
{   //user exit ~ save all to the account.txt
    ofstream outaccount(txt,ios::out);
    if(!outaccount)
    {
        cerr << "File couldn't be opened." << endl;
        exit(1);
    }
    for (int i=0; i<=mount+count; i++)
        outaccount << account[i] << " " << code[i] << endl;
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");
    //load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

    while( cin>>response )//user input to determine what they want to do
    {
        if(response.compare("1") == 0)//login
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;

            if(AccountSystem.logIn(username,password))
            //this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }

        else if(response.compare("2") == 0)//sgn up
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;

            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }

        else if(response.compare("3") == 0)//exit
        {
            AccountSystem.SaveDataToFile("account.txt");
            //it will update account.txt
            break;
        }

        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }

    return 0;
}
