#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;

class accountSystem
{
public:
    accountSystem();
    void loadDataFromFile(const char*);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(const char*);
private:
    string Raccount[100000];
    string Rpassword[100000];
    string test[100000];
    string U[100000];
    string P[100000];
    int n,number,m;
};

accountSystem::accountSystem()
{
    n=number=m=0;
}

void accountSystem::loadDataFromFile(const char *FileName)
{

    ifstream inAccountFile(FileName,ios::in);
    if(!inAccountFile)
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    string account;
    string password;

    while(inAccountFile>>account>>password)
    {
        Raccount[n]=account;//load the ID from account.txt
        Rpassword[n]=password;//load the password from account.txt
        n++;
    }
}

bool accountSystem::logIn(string Username,string Password)
{
    for(int i=0; i<n; i++)
    {
        if(Raccount[i]==Username&&Rpassword[i]==Password)//test if key in the correct ID and password
        {
            return true;
        }
    }
}

bool accountSystem::signUp(string Username,string Password)
{
    bool T=true;

    for(int i=0; i<n; i++)//test the ID if duplicate
    {
        if(Raccount[i]==Username)
            return false;
    }
    for(int i=0; i<number; i++) //test the ID if duplicate
    {
        if(test[i]==Username)
        {
            T=false;
            return false;
        }
    }
    test[number]=Username;//save the every ID which we key in
    number++;
    if(T==true)//save the ID which sign up complete
    {
        U[m]=Username;
        P[m]=Password;
        m++;
        return true;
    }
}

void accountSystem::SaveDataToFile(const char *FileName)
{
    int M=0;
    ofstream outAccountFile(FileName,ios::app);
    if(!outAccountFile)
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    while(M<=m)
    {
        outAccountFile<<"\n"<<U[M]<<' '<<P[M];//save the data to account.txt
        M++;
    }
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
