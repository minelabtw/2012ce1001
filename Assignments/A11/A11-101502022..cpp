#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
using namespace std;


static int i=3;//將註冊的帳密存入陣列中要使i不被還原,所以使用static
class accountSystem
{

public:
        bool logIn(string , string );
        bool signUp(string , string);
        void loadDataFromFile(string);
        void SaveDataToFile(string);

private:
        string username;
        string password;
        string response;
        string minelab[10000];
        string mine[10000];
        int j;
        string acount;
        string pass;
        string a;
        string b;
        string d;
        string e;
};

void accountSystem::loadDataFromFile(string d)
{
    int j=0;
    ifstream loadDataFromFile("account.txt ", ios::in);
    while(loadDataFromFile>>acount>>pass)//將現有的帳號密碼先讀出來
    {
        minelab[j]=acount;
        mine[j]=pass;
        j++;
    }
}

bool accountSystem::signUp(string username,string password)//測試帳密是否可註冊
{

    ofstream  loadDataFromFile("account.txt ", ios::app);//app附加上去
    minelab[i]=username;
    mine[i]=password;
    bool c=true;
    while(1)
   {
           for(int n=0;n<i;n++)//檢驗帳號是否有相同
           {

               if(minelab[n]==minelab[i])
               {
                     c=false;
                     break;
               }
           }
            if(c==true)
            {
                i++;
                return true;
            }
            else
            {
                return false;
            }

   }
}

bool accountSystem::logIn(string username,string password)//測試帳密是否可登入
{

    bool c=true;
    ifstream loadDataFromFile("account.txt",ios::in);
    int j=0;
    string a=username;
    string b=password;

    while(loadDataFromFile>>acount>>pass)
    {
        minelab[j]=acount;//將現有的帳號密碼先讀出來
        mine[j]=pass;
        j++;
    }
    while(1)
    {
        for(int n=0;n<=j;n++)
        {
            if(minelab[n]==a&&mine[n]==b)//若為相等，回傳一個false
            {
                c=true;
                break;
            }
            else
            {
                c=false;
            }
        }

        if(c==true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}


void accountSystem::SaveDataToFile(string e)
{
    ofstream  loadDataFromFile("account.txt ", ios::app);//app附加上去
    for(int f=3;f<=i;f++)
    {
        loadDataFromFile<<minelab[f]<<' '<<mine[f]<<"\n";//將帳秘存入account.txt
    }


}


int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
