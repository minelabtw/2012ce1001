#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;
class accountSystem
{
public:
    void loadDataFromFile(string);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(string);
private:
    string response,username,password;
    ofstream outAccountfile;
    ifstream inAccountfile;
};
void accountSystem::loadDataFromFile( string FileName )
{
    inAccountfile.open( FileName.c_str(), ios::in );
    if( !inAccountfile )
        outAccountfile.open( FileName.c_str(), ios::out );
    else
        outAccountfile.open( FileName.c_str(), ios::app );
}

bool accountSystem::logIn(string a,string b)
{
    string c,d;
    ifstream inAccountfile("account.txt",ios::in);
    int test=0;
    if(!inAccountfile)
    {
        exit(1);
    }
    while (inAccountfile>>c>>d)
    {
        if(c.compare(a) == 0&&d.compare(b)==0)
            test = 1;

    }
    if(test==1)
        return true ;
    else
        return false;
}
bool accountSystem::signUp(string a ,string b)
{
    int test1 = 0;
    string c,d;

    if(!outAccountfile)
        exit(1);
    while(inAccountfile>>c>>d)
        if (c.compare(a)==0)
        {
            test1 = 1;
            break;
        }
    inAccountfile.clear();
    inAccountfile.seekg( 0, ios::beg );
    if(test1==1)
        return false;
    else
    {
        outAccountfile<<a<<' '<<b<<endl;
        return true;
    }

}
void accountSystem::SaveDataToFile( string FileName )
{

}
int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
