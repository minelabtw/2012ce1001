#include<iostream>
#include<iomanip>
#include<string>
#include<fstream>
#include<cstdlib>

using namespace std ;

class accountSystem
{
    public:
        accountSystem() ;
        void loadDataFromFile(string account) ;
        bool logIn( string, string ) ;
        bool signUp( string, string ) ;
        void SaveDataToFile(string account) ;

    private:
        int dataNum;//代表目前文件中有幾筆資料
        string c_account[100] ;//儲存文件中的帳號
        string c_secret[100] ;//儲存文件中的密碼
};

accountSystem::accountSystem()
{//建立一個檔案
    ofstream outAccountFile("account.txt", ios::out) ;

    if (!outAccountFile)
    {
        cerr << "File could not be opened" << endl ;
        exit( 1 ) ;
    }
}

// 開啟當按並將讀出來的資料儲存於c_account、c_secret
void accountSystem::loadDataFromFile(string account)
{
    ifstream inAccountFile("account.txt", ios::in) ;
    dataNum = 0;
    string username;//暫時紀錄讀出的帳號與密碼
    string password;

    if (!inAccountFile)
    {
        cerr << "File could not be opened" << endl ;
        exit( 1 ) ;
    }


    while( inAccountFile >> username >> password )
    {
        c_account[dataNum] = username;
        c_secret[dataNum] = password;
        dataNum++;//累加dataNum紀錄資料數
    }

    inAccountFile.close();
}

// 判斷帳號是否存在
bool accountSystem::logIn( string x, string y )
{
    //依據資料數量，比對帳號密碼是否相同
    for( int n=0 ; n<=dataNum ; n++ )
    {
        if(x==c_account[n] && y==c_secret[n])
        {
            return true;
        }
    }
    return false;
}

// 判斷帳號是否可以新增
bool accountSystem::signUp( string x, string y )
{
    //依據資料數量，比對帳號是否曾出現過
    for( int n=0 ; n<=dataNum ; n++ )
    {
        if(x==c_account[n])
        {
            return false;
        }
    }

    //若沒有出現過則新增資料
    c_account[dataNum] = x;
    c_secret[dataNum] = y;
    dataNum++;
    return true;
}

// 將新的資料儲存回文件
void accountSystem::SaveDataToFile(string account)
{
    ofstream outAccountFile("account.txt", ios::out) ;

    for ( int k=0 ; k<dataNum ; k++ )
    {
        outAccountFile << c_account[k] << ' ' << c_secret[k] << endl ;
    }
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }

    return 0;
}

