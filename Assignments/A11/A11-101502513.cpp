#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;

class accountSystem
{
public:
    void loadDataFromFile( string );
    bool logIn( string , string );
    bool signUp( string, string );
    void SaveDataToFile( string );
private:
    int x = 0; //it's a counter
    string UN,PW;
    string user[10000]; //it's a space that storing username
    string key[10000]; //it's a space that storing password
};

void accountSystem::loadDataFromFile( string File )
{
    //ifstream constructor opens the file
    ifstream inAccountFile( "account.txt", ios::in );

    //exit program if ifstream could not open file
    if( !inAccountFile )
    {
        cerr << "File Could not be opened" << endl;
        exit( 1 );
    }

    while( inAccountFile >> UN >> PW ) //read the "account.txt" file
    {
        user[x] = UN; //UN is username
        key[x] = PW; //PW is password
        x++; //there are x accounts
    }
}

bool accountSystem::logIn( string input, string output )
{
    bool logInButton = false;

    for( int i = 0; i < x; i++ )
    {
        //input and output is correct
        if( input == user[i] && output == key[i] )
        {
            logInButton = true;
            break;
        }
    }

    return logInButton;
}

bool accountSystem::signUp( string input, string output )
{
    bool signUpButton = true;
    user[x] = input;
    key[x] = output;

    for( int i = 0; i < x; i++ )
    {
        if( input == user[i] ) //the account is used
            signUpButton = false;
    }

    if( signUpButton )//if you sign up successfully
        x++;

    return signUpButton;
}

void accountSystem::SaveDataToFile( string File )
{
    //ofstream constructor opens the file
    ofstream outAccountFile( "account.txt", ios::out );

    //exit program if unable to create the file
    if( !outAccountFile )
    {
        cerr << "File Could not be opened" << endl;
        exit( 1 );
    }

    for( int y = 0; y < x; y++ ) //place new accounts into the "account.txt" file
    {
        outAccountFile << user[y] << ' ' << key[y] << endl;
    }
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
