#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

class accountSystem
{
  public:
    void loadDataFromFile( string );
    bool logIn( string, string );
    bool signUp( string, string );
    void SaveDataToFile( string );
  private:
    string Rac;//Rac -> the account that is read from the file
    string Rpw;//Rpw -> the password that is read from the file
    ifstream loadAccount;
    ofstream outAccount;
    vector<string> Register;
    void SaveData( string, string );
};

void accountSystem::loadDataFromFile( string FileName )
{
    loadAccount.open( FileName.c_str(), ios::in );
    if( !loadAccount )  //if file "account.txt" dosen't exist, it will create one.
        outAccount.open( FileName.c_str(), ios::out );
}

bool accountSystem::logIn( string ac, string pw ) //"ac" = "account", "pw" = "password"
{
    while( loadAccount >> Rac >> Rpw )  //read the data from "account.txt".
        if( ac == Rac && pw == Rpw )    //if the account and the password are exact.
            return true;

    loadAccount.clear();
    loadAccount.seekg(0); //to the beginnig data from "account.txt".
    return false;
}

bool accountSystem::signUp( string ac, string pw )
{
    while( loadAccount >> Rac >> Rpw )//to know whether the account has exited or not
        if( ac == Rac )
            return false;

    loadAccount.clear();
    loadAccount.seekg(0); //to the beginnig data from "account.txt"
    SaveData( ac, pw );
    return true;
}

void accountSystem::SaveData( string AC, string PW ) //to save the register from the function "signUp" to the vector "Register"
{
    Register.push_back(AC);
    Register.push_back(PW);
}

void accountSystem::SaveDataToFile( string FileName ) //to update "account.txt"
{
    outAccount.open( FileName.c_str(), ios::app );
    for( int i = 0 ; i < Register.size() ; i+=2 )
        outAccount << Register[i] << ' ' << Register[i+1] <<endl;
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
