#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <iomanip>
using namespace std;

class accountSystem
{
public:
    void loadDataFromFile(string);
    bool logIn(string, string);
    bool signUp(string, string);
    void SaveDataToFile(string);
private:
    string username, password;
};

void accountSystem::loadDataFromFile(string str)
{
	ifstream accountSystem("account.txt", ios::in);
    if(!accountSystem) {
        cerr << "File cloud not open." << endl;
		system("pause");
        exit(1);
    }
}

bool accountSystem::logIn(string user, string code)
{
    bool correct;
	ifstream accountSystem("account.txt" ,ios::in);
	while(accountSystem >> username >> password)
	{
        if(username == user)
			if(password == code)
			{
				correct = true;
				break;
			}
			else
				correct = false;
		else
			correct = false;
    }
	return correct;
}

bool accountSystem::signUp(string user, string code) {

	bool repeat;
    ifstream accountSystem("account.txt",ios::in);
    while(accountSystem >> username >> password) {
        if(user != username) {
            repeat = true;
        }
        else {
            repeat = false;
			break;
		}
	}
    if(repeat == true) {
        ofstream accountSystem("account.txt",ios::app);
        accountSystem << endl <<user << ' ' << code;
        return true;
    }else
        return false;
}

void accountSystem::SaveDataToFile(string str) {
    ofstream accountSystem("account.txt",ios::app);
    if(!accountSystem) {
        cerr << "File cloud not open." <<endl;
        exit(1);
    }
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
