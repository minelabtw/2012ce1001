#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class accountSystem
{
public:
    void loadDataFromFile(string);
    bool logIn(string, string);
    bool signUp(string, string);
    void SaveDataToFile(string);

private:
    int mCount;
    string mUsernameArray[100];
    string mPasswordArray[100];
};

void accountSystem::loadDataFromFile(string filename)
{
    mCount = 0;
    string username;
    string password;

    ifstream inClientFile(filename.data() , ios::in);

    while(!inClientFile.eof())
    {
        inClientFile >> username >> password;

        mUsernameArray[mCount] = username;//把讀到的帳號放入mUsernameArray裡
        mPasswordArray[mCount] = password;//把讀到的密碼放入mPasswordArray裡

        mCount++;
    }
}

bool accountSystem::logIn(string user, string pw)
{
    for(int i = 0 ; i < mCount ; i++)
    {
        if(user == mUsernameArray[i] && pw == mPasswordArray[i])//如果帳密皆同就成功
            return true;
    }

    return false;
}


bool accountSystem::signUp(string user, string pw)
{
    for(int i = 0 ; i < mCount ; i++)
    {
        if(user == mUsernameArray[i])//如果帳號相同則無法註冊
            return false;
    }

    mUsernameArray[mCount] = user;
    mPasswordArray[mCount] = pw;//把帳密放入陣列的最後面

    mCount++;

    return true;
}

void accountSystem::SaveDataToFile(string filename)
{
    ofstream outClientFile(filename.data() , ios::out);

    for(int i = 0 ; i < mCount - 1 ; i++)
    {
        outClientFile << mUsernameArray[i] << ' ' << mPasswordArray[i] << endl;//將除了最後一筆的每筆資料寫入account.txt裡
    }

    outClientFile << mUsernameArray[mCount - 1] << ' ' << mPasswordArray[mCount - 1];//因為最後一筆資料不能換行 所以另外寫
}

int main()
{
    string response,username,password;

    accountSystem AccountSystem;

    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt

    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;

            cout << "Password : ";
            cin >> password;

            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;

            else
                cout << "Login failed!" << endl;
        }

        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;

            cout << "Password : ";
            cin >> password;

            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;

            else
                cout << "Detected same username. Return to menu." << endl;
        }

        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }

        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }

    return 0;
}
