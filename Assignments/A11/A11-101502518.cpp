#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
#include<vector>

using namespace std;

class accountSystem
{
    public:
        void loadDataFromFile(string);
        bool logIn(string,string);
        bool signUp(string,string);
        void SaveDataToFile(string);

    private:
        vector<string>ac;
        vector<string>key;
        string username,password;
};

void accountSystem::loadDataFromFile(string a)//讀檔
{
    ifstream account(a.c_str(),ios::in);

    while(account>>username>>password)
    {
        ac.push_back (username);
        key.push_back (password);
    }

}

bool accountSystem::logIn(string username,string password)//登入
{
    bool flag=false;
    for(int i=0;i<ac.size();i++)
    {//start for
        if(ac[i]==username&&key[i]==password)
        {
            flag=true;
            return true;
            break;
        }
    }//end for
    if(!flag)
        return false;
}

bool accountSystem::signUp(string username,string password)//註冊
{
    for(int i=0;i<ac.size();i++)
    {
        if(ac[i]==username)
        {
            return false;
        }
    }
    ac.push_back (username);
    key.push_back (password);
    return true;
}
void accountSystem::SaveDataToFile(string a)//存取更新
{
    ofstream account(a.c_str(),ios::out);

    for(int i=0;i<ac.size();i++)
        account<<ac[i]<<' '<<key[i]<<endl;

}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
