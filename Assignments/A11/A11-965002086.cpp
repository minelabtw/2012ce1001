/*
11 Login and Sign up system

利用您從課堂上學到的開讀檔知識，寫出一個帳號

登入註冊系統。

將使用者的帳號及密碼記錄在 account.txt 。

輸入：

1 表示登入，2 表示註冊，3 表示結束程式並更新

account.txt。

輸出：

詳見程式範例結果。

程式範例結果：
 1.Log in
2.Sign up
3.Exit
1
Username : mineLab
Password : mine
Login succeed!

1.Log in
2.Sign up
3.Exit
2
Username : mineLab
Password : mine
Detected same username. Return to menu.

1.Log in
2.Sign up
3.Exit
2
Username : kha'zix
Password : op
Sign up succeed!

1.Log in
2.Sign up
3.Exit
3
範例程式執行前 account.txt :
 mineLab mine
Nash10 steve
Zyra flower
範例程式執行後 account.txt :
 mineLab mine
Nash10 steve
Zyra flower
kha'zix op
為練習 class 運用，請按照以下格式：( main

function 不許更動 )
//you must design one or more class to

complete this program

*/

#include <iostream>
#include <string>
#include <cctype>
#include <fstream>
#define max_size 100;

using namespace std;

//unsigned int maxsize=100;
class accountSystem
{

    unsigned int counter, last_pointer;
    string username_a[100];//Name
    string password_a[100];//Password
    bool loged[100];//is logged?
    public:
    void loadDataFromFile(string); //read data from file
    bool logIn(string username,string password);//Login ckeck
    bool signUp(string username,string password);//Signup check
    void SaveDataToFile(string); //Save file
};

void accountSystem::loadDataFromFile(string inputfile)
{
    counter=0;
    last_pointer=0;
    ifstream infile;
    infile.open(inputfile.c_str());
    while (!infile.eof())
    {
          infile>>username_a[counter];
          infile>>password_a[counter];
          loged[counter]=0;
          counter++;
          last_pointer++;//How many data loaded
    }
    infile.close();
}

bool accountSystem:: logIn(string username,string password)
{
    for (counter=0; counter<100;counter++)
    {
        if (username==username_a[counter])//Is Name on file?
         if (password==password_a[counter])//Is passwork on file?
         {
                     loged[counter]=1;//yes,log marked
                     return true;
         }

     }
        return false;
}

bool accountSystem:: signUp(string username,string password)
{
    for (counter=0; counter<100;counter++)
    {
        if ( username==username_a[counter])
        {
            if (password==password_a[counter])
            if (loged[counter]==1); //Is already logged?
            return false;
         }
     }
     username_a[last_pointer+1]=username;//if is a newcommer, record name
     password_a[last_pointer+1]=password;//if is a newcommer, record password
     return true;  //allow sign up
}

void accountSystem::SaveDataToFile(const string outputfile)
{
     ofstream outfile;
     outfile.open(outputfile.c_str(),ios_base::app);// add new commer to end of file
     outfile<<username_a[last_pointer+1]<<" "<<password_a[last_pointer+1]<<endl;
     outfile.close();
 }





int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
