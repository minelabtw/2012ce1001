#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;

class accountSystem
{
public:
    void loadDataFromFile(string);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(string);
private:
    string un,pw;
    string hue[777],ha[777];
    int x=0,y=0;
};

void accountSystem::loadDataFromFile(string LMFAO)
{
    ifstream inAccountFile("account.txt", ios::in);
    if (!inAccountFile)
    {
        cerr << "File Could not be opened" << endl;
        exit(1);
    }
    while(inAccountFile >> un >> pw)//read file
    {
        hue[x] = un;
        ha[y] = pw;
        x++;
        y++;
    }
}

bool accountSystem::logIn(string und,string pwd)
{
    for (int i=0; i<x; i++)
    {
        if (und == hue[i] &&  pwd == ha[i])//check.
        {
            return true;
        }
    }
}

bool accountSystem::signUp(string und,string pwd)
{
    hue[x]=und, ha[y]=pwd;
    for (int j=0; j<x; j++)
    {
        if (und==hue[j])//check.
        {
            return false;
        }
    }
    x++,y++;
    return true;
}

void accountSystem::SaveDataToFile(string LMFAO)
{
    ofstream outAccountFile("account.txt", ios::out);
    if (!outAccountFile)
    {
        cerr << "File Could not be opened" << endl;
        exit( 1 );
    }
    for( int q=0; q<x; q++ )
    {
        outAccountFile << hue[q] << ' ' << ha[q] << endl;//write file
    }
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
