/***********************************************************************************/
/*                                                                                 */
/*  The file does not have to exist initially, the program will create it itself.  */
/*                                                                                 */
/***********************************************************************************/
#include<iostream>
#include<fstream>
#include<string>
#include<cstdlib>
using namespace std;

class accountSystem
{
public:
    void loadDataFromFile(string);//function to create file and load file
    bool logIn(string,string);//function for login
    bool signUp(string,string);//function for signup
    void SaveDataToFile(string);//function to save and close file
private:
    ofstream writeFile;//output filestream
    ifstream readFile;//input filstream
};
void accountSystem::loadDataFromFile(string fileName)
{
    writeFile.open(fileName.c_str(),ios::app);//open the file. if it does not exist, create one
    if(!writeFile)//exit program if unable to create file
    {
        cerr<<"File could not be created"<<endl;
        exit(1);
    }
    readFile.open(fileName.c_str(),ios::in);//load from the file
    if(!readFile)//exit program if ifstream could not open file
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
}
bool accountSystem::logIn(string uName,string pWord)
{
    string inUName="",inPWord="";
    readFile.clear();//clear the eof mark
    readFile.seekg(0);//shift the mark to the start
    while(readFile>>inUName>>inPWord)//load data from the file
    {
        if(uName==inUName&&pWord==inPWord)
        {
            return true;
        }
    }
    return false;
}
bool accountSystem::signUp(string signUName,string signPWord)
{
    string sUInUName="",sUInPWord="";
    readFile.clear();//clear the eof mark
    readFile.seekg(0);//shift the mark to the start
    while(readFile>>sUInUName>>sUInPWord)//load data from the file
    {
        if(signUName==sUInUName)
        {
            return false;
        }
    }
    writeFile<<signUName<<' '<<signPWord<<endl;//write the data into the file
    return true;
}
void accountSystem::SaveDataToFile(string saveFile)
{
    readFile.close();//save and close the file
    writeFile.close();//save and close the file
}
int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
