#include<iostream>
#include<fstream>
#include<cstring>
#include<cstdlib>
using namespace std;
class accountSystem
{
    public:
        void loadDataFromFile(string txt) //將檔案名稱紀錄至root裡 , 並從string轉換成char陣列
        {
            root = new char [txt.size()+1];
            strcpy(root, txt.c_str());
        }
        bool logIn(string at , string pw) //判斷文件中是否有組帳號密碼跟輸入的一樣 , 有的話則表示可以登入
        {
            ifstream ifile(root , ios::in); //讀取文件
            while(ifile >> l_at >> l_pw) //讀取文件內已註冊的帳號和密碼
            {
                if(l_at == at && l_pw == pw)
                    return 1;
            }
            return 0;
        }
        bool signUp(string at , string pw) //判斷文件中是否有帳號名稱跟輸入的重複 , 沒有的話則可以註冊
        {
            string l_at , l_pw;
            ifstream ifile(root , ios::in); //讀取文件
            while(ifile >> l_at >> l_pw) //讀取文件內已註冊的帳號和密碼
            {
                if(l_at == at)
                    return 0;
            }
            ofstream ofile(root , ios::app); //開啟文件
            ofile.seekp(ios::end); //將寫入位置移到最後
            ofile << endl << at << " " << pw; //將輸入的帳號密碼寫入文件中
            return 1;
        }
        void SaveDataToFile(string target) //更新文件
        {
            root = new char [target.size()+1]; //將檔案名稱紀錄至root裡 , 並從string轉換成char陣列
            strcpy(root, target.c_str());
            ofstream ofile(root , ios::app); //開啟文件
            ofile.clear(); //清除eof標記
            ofile.close(); //關閉文件
        }
    private:
        char* root;
        string l_at;
        string l_pw;
};
int main()
{
    string response , username , password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while(cin >> response)
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username , password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username , password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
