#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;

class accountSystem
{
public:
    accountSystem();
    void loadDataFromFile(const char*);
    int logIn(string,string);
    int signUp(string,string);
    void SaveDataToFile(const char*);
private:
    string user , passkey;
    string loginuser , loginpassword;
    int i ,k ;
    string arrayuser[100];
    string arraypasskey[100];
    string Newuser[100];
    string Newpassword[100];
};

accountSystem::accountSystem()
{
    i=k=0;
}

void accountSystem::loadDataFromFile(const char *thefile)
{
    ifstream inaccountfile( thefile, ios::in );
    if(!inaccountfile)//檢查檔案是否成功開啟
    {
        cerr<<"File could not be open" <<endl;
        exit(1);
    }
    while( inaccountfile >> user >> passkey)//讀檔,將原有的帳號密碼丟入陣列
    {
        arrayuser[i] = user;
        arraypasskey[i] = passkey;
        i++;
    }
}

int accountSystem::logIn(string loginuser,string loginpassword)
{
    for(int j=0;j<=i;j++)
    {
        if(loginuser==arrayuser[j] && loginpassword==arraypasskey[j])//判斷帳號密碼是否符合
            return true ;
    }
}

int accountSystem::signUp(string newuser,string newpassword)
{
    bool sameorno = true ;
    for(int j=0;j<i;j++)//判斷帳號是否與原先的重複
    {
        if(newuser==arrayuser[j] )
        {
            sameorno = false ;
            return false ;
        }
    }
    if(k>0)
    {
        for(int j=0;j<k;j++)//判斷帳號是否與新註冊的重複
        {
            if(newuser==Newuser[j])
            {
                sameorno = false ;
                return false ;
            }
        }
    }

    if(sameorno)//如果沒有重複,將新帳號密碼丟進陣列儲存
    {
        Newuser[k] = newuser;
        Newpassword[k] = newpassword;
        k++;
        return true;
    }
}

void accountSystem::SaveDataToFile(const char *thefile)
{
    ofstream updatefile( thefile ,ios::app);
    if(!updatefile)//檢查檔案是否成功開啟
    {
        cerr<<"File could not be opened"<<endl;
        exit(1);
    }
    for(int i=0 ; i<k ;i++)//將新註冊的帳號密碼
    {
        updatefile << Newuser[i] <<' '<<Newpassword[i]<<endl;
    }
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
