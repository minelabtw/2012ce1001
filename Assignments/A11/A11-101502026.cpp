#include <iostream> //include the header file "iostream"
#include <string> //include the header file "string"
#include <fstream> //include the header file "fstream"
#include <cstdlib> //include the header file "cstdlib"

using namespace std;

class accountSystem
{
public:
    void loadDataFromFile(string);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(string);
private:
    string txtusername;
    string txtpassword;
    string usernamestore[10000]; //declare "usernamestore" string to store the usernames
    string passwordstore[10000]; //declare "passwordstore" string to store the passwords
    int i; //declare "i" integer to help counting
};

void accountSystem::loadDataFromFile(string str) //set this function to open a file
{
    ifstream inClientFile("account.txt",ios::in);
    if(!inClientFile)
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }
}

bool accountSystem::logIn(string username,string password) //set this function to check whether the usernames and passwords entered exist or not
{
    ifstream inClientFile("account.txt",ios::in);
    while(inClientFile >> txtusername >> txtpassword)
    {
        if(username==txtusername)
            return true;
    }
    return false;
}

bool accountSystem::signUp(string username,string password) //set this function to check whether the usernames and passwords are the same with those in the file or not
{
    ofstream outClientFile("account.txt",ios::app);
    if(!outClientFile)
    {
        cerr << "File could not be settled." << endl;
        exit(1);
    }
    ifstream inClientFile("account.txt",ios::in);
    while(inClientFile >> txtusername >> txtpassword)
    {
        if(username==txtusername)
            return false;
    }
    i=0;
    while(usernamestore[i]!="")
    {
        i++;
    }
    usernamestore[i]=username;
    passwordstore[i]=password;
    return true;
}

void accountSystem::SaveDataToFile(string str) //set this function to append all output to the end of the file
{
    ofstream outClientFile("account.txt",ios::app);
    i=0;
    while(usernamestore[i]!="")
    {
        i++;
    }
    for(int x=0;x<i;x++)
    {
        outClientFile << usernamestore[x] << " " << passwordstore[x] << endl;
    }
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
