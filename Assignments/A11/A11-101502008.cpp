#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <vector>
using namespace std;
class accountSystem
{public:
    accountSystem();
    void loadDataFromFile(string );
    bool logIn(string ,string );
    bool signUp(string ,string );
    void SaveDataToFile(string );
private:
    string address,name,code;
    vector<string>namevector;
    vector<string>codevector ;
};
accountSystem::accountSystem()
{
}
void accountSystem::loadDataFromFile(string address)
{
    ifstream inclientfile(address.c_str(),ios::in);//具別人情報，不寫c_str會讓程式死掉
    while(inclientfile>>name>>code)//當讀取文件時
    {
        namevector.push_back(name);
        codevector.push_back(code);
    }
}
bool accountSystem::logIn(string name,string code)
{
    for(int count=0;count<(namevector.size());count++)
    {
        if(name==namevector[count] &&code==codevector[count])
        {
            return true;
        }
    }
    return false;//沒有核對的就false
}
bool accountSystem::signUp(string name,string code)
{
    for(int count=0;count<(namevector.size());count++)
    {
            if(namevector[count]==name)
        {
            return false;
        }
    }
    namevector.push_back(name);
    codevector.push_back(code);
    return true;//有重複的就false
}
void accountSystem::SaveDataToFile(string address)//將帳戶與密碼寫入account.txt
{
    ofstream outclientfile(address.c_str(),ios::out);
    for(int count=0;count<(namevector.size());count++)
    {
        outclientfile<<namevector[count]<<' '<<codevector[count]<<endl;
    }
}
int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
