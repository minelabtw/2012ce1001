#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>

using namespace std;

class accountSystem
{
 public:
     void loadDataFromFile(string);
     bool logIn(string,string);
     bool signUp(string,string);
     void SaveDataToFile(string);

 private:
     string _username;
     string _password;
     string fname;
};

void accountSystem::loadDataFromFile(string filename)
{
    ifstream inaccountFile("account.txt", ios::in); //檔案可讀
    fname = filename;
}

bool accountSystem::logIn(string username,string password)
{
    ifstream inaccountFile("account.txt", ios::in); //檔案可讀

    while(inaccountFile >> _username >> _password)
    {
        if(username == _username && password == _password) //若帳密皆與資料相同則成功!
        {
            inaccountFile.clear();//清除想比對的資料
            inaccountFile.seekg(0);//將標籤移到最前面
            return true;
        }
    }

    inaccountFile.clear();  //清除想比對的資料
    inaccountFile.seekg(0); //將標籤移到最前面
    return false;
}

bool accountSystem::signUp(string username,string password)
{
    ifstream inaccountFile("account.txt", ios::in); //檔案可讀
    ofstream outaccountFile("account.txt", ios::app); //檔案可寫

    while(inaccountFile >> _username >> _password)
    {
        if(username == _username)//若輸入的帳號與資料重複則失敗!
        {
            inaccountFile.clear();//清除想比對的資料
            inaccountFile.seekg(0);//將標籤移到最前面
            return false;
        }
    }

    outaccountFile << "\n" << username << ' ' << password;

    inaccountFile.clear();//清除想比對的資料
    inaccountFile.seekg(0);//將標籤移到最前面
    return true;
}

void accountSystem::SaveDataToFile(string filename)
{
    fname=filename;
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
