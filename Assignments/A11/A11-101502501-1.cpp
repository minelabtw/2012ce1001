#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
#include <cstdlib>
using namespace std;

class accountSystem
{
public:
    void loadDataFromFile(const char * )//檢查有無錯誤
    {
        outAccountFile.open("account.txt" , ios::app );
        ifstream inAccountFile("account.txt" , ios::in );

        if(!outAccountFile)
        {
            cerr << "File could not be opened"<<endl;
            exit(1);
        }
        if(!inAccountFile)
        {
            cerr << "File could not be opened"<<endl;
            exit(1);
        }
    }
    bool login( string user1, string pass1 )//登入
    {
        username1 = user1;
        password1 = pass1;
        bool sameOrNot = false;
        ifstream inAccountFile("account.txt" , ios::in );

        while(inAccountFile>>username2>>password2)//讀檔
        {
            if(username1==username2&&password1==password2)
                sameOrNot=true;//帳密相同為true
        }
        inAccountFile.clear();//重置
        inAccountFile.seekg(0);//重新讀取
        if(sameOrNot==true)
            return true;
        else
            return false;
    }
    bool signUp( string user2, string pass2 )//註冊
    {
        username1 = user2;
        password1 = pass2;
        bool sameOrNot = true;//定sameOrNot為true
        ifstream inAccountFile( "account.txt", ios::in );//讀檔

        while ( inAccountFile >> username2 >> password2 )//讀入資料
        {
            if(username1==username2)
                sameOrNot=false;//帳號相同為false
        }
        inAccountFile.clear();//重置
        inAccountFile.seekg(0);//重新讀取

        if(sameOrNot==true)//順利註冊
        {
            outAccountFile << username1 << ' ' << password1 << endl;//寫入檔案
            return true;
        }
        else
            return false;
    }
    void SaveDataToFile( const char *)
    {
        outAccountFile.open("account.txt", ios::app);//從下一個檔案繼續寫入
    }
private:
    string username1, password1;//輸入用
    string username2, password2;//讀檔用
    ofstream outAccountFile;
};

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt

    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;

            if(AccountSystem.login(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
            cout << "Sign up succeed!" << endl;
            else
            cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
