#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <iomanip>
using namespace std;

class accountSystem
{
public:
    accountSystem();
    void loadDataFromFile(const char *);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile( const char *);

private:
    string username1,password1,username2,password2;//1是輸入用,2是讀檔用
    ofstream outdatafromfile;
};
accountSystem::accountSystem()
{
    username1[0]='\0';
}
void accountSystem::loadDataFromFile(const char *)
{
    outdatafromfile.open("account.txt", ios::app );
    ifstream indatafromfile("account.txt" , ios::in );
    //錯誤偵測
    if(!outdatafromfile)
    {
        cerr << "Can't open file!"<<endl;
        exit(1);
    }
    if(!indatafromfile)
    {
        cerr << "Can't open file!"<<endl;
        exit(1);
    }
}
bool accountSystem::signUp(string a1,string b1)
{
    username1=a1;
    password1=b1;
    bool check1=true;//判斷帳號是否相同
    ifstream indatafromfile("account.txt" , ios::in );

    while(indatafromfile>>username2>>password2)//讀檔
    {
        if(username1==username2)
            check1=false;//帳號相同為false
    }
    indatafromfile.clear();
    indatafromfile.seekg(0);
    if(check1==true)//if true ,signup complete
    {
        outdatafromfile<<username1<<' '<<password1<<endl;//寫入檔案
        return true;
    }
    else
        return false;
}
bool accountSystem::logIn(string a,string b)
{
    username1=a;
    password1=b;

    ifstream indatafromfile("account.txt" , ios::in );

    bool check=false;//判斷帳密是否相同
    while(indatafromfile>>username2>>password2)//讀檔
    {
        if(username1==username2&&password1==password2)
            check=true;//帳密相同為true
    }
    indatafromfile.clear();//重置
    indatafromfile.seekg(0);//重新讀取
    if(check==true)
        return true;
    else
        return false;
}

void accountSystem::SaveDataToFile( const char *)
{
    outdatafromfile.open("account.txt", ios::app);//從下一個檔案繼續寫入
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
