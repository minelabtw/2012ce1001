#include<iostream>
#include<string>
#include<fstream>
using namespace std;
class accountSystem
{
private:
    string newdata;
    string data;
public:
    void loadDataFromFile( const char* filename)//把原本的資料存入data中
    {
        ifstream filein(filename, ios::in);//打開可讀出檔

        string bufferusername,bufferpassword;
        while (!filein.eof()) //讀出file裡之資料，輸至資料成員data中
        {
            filein >> bufferusername;
            filein >> bufferpassword;
            data+=(" "+bufferusername+" "+bufferpassword+" ,");//" " 可避免1234 1234與1234 123被判斷一樣
        }
        filein.close();
    }

    bool logIn(string username, string password)
    {
        if (data.find(" "+username+" "+password+" ,") != -1)//判斷帳密是否正確
        {                                 //" " 可避免1234 1234與1234 123被判斷一樣
            return true;
        }
        else
            return false;
    }

    bool signUp(string username, string password)
    {
        if (data.find(", "+username+" ") != -1)//帳號不可一樣，密碼可以一樣。
        {           //" "可避免1234 333與234 333被判斷一樣
            return false;
        }
        else
        {
            newdata+=(" \n"+username+" "+password);//紀錄新的帳號與密碼
            data+=(" "+username+" "+password+" ,");//輸至資料成員data中
            //cout << data <<endl;//check it.
            return true;
        }
    }

    void SaveDataToFile(const char* filename)
    {
        ofstream fileout(filename, ios::out|ios::app);//打開可寫入檔
        fileout << newdata;//將signIn的資料輸入檔案
        fileout.close();
    }
};

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))// logIn-> return 1 or 0 //this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password)) // signUp->return 1 or 0 //this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
