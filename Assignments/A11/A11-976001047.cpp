#include<iostream>
#include<string>
#include<fstream>
using namespace std;

class accountSystem                    //create a class named account.
{
public:
   accountSystem();
   void loadDataFromFile(char []);
   bool logIn(string,string);
   bool signUp(string,string);
   void SaveDataToFile(char []);
private:
   fstream account;
   fstream temp;
   string username;
   string password;
   string data;
   string compare;
   string a;
};

accountSystem::accountSystem()
{
    username="0";
    password="0";
    a="0";
}

void accountSystem::loadDataFromFile(char fname[]) //to open the file called in main function.
{                                                  //create a file called temp.txt to save the data from
    account.open(fname,ios::in);                   //the file called in main function.
    temp.open("temp.txt",ios::out);
    int i=1;
    a="0";
    while (!account.eof())
    {
        account>>data;
        if (a==data)
        {
            break;
        }
        temp<<data<<" ";
        if (i%2==0)
        {
            temp<<"\n";
        }
        i++;
        a=data;
    }
    account.clear();
    account.close();
    temp.clear();
    temp.close();
}

bool accountSystem::logIn(string username,string password) //the fuction is working for login.
{
    a=username+" "+password;
    temp.open("temp.txt",ios::in);              //open temp in reading state.
    while (!temp.eof())                         //the loop is compare the input of username and passwrod with the
    {                                           //data in temp.txt.
        temp>>compare;
        if (username==compare)
        {
            temp>>compare;
            if (password==compare)              //if the input of username and passwrod is same as the data in temp.txt
            {                                   //retrun true to main.
                temp.clear();
                temp.close();
                return true;
            }
        }
    }
    temp.clear();                              //if the input of username and passwrod is not same as the data in temp.txt
    temp.close();                              //retrun false to main.
    return false;
}

bool accountSystem::signUp(string username,string password)   //sinUp is working for created a account.
{
    temp.open("temp.txt",ios::in);                            //open temp.txt in reading state.
    a=username+" "+password;
    int i=1;
    while (!temp.eof())                      //the loop is compare the input of username with the
    {                                        //data in temp.txt.
        temp>>compare;
        if(username==compare&&i%2==1)
        {
            temp.clear();                  //if the input of username is same as the data in temp.txt
            temp.close();                  //return false to main.
            return false;
        }
    }
    temp.clear();
    temp.close();
    temp.open("temp.txt",ios::out|ios::app);  //if the input of username is not same as the data in temp.txt.
    temp<<a;                                  //write the input of username and password in temp.txt.
    temp.clear();
    temp.close();
    return true;
}

void accountSystem::SaveDataToFile(char fname[]) //the function is working for update data from temp.txt
{                                                //to the file called in main.
    account.open(fname,ios::out);
    temp.open("temp.txt",ios::in);
    int i=1;
    a="0";
    while (!temp.eof())
    {
        temp>>data;
        if (a==data)
        {
            break;
        }
        account<<data<<" ";
        if (i%2==0)
        {
            account<<"\n";
        }
        i++;
        a=data;
    }
    account.clear();
    account.close();
    temp.clear();
    temp.close();
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
