#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;

static int i=0;
class accountSystem
{
public:
    void loadDataFromFile(string);
    bool logIn(string,string);
    bool signUp(string,string);
    void SaveDataToFile(string);
private:
    string Save_name[10000];
    string Save_code[10000];

};

//先檢查檔案
void accountSystem::loadDataFromFile(string str)
{
    //尋找一個被讀取的檔案
    ifstream inClientFile(str.c_str(), ios::in);

    //如果檔案不存在就結束
    if(!inClientFile)
    {
        cerr << "File could not be opened" << endl;
        exit(1);
    }
}

//登入系統
bool accountSystem::logIn(string U_name,string P_word)
{
    //尋找一個被讀取的檔案
    ifstream inClientFile("account.txt", ios::in);

    string name;
    string code;

    bool X=true;

    while (inClientFile >> name >> code )
    {
        //檢查帳號密碼是否與檔案裡已註冊的資料一樣
        if (U_name==name&&P_word==code)
        {
            X=true;
            break;
        }
        else
            X=false;
    }
    //每次迴圈檢查完檔案內所有資料後要重置，等於可以重新檢查
    inClientFile.clear();
    //將游標指向開頭的記憶體
    inClientFile.seekg(0);

    if(X)
        return true;
    else
        return false;

}

//註冊系統
bool accountSystem::signUp(string User_name,string User_code)
{
    ifstream inClientFile("account.txt", ios::in);

    string name;
    string code;

    bool X=true;

    while (inClientFile >> name >> code)
    {

        if (User_name==name)
        {
            X=false;
            break;
        }
        else
            X=true;
    }
    //每次迴圈檢查完檔案內所有資料後要重置，等於可以重新檢查
    inClientFile.clear();
    //將游標指向開頭的記憶體
    inClientFile.seekg(0);

    if (X)
    {
        bool Y=true;

        //如果一直沒有輸入3儲存檔案的話，則需要這個for迴圈檢查剛剛已經註冊過的帳號不可以再註冊
        for (int j=0;j<i;j++)
        {
            if(User_name==Save_name[j])
            {
                Y=false;
                break;
            }
            else
                Y=true;
        }

        if(Y)
        {
            Save_name[i]=User_name;
            Save_code[i]=User_code;
            i++;
            return true;
        }
        else
            return false;
    }
    else
        return false;

}

//儲存註冊過的資料
void accountSystem::SaveDataToFile(string str)
{
    ofstream outClientFile(str.c_str(), ios::app);

    i--;;
    while(i>=0)
    {
        outClientFile << endl << Save_name[i] << ' ' << Save_code[i];
        i--;
    }
}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt

    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;

            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;

            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }

        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
