#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;

class accountSystem
{
public:
    accountSystem();
    void loadDataFromFile(string);
    int logIn(string,string);
    int signUp(string,string);
    void SaveDataToFile(string);
private:
    string response,username,password,a[100],p[100];
    bool real;
    int j,i ;
};

accountSystem::accountSystem()                              //此為先設初始值
{
    response = ' ',username = ' ',password = ' ';
    real = false;
    i,j = 0;
}

void accountSystem::loadDataFromFile(string account)       //先將文件檔的讀入陣列中，且設一個j值知道匯入到陣列第幾項
{
    ifstream Account("account.txt",ios::in);
    Account.seekg(0,ios::beg);
    for ( int i = 0 ; !Account.eof() ; i++)
    {
        Account >> a[i] >> p[i] ;
        j = i;
    }
}

int accountSystem::logIn(string username ,string password) //登入先找尋有沒有相同的帳號，再判斷密碼
{
    for ( int i = 0 ; i < 100 ; i++)
    {
        if ( username == a[i])
        {
            if ( password == p[i])
                return true ;
        }
    }
    return false;
}

int accountSystem::signUp(string username,string password) //註冊若有相同帳號則錯誤，若無則將這次輸入的放到陣列裡且j+1且正確
{
    real = false;
    for (int i = 0 ; i < 100 ; i++)
    {
        if ( username == a[i])
            real = true;
    }
    if (real == true)
        return false;
    else
    {
        a[j] = username ;
        p[j] = password ;
        j += 1;
        return true;
    }
}

void accountSystem::SaveDataToFile(string)  //將這次陣列中的所有東西存到文件檔中
{
    ofstream Account("account.txt",ios::out);
    for ( int j = 0 ; j < 100 ;j ++)
        Account << a[j] << " "<< p[j] << endl;
}
int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
