#include<iostream>
#include <fstream>
#include <cstdlib>
#include<string>
using namespace std;

class accountSystem
{
public:
    accountSystem();//建構子
    bool logIn(string,string);
    bool signUp(string,string);
    void loadDataFromFile(string);
    void SaveDataToFile(string);
    ofstream outaccountFile;//存資料
    ifstream inaccountFile;//讀資料
private:
    string n;
    string c,d,e,f;//宣告變數
};

accountSystem::accountSystem()
{

}

void accountSystem::loadDataFromFile(string a)
{
    outaccountFile.open("account.txt",ios::app);//讀檔
    inaccountFile.open("account.txt",ios::in);
}

void accountSystem::SaveDataToFile(string b)
{
    inaccountFile.open("account.txt",ios::in);
}

bool accountSystem::logIn(string n1,string n2)
{
    inaccountFile.clear();//清除
    inaccountFile.seekg(0);//重頭讀
    int i=0;
    while(inaccountFile>>c>>d)//讀輸入的資料
    {
        if(n1==c&&n2==d)//帳密相同
        {
            i=1;
            break;
        }
    }

    if(i==1)
    {
        return true;
    }
    return false;
}

bool accountSystem::signUp(string n3,string n4)
{
    inaccountFile.clear();//清除
    inaccountFile.seekg(0);//重頭讀
    int i=0;
    while(inaccountFile>>c>>d)//讀輸入的資料
    {
        if(n3==c)//重複
        {
            i=1;
            break;
        }
    }

    if(i==0)//沒重複
    {
        outaccountFile<<n3<<' '<<n4<<endl;
        return true;
    }
    return false;

}

int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}
