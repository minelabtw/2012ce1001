#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
using namespace std;
class accountSystem//
{
public:

    void loadDataFromFile(const char filename[])
    {
        inClientFile.open( filename,ios::in);//讀檔案
        outClientFile.open(filename,ios::app);//加在原本的東西後面
        if(!inClientFile)//開讀檔時出錯，輸出錯誤訊息
        {
            cerr<<"File could not be opened"<<endl;
            exit(1);
        }
    }
    bool logIn(string a,string b)//讀檔案
    {
        string u,p;//讀到原有的帳密
        inClientFile.clear();
        inClientFile.seekg(0);//每次都從頭開始讀
        while(inClientFile>>u>>p)//讀檔檢查的迴圈
        {
            if(a==u && b==p)//若帳密吻合，傳回true且結束函式
                return true;
        }
        return false;//
    }
    bool signUp(string m,string n)//登入
    {
        string c,d;//c,d為原有的帳密
        inClientFile.clear();
        inClientFile.seekg(0);//每次都從頭開始讀
        while(inClientFile>>c>>d)//讀檔檢查的迴圈
        {
            if(m==c)//若有帳號相同則傳回false，且函式結束
                return false;
        }
        outClientFile<<m<<' '<<n<<endl;//account檔印出帳號及密碼
        return true;
    }
    void SaveDataToFile(string name)//關閉且儲存檔案
    {
        outClientFile.close();
        inClientFile.close();
    }
private:
    ifstream inClientFile;
    ofstream outClientFile;
};//class結束
int main()
{
    string response,username,password;
    accountSystem AccountSystem;
    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt
    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    while( cin>>response )
    {
        if(response.compare("1") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"
                cout << "Login succeed!" << endl;
            else
                cout << "Login failed!" << endl;
        }
        else if(response.compare("2") == 0)
        {
            cout << "Username : ";
            cin >> username;
            cout << "Password : ";
            cin >> password;
            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"
                cout << "Sign up succeed!" << endl;
            else
                cout << "Detected same username. Return to menu." << endl;
        }
        else if(response.compare("3") == 0)
        {
            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt
            break;
        }
        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;
    }
    return 0;
}

