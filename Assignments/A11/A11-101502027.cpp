#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;
class accountSystem
{
public:
    void loadDataFromFile(string);
    int logIn(string ,string);
    int signUp(string,string);
    void SaveDataToFile(string);
private:
    string a;
    string acc1;
    string pass1;
    string acc[10000];
    string pass[10000];
    string finalacc[10000];
    string finalpass[10000];
    bool q;
    int y;

};
void accountSystem::loadDataFromFile(string a)
{
    ifstream in("account.txt");
    if (!in)
        exit(1);
}
int accountSystem::signUp(string acc1,string pass1)
{
    static int i=0;
    ifstream in("account.txt",ios::in);
    acc[i]=acc1;
    pass[i]=pass1;
    bool q=true;
    string acc2,pass2;
    while(in >> acc2 >> pass2)

    {
        if (acc2==acc1)
        {
            q=false;
            break;
        }
    }//判斷之前就在檔案裡的帳號
    for (int z=0 ;z<i;z++)
    {
        if (acc[z]==acc[i])
            q=false;
        if (q==0)
            break;
    }//存新帳到新字串列中
    if (q==true)
    {
        finalacc[i]=acc[i];
        finalpass[i]=pass[i];
        i++;
        y=i;
    }
    in.seekg(0);//指標回歸1
    return q;
}
int accountSystem::logIn(string acc1,string pass1)
{

    string acc2,pass2;
    ifstream in("account.txt",ios::in);
    bool a=false;
    while(in >> acc2 >> pass2)
    {
        if (acc2==acc1&&pass2==pass1)
        {
            a=true;
            break;
        }//判斷是否可登入
    }
    in.seekg(0);//指標回歸1
    return a;

}
void accountSystem::SaveDataToFile(string)
{
    ofstream out("account.txt",ios::app);
    for (int x=0;x<y;x++)
        out<<finalacc[x]<<' '<<finalpass[x]<<endl;
    //儲存最終的字串列進入檔案中

}

int main()

{

    string response,username,password;

    accountSystem AccountSystem;

    AccountSystem.loadDataFromFile("account.txt");//load the account data from account.txt

    cout << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

    while( cin>>response )

    {

        if(response.compare("1") == 0)

        {

            cout << "Username : ";

            cin >> username;

            cout << "Password : ";

            cin >> password;

            if(AccountSystem.logIn(username,password))//this is same as "if(AccountSystem.logIn(username,password)==ture)"

                cout << "Login succeed!" << endl;

            else

                cout << "Login failed!" << endl;

        }

        else if(response.compare("2") == 0)

        {

            cout << "Username : ";

            cin >> username;

            cout << "Password : ";

            cin >> password;

            if(AccountSystem.signUp(username,password))//this is same as "if(AccountSystem.signUp(username,password)==true)"

                cout << "Sign up succeed!" << endl;

            else

                cout << "Detected same username. Return to menu." << endl;

        }

        else if(response.compare("3") == 0)

        {

            AccountSystem.SaveDataToFile("account.txt");//it will update account.txt

            break;

        }

        cout << endl << "1.Log in" << endl << "2.Sign up" << endl << "3.Exit" << endl;

    }

    return 0;

}

