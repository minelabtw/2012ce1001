//作業 5-1
//班級:資工系四年B班
//學號:965002086
//姓名:張凱閔
/*請寫出程式，使電腦與玩家能隨機骰出骰子點數，並判斷玩家是否獲勝。
此遊戲必須能重複遊玩，當玩家輸入1則繼續遊戲，輸入其他數字或字元結束程式。*/

#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;
void dice();
//2定義骰子點數任意產生及比較涵式
void dice()
{
 int a_computer,a_player;

 a_computer=(rand()%6)+1;//任意產生介於1~6間整數
 a_player=(rand()%6)+1;
 cout<<"\nComputer's dice point is:"<<a_computer<<endl;
 cout<<"Your dice point is::"<<a_player<<endl;
 //三種情況擇一
 if (a_computer>a_player)
	 cout<<"Sorry, you lose."<<endl;

 else if (a_player>a_computer)
	 cout<<"Congratulation! You win."<<endl;

 else
	 cout<<"Draw"<<endl;
}


int main()

{

 char select;
 srand(time(NULL));//任意函數種子
 select='1';
while (select='1')
{
		 dice();
		 cout<<"Want to play again? ( 1 for continue, 0 for exit )";
		 cin>>select;
		if (select!='1') break;
 };
	cout<<"\nProcess returned 0\n";
//系統暫停以便觀察結果
	system("pause");
}

