#include <iostream>
#include<cmath>
using namespace std;

int main()
{
    while (1) // 重複輸入
    {
        int n;
        cin >> n;
        if ( !cin || n < 1 ) // 偵錯無效範圍或非數字輸入
            return 0;

        if ( n % 2 == 1 ) // 當輸入奇數時,輸出一個對應的菱形
        {
            for ( int i = 1 ; i <= (n/2)+1 ; i++ ) // i是階層數,從1到n/2+1層(上半個菱形包含最中間那一行的"*")
            {
                for ( int j = 1 ; j <= (n/2)-i+1 ; j++ ) // 輸出每一層所需的空白
                    cout << " ";
                for ( int j = 1 ; j <= i*2-1 ; j++ ) // 輸出每一層所需的*
                    cout << "*";

                cout << endl;
            }
            for ( int i = n-(n/2+1) ; i >= 1 ; i-- ) // 下半個菱形
            {
                for ( int j = 1 ; j <= n/2-i+1 ; j++ ) // 輸出每一層所需的空白
                    cout << " ";
                for ( int j = i*2-1 ; j >= 1 ; j-- ) // 輸出每一層所需的*
                    cout << "*";

                cout << endl;
            }
        }

        if ( n % 2 == 0 ) // 當輸入偶數時,輸出一個對應的矩形
        {
            for ( int i = 1 ; i <= n ; i++ ) // 輸出矩形頂部的"*"
                cout << "*";
            cout << endl;
            for ( int i = 1 ; i <= n-2 ; i++ )
            {
                cout << "*"; // 輸出中間層左側的"*"
                for ( int i = 1 ; i <= n-2 ; i++ ) // 輸出矩形中間所需的空白
                    cout << " ";

                cout << "*"; // 輸出中間層右側的"*"
                cout << endl;
            }

            for ( int i = 1 ; i <= n ; i++ ) // 輸出矩形底部的"*"
                cout << "*";
            cout << endl;
        }
    }
    return 0;
}
