#include <iostream>

using namespace std;

int main()
{
    int n;
    while(cin >>n)
    {
        if(n<=0)//錯誤偵測
            return 0;
        if(n%2==0)
        {
            for (int m=0;m<n;m++)//可以做出第一排
            {
                cout << "*";
            }
            for (int m=0;m<n-2;m++)//可以做出左右2排
            {
                cout << "\n*";
                for(int o=0; o<n-2; o++)
                {
                    cout << " ";
                }
                cout << "*";
            }
            cout << "\n";
            for (int m=0;m<n;m++)//可以做出最後一排
            {
                cout << "*";
            }
        }
        cout <<"\n";
        if(n%2==1)
        {
            for(int a=1;a<=n/2+1;a++)//可以做出上3角形
            {
                for(int b=1;b<=n-a;b++ )
                    cout << " ";
                for(int b=1;b<=a*2-1;b++ )
                    cout << "*";
                cout << endl;
            }
            for(int a=1;a<=n/2;a++)//可以做出下3角形
            {
                for(int b=1;b<=n/2+a;b++)
                    cout << " ";
                for(int b=1;b<=n-a*2;b++)
                    cout << "*";
                cout <<endl;
            }
        }
    }
     return 0;
}
