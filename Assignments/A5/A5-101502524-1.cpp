#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    while(1)//一個無限迴圈
    {
        int k;
        srand((unsigned)time(NULL));//設定兩個隨機的數字
        int a = rand();
        int b = rand();
        cout << "Computer's dice point is : " << a%6+1 <<endl;
        cout<<"Your dice point is : "<<b%6+1<<endl;
        if(a%6+1 > b%6+1)//你輸了
        {
            cout<<"Sorry , you lose . "<<endl;
        }
        else//你贏了
        {
            cout<<"Congratulation ! You win ."<<endl;
        }

        cout<<"Want to play again? ( 1 for continue, 0 for exit )";

        //輸入
        cin>>k;
        if(k!=1)
        {
            break;
        }
    }
    return 0;
}
