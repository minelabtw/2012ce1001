#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <time.h>

using std::cout;
using std::cin;
using std::endl;

int main()

{
    srand((unsigned)time(NULL));
    int c=1;
    int d ;
    
    while (c!=0)
    {
        if (cin==false)         // break out this loop for input is not number
        {
           break;           
        }  
        
        int a = rand(); 
        int b = rand();
        a = a%6+1;
        b = b%6+1;             //make two random number a,b in 1~6          
     
        cout << "Computer's dice point is: " << a;
        cout << "\nYour dice point is: "<< b;         //output the two random number
        
        if (a>b)                                      //compare a and b , and according to the three stament:a>B,a=b,a<b
        {                                             //to output message
            cout<<"\nSorry, you lose. ";
        }
        if (a==b)
        {
            cout <<"\nDraw";        
        }   
        if (a<b)
        {
            cout << "\nCongratulation! You win.";
        }
        
        cout << "\nWant to play again? ( 1 for continue, 0 for exit ) "  ;    //continue playing message 
        cin >> c;
        
    }           

    system("PAUSE");
    return 0;
}
