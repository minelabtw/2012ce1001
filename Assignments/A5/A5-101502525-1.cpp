#include<iostream>
#include<stdlib.h>
#include<limits>
#include<time.h>
using namespace std;


bool replay(int in);


int main()
{
    //initial
    srand(time(NULL));
    int comdice,playdice;
    int in=0;

    do
    {
        //initial
        comdice=rand()%6+1;
        playdice=rand()%6+1;


        //output
        cout<<"\nComputer's dice point is: "<<comdice;
        cout<<"\nYour dice point is: "<<playdice<<endl;


        //game
        if(comdice>playdice)
            cout<<"Sorry, you lose.";
        else if(comdice<playdice)
            cout<<"Congratulation! You win.";
        else
            cout<<"Draw";


        //replay
        cout<<"\nWant to play again? ( 1 for continue, 0 for exit ) ";
        cin>>in;
    }while(replay(in));


    //end
    cout<<endl;
    system("pause");
    return 0;
}


bool replay(int in)
{
    cin.clear();//reset cin
    cin.ignore(numeric_limits<streamsize>::max(),'\n');//clear cin buffer

    if(cin.gcount()!= 1)//not integer, exit
        return false;
    else if(in!=1)//not 1, exit
        return false;
    //1, replay
    return true;
}
