#include<iostream>
#include<cmath>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int number;
    while(1)
    {
        cin >> number;
        if( cin == false || number <= 0 )//輸入錯誤，number為零或負數則結束程式
        break;
        if( number % 2 == 1)//若number為奇數
        {
            for( int i=1 ; i <= ( number + 1 ) / 2 ; i++)//做菱形的上半部
            {
                for( int j = 1 ; j < ( number + 1 ) / 2 - ( i - 1 ) ; j++ )
                    cout << " ";
                for( int j = 1 ; j <= i * 2 - 1 ; j++)
                    cout << "*";
                cout << endl;
            }
            for( int i = 1 ; i < number - i; i++ )//做菱形的下半部
            {
                for( int j = 1 ; j <= i; j++ )
                    cout << " ";
                for( int j = 1 ; j <= number - ( i * 2 ) ; j++ )
                    cout << "*";
                cout << endl;
            }
        }
        if( number % 2 == 0)//若number為偶數
        {
            for( int i=1 ; i<=number ; i++)//做空心矩型的第一排
                cout << "*";
            cout << endl;
            for( int i=1 ; i<=number-2;i++)//做中心矩形的的中間部分
            {
                cout << "*";
                for( int i=1 ;i <= number-2 ;i++)
                    cout << " ";
                cout << "*";
                cout << endl;
            }
            for( int i=1 ; i<=number ; i++)//做空心矩形的最後一排
                cout << "*";
                cout << endl;
        }
    }
    return 0;
}
