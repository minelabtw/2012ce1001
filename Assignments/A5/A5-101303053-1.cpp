#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int main()
{
    int a,b,c = 1;

    while ( c == 1 )
    {
        srand((unsigned)time(NULL));

        a = ( rand()%6 + 1 );
        b = ( rand()%6 + 1 );
        cout << "Computer's dice point is: " << a << endl;
        cout << "Your dice point is: " << b <<endl;
        if ( a > b )
            cout << "Sorry, you lose." << endl;
        else if ( a < b )
            cout << "Congratulation! You win." << endl;
        else
            cout << "Draw" << endl;
        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
        cin >> c;
        if ( c == 0 )
            break;
    }
}
