#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int main()
{
    srand((unsigned)time(NULL));//製作亂數表
    int input;//宣告變數
    do//在輸入前先執行一次
    {
        int a=rand()%6+1;                           //電腦的點數需介於1-6間
        cout<<"Computer's dice point is: "<<a<<endl;//輸出電腦點數
        int b=rand()%6+1;                       //玩家的點數需介於1-6間
        cout<<"Your dice point is: "<<b<<endl;//輸出玩家點數
        if(b>a)                                     //若玩家點數較大
            cout<<"Congratulation! You win."<<endl;//則顯示勝利
        else if(a>b)                        //若玩家點數小
            cout<<"Sorry, you lose."<<endl;//則顯示輸
        else                    //若點數相同
            cout<<"Draw."<<endl;//則顯示平手
        cout<<"Want to play again? ( 1 for continue, 0 for exit ) ";//詢問是否繼續
    }while(cin>>input&&input==1);//若輸入非1的字元則跳出
    return 0;//程式結束
}
