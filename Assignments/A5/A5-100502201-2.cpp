#include<iostream> //Load cout/cin function in this program
using namespace std; //Shorten "cout/cin" code length in this program

int main()
{
    while(1) //Repetitive input
    {
        int n;
        cin>>n;
        if (n<=0) //Error detection
        break;
        if(n%2==1) //When input integer is odd, apply to this algorithm to print stars
        {
            for (int i=1;i<=n/2+1;i++)
            {
                for (int j=1;j<=n/2+1-i;j++)
                cout<<" ";
                for (int j=1;j<=i*2-1;j++)
                cout<<"*";
                cout<<endl;
            }
            for (int i=1;i<=n/2;i++)
            {
                for (int j=1;j<=i;j++)
                cout<<" ";
                for (int j=1;j<=n-2*i;j++)
                cout<<"*";
                cout<<endl;
            }
        }
        else //When input integer is even, apply to this algorithm to print stars
        {
            for (int i=1;i<=n;i++)
            cout<<"*";
            cout<<endl;
            for (int i=1;i<=n-2;i++)
            {
                cout<<"*";
                for (int j=1;j<=n-2;j++)
                cout<<" ";
                cout<<"*"<<endl;
            }
            for (int i=1;i<=n;i++)
            cout<<"*";
            cout<<endl;
        }
    }
    return 0;
}
