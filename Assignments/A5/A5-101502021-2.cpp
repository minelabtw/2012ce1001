#include <iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    int interger ;
    int line=1 ; //定義行數初始值為1

    if (cin >> interger == false) //如果輸入的不是數字
        return 0 ; //結束程式

    else if (interger < 1)
        return 0 ;

    else if (interger%2!=0) //如果輸入的數字是奇數，輸出一個菱形
    {
        while(line<=interger/2+1) //當行數小於等於interger/2+1，進入while。    如果interger是5，則interger/2+1=4
        {
            for (int i=interger/2 ; i >= line ; i--)
                cout << " " ;

            for (int j=1 ; j <= (2*line-1) ; j++)
                cout << "*" ;

            line++ ;
            cout << endl ;
        }

        line = interger/2  ;

        while (line>=1)
        {
            for (int i=interger/2 ; i >= line ; i--)
                cout << " " ;

            for (int j=1 ; j <= (2*line-1) ; j++)
                cout << "*" ;

            line-- ;
            cout << endl ;
        }
        //因為菱形的上下半部是對稱的，又因為主要變因是line，所以其實只要把line的初始值、跟while裡面line的判斷值交換，就可以以一樣的程式碼輸出顛倒的三角形
    }

    else //如果輸入的是偶數輸出一個矩形
    {
        for (int i=1 ; i <= interger ; i++) //先輸出最上面的邊長
                cout << "*" ;

        cout << endl ; //換行

        for (int i=1 ; i<=interger-2 ; i++) //用來做中間有包含空格的程式
        {
            cout << "*" ;

            for (int j=1 ; j<=interger-2 ; j++)
                cout << " " ;

            cout << "*" << endl ;
        }

        for (int i=1 ; i <= interger ; i++) //最後輸出最下面的邊長
                cout << "*" ;

        cout << endl ;
    }

    return 0 ;

}
