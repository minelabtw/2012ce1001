#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    int diseA , diseB ;// diseA is for user ,diseB is for computer.
    srand((unsigned)time(NULL));

    do
    {
        diseA = 1 + rand() % 6 ;
        diseB = 1 + rand() % 6 ;

        cout << "Computer's dice point is: " << diseB << endl ;
        cout << "Your dice point is: " << diseA << endl ;

        //check that who is the winner.
        if(diseA > diseB)
            cout << "Congratulation! You win." << endl ;
        else if(diseA == diseB)
            cout << "Draw" << endl ;
        else
            cout << "Sorry, you lose." << endl ;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) " ;
        cin >> diseA ;
        cout << endl ;
        if(cin == false)
            return 0 ;
    }while(diseA == 1);
}
