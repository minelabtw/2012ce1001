#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    srand((unsigned)time(NULL)); // 隨機產生數值

    while ( 1 ) //無限迴圈,使其可重複輸入
    {
        int number;
        int a = rand(); //隨機產生電腦所擲的數值

            cout << "Computer's dice point is: " << a % 6 + 1 << endl;

        int b = rand ();//隨機產生自己所擲的數值


            cout << "Your dice point is: " << b % 6 + 1 << endl;

        if ( a % 6 + 1 > b % 6 + 1  ) //若電腦數值較大則輸出失敗訊息
            cout << "Sorry, you lose.";

        if ( a % 6 + 1 == b % 6 + 1  ) //若兩方數值相等則輸出平手訊息
            cout << "Dram";

        if ( a % 6 + 1 < b % 6 + 1   ) //若玩家數值較大則輸出勝利訊息
            cout << "Congratulation! You win.";

        cout << "Want to play again? ( 1 for continue, 0 for exit )";

        cin >> number; // 變數number,若cin等於1則繼續迴圈,不等於1則結束程式
        if ( number != 1 )
            return 0;
    }
    return 0;
}
