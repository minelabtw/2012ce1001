#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int main()
{
    srand((unsigned)time(NULL));
    int a,b;
    while(1)//永續大迴圈
    {
        a=(rand()%6)+1;//玩家骰子點數
        b=(rand()%6)+1;//電腦骰子點數
        cout<<"Computer's dice point is: "<<b<<endl;
        cout<<"Your dice point is: "<<a<<endl;
        if (a<b)//判斷大小
            cout<<"Sorry, you lose."<<endl;
        else
        {
            if(a>b)
                cout<<"Congratulation! You win."<<endl;
            else
                cout<<"Draw"<<endl;
        }
        cout<<"Want to play again? ( 1 for continue, 0 for exit ) ";//確認是否繼續
        if((cin>>a==false)||(a!=1))//跳出條件
            return 0;
        cout<<endl;
    }
}
