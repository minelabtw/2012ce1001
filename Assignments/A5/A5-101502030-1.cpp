#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int main()
{
    srand((unsigned)time(NULL)) ;     //時間亂碼.
    {
        while ( true )
        {
            int a = rand() , b = rand() , c , d , out ;
            c = a % 6 + 1 ;
            d = b % 6 + 1 ;
            cout << "Computer's dice point is : " << c << endl ;
            cout << "Your dice point is: " << d << endl ;
            if ( c > d )                   //輸
            {
                cout << "Sorry, you lose." << endl ;
            }
            if ( d > c )                //獲勝.
            {
                cout << "Congratulation! You win." << endl ;
            }
            if ( c == d )               //平手.
            {
                cout << "draw." << endl ;
            }
            cout << "Want to play again? ( 1 for continue, 0 for exit ) " ; //離開條件.
            cin >> out ;
            if ( out != 1 || cin == false )  //輸入錯誤訊息.
            {
                break ;
            }
        }
    }
    return 0 ;
}
