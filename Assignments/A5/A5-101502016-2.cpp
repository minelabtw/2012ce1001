#include <iostream>

using namespace std;

int main()
{
    int n,i,j,k;
    cin >> n;

    while( n!=0 )
    {
        if ( cin == false || n<0 )//錯誤偵測
        return -1;

        k=(n-1)/2;//修正空格的數量
        /////////////////////////////////////
        if ( n%2 == 1 )//奇數區
        {
            for( i=1 ; i<=n/2+1 ; i++)//上半部星星
            {
                for( j=1 ; j<=n-i-k ; j++)
                    cout << " ";
                for( j=1 ; j<=2*i-1 ; j++)
                    cout << "*";

                cout << endl;
            }
            for( i=n/2 ; i>=1 ; i--)
            {
                for( j=1 ; j<=n-i-k ; j++)
                    cout << " ";
                for( j=1 ; j<=2*i-1 ; j++)
                    cout << "*";
            cout << endl;
            }
        }
        //////////////////////////////////
        else if( n%2 == 0)//偶數區
        {
            for( i=1 ; i<=n ; i++)//第一行
            {
                cout << "*";
            }
            cout << endl;
            for ( i=1 ; i<=n-2 ; i++)
            {
                cout << "*" ;//最前面一個星

                for ( j=1 ; j<=n-2 ; j++)//中間空白
                    cout << " " ;

                cout << "*" ;//最後面一個星
                cout << endl;
            }
            for( i=1 ; i<=n ; i++)//最後一行
            {
                cout << "*";
            }
            cout << endl;

        }


        cin >> n;

    }

    return 0;
}
