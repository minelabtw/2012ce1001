#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

void DicePlay()//Definition of function DicePlay
{
    int a,b;
    string str;
    srand ( time(NULL) );//Initialize random seed
    do
    {
        a=(rand()%6)+1;
        b=(rand()%6)+1;
        cout<<endl;
        cout<<"Computer's dice point is: "<<a<<endl;
        cout<<"Your dice point is: "<<b<<endl;
        if(a>b)//Compare a and b
        {
            cout<<"Sorry, you lose."<<endl;
        }
        else if(a==b)
        {
            cout<<"Draw"<<endl;
        }
        else
        {
            cout<<"Congratulation! You win."<<endl;
        }
        cout<<"Want to play again? ( 1 for continue, 0 for exit ) ";
        cin>>str;
    }while(str=="1");//Determine wheather to run the loop again
}
int main()
{
    DicePlay();//Calling function DicePlay
    return 0;
}
