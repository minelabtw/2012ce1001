#include<iostream>
using namespace std;
int main()
{
    int n;
    cin>>n;
    if (cin==false)
        return -1;
    while(n>0)
    {
        if(n%2==1) //判斷n是否為奇數
        {
            for(int line=1;n>=line;line++)
            {
                if((n-1)/2>=line) //菱形的上半部分
                {
                    for(int k=2;(n-1)/2+2-line>=k;k++)
                        cout<<" ";
                    for(int k=1;2*line-1>=k;k++)
                        cout<<"*";
                    cout<<endl;
                }
                else //菱形的下半部分
                {
                    for(int k=2;line-(n-1)/2>=k;k++)
                        cout<<" ";
                    for(int k=1;2*(n-line)+1>=k;k++)
                        cout<<"*";
                    cout<<endl;
                }
            }
        }
        if(n%2==0) //判斷n是否為偶數
        {
            for(int line=1;n>=line;line++)
            {
                if(line==1 || line==n) //若為第一行或最後一行,則輸出n個星星
                {
                    for(int k=1;n>=k;k++)
                        cout<<"*";
                    cout<<endl;
                }
                else  //若為其他行,則在最左邊跟最右邊輸出星星
                {
                    cout<<"*";
                    for(int k=2;n>k;k++)
                        cout<<" ";
                    cout<<"*"<<endl;
                }
            }
        }
        cin>>n;
        if (cin==false)
            return -1;
    }
    return 0;
}//end main
