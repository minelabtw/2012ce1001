#include <iostream>

using namespace std;

int main()
{
    int n1,n5;
    while (cin>>n1&&n1!=0)//debug
    {
        if (n1%2==1)//if the integer is odd number,it will print out a diamond.
        {
            n5=(n1+1)/2;
            for (int n2=1;n2<=n5;n2++)
            {
                for (int n3=1;n3<=n5-n2;n3++)
                    cout<<" ";
                for (int n3=1;n3<=n2*2-1;n3++)
                    cout<<"*";
                cout<<endl;
            }//to print out the upper part of the diamond.
            for (int n2=1;n2<=n5-1;n2++)
            {
                for (int n3=1;n3<=n2;n3++)
                    cout<<" ";
                for (int n3=1;n3<=n1-n2*2;n3++)
                    cout<<"*";
                cout<<endl;
            }//to print last part of it.
        }
        if (n1%2==0)
        {
            for (int n2=1;n2<=n1;n2++)
                cout<<"*";//the upper part of the rectangle
            cout<<endl;
            for (int n2=2;n2<=n1-1;n2++)
            {
                cout<<"*";
                for (int n2=1;n2<=n1-2;n2++)
                    cout<<" ";
                cout<<"*"<<endl;
            }//middle part
            for (int n2=1;n2<=n1;n2++)
                cout<<"*";//the bottom of the rectangle
            cout<<endl;
        }
    }
    return 0;
}
