/*****************************************
* Assignment 5
* Name: chi ju wu
* School Number: 986001511
* E-mail: lu19900213@gmail.com
* Course: 2012-CE1001
* Submitted: 2012/10/25
*
***********************************************/

#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    // i is for continue loop, c for computer's dice, d for player's dice
    int i,c,d;
    // initialize i=1 for continue
    i=1;

    while ( i == 1 )
    {
       srand((unsigned)time(NULL));
       //Generate random number for computer and put it in to a.
       int a = rand();
       //Generate random number for player and put it in to b.
       int b = rand();

       c = a%10+1;
       d = b%10+1;
       cout << "Computer's dice point is: " << c <<endl;
       cout << "Your dice point is: " << d <<endl;


      if ( c > d )
      {
         cout << "Sorry, you lose." << endl;
      }
      else if ( c < d )
      {
         cout << "Congratulation! you win." << endl;
      }
      else
      {
         cout << "Draw" << endl;
      }

      // ask users if contibnue playing
      cout << "Want to play again? ( 1 for continue,0 for exit ): ";
      cin >> i;
      cout << endl;
   }

    system("pause");
    return 0;
}

