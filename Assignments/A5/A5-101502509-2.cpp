#include <iostream>
#include <limits>
#include <cstdlib>

using namespace std;

int main()
{
     int length;

     while(cin >>length &&length>0)    //if length is a negative number or characters or equal to 0,finish the program,
                                       //but if length is a positive number,restart the program
     {
         cin.clear();
         cin.ignore(numeric_limits<int>::max(),'\n');
         if(length%2==1)  //if length is an odd number,print out a rhombus,and its area is (length)^2 /2
         {
              for(int i=1;i<=(length+1)/2;i++)  //print out the upper half of the rhombus
              {
                 for(int j=1;j<=(length+1)/2-i;j++)
                     cout <<" ";
                 for(int j=1;j<=i*2-1;j++)
                     cout <<"*";
                 cout << endl;
              }
              for(int i=1;i<(length+1)/2;i++)  //print out the lower half of the rhombus
              {
                 for(int j=1;j<=i;j++)
                     cout <<" ";
                 for(int j=1;j<=length-(i*2);j++)
                     cout <<"*";
                 cout << endl;
              }
         }

         else if(length%2==0)  //if length is an even number,print out a hollow rectangle,and its width is length
         {
              for(int i=1;i<=length;i++)
                 cout <<"*";
              cout << endl;
              for(int i=2;i<=length-1;i++)
              {
                 cout <<"*";
                 for(int j=1;j<=length-2;j++)
                      cout <<" ";
                 cout <<"*"<<endl;
              }
              for(int i=1;i<=length;i++)
                 cout <<"*";
              cout << endl;
         }
     }
     system("pause");
     return 0;

}
