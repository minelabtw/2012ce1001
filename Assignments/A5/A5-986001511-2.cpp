/*****************************************
* Assignment 5
* Name: chi ju wu
* School Number: 986001511
* E-mail: lu19900213@gmail.com
* Course: 2012-CE1001
* Submitted: 2012/10/25
*
***********************************************/

#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
  // input the number
  int n;
  // if the number is true
  while ( cin>>n != false )
  {
   // if the number is less than 1, thanstop programming
   if( n <= 0)
   {
     return -1;
   }

   // if the number is an odd number
   if( n%2 == 1 )
   {
      // i is for the lines
      for(int i=1 ; i<=n ; i=i+2)
      {
        // a is for the places in each line
        for(int a=0 ; a<(n-i+2)/2 ; a++)
        {
           cout<<" ";
        }
        for(int a=0;a<i;a++)
        {
           cout<<"*";
        }
       cout<<endl;
      }

      // i is for the lines
      for(int i=n-2 ; i>=1 ; i=i-2)
      {
        // a is for the places in each line
        for(int a=0 ; a<(n-i+2)/2 ; a++)
        {
           cout<<" ";
        }
        for(int a=0;a<i;a++)
        {
           cout<<"*";
        }
       cout<<endl;
      }
    }

    // if the number is odd
    else
    {
        // the first line, plot all stars
        for(int a=0;a<n;a++)
        {
           cout<<"*";
        }
        cout<<endl;

        // the middle lines, plot the coundaries
        for(int i=2 ; i<n ; i++)
        {
           cout<<"*";

           for(int a=1; a<n-1 ; a++)
           {
              cout<<" ";
           }

           cout<<"*";

         cout << endl;
        }

        // the final line, plot all stars
        for(int a=0;a<n;a++)
        {
           cout<<"*";
        }

        cout<<endl;
    }
  }

    system("pause");
    return 0;
}
