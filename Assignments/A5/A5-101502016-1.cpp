#include <iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int main()
{
    srand((unsigned)time(NULL));//設定一個不斷變動的亂數種子(不過我不知道unsigned的意思..)

    int x,y;//表示亂數除以六後的餘數+1(才不會出現0)
    int z;

    do
    {
        int a = rand();
        int b = rand();
        x = a%6+1;
        y = b%6+1;

        cout << "Computer's dice point is: " << x << endl;
        cout << "Your dice point is: " << y << endl;

        if ( x>y )
        {
            cout << "Sorry, you lose." <<endl;
            cout << "Want to play again? ( 1 for continue, 0 for exit )";
            cin >> z;
        }
        else if ( x<y )
        {
            cout << "Congratulation! You win." << endl;
            cout << "Want to play again? ( 1 for continue, 0 for exit )";
            cin >> z;
        }
        else if ( x==y )
        {
            cout << "Draw" << endl;
            cout << "Want to play again? ( 1 for continue, 0 for exit )";
            cin >> z;
        }
    }while( z == 1 );


    return 0;
}
