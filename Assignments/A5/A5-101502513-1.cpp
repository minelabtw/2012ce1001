#include<iostream>
#include<time.h> //contains prototype for function time
#include<cstdlib> //contains prototype for function srand and rand
using namespace std;

int main()
{
    srand((unsigned)time(NULL));
    //In order to generate random-like numbers, we need some value which changes every second.
    //like the value returned by the function "time"
    //And "srand((unsigned)time(NULL));" is distinctive enough for most trivial randoming needs.

    int a = rand() % 6 + 1;
    int b = rand() % 6 + 1; //Generate random number(1 to 6) and put it in to "a" and "b".
    int number;

    cout << "Computer's dice point is: " << a <<endl;
    cout << "Your dice point is: " << b <<endl;

    if( a > b )
        cout << "Sorry, you lose." << endl;
    else if( a < b )
        cout << "Congratulation! You win." << endl;
    else if( a = b )
        cout << "Draw" << endl;

    cout << "Want to play again? ( 1 for continue, 0 for exit ) ";

    while( cin >> number && number == 1 ) //input number repetitiously
    { //if number isn't equal to 1,program ends
        cout << endl;

        a = rand() % 6 + 1;
        b = rand() % 6 + 1;

        cout << "Computer's dice point is: " << a <<endl;
        cout << "Your dice point is: " << b <<endl;

        if( a > b )
            cout << "Sorry, you lose." << endl;
        else if( a < b )
            cout << "Congratulation! You win." << endl;
        else if( a = b )
            cout << "Draw" << endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
    }
    return 0;
}
