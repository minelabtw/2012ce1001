#include<iostream>
using namespace std;
int main()
{
    int a;
    while( cin >> a && a > 0 )//輸入一個數值a且a必須大於0
    {

        if( a % 2 != 0 )//a為奇數
        {
            for( int i = 1 ; i <= ( a + 1 ) / 2 ; i++ )//顯示上半部的三角形
            {
                for( int j = 1 ; j <= (a - ( 2 * i - 1 ) ) / 2 ; j++ )
                    cout<<" ";
                for( int j = 1 ; j <= i * 2 - 1 ; j++)
                    cout<<"*";
                cout<<endl;
            }
            for( int i = 1 ; i <= ( a - 1 ) / 2 ; i++)//顯示下半部的三角形
            {
                for( int j = 1 ; j <= i ; j++ )
                    cout<<" ";
                for( int j = 1 ; j <= a - 2*i ; j++ )
                    cout<<"*";
                cout<<endl;
            }

        }
        else//a為偶數
        {
            for(int i=1; i<=a; i++)//顯示最上排的*字號
            {
                cout<<"*";
            }
            cout<<endl;
            for(int j=1; j<=a-2; j++)//顯示中間的(a-2)排
            {
                cout<<"*";
                for(int k=1; k<=a-2; k++)
                {
                    cout<<" ";
                }
                cout<<"*";
                cout<<endl;
            }
            for(int i=1; i<=a; i++)//顯示最下排的*字號
            {
                cout<<"*";
            }
            cout<<endl;
        }
    }
    return 0;
}
