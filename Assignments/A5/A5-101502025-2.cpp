#include <iostream>
#include<cmath>
using namespace std;
int main()
{   int a,b,c,d;
    while(cin>>b)
    {
        if(b==false||b<=0) // error contection
          return 0;
        if(b%2==0) // when the input number is even
        {
            for(a=1;a<=b;a++) // the first line
                cout<<"*";
            cout<<endl;
            for(a=1;a<=(b-2);a++) // the central lines
            {
                cout<<"*";

                for(c=1;c<=(b-2);c++)
                    cout<<" ";

                cout<<"*"<<endl;
            }
            for(a=1;a<=b;a++) // the last line
                cout<<"*";
            cout<<endl;
         }
         if(b%2==1) // when the input is odd
         {
            for(a=1;a<=b/2+1;a++)  // the higher hail of the pattern
            {
                d=(b-1)/2;
                for(c=1;c<=b-a-d;c++)
                    cout << " ";
                for(c=1;c<=a*2-1;c++)
                    cout << "*";
                cout<<endl;
            }
            for(a=1;a<=b/2;a++) // another part of the pattern
            {
                for(d=1;d<=a;d++)
                    cout<<" ";
                for(d=1;d<=b-2*a;d++)
                    cout<<"*";
                cout<<endl;
            }
          }
    }
    return 0;
}
