#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int main()
{
    while(1)
    {
        srand((unsigned)time(NULL));
        int a = rand();
        int b = rand();
        int c;

        cout <<endl;
        cout << "Computer's dice point is: " << a%6+1 <<endl;//數字介於1~6因此用x%6+1
        cout << "Your dice point is: " << b%6+1 <<endl;

        if( a%6+1 > b%6+1 )
            cout << "Sorry, you lose." <<endl;
        if( a%6+1 == b%6+1 )
            cout << "Draw" <<endl;
        if( a%6+1 < b%6+1 )
            cout << "Congratulation! You win."<<endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
        cin >> c;

        if( c != 1 || cin == false || c < 0 )
            break;
    }
    return 0;
}
