// 101502503
#include<iostream> // allows program to output data to the screen
#include<cmath>

using namespace std ;
// funtion main begins program execution
int main ()
{

    int a ;

    cin >> a ;

    if ( cin == false ) // 如果輸入為字元,結束程式
        return -1 ;

    while ( a>0 ) // 當a大於0,進入while迴圈
    {
        if ( a%2==1 ) // 如果a為奇數,執行以下動作
        {
            for ( int i = 1 ; i<= (a+1)/2 ; i++) // i初始值為1,當i小於等於a+1除以2,做下面指令,做完i+1
            {
                for ( int j = 1 ; j <= (a+1)/2-i ; j++ )// j初始值為1,當j小於等於a+1除以2+i,做下面指令,做完j+1
                    cout << " ";
                for ( int j = 1 ; j <= i * 2 - 1 ; j++ )// j初始值為1,當j小於等於i*2-1,做下面指令,做完j+1
                    cout << "*";
                cout << endl ;
            } //end for
            for ( int i = 1 ; i <= (a-1)/2 ; i++)// i初始值為1,當i小於等於a-1除以2,做下面指令,做完i+1
            {
                for ( int j = 1 ; j <= i ; j++)// j初始值為1,當j小於等於i,做下面指令,做完j+1
                    cout << " " ;
                for ( int j = 1 ; j <= a-(i*2) ; j++)// j初始值為1,當j小於等於a-(i*2),做下面指令,做完j+1
                    cout << "*" ;
                cout << endl ;
            }// end for
        } // end if
        else if ( a%2==0 ) //如果a為偶數,執行以下動作
        {
            for ( int j = 1 ; j <= a ; j++ )// j初始值為1,當j小於等於a,做下面指令,做完j+1
                cout << "*";
            cout << endl;
            for ( int i = 1 ; i <= a-2 ; i++ )// i初始值為1,當i小於等於a-2,做下面指令,做完i+1
            {
                cout << "*";
                for (int j = 1 ; j <= a-2 ;j++ )// j初始值為1,當j小於等於a-2,做下面指令,做完j+1
                    cout << " ";
                cout << "*" << endl;
            } // end for
            for ( int j = 1 ; j <=a ; j++ )// j初始值為1,當j小於等於a+,做下面指令,做完j+1
                cout << "*" ;
            cout << endl ;
        } // end else if
        cin >> a ;
        if (cin == false)//如果輸入為字元,結束程式
            return -1 ;
    } // end while
    return 0 ;
} // end main
