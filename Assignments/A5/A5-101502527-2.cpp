#include<iostream>
using namespace std;
int main()
{
    int a;
    while(cin>>a!=false&&a>0)
    {
        if(a%2==1)
        {
            a=a/2+1;//奇數的第幾個圖
            for(int x=0;x<a;x++)//菱形上半部
            {
                for(int y=0;y<a-x-1;y++)//輸出空白  用a和x的變化求出要輸出幾個
                    cout<<' ';
                for(int y=0;y<2*x+1;y++)//輸出星號  用a和x的變化求出要輸出幾個
                    cout<<'*';
                cout<<"\n";//換行
            }
            for(int x=1;x<a;x++)//菱形下半部
            {
                for(int y=0;y<x;y++)//輸出空白  用a和x的變化求出要輸出幾個
                    cout<<' ';
                for(int y=0;y<(a-x)*2-1;y++)//輸出星號  用a和x的變化求出要輸出幾個
                    cout<<'*';
                cout<<"\n";//換行
            }
        }
        else
            for(int x=1;x<=a;x++)
            {
                for(int y=1;y<=a;y++)//XY用來輸出總共a*a次
                {
                    if(x==a||x==1||y==1||y==a)//在邊界的判斷 輸出星號
                        cout<<"*";
                    else//非邊界輸出空白
                        cout<<' ';
                }
                cout<<'\n';
            }
    }
    return 0;
}
