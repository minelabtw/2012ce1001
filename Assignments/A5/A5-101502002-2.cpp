#include<iostream>
using namespace std;
int main()
{
    int a;//a為輸入的數
    while(cin>>a)//當輸入整數時則進入迴圈
    {
        if(a>0)
        {
            if(a%2==1)//奇數時
            {
                for(int i=1;i<=a;i=i+2)//上半部的迴圈，每個I代表一行
                {
                    for( int j = 1 ; j <= (a - i)/2 ; j++ )//空格的迴圈
                        cout<<" ";
                    for( int j = 1 ; j <= i; j++ )//*的迴圈
                        cout<<"*";
                    cout<<endl;//結束一行
                }
                for(int i=a-2;i>=1;i=i-2)//下半部迴圈
                {
                    for( int j = 1 ; j <= (a - i)/2 ; j++ )
                        cout<<" ";
                    for( int j = 1 ; j <= i; j++ )
                        cout<<"*";
                    cout<<endl;
                }
            }
            else if(a%2==0)//當a為偶數
            {
                for(int i=1;i<=a;i++)//第一行的*數
                    cout<<"*";
                cout<<endl;//換行
                if(a!=2)//2以上的偶數才有中間
                {
                    for(int i=1;i<=a-2;i++)
                    {
                        cout<<"*";//先輸出一個*
                        for(int j=1;j<=a-2;j++)//在輸出空格
                            cout<<" ";
                        cout<<"*";//最後在一個*
                        cout<<endl;//煥行
                    }
                }
                for(int i=1;i<=a;i++)//最後一行的*
                    cout<<"*";
                cout<<endl;
            }

        }
        else
            break;
    }
    return 0;
}
