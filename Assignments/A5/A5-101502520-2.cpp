#include <iostream>
#include <cstdlib>
using namespace std;
int main()
{
    int num;
    while(cin>>num!=false && num>0)
        if(num%2==0)
            for(int i=1;i<=num;i++,cout<<endl)
                if(i==1 || i==num)//separate the square into the first line, the last line, and the other lines.
                {
                    for(int j=1;j<=num;j++)
                        cout<<"*";
                }
                else
                {
                    cout<<"*";
                    for(int j=1;j<=num-2;j++)
                        cout<<" ";
                    cout<<"*";
                }
        else
            for(int i=1;i<=num;i++,cout<<endl)
            {
                for(int j=0;j<abs(num/2-i+1);j++)//using absolute value to calculate how many times they should be printed.
                    cout<<" ";
                for(int k=0;k<num-abs(num-2*i+1);k++)
                    cout<<"*";
            }
    return 0;
}
