#include<iostream>
using namespace std;
main()
{
    while(1)//使程式能夠重覆執行
    {//start while
        int h;

        cout<<"Enter the height: ";

        if(cin>>h==false||h<=0)//若h為字元、0或1則結束程式
        {
            return -1;
        }

        else if(h%2==1)//若h為奇數則排出菱形
        {
            for(int i=1;i<=(h+1)/2;i++)
            {
                for(int j=1;j<=(h+1)/2-i;j++)
                cout<<" ";
                for(int j=1;j<=i*2-1;j++)
                cout<<"*";
            cout<<endl;
            }
            for(int i=1;i<=(h+1)/2-1;i++)
            {
                for(int j=1;j<=i;j++)
                cout<<" ";
                for(int j=1;j<=(((h+1)/2)-i)*2-1;j++)
                cout<<"*";
            cout<<endl;
            }
        }
        else//若h為偶數則排出空心方形
        {//start else
            for(int j=1;j<=h;j++)//方型的上緣
            {
                cout<<"*";
            }
            cout<<endl;
            for(int i=1;i<=h-2;i++)//方形兩側
            {
                cout<<"*";
                for(int j=1;j<=h-2;j++)
                cout<<" ";
                cout<<"*\n";
            }
            for(int j=1;j<=h;j++)//方形下緣
            {
                cout<<"*";
            }
            cout<<endl;
        }//end else
    }//end while
    return 0;
}
