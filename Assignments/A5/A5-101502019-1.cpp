#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int main()
{
    int num;
    do//直接先執行一次
    {
        srand((unsigned)time(NULL));

        int a = rand(),b = rand();//宣告隨機變數

        cout << "Computer's dice point is: " << a%6 + 1 << endl;
        cout << "Your dice is: " << b%6 + 1 << endl;

        if (a%6 + 1 > b%6 + 1)//判斷大小
            cout << "Sorry, you lose." << endl;
        else if (a%6 + 1 < b%6 + 1)//判斷大小
            cout << "Congratulation! You win." << endl;
        else if (a%6 + 1 == b%6 + 1)//判斷大小
            cout << "Draw" << endl;

        cout << "Want to play again? (1 for continue, 0 for exit )";
        cin >> num;

        if (cin == false || (num != 1 && num != 0))//非0或1的輸入則列出下方句子
            {
                cout << "The input should be 1 or 0.";
                break;
            }
    } while (num == 1);

    return 0;
}
