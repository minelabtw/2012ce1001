#include<iostream>
#include<time.h>
#include<cstdlib>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    while(int i = 1 )
    {
        srand((unsigned)time(NULL));
        int number1 = (rand()%6+1),number2 = (rand()%6+1);//數字1到6隨機變化
        cout << "Computer's dice point is: " << number1 << endl;
        cout << "Your dice point is: " << number2 <<endl;
        if( number1 > number2 )//number1 > number2 表示輸了
        {
            cout << "Sorry, you lose." << endl <<"Want to play again? ( 1 for continue, 0 for exit )";
            cin >> i;
        }
        if( number1 < number2 )//number1 < number2 表示贏了
        {
            cout << "Congratulation! You win." << endl << "Want to play again? ( 1 for continue, 0 for exit )";
            cin >> i;
        }
        if( number1 == number2 )//number1 = number2 表示平手
        {
            cout << "Draw" << endl << "Want to play again? ( 1 for continue, 0 for exit ) ";
            cin >> i;
        }
        if( cin == false )//輸入錯誤則結束程式
        {
            return-1;
        }
        if( i > 1 )//輸入1以外的數字則結束程式
        {
            return-1;
        }
        if( i <= 0 )//輸入為負數或零則結束程式
        {
            return-1;
        }
    }
    return 0;
}
