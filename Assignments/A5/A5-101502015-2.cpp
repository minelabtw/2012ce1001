#include<iostream>
using namespace std;
int main()
{
    int height,i,j,remainer;
    while(cin>>height && height>0)
    {
        remainer=height%2;//判斷是偶數或奇數
        if(remainer==0)//若為偶數
        {
            for(int i=1;height>=i;i++)//進入迴圈i代表第幾行數
            {
                if(i==1 || height==i )//若為第一行或最後一行
                {
                    for(int j=1;height>=j;j++)//輸出與邊長相同之星星
                        cout<<"*";
                    cout<<endl;
                }
                else//若為其它行
                {
                    cout<<"*";
                    for(int j=2;height>=j+1;j++)
                        cout<<" ";
                    cout<<"*"<<endl;
                }
            }
        }
        if(remainer==1)//若為奇數
        {
            for(int i=1;height>=i;i++)//其中之i代表行數
            {
                if((height-1)/2>=i)//圖形前半部之方法
                {
                    for(int j=1;j<=(height-1)/2-i+2;j++)//湊出來的空白數方法
                        cout<<" ";
                    for(int j=1;2*i>j;j++)
                        cout<<"*";
                    cout<<endl;
                }
                else//圖形後半部之方法
                {
                    for(int j=1;j<=i-(height-1)/2;j++)//湊出來的空白數方法
                        cout<<" ";
                    for(int j=1;2*(height-i)+1>=j;j++)
                        cout<<"*";
                    cout<<endl;
                }
            }
        }
    }
    return 0;
}
