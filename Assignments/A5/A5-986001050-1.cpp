#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int main()
{
    srand((unsigned)time(NULL));
    int computers = rand();     //Generate random number and put it in to "computers".
    int your = rand();          //Generate random number and put it in to "your".
    int integer;

    computers = computers%6 +1;   //Change to 1~6 random number
    your = your%6 +1;             //Change to 1~6 random number

    if( computers > your )      //computer's > your
    {
        cout << "Computer's dice point is: " << computers << endl
             << "Your dice point is: " << your << endl
             << "Sorry, you lose." << endl
             << "Want to play again? ( 1 for continue, 0 for exit ) ";
    }
    if( computers < your )      //computers < your
    {
        cout << "Computer's dice point is: " << computers << endl
             << "Your dice point is: " << your << endl
             << "Congratulation! You win." << endl
             << "Want to play again? ( 1 for continue, 0 for exit ) ";
    }
    if( computers == your )     //computers = your
    {
        cout << "Computer's dice point is: " << computers << endl
             << "Your dice point is: " << your << endl
             << "Draw" << endl
             << "Want to play again? ( 1 for continue, 0 for exit ) ";
    }

    while( cin >> integer && integer == 1 )  //如果輸入負數或零或字元則結束程式。
    {
        //解釋同上
        int computers = rand();
        int your = rand();

        computers = computers%6 +1;
        your = your%6 +1;

        cout << endl;

        if( computers > your )
        {
            cout << "Computer's dice point is: " << computers << endl
                 << "Your dice point is: " << your << endl
                 << "Sorry, you lose." << endl
                 << "Want to play again? ( 1 for continue, 0 for exit ) ";
        }
        if( computers < your )
        {
            cout << "Computer's dice point is: " << computers << endl
                 << "Your dice point is: " << your << endl
                 << "Congratulation! You win." << endl
                 << "Want to play again? ( 1 for continue, 0 for exit ) ";
        }
        if( computers == your )
        {
            cout << "Computer's dice point is: " << computers << endl
                 << "Your dice point is: " << your << endl
                 << "Draw" << endl
                 << "Want to play again? ( 1 for continue, 0 for exit ) ";
        }
    }
    return 0;
}
