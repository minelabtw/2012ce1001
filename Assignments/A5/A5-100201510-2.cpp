#include<iostream>

using namespace std;

int main()
{
    int n = 1 , j;

    do{
        cin >> n ;
        cout << endl ;

        if(n <= 0 || cin == false)
        {
            return 0 ;
        }
        else if(n == 1)
        {
            cout << "*" << endl ;
        }
        else if(n == 2)
        {
            cout << "**" << endl ;
            cout << "**" << endl ;
        }
        else if(n%2 == 1)
        {
            for(int i = 1 ; i <= n ; i++)
            {
                if(i < n/2 + 1) //see "*" as an increasing sequence
                {
                    for(j = 1 ; j <= n - i -2; j++ )
                        cout << " ";
                    for(j = 1 ; j <= 2*i - 1 ; j++ )
                        cout << "*";
                    cout << endl;
                }
                if(i > n/2 -1) //see "*" as an decreasing sequence
                {
                    for(j = 1 ; j <= i -2 ; j++ )
                        cout << " ";
                    for(j = 1 ; j <= 2*(n - i) - 1; j++ )
                        cout << "*";
                    cout << endl;
                }
            }
        }
        else // n is even , print square.
        {
            for(int i = 1 ; i <= n ; i++)
            {
                if(i == n || i == 1) //print a row of all "*"
                {
                    for(int j = 1 ; j <= n ; j++)
                        cout << "*" ;
                }
                else //print "*" only at start and end.
                {
                    cout << endl << "*" ;
                    for(int j = 1 ; j <= n-2 ; j++)
                    {
                        cout << " " ;
                    }
                    cout << "*" << endl;
                }
            }
            cout << endl ;
        }
    }while(n>0);
}
