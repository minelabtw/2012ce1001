#include <iostream>

using namespace std;

int main()
{
    int a,n,m,x;

    while((cin>>a)!=false && a>0)          //判斷是否為正整數,是則進入迴圈
    {
        if(a%2==0)                         //輸入值a為偶數得情況
        {
            for(n=1;n<=a;n++)              //產生a層
            {
                if(n==1 || n==a)           //第1層與第a層為a個星號
                {
                    for(m=1;m<=a;m++)
                        cout<<"*";
                    cout<<"\n";
                }
                else                       //其餘皆為頭尾星號,其餘空白
                {
                    cout<<"*";
                    for(m=1;m<=a-2;m++)    //輸出a-2個空白
                        cout<<" ";
                    cout<<"*\n";
                }
            }
        }

        else                               //輸入值a為奇數得情況
        {
            for(n=1,m=(a-1)/2 ; n<=a ; n++,m--)   //n用來控制星號數,m用來控制空白數
            {
                if(m>=0)                      //菱形在中間以上的排法(包含中間)
                {
                    for(x=1;x<=m;x++)         //隨m的遞減輸出空白
                        cout<<" ";
                    for(x=1;x<=n*2-1;x++)     //從1個開始,每隔行多輸出2個星號
                        cout<<"*";
                    cout<<"\n";
                }

                else                          //菱形在中間以下的排法(不包含中間)
                {
                    for(x=1;x<=m*(-1);x++)    //將負數的m調回正數,再輸出空白
                        cout<<" ";
                    for(x=1;x<=(a-n)*2+1;x++) //從中間開始,每隔行少2個星號
                        cout<<"*";
                    cout<<"\n";
                }
            }
        }
    }
    return 0;
}
