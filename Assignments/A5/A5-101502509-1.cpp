#include <iostream>
#include <time.h>
#include <limits>
#include <cstdlib>

using namespace std;

int main()
{
     int compoint,yourpoint,input=1;
     srand((unsigned)time(NULL));    //generate random-numbers,and random-numbers are changing every second
     do
     {
        compoint =rand()%6+1;        //generate a random number from 1~6,as the computer dice number
        yourpoint =rand()%6+1;       //generate a random number from 1~6,as your dice number
    
        cout << "Computer's dice point is: " << compoint << endl;
        cout << "Your dice point is: " << yourpoint <<endl;
        if(compoint>yourpoint)                   // if computer dice number>your dice number,you lose
	       cout << "Sorry, you lose."<<endl;
        else if(compoint<yourpoint)             // if computer dice number<your dice number,you win
	       cout << "Congratulation! You win."<<endl;
        else if(compoint==yourpoint)            // if computer dice number=your dice number,it is a tie game 
	       cout << "Draw"<<endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";   //give choose to continute or finish the program
        cin >> input;                                                    // 1 is continute,and 0 is exit  
        cin.clear();  //to clear the buffer
        cin.ignore(numeric_limits<int>::max(),'\n');  //to ignore the wrong data in the buffer

     }while(input==1 && cin.gcount()==1);   //if your choose is 1,restart the program

     system("pause");
     return 0;
}



