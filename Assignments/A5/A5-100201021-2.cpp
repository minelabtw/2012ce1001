#include<iostream>
#include<iomanip>

using namespace std;

int n,i,j;
int main()
{
    while(1>=0)
    {
        cin >> n;
        if(n == false)      //偵錯 (0,字元)
            return -1;
        if(n < 0)           //偵錯 (負數)
            return -1;
        if(n%2 == 1)        //判斷菱形or矩形
        {
            ////菱形部分
            for(i=0;i <= n/2;i++)   //每層開頭(上半)
            {
                cout << setw(n/2+1-i) << "*";
                for(j=0;j<=i-1;j++) //補 "*"
                    cout << "**";
                cout << endl;
            }
            for(i=n/2;i >= 1;i--) //每層開頭(下半)
            {
                cout << setw(n/2+2-i) << "*";
                for(j=1;j<=i-1;j++)
                    cout << "**";   //補 "*"
                cout << endl;
            }
        }
        else
        {
            ////矩形部分
            for(i=1;i<=n;i++)   //上邊
                cout << "*";
            cout << endl;
            for(i=1;i<=n-2;i++) //側邊
            {
                cout << "*" << setw(n-1) << "*";
                cout << endl;
            }
            for(i=1;i<=n;i++)   //底邊
                cout << "*";
            cout << endl;
        }
    }
    return 0;
}
