//作業 5-2
//班級:資工系四年B班
//學號:965002086
//姓名:張凱閔
/*請寫出一個可以印出矩形或菱形的程式。
 輸入：一連串的正整數 n 。如果輸入負數或零或字元則結束程式。
 輸出：
	如果 n 為奇數，則印出一個面積為 n 平方除以 2 的菱形。
	如果 n 為偶數，則印出一個邊長為 n 的空心矩形。。*/
#include <iostream>
#include <iomanip> //for setw()
using namespace std;
void prtsquare(int x);
void prtdiamond(int y);

int main()
{
	int a;
	
	cin>>a;

	//產生循環運作
	while (a>=1)
{ 
		if (a%2==0)//偶數
			prtsquare(a);
		else
			prtdiamond(a);//奇數列印
		cout<<endl;
		cin>>a;
		
 };
	cout<<"\nProcess returned 0\n";
//系統暫停以便觀察結果
	system("pause");
}


void prtsquare(int x)
{
	int i;
	//列印第一Row
	for (i=1;i<=x;i++) cout<<"*";
	//列印第二至倒數第二Row
	for (i=1;i<=(x-2);i++)
	{
		cout<<endl<<"*";
		cout<<setw(x-1)<<"*";
	}
	cout<<endl;
	//列印最後一Row
	for (i=1;i<=x;i++) cout<<"*";
}

void prtdiamond(int y)
{
	int i,j;
	//列印菱形上半部加一列
	for (i=1;i<=y/2+1;i++)
	{
	cout<<setw(y/2+2-i);
	for (j=1;j<=(2*i-1);j++)
		cout<<"*";
	cout<<endl;
	}
	//列印下半部
	for (i=y/2;i>=1;i--)
	{
	cout<<setw(y/2+2-i);
	for (j=(2*i-1);j>=1;j--)
		cout<<"*";
	cout<<endl;
	}
}
