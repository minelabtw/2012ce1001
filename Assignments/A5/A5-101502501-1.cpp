#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    int n;

    while ( n != false )//當n為整數時,進入迴圈
    {
        srand((unsigned)time(NULL));

        int a = rand();//製造一亂數a
        int b = rand();//製造一亂數b

        cout << "Computer's dice point is: " << a%6+1 <<endl;//輸出文字及a除以6之餘數加一(即可為骰子數目)
        cout << "Your dice point is: " << b%6+1 << endl;//輸出文字及b除以6之餘數加一(即可為骰子數目)

        if( a%6+1 > b%6+1 )//當電腦的骰子數目大於玩家時
            cout << "Sorry, you lose." << endl;

        else if ( a%6+1 < b%6+1 )//當電腦的骰子數目小於玩家時
            cout << "Congratulation! You win." << endl;

        else if ( a%6+1 == b%6+1 )//當雙方數目相同時
            cout << "Draw" << endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
        cin >> n;

        if( n > 1 )//若輸入大於1則結束程式
            return -1;

        cout << endl;
    }
    return 0;
}
