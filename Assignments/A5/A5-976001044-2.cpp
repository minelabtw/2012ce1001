// Assignment #5-2
// A5-976001044-2.cpp
#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

// Declare two fucntion to draw Rectangle or Rrhombus
int Rectangle(int);
int Rrhombus(int);

int main()
{
    int input=0;

    // Repeat the instruction in While Loop, only when the input number is non-zero integer
    while ( (cin >> input != false) && (input > 0) )
    {
        // If the input number is Even then draw Rectangle, and Odd to draw Rrhombus
        if ( input%2 == 0 )
        {
            Rectangle(input);
        }
        else if ( input%2 == 1 )
        {
            Rrhombus(input);
        }
    }
    system("pause");
    return 0;
}

// The function of draw Rectangle
// Output screen can be deemed as two-dimensional space
// Using two layers For Loop and check the location point
// Draw the sign '*' when following situations < Sideline of Rectangle >
//      1. Topmost Line    of the Rectangle ( i == 1 )
//      2. Bottom Line     of the Rectangle ( i == n )
//      3. Leftmost Line   of the Rectangle ( j == 1 )
//      4. Rightmost Line  of the Rectangle ( j == n )
// ,and the sign ' ' when other situations     < Internal of Rectangle >
int Rectangle(int n)
{
    for ( int i=1 ; i <= n ; i++ )
    {
        for ( int j=1 ; j <= n ; j++ )
        {
            if ( (i == n) || (j == n) || (i == 1) || (j == 1) )
            {
                cout << "*";
            }
            else
            {
                cout << " ";
            }
        }
        cout << endl;
    }
    return 0;
}


// The function of draw Rrhombus
// Output screen can be deemed as two-dimensional space
// Using two layers For Loop and check the location point
// Base on loop operation ( row by row ), cutting rhombus to two blocks
//                                      ( Upper block and Bottom block )
int Rrhombus(int n)
{
    // Declare this three variable
    // The variable MidLine is assist to set variable LowerBound and UpperBound
    int MidLine=(n+1)/2, LowerBound=0, UpperBound=0;

    // -----------------------------------------------------------------------------------------------------
    // If the location is between LowerBound and UpperBound, then the character "*" should be output.
    // And if the location is before UpperBound or after LowerBoun, then the character " " should be output.
    // For example, when input number is 7
    //
    //      *              A
    //     ***            U L
    //    *****          U   L
    //   *******   ->   U     L
    //    *****          U   L        location L means LowerBound
    //     ***            U L         location U means UpperBound
    //      *              A          location A means also LowerBound and UpperBound
    // -----------------------------------------------------------------------------------------------------

    // Upper block
    for ( int i=1 ; i < MidLine ; i++ )
    {
        for ( int j=1 ; j <= n ; j++ )
        {
            // Beacuse the Bound value are different at each row
            // This following two variable must be reset by variable MidLine in the internal loop
            LowerBound = MidLine + i - 1;
            UpperBound = MidLine - i + 1;

            // Check the column value to make sure which character should be output
            if ( (j < UpperBound) || (j > LowerBound) )
            {
                cout << " ";
            }
            else
            {
                cout << "*";
            }
        }
        cout << endl;
    }

    // Bottom block
    for ( int i=MidLine ; i <= n ; i++ )
    {
        for ( int j=1 ; j <= n ; j++ )
        {
            LowerBound = n - i + MidLine;
            UpperBound = i - MidLine + 1;
            if ( (j < UpperBound) || (j > LowerBound) )
            {
                cout << " ";
            }
            else
            {
                cout << "*";
            }
        }
        cout << endl;
    }
    return 0;
}
