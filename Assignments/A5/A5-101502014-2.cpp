#include <iostream>
using namespace std;
int main()
{
    int n;
    while(cin >> n && n > 0)
    {
        if(n % 2 == 0) //若n為偶數 , 則輸出邊長為n的正方形
        {
            for(int i = 1 ; i <= n ; i++)
            {
                for(int j = 1 ; j <= n ; j++)
                {
                    if(i == 1 || i == n || j == 1 || j == n) //最外圍輸出"*"
                        cout << "*";
                    else //"*"裡面輸出空白
                        cout << " ";
                }
                cout << endl;
            }
        }
        else //若n為奇數 , 則輸出面積為n*n除以2的菱形。
        {
            for(int i = 1 ; i <= n ; i++)
            {
                if(i > n / 2 + 1) //菱形下半部分的倒三角形
                {
                    for(int k = 1 ; k <= i - n / 2 - 1 ; k++)
                        cout << " ";
                    for(int j = 1 ; j <= (n - i) * 2 + 1 ; j++)
                        cout << "*";
                }
                else //菱形上半部分的三角形
                {
                    for(int j = 1 ; j <= n / 2 + 1 - i ; j++)
                        cout << " ";
                    for(int k = 1 ; k <= i * 2 - 1 ; k++)
                        cout << "*";
                }
                cout << endl;
            }
        }
    }
	return 0;
}
