#include <iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    int input;//設定變數

    do{//此迴圈為繼續遊戲
        int x=0,y=0;//設定變數
        cout << endl;//第一行要先空白，公告上的程式輸出結果是這樣吧我應該沒看錯吧???

        srand((unsigned)time(NULL));//隨機抽數
        int a = rand();//a為隨機抽出的第一個數
        x=a%6+1;//a運算後之x才會變成1~6的數字
        cout << "Computer's dice point is: " << x << endl;//第一行要輸出的字串與電腦骰出的數字x
        int b = rand();//b為隨機抽出的第二個數字
        y=b%6+1;//一樣進行運算才會變成1~6
        cout << "Your dice point is: " << y << endl;//第二航要顯示的字串與玩家骰出的數字y

        if(x>y)//電腦贏的結果
            cout << "Sorry, you lose." << endl;
        if(x<y)//玩家贏的結果
            cout << "Congratulation! You win." << endl;
        if(x==y)//平手
            cout << "Draw" << endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit )";//結束後輸出的字串
        cin >> input;//此時再輸入變數input

    }while(input==1&&cin!=false);//由此判斷是否要繼續遊戲

    return 0;
}
