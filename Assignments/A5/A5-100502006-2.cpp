#include<iostream>
#include<string>
#include<cstdlib>
#include<cmath>

using namespace std;

class CPrintGraph//Definition of class CPrintGraph
{
    public:
        void PrintGraph();//Declaration of function PrintGraph which is in void return type.
    private:
        bool Input();
        int iInput;//To store the input number
        void PrintOddGraph();
        void PrintEvenGraph();
};
void CPrintGraph::PrintGraph()//Definition of function PrintGraph
{
    while(Input())//Calling function Input
    {
        if(iInput%2==1)//Determine if iInput is odd or even
        {
            PrintOddGraph();//Calling function PrintOddGraph
        }
        else
        {
            PrintEvenGraph();//Calling function PrintEvenGraph
        }
    }
}
bool CPrintGraph::Input()//Definition of function Input
{
    string str;
    cin>>str;
    if(str=="0")
    {
        return false;
    }
    for(int i=0;i<str.length();i++)//Determine if the input string is number in each character
    {
        if(str.c_str()[i]<'0'||str.c_str()[i]>'9')
        {
            return false;
        }
    }
    iInput=strtol(str.c_str(),NULL,10);//Convert the string into int and store at iInput
    return true;
}
void CPrintGraph::PrintOddGraph()//Definition of function PrintOddGraph
{
    for(int i=1;i<=iInput;i++)//Counting lines
    {
        for(int j=0;j<abs((iInput+1)/2-i);j++)
        {
            cout<<" ";
        }
        for(int k=0;k<iInput-abs((iInput+1)-2*i);k++)//k<2*((iInput+1)/2-abs((iInput+1)/2-i))-1
        {
            cout<<"*";
        }
        cout<<endl;
    }
}
void CPrintGraph::PrintEvenGraph()//Definition of function PrintEvenGraph
{
    for(int i=1;i<=iInput;i++)//Counting lines
    {
            if(i==1||i==iInput)//Determine wheather it is the first, the last or other line
            {
                for(int j=0;j<iInput;j++)
                {
                    cout<<"*";
                }
                cout<<endl;
            }
            else
            {
                cout<<"*";
                for(int k=0;k<iInput-2;k++)
                {
                    cout<<" ";
                }
                cout<<"*"<<endl;
            }
    }
}
int main()
{
    CPrintGraph PGraph;//Declaration of class CPrintGraph
    PGraph.PrintGraph();//Call the function PrintGraph which is in PGraph
    return 0;
}

