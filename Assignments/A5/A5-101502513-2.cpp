#include<iostream>
#include<iomanip>
#include<cmath>
using namespace std;

int main()
{
    int number;

    while( cin >> number )
    {
        if( number <= 0 ) //number can't be negative or zero
            break;
        else if( number % 2 == 1 ) //if number is odd
        {
            for( int i = 1 ; i <= number ; i++ ) //output rhombus
            {
                if( i <= number / 2 + 1 )
                { //upper part of rhombus
                    for( int j = (number / 2 + 1) - i ; j >= 1 ; j-- )
                        cout << " ";
                    for( int j = 1 ; j <= i * 2 - 1 ; j++ )
                        cout << "*";
                    cout << endl;
                }
                else
                { //downer part of rhombus
                    for( int j = i ; j > number / 2 + 1 ; j-- )
                        cout << " ";
                    for( int j = i * 2 ; j <= number * 2 ; j++ )
                        cout << "*";
                    cout << endl;
                }
            }
        }
        else if( number % 2 == 0 ) //if number is even
        {
            for( int a = 1 ; a <= number ; a++ ) //output square
            {
                if( a == 1 || a == number )
                { //upper part of square
                    for( int b = 1 ; b <= number ; b++ )
                        cout << "*";
                    cout << endl;
                }
                else
                { //downer part of square
                    cout << "*";
                    cout << setw(number-1) << "*";
                    cout << endl;
                }
            }
        }
    }
    return 0;
}
