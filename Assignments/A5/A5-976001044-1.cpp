// Assignment #5-1
// A5-976001044-1.cpp
#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;
int SizeComparison(int,int);

int main()
{
    // Initialize the random number table
    srand((unsigned)time(NULL));

    int DicePlayer=0, DiceComputer=0;
    int ExecuteContrl=0;

    // Assign the random number to this two variable
    // And set the range from 1 to 6 ( 1, 2, 3, 4, 5, 6 )
    DicePlayer = rand()%6 + 1;
    DiceComputer = rand()%6 + 1;

    // Using this two arguments to call function to comparison the Dice number
    SizeComparison(DicePlayer,DiceComputer);

    cout << "Want to play again? ( 1 for continue, 0 for exit ) ";

    // When the input data is integer 1, repeat the instruction in While Loop
    while ( (cin >> ExecuteContrl) && (ExecuteContrl == 1) )
    {
        cout << endl;

        // Assign the random number to this two variable again to reset teh value
        // And also set the range from 1 to 6 ( 1, 2, 3, 4, 5, 6 )
        DicePlayer = rand()%6 + 1;
        DiceComputer = rand()%6 + 1;

        // Call function to comparison the Dice number
        SizeComparison(DicePlayer,DiceComputer);

        // Prompt the flow-control message
        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
    }

    system("pause");
    return 0;
}


// Functional phase
// Comparison Dice number ; y as computer's, and x as player's ( user's )
int SizeComparison(int x, int y)
{
    cout << "Computer's dice point is: " << y << endl;
    cout << "Your dice point is: " << x << endl;
    if ( x > y )
    {
        cout << "Congratulation! You win." << endl;
    }
    else if ( x < y )
    {
        cout << "Sorry, you lose." << endl;
    }
    else
    {
        cout << "Draw" << endl;
    }
    return 0;
}
