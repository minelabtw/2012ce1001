//Assignment 5-1
//Input : first enter without any input, than ask for repetition
//Output : rolling dice, compare user's point with computer's point, than display the message
#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std; /*include all the head file that may be used in the program, and using the namespace "std::"*/

int main() /*the main part of the program*/
{
    int cp,up,r; /*declare all the variable*/
    srand((unsigned)time(NULL)); /*use srand() function with time input to produce a series by random number*/
    for (int i=1;i>=0;i++) /*this for loop can let the users to run the program again and again, until he key in the "0".*/
    {
        cp = rand()%6+1; /*produce a random number and define to the "cp"(computer's point)*/
        up = rand()%6+1; /*than another to "up"(user's point), both are divided by 6 than plus 1. this can make sure that every number will be 1~6*/
        cout << "Computer's dice point is: " << cp << endl;
        cout << "Your dice point is: " << up << endl;
        if (cp>up) /*use if... else if... else to decide what will be displayed on the screen.*/
        cout << "Sorry, you lose.\n";
        else if (cp<up)
        cout << "Congratulation! You win.\n";
        else
        cout << "Draw.\n";
        for (int j=1;j>=0;j++) /*this for loop can let the user to decide to play again(1) or exit(0), if not 1 or 0, reask nutil the input can be recognize*/
        {
            cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
            cin >> r;
            if (r == 1)
            break;
            else if (r == 0)
            return -1;
            else;
        }
    }
    return 0;
}
