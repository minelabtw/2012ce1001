#include <iostream>

using namespace std;

int main()
{
    int n;
    while (cin >> n)
    {
        if (n <= 0)
            break;
        else
        {
            if (n%2 != 0)//判斷為奇數則列出菱形
            {
                for (int i = 1 ; i <= n ; i++)//上三角形(行)
                {
                    for (int j = 1 ; j <= n - i ; j++)//上三角形(列)
                        cout << " ";
                    for (int j = 1 ; j <= i * 2 - 1 ; j++)//上三角形(列)
                        cout << "*";
                    cout << endl;
                }
                for (int i = 1 ; i <= n ; i++)//下三角形(行)
                {
                    for (int j = 1 ; j <= i ; j++)//下三角形(列)
                        cout << " ";
                    for (int j = 1 ; j <= 2 * (n - i) - 1 ; j++)//下三角形(列)
                        cout << "*";
                    cout << endl;
                }
            }

            else//判斷為偶數則列出空心矩形
            {
                for (int k = 1 ; k <= n ; k++)//矩形頂邊
                    cout << "*";
                cout << endl;
                for (int i = 1 ; i <= n - 2 ; i++)//矩形行數(扣掉頂邊及底邊)
                {
                    cout << "*";//矩形側邊(左)
                    for (int i = 1 ; i <= n - 2 ; i++)//製造中空部份
                    cout << " ";
                    cout << "*";//矩形側邊(右)
                    cout << endl;
                }
                for (int k = 1 ; k <= n ; k++)//矩形底邊
                    cout << "*";
                cout << endl;
            }
        }
    }

    return 0;
}
