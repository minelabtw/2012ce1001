//A5-2 by J
//no additional function.
//a drawing promgram again,I want to find its drawing paradigm and "translate" it into c++ code
#include <iostream>
#include <iomanip>
using namespace std ;
int main()
{
    int n=0 , str=1 , spc=0 , ctr=1 ;
    //n is for input ; str as star(*) ;spc as space ; ctr as counter

    while ( cin>>n && n>0 )     //the situation that program's going to execute
    {
        if ( n%2==0 )       //for n is an even number
        {
            while ( ctr<=n )        //this loop is design for top level
            {
                ctr++;
                cout << "*" ;
            }
            cout << endl ;
            ctr=3;  //for drawing middle part precisely , ctr reset to 3
            while ( ctr<=n)
            {
                cout << "*" << setw(n-1) << "*" << endl ;
                ctr++ ;
            }
            ctr=1;      //ctr reset
            while ( ctr<=n )        //this loop is design for lowest level
            {
                ctr++;
                cout << "*" ;
            }
            cout << endl ;      //graph over. change to the next line
            ctr=1 ;     //ctr reset for next input
        }
        //when n is an even number , it's easy : 1.the top & lowest level's star number is
        //the same as input value. 2.the middle part of the graph is (n-2) layer of "*   *".
        //
        else        //n is an odd number here
        {
            while ( ctr<=((n+1)/2) )        //use ctr to show which level it is . This loop draws the upper half inclusive of the middle one
            {
                spc=((n+1)/2)-ctr ;
                str=2*ctr-1 ;
                while ( spc>0 )     //draw space
                {
                    cout << " " ;
                    spc-- ;
                }
                while ( str>0 )     //draw star
                {
                    cout << "*" ;
                    str-- ;
                }
                cout << endl ;
                ctr++ ;     //these 2 statement is to draw the next level
            }

            while ( ctr<=n )     //draw the rest part of this graph
            {
                spc=ctr-((n+1)/2) ;
                str=2*(n-ctr)+1 ;
                while ( spc>0 )
                {
                    cout << " " ;
                    spc-- ;
                }
                while ( str>0 )
                {
                    cout << "*" ;
                    str-- ;
                }
                cout << endl ;
                ctr++ ;     //these 2 statement is to draw the next level
            }
            ctr=1;
        }
        //when n is an odd number , I want to use 2 variables (spc&str) to darw it .
        //ex:when n=5, 1st level draw 2 spc then 1 str ,2nd level draw 1 spc 3 str...etc
        //there's a regularity in this thinking way.
    }

    return 0 ;
}
