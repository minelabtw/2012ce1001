//A5-1 by J
//No additional function
//This program seems easy. I just want to design 2 random variables for win/lose. 1 variable
//to keep game or not.
#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std ;
int main()
{
    srand((unsigned)time(NULL));
    int a=rand() , b=rand() , inp=1 ;

    cout << "Computer's dice point is: " << a%6+1 << endl ;     //These are for 2
    cout << "Your dice point is: " << b%6+1 << endl ;       //sentence above.

    if ( (a%6+1)>(b%6+1) )      //These if statement judge for 3rd sentence to show.
        cout << "Sorry, you lose." << endl ;
    else if ( (a%6+1)==(b%6+1) )
        cout << "Draw" << endl ;
    else
        cout << "Congratulation! You win." << endl ;

    cout << "Want to play again? ( 1 for continue, 0 for exit ) " ;

    while ( cin>>inp && inp==1 )
    {
        int a=rand() , b=rand() ;
        cout << endl ;
        cout << "Computer's dice point is: " << a%6+1 << endl ;     //These are for 2
        cout << "Your dice point is: " << b%6+1 << endl ;       //sentence above.

        if ( (a%6+1)>(b%6+1) )      //These if statement judge for 3rd sentence to show.
            cout << "Sorry, you lose." << endl ;
        else if ( (a%6+1)==(b%6+1) )
            cout << "Draw" << endl ;
        else
            cout << "Congratulation! You win." << endl ;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) " ;
    }

    return 0 ;      //End of this program.
}
