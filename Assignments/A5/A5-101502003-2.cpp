#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int a;
    while (cin>>a) //如果a為整數則進入迴圈,如果為非數字則結束程式
    {
        if(a<0) //如果a<0則直接結束程式
            return 0;
        else
        {
            if (a%2==1)  //如果是奇數,則進行下列指令(上半部的三角形)
            {
                for( int i=1 ; i<=a ; i=i+2 ) //如果滿足以上條件則進入下面迴圈
                {
                    for( int j = 1 ; j <= a -i ; j=j+2 ) //輸入空白的條件
                        cout << " ";
                    for( int j = 1 ; j <= i  ; j++ ) //輸入*的條件
                            cout << "*";
                            cout << endl;
                }
                for (int i=a-2 ; i>=1 ; i=i-2)//下半部的三角形 (倒過來算)
                {
                    for (int j=1; j<=a-i; j=j+2 ) //輸入空白的條件,和上半部三角形一樣
                        cout << " ";
                    for (int j=1;j<=i;j++) //輸入*的條件,和上半部三角形一樣
                        cout << "*";
                        cout << endl;

                }
            }
            else //如果輸入的是偶數,則進行下列指令
            {
                for(int j=1;j<=a;j++) //矩形的第一行:要有a個*
                        cout << "*" ;
                cout << endl; //換行
                for (int j=1; j<=a-2;j++) //中間部分
                {
                    cout << "*"; //先輸入*
                    for (int i=1;i<=a-2;i++) //在輸入a-2個空白
                    {
                        cout << " ";
                    }
                        cout << "*"; //最後再輸入一個*
                    cout << endl;  //換行,這樣就會有a-2行個*空白*
                }
                for (int i=1;i<=a;i++) //最後一行:a個*
                    cout << "*";
                cout << endl; //下一行,在輸入個值出現下一個值所屬的圖形
            }
        }
    }
    return 0;
}
