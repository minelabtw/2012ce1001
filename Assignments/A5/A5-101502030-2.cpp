#include<iostream>
using namespace std;

int main()
{
    int a ;
    while ( a > 0 )             //當a大於0.
    {
        cin >> a ;
        if ( a <= 0 || cin == false )                       //偵錯.
        {
            break ;
        }
        if ( a % 2 == 0 )          //判定偶數.
        {
            for ( int k = 1 ; k <= a ; k++ )        //蓋子
                cout << "*" ;
                cout << endl ;
            for ( int o = 1 ; o <= a - 2 ; o++ )    //中間的行數.
            {
                cout << "*" ;
                for ( int p = 1 ; p <= a - 2 ; p++ )//中間的空格.
                cout << " " ;
                cout << "*" << endl ;
            }
            for ( int k = 1 ; k <= a ; k++ )        //底線.
                cout << "*" ;
                cout << endl ;
        }
        else if ( a % 2 == 1 )         //判定奇數.
        {
            for ( int h = 1 ; h <= a / 2 + 1 ; h++ )        //上面大三角的高.
            {
                for ( int j = 1 ; j <= ( a - h * 2 + 1 ) / 2 ; j++ )       //大三角的空格.
                    cout << " " ;
                for ( int j = 1 ; j <= h * 2 - 1 ; j++ )        //大三角.
                    cout << "*" ;
                cout << endl ;
            }
            for( int h = 1 ; h <= a / 2 ; h++ )             //下面倒小三角.
            {
                for( int j = 1 ; j <= h ; j++ )             //高.
                    cout << " " ;
                for( int j = 1 ; j <= a - 2 * h ; j++ )      //三角.
                    cout << "*" ;
                cout << endl ;
            }
        }
    }
    return 0;
}
