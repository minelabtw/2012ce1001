#include <iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int main()
{
    int conti = 1; //先使判斷值為1使迴圈能進行
    /*取兩個亂數為1~6的數來比大小
      依據結果輸出不同的訊息
      若輸入1則繼續否則結束*/
    do
    {
        if (conti != 1)
        {
            return 0 ;
        }
        srand((unsigned)time(NULL));
        int number1 = rand();
        int number2 = rand();
        cout << endl << "Computer's dice point is: " << number1%6+1 << endl;
        cout << "Your dice point is: " << number2%6+1 << endl;
        if ( number1%6+1 > number2%6+1)
        {
        cout << "Sorry, you lose. "  << endl;
        }
        if ( number1%6+1 < number2%6+1)
        {
        cout << "Congratulation! You win. " << endl;
        }
        if ( number1%6+1 == number2%6+1)
        {
        cout << "Draw " << endl;
        }
        cout << "Want to play again? ( 1 for continue, 0 for exit ) " ;
    } while ( cin >> conti );
    return 0;
}
