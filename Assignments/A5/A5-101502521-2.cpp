//輸入0正常結束程式

#include <iostream>

using namespace std;

int get_valid_positive_int(void);             //Function to get valid input

int main()
{
    int n;
    cout << "Enter 0 to exit the program!\n";
    while((n=get_valid_positive_int()) != -1)       //檢查輸入
    {
        if(n==0)
            return 0;

        if(n%2==0)                                  //邊長為 n 的空心矩形
        {
            for(int i=0;i<n;i++)                    //上邊
                cout << "*";
            cout << endl;

            for(int row=1;row<=(n-2);row++)         //中間
            {
                cout << "*";                        //左邊

                for(int i=1;i<=(n-2);i++)           //中空
                    cout << " ";

                cout << "*\n";                      //右邊
            }

            for(int i=0;i<n;i++)                    //下邊
                cout << "*";
            cout << endl;
        }
        else                                        //邊長為 n 的空心矩形
        {
            for(int row=0;row<(n+1)/2;row++)        //上三角
            {
                for(int i=0;i<(n-1)/2-row;i++)
                    cout << " ";
                for(int i=0;i<row*2+1;i++)
                    cout << "*";
                cout << endl;
            }

            for(int row=0;row<(n-1)/2;row++)        //下三角
            {
                for(int i=0;i<row+1;i++)
                    cout << " ";
                for(int i=0;i<(n-(row+1)*2);i++)
                    cout << "*";
                cout << endl;
            }
        }

    }
    return -1;
}

int get_valid_positive_int(void)
{
    int n;
    char check;
    if(!(cin >> n)||n<0)                            // Error detect
        return -1;                                  //-1 means invalid input
    check = cin.peek();                             // get next char in istream
    if(check!=' ' && check!='\n' && check!=0)       //Detect invalid char after integer, like "123a"
        return -1;
    return n;
}
