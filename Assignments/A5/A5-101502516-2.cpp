#include <iostream>
using namespace std;

// 程式由此開始
int main()
{
    int a,e,f; // 假設變數
    int b = 1;
    int x = 1;

    while ( x = 1 ) // 當x等於1時進入迴圈
    {
        if ( cin >> a == false || a <= 0 ) // 判斷a是否為正整數
        {
            return -1;
        }

        if ( a % 2 == 1 ) // 如果 n 為奇數，則印出一個面積為 n 平方除以 2 的菱形。
        {
            int d = ( a + 1 ) / 2;
            e = a;

            for ( int b = 1; b <= a; b++ )
            {
                if ( d >= b )
                {
                    for ( int c = 1; c <= d - b; c++ )
                        cout << " ";
                    for ( int c = 1; c <= b * 2 - 1; c++ )
                        cout << "*";
                    cout << endl;
                }
                if ( d < b )
                {
                    f = a * (-1);
                    for ( int c = 1; c <= ( f - 1 ) / 2 + b; c++ )
                        cout << " ";
                    for ( int c = 1; c <= 2 * e - 2 * b + 1; c++ )
                        cout << "*";
                    cout << endl;
                }
            }
        }

        if ( a % 2 == 0 ) // 如果 n 為偶數，則印出一個邊長為 n 的空心矩形。
        {
            for ( int b = 1; b <= a; b++ )
            {
                if ( b == 1 || b == a )
                {
                    for ( int c = 1; c <= a; c++ )
                        cout << "*";
                    cout << endl;
                }
                else
                {
                    for ( int c = 1; c <= a; c++ )
                        if ( c == 1 || c == a )
                            cout << "*";
                        else
                            cout << " ";
                    cout << endl;
                }
            }
        }
    }

    return 0; // 結束並回報
}
