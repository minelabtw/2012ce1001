#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int main()
{
    int c;
    do//進入迴圈
    {
        cout<<endl;//與下次輸出的間距

        srand((unsigned)time(NULL));
        //In order to generate random-like numbers, we need some value which changes every second.
        //like the value returned by the function "time"
        //And "srand((unsigned)time(NULL));" is distinctive enough for most trivial randoming needs.

        int a=1+rand()%6;//a between 1 and 6
        int b=1+rand()%6;//b between 1 and 6

        cout<< "Computer's dice point is: "<<a<<endl;
        cout<< "Your dice point is: "<<b<<endl;

        if(a>b)
            cout<< "Sorry, you lose."<<endl;
        if(a<b)
            cout<< "Congratulation! You win."<<endl;
        if(a==b)
            cout<< "Draw."<<endl;
        //是否再執行
        cout<< "Want to play again? ( 1 for continue, 0 for exit )";

    }while(cin>>c and c==1);//重回迴圈

    return 0;
}
