#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    int integer;

    while( cin >> integer && integer > 0 )  //如果輸入負數或零或字元則結束程式。
    {
        if (integer%2==0)   //even
        {
            for( int i=1 ; i <= integer ; i++ )  //第一行*
                cout << "*";

            cout << endl;

            for( int i=1 ; i <= integer-2 ; i++ )   //第二行到第n行*
            {
                cout << "*";
                for( int k=1 ; k <= integer-2 ; k++ )   //印出空白
                    cout << " ";
                cout << "*" << endl;
            }

            for( int i=1 ; i <= integer ; i++)  //最後一行*
                cout << "*";

            cout << endl;
        }
        else    //odd
        {
            for( int i=1 ; i<=integer ; i++ )
            {
                for( int j = 1 ; j <= fabs(( integer/2 + 1 ) - i ) ; j++ ) //印出空白
                    cout << " ";
                int space = fabs( (integer/2)+1 - i ) ;     //算出該行有多少空白

                for( int k = 1 ; k <= (integer - 2*space) ; k++ )   //印出 "*"
                    cout << "*";
                cout << endl;
            }
        }
    }

    return 0;
}
