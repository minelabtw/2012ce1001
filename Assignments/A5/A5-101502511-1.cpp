#include <iostream>
#include <cstdlib>
#include <time.h>
using namespace std;

int main()
{
    int c;

    do//先執行一次程式內容
    {
        int a=rand(),b=rand();//設定亂數

        srand((unsigned)time(NULL));//重新設定一個亂數種子

        cout << "\nComputer's dice point is: "<< a%6+1 <<endl;
        cout << "Your dice point is: "<< b%6+1 <<endl;

        if ((a%6+1)>(b%6+1))//判斷大小
            cout << "Sorry, you lose."<<endl;
        else if ((a%6+1)<(b%6+1) )
            cout << "Congratulation! You win."<<endl;
        else if ((a%6+1)==(b%6+1))
            cout << "Draw"<<endl;
        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
    }
    while(cin>>c&& c==1);//輸入1時繼續執行程式

    return 0;
}
