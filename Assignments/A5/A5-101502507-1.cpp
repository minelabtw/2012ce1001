//compare two random number
#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    int c;//entered by user to decide whether continue or not
    do
    {
    srand((unsigned)time(NULL));
    int a = rand();//a is a random number
    int b = rand();//b is a random number

    cout << "Computer's dice point is: " << a%6+1 << endl;//a is a random number which between 1 and 6
    cout << "Your dice point is: " << b%6+1 << endl;//b is a random number which between 1 and 6

    if( a%6+1 > b%6+1 ) cout << "Sorry, you lose." << endl;//computer > user LOSE
    if( a%6+1 == b%6+1 ) cout << "Draw" << endl;//computer = user DRAW
    if( a%6+1 < b%6+1 ) cout << "Congratulation! You win." << endl;//computer < user WIN

    cout << "Want to play again? ( 1 for continue, 0 for exit ) ";//decide whether continue or not

    }while( cin >> c != false && c == 1 );//while the user enter 1 then ontinue, others end the program

    return 0;
}
