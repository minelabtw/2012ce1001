#include <iostream>
using namespace std;
int main()
{
    int a,i,j,k;
    cin >> a;

    while( cin != false && a > 0)
    {
        if( a%2 == 1)//奇數
        {
            for( i = 1; i <= (a+1)/2 ; i++ )//分為上下兩部分此為上部分1~a
            {
                for( j = ((a+1)/2)-i ; j > 0 ; j = j-1 )
                    cout << " ";
                for( j = (i*2)-1 ; j <= a && j > 0 ; j = j-1 )
                    cout << "*";
                cout << endl;
            }
            for( i = (a-1)/2 ; i <= (a-1)/2 && i>0 ; i = i-1 )//分為上下兩部分此為下部分a-2~1
            {
                for( j = ((a+1)/2)-i ; j <= (a-1)/2 && j > 0 ; j = j-1 )
                    cout << " ";
                for( j = (2*i)-1 ; j < a && j > 0 ; j = j-1 )
                    cout << "*" ;
                cout << endl;
            }
        }
        else//偶數
        {
            for( i = 1 ; i <= a ; i++ )//分為三部分此為第1列
                cout <<"*";
                cout <<endl;
            for( j = 1 ; j <= a-2 ; j++ )//分為三部分此為第2~a-1列
            {
                cout <<"*";
                    for(i=1;i<=a-2;i++)
                        cout <<" ";
                cout <<"*";
                cout <<endl;
            }
            for( i = 1 ; i <= a ; i++ )//分為三部分此為第a列
                cout <<"*";
            cout << endl;
        }
        cin >>a;
    }
    return -1;
}
