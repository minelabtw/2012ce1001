#include <iostream>

using namespace std;

int main()
{
    int n;//宣告一整數n

    while ( n > 0 )//當n大於0時,進入迴圈
    {
        if ( cin >> n == false )//錯誤輸入則結束程式
            return -1;

//////////////////輸入為偶數時//////////////////

        if ( n % 2 == 0 )
        {
            for ( int i = 0 ; i < n ; i++ )//輸出空心正方形
            {
                for ( int j = 0 ; j < n ; j++ )
                {
                    if ( i == 0 || i == n-1 )
                    cout << "*";

                    else if ( j == 0 || j == n-1 )
                    cout << "*";

                    else
                    cout << " ";
                }
                cout << endl;
            }
        }

//////////////////輸入為奇數時//////////////////

        if ( n % 2 == 1 )
        {
            for ( int i = 1 ; i <= n/2+1 ; i++ )//輸出菱形上半部
            {
                for ( int j = 1 ; j <= (n/2+1) - i ; j++ )
                    cout << " ";
                for ( int j = 1 ; j <= i * 2 - 1 ; j++ )
                    cout << "*";
                cout << endl;
            }

            for ( int i = n/2 ; i >= 1 ; i-- )//輸出菱形下半部
            {
                for ( int j = ( n/2 - i+1 ) ; j >= 1  ; j-- )
                    cout << " ";
                for ( int j = i * 2 - 1 ; j >=1 ; j-- )
                    cout << "*";
                cout << endl;
            }
        }
        cout << endl;
    }
    return 0;

}
