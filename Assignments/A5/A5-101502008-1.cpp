//有做當輸入非0與1的數字時的錯誤偵測

#include <iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int main()
{
    srand((unsigned)time(NULL));
    while(1)
    {
        int a = rand();//a為亂數
        int b = rand();//b為亂數
        int A,B,c=0;
        A=(a%6)+1;//a換成骰子的點數
        B=(b%6)+1;//b換成骰子的點數
        cout<<"Computer's dice point is: "<<A<<"\n";
        cout<<"Your dice point is: "<<B<<"\n";
        if (A>B)
        {
            cout<<"Sorry, you lose.\n";
        }
        if (B>A)
        {
            cout<<"Congratulation! You win.\n";
        }
        if (B==A)
        {
            cout<<"Draw\n";
        }
        cout<<"Want to play again?( 1 for continue, 0 for exit )";
        cin>>c;//輸入0 or 1
        cout<<"\n";
        if (c==0)
        {
            return 0;
        }
        if(c!=0 &&c!=1)//輸入非零與一的數字的錯誤偵錯
        {
            cout<<"Error!! The game will be closed!!";
            return 0;
        }
    }
    return 0;
}
