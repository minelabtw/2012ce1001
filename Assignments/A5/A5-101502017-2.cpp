#include <iostream>

using namespace std;

int main()
{
    int a,b,n,x,y,z;

    while(cin >> n && n > 0)
    {
        if(n%2==1)//如果 n 為奇數，則印出一個面積為 n 平方除以 2 的菱形。
        {
            for(a=1;a<=n/2+1;a++)//上金字塔
            {
                for(b=1;b<=n-a;b++)
                    cout << " ";
                for(b=1;b<=a*2-1;b++)
                    cout << "*";
                cout <<endl;
            }
            for(a=1;a<=n/2;a++)//下金字塔
            {
                for(b=1;b<=a+n/2;b++)
                    cout << " ";
                for(b=1;b<n-(a*2-1);b++)
                    cout << "*";
                cout <<endl;
            }
        }

        if(n%2==0)//如果 n 為偶數，則印出一個邊長為 n 的空心矩形。
        {
            for(x=1;x<=n;x++)//上邊
            {
                cout <<"*";
            }
            cout <<endl;
            for(y=1;y<=n-2;y++)//中部
            {
                cout <<"*";
                for(z=1;z<=n-2;z++)
                    cout <<" ";
                cout <<"*"<<endl;
            }
            for(x=1;x<=n;x++)//下邊
            {
                cout <<"*";
            }
            cout <<endl;
        }
    }
    return 0;
}
