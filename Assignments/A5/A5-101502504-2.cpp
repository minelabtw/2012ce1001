//101502504
#include <iostream>//allows program to output data to the screen
using namespace std;
int main()
{
	int a,i,j,k;//variable declaration

    while ( cin >> a && a > 0 )//cin a > 0
    {
        if ( a%2==0 )//if a is even
	{
		for ( i=1; i<=a; i++)//the first line ~ there are a*
			cout << "*";
        cout << endl;
		for ( j=1; j<=a-2; j++)//middle
		{
			cout << "*";
			for (k=1;k<=a-2; k++)
				cout << " ";
			cout << "*" << endl;
		}
		for ( i=1; i<=a; i++)//the last line ~ there are a*
			cout << "*";
        cout << endl;
	}

        if ( a%2==1 )//if a is odd
	{
		for( i=1; i<=(a+1)/2; i++)//the height is 1 to a
		{
			for (j=1; j<=(a+1)/2-i; j++)
				cout << " ";
			for (j=1; j<=2*i-1; j++)
				cout << "*";
            cout << endl;
		}
		for( i=(a-1)/2; i>=1; i--)//the height is (a-1) to 1
		{
			for (j=(a+1)/2-i; j>=1; j--)
				cout << " ";
			for (j=2*i-1; j>=1; j--)
				cout << "*";
            cout << endl;
		}
	}
    }

	return 0;
}//end main
