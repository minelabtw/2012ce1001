#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int main()
{
    int i;

    while (1)
    {
        srand((int)time(NULL));//製造出亂數

        int num1 = rand();
        int num2 = rand();

        cout << "Computer's dice point is: " << num1%6+1 << endl;
        cout << "Your dice point is: " << num2%6+1 << endl;

        if (num1%6+1 > num2%6+1)
            cout << "Sorry, you lose." << endl;

        else if (num1%6+1 < num2%6+1)
            cout << "Congratulation! You win." << endl;

        else
            cout << "Draw" << endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";

        if ((cin >> i) == false)//錯誤偵測
            return 0;

        if (i != 1)//錯誤偵測
            return 0;
    }//end while

    return 0;
}//end main
