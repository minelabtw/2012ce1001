#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int main()
{
    int m;
    do
    {
        srand((unsigned)time(NULL));
        //In order to generate random-like numbers, we need some value which changes every second.
        //like the value returned by the function "time"
        //And "srand((unsigned)time(NULL));" is distinctive enough for most trivial randoming needs.
        int a = rand(),b = rand();//產生亂數a,b
        a = a % 6 + 1;//將a的範圍限制在1~6
        b = b % 6 + 1;//將b的範圍限制在1~6
        cout<<"\nComputer's dice point is: "<<a<<endl;
        cout<<"Your dice point is: "<<b<<endl;

        if(a>b)//若a>b,顯示"Sorry, you lose."
        {
            cout<<"Sorry, you lose."<<endl;
        }
        else if(a<b)//若a<b,顯示"Congratulation! You win."
        {
            cout<<"Congratulation! You win."<<endl;
        }
        else//若a=b,顯示"Draw"
        {
            cout<<"Draw"<<endl;
        }
        cout<<"Want to play again? ( 1 for continue, 0 for exit ) ";

    }
    while(cin>>m && m==1);//輸入一個數值m,若m=1則再次進入迴圈否則結束程式
    return 0;
}
