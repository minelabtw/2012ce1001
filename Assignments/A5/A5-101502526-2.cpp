#include <iostream>

using namespace std;

int main()
{
    int number , star, side , space , line ;
    while ( cin >> number ) //重複輸入
    {
        if ( number <= 0 || cin == false )  //若負值或非數字則結束迴圈
        {
            return 0;
        }
        /*若為偶數，
          先設一個有number層的迴圈，
          設定上下兩邊的情況，裡面迴圈為輸出number個"*"
          設定其他層的情況，若line最左邊或最右邊則輸出"*"
          否則輸出空白                                  */
        if ( number % 2 == 0)
        {
            for ( side = 1 ; side <= number ; side ++)
            {
                if ( side == 1 || side == number)
                {
                    for ( star = 1 ; star <=number ; star ++)
                        cout << "*";
                }
                if ( side > 1 && side < number )
                {
                    for ( line = 1 ; line <= number ; line ++)
                    {
                        if ( line == 1 || line == number )
                            cout << "*";
                        else
                            cout << " ";
                    }
                }
                cout << endl;
            }
        }
        /*若為奇數的情況，
          先設一個由上往下遞增的"*"層
          設兩個迴圈，第一個迴圈先輸出空白，
          第二個迴圈輸出"*"，此兩個迴圈跑完則跳下一行
          後設一個由上往下遞減的"*"層
          設兩個迴圈，第一個迴圈先輸出空白，
          第二個迴圈輸出"*"，此兩個迴圈跑完則跳下一行
         */
        if ( number % 2 == 1)
        {
            for ( side = 1 ; side <= number/2 +1 ; side ++)
            {
                for ( space = 1 ; space <= number /2  +1 - side ; space ++)
                    cout << " ";
                for ( star = 1 ; star <= side *2-1 ; star ++)
                    cout << "*";
                cout << endl ;
            }
            for ( side = 1 ; side <= number/2 +1 ; side ++)
            {
                for ( space = number  ; space > number-side ; space --)
                    cout << " ";
                for ( star = number  ; star > side *2 ; star --)
                    cout << "*";
                cout << endl;
            }
        }
    }
    return 0;
}
