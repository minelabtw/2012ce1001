#include<iostream>
using namespace std;

int main()
{
    int number;

    while (1) //做個可重複輸入的無限迴圈
    {
        cin >> number;
            if (cin==false || number<0) //若輸入字元或負數則跳出迴圈
                break;

        if (number==1)
            cout << "*" <<endl;

        else if (number%2!=0) //若輸入奇數則輸出一個菱型
        {
            for (int i=1 ; i<=(number+1)/2 ; i++) //此為菱形之上半部
            {
                for (int j=1 ; j<=(number+1)/2-i ; j++)
                    cout << " ";
                for (int j=1 ; j<=i*2-1 ; j++)
                    cout << "*";
                cout << endl;
            }
            for (int i=1 ; i<=(number-1)/2 ; i++) //此為菱形之下半部
            {
                for (int j=1 ; j<=i ; j++)
                    cout << " ";
                for (int j=1 ; j<=number-2*i ; j++)
                    cout << "*";
                cout << endl;
            }
        }

        else if (number%2==0) //若輸入偶數則輸出一個矩形
        {
            for (int i=1 ; i<=number ; i++) //此為矩形之上底
                cout << "*";
            cout << endl;
            for (int i=1 ; i<=(number)-2 ; i++) //此為矩形之左右兩邊及中間空心部份
            {
                cout << "*" ;
                for (int j=1 ; j<=(number)-2 ; j++)
                    cout << " ";
                cout << "*" << endl;
            }
            for (int i=1 ; i<=number ; i++) //此為矩形之下底
                cout << "*";
            cout << endl;
        }
    }

    return 0;
}
