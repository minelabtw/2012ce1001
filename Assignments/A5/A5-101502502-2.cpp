//有可以偵測"邊長為零的正方形"的錯誤
#include<iostream>
using namespace std;

int main()
{
    int num;

    while(1)//重複輸入
    {
        if ((cin >> num) == false)//錯誤偵測
            return 0;

        if (num < 0)//錯誤偵測
            return 0;

        if (num == 0)//沒有邊長為零的正方形
        {
            cout << "there's no square with side=0" << endl;
            return 0;
        }
        //////////////////////////////偶數情況//////////////////////////////////

        if (num % 2 == 0)
        {
            for (int i = 1 ; i <= num ; i++)//輸出正方形上面的邊長
                cout << "*";
            cout << endl;

            for (int i = 2 ; i < num ; i++)//輸出正方形中間包含"*"號以及空格的部分
            {
                cout << "*";
                for (int j = 1 ; j <= num - 2 ; j++)
                    cout << " ";
                cout << "*" << endl;
            }//end for


            for (int i = 1 ; i <= num ; i++)//輸出正方形下面的邊長
                cout << "*";
            cout << endl;
        }//end if

        ///////////////////////////////奇數情況//////////////////////////////////

        else if (num % 2 == 1)
        {
            for (int i = 1 ; i <= num / 2 + 1 ; i++)//輸出上面正的三角形 包含最中間的那一行"*"號
            {
                for (int j = 1 ; j <= (num / 2) + 1 - i ; j++)
                    cout << " ";

                for (int j = 1 ; j <= i * 2 - 1 ; j++)
                    cout << "*";

                cout << endl;
            }//end for

            for (int i = num / 2 + 2 ; i <= num ; i++)//輸出下面倒的三角形
            {
                for (int j = 1 ; j <= i % (num / 2 + 1) ; j++)
                    cout << " ";
                for (int j = 1 ; j <= 2 * (num - i + 1) - 1 ; j++)
                    cout << "*";
                cout << endl;
            }//end for

        }//end else if

    }//end while

    return 0;

}//end main
