#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int n;

    while ( cin>>n && n>0)//重複輸入的迴圈
    {   /*偶數時跑出邊長n的矩形*/
        if ( n%2 ==0)
        {
            for (int i=1; i<=n; i++)//執行n次
            {
                if (i==1||i==n)
                {
                    for (int j=1; j<=n; j++)
                        cout <<"*";
                }
                else
                    cout << "*"<<setw(n-1)<<"*";//空心邊長
                cout<<endl;
            }
        }
        else
        {   /*奇數時跑出面積為n^2/2的菱形 */
            for (int i=1; i<=n; i++)//執行n次
            {
                if (i<=(n/2)+1)//上半部正的星星塔
                {
                    for (int j=1; j<=(n/2)+1-i; j++)
                        cout << " ";
                    for (int k=1; k<=i*2-1; k++)
                        cout << "*";
                    cout <<endl;
                }
                else//下半部倒星星塔
                {
                    for (int j=1; j<=(n/2)-(n-i); j++)
                        cout << " ";
                    for (int k=1; k<=(n+1-i)*2-1; k++)
                        cout << "*";
                    cout <<endl;
                }
            }
        }
    }
    return 0;
}
