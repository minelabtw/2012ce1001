#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;//簡化std
int main()
{
    int c=1;//c用來判斷是否重複執行
    while(c==1)
    {
        srand((unsigned)time(NULL));
        int a = rand(),b=rand();//a,b皆為隨機數

        cout<<"Computer's dice point is: "<<a%6+1<<endl;//輸出1-6的數
        cout<<"Your dice point is: "<<b%6+1<<endl;
        if(a%6+1>b%6+1)//若前者大於後者則輸出電腦獲勝
            cout<<"Sorry, you lose."<<endl;
        else if(a%6+1<b%6+1)//若後者大於前者則使用者獲勝
            cout<<"Congratulation! You win."<<endl;
        else
            cout<<"Draw"<<endl;//相等則平手
        cout<<"Want to play again? ( 1 for continue, 0 for exit ) ";//重複輸入

        if(cin>>c == false)//錯誤輸入則結束
            return 0;

    }
    return 0;

}
