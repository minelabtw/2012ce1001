#include <iostream>
using namespace std;
int main ()
{
    int a;
    do//執行下列程式碼，執行完畢符合條件，繼續執行
    {
        cin >> a;
        //當a是奇數時，把菱形拆成上下兩部分，用兩個迴圈執行
        if(a%2==1)
        {
            for(int b=1;(a+1)/2>=b;b++)//第一個迴圈，上半部
            {
                for(int c=1 ; c<=(a+1)/2-b ; c++)//印空白
                {
                    cout << " ";
                }
                for(int c=1 ; c<=b*2-1 ; c++)//印星星
                {
                    cout << "*";
                }
                cout << endl;
            }

            for(int b=1;a/2>=b;b++)//第二個迴圈，印下半部
            {
                for(int c=1; c<=b;c++)//印空白
                {
                    cout << " ";
                }
                for(int c=1 ; c<=a-2*b ; c++)//印星星
                {
                    cout << "*";
                }
                cout << endl;
            }
        }
        //當a是偶數時，將空心矩形拆成三部分，用三個迴圈執行
        if(a%2==0)
        {
            for(int c=1;a>=c;c++)//印出最上層的星星
            {
                cout <<"*";
            }
            cout << endl;

            for(int b=2;a>b;b++)
            {
                cout<<"*";//印出最左邊的星星
                for(int c=1;a>c+1;c++)//將中間空心部分印出
                {
                    cout<<" ";
                }
                cout<<"*";//印出最右邊的星星
                cout<<endl;
            }

            for(int c=1;a>=c;c++)//印出最下層的星星
            {
                cout <<"*";
            }
            cout << endl;
        }
    }while(a>0);//符合條件，繼續執行
    return 0;//回傳
}
