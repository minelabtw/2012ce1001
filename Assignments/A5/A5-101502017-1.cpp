#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int main()
{
    srand((unsigned)time(NULL));//產生1個跟時間有關的亂數
    int c=1;
    do
    {
        int a =rand(),b =rand ();
        a=a%6+1;
        b=b%6+1;
        cout << "Computer's dice point is: " <<a<< endl;
        cout <<"Your dice point is: "<<b<<endl;
        if(a>b)
            cout <<"Sorry, you lose."<<endl;
        else if(a==b)
            cout <<"Draw."<<endl;
        else if(b>a)
            cout <<"Congratulation! You win."<<endl;

        cout <<"Want to play again? ( 1 for continue, 0 for exit ) ";

    }while(cin >>c && c== 1);//先執行一次,再做判別
    return 0;
}
