#include<iostream>
#include<cstdlib>
#include<time.h>
using namespace std;

int main()
{
    srand((unsigned)time(NULL)); //In order to generate random-like numbers, we need some value which changes every second.
    int c; //A variable to tell whether to play the game again or not
    do
    {
        int a = rand(); //Generate random number and put it in to "a".
        a=a%6+1; //a%6+1 to present the point of dice

        int b = rand();
        b=b%6+1; //b%6+1 to present the point of dice

        cout<<"Computer's dice point is: "<<a<<endl;
        cout<<"Your dice point is: "<<b<<endl;

        if(a<b)
        cout<<"Congratulations! You win."<<endl;
        else if (a==b)
        cout<<"Draw"<<endl;
        else
        cout<<"Sorry, you lose."<<endl;

        cout<<"Want to play again? ( 1 for continue, 0 for exit )";
        cin>>c;
        cout<<"\n";
    }
    while (c==1); //Input 1 to play again
    return 0;
}
