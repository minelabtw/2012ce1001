#include<iostream>

using namespace std;

int main()
{
    int n,m;//設定變數

    while(n>=1)//此迴圈可讓程式繼續
    {
        cin >> n;//輸入變數

        if(cin==false)//若變數為其他字元則跳出程式
        {
            break;
            return -1;
        }

        if(n%2==1)//若n為奇數
        {
            m=n/2+1;//先運算中間最長是第幾行
            for(int i=1;i<=m;i++)//此迴圈為運算上半部直到最長那行的圖形，一行一行運算
            {
                for(int j=1;j<=m-i;j++)//計算一行需要幾個空格
                    cout << " ";
                for(int j=1;j<=i*2-1;j++)//計算一行需要多少星星
                    cout << "*";
                cout << endl;
            }
            for(int i=m-1;i>=1;i--)//此迴圈運算圖形下半部，一行一行運算
            {
                for(int j=m-i;j>=1;j--)//計算一行多少空格
                    cout << " ";
                for(int j=i*2-1;j>=1;j--)//計算一行多少星星
                    cout << "*";
                cout << endl;
            }
        }

        else if(n%2==0)//若n為偶數
        {
            for(int x=1;x<=n;x++)//先跑出第一行要顯示多少星星
                cout << "*";
            cout << endl;
            for(int z=1;z<=n-2;z++)//接著計算一共要幾行
            {
                cout << "*";//第一個先輸出星星
                for(int y=1;y<=n-2;y++)//計算中間幾個空格
                    cout << " ";
                cout << "*";//最後放星星
                cout << endl;
            }
            for(int x=1;x<=n;x++)//此行與第一行相同，輸出n個星星
                cout << "*";
            cout << endl;
        }
    }

    return 0;
}
