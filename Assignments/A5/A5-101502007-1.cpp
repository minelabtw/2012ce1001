
#include <iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    int a,b,n;
    srand((unsigned)time(NULL)); //隨機產生數字
    a=rand();
    cout <<"Computer's dice point is: "<<a%6+1<<endl; //如此可隨機取得1~6
    b=rand();
    cout <<"Your dice point is: "<<b%6+1<<endl; //如此可隨機取得1~6
    if (a%6+1>b%6+1)
    cout <<"Sorry, you lose."<<endl;
    else if (a%6+1<b%6+1)
    cout <<"Congratulation! You win."<<endl;
    else
    cout <<"Draw"<<endl;
    cout <<"Want to play again? ( 1 for continue, 0 for exit ) ";
    cin >>n;
    cout <<endl;
    while (n==1) //判斷是否進入迴圈
    {
        a=rand();
        cout <<"Computer's dice point is: "<<a%6+1<<endl;
        b=rand();
        cout <<"Your dice point is: "<<b%6+1<<endl;
        if (a%6+1>b%6+1)
        cout <<"Sorry, you lose."<<endl;
        else if (a%6+1<b%6+1)
        cout <<"Congratulation! You win."<<endl;
        else
        cout <<"Draw"<<endl;
        cout <<"Want to play again? ( 1 for continue, 0 for exit ) ";  //輸入1則進入迴圈,輸入0或其他數則跳出迴圈
        cin >>n;
        cout <<endl;
    }
        return 0;
}
