#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;
int main()
{
    int again = 1 , com , you; //初始again的值為1 -> 繼續遊戲 , com紀錄電腦方的骰子數字 , you紀錄玩家方的骰子數字
    do
    {
        srand((unsigned)time(NULL)); //亂數表
        com = rand() % 6 + 1; //取亂數 1 ~ 6
        you = rand() % 6 + 1; //取亂數 1 ~ 6
        cout << "Computer's dice point is: " << com << endl;
        cout << "Your dice point is: " << you << endl;
        if(com > you) //電腦方的骰子數字比玩家方的骰子數字大
            cout << "Sorry, you lose." << endl;
        else if(com == you) //電腦方的骰子數字和玩家方的骰子數字一樣大
            cout << "Draw" << endl;
        else //電腦方的骰子數字比玩家方的骰子數字小
            cout << "Congratulation! You win." << endl;
        cout << "Want to play again? ( 1 for continue, 0 for exit ) "; //判斷是否要繼續遊戲
    }while(cin >> again && again == 1); //again的值為1時 , 則繼續遊戲
	return 0;
}
