//enter odd number then show diamond, enter even number then show hollow square
#include <iostream>

using namespace std;

int main()
{
    int n,i,j;
    n = 0;//return n
    cin >> n;//number entered by user

    if( n == false || n <= 0 ) return 1;//if enter number<=0 or not a number then end the program

    do
    {
        if( n % 2 != 0 )//n is odd
        {
            for( i = 1 ; i <= (n+1)/2 ; i++ )//the upper part of the diamond
            {
                for( j = (n+1)/2-i ; j > 0 ; j-- ) cout << " ";//print space

                for( j = 1 ; j <= i*2-1 ; j++ ) cout << "*";//print *

                cout << endl;
            }//for end

            for( i = 1 ; i <= n-(n+1)/2 ; i++ )//the lower part of the diamond
            {
                for( j = 0 ; j < i ; j++ ) cout << " ";//print space

                for( j = n-(2*i) ; j > 0 ; j-- ) cout << "*";//print *

                cout << endl;
            }//for end
        }//if end

        if( n % 2 == 0 )//n is even
        {
            for( i = 1 ; i <= n ; i++ ) cout << "*";

            cout << endl;

            for( i = 2 ; i <= n-1 ; i++ )
            {
                cout << "*";
                for( j = 2 ; j < n ; j++ ) cout << " ";
                cout << "*";
                cout << endl;
            }//for end

            for( i = 1 ; i <= n ; i++ ) cout << "*";

            cout << endl;
        }//if end


        }while( cin >> n != false && n > 0 );//do-while end

    return 0;

}//main end
