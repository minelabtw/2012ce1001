#include<iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    srand((unsigned)time(NULL));
    int n3;
    do
    {
        int n1=rand(),n2=rand();
        cout<<"Computer's dice point is : "<<n1%6+1<<endl;
        cout<<"Your dice point is : "<<n2%6+1<<endl;
        if (n1%6+1>n2%6+1)
            cout<<"Sorry, you lose.\n";//to judge whether you win or not
        else if (n1%6+1==n2%6+1)
                cout<<"Draw\n";//to judge whether your dice point and computer's point are the same
             else
                cout<<"Congratulation! You win.\n";//to judge whether you win or not
        cout<<"Want to play again? ( 1 for continue, 0 for exit ) ";
        cin>>n3;
    }while(n3==1);//Let the loop run again when you key in 1.
    return 0;
}
