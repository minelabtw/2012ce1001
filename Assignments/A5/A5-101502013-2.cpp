#include <iostream>

using namespace std;

int main()
{
    /*
    我把*號一行一行的增加 再縮小 如下
      *
     ***
    *****
     ***
      *

    */
    int input,l,w,n,b=1; //l決定*寬度 w決定空格寬度 n是計數器 b決定菱形的縮放
    double input2; //檢查小數位
    while (1)
    {
        cin >> input2;
        if (!cin)
        break;
        if (input2<=0)
        break;
        input=input2;
        if (input!=input2)
        break;
        //偵錯部分結束

        if (input%2==1)//奇數部分
        {
            l=1,w=input/2;
            b=1;
            //
            while (1)
            {
                //菱形將循行放大
                for (n=1;n<=w;n++) //空格部分
                {
                    cout << " ";
                }
                for (n=1;n<=l;n++) //* 部分
                {
                    cout << "*";
                }
                cout << "\n"; //排版換行
                if (w==0)  //w=0時 將順序顛倒 開始菱形開始縮小
                b=0;
                if (b==1) //b值決定 放大縮小
                {
                    l=l+2;
                    w--;
                }
                else
                {
                    if (w==input/2) //w值等於初始值時判定結束
                    break;
                    l=l-2;
                    w++;
                }
            }

        }
        //奇數部分結束 換偶數部分
        else
        {
            l=1;
            while (l<=input) //將l用來限制邊長
            {
                if (l==1 || l==input) //第一行與最後一行*要填滿該行
                for (n=1;n<=input;n++)
                {
                    cout <<"*" ;
                }
                else
                {
                    cout <<"*";
                    for (n=1;n<=(input-2);n++)//前後*號 中間用迴圈填滿空格
                    {
                        cout <<" ";
                    }
                    cout << "*";
                }
                cout <<"\n";
                l++;
            }
        }

    }
    return 0;
}
