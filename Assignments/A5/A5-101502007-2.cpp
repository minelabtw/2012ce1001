#include <iostream>

using namespace std;

int main()
{
    int n;
    while (cin >>n && n>0) //若輸入數字且大於0,則進入迴圈
    {
        if (n%2!=0) //n為奇數的話,則要輸出菱形
        {
            for (int num=1;num<=(n+1)/2;num++) //菱形的上半部,包含中間最長的對角線
            {
                for (int x=1;x<=n-num-(n-1)/2;x++)
                cout <<" ";
                for (int x=1;x<=2*num-1;x++)
                cout <<"*";
                cout <<endl;
            }
            for (int num2=1;num2<=(n+1)/2-1;num2++) //菱形下半部,不包含中間最長的對角線
            {
                for (int y=1;y<=num2;y++)
                cout <<" ";
                for (int y=1;y<=n-2*num2;y++)
                cout <<"*";
                cout <<endl;
            }
        }
        else //n為偶數的話,則要輸出空心矩形
        {
            for (int num3=1;num3<=n;num3++)
            {
                if (num3==1 || num3==n) //上下兩邊的*數量洽為n
                {
                    for (int z=1;z<=n;z++)
                    cout <<"*";
                    cout <<endl;
                }
                else
                {
                    cout <<"*"; //其餘中間的*數量為n-2
                    for (int z=1;z<=n-2;z++)
                    cout <<" ";
                    cout <<"*";
                    cout <<endl;
                }
            }
        }
    }


    return 0;
}
