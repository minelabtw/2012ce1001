#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;

int main()
{
    srand((unsigned)time(NULL));
    int test;

    do
    {
        int com=(rand()%6)+1,you=(rand()%6)+1; //將兩個隨機的數設定於1到6之間
        cout << "Computer's dice point is: " << com <<endl;
        cout << "You dice point is: " << you << endl;
        if (com>you) //判斷是否輸給電腦
            cout << "Sorry, you lose." << endl;
        else if (com<you) //判斷是否贏過電腦
            cout << "Congratulation! You win." <<endl;
        else if (com==you) //判斷是否與電腦平手
            cout << "Draw" << endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
        cin >> test;
            if (cin==false||test!=1) //輸入字元或是不等於1的數則退出此遊戲
                break;
        cout << endl;
    }
    while (test==1); //若將test輸入為1則重玩此遊戲

    return 0;
}
