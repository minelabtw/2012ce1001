#include <iostream>
#include <cstdlib>
#include <iomanip>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int n =0;
    
    cin >>n;
    
    while(n>0)                                          //n>0 to continue this progarm
    {
        if (n%2==1)                                     //for input n is odd to draw a diamond
        {
            for (int i=1; i<=(n-1)/2+1 ;i=i+1)          //this loop to draw the up-half of diamond
            { 
                 for (int j=1; j<=(n-1)/2+1-i ; j++)
                      cout <<" ";
              
                 for (int j=1 ; j<=2*i-1 ; j++)
                      cout <<"*";
                      cout << endl;          
            }         
            for (int i=1; i<=(n-1)/2 ;i=i+1)            //this loop to draw the under-half of diamond
            {  
                 for (int j=1; j<=i ; j++)
                      cout <<" ";
              
                 for (int j=1 ; j<=n-2*i ; j++)
                      cout <<"*";
                      cout << endl;   
            }
        }
             
        if (n%2==0)                                   //for input n is even to draw a squere
        {
            for (int i=1; i<=n ;i++ )
            {
                 if (i==1||i==n)                      //for first row and last row
                 {  
                     for (int i=1;i<=n;i++)          
                          cout <<"*";
                          cout<< endl;
                 }
                 else                                //draw other rows
                 { 
                     cout<<"*";
                     for (int j=1;j<=n-2;j++)
                     {
                          cout<<" ";                        
                     }
                     cout<<"*";    
                     cout<< endl;
                 }      
            }   
                 
        }  
                    
        cin >>n;                                   //repeat input                                                  
     }  
     
     system("PAUSE");
     return 0;      
}
