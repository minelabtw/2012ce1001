#include<iostream>
#include<time.h>
#include<cstdlib>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    srand((unsigned)time(NULL));//for trivial randoming needs.

    while( 1 )//重複執行
    {
        int a = rand(),b = rand(),input;

        cout << "Computer's dice point is: " << a%6+1 << endl;
        cout << "Your dice point is: " << b%6+1 << endl;

        if( a%6+1 > b%6+1 )//電腦數字大於玩家數字時
            cout << "Sorry, you lose." << endl;

        if( a%6+1 < b%6+1 )//電腦數字小於玩家數字時
            cout << "Congratulation! You win." << endl;

        if( a%6+1 == b%6+1 )//電腦數字等於玩家數字時
            cout << "Draw" << endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
        cin >> input;
        cout << endl;

        if( input != 1 )//輸入不為1時結束程式
            break;
    }
    return 0;
}
