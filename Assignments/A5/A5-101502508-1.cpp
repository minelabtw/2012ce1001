#include<iostream>
#include<time.h>
#include<cstdlib> //standard library

using namespace std ;

int main ()
{
    srand((unsigned)time(NULL));
    //In order to generate random-like numbers, we need some value which changes every second.
    //like the value returned by the function "time"
    //And "srand((unsigned)time(NULL));" is distinctive enough for most trivial randoming needs.

    int input ;

    do
    {

    input = 0 ;
    int computer = rand() % 6 + 1 ;
    int player = rand () % 6 + 1 ;

    cout << "Computer's dice point is: " << computer << endl ; //let the dice generate number 1~6
    cout << "Your dice point is: " << player << endl ;

    if ( (computer) > (player) )
        cout << "Sorry, you lose." << endl ;

    if ( (computer) < (player ) )
        cout << "Congratulation! You win." << endl ;

    if ( (computer ) == (player ) )
        cout << "Draw" << endl ;

    cout << "Want to play again? ( 1 for continue, 0 for exit )" ;
    cin >> input ;
    cout << endl ;

    if ( input == 0 )
        return 0 ;

    }while ( input == 1 ) ; //use do...while to confirm it will start first then to loop

}
