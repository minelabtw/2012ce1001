#include <iostream>
#include<time.h>
#include<cstdlib>

using namespace std;

int main()
{
    srand((unsigned)time(NULL));
    //In order to generate random-like numbers, we need some value which changes every second.
    //like the value returned by the function "time"
    //And "srand((unsigned)time(NULL));" is distinctive enough for most trivial randoming needs.

    do //先跑do裡面的程式，做完一次之後才判斷要不要進入while
    {
        int Cdice = (rand() % 6) +1; //Generate random number and put it in to "a".
        int Pdice = (rand() % 6) +1;
        int A ;

        cout << "Computer's dice point is: " << Cdice << endl ;
        cout << "Your dice point is: " << Pdice << endl;

        if (Cdice == Pdice) // 如果電腦的數字等於玩家的數字
        {
            cout << "Draw" << endl ;
            cout << "Want to play again? ( 1 for continue, 0 for exit ) " ;

            if (cin >> A == false) //如果輸入的不是數字
                break ; //強制結束迴圈

            else if (A!=1)
                break ;

            cout << endl ;

        }
        else if (Cdice > Pdice)
        {
            cout << "Sorry, you lose." << endl ;
            cout << "Want to play again? ( 1 for continue, 0 for exit ) " ;

            if (cin >> A == false)
                break ;

            else if (A!=1)
                break ;

            cout << endl ;
        }
        else
        {
            cout << "Congratulation! You win." << endl ;
            cout << "Want to play again? ( 1 for continue, 0 for exit ) " ;

            if (cin >> A == false)
                break ;

            else if (A!=1)
                break ;

            cout << endl ;
        }
    }while (1) ; //當輸入的是數字

    return 0;
}
