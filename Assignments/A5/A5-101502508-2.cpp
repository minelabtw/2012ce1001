#include<iostream>
#include<cmath>

using namespace std ;

int main ()
{
    int n ;
    while(1)
    {
        n = 0 ;

        cin >> n ;

        if ( n == false || n<=0 )
            return 0 ;

        if ( n%2 == 0 ) //if n is even
        {
            for ( int height=1 ; height<=n ; height++ ) //every layers
            {
                if ( height==1 || height==n ) //the first and last layer s
                {
                    for ( int i=1 ; i<=n ; i++ )
                        cout <<"*" ;
                    cout << endl ;
                }

                else //except for first and last layers
                {
                    cout << "*" ;

                    for ( int j=1 ; j<=(n-2) ; j++ ) //permuta (n-2) blank space between * and *
                        cout << " " ;

                    cout << "*" ;
                    cout << endl ;
                }
            }
        }

            if ( n%2 == 1 ) //if n is odd
            {//then, dividing rhombus into two parts of triangle
                for ( int height = 1 ; height <= ( n/2 + 1) ; height++ )//the top half of triangle
                {
                    for ( int i=1 ; i<=(n/2+1-height) ; i++ )
                        cout << " " ;
                    for ( int i=1 ; i<=(2*height-1) ; i++ )
                        cout << "*" ;
                    cout << endl ;
                }

                int N=n ;//use N to change its value in every loop
                for ( int height = 1 ; height <= (n/2) ; height++ )//the bottom half of triangle
                {
                    for ( int i=1 ; i<=height ; i++ )
                        cout << " " ;
                    for ( int i=1 ; i<=N-2 ; i++ )
                        cout << "*" ;
                    N=N-2 ;
                    cout << endl ;
                }
            }
        }
}
