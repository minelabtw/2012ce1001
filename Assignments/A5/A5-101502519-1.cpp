#include <iostream>
#include <time.h>
#include<cstdlib>

using namespace std;

int main()
{
    int n=1;

    while(n==1)
    {
        srand((unsigned)time(NULL));
        int a = rand(),b = rand();           //由亂數表中產生a,b

        cout<<"Computer's dice point is : "<<a%6+1<<"\n";        //將a,b變成六的餘數+1,即可產生1~6
        cout<<"Your dice point is: "<<b%6+1<<"\n";
                                                                 //將兩個數字做比較
        if(a%6+1 < b%6+1)                                        //贏的狀況
            cout<<"Congratulation! You win.\n";
        else if(a%6+1 > b%6+1)                                   //輸的狀況
            cout<<"Sorry, you lose.\n";
        else                                                     //平手的狀況
            cout<<"Draw\n";

        cout<<"Want to play again? ( 1 for continue, 0 for exit )";
        cin>>n;                                                  //若入入支值為1,再進入迴圈
    }

    return 0;
}
