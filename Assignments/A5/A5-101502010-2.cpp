#include<iostream>
using namespace std;
int main()
{
    int input,w,h;//宣告變數
    while((cin>>input)!=false&&input>0)//若輸入字元或小於0之數則跳出迴圈
    {
        if(input%2==0)//若輸入為偶數則印出矩形
        {
            for(h=1;h<=input-1;h++)//印出上緣
                cout<<"*";
            for(h=1;h<=input-2;h++)
            {
                cout<<"*\n*";//印出結尾及換行
                for(w=1;w<=input-2;w++)
                    cout<<" ";//印出空白部分
            }
            cout<<"*\n*";//需多印一次換行
            for(h=1;h<=input-1;h++)
                cout<<"*";//印出下緣
            cout<<endl;//圖形結束換行
        }
        else//若奇數則印出菱形
        {
            for(h=1;h<=(input-1)/2;h++)//將圖形分為上半部處理
            {
                for(w=1;w<=(input/2)-h+1;w++)//先印出空白
                    cout<<" ";
                for(w=1;w<=h*2-1;w++)//再印出*號
                    cout<<"*";
                cout<<endl;
            }
            for(h=(input+1)/2;h<=input;h++)//圖形下半部
            {
                for(w=1;w<=h-(input/2)-1;w++)//先印出空白
                    cout<<" ";
                for(w=1;w<=(input-h)*2+1;w++)//再印*號
                    cout<<"*";
                cout<<endl;//圖形結束換行
            }
        }
    }
    return 0;//程式結束
}
