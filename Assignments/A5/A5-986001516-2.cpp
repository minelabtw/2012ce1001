//Assignment 5-2
//Input: Integer, a positive integer
//Output: a corresponding figure build up by "*"
#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    int num; //there is only one input to control the display.
    for (int times=1;times>0;times++) //these nested for loop can print the space and the "*" by the user's input and it also can repeat until the users order the command
    {
        cin >> num;
        if (num==-1) //key in number 1 can exit
        break;
        if (num%2==1) //if the input is odd than use the fllowing for loop to draw a corresponding solid diamond
        {
            for (int i=1;i<=(num+1)/2;i++) //this loop can finish the upper half part of the diamond, and the middle line
            {
                for (int j=(num+1)/2-i;j>0;j--)
                {
                    cout << " ";
                }
                for (int k=1;k<=2*i-1;k++)
                {
                    cout << "*";
                }
                cout << endl;
            }
            for (int i=(num-1)/2;i>0;i--) //this loop can finish the rest of the part.
            {
                for (int j=(num+1)/2-i;j>0;j--)
                {
                    cout << " ";
                }
                for (int k=1;k<=2*i-1;k++)
                {
                    cout << "*";
                }
                cout << endl;
            }
        }
        else //if the input is even, the fllowing for loop will draw a corresponding hollow rectangle.
        {
            for (int i=num;i>0;i--) //the first lin is always full of "*"
            {
                cout << "*";
            }
            cout << endl;
            for (int j=num-2;j>0;j--) //there are 2 "*" in all lines between the first and the last
            {
                cout << "*";
                for (int k=num-2;k>0;k--)
                {
                    cout << " ";
                }
                cout << "*";
                cout << endl;
            }
            for (int l=num;l>0;l--) //the last line is always fuul of "*"
            {
                cout << "*";
            }
            cout << endl;
        }
    }
    return 0;
}
