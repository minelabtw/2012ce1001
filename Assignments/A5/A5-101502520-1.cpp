#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;

int main()
{
    int computersnumber,yournumber,decision;
    srand((unsigned)time(NULL));//using time to reset randam table

    do{
        computersnumber=rand()%6+1;//the randam result will be 1~6
        yournumber=rand()%6+1;
        cout<<"Computer's dice point is: "<<computersnumber<<endl;
        cout<<"Your dice point is: "<<yournumber<<endl;
        if(computersnumber>yournumber)
            cout<<"Sorry, you lose."<<endl;
        else if(computersnumber<yournumber)
            cout<<"Congratulation! You win."<<endl;
        else
            cout<<"Draw"<<endl;
        cout<<"Want to play again? ( 1 for continue, 0 for exit ) ";//enter 1 to continue
        cin>>decision;
        cout<<endl;
    }    while(cin!=false && decision==1);
    return 0;
}
