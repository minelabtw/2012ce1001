#include <iostream>
using namespace std;
int main()
{
    int a;
    while(cin>>a)//輸入整數時則進入迴圈
    {
        if(a<=0)//小於等於0則結束

            return 0;

        if(a%2==1)//若為奇數時
        {

            for( int i=1 ; i<=(a+1)/2; i++ )//上半部的迴圈，每個i為一行
            {
                for( int j = 1 ; j<=a-i ; j++ )//空格迴圈
                    cout << " ";
                for( int j = 1 ; j<= i * 2 - 1 ; j=j+1 )//星星迴圈
                    cout << "*";
                cout << endl;//一行結束
            }
            for(int i=(a-1)/2;i>=1;i--)//下半部
            {
                for( int j = 1 ; j<=a-i ; j++ )//空格迴圈
                    cout << " ";
                for( int j = 1 ; j<= i * 2 - 1 ; j=j+1 )//星星迴圈
                    cout << "*";
                cout << endl;//一行結束
            }
        }

        if(a%2==0)//若為偶數時
        {
            for(int i=1;i<=a;i++)//第一行迴圈
                cout<<"*";
            cout<<endl;

            for(int i=a-2 ; i>=1 ; i=i-1 )//中間迴圈
            {
                cout<<"*";//每行第一個星星
                for(int j=1;j<=a-2;j++)//中間空格
                    cout<<" ";
                cout<<"*";//每行最後一個星星
                cout<<endl;
            }
            for(int i=1;i<=a;i++)//最後一行迴圈
                cout<<"*";
            cout<<endl;//一組結束

        }

    }


    return 0;
}


