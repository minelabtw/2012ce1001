#include<iostream>
#include<time.h>
#include<cstdlib>   // Must include!

using namespace std;

int main()
{
    srand((unsigned)time(NULL)); //說真的 我還是看不懂這是什麼...
    //In order to generate random-like numbers, we need some value which changes every second.
    //like the value returned by the function "time"
    //And "srand((unsigned)time(NULL));" is distinctive enough for most trivial randoming needs.

    int a,b,c; //Generate random number and put it in to "a,b".
    do //先執行一遍 在判斷條件
    {
        a = rand();// a和b都是隨機正數
        b = rand();
        cout << "Computer's dice point is: " << a%6+1 << endl; //電腦的骰子 A%6+1=必在1~6
        cout << "Your dice point is: " << b%6+1 << endl; //玩家的骰子 B%6+1=必在1~6
        if (a%6+1>b%6+1)
            cout << "Sorry, you lose." << endl;
        else if (a%6+1<b%6+1)
            cout << "Congratulation! You win." << endl;
        else if (a%6+1==b%6+1)
            cout << "Draw" << endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";

        cin >> c;
        if (cin==false) //錯誤偵測: 輸入字元則跳出程式
            break;

        cout << endl;
    }while (c==1);


    return 0;
}//end main
