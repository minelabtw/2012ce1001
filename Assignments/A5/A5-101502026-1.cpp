#include <iostream>
#include <time.h>
#include <cstdlib>

using namespace std;

int main()
{

    while(1)
    {
        int x,y,z;

        srand((unsigned)time(NULL)); //this is the fucntion to produce initial the random number

        x=1+rand()%6; //to produce a random number ranged from 1 to 6
        y=1+rand()%6; //to produce another random number ranged from 1 to 6

        cout << "Computer's dice point is: " << x << endl;
        cout << "Your dice point is: " << y <<endl;

        if(x>y)
            cout << "Sorry, you lose." << endl;

        if(x<y)
            cout << "Congratulation! You win." << endl;

        if(x==y)
            cout << "Draw" << endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
        cin >> z;
        cout << endl;

        if(cin==false) //if the z entered isn't an integer,then exit the loop
            break;
        if(z==1)
            continue; //if the z entered is 1, then the program continues
        if(z<=0||z>1)
            break; //if the z entered is smaller than 0 or greater than 1,then exit the loop
    }
        return 0;
}
