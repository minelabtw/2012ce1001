//輸入如果大於80會爆炸耶，怎麼辦
#include <iostream>

using namespace std;

int main()
{

    while(1)//重複輸入的while
    {
        int input_num=0;//為了判斷字元，先設定input_num為0
        cin>>input_num;

        if(0>=input_num)//錯誤偵錯,錯誤則結束程式
        {
            return 0;
        }
///////////////////////////////////////////偶數情形/////////////////////////////////////////////
        if(input_num%2==0)
        {
            for (int count=1;input_num>=count;count++)//輸出與input_num相同數目的*數
            {
                cout<<"*";
            }
            cout<<"\n";//下一行
            for (int c=1;(input_num-2)>=c;c++)//輸出邊框的第一顆*
            {
                cout<<"*";
                for(int count=1;(input_num-2)>=count;count++)//輸出與input_num-2相同的*數
                {
                    cout<<" ";
                }
                cout<<"*\n";//下一行
            }
            for (int count=1;input_num>=count;count++)//輸出邊框的最後一顆*
            {
                cout<<"*";
            }
            cout<<"\n";//下一行
        }
////////////////////////////////////////////奇數情況////////////////////////////////////////////////
        if (input_num%2==1)
        {
            for(int count=1;input_num>=count;count+=2)//下一行的*數是上一行+2
            {
                for(int space=(input_num-count)/2,cc=1;space>=cc;cc++)//補齊前面的空格
                {
                    cout<<" ";
                }
                for(int c=1;count>=c;c++)//count等於*數
                {
                    cout<<"*";
                }
                for(int space=(input_num-count)/2,cc=1;space>=cc;cc++)//補齊後面的空格
                {
                    cout<<" ";
                }
                cout<<"\n";
            }//已經完成上半部的金字塔
            for(int count=(input_num-2);count>=1;count-=2)//要完成下半部的金字塔
            {//方法如上，只有起始值的差別
                for(int space=(input_num-count)/2,cc=1;space>=cc;cc++)
                {
                    cout<<" ";
                }
                for(int c=1;count>=c;c++)
                {
                    cout<<"*";
                }
                for(int space=(input_num-count)/2,cc=1;space>=cc;cc++)
                {
                    cout<<" ";
                }
                cout<<"\n";
            }

        }
    }
    return 0;
}
