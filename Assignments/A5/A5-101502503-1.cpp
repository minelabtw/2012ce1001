
#include<iostream> // allows program to output data to the screen
#include<time.h>
#include<cstdlib>

using namespace std ;
// funtion main begins program execution
int main ()
{
    int i=0, j=0 ,k=1 ; // variable declarations

    while ( k=1 ) // 當k=1,進入while迴圈
    {
        srand((unsigned)time(NULL));
        int a = rand() , b = rand(); //Generate random number and put it in to "a".
        i = a%6+1 ;
        j = b%6+1 ;

        cout << "Computer's dice point is: " << i << endl;
        cout << "Your dice point is: " << j <<endl;


        if ( i > j ) //如果i>j,執行以下動作
            cout << "Sorry, you lose." << endl ;
        else if ( j > i )//當j>i,執行以下動作
            cout << "Congratulation! You win." << endl ;
        else
            cout << "Draw" << endl ;

        cout << "Do you want to play again? ( 1 for continue, 0 for exit ) ";
        cin >> k ;
        cout << endl ;

        if ( k != 1 ) //如果k不等於1,結束程式
            return -1 ;
        else if ( cin == false ) //如果輸入為字元,結束程式
            return -1 ;
    }// end while
    return 0 ;
} // end main
