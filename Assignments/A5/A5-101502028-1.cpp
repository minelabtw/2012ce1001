#include <iostream>
#include <cstdlib>
#include<time.h>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    srand((unsigned)time(NULL)); // random
    while (true) // start a loop untill a break appears
    {
        int computer, me, first = rand (), second = rand(), exit; // integers
        computer = first % 6 + 1, me = second % 6 + 1; // randomize the first number and second number between 1 ~ 6 and change the name to computer and me
        cout << "Computer's dice point is: " << computer << endl; // appears the random number of computer
        cout << "Your dice point is: " << me << endl; // appears the random number of me

        if (computer > me) // if computer is bigger than me
            cout << "Sorry, you lose." << endl; // appears this message
        else if (computer < me) // if the computer is smaller than me
            cout << "Congratulation! You win." << endl; // appears this message
        else if (computer == me) // if both have the same number
            cout << "Draw." << endl; // appears this message

        cout << "Want to play again? ( 1 for continue, 0 for exit ) "; // appears this message answering the question
        cin >> exit; // input an exit number

        if (exit != 1 || cin == false) // if the exit number isn't 1
            break; // breaks the loop
        else // if it's 1
        {
            cout << endl; // skip a line
            continue; // and continue
        }
    }
    return 0; // finish the program
}
