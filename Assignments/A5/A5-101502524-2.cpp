#include<iostream>
using namespace std;

int main()
{
    int k;
    while(1)//一個無限迴圈
    {
        cin>>k;

        if(k<0)//若輸入為負值,則跳出迴圈
        {
            break;
        }
        if(!cin)//若輸入錯誤字元,則跳出迴圈
        {
            break;
        }
        if(k%2==1)//輸入為奇數,則執行此運算
        {
            for(int i= 1;i<=k;i++)//菱形的上半部
            {
                for(int j=1;j<=k-i;j++)
                {
                    cout<<" ";
                }
                for(int j=1;j<=i*2-1;j++)
                {
                    cout<<"*";
                }
                cout<<endl;
            }
            for(int a=1;a<=k-1;a++)//菱形的下半部
            {
                for(int b=k-1;b>=k-a;b--)
                {
                    cout<<" ";
                }
                for(int b=2*k-1;b>a*2;b--)
                {
                    cout<<"*";
                }
                cout<<endl;
            }
        }
        if(k%2==0)//輸入為偶數,則執行此運算
        {
            for(int c=1;c<=k;c++)//第一行
            {
                cout<<"*";
            }
            cout<<endl;
            for(int l=1;l<=k-2;l++)//中間
            {
                cout<<"*";
                for(int g=1;g<=k-2;g++)
                {
                    cout<<" ";
                }
                cout<<"*"<<endl;
            }
            for(int f=1;f<=k;f++)//最後一行
            {
                cout<<"*";
            }
            cout<<endl;
        }
    }
    return 0;
}
