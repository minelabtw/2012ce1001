#include <iostream>
#include <cstdlib>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int number;
    do
    {
        cin >> number;
        if (number <= 0 || cin == false) // break the loop if it's negative, zero or a character input
            break;
        if (number % 2 == 1) // if the number is odd, make a * diamond shape depending of the input number
        {
////////////////////////////////////////////////////from the top part to the middle part///////////////////////////////////////////////////////////
            for (int line = 1; line <= number; line += 2)
            {
                for (int space = 1; space <= (number - line) / 2; space++)
                    cout << " ";
                for (int star = 1; star <= line; star++)
                    cout << "*";
                cout << endl;
            }
////////////////////////////////////////////////////from the middle - 1 part to the bottom part///////////////////////////////////////////////////
            for (int line = number - 2; line >= 1 / 2; line -= 2)
            {
                for (int space = 1; space <= (number - line) / 2; space++)
                    cout << " ";
                for (int star = 1; star <= line; star++)
                    cout << "*";
                cout << endl;
            }
        }
////////////////////////////////////////////////////ends the diamond shape program/////////////////////////////////////////////////////////////////

        else if (number % 2 == 0) // if the input number is even, make a * square shape depending of the input number
        {
////////////////////////////////////////////////////start the square shape program/////////////////////////////////////////////////////////////////
            for (int star = 1; star <= number; star++) // here will make the top part of the square
                cout << "*";
            cout << endl;
            for (int star = 1; star <= number - 2; star++) // here will make the square body
            {
                cout << "*";
                for (int star = 1; star <= number - 2; star++)
                    cout << " ";
                cout << "*";
                cout << endl;
            }
            for (int star = 1; star <= number; star++) // here will make the bottom part of the square
                cout << "*";
            cout << endl;
        }
/////////////////////////////////////////////////////ends the square shape program//////////////////////////////////////////////////////////////////

    }while (true); // make a loop until it breaks
    return 0; // ends
}
