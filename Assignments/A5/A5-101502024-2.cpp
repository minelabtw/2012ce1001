#include <iostream>
using namespace std;

int main()
{
    int a,b,c,d,e,f,g,h;

    while ( cin >> a )//Judge if the input is valid or not.
    {
        if ( a <= 0 )//If the input is smaller than zero, end the program.
        {
            return 0;
        }
        if ( a % 2 == 1 )//The formula to be used if a is odd.
        {//To generate the graphic.
            h = ( a + 1 ) / 2;
            for ( f = 1;f <= h;f++ )
            {
                for ( g = 1;g <= h-f;g++ )
                {
                    cout<<" ";
                }
                for ( g = 1;g <= f * 2 - 1;g++ )
                {
                    cout<<"*";
                }
                cout<<endl;
            }
            for ( f = 1;f <= h - 1;f++ )
            {
                for ( g = 1;g <= f;g++ )
                {
                    cout<<" ";
                }
                for ( g = 1;g <= a - f * 2;g++ )
                {
                    cout<<"*";
                }
                cout<<endl;
            }
        }
        if ( a % 2 == 0 )//The formula to be used if a is even.
        {//To generate the graphic.
            for ( b = 1;b <= a;b++)
            {
                cout << "*";
            }
            cout << "" <<endl;
            for ( e = 2;e <= a - 1;e++)
            {
                cout << "*";
                for ( c = 2;c <= a - 1;c++)
                {
                    cout <<" ";
                }
                cout << "*" << endl;
            }
            for ( d = 1;d <= a;d++)
            {
                cout << "*";
            }
            cout << "" <<endl;
         }
    }
    return 0;
}
