//101502504
#include<iostream>//allows program to output data to the screen
#include<time.h>
#include<cstdlib> // Must include!
using namespace std;

int main()
{
    srand((unsigned)time(NULL));
        //In order to generate random-like numbers, we need some value which changes every second.
        //like the value returned by the function "time"
        //And "srand((unsigned)time(NULL));" is distinctive enough for most trivial randoming needs.
    int t;//use t to determaine if user want to play again
    do
    {
        int i = rand(); //Generate random number and put it in to "i".
        int j = rand(); //Generate random number and put it in to "j".
        int a = i%6+1,b = j%6+1;

        cout << "Computer's dice point is: " << a << endl;
        cout << "Your dice point is: " << b << endl;

        if ( a > b )//computer win
            cout << "Sorry, you lose." << endl;
        if ( a == b )//even
            cout << "Draw" << endl;
        if ( a < b )//you win
            cout << "Congratulation! You win." << endl;

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";
        cin >> t;//decide if user want to play
        cout << endl;
        if ( t != 1 )
            return 0;
    }while ( t == 1 );//keep going if user print 1

    return 0;
}//end main
