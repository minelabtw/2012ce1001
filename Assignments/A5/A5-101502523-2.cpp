#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    while( 1 )//重複輸入
    {
        int n;
        cin >> n;

        if( n <= 0 )//輸入為負數或零時結束程式
            break;
        if( !cin )//輸入為字元時結束程式
            break;

        if( n % 2 == 1 )//輸入為奇數時
        {
            for( int i = 1 ; i <= (int)n/2 + 1 ; i++ )          //把菱形拆成上下部分，此為上半部分
            {                                                   //
                for( int j = 1 ; j < (int)n/2 + 2 - i  ; j++ )  //例如輸入5        *
                    cout << " ";                                //                ***
                for( int j = 1 ; j < i * 2 ; j++ )              //               *****
                    cout << "*";                                //
                cout << endl;                                   //
            }                                                   //
            for( int i = (int)n/2 ; i > 0 ; i-- )               //此為下半部分    ***
            {                                                   //                 *
                for( int j = 1 ; j < (int)n/2 + 2 - i ; j++ )
                    cout << " ";
                for( int j = 1 ; j < i * 2 ; j++ )
                    cout << "*";
                cout << endl;
            }
        }
        if( n % 2 == 0 )//輸入為偶數時
        {
            for( int j = 1 ; j <= n ; j++ )//先行輸出n個*
                cout << "*";
            cout << endl;
            for( int i = 1 ; i < n - 1 ; i++ )//符號*加上空白的輸出   例如： *  *
            {                                 //                             *  *
                cout << "*";
                for( int j = 1 ; j < n - 1 ; j++ )
                    cout << " ";
                cout << "*" << endl;
            }
            for( int j = 1 ; j <= n ; j++ )//輸出n個*
                cout << "*";
            cout << endl;
        }
    }
    return 0;
}
