#include <iostream>
#include<time.h>
#include<cstdlib>   // Must include!

using namespace std;

int get_valid_positive_int(int upperbound);             //Function to get valid input with upper bound

int main()
{
    srand((unsigned)time(NULL));
    while(1)
    {
        cout << "Computer's dice point is: ";
        int num_computer = rand()%6+1;                  // 產生電腦的數
        int num_yours = rand()%6+1;                     // 產生玩家的數
        int check=0;
        cout << num_computer << endl;
        cout << "Your dice point is: " <<num_yours << endl;

        if(num_computer>num_yours)
            cout << "Sorry, you lose.\n";
        else if(num_computer<num_yours)
            cout << "Congratulation! You win.\n";
        else
            cout << "Draw\n";

        cout << "Want to play again? ( 1 for continue, 0 for exit ) ";

        if((check = get_valid_positive_int(1))==-1)  //invalid input
            return -1;
        if(check==0)
            return 0;

    }
    return 0;
}

int get_valid_positive_int(int upperbound)
{
    int n;
    char check;
    if(!(cin >> n)||n<0||n>upperbound)                            // Error detect
        return -1;                                  //-1 means invalid input
    check = cin.peek();                             // get next char in istream
    if(check!=' ' && check!='\n' && check!=0)       //Detect invalid char after integer, like "123a"
        return -1;
    return n;
}
