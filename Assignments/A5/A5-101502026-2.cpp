#include <iostream>

using namespace std;

int main()
{
    while(1)
    {
        int x,i=1,j=0,k=1;
        cin >> x;

        if(cin==false||x<=0) //when x entered isn't an integer,then exit the program
        return 0;

        if(x%2==1) //to judge if x is an odd number
        {
            while(i<=x/2+1) //this is the loop to print out the upper layors of the pyramid
            {
                j=0;
                while(i+j<=(x/2))
                {
                    cout << " ";
                    j++;
                }
                j=0;
                while(j<i*2-1&&i<=(x/2)+1)
                {
                    cout << "*";
                    j++;
                }
                i++;
                cout << endl;
            }
            while(i>x/2+1&&i<=x) //this is the loop to print out the lower layors of the pyramid
            {
                j=0;
                while(i+j+(x+3)/2<=i*2)
                {
                    cout << " ";
                    j++;
                }
                j=0;
                while(i+j<=x*2-i)
                {
                    cout << "*";
                    j++;
                }
                i++;
                cout << endl;
            }
            k++;
        }
        else //if x entered isn't an odd number, then enter this section
        {
            while(j<=x-1) //this is the loop to print out the first layor of the hollow rectangle
            {
                cout << "*";
                j++;
            }
            cout << endl;
            i=2;
            while(i>1&&i<x) //this is the loop to print out the middle layors of the hollow rectangle
            {
                j=0;
                while(j==0)
                {
                    cout << "*";
                    j++;
                }
                while(j>=1&&j<x-1)
                {
                    cout << " ";
                    j++;
                }
                while(j==x-1)
                {
                    cout << "*";
                    j++;
                }
                cout << endl;
                i++;
            }
            j=0;
            while(j<=x-1) //this is the loop to print out the last layor of the hollow rectangle
            {
                cout << "*";
                j++;
            }
            cout << endl;
        }
    cout << endl;
    }
    return 0;
}
