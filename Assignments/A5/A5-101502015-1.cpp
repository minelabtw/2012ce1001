#include<iostream>
#include<time.h>
#include<cstdlib>
using namespace std;
int main()
{
    int a;
    do//使用do while迴圈使之先執行一次再做判斷
    {
        srand((unsigned)time(NULL));
    //In order to generate random-like numbers, we need some value which changes every second.
    //like the value returned by the function "time"
    //And "srand((unsigned)time(NULL));" is distinctive enough for most trivial randoming needs.
        int n1=rand();
        int n2=rand();
        cout<<"Computer's dice point is: "<<n1%6+1<<endl;//讓電腦隨機輸出第一個數
        cout<<"Your dice point is: "<<n2%6+1<<endl;//讓電腦隨機輸出第二個數
        if(n1%6+1>n2%6+1)//比較兩數大小
            cout<<"Sorry, you lose."<<endl;
        if(n1%6+1<n2%6+1)
            cout<<"Congratulation! You win."<<endl;
        if(n1%6+1==n2%6+1)
            cout<<"Draw."<<endl;
        cout<<"Want to play again? ( 1 for continue, 0 for exit )";
        cin>>a;//輸入一數判斷是否繼續執行
    }
    while(a==1);
    return 0;
}
