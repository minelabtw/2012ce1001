#include<iostream>
#include<stdlib.h>
#include<limits>
using namespace std;


bool inputCheck(int in);
void printDiamond(int n);
void printRectangle(int n);


int main()
{
    //initial
    int n=0;


    while(1)
    {
        //input
        cin>>n;


        //exit
        if(!inputCheck(n))
            break;


        //output
        if(n%2==1)//odd number
            printDiamond(n);
        else//even number
            printRectangle(n);
    }


    //end
    cout<<endl;
    system("pause");
    return 0;
}




bool inputCheck(int in)
{
    cin.clear();//reset cin
    cin.ignore(numeric_limits<streamsize>::max(),'\n');//clear cin buffer
    if(cin.gcount()!= 1)//not integer
        return false;
    else if(in<=0)//negative or zero
        return false;
    //no error
    return true;
}


void printDiamond(int n)
{
    for(int line=-(n/2);line<=n/2;line++)//abs(-(n/2))+n/2=n, n line total
    {
        for(int j=abs(line);j>0;j--)//space abs(-(n/2))times to 0 times and to (n/2) times
            cout<<' ';
        for(int j=0;j<(n/2-abs(line))*2+1;j++)//sign 1 times to (n/2-abs(i))*2+1 times and to 1 times
            cout<<'*';
        cout<<endl;
    }
}


void printRectangle(int n)
{
    for(int i=0;i<n;i++)//top side
        cout<<'*';
    cout<<endl;

    for(int i=0;i<n-2;i++)//body
    {
        cout<<'*';
        for(int j=0;j<n-2;j++)
            cout<<' ';
        cout<<'*'<<endl;
    }

    for(int i=0;i<n;i++)//bottom side
        cout<<'*';
    cout<<endl;
}
