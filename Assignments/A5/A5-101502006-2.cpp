#include<iostream>

using namespace std;

int main()
{
    int n;
    while (cin >> n && n>0) //used for enter again and error detection.(negative,zero,letter)
    {
        if(n%2!=0) //used for detect whether n is odd.
        {
//////////////////////////    Diamond    ////////////////////////
            for( int i=1 ; i<=(n/2+1) ; i++ )//the upper triangle.
            {
                for( int j = 1 ; j <= (n/2+1) - i ; j++ )
                    cout << " ";
                for( int j = 1 ; j <= i * 2 - 1 ; j++ )
                    cout << "*";
                cout << endl;
            }
            for( int i=1 ; i<=(n/2) ; i++ )//the lower triangle.
            {
                for( int j = 1 ; j <= i;  j++ )
                    cout << " ";
                for( int j = 1 ; j <= n-i*2 ; j++ )
                    cout << "*";
                cout << endl;
            }
//////////////////////////    Diamond    ////////////////////////
        }
        else //used for detect whether n is even.
        {
//////////////////////////    Square    // //////////////////////
            for (int j=1; j<=n ; j++ ) //top of the square
            {
                cout << "*";
            }
            cout << endl;
            for (int i=2 ; i<n ;i++) // middle of the square
            {
                for (int j=1; j<=n ; j++)
                {
                    if (j==1 || j==n)
                        cout << "*";
                    else
                        cout << " ";
                }
                cout << endl;
            }
            for (int j=1; j<=n ; j++ ) // bottom of the square
            {
                cout << "*";
            }
            cout << endl;
//////////////////////////    Square    // //////////////////////
        }
    }
    return 0;
}
