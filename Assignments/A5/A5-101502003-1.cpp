#include <iostream>
#include<time.h>
#include<cstdlib>

using namespace std; //以下可以省略std::

int main()

{
    int c; //設變數,c=看要不要繼續玩所輸入的數
    srand((unsigned)time(NULL)); //隨機取數的方法
    int a = rand();
    cout << "Computer's dice point is: " ; //電腦的骰子
    cout << (a%6)+1 <<endl; //a/6的餘數+1 剛好會等於骰子的1~6

    int b = rand(); //隨機取數的方法
    cout <<"Your dice point is: " ; //玩家的骰子
    cout << (b%6)+1 <<endl; //b/6的餘數+1 剛好會等於骰子的1~6

    if (a>b) //如果電腦的數>玩家的數 則代表輸
    {
        cout << "Sorry, you lose." << endl;
        cout <<"Want to play again? ( 1 for continue, 0 for exit ) " ;
    }
    else if (b>a)  //如果電腦的數<玩家的數 則代表贏
    {
        cout << "Congratulation! You win."<<endl;
        cout <<"Want to play again? ( 1 for continue, 0 for exit ) ";
    }
    while (cin>>c) //如果輸入值c是整數,則進入迴圈
    {
        if (c!=1)  //如果c不是1,則直接結束程式
            return 0;
        else  //如果輸入的是1,則在隨機取數,比大小 (和上面一樣)
        {
            srand((unsigned)time(NULL)); //隨機取數的方法
            int a = rand();
            cout << "Computer's dice point is: " ; //電腦的骰子
            cout << (a%6)+1 <<endl; //a/6的餘數+1 剛好會等於骰子的1~6

            int b = rand(); //隨機取數的方法
            cout <<"Your dice point is: " ; //玩家的骰子
            cout << (b%6)+1 <<endl; //b/6的餘數+1 剛好會等於骰子的1~6

            if (a>b) //如果電腦的數>玩家的數 則代表輸
            {
                cout << "Sorry, you lose." << endl;
                cout <<"Want to play again? ( 1 for continue, 0 for exit ) " ;
            }
            else if (b>a)  //如果電腦的數<玩家的數 則代表贏
            {
                cout << "Congratulation! You win."<<endl;
                cout <<"Want to play again? ( 1 for continue, 0 for exit ) ";
            }
        }
    }

    return 0;
}
