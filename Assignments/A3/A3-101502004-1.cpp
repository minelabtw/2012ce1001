#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int salary;//設變數salary為整數
    double Tax;//設變數Tax為浮點數即小數

    cout << "Enter the salary per month (negative to end): ";//第一行要顯示的字串
    cin >> salary;//輸入salary

    while (salary >= 0)//在輸入之變數salary>=0的條件下進入迴圈，若小於零則直接結束
    {
        if (salary >= 50000)//當salary大於50000的時候
        Tax = salary*0.3;//salary乘上0.3等於Tax

        else if (salary >= 15000)//若無法滿足上面條件，則繼續檢驗，但限制在salary大於等於15000
        Tax = salary*0.2;//滿足條件的數乘上0.2則是答案Tax

        else if (salary < 15000)//最後一個條件判斷
        Tax = salary*0.1;//乘上0.1為答案

        cout << "Tax : " << fixed << setprecision(3) << Tax << endl;//輸出Tax，且要顯示小數點後3位
        cout << "Enter the salary per month (negative to end): ";//繼續準備下一個迴圈
        cin >> salary;//輸入下一個變數
    }
    return 0;
}
