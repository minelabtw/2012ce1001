#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    cout << "Enter the salary per month (negative to end): "; //at first print message
    float salary=0;

    while(cin >> salary && salary>=0) // Check the input
    {
        cout << "Tax : ";

        if(salary<15000)
            cout << fixed << setprecision(3) << 0.1*salary << endl; // precision to 3 digit after '.'
        else if(salary<50000)
            cout << fixed << setprecision(3) << 0.2*salary << endl;
        else
            cout << fixed << setprecision(3) << 0.3*salary << endl;

        cout << "Enter the salary per month (negative to end): "; // show message every time
    }
    if(salary>=0) // check for Invalid input.
    {
        cout << "Invalid input." << endl;
        return -1;
    }
    return 0;
}
