#include <iostream>

using namespace std;

int main()
{
    int a=0,t=10,n=1,m,o,p=0,sum=0;

    cout << "Enter a binary number: ";

    if( (cin >> a)==false || (a<0) )  //判斷a是否為數字與正數
    {
        if(a<0)
            cout << "Input can't be negative. \n";
        else
            cout << "Invalid input. \n";
    }

    else
    {
        while(10*a>=t)
        {
            m=((a%t)/(t/10));  //m隨t的10倍成長形成商,找出每一位數之值

            if(m/2>=1&&n==1)   //若m除2之商大於等於1,輸出一次Can't be binary input.
            {
                cout << "Can't be binary input. \n" ;
                n=n+1;
            }

            else if(m/2<1)     //若m除2之商小於1,進入迴圈
            {
                o=10;
                while(o<t)     //o隨t的10倍成長,讓m乘2的次方
                {
                    m=m*2;
                    o=o*10;
                }

                sum=sum+m;     //將每個二進位數字相加
            }
            t=t*10;
        }

        if(n==1)               //非 binary input 者輸出sum
        {
            cout << "Decimal is: " <<sum;
        }
    }
    return 0;
}
