#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int salary ;
    float tax ; // tax is a degital that can show decimal point

    cout << "Enter the salary per month (negative to end) : " ;
    cin >> salary ; // the interger entered by user

    while (salary > 0)
    {
        tax = 0.1 * salary ;

        if (salary >= 15000)
        tax = 0.2 * salary ;

        if (salary >= 50000)
        tax = 0.3 * salary ;

        cout << "Tax : " << fixed << setprecision(3) << tax << endl ; // show decimal after the third digit
        cout << "Enter the salary per month (negative to end) : " ;
        cin >> salary ;
    } // end while

    return 0 ;

}
