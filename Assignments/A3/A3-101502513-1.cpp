#include <iostream>
#include <iomanip> //parameterized stream manipulators
using namespace std;

int main()
{
    float salary;
    cout << "Enter the salary per month (negative to end): ";

    while ( cin >> salary && salary >= 0 )
    {
        if ( salary >= 50000 )
        {   //display the taxes with three digits of precision
            cout << "Tax : " << fixed << setprecision( 3 ) << 0.3 * salary << endl;
        }

        else if ( salary >= 15000 )
        {
            cout << "Tax : " << fixed << setprecision( 3 ) << 0.2 * salary << endl;
        }

        else if ( salary >= 0 )
        {
            cout << "Tax : " << fixed << setprecision( 3 ) << 0.1 * salary << endl;
        }
        cout << "Enter the salary per month (negative to end): ";
    }
    return 0;
}
