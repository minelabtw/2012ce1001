#include <iostream>
#include <iomanip>
#include <limits>
using namespace std;
int main()
{
    double salary;//salary can be decimal fraction
    cout << "Enter the salary per month (negative to end): ";
    while ( (cin >> salary==false) || (salary > 0) ) // operate while if salary > 0 or salary is character
    {
        if ( salary >= 50000 ) //operate if salary >= 50000
            cout << "Tax : " << fixed << setprecision(3) << salary * 0.3 << endl;
        else if ( salary >= 15000 ) //operate if 50000 >= salary >= 15000
            cout << "Tax : " << fixed << setprecision(3) << salary * 0.2 << endl;
        else if ( salary > 0 ) //operate if salary < 15000
            cout << "Tax : " << fixed << setprecision(3) << salary * 0.1 << endl;
        else //error detection : if  salary is character, display Invalid input
            {
            cout << "Invalid input." << endl;
            cin.clear(); //this will reset cin to default.
            cin.ignore(numeric_limits<streamsize>::max(),'\n'); //this will clear cin buffer.
            }

        cout << "Enter the salary per month (negative to end): ";
    } //end while

    return 0;
} //end main
