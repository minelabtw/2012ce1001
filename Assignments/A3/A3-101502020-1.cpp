//Assignment3-1 by J
#include <iostream>
//for funtion cout
#include <iomanip>
//for function setprecision
using namespace std;
int main()
{
    int slr=1 ;
    float tax=0 ;

    //anoucing all the variables used : slr as salary , tax.

    while ( slr>=0 )  //use while for a repeat function until input a negative number as stop
    {
        cout << "Enter the salary per month (negative to end): " ;
        cin >> slr ;

        if ( slr>=50000)
        tax=0.3*slr ;
        else if (slr>=15000)
        tax=0.2*slr ;
        else if (slr>0) //3 standards aquired by index
        tax=0.1*slr ;
        else
        tax=-1 ; //for stopping showing informations not needed

        if ( tax>0 ) //for stopping showing informations not needed too
        cout << "Tax : " << fixed << setprecision( 3 ) << tax << endl ;
    }

    return 0 ;
}
