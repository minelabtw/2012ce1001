#include <iostream>
#include <iomanip>
#include <cstdlib>
using namespace std;

int main()
{
    int salary;
    float tax;
    cout << "Enter the salary per month (negative to end) : ";
    cin >> salary;
    if  (salary>=50000)
    {
        tax=salary*0.3;
        cout << "Tax : " << fixed << setprecision(3) << tax << endl;
    }
    else if (salary<50000 && salary>=15000)
    {
         tax=salary*0.2;
         cout << "Tax : " << fixed << setprecision(3) << tax << endl;
    }
    else if (salary<15000 && salary>=0)
    {
         tax=salary*0.1;
         cout << "Tax : " << fixed << setprecision(3) << tax << endl;
    }
    else;                                                                       /* 用if... else if... else...的敘述來處理輸入的值,並且涵蓋個範圍,除此之外,當輸入的為負數時,不做任何動作即結束程式 */ 
    system("pause");
    return 0;
}
