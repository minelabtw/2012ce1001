#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int salary=0;//薪水初始值
    double tax;//實數tax


    while(salary>=0)
    {
        cout<<"Enter the salary per month (negative to end): ";//輸入薪水
        cin>>salary;


        if (salary<15000 && salary>0)
        tax=salary*0.1;
        if (salary<50000 && salary>=15000)
        tax=salary*0.2;
        if (salary>=50000)
        tax=salary*0.3;
        if (salary<0)//當輸入負數   程式結束
        {
            return 0;
        }


        cout<<"Tax : "<<setprecision(3)<<fixed<<tax<<"\n";//小數點後精準到第三位


    }
    return 0;
}
