//According to the salary which enter by user and count the tax
#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int salary;
    float tax;

    cout << "Enter the salary per month (negative to end): ";
    cin >> salary;

    while( salary >= 0 )//if user enter negative then the program stop
    {
        if( salary < 15000 ) tax = salary*0.1;

        else if( 15000 <= salary && salary < 50000 ) tax = salary*0.2;

        else tax = salary*0.3;

        cout << "Tax : " << fixed << setprecision( 3 ) << tax << endl;//accurate to the place after decimal point

        cout << "Enter the salary per month (negative to end): ";

        cin >> salary;//let user keep entering salary until user enter nagative number
    }//while end

    return 0;

}//main end

