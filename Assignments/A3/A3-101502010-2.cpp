#include <iostream>
using namespace std;
int main()
{
    int input,count=1,output=0;//宣告變數
    cout << "Enter a binary number: ";
    if ( (cin >> input) == false )//判定輸入是否為字元
    {
        cout << "Invalid input." << endl;//輸入為字元之錯誤輸出
        return -1;
    }
    else if ( input < 0 )//判定輸入是否為負數
    {
        cout << "Input can't be negative." << endl;//輸入為負數之錯誤輸出
        return -1;
    }
    else
    {
        while ( input != 0)//當餘數為0時跳出迴圈
        {
            if ( (input%10) > 1 )//判定是否有非0或1之輸入
            {
                cout << "Can't be binary input." << endl;//非0或1之錯誤輸出
                return -1;
            }
            output += ( input%10 ) * count;//將欲輸出值加上最末位數字乘以2之倍數
            input /= 10;//將最末位數字去除
            count *= 2;//將底數增加

        }
        cout << "Decimal is: " << output <<endl;//輸出結果
    }
    return 0;
}
