#include <iostream>
#include <cstdio>
#include <string.h>
int main()
{
    char a[64]; //String length is not given, so we suppose that the string length is 64
    int i; //Used in "for" function
    long j=0; //Binary could turn out to be a large number, so we use "long" here, and "j" is the decimal number we want, so initialize it
    printf( "Enter a binary number: " );
    scanf( "%s" , a ); //Scan the string we input
    for ( i=0; i<strlen(a); i++ ) //Analyze every string we input one by one
    {
        if ( ( a[i] == '1' ) || ( a[i] == '0' ) ) //It's the normal input whose number concludes only 1 and 0
            j = j << 1 | ( a[i] - '0' ); //Use one bit (left) shift function and mask with every string by ASCII number. If the mask result is number "1", "j" multiplies 2
        else if (a[i] == '-') //When the minus symbol is scanned, print "Input can't be negative."
        {
            printf("Input can't be negative.");
            return 0;
        }
        else if ( ( a[i] > 49 ) && ( a[i] < 58 ) ) //It's the situation whose input number is without 0 or 1
        {
            printf("Can't be a binary input.");
            return 0;
        }
        else // Other circumstances like alphabets
        {
            printf( "Invalid input." );
            return 0;
        }
    }
    printf( "Decimal is: %ld", j ); //Calculate the decimal number
    return 0;
}
