#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int num,n=0,sum=0;

    cout << "Enter a binary number: ";
    while (cin >> num)//to divide string
    {//start while(1)
        if (num > 0)//to divide negative number
        {//start if(1)
            while (num >= 1)
            {//start while(2)
                if (num%10 <= 1)//to transfer a binary number into a decimal number
                {
                    sum = sum + num%10 * pow(2,n);
                    num =num/10;
                    n++;
                }
                else//to divide binary number
                {
                    cout << "\nCan't be binary input.";
                    return 0;
                }
            }//end while(2)
            cout << "\nDecimal is: " << sum;
            return 0;
        }//end if(1)
        else
        cout << "\nInput can't be negative.";
        return 0;
    }//end while(1)
    cout << "\nInvalid input.";
    return 0;
}
