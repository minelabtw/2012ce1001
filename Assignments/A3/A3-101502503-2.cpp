//101502503
#include <iostream>// allows program to output data to the screen

using namespace std;

// funtion main begins program execution
int main()
{
    int a, b=0 ,c=1 ,d=0 ,Decimal=0; // variable declarations

    cout << "Enter a binary number: " ;

    if ( cin >> a == false || a < 0 )
    {
        if ( a < 0 ) // determine whether a is smaller than 0
        cout << "Input can't be negative." << endl ; // display result
        else
        cout << "Invalid input." << endl ; // display result
    } // end if
    else if ( a > 0 ) // determine whether a is greather than 0
    {
        b = a % 10 ;

        for ( a ; a > 0 ; a%10 )
        {
            if ( b > 1 ) // determine whether a is greather than 1
            {
                cout << "Can't be binary input." << endl ; // display result
                return -1 ;
            } //end if
            else
            {
                d = (a%10) * c ;
                c = 2 * c ;
                a = a / 10 ;
                Decimal += d ;
            } // end else
        } // end for
        cout << "Decimal is: " << Decimal << endl; // display result
    } // end else if
    return 0 ;
} // end main
