#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int x;
    int temp=1;
    int sum=0;

    cout << "Enter a binary number: ";
        if ( ( cin >> x == false ) || ( x < 0 ) ) //error detection : if receive a character or a negative number , program is terminated.
        {
            if( x < 0 )
            cout << "Input can't be negative." << endl;
            else
            cout << "Invalid input." << endl;
            return 0;
        }

        while(x>0)
        {
            if ((x%10)>= 2) //error detection : if input is not a binary number, end the program.
                {cout << "Can't be binary input." << endl;
                return 0;}

            else //binary to decimal
                sum=sum + temp * (x%10);
                temp=temp * 2;
                x = x / 10;
        }
        cout<<"Decimal is: "<<sum<<endl;

    return 0;
}
