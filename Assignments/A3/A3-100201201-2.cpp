#include<iostream>
#include<iomanip>
#include<cctype>
using namespace std;

int main()
{
	
	int num,sum=0,y=1,t,x;
	
	cout << "Enter a binary number: ";
	cin >> num;
	
	if(isalpha(num)!=0)  //如果輸入非英文字母就執行下列結果 
	{
							
		while(num!=0)  //當num還不是0的時候,重複執行while迴圈 
		{
		 	sum=sum+((num%10)*y); // 
		 
		 	t=(num%10);  //t為num除以10的餘數 
		 	if(t==0||t==1) //若t為0或1就執行下列動作 
		 	{
		   		y=y*2;  //代表2的y次方	
		    	num=num/10;  //num除以10,變成整數,再重複執行一次while迴圈 
		 	}
		 	else        //若t不是0或1,代表輸入的num不是 binary number
		 	{           // 就把這樣的結果設成x=1,然後跳出程式,執行下方的if 
				x=1;
		    	break;			
			}				
	    }						
		     
        if(x!=1)  //若x不是1,代表進來的數字是原本輸入的  binary number已經轉成Decimal 
	      	cout << "Decimal is: "<< sum; 	  	  
	    else  //若x=1,代表輸入的num不是 binary number 
		{
		    cout<<"Can't be binary input.";
		    return 0;
	    }		
	}  
	  
	else  // num是英文字母 
	{
		cout<<"Invalid input.";
		 return 0;
	}
			          	
}
