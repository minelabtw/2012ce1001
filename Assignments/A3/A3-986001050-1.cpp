//
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    float a,tax;    //宣告浮點數
    cout << "Enter the salary per month (negative to end): ";
    cin >> a;
    if (a>0)    //salary>0
    {
        if (a<15000)    //salary 15000 以下則 tax 為 0.1 * salary
            tax=0.1*a;
        else
            if (a<=50000)   //salary 15000 以上(包含)且50000以下則tax為0.2*salary
                tax=0.2*a;
            else            //salary 50000 以上(包含)則tax為0.3*salary
                tax=0.3*a;
    //輸出浮點數 tax，並精確到小數點後 3 位數
    cout << "Tax :" << fixed << setprecision( 3 ) << tax << endl;
    }

    return 0;
}
