#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int input,x=0,y=0,a=0,b=0;//設定變數

    cout << "Enter a binary number: ";//第一行要顯示的字串
    cin >> input;//輸入要分析的變數input

    if(input<0)//第一個分析條件，若是input<0，
    cout << "Input can't be negative." << endl;//達成上方條件，即輸出此字串

    else if (cin==false)//第二個分析條件，若是輸入的變數無法比大小，即非數字
    cout << "Invalid input." << endl;//則顯示此行字串

    else if (input>0)//第三個分析條件，若是input>0
    {
        while(input!=0)//此迴圈為判斷是否為二進位
        {
            x=input%10;//設一變數x儲存為input除以10之餘數
            input=input/10;//新變數input變成舊變數input除以10後的變數

            if(x>1)//每當出現一個x>1的時候
            y++;//多設一變數y，用來計算有幾個x是大於1的

            b+=x*pow(2,a);//設一變數b，為上方的餘數x乘上2的a的次方，為二進位變成十進位的計算方式
            a++;//a是位數，一路累積上去
        }

        if(y<1)//當y<1的時候，即確認此數字只由1與0構成，是二進位的形式
        cout << "Decimal is: " << b << endl;//輸出字串與轉換為十進位的答案b

        if(y>=1)//若是y>=1，可判斷此數並不屬於二進位，於是不需要再繼續待在迴圈裡做運算
        cout << "Can't be binary input." << endl;//直接輸出字串
    }

    return 0;
}
