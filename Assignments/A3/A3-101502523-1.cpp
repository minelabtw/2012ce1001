#include <iostream>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;
int main()
{
    float salary,tax;

    cout << "Enter the salary per month (negative to end): ";

    while( cin >> salary )// if cin encounter an invalid input( such as character ), it will return false then break the loop.
    {
        if( salary < 15000 )
            tax = 0.1 * salary;
        if( salary >= 15000 )
            tax = 0.2 * salary;
        if( salary >= 50000 )
            tax = 0.3 * salary;
        if( salary < 0 )// if salary is a negative number, program is terminated.
            return 0;

    cout << "Tax : " << fixed << setprecision( 3 ) << tax << endl;
    cout << "Enter the salary per month (negative to end): ";
    }
    return 0;
}
