#include <iostream>
#include <math.h> //declare "pow" in this scope

using namespace std;

int main()
{
    int x,y,z=0,i=0; //declare four variables,of which "z" and "i" starts from 0
    cout << "Enter a binary number: "; //print out a request on the screen
    while(cin >>x) // set the 1st loop,of which the condition to enter is entering an integer
    {
        if ( x<0 )
        {
            cout << "Input can't be negative." << endl;
            return 0; //if the variable "x" entered is negative,then print out an error message on the screen and jump off the loop and function
        }

        for(x;x>=1;i++) //set the 2nd loop to convert the binary number into a decimal number
        {
            y=x%10; //the second variable "y" is the remainder of "x"/10
            if( y==0 )
                x=x/10; //if y is 0,then execute x/10
            else if ( y==1 )
                x=x/10; //if y is 1,then execute x/10
            else
                {
                    cout << "Can't be binary input." <<endl;
                    return 0; //if the variable "x" entered can't be a binary number,then print out an error message on the screen and jump off the whole loop and the function
                }
            z=z+y*pow(2,i); //this is an equation to convert the binary number entered into a decimal one
        }
        cout << "Decimal is: " << z << endl;
        return 0; //print out the decimal number converted from the binary number entered and then jump off the whole loop and the function
    }
    cout << "Invalid input." << endl; //if the variable "x" entered isn't an integer,then print out an error message on the screen
    return 0; //return a number 0
}
