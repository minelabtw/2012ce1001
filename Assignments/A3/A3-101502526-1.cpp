#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    double salary,tax;//先訂salary和tax為double型態
    cout << "Enter the salary per month (negative to end): " ;//輸出要求輸入
    for (;cin>>salary;)//開始迴圈，輸入若數字則進行否則結束迴圈且結束程式
    {
        if (salary>=0)//若salary大於等於0則進一步判斷，否則結束程式
        {
            /*
            若salary<15000則tax = salary*0.1，
            若salary>=15000且salary<50000則tax = salary*0.2，
            若salary>=50000則tax = salary*0.3，
            輸出tax值且精確到小數點後3位數，
            再一次輸出要求輸入，且繼續迴圈。
            */
            if (salary<15000)
                tax = salary*0.1;
            if (salary>=15000 && salary<50000)
                tax = salary*0.2;
            if (salary>=50000)
                tax = salary*0.3;
            {
                cout << "Tax : " << fixed << setprecision(3)<< tax << endl;
                cout << "Enter the salary per month (negative to end): " ;
            }
        }
        else
        return 0;
    }
    return 0;
}
