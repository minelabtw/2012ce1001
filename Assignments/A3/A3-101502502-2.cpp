#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int num1;
    int num2;
    int i=0;
    int sum=0;

    cout << "Enter a binary number: ";

    if ( (cin >> num1) == false ) //recognize whether the input is letter or not
    {
        cout << "Invalid input." << endl;
        return 0;
    }// end if

    else if ( num1 < 0 ) //recognize whether the input is negative or not
    {
        cout << "Input can't be negative." << endl;
        return 0;
    }// end else if

    else if ( num1 % 10 > 1 ) //recognize whether the input is binary or not
    {
        cout << "Can't be binary input." << endl;
        return 0;
    }// end else if

    while ( num1 > 0 ) //calculate from binary to decimal
    {
        if( (num1 % 10) == 1 )
            sum += pow(2, i);// using the header file to calculate the square about 2

        i ++;
        num1 = num1 / 10;
    }//end while
    cout << "Decimal is: " << sum << endl;
    return 0;
}
