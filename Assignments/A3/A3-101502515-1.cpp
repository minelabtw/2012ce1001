#include <iostream>
#include <iomanip>
using namespace std; 

int salary;
double tax;

int main()
{
	while(salary>=0)
	{cout << "Enter the salary per month (negative to end): ";
	 cin >> salary;
	 if(salary<0)
		 break;
	 if(salary<15000)
		tax=salary*0.1;
	 if(salary>=15000 && salary<50000)
		tax=salary*0.2;
	 if(salary>=50000)
		tax=salary*0.3;
	 cout <<"Tax : "<< fixed << setprecision( 3 ) << tax << endl;
	}
	return 0;
}