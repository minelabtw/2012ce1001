#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int bin,countpower=0,decimalresult=0;
    bool decide=true;  //bin存放初值；countpower計算次方；decimalresult是結果
                       //decide決定是否印出結果
    cout<<"Enter a binary number: ";
    if(cin>>bin==false)//檢查bin是否符合int格式
        cout<<"Invalid input.";
    else if(bin<0)
        cout<<"Input can't be negative.";
    else
    {
        while(bin!=0)
            if(bin%10==1 || bin%10==0)
            {//判斷該數字是否符合二進位
                decimalresult=decimalresult+(bin%10*pow(2,countpower));
                countpower++;//次方增加
                bin=bin/10;  //去除bin的個位數
            }
            else
            {//數字不符合二進位則跳出迴圈並決定不顯示結果
                cout<<"Can't be binary input.";
                decide=false;
                break;
            }
        if(decide)
            cout<<"Decimal is: "<<decimalresult;
    }
    return 0;
}
