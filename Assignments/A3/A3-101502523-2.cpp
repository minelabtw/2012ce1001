#include <iostream>
#include <iomanip>
#include <math.h>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int binary,decimal,n;
    n = 0;
    decimal = 0;

    cout << "Enter a binary number: ";
    cin >> binary;

    if( binary < 0 )//if input is negative, program is terminated.
    {
        cout << "Input can't be negative." << endl;
        return -1;
    }

    if( !cin )//if cin encounter an invalid input( such as character ), it will return false then break the loop.
    {
        cout << "Invalid input." << endl;
        return -1;
    }

    while( binary > 0 )
    {
        if( binary % 10 == 1 )//for binary number changing to decimal number.
        {
            decimal = decimal + pow(2,n);
            binary = ( binary - 1 ) / 10;
            n = n + 1;
        }
        else if( binary % 10 == 0 )//for binary number changing to decimal number.
        {
            decimal = decimal;
            binary = binary / 10;
            n = n + 1;
        }
        if( binary % 10 > 1 )//if input is not included 1 or 0 (such as 5,7 etc.), program is terminated.
        {
            cout << "Can't be binary input." << endl;
            return -1;
        }
        if( binary == 0 ) break;//to break the infinite loop at 0.
    }
    cout << "Decimal is: " << decimal << endl;
    return 0;
}
