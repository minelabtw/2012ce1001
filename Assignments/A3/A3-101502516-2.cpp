#include <iostream>
using namespace std;

int main()
{
    int a,c; // 假設2個變數
    int d = 1; // 變數d等於1
    int sum = 0; // 假設變數sum等於0


    cout << "Enter a binary number: ";  // display message


    if( (cin >> a == false ) || ( 0 > a ) ) // 如果a不是整數，則顯示錯誤資訊；如果a為負數，則顯示錯誤訊息
    {
        if ( 0 > a )
            cout << "Input can't be negative.";
        else
            cout << "Invalid input.";
        return -1;  // indicate that program ended successfully
    }

    int b = a; // 假設1個變數b等於a
    for ( ; a > 0 ; a = a / 10 ) // 判斷條件，a大於0，且b等於b除於10則執行程式
        if ( a % 10 > 1 ) // 如果a除於10的餘數大於1
        {
            cout << "Can't be binary input."; // 顯示錯誤資訊
            return -1;
        }


    for( ; b > 0 ; b = b / 10 ) // 判斷條件，b大於0，且b等於b除於10則執行程式
        {
            c = b % 10 * d; // 運算公式
            d = d * 2;
            sum += c;
        }

    cout << "Decimal is: " << sum << endl; // display message

return 0; // indicate that program ended successfully
} // end function main
