#include<iostream>
#include<iomanip>
#include<cstdlib>

using namespace std;
using std::setprecision;
using std::fixed;

int main()
{
    int salary;                                                                 //set the varible salary is a integer
    float tax;                                                                  //set the answer tax is float number
 
    cout << "Enter the salary per month (negative to end) :";                     
    cin >> salary;                                                              //input the given number 
    
    while ( salary >=0  ) {                                                     //determine if the given number is positive
       if ( salary < 15000 )                                                    //determine the amount of initial numebr
       { tax=0.1*salary;                                                        //output the final tax , and fixed in 3 precision.
         cout << "Tax :" << fixed << setprecision( 3 ) << tax << endl;}  
       else if ( (salary >=15000) && (salary <50000) )
       { tax=0.2*salary; 
         cout << "Tax :" << fixed << setprecision( 3 ) << tax << endl;}   
       else
       { tax=0.3*salary; 
         cout << "Tax :" << fixed << setprecision( 3 ) << tax << endl;}
         
        cout << "Enter the salary per month (negative to end) :";               //repeat the steps until the given nimber is less than 0
        cin >> salary;
       }
    
    system("pause");                                                            //stop program
    return 0;

    }
