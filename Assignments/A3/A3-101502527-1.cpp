#include<iostream>
#include <iomanip>
using namespace std;
int main()
{
    double a;//20行以內
    while(1)
    {   cout<<"Enter the salary per month (negative to end): ";
        cin>>a;
        if(a<0)
            break;
        else if(a<15000)
            cout<<"Tax : "<<fixed<<setprecision(3)<<a*0.1<<endl;
        else if(a>=15000&&a<50000)
            cout<<"Tax : "<<fixed<<setprecision(3)<<a*0.2<<endl;
        else if(a>=50000)
            cout<<"Tax : "<<fixed<<setprecision(3)<<a*0.3<<endl;
    }
    return 0;
}
