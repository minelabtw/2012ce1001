# include <iostream>
using namespace std;

int main()
{
    int binary,c,sum;//c為2的次方的值 sum為十進位的值

    c=1,sum=0;

    cout << "Enter a binary number: ";


    if((cin  >> binary==false)||(binary<0))
    {//1
        if(binary<0)
            {//2-1
                cout << "Input can't be negative.";
            }//2-1
        else
            {//2-2
                cout << "Invalid input.";
            }//2-2

        return 0;
    }//1
    while(binary>=1)
    {//1
        if(binary%10>=2)
        {//2
            cout<<"Can't be binary input.";
            return 0;
        }//2
        sum=sum+(c*(binary%10));
        c=c*2;
        binary=binary/10;
    }//1
    cout << "Decimal is: " << sum <<endl;

    return 0;

}
