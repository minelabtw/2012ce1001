#include <iostream>
using namespace std;//使用名稱空間std下的名稱
int main()//主函式
{
    int x,integer,y = 0,z = 1;//宣告整數
	cout << "Enter a binary number: ";//顯示於銀幕
	cin >> x;//輸入一整數

    if ( cin == false )//若輸入為0或非數字
    {
        cout << "Invalid input." << endl;//則顯示為無效輸入
        return -1;//程式結束後傳回-1
    }
	else if ( x < 0 )//若整數小於0
	{
        cout << "Input can't be negative." << endl;//顯示錯誤於銀幕
        return -1;//程式結束後傳回-1
	}
    else
        {
        while ( x >= 1 )
            {
            integer = x%10;
            if (( integer != 0 ) && ( integer != 1 ))//若餘數不為0或1
                {
                cout << "Can't be binary input." << endl;//則不符合二進位
                return -1;//程式結束後傳回-1
                }
            else
                {
                y = y + z * ( x % 10 );//二進位轉十進位公式
                z = z * 2;
                }
             x = x / 10;
            }
        cout << "Decimal is: " << y << endl;//顯示結果
		}
	return 0;//程式結束後回傳0
}
