#include <iostream>
#include <iomanip>
using namespace std;//不用加std::

int main()
{
    int i ;//假設變數
    float salary,tax;

    cout <<" Enter the salary per month (negative to end): " ;//第一航輸出文字
    while( cin >> i )//假設迴圈
    {
        salary=(double)i;//假設變數是浮點數
        if(salary<0)return 0;//如果薪水小於0，則程式直接結束
        if(salary<15000)tax = salary * 0.1;//如果薪水低於15000,則稅金是薪水的0.1倍
        if(salary>=15000&&salary<50000)tax = salary * 0.2;//如果薪水大於等於15000且小於50000，則稅金是薪水的0.2倍
        if(salary>50000)tax = salary * 0.3;//如果薪水大於50000，則稅金是薪水的0.3倍
        cout << "Tax : " <<fixed << setprecision( 3 ) <<tax<< endl;//輸出稅金到小數點後三位
        cout<<"Enter the salary per month (negative to end):";//輸出要經過計算的薪水值

    }

    return 0;
}
