# include<iostream>
# include <iomanip>
using namespace std;

int main()
{
    int salary;
    float a;

    cout << "Enter the salary per month (negative to end): ";
    cin  >> salary;

    if(salary<0)//if salary is negative
    {
        return 0;
    }
    else if(salary<15000)//if 0<salary<15000
    {
        a=salary*0.1;
        cout << "Tax : " << fixed <<setprecision( 3 ) << a << endl;
    }
    else if(salary<50000)//if 15000<=salary<50000
    {
        a=salary*0.2;
        cout << "Tax : " << fixed <<setprecision( 3 ) << a << endl;
    }
    else//if 50000<=salary
    {
        a=salary*0.3;
        cout << "Tax : " << fixed <<setprecision( 3 ) << a << endl;
    }

    return 0;
}
