//101502504
#include <iostream>//allows program to output data to the screen
#include <iomanip>
using namespace std;//program uses

int main()//function main begins program exection
{
    float tax,salary;//variable declarations
    cout << "Enter the salary per month (negative to end): ";//prompt user to data

    while ( cin >> salary && salary >= 0 )//loop until salary != 0
    {
        if ( salary < 15000 )//salary < 15000
            tax = 0.1*salary;
        if ( salary >= 15000 && salary < 50000 )//15000 < salary < 50000
            tax = 0.2*salary;
        if ( 50000 <= salary )//50000< salary
            tax = 0.3*salary;

        cout << "Tax : " << fixed << setprecision(3) << tax << endl;//shoe tax
        cout << "Enter the5 salary per month (negative to end): ";//keep going
    }

    return 0;
}
