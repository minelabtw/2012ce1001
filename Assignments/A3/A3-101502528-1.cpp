#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    double salary;

    while ( 1 )//一個無窮迴圈
    {
       cout << "Enter the salary per month (negative to end): ";
       cin >> salary;

       if ( salary >= 50000 )//若薪水大於等於50000,稅等於薪水乘以0.3
       cout << "Tax : " << fixed<< setprecision( 3 )  << salary * 0.3 << endl;

       else if ( salary >= 15000 )//若薪水小於50000且大於等於15000,稅等於薪水乘以0.2
       cout << "Tax : " << fixed<< setprecision( 3 )  << salary * 0.2 << endl;

       else if ( salary >= 0 )//若薪水小於15000且大於等於0,稅等於薪水乘以0.1
       cout << "Tax : " << fixed << setprecision( 3 ) << salary * 0.1 << endl;

       else if ( salary < 0 )//若薪水輸入一個負數則跳出此迴圈
       break;

    }

    return 0;

}
