#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int number,remainder,total=0,count=0;//先定義number,remainder,total,count為整數
    cout << "Enter a binary number: ";//輸出要求輸入
    if( cin >> number == false)//要求輸入number，且若非數字則顯示錯誤
    {
        cout << "Invalid input. " << endl;//輸出錯誤的輸入
        return 0;//結束程式
    }
    if (number>=0)//假使number>=0
    {
        /*
        假使輸入的數字不管有沒有經過運算後大於等於1的話則進行迴圈，
        先求出number除10的餘數，
        總值會等於原本的加上2的count次方(起始值為0)乘上餘數，
        之後count遞增，且number/10
        */
        for (;number>=1;)
        {
            remainder=number%10;
            total=total+(pow(2,count)*remainder);
            count++;
            number=number/10;
            /*
            若餘數大於1，則代表輸入的並非二進位
            */
            if (remainder>1)
            {
                cout << "Can't be binary input. " << endl;
                return 0;
            }
        }
        cout << "Decimal is: " << total;//輸出十進位的值
        return 0;//結束程式
    }
    if(number<0)//若輸入的值為負數
    {
        cout << "Input can't be negative. " << endl;//輸出不能為負數
        return 0;//結束程式
    }
    return 0;
}

