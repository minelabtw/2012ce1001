#include <iostream>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;

int main()
{
    while(1)
    {
        int a,b;
        cout<< "Enter the salary per month : ";
        cin>>a;

        if (0<=a&&a<15000)//設定範圍
            {
            float b;
            b=a*0.1;
            cout << fixed << setprecision( 3 )<<"Tax : "<< b << endl;
            }
        if(15000<=a&&a<50000)//設定範圍
            {
            float b;
            b=a*0.2;
            cout << fixed << setprecision( 3 )<<"Tax : "<< b << endl;
            }
        if (50000<=a)//設定範圍
            {
            float b;
            b=a*0.3;
            cout << fixed << setprecision( 3 )<<"Tax : "<< b << endl;
            }
        if(a<0)//a小於0,結束迴圈
        break;
    }

    return 0;
}
