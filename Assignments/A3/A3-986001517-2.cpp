#include <iostream>                                         /*將下列函數會用到的相關標頭檔涵括*/ 
#include <ctype.h>
#include <math.h>
using namespace std;                                        /* std的命名空間,可以縮減cin、cout、endl的原始寫法*/ 
int main()                                                  /*函數主體*/ 
{   
    char bin[20];                                           /*宣告一個字串型態之變數bin，用以存取輸入值*/ 
    int  dec ,i ,bin1,bin2,;                                /*宣告其他整數型態之變數，供迴圈運算使用 */ 
    cout << " Enter a binary number:" ;              
    cin >>  bin ;
    dec=0;                                                  /*設定 起始值為0*/ 
     if( bin[0]== '-' )                                     /*判斷輸入字串第一位是否為負號，有則不符合題意而跳躍到end*/ 
    {
     cout << " Input can't be negative " << endl;
     goto end;
     }
    for (i=0;i<strlen(bin);i++)                             /*使用for迴圈做反覆的檢驗*/ 
    { 
      if( isdigit(bin[i]) == 0)                             /*檢查字串中每個元素是否皆為數值 ，有則不符合題意而跳到end*/ 
      {
        cout<< " Invalid input " << endl;
      goto end ;
      }        
    }
    bin1=atoi(bin);                                          /*確定為數值，將字串轉換成整數 ，寫入bin1*/ 
    while ( bin1>1 )                                         /*使用while迴圈做最後一步檢驗和計算*/ 
    {
     if ( bin1%10==0 || bin1%10==1 )                         /*逐步檢查每一個字元*/ 
      {
        bin1=bin1/10;
      }
     else                                                     /*只要有大於1的字元，即不符合題意，跳至end*/ 
      {
       cout<< " Can't be binary input" << endl;
       goto end;
       }
     }
      bin2= atoi(bin);                                        /*確定為2進位的數值，再一次轉換成整數並寫入bin2*/ 
      for (i=0;i<20;i++)                                      /*使用for迴圈作2進位轉10進位的計算*/ 
      {
      dec+=pow(2,i)*(bin2%10);
      bin2=bin2/10;
      }
      cout<< " Decimal is : " << dec << endl;                  /*輸出轉換後的10進位數值*/ 
    end:                                                       /*此行為goto跳躍執行之索引*/ 
      system("pause");
      return 0; 
}
    
    
