
#include<iostream>
#include<iomanip>

using namespace std ;

int main()

{
    int salary ;
    float tax ;

    while ( salary >= 0 ) //loop until the value of salary being negative
    {
        cout << "Enter the salary per month (negative to end): " ;
        cin >> salary ; //input salary

        //use "if" to seperate three kinds of paying tax
        if ( salary >= 0 && salary < 15000 )
        {
            tax = 0.1 * salary ; // a way to pay tax
            cout << "Tax : " << fixed << setprecision( 3 ) << tax << endl ; //display the result with decimal points
        }

        if ( 15000 <= salary && salary < 50000 )
        {
            tax = 0.2 * salary ; // a way to pay tax
            cout<<"Tax : "<<fixed<<setprecision( 3 )<<tax<<endl ;
        }

        if ( salary >= 50000 )
        {
            tax = 0.3 * salary ; // a way to pay tax
            cout<<"Tax : "<<fixed<<setprecision( 3 )<<tax<<endl ;
        }

    }

    return 0 ;
}
