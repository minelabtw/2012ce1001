#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
    float a;
    cout<<"Enter the salary per month (negative to end): ";
    cin>>a;
    while (a>=0)//讓數字進入迴圈
    {
        if (a>=50000)//判斷不同收入有不同的稅制
                cout<<"Tax : "<<fixed<<setprecision(3)<<a*0.3<<endl;
        else if (a>=15000)
                cout<<"Tax : "<<fixed<<setprecision(3)<<a*0.2<<endl;
        else if (a>=0)
                cout<<"Tax : "<<fixed<<setprecision(3)<<a*0.1<<endl;
        cout<<"Enter the salary per month (negative to end): ";//輸出所要求的金額
        cin>>a;
    }
    cout<<endl;
    return 0;
}
