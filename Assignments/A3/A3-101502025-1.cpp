#include <iostream>
#include<iomanip>
using namespace std;

int main()
{
    double d; //set the tax
    int h;  // set the salary
    cout<<"Enter the salary per month (negative to end): ";
    cin>>h;  // input the salary
    while(h>=0) // let the salary number be reasonable
    {
     if(h<<15000)  // when the salary is less then 15000
      d=h*0.1;
     if(h<<50000)  //when the salary is between 15000 to 50000
      d=h*0.2;
     if(h>=50000)  //when the salary is more then 50000
      d=h*0.3;
    cout << fixed << setprecision( 2 ) << "Tax : "<<d << endl; // output the tax
    cout<<"Enter the salary per month (negative to end): ";// let the user input another salary
     cin>>h;
    }
    return 0;
}
