#include <iostream>
#include <iomanip>
using std::cout ;
using std::cin  ;
using std::endl ;
using std::setprecision;
using std::fixed;
int main()
{
    float a ;
    cout << "Enter the salary per month (negative to end): " ;
    cin  >> a ;// 輸入 a
    while(a>=0)// 判定a是否正數，若非負數，進入迴圈；若為負數，跳過迴圈，結束程式
    {
        if (a<15000)
            cout << "Tax : "<< fixed <<setprecision(3)<< a*0.1 <<endl;//a<15000 時
        else
            {
            if (a>=50000)
                cout << "Tax : "<< fixed <<setprecision(3)<< a*0.3 <<endl;//a>=50000 時
            else
                cout << "Tax : "<< fixed <<setprecision(3)<< a*0.2 <<endl;//15000<=a<50000 時
            }
        cout << "Enter the salary per month (negative to end): " ;
        cin  >> a ;
    }
    return 0;//回傳
}
