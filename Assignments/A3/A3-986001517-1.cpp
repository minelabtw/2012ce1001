#include <iostream>                                                   /*將下列函數會用到的相關標頭檔涵括*/ 
#include <iomanip>
using namespace std;                                                  /* std的命名空間,可以縮減cin、cout、endl的原始寫法*/ 
int main()                                                            /*函數主體 */ 
{
    float a;                                                          /*宣告一個浮點數型態之變數a，用以儲存輸入的數值 */ 
    cout <<" Enter the salary per month (negative to end): " ;
    cin >> a;
    if (a>=50000)                                                     /*依題判斷數值a位於那個區域並套用該區域之公式，以最狹隘的條件先判斷 ，並將輸出值取到小數點第三位*/ 
    {
     cout << " Tax : " << fixed << setprecision( 3 ) << 0.3*a << endl; /*輸出題目所需之答案形式*/        
               }
    else
     if (a>=15000)
    {
     cout << " Tax : " << fixed << setprecision( 3 ) << 0.2*a << endl;          
               }          
    else
     if (a>=0)
    {
     cout << " Tax : " << fixed << setprecision( 3 ) << 0.1*a << endl;          
               }    
    else                                                               /*如輸入數值不符合題目所要求，則無任何數值輸出*/ 
    {} 
    system("pause");
    return 0;
    }
    
    
