#include <iostream>
#include<cmath> //standard C++ math library
using namespace std;

int main()
{
    int Binary,Decimal = 0,x = 0;
    cout << "Enter a binary number: ";

    if ( cin >> Binary == false || Binary < 0 )
    {
        if ( Binary < 0 )
            cout << "Input can't be negative." << endl; // input must be positive
        else
            cout << "Invalid input." << endl; // input can't be character
        return 0;
    }

    for ( Binary ; Binary >= 1 ; x++ )
    {
        if ( Binary % 10 == 0 || Binary % 10 == 1 )
            Decimal += Binary % 10 * pow( 2 , x ); //transform binary into decimal
        else
        {
            cout << "Can't be binary input." << endl; //input is a binary number
            return 0;
        }
        Binary = Binary / 10;
    }
    cout << "Decimal is: " << Decimal << endl;
    return 0;
}
