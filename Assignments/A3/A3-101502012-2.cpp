#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;
int main()
{
    double binary;//To let binary can be decimal fraction
    int integer,count=0,answer=0;

    cout << "Enter a binary number: ";
    cin >> binary;

    if ( binary < 0 ) //error detection : if binary is negative,  program is terminated.
        cout << "Input can't be negative." << endl;

    if ( ( cin == false ) || ( binary < 1 ) && ( binary > 0 ) ) //error detection : if binary is decimal fraction or character, program is terminated.
        cout << "Invalid input." << endl;

    if ( ( binary > 0 ) && ( cin != false ) ) //operate if binary > 0
    {
        while ( binary >= 1 )//loop until binary <1
        {
            integer = static_cast<int>(binary)%10; //change binary into integer
            if ( ( integer != 0 ) && ( integer != 1 )) //error detection : if binary is not binary iput, program is terminated.
            {
                cout << "Can't be binary input." << endl;
                return -1;
            }
            else
                answer = answer + integer * pow(2,count);//change binary into decimal

            binary = binary / 10;
            count++;//count+1
        }
        cout << "Decimal is: " << answer << endl;
    }
    return 0;
} //end main
