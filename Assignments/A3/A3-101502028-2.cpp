#include <iostream>
#include <cstdlib>
using std::cout;
using std::cin;
using std::endl;
int main ()
{
    int binary, decimal = 0, base = 1, split;
    cout << "Enter a binary number: ";
    cin >> binary; // enter a binary number
    if ( cin == false || binary < 0 ) // the program will see if the entered number is a character o a number lower than 0
    {
        if ( cin == false ) // if the entered number is any character
        {
            cout << "Invalid input." << endl; // the program send this message
            return -1;
        }
        else // if the entered number is lower than 0
        {
            cout << "Input can't be negative." << endl; // the program send this message
            return -1;
        }
    }
    if ( binary > 0 ) // if the input is a number
    {
        while ( binary >= 0 ) // repet the process until the last digit is checked
        {
            split = ( binary % 10 ); // the program will take the first digit
            if ( split != 1 && split != 0 ) // if the split number isn't 1 or 0
            {
                cout << "Can't be binary input." << endl; // appears this message
                return -1;
            }
            split = split * base; // take the digit multiply by the first base
            base = base * 2; // it's a binary number so the base will multiply by 2
            binary = binary / 10; // take the next digit
            decimal = decimal + split; // the decimal will add all splited digit
            if ( binary == 0 )
            break;
        }
    }
        cout << "Decimal is: " << decimal << endl; // demostrate all added number
        return 0;
}
