#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int a,b,c=0,i=0;

    cout << "Enter a binary number: ";
    while(cin >>a)
    {
        if (a<0)
        {
            cout << "Input can't be negative." << endl;//使a<0的數不能計算並跑出Input can't be negative.
            return 0;
        }

      for(a;a>=1;i++)
        {
            b=a%10;
            if(b==1)
                a=a/10;
            else if (b==0)
                a=a/10;
        else
            {
                cout <<"Can't be binary input."<<endl;//使不是2進位的數跑出Can't be binary input.
                return 0;
            }
        c=c+b*pow(2,i);//使2進位換成10進位
        }
        cout <<"Decimal is: "<<c<<endl;
        return 0;
    }
    cout <<"Invalaid input."<<endl;//使不是數字的字跑出Invalaid input.
    return 0;
}
