#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int a,s=0;
    cout <<"Enter a binary number: ";
    if ((cin>>a==false)||(a<0))//若a為錯誤輸入或負數，進入回圈
    {
        if (a<0)//a<0時
            cout <<"Input can't be negative."<<endl;
        else//a為錯誤輸入時
            cout << "Invalid input." << endl;
        return -1 ;//回傳-1 結束
    }
    for(int c=1;a>0;a=a/10)//a>0近入迴圈，每次執行完，a除以10
    {
        if(a%10>1)//若a/10之餘數大於1，即不合2進位
        {
            cout <<"Can't be binary input."<<endl;
            return -1;//回傳-1 結束
        }
        s=s+a%10*c;//總和即上輪總和+此輪餘數*c
        c=c*2;//c變成原來2倍，即1.2.4.8.16.32.....
    }
    cout <<"Decimal is: "<< s<<endl;//輸出總和s
    return 0;//回傳0 結束
}
