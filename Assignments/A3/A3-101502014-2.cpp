#include <iostream>
using namespace std;
int main()
{
    int binary , decimal , count = 1 , ans = 0 , c = 1;//count restore place value.
    cout << "Enter a binary number: ";
    if(cin >> binary == false)//error detection : if receive a character , program is terminated.
        cout << "Invalid input." << endl;
    else if(binary < 0)//error detection : if receive a negative number , program is terminated.
        cout << "Input can't be negative." << endl;
    else
    {
        while(binary > 0)
        {
            decimal = binary % 10;//get every figure in binary number.
            if(decimal > 2)//error detection : if receive a number excepting 0 or 1 , c will change then break the loop.
            {
                cout << "Can't be binary input." << endl;
                c = 0;
                break;
            }
            binary /= 10;//move to the next figure.
            ans += decimal * count;//convert the integral part of a number in decimal to binary.
            count *= 2;//change the place value.
        }
        if(c == 1)//if c does not change , then output the answer.
            cout << "Decimal is: " << ans << endl;
    }
	return 0;
}
