#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int number,i,sum,j,number2;
    sum=0;
    number2=number;
    //輸入
    cout<<"Enter a binary number : ";
    cin>>number;


    //除錯
    if (number<0)
    {//當輸入小於零,出現錯誤訊息
        cout<<"Input can't be negative.";
        return -1;
    }
    if(!cin)
    {//當輸入錯誤字元,出現錯誤訊息
        cout<<"Invalid input.";
        return -1;
    }

    for(;number2>0;)
    {//當輸入的不是二進數字位時,出現錯誤訊息
        if(number2%10>1)
        {
            cout<<"Can't be binary input.";
            return -1;
        }
        number2/=10;
    }


    for(i=1;number>0;i=i*2)
    {//將二進位換成十進位
        sum=sum+number%10*i;
        number = number /10;
    }


    //輸出
    cout<<"Decimal is : "<<sum;


    return 0;
}
