#include <iostream>
#include <iomanip>
using namespace std;

int main()
{

    float s=0;

    while(s>=0)      //若輸入為負數則跳過迴圈
    {
        cout << "Enter the salary per month (negative to end): ";

        cin >> s;

        if(s>=0&&s<15000)
            cout << "Tax : " << fixed << setprecision( 3 ) << s*0.1 << endl;  //介於0到15000者乘0.1

        if(s>=15000&&s<50000)
            cout << "Tax : " << fixed << setprecision( 3 ) << s*0.2 << endl;  //介於15000到50000者乘0.2

        if(s>=50000)
            cout << "Tax : " << fixed << setprecision( 3 ) << s*0.3 << endl;  //大於等於50000者乘0.3
    }

    return 0;
}
