// Assignment #3-2
// A3-976001044-2.cpp 
#include <iostream>
#include <cstdlib>
#include <cmath>
#define n_max 100   // Define a maximum number to setup the array
using namespace std;

int main()
{
    char input[n_max]={'0'};
    int output=0, N=0;
    
    cout << "Enter a binary number: ";
    cin >> input;
    
    // Search entire string with length n_max
    // The character '\0' means end of the string, so i can make sure the length
    for ( int i=0 ; i < n_max ; i++ )
    {
        if ( input[i] == '\0' )
        {
             N = i;
             break;
        }
    }
    
    // Search the entire string with length N
    // which N has been setup by front loop
    for ( int i=0 ; i < N ; i++ )
    {
        // Functional phase
        // The condition which is invalid input part
        if ( (input[i] != '0') && (input[i] != '1') )
        {
             switch (input[i])
             {
                    case '-':
                            cout << "Input can't be negative." << endl;
                            system("pause");
                            exit(1);
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                            cout << "Can't be binary input." << endl;
                            system("pause");
                            exit(1);
                    default :
                            cout << "Invalid input." << endl;
                            system("pause");
                            exit(1);
             }
        }
        
        // Functional phase
        // The condition which is effective input part
        // Only the character '1' needs to computing
        // The computing can be skip, if the character is '0'
        if ( input[i] == '1' )
        {
             output = output + int(pow(2.0,double(N-i-1)));
        }
    }
    
    // Output data phase
    cout << "Decimal is: " << output << endl;
    
    system("pause");
    return 0;
}
