#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main()
{
    string w;
    int binary,decimal=0;

    cout << "Enter a binary number: ";
    getline (cin, w);
    stringstream ss (w);

    if (ss >> binary)   //確認binary是否為整數
    {
        if (binary >= 0)    //確認binary是否大於等於零
        {
            int power = 1;  //一開始 2^0 =1
            while (binary>0)    //當 binary/10 小於等於零時停止迴圈
            {
                int mod = binary%10;    //算出最後一位之餘數(1 or 0)
                if (mod < 2)  //檢查每一位數，判斷是否是二進位
                {
                    decimal=decimal+mod*power;  //(0 or 1 )*2^n
                    power=power*2;  //2^n每往前一位，2^n 次方加一
                    binary=binary/10;   //binary往前移一位
                }
                else
                {
                    cout << "\nIt must be binary input.\n";
                    break;
                }
            }
            cout << "\nDecimal is: " << decimal << endl;
        }
        else
            cout << "\nInput can't be negative.\n";
    }
    else
        cout << "\nInvalid input.\n";

    return 0;
}
