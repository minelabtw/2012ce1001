#include <iostream>

using namespace std ;

int main()
{
    int a;
    int i;//紀錄 2的次方 
    int sum;//最後10進位的值 
    cout << "Enter a binary number: " ;

    if( cin >> a == false || a < 0 )//檢查輸入 如果是字元或負數 輸出錯誤訊息 
    {
        if( a < 0 ) 
            cout << "Input can't be negative." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }
    
    
    i = 1;  //起始值為  2的零次方 = 1 
    sum = 0;//起始值設為 0 
    while( a > 0)// a 為 int  當a還有值的時候  繼續計算 
    {
      if ( a % 10 == 0 ) // 檢查第一位數 等於零時 不用加進sum 
      {
           i = i*2;      // 增加1次 
           a /= 10;      // 刪去第一位數  讓原來的第二位數 變成第一位數 
      }
      else if ( a % 10 == 1 )  // 第一位數等於零時   
      {
           sum += 1*i;     //  乘以 2的某次方  加進 sum 
           i = i*2;
           a /= 10;
      }
      else               //  不為二進位時  顯示錯誤 
      {
           cout << "Can't be binary input.";
           return -1;
      }
     }
     cout << "Decimal is: " <<  sum ;      //  無錯誤時  輸出 sum  
     

    return 0;
}
