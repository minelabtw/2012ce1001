#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main ()
{
    int a,b,c,d;
    d = 0;//起始總數
    b = 1;
    cout << "Enter a binary number: ";//輸入一個二進位數
    cin >> a;

    if ( cin == false || a < 0 )//發現錯誤和所輸入的數為0
    {
        if( a < 0 )
            cout << "Input can't be negative." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }
    if ( a > 0 )
    {
        while ( a >= 0 )
        {
            c = a % 10;//若所輸入數字除以10，可求出第一個數字
            if ( c != 1 && c != 0 )
            {
                cout << "Can't be binary input." << endl;
            return -1;
            }
        c = c * b;
        a = a / 10;
        b = b * 2;
        d = d + c;//總和
        }
    }
    cout << "Can't be binary input." << endl;

    return 0;
}
