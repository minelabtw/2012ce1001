#include<iostream>
#include<iomanip>
#include<math.h>
using namespace std;

int main()
{
    int a,b,h=0,d=0; // set the numbers needed
    cout<<"Enter a binary number: ";
    while(cin>>a) // let the input number only can be integer
    {
    if(a<0) // let the number > 0
     {
      cout<<"Input can't be negative."<<endl;
      return 0;
     }
    while (a>0)
       {
        b=a%10;
        if (b==1)
           a=a/10; // when the number is 1,we should calculate it
        else if (b==0)
           a=a/10; // if the number is 0, we can not calculate it
        else   // if the remain is not 0 or 1,the number is not reasonable
           {
            cout<<"Can't be binary input."<<endl;
            return 0;
           }
        h=h+b*pow(2,d); // calculate the decimal
        d=d+1; //calculate another integer
        }
    cout<<"Decimal is: "<<h<<endl; //output the resault
    return 0;
    }
    cout<<"Invalid input."<<endl;// if the input number is not a integer, it is invalid
    return 0;
}
