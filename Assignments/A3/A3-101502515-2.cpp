#include <iostream>
#include <ctype.h>
#include <math.h>
using namespace std; 

int main()
{	
	int number;
	int counter = 0;
	int sum = 0;

	cout<<"Enter a binary number: ";
	cin>>number;
	//judge if not a number
	if(cin.fail())
        cout << "Invalid input."<<endl;
	//judge if number is negative
	if(number<0)
		cout<<"Input can't be negative."<<endl;
	//check
	while(number>0)
    {
        if((number%10) != 0 && (number%10) != 1)//if it has not 1 or 0
        {
            cout<<"Can't be binary input."<<endl;
            break;
        }
        sum+=(number%10)*(pow(2.0,counter));
        counter=counter+1;
        number=number/10;

        if(number<=0)//conclude
            cout<<"Decimal is: " <<sum<<endl;
	}
	return 0;
}