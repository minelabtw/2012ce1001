#include <iostream>
using namespace std;//簡化std
int main()
{
    int a,b,c=1,total=0;//設變數
    cout<<"Enter a binary number: ";//螢幕顯示
    if(cin>>a)//若輸入為數字則繼續
    {
        while(a>0)//若a大於零則進入迴圈
        {   b=a%10;//b為a除以10的餘數(a的尾數)
            if (b>1)//若b大於1
            {
                cout<<"Can't be binary input."<<endl;//則輸出文字
                return 0;//並結束
            }
            else if (b==1)//若b等於零
                {
                    total=total+b*c;//則總和加上b乘以2的指數
                    a=(a-1)/10;//除以十以便取得a的下一個位數
                }
            else  if (b==0)//若b等於零
                {
                    a=a/10;//則總和不便直接將a除以十
                }
            c=c*2;//迴圈改變值改變2的指數
        }
        if(a<0)//若a小於零
        {
            cout<<"Input can't be negative."<<endl;//則輸出文字
            return 0;//並結束
        }
        cout<<"Decimal is: "<<total<<endl;//輸出文字(十進位的結果)
    }
    else
    cout<<"Invalid input."<<endl;//若輸入部為數字實則輸出此
    return 0;
}
