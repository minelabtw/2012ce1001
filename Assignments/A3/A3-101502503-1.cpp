//101502503
#include <iostream>// allows program to output data to the screen
#include <iomanip>

using namespace std;

// funtion main begins program execution
int main()
{
    int salary ;// variable declarations
    float tax ;

    cout << "Enter the salary per month (negative to end): " ;

    while ( cin >> salary && salary >= 0 )
    {
        // determine whether salary is smaller than 15000
        if ( salary < 15000 )
        tax = 0.1 * salary;

        // determine whether salary is greater than 15000 and smaller than 50000
        if ( salary >= 15000 && salary < 50000 )
        tax = 0.2 * salary;

        // determine whether salary is greather than 15000
        if ( salary >= 50000 )
        tax = 0.3 * salary;

        cout << "Tax : " << fixed << setprecision( 3 ) << tax << endl;
        cout << "Enter the salary per month (negative to end): " ; // display result
    } // end while

    return 0 ;
} // end main
