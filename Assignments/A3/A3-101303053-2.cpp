#include <iostream>
#include <string>
#include<cstdlib>
#include<sstream>
using namespace std;

int main()
{
    string bin;
    int dec = 0;
    int i;

    cout << "Enter a binary number:";
    cin >> bin;
    for(int i = 0; i < bin.length(); i++)
        {
	        if(!isdigit(bin[i])&&bin[i]!='-')
			    {
				    cout<<"Invalid input.";
				    return -1;
			    }
			    if(bin[i]!='0'&&bin[i]!='1'&&bin[i]!='-')
                {
                      cout<<"Can't be binary input.";
                      return -1;
                }

        }
    int num;
    istringstream ( bin ) >> num;
    if ( num < 0 )
        {
        cout << "Input can't be negative.";
        return -1;
        }
    for(i = 0; i < bin.length(); i++)
    dec = dec * 2 + (bin[i] - '0');
    cout << "Decimal is:" << dec << endl;


    return 0;

}
