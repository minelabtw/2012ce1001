#include<iostream>
#include<iomanip>

using namespace std ;

int main(){
    float a;
    a = 0; //起始值設為零 


    while( a >= 0){ //如果輸入負數 則不運作 
       cout << "Enter the salary per month (negative to end): " ;
       cin  >> a ;
       if ( a > 0 )
       cout << "Tax : " ;

       
       if ( a >= 0 && a < 15000)     // 15000 以下 乘0.1 
       cout << fixed << setprecision( 3 ) << a*0.1 << endl;
       
       if ( a >= 15000 && a < 50000)// 15000 ~ 50000 乘0.2 
       cout << fixed << setprecision( 3 ) << a*0.2 << endl;

       if ( a >= 50000)              // 50000 以上  乘0.3 
       cout << fixed << setprecision( 3 ) << a*0.3 << endl;
       
    }

    return 0;
}
