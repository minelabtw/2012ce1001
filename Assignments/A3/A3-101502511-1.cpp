#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int salary;//設定變數

    cout << "Enter the salary per month (negative to end): ";//進入迴圈前時的輸入
    while ( cin >> salary  )//輸入為整數時
    {
       if ( salary <0 )break;//負數時跳出迴圈 (這行移到下層則失敗..不知為何
       if ( salary < 15000 )//判斷小於15000
       {
          cout << fixed << setprecision( 3 ) << "Tax : " << ( double )salary * 0.1 <<endl;//計算以及輸出TAX
       }
       if ( (salary >= 15000) && (salary < 50000) )//判斷大於15000跟小於50000
       {
          cout << fixed << setprecision( 3 ) << "Tax : " << ( double )salary * 0.2 <<endl;//計算以及輸出TAX
       }
       if ( salary >= 50000 )//判斷是否大於50000
       {
          cout << fixed << setprecision( 3 ) << "Tax : " << ( double )salary * 0.3 <<endl;//計算以及輸出TAX
       }
       cout << "Enter the salary per month (negative to end): ";//迴圈進行時的輸入
    }
    return 0;
}
