#include <iostream>

using namespace std;

int main()
{
    int binary , decimal , n , i;
        decimal = 0;
        n = 1;

    cout << "Enter a binary number: ";
    cin >> binary;
        i = binary;

    if ( binary < 0 ) //偵錯負數輸入
    {
        cout << "Input can't be negative.";
        return 0;
    }

    if ( !cin ) //偵錯無效輸入，如英文字元
    {
        cout << "Invalid input." << endl;
        return 0;
    }

    while ( i > 0 ) //偵錯非二進位數值輸入
    {
        if ( i % 10 > 1 )
           {
           cout << "Can't be binary input.";
           return 0;
           }

        i = i / 10;
    }

    while ( binary > 0 ) //計算二進位數值轉換為十進位數值
    {
        decimal = decimal + ( binary % 10 ) * n;
        binary = binary / 10;
        n = n * 2;
    }
        cout << "Decimal is: " << decimal;

    return 0;
}
