#include <iostream>//含入標頭檔 iostream
#include <string>//含入標頭檔 string
#include <stdio.h>//含入標頭檔 stdio.h
#include <stdlib.h>//含入標頭檔 stdlib.h
#include <iomanip>//含入標頭檔 iomanip

using namespace std;//使用名稱空間 std

class CTax//定義類別
{
    public://可供一般的敘述或 function以 物件.資料成員 方式取用
        bool Tax();//Tax函式宣告，型別：bool
    private://僅可供 member function 直接取用
        string strSalary;//變數宣告，型別: string
        int iSalary;//變數宣告，型別: int
        void Input();//輸入函式宣告，型別: void
        double TaxCount(int a);//稅金計算函式宣告，型別: double
        bool CheckInput(string str);//檢查輸入函式宣告，型別: bool
};
void CTax::Input()//定義輸入函式
{
    do//執行迴圈
    {
        cout<<"Enter the salary per month (negative to end): ";//印出
        cin>>strSalary;//輸入儲存
    }while(CheckInput(strSalary));//作輸入檢查判斷是否再執行迴圈
}
bool CTax::Tax()//定義 Tax函式
{
    Input();//呼叫類別成員中的輸入函式
    if(iSalary<0)//判斷輸入是否為負數
    {
        return false;//回傳false
    }
    else
    {
        cout<<"Tax : "<<fixed << setprecision( 3 ) <<TaxCount(iSalary)<<endl;//計算並輸出稅金，形式：小數點後三位
    }
    return true;//回傳true
}
double CTax::TaxCount(int a)//定義稅金計算函式
{
    if(a<15000)//判斷輸入大小
    {
        return 0.1*a;//稅金計算
    }
    else if(a<50000)//判斷輸入大小
    {
        return 0.2*a;//稅金計算
    }
    else
    {
        return 0.3*a;//稅金計算
    }
}
bool CTax::CheckInput(string str)//定義檢查輸入函式
{
    if(str.c_str()[0]!=45)//判斷首字是否為負號
     {
        for(int j = 0; j < str.length(); j++)//逐一檢查字串所有字元是否為數字
        {
           if( str.c_str()[j] < '0' || str.c_str()[j] > '9' )
           {
               cout<<"Input error!"<<endl;//顯示輸入錯誤
               return true;//回傳 true
           }
        }
        iSalary=atoi(str.c_str());//將字串型別轉換為整數儲存
        return false;//回傳 false
     }
     else
     {
       for(int j = 1; j < str.length(); j++)//逐一檢查字串第一字元後所有字元是否為數字
        {
           if( str.c_str()[j] < '0' || str.c_str()[j] > '9' )
           {
               cout<<"Input error!"<<endl;//顯示輸入錯誤
               return true;//回傳 true
           }
        }
       iSalary=atoi(str.c_str());//將字串型別轉換為整數儲存
       return false;//回傳 false
     }

}

int main()//主程式 型別: int
{
    CTax tax;//類別CTax 宣告
    while(tax.Tax());//以類別中 Tax函式判斷是否執行迴圈
    return 0;//回傳 0
}
