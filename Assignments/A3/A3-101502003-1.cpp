#include <iostream>
#include <iomanip>

using namespace std; //可以省略std::

int main()
{
    int a; //設變數
    float tax;
    cout << "Enter the salary per month (negative to end): "; //輸出的文字
    cin >> a;
    while ( a >= 0) //設迴圈,如果輸入負數的話就直接結束
    {
        if ( a < 15000) //薪水小於一萬五乘以零點一
         {
             tax = a * 0.1;
         }
        else if ( a < 50000 and a >= 15000 ) //薪水在五萬和一萬五之間的乘以零點二
         {
            tax = a * 0.2;
         }
        else if ( a >= 50000 ) //薪水大於五萬的乘以零點三
         {
             tax = a * 0.3;
         }

        cout << "tax : ";
        cout << fixed << setprecision( 3 ) << tax << endl; //輸出文字,到小數點第三位
        cout << "Enter the salary per month (negative to end): "; //再次輸入數值
        cin >> a;
    }

    return 0;
}
