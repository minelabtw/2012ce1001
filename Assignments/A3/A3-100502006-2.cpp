#include <iostream>//含入標頭檔 iostream
#include <string>//含入標頭檔 string
#include <stdio.h>//含入標頭檔 stdio.h
#include <stdlib.h>//含入標頭檔 stdlib.h
#include <cmath>//含入標頭檔 cmath

using namespace std;//使用名稱空間 std

class CBinToDec//定義類別
{
    public://可供一般的敘述或 function以 物件.資料成員 方式取用
        int BinToDec();//BinToDec函式宣告，型別：int
    private://僅可供 member function 直接取用
        int iBinary;//變數宣告，型別: int
        int BToD(string str,int a);//BToD函式宣告，型別：int
        bool CheckInput(string str);//檢查輸入函式宣告，型別: bool
};
int CBinToDec::BinToDec()//定義BinToDec函式
{
    string strInput;//變數宣告，型別: string
    cout<<"Enter a binary number: ";//印出
    cin>>strInput;//輸入儲存，型別: 字串
    if(CheckInput(strInput))//輸入檢查判斷
    {
        cout<<"Decimal is: "<<BToD(strInput,iBinary)<<endl;//印出
        return 0;//回傳 0
    }
    else
    {
        return -1;//回傳 -1
    }
}
int CBinToDec::BToD(string str,int a)//定義BToD函式
{
   int temp=0;//變數宣告，設置為 0,型別: int
   for(int i=0;i<str.length();i++)//二進位數字轉換為十進位
   {
       temp+=(a%10)*pow(2,i);
       a/=10;
   }
   return temp;//回傳 temp
}
bool CBinToDec::CheckInput(string str)//定義檢查輸入函式
{
    if(str.c_str()[0]!=45)//判斷首字是否為負號
     {
        for(int j = 0; j < str.length(); j++)//逐一檢查字串字元
        {
           if( str.c_str()[j] < '0' || str.c_str()[j] > '9' )//逐一檢查字串所有字元是否為數字
           {
               cout<<"Invalid input."<<endl;//顯示輸入錯誤
               return false;//回傳 false
           }
           if( str.c_str()[j] < '0' || str.c_str()[j] > '1' )//逐一檢查字串所有字元是否為二進位
           {
               cout<<"Can't be binary input."<<endl;//顯示輸入非二進位錯誤
               return false;//回傳 false
           }
        }
        iBinary=atoi(str.c_str());//將字串型別轉換為整數儲存
        return true;//回傳 true
     }
     else
     {
       for(int j = 1; j < str.length(); j++)//逐一檢查字串第一字元後字元
        {
           if( str.c_str()[j] < '0' || str.c_str()[j] > '9' )//逐一檢查字串第一字元後字元是否為數字
           {
               cout<<"Invalid input."<<endl;//顯示輸入錯誤
               return false;//回傳 true
           }
           if( str.c_str()[j] < '0' || str.c_str()[j] > '1' )//逐一檢查字串第一字元後字元是否為二進位
           {
               cout<<"Can't be binary input."<<endl<<"And input can't be negative."<<endl;//顯示輸入非二進位及輸入負數錯誤
               return false;//回傳 true
           }
        }
       cout<<"Input can't be negative."<<endl;//顯示輸入負數錯誤
       return false;//回傳 false
     }

}
int main()//主程式，型別: int
{
    CBinToDec btod;//類別CBinToDec 宣告
    return btod.BinToDec();//回傳類別中 BinToDec函式，型別: int
}
