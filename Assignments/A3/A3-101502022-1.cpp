#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
 double salary;
 float tax;

 cout<<"Enter the salary per month (negative to end): ";
 cin>>salary;


 while(salary>0)//If the salary you cin is less than 0,the salary will not go into this loop.
 {

 if(salary>=15000)
 tax=salary*0.2;

 if(salary<50000)
 tax=salary*0.2;

 if(salary<15000)//if we put the <15000 before the 15000=< salary<50000, when it meets "salary<50000" the answer will be interfered.
 tax=salary*0.1;

 if(salary>=50000)
 tax=salary*0.3;

 cout<<fixed<<setprecision(3)<<"Tax :"<<tax<<endl;//because of fixed<<setprecision(3) ,we can cout the value of TAX"s three number behind the dot.

 cout<<"Enter the salary per month (negative to end): ";//this can stop the loop,and you can print other salary
 cin>>salary;

 }
 return 0;
}
