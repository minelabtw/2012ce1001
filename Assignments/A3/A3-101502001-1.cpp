#include <iostream>
#include <iomanip>

using namespace std;
using std::cout;
using std::endl;
using std::setprecision;
using std::fixed;

int main()
{
    int salary , tax;

    std::cout << "Enter the salary per month (negative to end):" ;//輸出的問題
    std::cin >> salary;//鍵盤輸入的數字

    while (salary >= 0)//迴圈,讓問題一直提問下去

    {

    if ( salary >= 50000 )
     {tax = 0.3 * salary;//如果salary大於或等於50000,則tax等於salary乘以0.3
      float a;//float是小數點
      a = tax;
      cout << fixed << setprecision( 3 ) << "Tax :" << a << "\n";//精確到小數點後第三位
     }

    else if ( salary >= 15000 )
     {tax = 0.2 * salary;//如果salary小於50000且大於等於15000,則tax等於salary乘以0.2
      float a;
      a = tax;
      cout << fixed << setprecision( 3 ) << "Tax :" << a << "\n";
     }

    else
     {tax = 0.1 * salary;//如果salary小於15000,則tax等於salary乘以0.1
      float a;
      a = tax;
      cout << fixed << setprecision( 3 ) << "Tax :" << a << "\n";
     }

    std::cout << "Enter the salary per month (negative to end):" ;
    std::cin >> salary;

    }

}
