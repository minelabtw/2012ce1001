#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    float sal;
    while ( sal > 0){
        cout << "Enter the salary per month (negative to end): ";
        cin >> sal ;

        if( sal< 15000 && sal> 0 )
            cout << "Tax : " << fixed << setprecision( 3 ) << sal*0.1 << endl;
        else if( sal>=15000 && sal<50000)
            cout << "Tax : " << fixed << setprecision( 3 ) << sal*0.2 << endl;
        else if( sal >= 50000 )
            cout << "Tax : " << fixed << setprecision( 3 ) << sal*0.3 << endl;
    }
    return 0;
}
