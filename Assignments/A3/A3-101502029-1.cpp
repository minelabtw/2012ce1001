#include <iostream>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;
int main()
{
    int a,b;
    while (1)
    {
        cout << "Enter the salary per month (negative to end): ";//輸入一個月的薪水
        cin >> a;
        if ( a >= 0 && a < 15000  )//薪水範圍在0以上，不超過15000
        {
            float b;
            b =  0.1 * a;
            cout << fixed << setprecision( 3 ) << "Tax : " << b << endl;//算到小數點後3位
        }
        if ( a >= 15000 && a < 50000 )//薪水範圍在15000以上，不超過50000
        {
            float b;
            b = 0.2 * a;
            cout << fixed << setprecision( 3 ) << "Tax : " << b << endl;
        }
        if ( a >= 50000 )//薪水範圍在50000以上
        {
            float b;
            b = 0.3 * a;
            cout << fixed << setprecision( 3 ) << "Tax : " << b << endl;
        }
        if ( a < 0 )//如果輸入一個負數，則結束迴圈
        break;
    }

    return 0;
}
