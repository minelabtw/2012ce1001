//With decimal number compute and debug.

#include<iostream>
#include<stdlib.h>
#include<iomanip>
using namespace std;


int main()
{
    //initial
    int pow=1,
        decpoint=0;
    float decimal=0,
          decimal2=0,
          pow2=0.5;
    string binary="";




    //input&exception
    bool error;
    do
    {
        //input
        cout<<"Enter a binary number: ";
        cin>>binary;


        //exception
        error=0;
        if(binary[0]=='-')//negative number
        {
            cout<<"Input can't be negative.\n\n";
            error=1;
            continue;
        }

        decpoint=binary.length();//default
        for(int i=0;i<binary.length();i++)//decimal number
        {
            if(binary[i]==binary[decpoint])//not only one point
            {
                cout<<"Invalid input.\n\n";
                error=1;
                break;
            }
            if(binary[i]=='.')
                decpoint=i;
        }
        if(error)
            continue;

        for(int i=0;i<binary.length();i++)//not number input
            if((binary[i]>'9'||binary[i]<'0')&&binary[i]!='.')
            {
                cout<<"Invalid input.\n\n";
                error=1;
                break;
            }
        if(error)
            continue;

        for(int i=0;i<binary.length();i++)//not binary input
            if(binary[i]<='9'&&binary[i]>'1')
            {
                cout<<"Can't be binary input.\n\n";
                error=1;
                break;
            }
        //exception above
    }while(error);




    //compute
    for(int i=decpoint-1;i>=0;i--)//interger parts
    {
        decimal+=(binary[i]-48)*pow;
        pow*=2;
    }
    for(int i=decpoint+1;i<binary.length();i++)//decimal number parts
    {
        decimal2+=(binary[i]-48)*pow2;
        pow2/=2;
    }


    //output
    cout<<"Decimal is: "
        <<fixed<<setprecision(binary.length()-decpoint)
        <<decimal+decimal2
        <<endl;


    //end
    system("pause");
    return 0;
}
