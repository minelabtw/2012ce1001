#include <iostream>
#include <iomanip>
using namespace std;//簡化std
int main()
{
    int salary;//設整數salary
    double tax;//設實數tax
    cout<<"Enter the salary per month (negative to end): ";//螢幕顯示
    cin>>salary;//使用者輸入salary
    while(salary>=0)//若薪資大於等於零則進入迴圈，小於零則結束
    {
        if(salary<15000)//如果薪資是15000以下
            tax=static_cast<double>(salary)*0.1;//將薪資化為實數再*0.1即為稅
        else
            {   if(salary>=50000)//若薪資是50000(含以上)
                    tax=static_cast<double>(salary)*0.3;//將薪資化為實數再*0.3即為稅
                else//若薪資介於15000至50000
                    tax=static_cast<double>(salary)*0.2;//將薪資化為實數再*0.2即為稅
            }
        cout<<"Tax: "<<setprecision(3)<<fixed<<tax<<endl;//稅取到小數點第三位
        cout<<"Enter the salary per month (negative to end): ";//輸出文字
        cin>>salary;//再次輸入薪資
    }
    return 0;
}
