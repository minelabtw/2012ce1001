#include <iostream>
#include <string.h>

using namespace std;

int cal(int); // Function to calculate the answer

int main()
{
    int input;
    cout << "Enter a binary number: ";

    if(!(cin >> input)) // if is Invalid input. such as char as prefix "abc123"
    {
        cout << "Invalid input." << endl;
        return -1;
    }

    char t=0;
    cin.get(t); // cin may left the '\n' in istream everytime when cin >> xxx.
                // try get next char to check if it is Invalid input. such as char as suffix "123abc"

    if(t!='\n' && t!=0) // if not nextline or End Of File
    {
        cout << "Invalid input." << endl;
        return -1;
    }

    if(input<0)
    {
        cout << "Input can't be negative." << endl;
        return -1;
    }

    int ans = cal(input);

    if(ans==-1)
    {
        cout << "Can't be binary input." << endl;
        return -1;
    }

    cout << "Decimal is: " << ans << endl;
    return 0;

}

int cal(int n)
{
    int basenow=1;
    int rtn = 0;
    int tail;

    while(n>0)
    {
        tail = n%10;
        n/=10;
        if(tail!=1 && tail!=0)
            return -1;
        rtn += basenow*tail;
        basenow *= 2;
    }

    return rtn;

}
