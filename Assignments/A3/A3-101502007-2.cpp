#include <iostream>
using namespace std;
int main()
{
    int num,j,i=0,sum=0,k=0;
    cout<<"Enter a binary number: ";
    if ((cin>>num==false)||(num<0)) //if receive a character or a negative number , program is terminated.
    {
        if (num<0)
        cout <<"Input can't be negative."<<endl;
        else
        cout <<"Invalid input."<<endl;
        return -1;
    }

    while (num>0)
    {
      j=num%10;
      while (i>0)
      {
       j*=2;
       i--;
      }
      k++;
      i=k;
      sum+=j;
      if (num%10<=9&&num%10>=2) //if num can't be binary input, program is terminated.
      {
         cout <<"Can't be binary input."<<endl;
         return -1;
      }
         num=num/10;
    }
    cout<<"Decimal is: "<<sum;
    return 0;
}
