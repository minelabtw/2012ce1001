#include <iostream>

using namespace std;

int main()
{
    int input_num=0;//若輸入字串，則input_num為0
    cout<<"Enter a binary number: ";
    cin>>input_num;



    int result = 0;//結果初始值
    int count = 1;//倍數初始值

    if (input_num<0)//負數不考慮
    {
        cout<<"Input can't be negative.";
        return 0;
    }
    for (;input_num > 0;input_num /= 10)//for迴圈解   先確認>0  各位數字進行轉化運算
    {
        int n = input_num%10;//餘數確認


        if (n>1)//大於一則非二進位
        {
            cout<<"Can't be binary input.";
            return 0;
        }
        if(n==1)
        {
            result+=count;
        }

        count *=2;//隨各位數字變化數值

     }

     if (result==0)//若是輸入字串則不符合，而輸入0不用char解會不好討論
     {
         cout<<"Invalid input.";
         return 0;
     }


     cout<<"Decimal is: "<<result<<"\n";//輸出答案


    return 0;
}
