#include <iostream>
using namespace std;
int main ()
{
    int a,b,c=1,sum=0;
    cout<<"Enter a binary number: ";
    if((cin>>a==false )||(a<0))//如果a的型態錯誤，則顯示錯誤資訊
    {
        if( a<0 )//當a小於0，印出Input can't be negative.
            cout << "Input can't be negative." << endl;
        else//否則則顯示Invalid input.
            cout << "Invalid input." << endl;
        return -1;//回傳
    }
    int d=a ;//宣告d=a
    for(;a>0;a=a/10)//當a>0時，進入迴圈
    if(a%10>1)//如果a%10>1時，輸出錯誤訊息
    {
        cout << "Can't be binary input." <<endl;
        return -1;
    }
    for(; d>0; d=d/10)//當d>0時，進入迴圈
    {
        b=d%10*c;//將d除10並將餘數*c
        c=c*2;//c進行每次回圈都乘於2
        sum=sum+b;
    }
    cout << "Decimal is: "<<sum <<endl;//印出結果
    return 0;//回傳
}
