//101502504
#include <iostream>//allows program to output data to the screen
using namespace std;//program uses

int main()//function main begins program exection
{
    int a,b=1,c=0,total=0;//variable declarations
    cout << "Enter a binary number: ";//prompt user to data

    if (( cin >> a == false ) || ( a < 0 ))//determine if a isn't a number or a < 0
    {
        if ( a < 0 )// a < 0
            cout << "Input can't be negative." << endl;
        else// a is not a number
            cout << "Invalid input." << endl;
        return -1;
    }//end if

    else// a > 0
    {
        for (a; a>0; a%10 )//loop
        {
            if ( a%10 > 1 )// a is an integer but not a binary number
            {
                cout << "Can't be binary input." << endl;
                return -1;
            }//end if

            else//loop until a / 10 < 0
            {
                c=(a%2)*b;
                a=a/10;
                b=b*2;
                total += c;
            }//end else
        }//end loop
        cout << total << endl;//display total ; end line
    }//end else

    return 0;
}
