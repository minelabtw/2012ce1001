#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    float s;

    cout << "Enter the salary per month (negative to end) : ";//輸入你的月薪

    while(cin>>s && s >=0)
    {

        if( s < 15000 )//月薪在一萬五以下
        {
            cout << "Tax : " << fixed << setprecision( 3 ) << 0.1 * s << endl;
        }//稅金為月薪的0.1
        if( 15000<= s && s< 50000 )//月薪在一萬五(含)到五萬之間
        {
            cout << "Tax : " << fixed << setprecision( 3 ) << 0.2 * s << endl;
        }//稅金為月薪的0.2
        if( s >= 50000 )//月薪在五萬(含)以上
        {
            cout << "Tax : " << fixed << setprecision( 3 ) << 0.3 * s << endl;
        }//稅金為月薪的0.3
        cout << "Enter the salary per month (negative to end) : ";
    }return 0;
}
