#include <iostream>
#include <iomanip>
using namespace std;
using std::setprecision;
using std::fixed;
int main()
{
    float a;//宣告一個a
    cout << "Enter the salary per month (negative to end): ";
    cin >> a; //input salary
    while (a>=0)//當a大於等於0時，判斷a的範圍，並處理a
    {
        if (a < 15000)//當a小於15000，則乘於0.1
            cout << "Tax : " << fixed << setprecision(3) << a*0.1 << endl;//印出結果，並精確到小數點以下三位
        else
            if (a>=50000)//當a大於等於50000，乘於0.3
                cout << "Tax : " << fixed << setprecision(3) <<a*0.3 << endl;
            else//a在50000與15000之間，乘於0.2
                cout << "Tax : " << fixed << setprecision(3) <<a*0.2 << endl;

        cout << "Enter the salary per month (negative to end): ";
        cin >> a;
    }
    return 0;//回傳數值
}
