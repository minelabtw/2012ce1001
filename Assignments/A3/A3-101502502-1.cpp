#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    double num1;
    double num2;

    cout << "Enter the salary per month (negative to end): ";
    cin >> num1;

    while ( num1>=0 )
    {
        if ( num1<15000 ) //recognize if the salary is smaller than 15000
        num2 = num1 * 0.1;

        else if ( num1>=15000 && num1<50000 ) //recognize if the salary is greater than 15000 and smaller than 50000
        num2 = num1 * 0.2;

        else if ( num1>=50000 ) //recognize if the salary is greater than 15000
        num2 = num1 * 0.3;

        cout << "Tax : " << fixed << setprecision( 3 ) << num2 << endl;
        cout << "Enter the salary per month (negative to end): ";
        cin >> num1;
    } // end while
    return 0;
} // end main
