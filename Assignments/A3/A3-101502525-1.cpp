//With input debug.

#include<iostream>
#include<stdlib.h>
#include<iomanip>
#include<limits>
using namespace std;


void input(int &in);


int main()
{
    //initial
    int salary=0;
    float tax=0;


    for(;;)
    {
        //input
        cout<<"Enter the salary per month (negative to end): ";
        input(salary);


        //exit
        if(salary<0)
            break;


        //output
        if(salary<15000)
            tax=0.1;
        else if(salary>=15000&&salary<50000)
            tax=0.2;
        else if(salary>=50000)
            tax=0.3;
        cout<<"Tax : "<<fixed<<setprecision(3)<<tax*salary<<endl;
    }


    //end
    system("pause");
    return 0;
}




void input(int &in)
{
    bool error;
    do
    {
        error=0;
        cin>>in;

        //input exception
        cin.clear();//reset cin;
        cin.ignore(numeric_limits<streamsize>::max(),'\n');//clear cin buffer
        if(cin.gcount()!= 1)//not integer
        {
            error=1;
            cout<<"Error, please enter a number : ";
        }
    }while(error);
}
