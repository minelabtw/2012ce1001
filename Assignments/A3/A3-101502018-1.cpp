#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    float salary;
    while(salary>=0)//使salary>=0的數可以繼續計算
    {
    cout << "Enter the salary per month (negative to end): ";
    cin >> salary;

    if(salary>=50000)
    {
        cout <<"Tax : " ;
        cout <<fixed << setprecision( 3 )<<salary*0.3<<endl;//留小數點3位跟salary*0.3
    }
    else if(salary>=15000)
    {
        cout <<"Tax : " ;
        cout <<fixed << setprecision( 3 )<<salary*0.2<<endl;//留小數點3位跟salary*0.2
    }
    else if(salary>=0)
    {
        cout <<"Tax : " ;
        cout <<fixed << setprecision( 3 )<<salary*0.1<<endl;//留小數點3位跟salary*0.1
    }
    }
    return 0;
}
