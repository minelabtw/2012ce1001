#include <iostream>
#include <iomanip>
using std::endl;
using std::cin;
using std::cout;
using std::setprecision;
using std::fixed;

int main()
{
    int salary;//宣告變數
    float tax;

    cout << "Enter the salary per month (negative to end):"  ;
    cin  >> salary;

     if ( salary < 0 )
       cout <<  endl;//若salary<0則程式直接結束
     while ( salary >= 0 )
   {

     if ( salary < 1500 )

         tax = 0.1 * salary;

     else if ( salary >= 1500 && salary < 5000 )

       tax = 0.2 * salary;

     else
       tax = 0.3 * salary;

       cout << fixed << setprecision( 3 ) << "Tax: " << tax << endl;//輸出tax到小數點後三位
       cout << "Enter the salary per month (negative to end):"  ;
       cin  >> salary;

   }

    return 0;

}
