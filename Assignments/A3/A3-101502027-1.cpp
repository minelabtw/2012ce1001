#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    double n1;//in order to save larger numbers.
    float n2;//in order to save float numbers.
    cout<<"Enter the salary per month (negative to end): ";
    cin>>n1;//to print ont the frist line.
    while (n1>0)
    {
        if (n1<15000)
        n2=n1*0.1;
        else if (n1>=50000)
        n2=n1*0.3;
        else
        n2=n1*0.2;//if the integer matches (1500<=integer<50000),then do the following programs.
        cout <<fixed << setprecision( 3 ) << "Tax : "<< n2 << endl;
        cout<<"Enter the salary per month (negative to end): ";
        cin>>n1;
    }
   return 0;
}
