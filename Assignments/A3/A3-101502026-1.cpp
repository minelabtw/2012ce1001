#include <iostream>
#include <iomanip> //declare "setprecision" in this scope

using namespace std;

int main()
{
    double x; //declare a number x(floating point number)

    while(x>=0) //set a loop,of which the condition to enter is that x is greater than or equal to 0
    {
        cout << "Enter the salary per month (negative to end): "; //print out a request on the screen
        cin >> x; //resquest the user to enter a number

        if(x>=0&&x<15000) //if the value of the number entered is between 0 (including 0) and 15000(except 15000),then execute the instruction below
        cout << "Tax : " << fixed << setprecision (3) << 0.1*x <<endl; //the instruction mentioned above is to compute "0.1 times x" and then print out the result(accurate to only the third digit after the decimal point) on the screen

        else if(x>=15000&&x<50000) //if the value of the number entered is between 15000 (including 15000) and 50000(except 50000),then execute the instruction below
        cout << "Tax : " << fixed << setprecision (3) << 0.2*x <<endl; //the instruction mentioned above is to compute "0.2 times x" and then print out the result(accurate to only the third digit after the decimal point) on the screen

        else if(x>=50000) //if the value of the number entered is great than or equal to 50000,then execute the instruction below
        cout << "Tax : " << fixed << setprecision (3) << 0.3*x <<endl; //the instruction mentioned above is to compute "0.3 times x" and then print out the result(accurate to only the third digit after the decimal point) on the screen

        else if(x=-1)
        return 0; //if the value of the number entered is -1,then jump off the loop and the function
    }
    return 0; //return a number 00
}
