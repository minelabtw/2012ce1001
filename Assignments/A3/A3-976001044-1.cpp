// Assignment #3-1
// A3-976001044-1.cpp 
#include <iostream>
#include <cstdlib>
#include <iomanip>
using namespace std;

int main()
{
    int salary=0;
    float ratio=0.0, tax=0.0;
    char prompt[]="Enter the salary per month (negative to end): ";
    
    cout << prompt;
    while ( cin >> salary )
    {
          // Functional phase
          // To setup the ratio of tax
          // If the input data is negative or invalid, 
          // break out the while loop and skip the Output phase
          if ( salary < 0 )
          {
               break;
          }
          else if ( salary < 15000 )
          {
               ratio = 0.1;
          }
          else if ( salary < 50000 )
          {
               ratio = 0.2;
          }
          else if ( salary >= 50000 )
          {
               ratio = 0.3;
          }
          else
          {
               break;
          }
          
          // Output phase
          // Setting accuracy to the third decimal place, 
          // then fix the result from Scientific notation to float
          tax = float(salary) * ratio;
          cout << "Tax : " << fixed << setprecision(3) << tax << endl;
          cout << prompt;
    }
    
    system("pause");
    return 0;
}
