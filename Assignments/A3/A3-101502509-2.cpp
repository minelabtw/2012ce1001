#include <iostream>
#include <limits>
#include <cstdlib>

using namespace std;


int main()
{
     int binary;
     cout <<"Enter a binary number: ";
     cin >> binary;   
     cin.clear();                                     /*重設 stream 的 status flags,錯誤的輸入資料stream就會設一個失敗的 flag,
                                                                忽略不足夠繼續讀入,直到重設 flag*/
     cin.ignore(numeric_limits<int>::max(), '\n');   //把字元取出並且忽略,取(整數的最大值)個字元或取到delim,
                                                               
                                                             
     if( cin==false || cin.gcount() != 1  )         // 當輸入為字元，為Invalid input.並結束程式
     {
          cout <<"Invalid input."<< endl;
          return -1;
     }
     if(binary<0)                                    // 當輸入沒有字元為負數，告知Input can't be negative.並結束程式
     {
          cout <<"Input can't be negative."<< endl;
          return -1;
     }
     int y=0,arybin[32],base=1,decimal=0;            
     while(binary>0)
     {
          arybin[y]=binary%10;                       /*將binary的位數逐項檢視，並將位數分開儲存，因為是二進位，只有1或是0，
                                                               如果是其他數字的話，告知Can't be binary input.並結束程式*/
          if(arybin[y]!=0 && arybin[y]!=1)           
          {
              cout <<"Can't be binary input."<< endl;
              return -1;
          }
        
          y=y+1;
          binary=binary/10;
     }

     for(int i=0;i<=y-1;i++)                       //將binary的值轉變成十進位，並印出
     {
          decimal=decimal+arybin[i]*base;
          base=base*2;
     } 
     cout <<"Decimal is: "<<decimal<< endl;

     
     system("pause");
     return 0;

}
