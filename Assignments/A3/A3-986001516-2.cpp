#include <iostream>                                                             /* 前三行將下面程式會用到的函數的相關標頭檔含括 */
#include <math.h>
#include <ctype.h>
#include <cstdlib>
using namespace std;                                                            /* 使用std的命名空間,可以省去cin,cout,endl等原始寫法的麻煩 */

int power(int a,int b)                                                          /* 因為內建指數函數power()預設為double的資料型態,為了避免之後碰到強制改變資料型態的警告訊息,這裡另外寫一個副程式,回傳值為整數 */
{
    int sum=1;
    for (b;b>0;b--)
    {
        sum=sum*a;
    }
    return sum;
}

int main()                                                                      /* 程式主體 */
{
    char temp[30];                                                              /* 宣告一個字串型態的變數,用來存取使用者輸入的字串或是數值 */
    int i,j,temp2,bin,sum=0;
    cout << "Enter a binary number: ";
    cin >> temp;
    bin=atoi(temp);                                                             /* 將字串temp轉為整數型態定義給變數bin,用於檢驗完畢後,二進位轉換成十進位值 */
    temp2=atoi(temp);                                                           /* 同樣將字串temp轉為整數型態定義給變數temp2,用於逐位確認是否為1或0 */
    if  (temp[0]=='-')                                                          /* 用if... else...敘述來判斷使用者輸入的字串為數值還是符號 "-",若為符號 "-",則判斷為負值,印出錯誤警告 */
    {
        cout << "Input can't be negative.\n";
    }
    else                                                                        /* 若判斷第一個位置並非 "-"號,則將字串以其他條件檢驗是否為二進位表示法的數值 */
    {
        for (i=strlen(temp)-1,j=0;i>=0;i--,j++)                                 /* 用for迴圈達到逐字逐位確認的方式,strlen()函數能夠將字串的長度回傳,這裡從字串的最右方位置開始檢驗 */
        {
            if  (isdigit(temp[i])==0)                                           /* 若該字串任何一位為非數值之字母或符號,則顯示錯誤訊息,並跳出迴圈,isdigit()函數可以確認該位是否為數值,若非數值,則會回傳整數0 */
            {
                cout << "Invalid input.\n";
                break;
            }
            else                                                                /* 若檢驗中的位置或元素為數值,則再利用if... else... 敘述檢查是否為1或0 */
            {
                if  (temp2%10==0 || (temp2-1)%10==0)
                {
                    if  (temp2%10==0)                                           /* 42行至54行,若該位置為1或0,則將該位乘上2的相對應次方,累加至變數sum中,即2進位轉為10進位的數學運算方式 */
                    {                                                           /* 且將temp2及bin兩個變數以新結果覆蓋,方便逐位檢查 */
                        temp2=temp2/10;
                        bin=bin/10;
                    }
                    else
                    {
                         temp2=(temp2-1)/10;
                         sum=sum+power(2,j);
                         bin=(bin-1)/10;
                    }
                    
                }
                else                                                            /* 若檢驗過程中,任何一位為非0或1的其他數字,則判斷輸入的字串非二進位表示的數值,無法運算,直接印出錯誤訊息,跳出檢驗迴圈 */
                {
                    cout << "Can't be binary input.\n";
                    break;
                }
            }
        }
        if  (i==-1)                                                             /* 最後,若輸入的字串的各個位數皆通過檢驗,迴圈可完全執行,i值會自最大值(即字串長度-1)遞減至-1,此if敘述判斷若i為-1,則印出轉換結果 */
        {
            cout << "Decimal is: " << sum << endl;
        }
    }
    system("pause");
    return 0;
}
