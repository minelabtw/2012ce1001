#include <iostream>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;
int main()
{
    int salary;
    float tax;
    cout<<"Enter the salary per month : ";

    //輸入
    while(cin>>salary)
    {//在每個不同數量下,所要抽的稅
        if (salary<15000)
        {
            tax=0.1 * salary;
        }
        if(salary>=15000)
        {
            tax=0.2 * salary;
        }
        if(salary>=50000)
        {
            tax=0.3 * salary;
        }
        if(salary<0)
        {
            return 0;
        }


        //輸出
        cout <<"Tax : "<< fixed << setprecision( 3 ) << tax << endl;
        cout<<"Enter the salary per month : ";
    }
    return 0;
}
