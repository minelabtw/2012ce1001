#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    double salary=0;
    while(salary>=0)//限制薪水是正數才做迴圈
    {
        cout<<"Enter the salary per month (negative to end): ";
        cin>>salary;
        if(salary>=50000)
            cout<<"Tax : "<< fixed << setprecision( 3 ) <<0.3*salary<<endl;
        else if(salary>=15000)
            cout<<"Tax : "<< fixed << setprecision( 3 ) <<0.2*salary<<endl;
        else if(salary>=0)
            cout<<"Tax : "<< fixed << setprecision( 3 ) <<0.1*salary<<endl;
    }

    return 0;
}
