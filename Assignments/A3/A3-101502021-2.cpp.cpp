#include <iostream>

using namespace std;

int main()
{
    int binary ;
    int sum=0 ;
    int A ;
    int B=1 ;

    cout << "Enter a binary number: " ;
    cin >> binary ; // the interger entered by user

    if (binary == false) // if binary is not a digital
    {
    cout << "Invalid input." << endl ;
    return 0 ;
    } // end if

    if (binary < 0) // if binary is a negative digital
    {
    cout << "Input can't be negative." << endl ;
    return 0 ;
    } // end if

    while (binary >= 1) // when binary > or = 1
    {
        A = binary % 10 ; // A is the remainder of binary/10

        if (A == 1)
        {
            sum = sum + B ;
        } // end if

        if (A != 0 && A != 1) // if A is not 0 or 1
        {
            cout << "Can't be binary input." << endl ;
            return 0 ;
        } // end if

        binary = binary / 10;
        B = B * 2 ;
    } // end while

    cout << "Decimal is: " << sum << endl ;

    return 0 ;
}
