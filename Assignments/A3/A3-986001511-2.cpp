#include<iostream>
#include<math.h>
#include<cstring>
#include<cstdlib>
using namespace std;

int main()
{
    char num1[20],p;                                                            //set initial input is character,temporary character p
    int len,temp,sum,count;                                                     //set initia; integer variables
                                            
    cout << "Enter a binary number: ";                                          //input an initial number/character
    cin >> num1;
    len=strlen(num1);                                                           //determine the length of the character
    count=len-1;                                                                //count will be the index
    int num=atoi(num1);                                                         //change the character into integer number
    sum=0;                                                                      //the initial summary is 0
    
      if (num <0)                                                               //if the number is less than 0
         {cout << "Input can't be negative."<< endl;}                           //show warning massage

      if (num >0)                                                               //if the number is a valid binary number
         {
            for (int c=0; c<len; c=c+1)                                         //do loop len-1 times
                {
                  p=num1[c];                                                    //determine each character in initial string
                  p=int(p);                                                     //change character into numbers
                  temp=p-48;                                                    //change ascii number into real number
            
                  if ( (temp==0) || (temp==1) )                                 //if the number is composed by 0 and 1
                     {   temp=int(temp*pow(2,count));                           //do calculating
                         count=count-1;
                         sum=sum+temp;                                          //do summing
                    
                        if (count<0)                                            //the last calculating step
                           {cout << "Decimal is: "<< sum << endl;}              //show final result of the decimal.
                     }
                  else {cout << "Can't be binary input." << endl;}              //if the number is not composed bty 0 and 1, show error massage
                 }
         }                                                               

       if ( (sum==0) && (num==0) )                                              //if the summary is still 0 or the initial input are alphabets
       {cout << "Invalid input."<< endl;}                                       //show warning massage
    
     system("pause");                                                           //stop program
     return 0;
    }
