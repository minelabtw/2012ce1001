#include <iostream>
using namespace std;

int main()
{
    int number,sum=0,n=1;//設定變數
    cout << "Enter a binary number: ";//輸入數字

    if (cin >> number)//判斷是否為整數
    {
        if ( number < 0)// number小於0的話輸出的話結束程式
        {
            cout << "Input can't be negative." <<endl;
            return 0;
        }
        if ( (number%10) > 1)//判斷是否有非1或0
        {
            cout << "Can't be binary input." <<endl;
            return 0;
        }
        while ( number > 0)//number大於0進入迴圈
        {

            if ( (number%10) == 1)//餘數為1的話計算下列算式
            {
                sum = sum + number%10*n;
                number = (number)/10;//除以10重新開始迴圈
            }
            else if ((number%10) ==0)//餘數為0
            {
                number = number /10;//除以10重新開始迴圈
            }
            n = n*2;//下一次2的倍數
        }
        cout << "Decimal is: " <<sum;
    }
    else//為字元的話為失敗輸入
        cout << "Invalid input." <<endl;

    return 0;
}
