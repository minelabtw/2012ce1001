#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int salary;

    cout << "Enter the salary per month (negative to end): ";
    cin >> salary;

    if ( salary < 15000 and salary > 0 ) //若salary在0~15000元的範圍內，則tax為0.1倍的salary並且計算到小數點後3位
        cout << "Tax : " << fixed << setprecision ( 3 ) << 0.1 * salary << endl;

    if ( salary >= 15000 and salary < 50000 ) //若salary在15000(含)到50000元的範圍內，則tax為0.2倍的salary並且計算到小數點後3位
        cout << "Tax : " << fixed << setprecision ( 3 ) << 0.2 * salary << endl;

    if ( salary >= 50000 ) //若salary在50000元(含)以上時，tax為0.3倍的salary並且計算到小數點後3位
        cout << "Tax : " << fixed << setprecision ( 3 ) << 0.3 * salary << endl;

    else // 除錯:salary為負數時，結束程式

    return 0;
}
