#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int salary;//宣告變數
    cout << "Enter the salary per month (negative to end): ";
    while( cin >> salary )//設計輸入迴圈
    {
        if ( salary < 0 )
            break;//若輸入為負數則跳出
        else if ( salary < 15000 )//用else先過濾負數
            cout << "Tax : " << fixed << setprecision( 3 ) << 0.1*salary << endl;//小於15000輸出
        else if ( salary < 50000 )//用else過濾小於15000者
            cout << "Tax : " << fixed << setprecision( 3 ) << 0.2*salary << endl;//小於50000輸出
        else//餘下皆大於50000
            cout << "Tax : " << fixed << setprecision( 3 ) << 0.3*salary << endl;//大於50000輸出
        cout << "Enter the salary per month (negative to end): ";
    }
    return 0;
}

