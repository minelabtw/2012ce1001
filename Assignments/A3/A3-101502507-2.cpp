//trainsform binary number to decimal number enter by the user
#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int binary;//user enter the binary number
    int i = 0;//the power of 2
    int sum = 0;//decimal

    cout << "Enter a binary number: ";

    if( cin >> binary == false ) cout << "Invalid input." << endl;
    if( binary < 0 ) cout << "Input can't be negative." << endl;//the number enter by user is negative

    else{ while( binary != 0 )
    {
        sum += ( binary % 10 )*pow(2,i);

        binary = binary/10;

        i++;//i+1

    }//else end

    cout << "Decimal is: " << sum << endl;
    }//else end


    return 0;
}//main end
