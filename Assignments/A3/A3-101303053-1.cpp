#include <iostream>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;
int main()
{
    int salary;
    double tax;

    while(1)
    {
        cout << "Enter the salary per month (negative to end):";
        cin >> salary;

        if ( salary < 15000 )
            tax = salary * 0.1;
        if ( salary >= 15000 && salary < 50000 )
            tax = salary * 0.2;
        if ( salary >= 50000 )
            tax = salary * 0.3;
        if ( salary < 0 )
            break;
    cout << fixed << setprecision( 3 ) << tax << endl;

    }

    return 0;
}
