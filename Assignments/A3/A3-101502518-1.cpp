#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int c,salary;
    c=0;

    cout <<"Enter the salary per month (negative to end): ";
    cin >> salary;

    while (salary>=0)
    {
        if(salary<15000)
            cout << "Tax: " <<  fixed <<setprecision(3) << salary*0.1 << endl;
        if(salary>=15000 and salary<50000)
            cout << "Tax: " <<  fixed <<setprecision(3) << salary*0.2 << endl;
        if(salary>=50000)
            cout << "Tax: " <<  fixed <<setprecision(3) << salary*0.3 << endl;

        cout <<"Enter the salary per month (negative to end): ";
        cin >> salary;
        c++;
    }
    return 0;
}
