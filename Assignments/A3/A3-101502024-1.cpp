#include <iostream>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;
int main()
{
    int salary;//Declare the integers that will be used in the program.
    float tax;

     while ( salary > 0 )//Judge if the salary is negative or not.
    {
    cout << "Enter the salary per month (negative to end): ";
    cin >> salary;
    if( 0 <= salary )//Judge the volume of the salary.
     {
         tax =  0.1 * salary;//The foumula based on the volume of the salary.
     }
    if( 15000 <= salary )//Judge the volume of the salary.
     {
         tax =  0.2 * salary;//The foumula based on the volume of the salary.
     }
    if( 50000 <= salary )//Judge the volume of the salary.
     {
         tax = 0.3 * salary;//The foumula based on the volume of the salary.
     }
    cout << "Tax : " << fixed << setprecision( 3 ) << tax << endl;//Display the result.
    }
     return 0;
}
