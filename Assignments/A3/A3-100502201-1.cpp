#include <iostream>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
using std::setprecision; //Used to set the floating precision
using std::fixed; //Make setprecision function work
int main()
{
    int salary; //Given that salary is an integer
    float tax; //Tax is set to be a floating number
    cout << "Enter the salary per month (negative to end): " ;
    while ( cin >> salary >= 0) //Set cin function to input and use while function to make the program loop
    {
        if (salary < 0)
        {
            return 0;
        }
        else if (salary < 15000)
        {
            tax=0.1*salary;
        }
        else if (salary < 50000)
        {
            tax=0.2*salary;
        }
        else
        {
            tax=0.3*salary;
        }
        // If the input is negative, end the program. Otherwise follow the rule to calculate the tax.
        cout << "Tax : " << fixed << setprecision( 3 ) << tax ; // Set tax's floating numbers
        cout << "Enter the salary per month (negative to end): " ; //Ask the user and continue to loop
    }
    return 0;
}

