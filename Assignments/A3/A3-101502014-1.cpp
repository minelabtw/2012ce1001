#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    double salary = 1;//make sure entering the loop.
    cout << "Enter the salary per month (negative to end): ";
    while(cin >> salary && salary >= 0)//error detection : if receive a negative number , program is terminated.
    {
        if(salary >= 50000)
            salary *= 0.3;
        else if(salary >= 15000)
            salary *= 0.2;
        else
            salary *= 0.1;
        cout << "Tax : " << fixed << setprecision(3) << salary << endl;
        cout << "Enter the salary per month (negative to end): ";
    }
	return 0;
}
