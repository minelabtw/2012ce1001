#include <iostream>
using namespace std;
int main()
{
    int a , b = 0 , c = 0;
    cout<<"Enter a binary number: ";
    if(cin>>a)//輸入變數a
    {
        if( a < 0 )//如果a小於零 顯示"Input can't be negative."
        {
            cout<<"Input can't be negative."<<endl;
            return 0;
        }
        for(int i = 1 ; a >= 1; i *= 2)//設定一個變數i等於1;a大於或等於1時則進入迴圈;每跑一次迴圈i就乘以2
        {
            if(a % 10 == 1||a % 10 == 0)//若a除以10餘0或1
            {
                b = ( a % 10 ) * i;//b等於a除以10的餘數乘以i
                a = a/10;//a等於a除以10
                c += b;//c等於c加b
            }
            else//若a除以10不餘0或1則顯示"Can't be binary input."
            {
                cout<<"Can't be binary input."<<endl;
                return 0;
            }
        }
        cout << "Decimal is: " <<c<<endl;//輸出c的值
    }
    else
        cout << "Invalid input." << endl;//輸入字元則顯示"Invalid input."

    return 0;
}
