#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int integer1;
    cout << fixed <<setprecision(3)<< "Enter the salary per month (negative to end): ";//設定顯示到小數點第三位
    while (cin >> integer1)
    {
        if (integer1>0)
        {
            if (integer1>=50000)
            cout << "Tax : " << integer1*0.3 << endl ;
            if (integer1>=15000 && integer1<50000)
            cout << "Tax : " << integer1*0.2 << endl ;
            if (integer1< 15000)
            cout << "Tax : " << integer1*0.1 << endl ;
        }
        else
        break;
    cout << "Enter the salary per month (negative to end): ";
    }
    return 0;
}
