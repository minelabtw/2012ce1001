#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    double salary=0;
    double tax;

    cout << "Enter the salary per month (negative to end): " ;
    cin >> salary ;

    while(salary>0)//如果輸入一個非正整數,會破壞迴圈,回傳一個0
    {
       if(salary>=50000)
       {
        tax = 0.3 * salary;
        cout <<"Tax : "<< fixed << setprecision( 3 ) << tax <<endl;
       }

        else if(salary<15000)
        {
            tax = 0.1 * salary;
            cout <<"Tax : "<< fixed << setprecision( 3 ) << tax <<endl;
        }

        else
        {
            tax = 0.2 * salary;
            cout <<"Tax : "<< fixed << setprecision( 3 ) << tax <<endl;
        }
        cout << "Enter the salary per month (negative to end): " ;
        cin >> salary ;

    }

    return 0;
}
