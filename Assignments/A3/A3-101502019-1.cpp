#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    double sal;
    while (sal >= 0)
    {// start while
    cout << "\nEnter the salary per month (negative to end) : ";
    cin >> sal;

    if (sal < 0)// <0 will end the program
            return 0;
    else if (sal >= 50000)
        cout << "\nTax : " << fixed << setprecision( 3 ) << sal * 0.3 << endl;// >=50000 ,tax will *0.3
    else if (sal >= 15000)
        cout << "\nTax : " << fixed << setprecision( 3 ) << sal * 0.2 << endl;// >=15000 ,tax will *0.2
    else if (sal < 15000)
        cout << "\nTax : " << fixed << setprecision( 3 ) << sal * 0.1 << endl;// <15000 ,tax will *0.1
    }// end while

    return 0;
}
