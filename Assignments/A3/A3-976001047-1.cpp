#include <iostream>
#include <cstdlib>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;

int main()

{   
    int salary;
    float tax;
      
    cout << "Enter the salary per month (negative to end): ";
    cin >> salary ;                    //input salary from user
    
    while (salary>0)           //when salary bigger than zero calculate tke tax
    {
        if (salary < 15000)   // tax for under $15000
        {
            tax = salary*0.1;
            cout << "Tax : "<< fixed << setprecision( 3 ) << tax << endl;
        }
        
        if (salary >= 15000 && salary < 50000) // tax for between $15000 and $50000
        {
            tax = salary*0.2;
            cout << "Tax : "<< fixed << setprecision( 3 ) << tax << endl;
        }
        
        if (salary >= 50000)     // tax for more than $50000
        {
            tax = salary*0.3;      
            cout << "Tax : "<< fixed << setprecision( 3 ) << tax << endl; 
        }
        cout << "Enter the salary per month (negative to end): ";
        cin >> salary ;
        
    }                  


    system("PAUSE");
    return 0;
}
