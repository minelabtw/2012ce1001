#include <iostream>
#include <iomanip>

using namespace std;
int main()
{
    int salary;
    double Tax;

    cout << "Enter the salary per month (negative to end):";

    while( (cin >> salary == false) || (salary > 0) )//當salary=0或salary大於0時,進入while迴圈
    {
        if ( salary < 15000 )//如果salary小於15000
            Tax = 0.1 * salary;//則Tax為0.1乘以salary

        else if ( salary >= 15000 && salary <50000 )//如果salary大於等於15000,小於50000
            Tax = 0.2 * salary;//則Tax為0.2乘以salary

        else if ( salary >= 50000 )//如果salary大於等於15000,小於50000
            Tax = 0.3 * salary;//則Tax為0.3乘以salary

        cout << "Tax : " << fixed << setprecision( 3 ) << Tax << endl;//輸出的Tax顯示至小數點後三位
        cout << "Enter the salary per month (negative to end): ";
    }
        return 0;
}
