#include <iostream>
#include <iomanip>
using namespace std;

// function main begins program execution
int main()
{
    float a; // 假設一個可以為小數的變數a
    float sum; // 假設一個可以為小數的變數sum
    cout << "Enter the salary per month (negative to end): "; // display message
    cin >> a;

    while( 0 > a ) // 如果a小於0，直接回報並且結束程式
    {
        return 0;
    }


    if ( 15000 > a ) // 如果a小於15000，則用下列運算公式計算sum的值，並且將結果顯示在螢幕上
    {
        sum = a * 0.1;
        cout << "Tax : " << fixed << setprecision( 3 ) << sum;
    }

    if ( a >= 15000 )
    if ( 50000 > a )
    {
        sum = a * 0.2;
        cout << "Tax : " << fixed << setprecision( 3 ) << sum;
    }

    if ( a >= 50000 )
    {
        sum = a * 0.3;
        cout << "Tax : " << fixed << setprecision( 3 ) << sum;

    }

    while( a > 0 ) // 如果a大於0，則重複顯示出訊息，並且可以繼續程式
    {
        cout << "\n" << "Enter the salary per month (negative to end): ";
        cin >> a;

        while( 0 > a )
        {
            return 0;
        }


        if ( 15000 > a )
        {
            sum = a * 0.1;
            cout << "Tax : " << fixed << setprecision( 3 ) << sum;
        }

        if ( a >= 15000 )
        if ( 50000 > a )
        {
            sum = a * 0.2;
            cout << "Tax : " << fixed << setprecision( 3 ) << sum;
        }

        if ( a >= 50000 )
        {
            sum = a * 0.3;
            cout << "Tax : " << fixed << setprecision( 3 ) << sum;

        }
    }


return 0;  // indicate that program ended successfully
} // end function main

