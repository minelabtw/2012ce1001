#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;


int main()
{
    
   int salary;
   double tax;
       
   cout << "Enter the salary per month (negative to end): ";
   
   while(cin >> salary && salary>=0)   //當salary的輸入是字元或是小於0的整數時，迴圈就會結束   
   {
        if(salary<15000)               //當salary未滿15000時，稅率為0.1，而Tax為0.1*salary,並精確到小數點第3位
        {
            tax =0.1*salary;
            cout <<"Tax : " << fixed << setprecision( 3 ) << tax << endl;
   
        }
        if(salary<50000 && salary>=15000)   //當salary為15000以上，未滿50000時，稅率為0.2，而Tax為0.2*salary,並精確到小數點第3位
        {
            tax =0.2*salary;
            cout <<"Tax : " << fixed << setprecision( 3 ) << tax << endl;
        } 
        
        if(salary>=50000)              //當salary為50000以上時，稅率為0.3，而Tax為0.3*salary,並精確到小數點第3位
        {
            tax =0.3*salary;
            cout <<"Tax : " << fixed << setprecision( 3 ) << tax << endl;
        }

        cout << "Enter the salary per month (negative to end): ";
   }

   system("pause");
   return 0;

}

