#include <iostream>
#include <cstdlib>
#include <math.h>

using std::cout;
using std::cin;
using std::endl;

int main()

{   
    int x1=0;
    int i=0;
    float x2=0;
    float total=0;  
    
    cout << "Enter a binary number: ";
    cin >> x1 ;
    
    if(x1%10 > 1)      // error message for the number is not combine from 0 or 1
    {
       cout << "Can't be binary input.\n";
       
       system("PAUSE");
       return 0;
    }
    
    if(cin==false)         // error message for input is not number
    {
       cout <<"Invalid input.\n";
       
       system("PAUSE");
       return 0;      
    }
    
    if(x1<0)         // error message for the number is negative   
    {
      cout <<"Input can't be negative.\n";
      
      system("PAUSE");
      return 0; 
        
    }
    
    if(x1==1)      // make up when the input number=1 the loop can't calculate 
    {
     cout <<"Decimal is: 1\n";
     
     system("PAUSE");
     return 0;
    }
    
    while(x1/10 != 1)   //this loop calculate binary number to decimal number
    {
  
       if (x1%10==1)
       {
          x2=pow(2,i);
          total = total+x2;
       }  
       
       x1=x1/10;
       i = i+1;
       
    }
    
    total = total + pow(2,i+1);   // odd the last one order , beacause it is not calculate in the loop
    
    cout <<"Decimal is: "<< total << endl;
    
    system("PAUSE");
    return 0;
}

       
     
