//Assignment 3-2 by J
#include <iostream>     //for function cout. cin.
#include <cmath>        //for function pow(x,y)
using namespace std;
int main()
{
    int inp=0 , oup=0 , ctr=0 ;
    //the only 2 used variables in this program : inp as input , oup as ouput.

    cout << "Enter a binary number: " ;

    if ( ( cin >> inp == false ) || inp<0 )  //this instruction provide 2 check functions:1.character  2.negative number
    {
        if ( inp<0 )        //function of checking negative number
            cout << "Input can't be negative." << endl ;
        else        //function of checking charater input
            cout << "Invalid input." << endl ;

        return -1;
    }

    if ( inp%10==0 || inp%10==1 )        //function of checking none-binary input
    {
        while ( inp/10>0 || (inp%10)!=0 )       //transfering function , binary to decimal
        {
            oup=oup+((inp%10)*pow(2,ctr));
            inp=inp/10;
            ctr+=1 ;
        }

    }
    else        //fuction for checking input like 2.3.4......9
    {
        cout << "Can't be binary input." << endl ;
        return -1;
    }

    cout << "Decimal is: " << oup << endl ;     //final transfer result

    return 0;
}
