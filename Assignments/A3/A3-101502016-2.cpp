#include <iostream>
#include <math.h>
 using namespace std;

 int main()
 {
     int a,b,c=0 , i=0;

     cout << "Enter a binary number: ";
     while ( cin >> a )
     {
         if ( a < 0 )
         {
             cout << "Input can't be negative"<<endl;
             return -1;
         }
          for( a; a >= 1; i++ )//i指的是這個迴圈執行多少次,即2的i次方,迴圈令a不斷被10除並產生一個數b代表餘數,直到a<1再跳出迴圈
          {
              b=a%10;//設定一個變數b來表示a/10的餘數,若是超過不為1or0即不符合條件
              if ( b == 0)//在b=0或b=1的狀況下,代表輸入值a皆為有效的數字
                  a=a/10;
              else if ( b == 1)
                  a=a/10;
              else
              {
                  cout << "Can't be binary input." << endl;

                  return -1;
              }
              c=c+b*pow(2,i);//設定一個數c,為每次迴圈所產生的b*第i次迴圈(就是2的幾次方)的總和
          }
          cout << "Decimal is: " << c << endl;

          return 0;
     }
     cout << "Invalaid input." << endl;//不符合while的條件(x不是整數時),會顯示其為無效的輸入
          return -1;
 }
