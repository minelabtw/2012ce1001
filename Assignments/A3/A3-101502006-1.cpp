#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int input;
    float tax;
    cout << "Enter the salary per month (negative to end):";
    while( cin >> input )  // if cin encounter an invalid input( such as character ), it will return false then break the loop
    {
        if (input >= 50000)  // 50000 <= salary , tax is 0.3 * salary
                {tax = input * 0.3;
                cout << "Tax : " << fixed << setprecision( 3 ) << tax << endl;
                cout << "Enter the salary per month (negative to end):";}
        else if (input >= 15000) // 15000 <= salary < 50000 , tax is 0.2 * salary
                {tax = input * 0.2;
                cout << "Tax : " << fixed << setprecision( 3 ) << tax << endl;
                cout << "Enter the salary per month (negative to end):";}
        else if (input >= 0) // salary < 15000 , tax is 0.1 * salary
                {tax = input * 0.1;
                cout << "Tax : " << fixed << setprecision( 3 ) << tax << endl;
                cout << "Enter the salary per month (negative to end):";}
        else return 0; // if input < 0, end the program
    }
    return 0;
}
