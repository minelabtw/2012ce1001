#include<iostream>
using namespace std;

int main()
{
    int number,process,i,total,remainer;
    total=0;
    i=1;

    cout << "Enter a binary number: ";//輸入想判斷的數字
    cin>>number;
    if( number < 0 )//當數字小逾零時
    {
        cout<<"Input can't be negative."<< endl;
        return -1;
    }
    else if( ( cin == false ) || ( number < 1 ) && ( number > 0 ) )//又如果唯一到零的小數或不是binary
    {
        cout<<"Invalid input."<<endl;
        return -1;
    }
    else if (number>=0)
    {
        while (number>0)//進入迴圈加總並判斷
        {
            remainer=number%10;
            if(remainer!=1 && remainer!=0)//當餘數不等於零或一時
            {
                cout<<"Can't be binary input."<<endl;
                process=1;
                break;
            }
            else
            {
                total=total+remainer*i;
                i=i*2;
                number=number-remainer;
                number=number/10;
            }
        }
        if(process==1)
            cout<<endl;
        else//輸出total十進位之值
        {
            cout<<"Decimal is: "<<total<<endl;
            return 0;
        }
    }
}


