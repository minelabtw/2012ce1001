#include <iostream>
#include <iomanip>
using std::cout;
using std::cin;
using std::endl;
using std::setprecision;
using std::fixed;
int main()
{
    int salary;
    float tax;
    cout << "Enter the salary per month (negative to end): ";

    while ( cin >> salary ) // start a loop typing a salary
    {
        if (salary >= 50000 ) // if the salary is bigger or equal than 50000
            tax = salary * 0.3; // the salary have to multiply a tax which is 0.3

        else if ( salary < 50000 && salary >= 15000 ) // if the salary is smaller than 50000 and smaller or equal than 15000
            tax = salary * 0.2;// the salary have to multiply a tax which is 0.2

        else if ( salary < 15000 ) // if the salary is smaller than 15000
            tax = salary *0.1; // then the salary have to multiply a tax which is 0.1
        {
            if ( salary < 0 ) // if the salary is a negative number
            return 0; // the program ends
        }
            cout <<  "Tax : " << fixed << setprecision( 3 ) << tax << endl;
            cout << "Enter the salary per month (negative to end): ";
    }

    return 0;
}
