#include <iostream>
using namespace std;

int main()
{
    int binary,decimal,number,test,save;

    cout << "Enter a binary number: ";
    cin >> binary;

    decimal = 0;
    number = 1;
    test = 0;
    save = binary;

    if ( cin == false ) //若輸入字元則輸出Invalid input
    cout << "Invalid input." << endl;

    else if ( binary < 0 ) //其餘若輸入負數則輸出Input can't be negative
    cout << "Input can't be negative." << endl;

    else //若以上都不成立則執行下列判斷
    {
        while ( binary != 0 ) //判斷輸入的數是否為二進位數
        {
            if ( binary % 10 != 0 && binary % 10 != 1 ) //若被10餘除不等於1或0則為非二進位數
            {
                test = 1; //若輸入的數不是二進位數則讓test=1並跳出此判斷迴圈
                break;
            }
            binary = binary / 10;
        }

        if ( test == 1 ) //test=1表示輸入的數不是二進位數
        cout << "Can't be binary input." << endl;

        else
        {
            binary = save; //先前判斷是否為二進位數時binary已被除過,以此將binary還原

            while ( binary != 0 ) //將輸入的二進位數轉換成十進位數
            {
                decimal = decimal + number * ( binary % 10 );
                number = number * 2;
                binary = binary / 10;
            }
            cout << "Decimal is: " << decimal << endl;

        }
   }

    return 0;

}
