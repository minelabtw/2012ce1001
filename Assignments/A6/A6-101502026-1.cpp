//if the integers entered is out of the range of -2147483648<=n<2147483648,then exit the program
#include <iostream> //include header file "iostream"
#include <cstdlib> //include header file "cstdlib"

using namespace std;

long long int x;
void reversal() //this is the function to reverse the integers entered
{
    long long int y=0;

    if(x>=2147483648||x<y-2147483648||cin==false) //if the number entered isn't an integer,then exit the program
        exit(0);

    for(;x!=0;x=x/10)
    {
        y=(y+x%10)*10;
    }
    y/=10;
    cout << y <<endl; //print out the reversed integers on the screen
}

int main()
{
    while(cin >> x)
        reversal(); //call the function "reversal()"
    cout << x;
    return 0;
}
