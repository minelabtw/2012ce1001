#include <iostream>
#include <cstdlib>

using namespace std;

int changefunc(int x)  //define change function
{
    int base,final=0;  //find each item of the number one by one,and take it to the right side of [total]
    while(x!=0)
    {
         base=x%10;
         final=10*final+base;
         x=x/10;
    }
    return final;     //return the reverse number
}

int main()
{
    int num;
    do
    {    cin >>num;
         if(cin==false)  //if input character,break the loop
            break;
         cout<<changefunc(num)<<endl;  //call change function and return the reverse number
    }while(cin !=false); //if input integer,loop will continue

    system("pause");
    return 0;
}
