#include <iostream>

using namespace std;

int f (int,int,int);

int main()
{
    int t,a,b,c;
    cin >>t;
    while (t!=0)
    {
        if (0 <= a < 2147483648 && cin >>a)
        cout <<f(a,b,c)<<endl;
        t--;
    }
    return 0;
}

int f (int x,int y,int z) //自定義函數
{
    z=1;
    while (x/10!=0)
    {
        y=x%10;
        z=z*y;
        x=x/10; //每乘完一次,x就除以10,退位一次
    }
    z=z*x; //剩最後一位數時,因無法進入迴圈,故直接乘上去
    return z;
}
