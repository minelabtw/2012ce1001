#include <iostream>
#include <cmath>
using namespace std ;

int digimult(int);//function multply each digit;

int main()
{
    long long number = 1 ;

    do//do...while loop is used to do the repeat input.
    {
        cin >> number ;
        if(number == false || number < 0 || 2147483648 <= number)
        {
            return -1 ;
        }
        else
        {
            cout << digimult(number) << endl;//use "digimult" to multply the input value.
        }
    }while(number != 0);
    cout << endl ;
}

int digimult(int x)
{
    long long numbermultplied = 1 ;
    while(x != 0)
    {
        numbermultplied = numbermultplied*(x%10);
        x /= 10 ;
    }
    return numbermultplied ;
}
