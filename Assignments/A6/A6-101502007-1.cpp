#include <iostream>

using namespace std;

int g (int);

int main()
{
    int x;
    while (cin >>x )
    {
        while (x%10==0) //當x可以整除10時,將後面的0去除
        x=x/10;
        if (x<0) //對負數作處理
        {
            cout <<-g(x);
            x=x/10;
        }
        while (x/10!=0)
        {
            cout <<g(x);
            x=x/10;
        }
        if (x<0) //最後跳出迴圈,對最後一個數作處理
        cout <<-x<<endl;
        else
        cout <<x<<endl;
    }
}

int g (int y) //自定義函數
{
    y=y%10;
    if (y>0)
    return y;
    if (y<0)
    return -y;
}
