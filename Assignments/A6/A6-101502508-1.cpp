#include<iostream>

using namespace std ;

int InsideOut ( int ) ; //define a function

int main()
{
    int number ;

    while ( cin >> number ) //to input a number many times
    {
        if ( cin == false )//if input is not a integer, it will end
            return 0 ;

        cout << InsideOut ( number ) << endl ;
    }
}

//funtion Insidout definition
int InsideOut ( int x )
{
    int sum = 0 ;//initialize the value of sum

    //use loop to let the input inside out
    while ( x != 0 )
    {
        sum = sum * 10 + x % 10 ;
        x = x / 10 ;
    }

    return sum ;
}

