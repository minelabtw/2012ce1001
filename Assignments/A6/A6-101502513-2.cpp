#include <iostream>
using namespace std;

int multiply( int ); //function prototype

int main()
{
    int T, n;

    cin >> T;
    for( T; T > 0; T-- ) //input T times
    {
        if( cin >> n && n >= 0 && n < 2147483648 )
            cout << multiply( n ) << endl; //function call
        else
            break;
    }
}

int multiply( int x ) //if n is abcd (every letter means a number)...
{
    int sum = 1;

    while( x > 0 )
    {
        int y = x % 10; //..., find d , c , b , and a in turn
        x /= 10;
        sum *= y;
    } //calculate a * b * c * d
    return sum;
}
