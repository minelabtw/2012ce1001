/*
    Assignment #6-1
    A6-976001044-1.cpp
    Input a non-zero integer, which can be positive and negative
    Reverse the order of number as a result
    And then output the result
    --------------------------------
    Input a character or zero or floating-point at runtime,
    the whole program will shutdown immediately
*/
#include <iostream>
#include <cstdlib>
#include <cmath>
// Integer can only store -2147483648 ~ 2147483647
// If input number is between ( -Bound-1 ~ Bound ), then the value can be store in variable,
// if input number is out of this range, then the instruction cin will be false
#define Bound 2147483647
using namespace std;
int ReverseOutput(int);

int main()
{
    int input=1;

    // if input is not a character and non-zero integer, call function ReverseOutput
    // if input number is out if range, cin will be false and stop the loop
    while ( (cin >> input) && (input != 0) )
    {
        if ( input < 0 )
        {
            // if input < -Bound, abs(input) will casue over flow
            // the following condition will work only when the input number is < -Bound-1 (-2147483648)
            if ( input < -Bound )
            {
                cout << "The input number is out of range!!" << endl;
                system("pause");
                exit(-1);
            }
            input = abs(input);
            cout << "-";
        }

        ReverseOutput(input);
    }

    system("pause");
    return 0;
}

int ReverseOutput(int n)
{
    int Boundary=0, Quotient=0, Remainder=0;
    bool ZeroFlag=false;

    // Boundary = log(n), which is positive integer,
    // Ues to set the Highest significant digit
    Boundary = int(log10(double(fabs(n)))) + 1;

    // Under the Decimal system, Divided by 10 in each loop
    // And let the Quotient replace the input number
    for ( int i=1 ; i <= Boundary ; i++ )
    {
        Quotient = n / 10;
        Remainder = n % 10;

        if ( Remainder != 0 )
        {
            ZeroFlag = true;
        }

        if ( ZeroFlag )
        {
            cout << Remainder;
        }

        n = Quotient;
    }
    cout << endl;
    return 0;
}
