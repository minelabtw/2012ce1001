#include <iostream>
#include <cstdlib>

using namespace std;

int multifunc(int x)  //define multiply function
{
    int total=1;
    if(x==0)           //if number value is zero,return zero
       return 0;
    while(x>0)         //if number value is larger than zero,find each item of the number and multiply them
    {
       if(x%10==0)      //but if one item value is zero,return 0
           return 0;
       else if( x%10 != 0)
       {
           total=total*(x%10);
           x=x/10;
       }
    }
    return total;      //return the total multiplication
}

int main()
{
    int item,num;
    cin >>item;         //input groups of numbers,you want to check
    for(int i=1;i<=item;i++)
    {
       cin >>num;      //input number value
       cout<<multifunc(num)<<endl;   //call multiply function and return the total
    }

    system("pause");
    return 0;
}
