//Assignment 6-1
//This problem does not ask us to do exit-program condition
//So suppose that if we input a zero or negative numbers then it will exit
//Also, we can do repetitive input.
#include<iostream>
using namespace std;

int reverse(int x) //A self-defining function called "reverse" and return the data type of int
{
    int rev = 0; //Initialize the reverse numbers
    while( x ) //Use a math method to reverse the numbers
    {
        rev *= 10;
        rev += x % 10;
        x /= 10;
    }
    return rev;
}
int main()
{
    int input;
    while (cin>>input && input > 0) //Exitting condition
        cout<<reverse(input)<<endl; //Call self-defining function and print
    return 0;
}
