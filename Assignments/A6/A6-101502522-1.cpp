#include<iostream>
using namespace std;

int trans( int n ) // 自定義的轉換函式trans
{
    int r = 0;

    if ( n > 0 ) // 正數轉換
    {
        for ( ; n >= 1 ; n = n/10 )
            r = r * 10 + n % 10;
    }
    else if ( n < 0 ) //負數轉換
    {
        for ( ; n <= -1 ; n = n / 10 )
            r = r * 10 + n % 10;
    }
    return r;
}
int main( )
{
    int a;
    while ( cin >> a && cin != false ) //重複輸入,輸出轉換後的數字,輸入錯誤則跳出
    {
        cout << trans(a) << endl;
    }
    return 0;
}
