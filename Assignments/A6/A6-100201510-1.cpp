#include <iostream>
using namespace std ;

int reverse(int); //reverse the order of digits.

int main()
{
    long long number = 1 ;

    do//do...while loop is used to do the repeat input.
    {
        cin >> number ;
        if(number == false || number >= 2147483648 || number <= 2147483648)
        {
            return -1 ;
        }
        else
        {
            cout << reverse(number) << endl;//use "reverse" to reverse the input value.
        }
    }while(number != 0);
    cout << endl ;
}

int reverse(int x)
{
    long long numberreversed = 0 ;
    while(x != 0)
    {
        numberreversed = 10*numberreversed + x%10 ;
        x /= 10 ;
    }
    return numberreversed ;
}
