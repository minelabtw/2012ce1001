/********************************************/
/*                                          */
/* Rerunable, input Q or q to quit          */
/* Invalid input prevented, ex: 123abc, 0.5 */
/*                                          */
/********************************************/
#include<iostream>
#include<string>
#include<cstdlib>
#include<cmath>
using namespace std ;

class CNumberReverse
{
    public:
        int NumberReverse();//function to reverse the number
    private:
        bool Input();//function of input
        int iNumber;//to store the input
};
int CNumberReverse::NumberReverse()
{
    while(Input())//Input and check
    {
        if(iNumber<0)
        {
            cout<<"-";
            iNumber=abs(iNumber);//transfer the negative into positive
        }
        if(iNumber==0)
        {
            cout<<"0"<<endl;
        }
        else
        {
            while(iNumber%10==0)//eliminate not need 0s
            {
                iNumber/=10;
            }
            while(iNumber!=0)
            {
                cout<<iNumber%10;
                iNumber/=10;
            }
            cout<<endl;
        }
    }
}
bool CNumberReverse::Input()
{
    string str;
    while(1)
    {
        bool fInput=true;
        cin>>str;
        if(str=="q"||str=="Q")//If Input "Q" or "q"
        {
            return false;
        }
        if(str.c_str()[0]!='-')//Check each character if it is a number
        {
            for(int i=0;i<str.length();i++)
            {
                if(str.c_str()[i]<'0'||str.c_str()[i]>'9')
                {
                    cout<<"Invalid input."<<endl;
                    fInput=false;
                    break;
                }
            }
        }
        else
        {
            for(int i=1;i<str.length();i++)
            {
                if(str.c_str()[i]<'0'||str.c_str()[i]>'9')
                {
                    cout<<"Invalid input."<<endl;
                    fInput=false;
                    break;
                }
            }
        }
        if(fInput)
        {
            iNumber=strtol(str.c_str(),NULL,10);//transfer the string into integer
            return true;
        }
    }
}
int main()
{
    CNumberReverse NumRev;//Declaration NumRev of class CNumberReverse
    NumRev.NumberReverse();
    return 0;
}

