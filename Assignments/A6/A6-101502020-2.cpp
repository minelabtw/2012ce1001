//A6-2 by J
//no additional function
//wrong detections:no letter input, no negative number input, no number with fraction, no overflow input.
#include <iostream>
using namespace std;
int tool(int);
int main()
{
    int chk ;
    double tme , inp ;

    if ( cin>>tme==false )      //3 input checks below:1.char input. 2.negative input.
        return -1 ;             //3. input with fraction.
    else if ( tme<0 )
        return -1 ;
    else if ( (chk=tme)!=tme )
        return -1 ;

    while ( tme>0 )
    {
        if ( cin>>inp==false )      //3 input checks below:1.char input. 2.negative input.
            return -1 ;             //3. input with fraction.
        else if ( inp<1 )
            return -1 ;
        else if ( (chk=inp)!=inp )
            return -1 ;
        cout << tool(inp) << endl ;
        tme--;
    }

    return 0 ;
}

int tool( int x )
{
    int oup=1 ;
    while ( x>0 )
    {
        oup=oup*(x%10);
        x/=10;
    }
    return oup;
}
