#include<iostream>

using namespace std;

int un(int); //un=>倒置函數
int x,outp;  //輸入x 輸出outp
int main()
{
    cin >> x;
    while(cin != false)     //重複輸入,輸入非數字則結束程式
    {
        cout << un(x) << endl;  //輸出結果
        cin >> x;
        outp = 0;               //更新
    }
    return 0;
}

int un(int)
{
    while(x!=0)     //倒置
    {
        outp *= 10;
        outp += x%10;
        x /= 10;
    }
    return outp;    //函式結果帶回main
}
