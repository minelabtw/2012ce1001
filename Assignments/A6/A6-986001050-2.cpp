#include <iostream>
using namespace std;

long multi( long );
long number;
int notds;  //nummber of test datas
int i=1;

int main()
{
    cin >> notds;
    while( cin >> number && number > 0)
    {
        cout << multi( number ) << endl;     //function call
        i++;
        if( i > notds )
            break;
    }
    return 0;
}
//multi function difinition returns "每個位數都乘在一起"
long multi( long a )
{
    long c=1;
    while ( a >= 10 )
    {
        int b = a%10;
        c = c*b;
        a = (a-b)/10;
    }
    c = c*a;
    return c;    //return c is "每個位數都乘在一起"
}
