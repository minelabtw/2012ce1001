#include <iostream>
#include <math.h>
using namespace std;

int x,y;


int reverse(int x)
{
    int i=0;//跳出回迴圈並計算執行次數
    int sum=0;//表反轉後的結果
    y = x;//y用來代替初始的x值



    while( x != 0)
    {
        x /= 10;
        i++;
    }
    while( y != 0)
    {
        sum += (y%10)*pow(10,i-1);
        y /= 10;
        i--;
    }
    return sum;
}

int main()
{
    while ( cin >> x )
    {
        if ( x > pow(2,31))//錯誤偵測
        return -1;

        cout << reverse ( x ) << endl;
    }
    return 0;
}





