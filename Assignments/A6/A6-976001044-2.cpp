/*
    Assignment #6-2
    A6-976001044-2.cpp
    -------------------------
    In the main function :
        First, input a integer, and assign the value to variable InputTime
        InputTime is use to setup the user can execution how many times of computing
        Second, input a integer, and assign the value to variable InputNumber
        InputNumber is the input data which will be process in subroutine
    In the Multiplication function :
        Decomposition the input integer to some Single digits integer
        Then Multiply each integer
    -------------------------
    Input a character or zero or negative integer or floating-point at runtime,
    the whole program will shutdown immediately
*/
#include <iostream>
#include <cstdlib>
#include <cmath>
#define Bound 2147483647
using namespace std;
int Multiplication(int);

int main()
{
    int InputNumber=0, InputTime=0;

    // if the InputTime is not character and positive, do the following instruction
    if ( (cin >> InputTime != false) && (InputTime > 0) )
    {
        // And if the InputNumber is not character and positive, do the following loop by InputTime times
        while ( (cin >> InputNumber != false) && (InputNumber > 0) )
        {
            Multiplication(InputNumber);
            InputTime = InputTime - 1;
            if ( InputTime <= 0 )
            {
                break;
            }
        }
    }

    system("pause");
    return 0;
}

// According to the function in A6-1#
// Decomposition the input integer, and then Multiply each number and output that
int Multiplication(int n)
{
    int Boundary=0, Quotient=0, Remainder=0, Result=1;
    Boundary = int(log10(double(n)));

    for ( int i=1 ; i <= Boundary+1 ; i++ )
    {
        Quotient = n / 10;
        Remainder = n % 10;

        if ( Remainder == 0 )
        {
            Result = 0;
            break;
        }

        Result = Result * Remainder;

        n = Quotient;
    }
    cout << Result << endl;
    return 0;
}
