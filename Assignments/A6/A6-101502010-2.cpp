#include<iostream>
using namespace std;
int cross(int n)//自定義函數
{
    int k=1;
    if(n==0)//若輸入為0時
        k=0;//k為0
    while(n!=0)
    {
        k=k*(n%10);//k為原本的k乘上n之尾數
        n=n/10;//將n的尾數刪除
    }
    return k;//回傳k值
}
int main()
{
    int n,t;
    cin>>t;
    while(t!=0)//當計數器(t)為0時跳出迴圈
    {
        cin>>n;//輸入需運算的數
        if(cin==false)//若輸入為字元
            break;//則跳出迴圈
        cout<<cross(n)<<endl;//輸出結果
        t--;//計數器-1
    }
    return 0;//程式結束
}
