#include<iostream>
#include<cmath>
using namespace std;

int a(int);

int main()
{
    int y;
    while(cin>>y and y<pow(2,31))
        cout<< a(y) << endl;
}

int a(int y)
{
    int c = 0;
    int b = y/10;
    while(b != 0)//找出a函式須執行幾次
    {
        c++;
        b /= 10;
    }
    int sum = 0;//最初的總和

    while(c >= 0)//執行c+1次
    {
        sum += y%10 * pow(10,c);
        y = y/10;
        c--;
    }
    return sum;//結束函式a
}
