//Assignment 6-2
//First, define a int "convert" to calculate consecutive numbers
//Next, according to the input (times calculated) to design a loop function
#include<iostream>
using namespace std;

int convert(int y) //A math method to make integers multiply with each other one by one
{
    int z=1;
    int x;
    while (y)
    {
        x=y%10;
        y/=10;
        z*=x;
    }
    return z;
}
int main()
{
    int count; //Times we want to calculate
    int input; //The number we want to calculate
    while (cin>>count && count>0) //When count<=0, we end the program
    {
        for (int i=1;i<=count;i++)
        {
            cin>>input;
            if (input>=0 && input<2147483648) //Meet the given conditions
                cout<<convert(input)<<endl;
        }
    }
    return 0;
}
