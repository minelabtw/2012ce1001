#include <iostream>
#include <cmath>
using namespace std;

int reverse( int );

int main()
{
    int n1;
    while(cin>>n1)//用來防止字元
    {
        if(n1>0)
            cout<<reverse(n1)<<endl;
        else
            cout<<-reverse(-n1)<<endl;//修正負數的情形
    }
    return 0;
}
int reverse( int x )//定義reverse
{
    int q,w=0,y,z=0;
    q=x;
    while (x>=1)
    {
        x=x/10;
        w++;
    }//計算幾位數
    while (w>0)
    {
        y=q%10;
        q=q/10;
        z=z+y*pow(10,w-1);//因為W位數的意義是數字後有W-1個0,所以用W-1
        w--;
    }
    return z;//回傳Z值

}
