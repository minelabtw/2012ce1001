#include<iostream>

using namespace std;

int axbxc( int );//axbxc()為一自定義函式

int main()
{
    int t, n, i, sum;
    cin >> t;//t代表輸入輸出的組數
    axbxc( t );//將t傳給axbxc()函式
}

int axbxc( int t )//接收main輸入的t
{
    int n, i, sum;

    for( ; t > 0 ; t-- )
    {
        cin >> n;
        sum = 1;//sum為所有數字之乘積
        while( n > 0 )
        {
            i = n % 10;//一一拆解並乘積
            sum *= i;
            n /= 10;
        }

        cout << sum << endl;//輸出結果
    }

    return 0;

}
