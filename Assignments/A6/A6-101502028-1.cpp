#include <iostream>
#include <cstdlib>
using std::cout;
using std::cin;
using std::endl;
long inverted(int, int); // function prototype
int main()
{
    int num;
    int inv;
    while (true)
    {
        cin >> num;
        if (cin == false) // character inputs, break
            break;
        cout << inverted(num, inv) << endl; // num, inv are arguments to the function inverted
    }
    return 0; // ends
}
long inverted(int x, int y) // function inverted definition using x and y as parameter
{
    y = 0; // first number of the total is 0
    while (x != 0) // loop when x is not 0
    {
        y = (y * 10) + x % 10; // input the last number of the x to y
        x = x / 10; // select the next x last number
    }
    return y; // inversed number
} // end function inverted
