#include<iostream>
#include<cmath>
using namespace std;

int change(int);//自定義一個函數

int main()
{
    int a;
    while(cin>>a && a < pow(2,31))//輸入一個數a且a必須小於2^31
    {
        cout<<change(a)<<endl;//輸出自定義函數裡的m
    }
}

int change(int x)
{
    int y , z , n = -1 , m = 0 , k = x;
    while(x >= 1 || x <= -1)//計算x是幾位數
    {
        x /= 10;
        n++;
    }

    while(n >= 0)//將數字翻轉過來
    {
        y = k % 10;//取k的最後一個數
        k /= 10;//k除以10
        z = y * pow(10,n);//z = y * (10^n)
        n--;//n-1
        m += z;//m = m + z
    }
    return m;//將m回傳回int main()裡面
}
