#include <iostream>
using namespace std;

int swap(int);//function prototype

int main()
{
    int number;
    while (cin >> number )//repeat inpute
    {
        cout <<swap(number)<<endl;
    }
    return 0;
}//end main

//detail of function
int swap(int number1)//number1 is a copy argument to function
{
    int sum=0;
    while (number1 != 0)
    {
        sum = sum + number1%10;
        sum = sum*10;
        number1 =number1/10;
    }
    sum = sum/10;

    return sum;//return sum as an int
}
