#include <iostream>
using namespace std;

int multiply ( int n ) //自定義函式multiply,將某數n轉換為每個位數相乘(a)
{
    int a = 1;
    for( ; n >= 1 ; n /= 10 )
        a = a * ( n % 10 );
    return a;
}
int main()
{
    int t;
    int m;
    cin >> t;
    if ( !cin ) //偵錯非數字輸入
        return 0;

    for ( int i = 1 ; i <= t ; i++ ) // 判斷使用者需要轉換多少個數字,並輸出結果
    {
        cin >> m;
        if ( m == 0 ) //0轉換直接輸出0
        {
            cout << "0";
            return 0;
        }
        cout << multiply( m );
        cout << endl;
    }
    return 0;
}
