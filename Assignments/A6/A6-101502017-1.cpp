#include <iostream>
#include <math.h>//利用絕對值

using namespace std;

int main()
{
    int a,n,buf,sum=0,x=1;

    while(cin >> n )//重複輸出 + 偵錯
    {
        buf = n;
        while( fabs(n) >= 10)//計算幾位數
        {
            n/=10;
            x++;
        }

        for(int i=1;i<=x;i++)//數字顛倒
        {
            a = buf % 10;
            buf = buf / 10;
            sum = (sum + a) * 10;
        }
        sum /= 10;//把個位數多乘的10消掉
        cout <<sum<<endl;
        sum = 0;
        x = 1;
    }
    return 0;
}
