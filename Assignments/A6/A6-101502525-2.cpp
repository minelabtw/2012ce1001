//enter not integer or negative number to exit when anytime.

#include<iostream>
#include<stdlib.h>
#include<limits>
using namespace std;


bool inputcheck(int in);
int selfMultiply(int n);


int main()
{
    //initial
    int n,in;


    //input times
    cin>>n;
    if(!inputcheck(n))//exit
        return -1;


    for(int i=0;i<n;i++)
    {
        //input numbers
        cin>>in;
        if(!inputcheck(in))//exit
            return -1;


        else//compute & output
            cout<<selfMultiply(in)<<endl;
    }


    //end
    cout<<endl;
    system("pause");
    return 0;
}




bool inputcheck(int in)
{
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    if(cin.gcount()!=1)//not integer
        return false;
    else if(in<0)//negative
        return false;

    //no error
    return true;
}


int selfMultiply(int n)
{
    if(n/10==0)
        return n%10;
    return n%10*selfMultiply(n/10);
}
