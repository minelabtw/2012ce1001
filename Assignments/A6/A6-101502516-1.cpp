#include <iostream>
using namespace std;

long long ac ( long long ); // 自訂函式
long long aacc ( long long );

int main()
{
    while ( 1 ) // 除非輸入字串，否則進入迴圈
    {
        long long b;
        if ( cin >> b == false )
            return 0;
        else if ( b < 0 )
            cout << aacc ( b ) << endl;
        else
            cout << ac ( b ) << endl;
    }
    return 0;
}

long long aacc ( long long c )  // 輸入的值小於0的函式
{
    long long d = 1;
    long long e = 1;
    long long sum = 0;
    long long f = 1;
    c = (-1) * c;
    long long g = c;

    for ( ; c > 0; c = c / 10 ) // 判斷位數
    {
        if ( c / 10 > 0 )
        {
            d++;
            for ( ; f < d ; f++ )
            {
                e = e * 10;
            }
        }
    }
    for ( ; g > 0; g = g / 10 ) // 計算最後結果
    {
        sum += ( g % 10  * e );
        e = e / 10;
    }
    return (-1) * sum;
}

long long ac ( long long c ) // 輸入值大於或等於0的函式
{
    long long d = 1;
    long long e = 1;
    long long sum = 0;
    long long f = 1;
    long long g = c;

    for ( ; c > 0; c = c / 10 ) // 判斷位數
    {
        if ( c / 10 > 0 )
        {
            d++;
            for ( ; f < d ; f++ )
            {
                e = e * 10;
            }
        }
    }
    for ( ; g > 0; g = g / 10 ) // 計算最後結果
    {
        sum += ( g % 10 * e );
        e = e / 10;
    }
    return sum;
}
