/********************************************/
/*                                          */
/* Invalid input prevented, ex: 123abc, 0.5 */
/*                                          */
/********************************************/
#include<iostream>
#include<string>
#include<cstdlib>
using namespace std;

class CMultiply
{
    public:
        void Multiply();//function to multily each number in a integer
    private:
        int Input();//function to input
};
void CMultiply::Multiply()
{
    int iTimesCounts=Input();
    int iInputNumber;
    for(int i=0;i<iTimesCounts;i++)//Count how much times to run
    {
        int a=0;
        iInputNumber=Input();
        if(iInputNumber!=0)
        {
            a=1;
            while(iInputNumber!=0)
            {
                a*=iInputNumber%10;
                iInputNumber/=10;
            }
        }
        cout<<a<<endl;
    }
}
int CMultiply::Input()
{
    string str;
    while(1)
    {
        bool fCheckInput= true;
        cin>>str;
        for(int i=0;i<str.length();i++)//check each character in the input
        {
            if(str.c_str()[i]<'0'||str.c_str()[i]>'9')
            {
                cout<<"Invalid input."<<endl;
                fCheckInput=false;
                break;
            }
        }
        if(fCheckInput)
        {
            return strtol(str.c_str(),NULL,10);
        }
    }
}
int main()
{
    CMultiply Mul;//Declaration Mul of class CMultiply
    Mul.Multiply();
    return 0;
}
