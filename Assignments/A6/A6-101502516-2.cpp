#include <iostream>
using namespace std;

int ac ( int ); // 自訂函式

int main()
{
    int a;
    int h = 1;
    int b;

    if ( cin >> a == false || a < 0 ) // 當輸入值a不是正整數結束程式
        return -1;
    else
        while ( a >= h ) // 當輸入值a大於等於h時進入迴圈
        {
            cin >> b;
            cout << ac ( b ) << endl;
            h++;
        }
        return 0;
}

int ac ( int x ) // 將各個位數相乘的函式
{
    int z = 1;
    int y;

    if ( x == 0 ) // 特殊狀況，如果輸入值為0，輸出0
        z = 0;
    else for ( ; x > 0; x = x / 10 ) // 正常狀況下的計算公式
        {
            if ( x / 10 >= 0 && x % 10 != 0 )
            {
                y = x % 10;
                z = z * y;
            }
            else
                z = 0;
        }
    return z;
}

