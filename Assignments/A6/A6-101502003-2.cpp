#include <iostream>

using namespace std; //可以省略std::

int abc(int);//自訂的函式
int main()
{
    int a,b,c; //a=輸入的次數,b=計算總共乘了幾次,c=要分開乘的數
    b=1; //一開始b=1
    cin >> a; //輸入幾次
    while (b<=a) //如果b<=a進入迴圈
    {
        cin >> c; //輸入要乘的數
        cout << abc (c) << endl; //輸出結果
        b++; //b+1再進入迴圈
    }
    return 0;
}

int abc(int x) //自訂的函式
{
    int y,z=1;
    if (x!=0) //不等於0的話
    {
        while (x>0)
        {
            y=x%10;
            z=z*y; //餘數互乘
            x=(x-y)/10;
        }
    }
    else //等於0的話
        z=0; //答案=0
    return z;
}
