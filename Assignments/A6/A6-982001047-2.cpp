#include<iostream>

using namespace std ;

long happy( long y );    //自定義函數  happy

int main()
{
    
    int a,i;
    long b;
    cin >> a;
    for(i=0; i<a ;i++)      //  執行 a 次 
    {
      cin >> b;             //  輸入數字 
      cout << happy(b) << endl;     // 使用函數 
    }

  return 0 ;   
}

long happy( long y )
{
     long x;  
     x=1;              // 起始值定1 
     while( y != 0 ) 
     {
        x *= y%10;     //  把各個位數相乘 
        y /= 10;
     }
  return x;
}
