#include<iostream>

using namespace std;

int mu(int);        //宣告函式
int x,i,t,end=1;    //輸入x 次數t 輸出end
int main()
{
    cin >> t;
    for(i=1;i<=t;i++)   //重複執行,次數t
    {
        cin >> x;
        cout << mu(x) << endl;  //輸出結果
        end = 1;
    }
    return 0;
}

int mu(int)         //定義函式
{
    while(x != 0)
    {
        end *= x%10;
        x /= 10;
    }
    return end;     //結果帶回main
}
