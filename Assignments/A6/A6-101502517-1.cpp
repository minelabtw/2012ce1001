#include<iostream>
using namespace std;
long long Reverse(long long a)
{
    long long b=0,d=0,f=1,o=1;
    if(a<0)//處理負數
    {
        a=a*-1;
        o=0;
    }
    for(long long c=a;c>0;c=c/10)//計算輸入值的位數
        d++;
    long long e[d];
    for(long long c=1;c<=d;c++,a=a/10)//自個位數將每一位數存入陣列
        e[c-1]=a%10;
    for(long long c=d-1;c>=0;c--,f=f*10)//將陣列反向讀取，並加上位數
        b=b+e[c]*f;
    if(o==1)
        return b;//傳回計算值
    else//若為負
        return -b;//傳回-計算值
}
int main()
{
    long long a;
    while(cin>>a)//近回圈，輸入值
       cout<<Reverse(a)<<endl;//輸出他
}
