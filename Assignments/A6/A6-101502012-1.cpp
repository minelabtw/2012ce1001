#include <iostream>
#include <cmath>
#include <limits>
using namespace std;
int reverse(int);//設函數exchage()可以翻轉數字

int main()
{
    int x;
    while (1) //永遠迴圈
    {
        cin >> x;
        if (cin==false) //錯誤偵測:輸入char直接忽略
        {
            cin.clear(); //重置cin為初始值
            cin.ignore(numeric_limits<streamsize>::max(),'\n'); //刪除cin的暫存值
            cout << "Invalid input" << endl;
            continue;
        }
        cout << reverse(x) << endl;
    }
    return 0;
}//end main

int reverse(int y)
{
    int count=-1,buffer,answer=0,z=1; //z來判斷y是否是正數
    if (y<0)//小於零先使值便正值
    {
        z=0;
        y=y*-1;
    }
    buffer=y;//暫存原來的值
    while (y>0)
    {
        y=y/10;
        count++;
    }//count用來計算y是幾位數
    while (buffer>0)
    {
        if (buffer>0)
        answer=answer+(buffer%10)*pow(10,count);
        count--;
        buffer=buffer/10;
    }
    if (z==0) //當y是負數 因為前面轉成正數 後面再把他轉回負數
        answer=answer*-1;
    return answer;
}//end reverse

