#include<iostream>
#include<cmath>

using namespace std;

int turn(int);//自定義函數

int main()
{
    int x;
    while(x<=pow(2,31))//根據題目意思限制變數的範圍
    {
        cin >> x;
        if(cin==false)//若變數不屬於數字則直接跳出迴圈
        {
            break;
            return -1;
        }
        cout << turn(x) << endl;//輸出的值為自定義函數的回傳值
    }

    return 0;
}

int turn(int y)//此函數可運算數字翻轉
{
    int i=-1,z=y;
    while(z!=0)//此迴圈為計算最高位為10的i次方
    {
        z/=10;
        i++;
    }

    int a=0,total=0;
    while(y!=0)//此迴圈為利用加總讓數字翻轉
    {
        a=y%10;//a為餘數
        total+=a*pow(10,i);//乘上10的次方後加總
        i--;//十的次方位數下降
        y/=10;//計算位數
    }
    return total;//回傳值
}
