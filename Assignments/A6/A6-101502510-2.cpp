#include<iostream>
using namespace std;

int square(int);//自定義一個函數

int main()
{
    int t,a;
    cin>>t;
    for(int i = 1; i <= t; i++)//設定可以讓使用者輸入幾次
    {
        if(cin>>a && a>=0 && a<2147483648)//輸入一個數a且0<=a<2147483648

            cout<<square(a)<<endl;//輸出自定義函數裡的z

        else

            return 0;

    }
}

int square(int x)
{
    int y , z = 1;
    while(x >= 1)//數字相乘
    {
        y = x % 10;//取x的最後一個數
        x /= 10;//將x除以10
        z *= y;//z = z * y
    }
    return z;//將z回傳回int main()裡
}
