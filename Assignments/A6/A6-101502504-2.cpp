#include <iostream>
using namespace std;

int Multiply(int);//function prototype

int main()
{
    int a,n;//variable declaration
    if ( cin >> a == false )//determine how many time does user want to input
        return 0;
    for (;a>0;a--)//loop until a=0
    {
        if ( cin >> n == false )//n is not an integer
            return 0;
        n <= 214748364;
        if ( n < 0 )//input<0
        {
            n=-n;
            cout << -Multiply(n) << endl;
        }
        else if ( n > 0 && n < 2147483648 )//input>0
            cout << Multiply(n) << endl;
        else
            cout << 0 << endl;
    }
}//end main

int Multiply(int x)//function Multiply definition && x is parameter
{
    int sum=1;
    for (;x>0;)//let sum be the multiply of every number
    {
        sum*=(x%10);
        x=x/10;
    }//end loop
    return sum;
}//end function  Multiply
