// 101502503
#include<iostream>

using namespace std ; // program uses names from the std namespace

int xyz(int);

int main ()
{
    int t , n; // 宣告整數

    if ( cin >> t == false || t <=0 ) // 如果輸入t為字元或小於等於0
    {
        return -1 ; // 結束程式
    } // end if
    else
    {
        for( int i = 1 ; i<=t ; i++) // 重複輸入t次
        {
            if ( cin >> n == false || n < 0 || n > 2147483647 ) // 如果輸入n為字元或n<0或n>2147438647
            {
                return -1 ; // 結束程式
            } // end if
            cout << xyz(n) << endl ; // 輸出結果
        } // end for
    } // end else
    return 0 ;
} // end main

int xyz(int n)
{
    int  b=0,c=1 ; // 宣告整數

    while ( n > 0 ) // 當n>0,進入while迴圈
    {
        b = n%10 ; // 取最後一位數
        c *= b ; // 將最後一位數相乘
        n = n/10 ; // 去掉n的最後一位數
    } // end while
    return c ; // 回傳c
}
