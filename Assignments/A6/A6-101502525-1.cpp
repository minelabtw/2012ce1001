//enter not integer to exit

#include<iostream>
#include<stdlib.h>
#include<limits>
using namespace std;


bool inputcheck(int in);
int reverse(int n);


int main()
{
    //initial
    int n;


    while(1)
    {
        //input
        cin>>n;
        if(!inputcheck(n))
            break;


        //compute & output
        cout<<reverse(n)<<endl;
    }


    //end
    cout<<endl;
    system("pause");
    return 0;
}




bool inputcheck(int in)
{
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    if(cin.gcount()!=1)//not integer, exit;
        return false;
    //no error
    return true;
}


int reverse(int n)
{
    int sum=0,pow=1;

    for(;;pow*=10)//find the power of n
        if(abs(n)<pow*10)
            break;
    for(;pow>0;pow/=10,n/=10)
        sum+=(n%10)*pow;

    return sum;
}
