#include<iostream>
using namespace std;

int reverse(int);

int main()
{
    int a;
    while (cin >> a && cin!=false)
        cout << reverse(a) << endl;

    return 0;
}

int reverse (int x) //將數字倒轉之函式
{
    int y=0;

    if (x>0) //正數之倒轉
    {
        for (; x>=1 ; x=x/10)
            y=y*10+x%10;
    }

    else if (x<0) //負數之倒轉
    {
        for (; x<=-1 ; x=x/10)
            y=y*10+x%10;
    }

    return y;
}
