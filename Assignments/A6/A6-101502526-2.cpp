#include <iostream>                               //可計算負數//

using namespace std;

int result(int x)                                 //總值先設1
{                                                 //一直將輸入值去掉尾數(除10)
    int total = 1;                                //而去掉的尾數乘上總值
    int a;                                        //之後回傳總值
    for (; x>0 || x<0 ;)
    {
        a = x % 10;
        total = total * a;
        x /= 10 ;
    }
    return total ;
}
int main()
{
    int number;                                   //計算所需要數入的次數，大於則結束
    int times;                                    //輸出經由凾式運算的結我
    int count = 1;                                //另外輸入若為0則直接輸出0
    cin >> times;
    for (;count <= times;count ++)
    {
        cin >> number;
        if (number == 0)
            cout << "0" ;
        else
            cout << result(number) << endl;
    }
    return 0;
}
