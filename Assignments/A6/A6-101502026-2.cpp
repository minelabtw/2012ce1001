//if x entered is out of the range of 0<x<2147483648 or isn't an integer,then exit the program
//if y entered is out of the range of 0<=y>2147483648 or isn't an integer, then exit the program
#include <iostream> //include the header file "iostream"
#include <cstdlib> //include the header file "cstdlib"

using namespace std;

int x,y; // x is the number of times that the user can enter y
         // y is the number that will be multiplied

int main()
{
    cin >> x;
    for(int i=1;i<=x;i++)
        multiplication(int y,int z); //call the function "multiplication"
    return 0;
}

void multiplication() //this is the function to multiplied y entered
{
    int y,z=1;
    cin >> y;
    if(cin==false||y<0) //if y isn't an integer or smaller than 0,then exit the program
        exit(0);
    if(y>0)
    {
        for(;y>=1;y=y/10) //this is the loop to let y be multiplied
            z=z*(y%10);
        cout << z << endl;
    }
    else
        cout << "0" << endl; //if y entered is 0,then print out 0
}
