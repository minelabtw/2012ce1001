//遞迴想法
//重複輸入
#include<iostream>
int cro(int);
using namespace std;
int main()
{
    int a,b;
    while(cin>>a&&a>=0)//輸入非正整數或非0則跳出
    {
        for(int x=0;x<a;x++)
        {
            cin>>b;
            if(b==0)//0另外判斷 因為我設x==0時傳回直微1 所以輸入0時需另外判斷
                cout<<'0'<<endl;
            else
                cout<<cro(b)<<endl;//正整數
        }
    }
    return 0;
}
int cro(int x)
{
    if(x!=0)
        return x%10*cro(x/10);//每%一次就乘一次進入遞迴
    else
        return 1;//當0時傳為1

}
