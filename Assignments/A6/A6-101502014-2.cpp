#include<iostream>
using namespace std;
int mutiply(int);
int main()
{
    int t , n; //t紀錄要輸入幾個數字
    while(cin >> t)
    {
        for(int i = 0 ; i < t ; i++) //迴圈要跑 t 次
        {
            cin >> n;
            cout << mutiply(n) << endl;
        }
    }
	return 0;
}
int mutiply(int num) //mutiply函式用來把num的每一位數乘起來
{
    int ans = 1; //ans記錄每一位數相乘後的答案
    do //預防若num = 0 , 而進不去while迴圈的狀況
    {
        ans *= num % 10; //ans乘num的每一位數
        num /= 10; //num後退一位
    }while(num > 0); //判斷num的每一位數有沒有抓出來
    return ans; //回傳每一位數相乘後的答案
}
