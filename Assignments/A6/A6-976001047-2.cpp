#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;

int chenchenhappy(int);

int main()                           //this program is run a function chenchenhappy
{                                   
    int input=0;                
       
    while ( (cin >>input!=false) )
    { 
        chenchenhappy(input);  
    }
    
    system ("PAUSE");
    return 0;
}


int chenchenhappy(int n)           //function chenchenhappy : input a integer number , the first input number decide you can 
{                                  //play how many times chenchenhappy.
                                   //next input numbers will be separate a few number , they time each other
    int x=0;                       //then out put the anwser.  ex: in:3256 =>out:3*2*5*6=180
    int y=0;                       
    int w=0;
    double z=0;
    int total=0;

    for (int i=1 ; i<=n ; i++)
    {   
        cin >> x;          
        z=x;
        y=int(log10(z)); 
              
        w = x%10;
        total = w * int(x/pow(10,y));
             
        while (x>100)
        {            
             x=x/10;
             total =total * (x%10);            
        }   
                  
        cout << total <<"\n"; 
    }  
    system ("PAUSE");            
    return 0;
}
