// 101502503
#include<iostream>
#include<cmath>

using namespace std ; // program uses names from the std namespace

int xyz(int);

int main ()
{
    int x ; // 宣告整數

    while ( x <= pow(2,31) ) // 當x小於等於2的31次方,進入while迴圈
    {
        if ( cin >> x == false ) // 如果輸入x為字元,結束程式
        {
            return -1;
        } // end if
        else if ( x > 0) // 如果x為正數,輸出結果
            cout << xyz(x) << endl ; // 輸出結果
        else //如果x為負數
        {
            x = -x ; // 將x轉成正數
            cout << "-" << xyz(x) << endl ; // 輸出結果
        } // end else
    } // end while
    return 0 ;
} // end main

int xyz(int x)
{
    int a=0 , b , c , i ,sum=0 , y=0 ; // 宣告整數

    a = x ; // 將x丟進a
    while (a>=1) // 當a>=1,進入while迴圈
    {
        y++ ; // y+1
        a=a/10 ;
    } // end while
    c = pow(10,y-1) ;

    while (x > 0) // 當x>0,進入while迴圈
    {
        b = x%10 ; // 取最後一位數字
        i = b*c ;
        sum += i ; // 將
        x = x/10 ; // 去掉X的最後一位數
        c = c/10 ;
    } // end while
    return sum ; // 回傳sum
}
