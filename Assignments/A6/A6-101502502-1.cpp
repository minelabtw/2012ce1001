#include<iostream>
#include<cmath>
using namespace std;

int reverse( int );//自訂反轉數字的函式

int main()
{
    int num;

    while(1)
    {
        if ((cin >> num) == false)//錯誤輸入
            return 0;

        cout << reverse(num) << endl;
    }

    return 0;
}

int reverse( int i )
{
    int sum = 0;

    while (i)
    {
        sum = sum * 10 + i % 10;//把mod出來的數乘10後 繼續加上去
        i /= 10;
    }

    return sum;
}
