#include <iostream>

using namespace std;

int number ( int );//自訂函式

int main()
{
    int t,n;

    if ( cin >> t == false || t <= 0 )//t需為一正數
        return -1;

    for ( int i = 1; i <= t; i++ )//讓迴圈可以跑t次
    {
        if( cin >> n == false || n > 2147483648 || n < 0 )//輸入不可為字元或大於2147483648j或小於0的數
        {
            cout <<"You need to enter a number which smaller than 2147483648." << endl;
            return -1;
        }

        cout << number ( n ) << endl;
    }
    return 0;
}

int number ( int x )
{
    int a = 1;//定a初始值為1

    while ( x > 0 )
    {
        a *= x % 10;//a等於a乘上x的餘數
        x /= 10;//x少一位數
    }
    return a;
}
