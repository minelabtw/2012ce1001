#include <iostream>
#include <cstdlib>
using std::cout;
using std::cin;
using std::endl;
long inverted(int, int); // function prototype
int main()
{
    int num;
    int total;
    int time;
    cin >> time; // number of the time of the loop
    if (cin == false || time <= 0) // error if the time is a negative number or character
        return -1;
    do // do while loop depending of the number of time
    {
        cin >> num; // input a number
        if (cin == false || num < 0) // negative number or character, break
            break;
        cout << inverted(num, total) << endl; // num, total are arguments to the function inverted
        time --; // time - 1
    }while (time > 0); // until time is cero
    return 0; // ends
}
long inverted(int x, int z) // function inverted definition
{
    int y = 0; // makes y as a contenetor
    z = 1; // first total to multiply
    if (x == 0) // if the x is 0
        z = 0; // total equals to 0
    while (x > 0) // loop for positive numbers
    {
        y = x % 10; // take the last number of x and put it in the contenetor
        x = x / 10; // take the next number of x
        z = z * y; // multiply tha taken number by the total
    }
    return z; // total multiplied number
} // end function
