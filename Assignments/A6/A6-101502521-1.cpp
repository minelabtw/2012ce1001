#include<iostream>
#include<cstring>

using namespace std;

void printans(int n); // function to print the ans

int main()
{
    int n;
    while(cin >> n)
    {
        if(n<0)     // if it's a negitive number
        {
            n = -n;
            cout << "-";
        }
        printans(n); // print the answer
    }
    return 0;
}

void printans(int n)
{
    bool sw=0;                                  //紀錄是否已經出現非零的數
    while(n>0)
    {
        if(n%10!=0&&!sw)
        {
            sw = true;
            cout << (n%10);
        }
        else if((n%10 == 0 && sw) || n%10!=0)    // 0要檢查是不是合法輸出
            cout << (n%10);
        n/=10;
    }
    cout << endl;
}
