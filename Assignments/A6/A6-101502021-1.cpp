#include <iostream>

using namespace std;

int Reverse(int N)//宣告一個自定義函式，輸入值以及回傳值都是整數
{
    int sum=0 ; //定義總合一開始為0

    if (N>0)
    {
        while (N>0)
        {
            sum*=10 ;
            sum+=N%10 ;
            N/=10 ;
        }
        return sum ; //回傳sum的值
    }
    else //如果N<=0
    {
        N=-N;
        while (N>0)
        {
            sum*=10 ;
            sum+=N%10 ;
            N/=10 ;
        }
        return -sum ;
    }
}


int main()
{
    int interger ;

    while (cin>>interger)//當輸入的是整數
    {
        cout << Reverse(interger) << endl ; //印出在Reverse函數所回傳的值
    }
    return 0 ;//結束主程式
}
