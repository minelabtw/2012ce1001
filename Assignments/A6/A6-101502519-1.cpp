#include <iostream>

using namespace std;

void aa(int c)
{
    int e,n=0;
    if(c==0)             //輸入為0的情況
        cout<<"0";
    if(c<0)
    {
        c=-1*c;          //輸入值為負數時轉為正數,並先輸出負號
        cout<<"-";
    }
    while(c>=1)          //c小於1跳出迴圈
    {
        e=c%10;          //e儲存每一位數(由後往前)
        c=c/10;          //c除10遞減
        if(e!=0||n>=1)   //當開頭為非0數才輸出,若輸出過非0數,之後的數皆可輸出
        {
            cout<<e;
            n=n+1;
        }
    }
    cout<<"\n";
}

int main()
{
    int a;
    while(1)                 //重複輸入
    {
        if(cin>>a==false)    //錯誤輸入則結束程式
            return -1;
        else
            aa(a);           //將輸入之a帶入函式aa
    }
    return 0;
}
