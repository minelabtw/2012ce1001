//計算機實習6-2
//965002086_張凱閔
//資工系四年B班
/*
題目 ： 乘乘樂
一開始有一個數字 T，表示共有幾組測試資料。
接下來有 T 個數字 n (0 <= n < 2147483648)。
*請使用自定義函式相乘數字
*/
#include <iostream>
using namespace std;
unsigned int digital_mulitiplication(unsigned int x);

int main()

{
	int i,total_no;
	unsigned int n;

	cin>>total_no;//輸入需計算次數

	for(i=0; i<total_no; i++)//依序讀入數字
	{
		cin>>n;
		
		cout<<digital_mulitiplication (n)<<endl;
	}
	system("pause");
	return 0;

}

//自定義函式相乘數字
unsigned int digital_mulitiplication (unsigned int x)
{
		
		unsigned int temp, total_value;
		if(x!=0)
		//輸入不為0
		{

			total_value = 1;

			while(x != 0)

				{

				temp = x % 10;//以餘數求得最後一位數字

				x /= 10;//將數字除10，準備計算下一位數字

				total_value *= temp;//求數字乘積
			
				}
				return total_value;
		}
			else
			return 0;//輸入為0，直接輸出0

}		
