// A6-2
// Input:integer
// Output:multiply each digits of the input
#include <iostream> /* all the head file may be used. */
#include <stdlib.h>
#include <math.h>
using namespace std;

int mul(int num) /* the function will multiply all the digits of the input */
{
    int i=0,temp=num,result=1; /* all the variable may be used, temp is used to check how many digits is the input. */
    while(temp!=0) /* this while loop can check how many digits is the input, and store the result in i */
    {
        temp=temp/10;
        i+=1;
    }
    while(num!=0) /* this while looop can multiply each digits, store it then return to the main part */
    {
        result=result*(num%10);
        num=num/10;
        i=i-1;
    }
    return result;
}

int main() /* the main part of this program */
{
    int n,T;
    cin >> T;
    for (T;T>0;T--) /* this for loop can let the users repeat this program */
    {
        cin >> n;
        cout << mul(n) << endl; /* call the function "mul" by the input then out put it. */
    }
    return 0;
}
