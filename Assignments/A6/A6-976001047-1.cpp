#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;

int reverse(int);

int main()
{
    int input=0;
    cout <<"input -1 to end it\n";                //this program can continue input , input -1 to stop it
    while ( (cin >>input!=false) && input != -1)
    { 
        reverse(input);  
    }
    
    system ("PAUSE");
    return 0;
}


int reverse(int n)                  //this function is input a number and output a reverse number    
{                                   // ex: input 3245 => output 5423
    int x=0;
    int y=0;
    double z=0;                     
    double total=0;
    
    z=n;                              
    y=int(log10(z));                  
    z=y;
    
    while (n>10)                    //the loop is to make the number reverse   
    {          
        x = n%10;                   
        total =total+x*pow(10,z);   
        n=n/10;
        z=z-1;
    }   
             
    total = total+n;
        
    cout << total <<"\n";           
    return 0;
}

