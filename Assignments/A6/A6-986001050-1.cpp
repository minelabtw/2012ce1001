#include <iostream>
#include <cmath>
using namespace std;

long turn( long );  //function prototype
long number;

int main()
{
    while( cin >> number )
    cout << turn( number ) << endl;     //function call
    return 0;
}
//turn function definition returns "�Ʀr��������"
long turn( long a )
{
    long c=0;
    while( fabs(a) >= 10 )
    {
        int b = a%10;
        c = (c+b)*10;
        a = (a-b)/10;
    }
    c = c+a;
    return c;   //return c that is turned over
}
