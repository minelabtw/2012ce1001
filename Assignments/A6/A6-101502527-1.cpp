//遞迴想法 (不知道這樣是否能當作加分)
//重複輸入
#include<iostream>
using namespace std;
int re(int,int);
int main()
{
    int a,x,t;
    while(cin>>a)//輸入為非數字的時候跳出
    {
        t=0;
        if(a<0)//負數當作正數去算加負號
        {
            a*=-1;
            t=1;
        }
        x=1;
        while(x*10<=a)
            x*=10;
        if(t==1)//負數
            cout<<"-"<<re(a,x)<<endl;
        else//正數
            cout<<re(a,x)<<endl;
    }
    return 0;
}
int re(int b,int c)//各位數開始乘以10的c次方 進入遞迴 越往高位數 c越小 最後總加起來
{
    if(c!=0)
        return b%10*c+re(b/10,c/10);
}
