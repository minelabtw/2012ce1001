#include<iostream>
#include<math.h>
#include<cstdlib>

/*to make a revesing numer */

using namespace std;

/* function */
int reverse(int num);

int main()
{

    int num1;

    while (cin >> num1 != false)
    {
        //call finction and output the reverse result
        cout << reverse(num1)<<endl;
    }

    system("pause");
    return 0;
}


int reverse(int num)
{
    //a=how many numbers, t=doing a times, num0=initial number
    int a,t,num0,result,i=1;
    float temp;
    num0=num;

    //determine how many numbers
    while (num != 0)
    {
        num=num/10;
        i++;
    }
    a=i-1;

    // calculate result
    result=0;
    for(int t=0; t<a; t++)
    {
        temp=((float)num0/pow(10,t));
        temp=(int)temp%10;
        temp=(float)temp*pow(10,a-t-1);
        result=result+(int)temp;
    }

    return result;
}
