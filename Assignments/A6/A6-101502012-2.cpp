#include <iostream>
#include <cmath>
#include <limits>
using namespace std;
int multiplication(int);
int main()
{
    int x,count;
    while(cin >> count==false||count<0)
    {
        cin.clear(); //重置cin為初始值
        cin.ignore(numeric_limits<streamsize>::max(),'\n'); //刪除cin的暫存值
        cout << "Invalid input." << endl;
    }
    while (count>0)
    {
        cin >> x;
        if (cin==false||x<0)
        {
            cout << "Invalid input." << endl;
            cin.clear(); //重置cin為初始值
            cin.ignore(numeric_limits<streamsize>::max(),'\n'); //刪除cin的暫存值
            continue;
        }
        cout << multiplication(x) << endl;
        count--;
    }
    return 0;
}

int multiplication(int y)
{
    int answer=1;

    while (y>0)
    {
        answer=answer*(y%10);
        y=y/10;
    }
    return answer;
}
