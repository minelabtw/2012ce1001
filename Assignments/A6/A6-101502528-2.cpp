#include<iostream>
using namespace std;

int multi(int);

int main()
{
    int time;
    while(cin >> time && cin!=false)
    {
        for (; time>=1 ; time--)
        {
            int a;
            cin >> a;
            cout << multi(a) << endl;
        }
    }

    return 0;
}

int multi (int x) //將數字拆開相乘之函式
{
    int y=1;

    if (x==0)
        y=0;

    else
    for (; x>=1 ; x=x/10) //從個位數開始拆開並相乘
        y=y*(x%10);

    return y;
}
