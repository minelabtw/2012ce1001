#include <iostream>
#include <cmath>
using namespace std;

int Reverse(int);//function prototype

int main()
{
    int a;
    while ( cin >> a )//determine how many time does user want to input
    {
        if ( a > 0 && a <= 2147483648 )//input 0 < a <= 2147483648
            cout << Reverse(a) << endl;
        else //input<=s0
        {
            a = -a;
            cout << -Reverse(a) << endl;
        }
    }
}//end main

int Reverse(int x)//function Reverse definition && x is parameter
{
    int grade=0,t=0,total=0;//variable declaration
    t=x;//avoid that the value of x change
    for(int i=0;t>=1;i++)//determaine how many figures
    {
        t=t/10;
        grade = i;
    }//end loop
    for(;grade>=0;grade--)//calculate
    {
        total += pow(10,grade)*(x%10);
        x=x/10;
    }
    return total;
}//end function  Multiply
