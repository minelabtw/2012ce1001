//計算機實習6-1
//965002086_張凱閔
//資工系四年B班
//題目 ： 數字翻轉
/*
請使用自定義函式，輸入任意數字，函式將其數字全部倒轉 
輸入說明 ：輸入有若干行, 每行包含一個整數，不超過 231 
輸出說明 ：輸出翻轉過後的數字
*/

#include <iostream>

using namespace std;

 long long int m_flip(long long int x)
 {
	long long int output;
	output=0;
	 while(x)
        {        
			output *= 10;//將欲輸出之十進位數左移一位，後面補0	;如輸入數字為0，則不移位。
            output += x % 10;//輸入數字之最左邊一位找出，加到上一列(補加個位數)
            x /= 10;//輸入數字減少之最左一位數移走
        }
	 return output;
}

int main() 
{
  
    long long int input;
	cout<<"請輸入大於0之正整數，輸入 0 則停止運算:"<<endl;
	while(1)
	{	
	cin>>input;
	if (input == 0)
	{
		cout<<"輸入為0，停止運算"<<endl;
		return 0;
		exit(1);
	}
	else
	{
	cout<<m_flip(input)<<endl;
	}
	}
    system("pause");
    return 0;


}
