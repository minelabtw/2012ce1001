//A6-1 by J
//Wrong input detections:no letter input, no none-integer input, no over-flow input
#include <iostream>
using namespace std;
int changer(int);
int main()
{
    int chk ;
    double inp ;

    while ( cin>>inp )
    {
        if ( (chk=inp)!=inp )
            return -1 ;     //check for fractional part

        cout << changer(inp) << endl ;
    }

    return 0 ;
}

int changer(int x)      //main function
{
    int oup=0 , uld=1 , dld=1 ;
    //oup for ouput, uld for up-ladder, dld for down-ladder.

    while ( x/dld!=0 )
    {
        dld*=10 ;
    }

    dld/=10 ;       //2 statements above for dld's start point.ex:123,dld start form 100.

    while ( x!=0 )
    {
        oup+=(x/dld)*uld ;
        x%=dld ;
        uld*=10 ;
        dld/=10 ;
    }       //The situation x!=0 means it's not done!

    return oup ;        //return result!
}
