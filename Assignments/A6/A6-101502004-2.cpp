#include<iostream>

using namespace std;

int multiply (int);//定義一個自定義函數

int main()
{
    int n,x;
    cin >> n;//輸入變數
    for(int i=1;i<=n;i++)//此迴圈一共運行n次
    {
        cin >> x;
        cout << multiply(x) << endl;//輸入變數a進入自定義函數後回傳輸出
    }

    return 0;
}

int multiply(int y)//此函數進行運算
{
    int a,total=1;
    while(y!=0)//此迴圈運行直到所有位數計算完成
    {
        a=y%10;//a為餘數
        total*=a;//將a全部乘起來
        y/=10;//繼續下個位數
    }

    return total;//回傳答案
}
