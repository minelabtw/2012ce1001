# include <iostream>
using namespace std;
int change( int ); //function prototype
int main()
{
    int number;
    while (cin >> number) // used for enter again.
    {
        cout << change( number ) << endl;
    }
    return 0;
}
/////// function to change the order ///////
int change( int x )
{
    int y=0;
    while( x!=0 )
    {
        y=y*10+x%10; //used for reverse the order;
        x/=10;
    }
    return y;
}
/////// function to change the order ///////
