#include <iostream>
#include <cmath>
using namespace std;
int xyz(int);

int xyz(int a)
{
    int x,y,z=0;
    while( a != 0 )
    {
        x = a%10;
        y = z*10+x;
        z = y;
        a = a/10;
    }
    return z;
}
int main()
{
    int b;
    while ( cin >> b )
    {
        cout << xyz( b ) << endl;
    }

    return 0;
}
