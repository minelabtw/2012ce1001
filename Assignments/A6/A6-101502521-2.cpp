#include <iostream>

using namespace std;

int solve(int);         // function to caculate the answer

int main()
{
    int case_num;
    while(cin >> case_num)
    {
        while(case_num--)               //number of the cases
        {
            int n;
            cin >> n;
            if(n==0)
                cout << "0" << endl;
            else
                cout << solve(n) << endl;
        }
    }
    return 0;
}

int solve(int n)
{
    int ans=1;
    while(n>0)
    {
        ans *= (n%10);
        n /= 10;
    }
    return ans;
}
