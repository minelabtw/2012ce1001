#include <iostream>
#include <cmath>//利用絕對值

using namespace std;

int main()
{
   int t,n,x=1,r,buf,sum =1;
    cin >>t;
    for(int i = 1 ; i <= t ; i++ )//t組資料
    {
        cin >>n;
        buf = n;
        if(n < 1&&n > 2147483647)
            return 0;
        while( fabs(n) >= 10)//計算幾位數
        {
            n/=10;
            x++;
        }

        for(int j=1;j<=x;j++)//數字相乘
        {
            r = buf % 10;
            buf = buf / 10;
            sum = sum * r ;
        }
        cout <<sum<<endl;
        sum = 1;
        x = 1;
    }
    return 0;
}
