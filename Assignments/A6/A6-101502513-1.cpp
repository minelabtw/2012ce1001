#include <iostream>
#include <cmath>
using namespace std;

int reverse( int ); //function prototype

int main()
{
    int number;

    while( cin >> number )
    {
        if( number < pow( 2, 31) )
            cout << reverse( number ) << endl; //function call
        else
            break;
    }
}

int reverse( int x )
{
    int sum = 0, a = 0, y = x;

    for( y; y >= 10 || y <= -10; y /= 10 )
        a++; //calculate the power
    for( a; a >= 0; a-- )
    {
        sum += x % 10 * pow( 10, a ); //let number be reverse
        x /= 10;
    }
    return sum;
}
