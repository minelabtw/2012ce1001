#include <iostream>

using namespace std;
long long int multiply(long long int n)
{

    //The initial of ans should be 1, because ans will be used as the multiple base of result.
    long long int ans=1;

    //This while loop is used to multiply each digit. It will stop when n=0.
    while(n)
    {
        ans=ans*(n%10);
        n=n/10;
    }
    return ans;
}

int main()
{
    long long int counter,num;

    //This Varible "counter" is used to count how many data you want to input.
    cin>>counter;

    for(counter;counter>0;counter--)
    {
        cin>>num;
        if(num==0)
            cout<<0<<endl;
        else
            cout<<multiply(num)<<endl;
    }
    return 0;
}
