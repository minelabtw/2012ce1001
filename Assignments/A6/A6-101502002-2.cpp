#include<iostream>
using namespace std;
int penny(int);//令函式
int main()
{
    int a,p;//a為輸入次數，p為輸入值
    cin>>a;
    while(a>0)//讓使用者輸入a次的迴圈
    {
        cin>>p;//
        cout <<penny(p)<<endl;//輸出函式結果
        a=a-1;
    }

    return 0;
}
int penny(int b)//函式
{
    int d,answer=1;//d為末位數，answer為相乘結果
    if(b!=0)//b不等於0時
    {
        while(b>0)//運算相乘結果的迴圈
        {
            d=b%10;
            answer=answer*d;
            b=(b-d)/10;
        }
    }
    else//b等於零
        answer=0;//答案為0
    return answer;//回傳answer
}
