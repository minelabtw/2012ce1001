#include <iostream>

using namespace std;

int multiplayer( int T,unsigned long long n )
{
    unsigned long long rst;
    cin >> T;
    while(T--)
    {
        cin >> n;
        rst=n%10ULL;
        n/=10ULL;
        while(rst!=0 && n)
        {
            rst*=(n%10ULL);
            n/=10ULL;
        }
        cout << rst << endl;
    }
    return 0;
}

int main()
{
    int a;
    unsigned long long b;
    multiplayer( a,b );
    return 0;
}
