#include <iostream>

using namespace std;

int Product(int N)//宣告一個自定義函式，輸入值以及回傳值都是整數
{
    int sum=1 ;

    if (N>0)
    {
        while (N>0)
        {
            sum*=N%10 ;
            N/=10 ;
        }
        return sum ;//回傳sum的值
    }

    else if (N==0) //N==0要另外寫一個回傳0，因為如果進入上面的迴圈去運算，sum的值一開始被我定義為1，
        return 0 ;
}

int main()
{
    int input ;//組數
    int interger ;//一個整數

    if (cin>>input!=false && input>0)//如果輸入的組數是數字而且輸入的數字大於0
        do//先將程式跑一次在決定是否進入while迴圈
        {
            if (cin>>interger!=false&&interger>=0)
                cout << Product(interger) << endl ;//印出在Product函數所回傳的值

            else//如果輸入的不是數字而且如果是數字，輸入的是小於0的數字
                return 0 ;

            input-- ;
        }while(input>0);

    return 0 ;
}
