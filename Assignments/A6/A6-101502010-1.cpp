//若輸入小於1的數或字元則結束程式
#include<iostream>
using namespace std;
int back(int n)//自定義函數
{
    int k=0;
    while(n!=0)
    {
        k=k*10+(n%10);//k為原本的k乘上10再加上n之尾數
        n=n/10;//將n的尾數刪除
    }
    return k;//回傳k值
}
int main()
{
    int n;
    while((cin>>n)!=false&&n>0)//每次迴圈皆須輸入一個整數，若輸入的整數小於一則跳出迴圈
        cout<<back(n)<<endl;//輸出結果

    return 0;//程式結束
}
