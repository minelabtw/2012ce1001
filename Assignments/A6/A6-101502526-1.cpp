#include <iostream>                  /////////////////////////////
#include <cmath>                     //       可計算負數        //
using namespace std;                 /////////////////////////////

int oppsite(int x)
{
    int total = 0;
    int count = 1;
    int a = x;
    for (; x>=10 || x<= -10; count ++ )  // 先計算有幾位數
        x /= 10;
    for (; count > 0;count --)           // 由最大位數來乘逐步遞減
    {
        int site = pow(10,count-1);      // 位數為10的count次方(個、十、百..)
        int mod = a%10 * site;           // 取除10的餘數，也就是最後一個數
        a /= 10;                         // 此數除10
        total = total + mod;             // 總值相加
        site /= 10;                      // 位數少一位(除10)
    }
    return total;                        // 回傳總值

}
int main()                               //此為輸出的函式
{
    int a;
    while (cin >> a)
    {
        cout << oppsite(a) << endl;      // 呼叫oppsite
    }
    return 0;
}
