#include <iostream>

using namespace std;

int plusplus( int );

int main()
{
    int n1,n2=0,n3;
    cin>>n1;
    while (n1>n2)//用來做輸入限制
    {
        while(cin>>n3)
        {
            cout<<plusplus( n3 )<<endl;
            n2++;
        }
    }
    return 0;
}
int plusplus( int x )
{
    int y,z=1;
    while (x>=1)
    {
        y=x%10;//分離每個位的數字
        x=x/10;
        z=z*y;//把每位的數相乘
    }
    return z;
}
