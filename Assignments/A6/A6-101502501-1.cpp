#include <iostream>
#include <cmath>

using namespace std;

int number( int );//自訂函式

int main()
{
    while (1)//重複輸入
    {
        int a;

        if( cin >> a == false || a > pow( 2,31 ) )//輸入不可為字元及大於2^31的數
        {
            cout <<"You need to enter a number which smaller than 2^31." << endl;
            return -1;
        }

        if ( a > 0 )//輸入為正數
            cout << number ( a ) << endl;

        else if ( a < 0 )//輸入為負數
        {
            a = -a;
            cout << "-" << number ( a ) << endl;
        }
    }
    return 0;
}

int number ( int x )
{
    int n = 0, b = 1, c = 0;

    while (  b > 0 )
    {
        b = x / 10;//讓迴圈能夠跑幾次
        n = x % 10;//計算餘數
        c = 10 * c + n;//利用餘數乘10再加上餘數來反轉
        x = x / 10;//讓數字少一位
    }
    return c;
}
