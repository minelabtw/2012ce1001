#include<iostream>
#include <cmath>
using namespace std;

int abc ( int, int ); //abc()是自定義函式

int main()
{
    int n, a;//n為使用者輸入數字 a為相反後一個一個輸出之數字
    abc( n, a );//呼叫abc()函式
}

int abc ( int n, int a )//abc()函式
{
    while( n < pow(2,31) )//重複輸入且限制n不大於2的31次方
    {
        cin >> n;

        if( n < 0 )//負數處理
        {
            n = -n;
            cout << "-";
        }

        while( n != 0 && n % 10 == 0 )//若後面有許多零 先除掉
        {
            n = n / 10;
        }
        while( n != 0 && n % 10 >= 0 )//上一步處理完後面的0後 相反之
        {
            a = n % 10;
            n = n / 10;
            cout << a;//一一輸出相反後的數字
        }

        cout << "\n"//換行
    }

    return 0;

}
