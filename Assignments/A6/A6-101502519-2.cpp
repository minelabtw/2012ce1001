#include <iostream>

using namespace std;

int aa(int c)
{
    int e,d=1;
    if(c==0)                  //輸入為0的情況
        d=0;
    else
    {
        while(c>=1)           //c小於1跳出迴圈
        {
            e=c%10;           //e儲存每一位數(由後往前)
            c=c/10;           //c除10遞減
            d=d*e;            //將每一位數相乘
        }
    }
    return d;                 //把值帶回函式
}

int main()
{
    int a,n,m=0;

    if(cin>>n==false)         //錯誤輸入偵測
        return -1;
    else
    {
        while(m<n)            //次數內執行
        {
            if( cin>>a==false || a<0 )   //錯誤輸入偵測
                return -1;
            else
            {
                aa(a);                   //將輸入之a帶入函式aa
                cout<<aa(a)<<"\n";       //將函式之值輸出
                m=m+1;                   //計算次數
            }
        }
    }
    return 0;
}
