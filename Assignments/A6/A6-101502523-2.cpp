//輸入負數或字元時結束程式
#include <iostream>
using namespace std;

int multiply( int ); // function prototype

int main()
{
    int n;

    if( cin >> n && n != false ) // 排除負數,字元
    {
        int input;

        for( int i = 0; i < n; i++ ) // n組測試資料
        {
            cin >> input;
            if( !cin )
                break;
            cout << multiply( input ) << endl;
        }
    }
    return 0;
} // end main

int multiply( int x ) // 乘乘樂函數
{
    int a=1;

    if( x == 0 ) // 排除輸入為0依然執行迴圈
        return 0;

    while( x ) // 乘乘樂迴圈
    {
        a = a * ( x % 10 ); // 例如 a = 1 * ( 123 % 10 ) = 1 * 3
        x /= 10; // 運算下一位數
    }
    return a;
} // end function multiply
