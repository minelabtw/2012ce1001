#include<iostream>
using namespace std;

int calculate(int);//自訂一個計算把位數全部相乘的函式

int main()
{
    int num;
    int i;

    cin >> i;

    if (i > 0)
    {
        while(i >= 1)
        {
            cin >> num;
            cout << calculate(num) << endl;

            i--;
        }

    }

    else
        return 0;

    return 0;
}

int calculate(int a)
{
    int sum = 1;
    int j;

    if (a == 0)//當輸入值等於零時,輸出值也要是零
        sum = sum - 1;

    else
    {
        while(a>0)
        {
            j = a % 10;
            sum = sum * j;
            a /= 10;
        }

    }

    return sum;
}
