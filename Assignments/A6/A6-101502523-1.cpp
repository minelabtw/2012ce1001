#include <iostream>
#include <cmath>
using namespace std;

int overturn( int ); // function prototype

int main()
{
    int input;
    while( cin >> input )
        cout << overturn( input ) << endl;
    return 0;
} // end main

int overturn( int x )
{
    int a=0;

    while( x % 10 == 0 ) // 先讓數字的0消除 例如 201000 → 201
        x /= 10;         // 為了倒轉之後的數字無意義0消除 (000102)

    while( x > 0 ) // 正數的倒轉
    {
        int i = 0;

        while( pow(10,i) <= x ) //10的i次方大於 x 時結束loop,例如輸入為123 → i=3, pow(10,i)= 1000 > x
            i++;
        a = a + ( x % 10 ) * pow(10,i-1); // 例如輸入 123 → a = 0 + 3 * 100 = 300
        x = x / 10;                       // x = 12, 繼續執行迴圈
    }
    while( x < 0 ) // 負數的倒轉
    {
        int i = 0;

        while( pow(10,i) <= x * (-1) ) //先使 x 為正,10的i次方大於 x 時結束loop
            i++;
        a = a + ( x % 10 ) * pow(10,i-1); // 例如輸入 -123 → a = 0 + -3 * 100 = -300
        x = x / 10;                       // x = -12, 繼續執行迴圈
    }
    return a;
} // end function overturn
