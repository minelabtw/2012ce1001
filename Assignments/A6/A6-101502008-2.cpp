#include <iostream>

using namespace std;
int transform(int x)//應題目要求用函式寫
{
    int output=1,i=1;
    do//提出各位數並乘積
    {
        i=x%10;
        output=output*i;
        x/=10;
    }while(x!=0);
    return output;//輸出
}
int main()
{
    int times,input;
    cin>>times;//輸入執行幾次
    for(int count=1;times>=count;count++)//count為好用的計數器
    {
        cin>>input;
        cout<<transform(input)<<"\n";
    }
    return 0;
}
