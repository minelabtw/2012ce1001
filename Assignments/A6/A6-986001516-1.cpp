// A6-1
// Input:integer
// Output:reverse the input
#include <iostream> /* all the head file may be used. */
#include <stdlib.h>
#include <math.h>
using namespace std;

int reverse(int temp) /* the function that can do the reversing */
{
    int i=0,num=temp,result=0; /* all the variable may be used, temp is used to check how many digits is the input. */
    while(temp!=0) /* this while loop can check how many digits is the input, and store the result in i */
    {
        temp=temp/10;
        i+=1;
    }
    while(num!=0) /* use this while loop and i to do the reversing and store it in the variable "result" then return to the main part of tihs program. */
    {
        result=result+(num%10)*pow(10,i-1);
        num=num/10;
        i=i-1;
    }
    return result;
}

int main() /* the main part of this program */
{
    int num;
    cout << "If you type zero, you can end this program." << endl; /* first show this message to tell the users exit this program by typing 0 */
    for (int i=0;i>=0;i++) /* this for loop can let the users repeat this program */
    {
        cin >> num;
        if (num==0) /* when the user type 0, break this for loop then end the program */
        break;
        cout << reverse(num) << endl; /* call the function "reverse" by the input then out put it. */
    }
    return 0;
}
