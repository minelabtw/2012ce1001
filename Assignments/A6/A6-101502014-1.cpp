#include<iostream>
using namespace std;
int reverse(int);
int main()
{
    int n;
    while(cin >> n)
        cout << reverse(n) << endl;
	return 0;
}
int reverse(int num) //reverse函式用來翻轉數字
{
    int ans = 0; //ans紀錄數字翻轉後的結束
    while(num > 0) //判斷num的每一位數有沒有抓出來
    {
        ans *= 10; //ans前進一位數
        ans += num % 10; //把num的每一位數加到ans裡
        num /= 10; //num後退一位數
    }
    return ans; //回傳數字翻轉後的答案
}
