#include <iostream>
#include <math.h>

using namespace std;

int mutiply(int x);

int main()
{
    int x;
    int n,j=1;//n表示可以輸入的次數,j用來控制跳出迴圈

    cin >> n;

    while ( j <= n)
    {
        cin >> x;
        cout <<  mutiply( x ) << endl;
        j++;
    }

    return 0;
}

int mutiply(int x)
{
    int i=1;//i表執行迴圈次數,即位數
    int a;//用來儲存x的初始值
    int sum=1;//表最終結果


    a = x;//宣告a之值為x的輸入值

    while ( x != 0 )
    {
        x /= 10;
        i++;
    }

    while ( a != 0 )
    {
        sum *= a%10;//每一次乘10的餘數
        a /= 10;
        i--;
    }

    return sum;
}
