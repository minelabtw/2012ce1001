#include <iostream>
#include <cmath>
#include <stdio.h>
using namespace std;

long long int flip(long long int n)
{
    long long int temp=n,counter=0,sum=0;

    //This while loop is used to count how many digits the input has. It will stop when temp=0.
    while(temp)
    {
       temp=temp/10;
       counter++;
    }

    //This for loop is used to add each digit with it's power.
    for(counter;counter>=0;counter--)
    {
        sum=sum+(n%10)*pow(10,counter-1);
        n=n/10;
    }
    return sum;
}

int main()
{
    long long int num;
    while(cin>>num && (cin.get())!=EOF)//EOF to exit
        cout<<flip(num)<<endl;
    return 0;
}
