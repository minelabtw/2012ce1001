#include<iostream>
#include<math.h>
#include<cstdlib>
/* multiply numbers */
using namespace std;

/* function */
int multiply(int num);

int main()
{
    //determine how many numbers and whilch numbers to be input
    int count,number;
    cin >> count;

    //inputting count times numers
    for(int i=0; i<count; i++)
    {
        cin >> number ;
        // call function and output result
        cout << multiply(number) <<endl;
    }


    system("pause");
    return 0;
}


int multiply(int num)
{
    // initialize the result is 1
    int result=1;

    while(num != 0)
    {
       result=result*(num%10);
       num=num/10;
    }

    return result;
}
