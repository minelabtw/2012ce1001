#include <iostream>
using namespace std;
long long Reverse(long long a)
{
    if (a==0)//若輸入值等於0
        return 0;//直接回傳0
    long long b=1,d=0;
    for(long long c=a;c>0;c=c/10)//計算位數
        d++;
    long long e[d];//宣告陣列，以位數當長度
    for(long long c=1;c<=d;c++,a=a/10)//將每一格填入每一位數
        e[c-1]=a%10;
    for(long long c=0;c<=d-1;c++)//全部相乘
        b=b*e[c];
    return b;//回傳
}
int main()
{
    long long a,b;
    cin>>b;
    for(;b>=1;b--)//計算回圈次數
    {
        cin>>a;
        cout<<Reverse(a)<<endl;//呼叫函式
    }
    return 0;
}
