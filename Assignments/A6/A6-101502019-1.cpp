#include <iostream>

using namespace std;

int rvs (int);

int main()
{
    int num;

    while (cin >> num)
    {
        cout << rvs (num) << endl;//執行自定義函式
    }

    return 0;
}

int rvs (int num)
{
    int n,buff = 0;

    while (num != 0)
    {
        n = num % 10;
        buff = buff * 10 + n;//使用buff將先得到的餘數累加起來
        num = num / 10;
    }

    return buff;
}
