#include<iostream>
using namespace std;

int mul(int x);

int main()
{
    int t,n,count=0;
    cin>> t;

    while(count<t)
    {
        cin>> n;
        cout<<mul(n)<<endl;
        count++;
    }
}

int mul(int x)
{
    int sum=1;

    while(x>0)
    {
        sum=sum*(x%10);
        x=x/10;
    }
    return sum;
}

