#include<iostream>
#include<cmath>
bool prine(int);
using namespace std;
int main()
{
    int a;
    cout<<"Enter an positive integer : ";
    if(cin>>a==false||a<=0)//判斷錯誤
    {
        if(a<=0)
            cout<<"Input can't be negative or zero."<<endl;
        else
            cout<<"Invalid input."<<endl;
        return -1;
    }
    int x;
    cout<<"positive prime divisors of "<<a<<" : ";
    for(x=2;a!=1;x++)//迴圈跑範圍內的數 當質因數分解完則跳出
    {
        if(a%x==0&&prine(x))//如果可以被整除 且為質數者輸出
        {
            cout<<x<<" ";
            for(;a%x==0&&a!=1;a/=x);//以找過的質因樹數剃除 減少判斷!!!!!!!
        }
    }
    return 0;
}
bool prine(int b)//判斷是否為質數
{
    for(int x=2;x<=sqrt((double)b);x++)
    {
        if(b%x==0)
            return false;
    }
    return true;
}
