//bug:只要是20的倍數就不會輸出5
#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int num;
    bool x = true;

    cout << "Enter an positive interger : ";

    if (cin >> num == false)//check num is number or not
    {
        cout << "Invalid input.";
        return -1;
    }

    if (num > 0)//check num is a negative number or not
    {
        cout << "positive prime divisors of " << num << " : ";

        for (int d = 2;d <= num;d++)//to find the factors of num
        {
            if (num % d != 0)
            continue;

            else
            {
                for (int d2 = 2;d2 <= sqrt( (float) d);d2++)//check the factors are prime numbers or not
                {
                    if (d % d2 == 0)
                    {
                        x = false;
                        break;
                    }
                }
                if (x)
                    cout << d << " ";
            }
        }
    }

    else
    {
        cout << "Input can't be negative or zero.";
        return -1;
    }

    return 0;
}
