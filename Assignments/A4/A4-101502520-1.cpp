#include <iostream>
#include <math.h>
using namespace std;

inline void Show_Prime_Divisors(int);

int main()
{
    int num,trash;//trash用來存取cin的非整數部分
    cout<<"Enter an positive integer : ";
    if(cin>>num==false)
    {
        cout<<"Invalid input.";
        return -1;
    }

    trash=cin.get();
    if(trash!=10)//檢查輸入數字之後是不是換行(ASCII 10)
    {
        cout<<"Invalid input.";
        return-1;
    }

    if(num<=0)
    {
        cout<<"Input can't be negative or zero.";
        return -1;
    }

    cout<<"positive prime divisors of "<<num<<" : ";
    if(num==1)//檢查輸入是否是1，如果是1就沒有質因數
        cout<<"None.";
    else//符合輸入條件之後開始檢查質因數
        Show_Prime_Divisors(num);

    return 0;
}

void Show_Prime_Divisors(int traget)
{
    for(int i=2;i<=traget;i++)
    {//首先找出小於輸入數字的所有質因數
        bool flag1=true;//先假設i是質數
        for(int j=2;j<=sqrt(i);j++)
            if(i%j==0)
            {
                flag1=false;//如果發現有人可以整除i，就標記不是質數
                break;
            }
        if(flag1)
            if(traget%i==0)//如果是質數就拿去除，如果可以整除就輸出
                cout<<i<<" ";
    }
    return;
}
