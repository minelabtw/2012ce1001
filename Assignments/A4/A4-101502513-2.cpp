#include <iostream>
#include<cmath> //standard C++ math library
using namespace std;

int main()
{
    int Decimal,Binary = 0,x = 0;
    cout << "Enter a positive number: ";

    if ( cin >> Decimal == false || Decimal < 0 )
    {
        if ( Decimal < 0 )
            cout << "Input can't be negative." << endl; // input must be positive
        else
            cout << "Invalid input." << endl; // input can't be character
        return 0;
    }

    cout << "Binary of " << Decimal << " : ";
    for ( Decimal ; Decimal >= 1 ; x++ )
    {
        Binary += Decimal % 2 * pow( 10 , x ); //transform positive into binary
        Decimal = Decimal / 2; //decimal divide 2 every time in the loop
    }
    cout << Binary << endl;
    return 0;
}
