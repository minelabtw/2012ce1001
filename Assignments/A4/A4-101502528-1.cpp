#include <iostream>
using namespace std;

int main()
{
    int a,b,c,test;

    cout << "Enter an positive integer : ";
    cin >> a;

    if ( cin == false || a <= 0 ) //偵測輸入字元或負數的錯誤
    {
        if ( a <= 0 ) //若輸入負數則輸出以下錯誤訊息
            cout << "Input can't be negative or zero." << endl;
        else //若輸入字元則輸出以下錯誤訊息
            cout << "Invalid input." << endl;
        return -1;
    }

    cout << "positive prime divisors of " << a << " : ";

    for ( b = 2 ; b <= a ; b++ ) //判斷a的所有因數
    {
        if ( a % b == 0 )
        {
            for ( c = 2 ; c <= b ; c++ ) //判斷a的所有因數是否為質數
            {
                test = 0;
                if ( b == 2 ) //判斷是否有2這個質因數
                {
                    test = 0; //因數為質數
                    break;
                }
                else if ( c < b && b % c ==0 )
                {
                    test = 1; //因數非質數
                    break;
                }
            }
            if ( test == 0 ) //輸出所有質因數
                cout << b << " ";
        }

    }

    return 0;
}
