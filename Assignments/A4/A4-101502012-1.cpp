#include <iostream>
using namespace std;

int main()
{
    int integer,x=2,y=2;

    cout << "Enter an positive integer : ";
    cin >> integer;

    if (cin==false) //錯誤偵測:輸入非字元
        cout << "Invalid input.";
    else if (integer<=0) //錯誤偵測:輸入不能小於等於0
        cout << "Input can't be negative or zero.";
    else
        {
        if (0==integer%2) //如果integer有2的質因數 直接顯示(修復下面無法顯示2質因數)
            cout << "positive prime divisors of " << integer << " : 2 ";
        else if (0!=integer%2)
            cout << "positive prime divisors of " << integer << " : ";
        while (x<=integer)  //x用來檢測整部整除integer
            {
            if (0==integer%x) //確定x整除integer
                while (y<x)
                {
                    if (0==x%y) //y用來檢測x是不是質因數
                        break;
                    else if ((y==x-1)&&(0!=x%y)) // y=x-1可確定迴圈跑完了 跑完就代表x沒有數可以整除他 x也不可能被x-1整除
                        cout << x << " ";
                    y++;
                }
            y=2;
            x++;
            }
        }
    return 0;
} //end main
