#include<iostream>
using namespace std;
int main()
{
    int a,b,c=1,answer=0;//設a為輸入數、b為被除數、c為10的指數
    cout<<"Enter a positive integer : ";//輸出文字
    if(cin>>a)//若輸入為整數則繼續
    {
        if(a<0)//若輸入小於零的數
            cout<<"Input can't be negative."<<endl;//輸出文字換行並結束
        else
        {
            cout<<"Binary of "<<a<<" : ";
            {
                while(a>0)
                {
                    b=a%2;//b為除以二的餘數
                    answer=answer+b*c;//將b一位一位排好
                    c=c*10;//迴圈改變值
                    a=a/2;
                }
                cout<<answer<<endl;//輸出結果
            }
        }
    }
    else//若為非法輸入
        cout<<"Invalid input."<<endl;//輸出文字並結束
    return 0;
}

