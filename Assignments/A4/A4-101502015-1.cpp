#include<iostream>
using namespace std;
int main()
{
    int number,i=2,j,remainer1,remainer2;
    cout<<"Enter an positive integer : ";
    cin>>number;
    if(cin==false)//偵錯若為字元則輸出
       {
            cout<<"Invalid input."<<endl;
            return -1;
        }
    else if(number<=0)//偵錯若為負數則輸出
        {
            cout<<"Input can't be negative or zero."<<endl;
            return -1;
        }
    else
    {
        cout<<"positive prime divisors of "<<number<<" is : ";
        while(number>i)//先判斷非質數之數
        {
            remainer1=number%i;
            if(remainer1==0)//若餘數等於零代表輸入之數有因數
            {
                j=2;
                while(i>j)//判斷輸出數之因數是否為質數
                {
                    remainer2=i%j;
                    if(remainer2==0)//若非質數則捨去
                        break;
                    j=j+1;
                }
                if(i==j)//輸出質因數
                    cout<<i<<" ";
            }
            i=i+1;
        }
        j=2;
        while(number>j)//輸入判斷質數
        {
            remainer2=number%j;
            if(remainer2==0)
                break;
            j=j+1;
        }
        if(number==j)//若為質數則輸出
            cout<<number<<endl;
    }
    return 0;
}
