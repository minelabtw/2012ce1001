#include <iostream>

using namespace std;

int main()
{
    int num,ans;

    cout << "Enter apositive interger : ";

    if (cin >> num == false)//check whether num is a number or not
    {
        cout << "Invalid input.";
        return -1;
    }

    if (num < 0)//check whether num is a negative number or not
    {
        cout << "Input can't be negative.";
        return -1;
    }

    cout << "Binary of " << num << " : ";

    while (num > 0)
    {
        ans = num % 2;
        num = num / 2;
        cout << ans;
    }

    return 0;
}
