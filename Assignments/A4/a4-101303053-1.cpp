
#include <iostream>

using namespace std;

int main(){
   int i, x, first = 0;
   cout << "Enter an positive integer :";
   cin >> x;

   if( cin >> x == false )
    {
        cout << "Invalid input." << endl;
        return -1;
    }

    if( x < 0 )
    {
        cout << "Input can't be negative." << endl;
        return -1;
    }

   for( i = 2; x > 1; i++ ) {
      if( x % i == 0 ) {
         if( first == 0 ) {
            cout << i;
            first = 1;
         } else
            cout << "�B" << i;
         while( x % i == 0 )
            x /= i;
      }
   }
   return 0;
}
