#include <iostream>
#include <cmath>
#include <limits>
#include <cstdlib>

using namespace std;

int main()
{
    int num;
    bool prime=true;
    cout << "Enter an positive integer : ";
    cin >>num;

    while(num != (-1))    //可以重複輸入，但輸入-1的話結束程式
    {
           cin.clear();   //清除cin緩衝區的資料
           cin.ignore(numeric_limits<int>::max(), '\n');
           if(cin == false|| cin.gcount() != 1 )     //如果輸入為字元，告知為錯誤訊息
               cout <<"Invalid input.";

           else if(num<=0)                           //如果輸入為負數或0，告知輸入必須為正數
               cout <<"Input can't be negative or zero.";

           else if(num>0)
           {
               cout <<"positive prime divisors of "<<num<<" : ";
               for(int i=1;i<=num;i++)
               {
                      if(num%i==0)                   //找出num的正因數
                      {
                           for(int j=2;j<=sqrt(i);j++)  //判斷這些正因數，是否為質數
                           {
                                if(i%j==0)
                                {
                                   prime=false;
                                   break;
                                }
                           }
                           if(prime==true && i>1)      //扣掉1不是質數，並印出質因數
                                cout <<i<<" ";
                           else
		                        prime=true;
                      }
               }
            }
            cout <<endl<< "Enter an positive integer : ";
            cin >>num;
    }

    system("pause");
    return 0;

}


