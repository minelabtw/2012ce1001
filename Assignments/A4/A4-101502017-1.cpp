#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int x,div;
    bool c;
    cout << "Enter an positive integer : ";

    while(cin>>x)
    {
        if(x<=0)
        {
            cout <<"Input can't be negative or zero.";
            return 0;
        }

        else if(x==1)
        {
            cout <<"1 have not a positive prime divisor.";
            return 0;
        }

        else
        {
            c = true;
            cout <<"positive prime divisors of "<<x<<" : ";
            for(div=2;x>div;div++)//The frist loop to decide the divider
            {
                if(x%div==0)
                {
                    for(int i=2;i<=sqrt((float)div);i++)//The second loop to decide the positive prime number
                        if(div%i==0)
                        {
                            c = false;
                            break;
                        }
                    if(c)
                      cout<<div<<" ";
                }
            }

        }
        return 0;
    }
    cout <<"Invalid input.";
    return 0;
}
