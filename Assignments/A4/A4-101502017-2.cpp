#include <iostream>

using namespace std;

int main()
{
    int dec,bin;
    cout << "Enter a positive integer : " ;

    if( cin >> dec == false || dec < 0 )//除錯偵測
    {
        if( dec < 0 )
            cout <<"Input can't be negative.";
        else
            cout <<"Invalid input.";
        return 0;
    }
    cout<<"Binary of "<<dec<<" :";//十進位轉二進位
    while( dec > 0 )
    {
            bin = dec % 2;
            dec = dec / 2;
            cout <<bin;
    }
    return 0;
}
