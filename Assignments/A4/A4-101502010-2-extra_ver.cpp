//加分版會有overflow的問題，僅能處理1024以下之正整數
#include<iostream>
using namespace std;
int main()
{
    int input,log=0,i=1,sum=0;//宣告變數
    cout<<"Enter an positive integer : ";
    if(cin>>input==false||input<=0)//錯誤偵測
    {
        if(input<=0)//若為負數之錯誤輸出
            cout<<"Input can't be negative.\n";
        else
            cout<<"Inalid input.\n";//若為字元之錯誤輸出
        return -1;
    }
    else
    {
        while(log==0)//尋找最相近之2的指數
        {
            if(i<input && input<=i*2)
                log=i;//若 input 介於 i 與 2倍 i 之間，則將 i 紀錄
            else
                i*=2;//否則將 i 加倍
        }
        i=input;//將input初始值紀錄
        while(log!=0)//當指數不為0時進入迴圈(因log為int型態，所以當log小於1時為0)
        {
            if((i-log)<0)//若變數 i 比當前指數小時
                sum=sum*10;//該位數為0
            else
            {
                sum=sum*10+1;//若變數 i 比當前指數大時，該位數為1
                i-=log;//將 i 減去現在指數值
            }
            log/=2;//將指數除2進行下一次運算
        }
        cout<<"Binary of "<<input<<" : "<<sum<<endl;//輸出結果
    }
    return 0;
}
