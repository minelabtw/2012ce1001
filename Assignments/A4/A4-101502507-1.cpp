//Enter a positive integer and output the positive prime divisors of it.
#include <iostream>
#include<cmath>

using namespace std;

int main()
{
    int num = 1;
    int i = 2;//the integers between 0 and the number which entered by user
    int j;//the integers between 0 and the sqrt of i, to check whether i is prime divisor or not
    int end;//sqrt of i
    bool isPrimeFlag ;//isPrimeFlag is used for record whether this number is a prime number

    cout << "Enter an positive integer : ";

    if( cin >> num == false || num <= 0)//check the number entered by user is positive and not invalid input
    {
        if( num < 0 )
        cout << "Input can't be negative or zero." << endl;
        else
        cout << "Invalid input." << endl;
        return -1;
    }//if end

    if( num == 1 )//1 doesn't have positive prime divisor
    {
        cout << "1 doesn't have positive prime divisor!!!" << endl;
        return 0;
    }//if end

    cout << "positive prime divisors of " << num << " : ";

    while( i <= num )
    {
        isPrimeFlag = true;

        if( num % i != 0 )//i is not num's divisor
        {
            isPrimeFlag = false;

        }//if end
        else for( j = 2 , end = sqrt(i) ; j <= end ; j++ )//i is num's divisor and check whether i is prime or not
        {
            if( i % j == 0 )//j is i's divisor
            {
                isPrimeFlag = false;
            }//if end
        }//for end

        if( isPrimeFlag )//i is num's prime divisors
        cout << i << " ";//output all the prime divisors of num
        i++;
    }//while end

    return 0;

}//main end








