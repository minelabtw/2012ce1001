#include<iostream>

using namespace std ;

int main(){
    
    
    int de;
    int bi= 0;
    int s = 1;
    int sum=0;
    cout << "Enter a positive integer : ";
    
    if( cin >> de == false )  // 輸入字元 停止程式 
    {                          
      cout << "Invalid input." << endl; 
      return -1;
    }

    if( de < 0 )            // 輸入負數  停止程式 
    {
      cout << "Input can't be negative." << endl;
      return -1;
    }
    
    cout << "Binary of " << de ; 
    
    while(de!=0)            //   
    {
      sum = sum+(de%2*s);   //把除以2的餘數 乘上原來的位數 存入sum 
      de /= 2;
      s*=10;
    }
    
    cout << " : " << sum << " (normal expression)" << endl;  

   return 0;
} 
