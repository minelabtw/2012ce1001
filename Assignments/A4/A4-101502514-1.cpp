#include <iostream>
using namespace std;
int main()

{
    int i,k,h;//宣告整數型態變數
    cout << "Enter an positive integer : ";
    if(( cin >> i == false ) || ( i <= 0 ))//如果輸入的i不符合整數型態或是小於等於0
    {
        if(i<=0)
            cout << "Input can't be negative or zero." ;//小於等於零輸出Input can't be negative or zero.
        else
            cout << "Invalid input.";//型態錯誤則輸出Invalid input.
        return -1;//回傳-1結束
    }

    cout  << "positive prime divisors of " << i << " : ";
    for(int j=2;i>=j;j++)//當i>=2時進入迴圈
        {
            if(i%j==0)//判斷是不是i的因數
            {
                h=0;
                for(int k=2;k<j;k++)//判斷i的因數j是不是質數
                {
                    if(j%k==0)//如果不是質數，讓h不等於0
                    {
                        h=j;
                    }
                }
                if(h==0)//如果h=0，表示j是質數。
                {
                    cout<<j<<" ";//印出j
                }
            }
        }
    return 0;//回傳
}
