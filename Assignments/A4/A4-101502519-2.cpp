#include <iostream>
                                           //此為正常表示法
using namespace std;

int main()
{
    int a,n=2;

    cout<<"Enter an positive integer : ";

    if(cin>>a==false||a<0)                //判斷是否為正確輸入值
    {
        if(a<0)                           //小於零的情況
            cout<<"Input can't be negative.";
        else                              //錯誤輸入的情況
            cout<<"Invalid input.";
    }

    else
    {
        while(n<=a)                       //找到小於a的最大二次方為n/2
            n=n*2;

        cout<<"Binary of "<<a<<" : ";

        while(n>=2)                       //因二的次方(n/2)最小為1，故只須算到n>=2
        {
            if(a>=n/2)                    //若a>=n/2此二次方,輸出1並減n/2，下次迴圈即可再判斷
            {
                cout<<"1";
                a=a-n/2;
            }

            else                          //若a較n/2小，則a不變，並輸出0
                cout<<"0";

            n=n/2;                        //將n/2，此迴圈即會將a與小於a第二大的二次方再比較大小
        }
    }
    return 0;
}
