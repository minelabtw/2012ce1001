#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    int a , b = 2;
    bool isPrimeFlag;//isPrimeFlag用來記錄是否為質數

    cout<< "Enter an positive integer : ";

    if( cin >> a == false || a <= 0 )//輸入一個數值a,a<=0或a為字元
    {
        if( a <= 0 )//若a<=0則顯示"Input can't be negative or zero."

            cout<< "Input can't be negative or zero." << endl;

        else//若a為字元則顯示"Invalid input."

            cout << "Invalid input." << endl;
        return 0;
    }

    cout << "positive prime divisors of " << a << " : ";

    while( b <= a )//若b<=a則進入迴圈
    {
        isPrimeFlag = true;

        for( int i = 2 ; i <= sqrt(b) ; i++ )//檢驗b是否為質數
        {
            if( b % i == 0 )
            {
                isPrimeFlag = false;
                break;
            }
        }
        if( a % b == 0 )//若b為a的質因數,則顯示b
        {
            if( isPrimeFlag )
            {
                cout << b << " ";
            }
        }
        b++;
    }
    return 0;
}
