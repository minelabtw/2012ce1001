/*  重複輸入
    以正常型態輸出二進位數
    輸入 0 ∼1048575 的整數
    輸入 -1 結束程式
    輸入大於 1048575 的整數會擋掉，以防止之後運算溢位
    若任何錯誤輸入，程式皆會擋掉(ex: -abc123, 567def, 10.675, -123, 大於1048575 的整數)
*/
#include<iostream>
#include<string>
#include<cstdlib>
using namespace std;

class CDecToBin//定義類別：十進位轉二進位
{
    public:
        void DecToBin();//十進位轉二進位函式
    private:
        int iPositiveInteger;//int變數，儲存輸入整數
        void Input();//輸入函式
        bool CheckInput(string str);//輸入檢查函式
};
void CDecToBin::DecToBin()//十進位轉二進位函式
{
   Input();//呼叫輸入函式
   cout<<"Binary of "<<iPositiveInteger<<" : ";
   unsigned long long int iBinary=0;//無號長長整數宣告，用於存轉換的二進位數
   long long int b=1;//長長整數宣告，用於存 10的次方數
   while(iPositiveInteger!=0)//跳出迴圈時機為十進位數變為零
   {
        iBinary+=(iPositiveInteger%2)*b;
        iPositiveInteger/=2;
        b*=10;
   }
   cout<<iBinary<<endl;
}
void CDecToBin::Input()//輸入函式
{
    string strInput;
    do
    {
        cout<<"Enter a positive integer : ";
        cin>>strInput;
    }while(CheckInput(strInput));//作輸入檢查判斷
}
bool CDecToBin::CheckInput(string str)//輸入檢查函式
{
    if(str=="-1")//若輸入為-1，結束程式
    {
        exit(0);
    }
    if(str.c_str()[0]!=45)//判斷第一字元是否為"-"
    {
        for(int i=0;i<str.length();i++)//逐字檢查是否為數字
        {
            if(str.c_str()[i]<'0'||str.c_str()[i]>'9')
            {
                cout<<"Invalid Input."<<endl;
                return true;
            }
        }
        iPositiveInteger=strtol(str.c_str(),NULL,10);//將數字字串轉換成整數儲存
        if(iPositiveInteger>1048575)//輸入數字必須小於等於1048575，否則之後運算會溢位
        {
            cout<<"Input must be in the range of 0 ∼1048575."<<endl;
            return true;
        }
        return false;
    }
    else
    {
        for(int j=1;j<str.length();j++)//第一字元後逐字檢查，是否為數字
        {
            if(str.c_str()[j]<'0'||str.c_str()[j]>'9')
            {
                cout<<"Invalid Input."<<endl;
                return true;
            }
        }
        cout<<"Input can't be negative."<<endl;//輸入不可為負數
        return true;
    }
}
int main()
{
    CDecToBin DTB;//命名十進位轉二進位類別
    while(1)
    {
        DTB.DecToBin();//呼叫類別中十進位轉二進位函式
    }
    return 0;
}
