//101502504
#include <iostream>//allows program to output data to the screen
using namespace std;//program uses

int main()//function main begins program exection
{
    int a,b=1,c=0,binary=0;//variable declarations
    cout << "Enter a positive integer : ";//prompt user to data

    if ( cin >> a == false || a < 0 )
    {
        if ( a < 0 )
            cout << "Input can't be negative." << endl;
        else//if a is not a number
            cout << "Invalid input." << endl;
            return -1;
    }//end if

    cout << "Binary of " << a << " : ";//show the answer
    for ( a > 0; a >= 1 ; a /= 2 )
    {
        c = ( a % 2 )*b;//c=the modulus of a/2
        binary += c;
        b *= 10;
    }//end loop
    cout << binary << endl;//display binary of input "a"

    return 0;//indicate program ended successfully
}//end function main
