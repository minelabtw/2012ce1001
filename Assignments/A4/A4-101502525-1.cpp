//enter -1 to exit

#include<iostream>
#include<stdlib.h>
#include<math.h>
#include<limits>
using namespace std;


bool inputcheck(int in);
bool isPrime(int integer);


int main()
{
    //initial
    int integer=0;


    while(integer!=-1)
    {
        //input
        do
        {
            cout<<"Enter an positive integer : ";
            cin>>integer;
        }while(!inputcheck(integer));


        //exit
        if(integer==-1)
            break;


        //compute & output
        cout<<"positive prime divisors of "<<integer<<" : ";

        if(integer%2==0)//if 2 is a divisor
            cout<<2<<' ';
        for(int i=3;i<=integer;i+=2)
            if(integer%i==0&&isPrime(i))//if i is divisible & prime
                cout<<i<<' ';

        cout<<"\n\n";
        //compute & output  above
    }


    //end
    system("pause");
    return 0;

}




bool inputcheck(int in)
{
    //exit
    if(in==-1)
        return true;

    cin.clear();//reset cin;
    cin.ignore(numeric_limits<streamsize>::max(),'\n');//clear cin buffer
    if(cin.gcount()!= 1)//not integer
    {
        cout<<"Invalid input.\n\n";
        return false;
    }
    else if(in<=0)//negative or zero
    {
        cout<<"Input can't be negative or zero.\n\n";
        return false;
    }
    //no error
    return true;
}


bool isPrime(int integer)
{
    for(int i=2;i<=sqrt(integer);i++)
        if(integer%i==0)//not prime number
            return false;
    //prime number
    return true;
}
