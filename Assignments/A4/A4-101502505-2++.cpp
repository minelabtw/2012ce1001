#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    int x,y=0,z,w=0;
    cout << "Enter a positive integer : ";
    cin >> x;

    if ( cin == false )//錯誤偵測非數字
        cout << "Invalid input.";
    else if ( x <= 0 )//錯誤偵測小於等於0的數
        cout << "Input can't be negative.";
    else
    {
        cout << "Binary of " << x <<" : ";
        while ( x > 0 )//迴圈這要怎麼解釋...嗯......
        {
            z=x%2;//餘數為1或0
            w=w+z*pow(10,y);//連加像是?*10^0+?*10^1+?*10^3+...
            x=x/2;
            y++;
        }
        cout << w;
    }
    return 0;
}
