//這是加分版 輸出為正常方向
#include <iostream>
#include <math.h>
using namespace std; 

int main()
{
	int in;
	int counter=0;
	int buffer;

	cout<<"Enter a positive integer : ";
	if(cin>>in==false)
		cout<<"Invalid input.";
	if(in<0)
		cout<<"Input can't be negative.";
	buffer=in;
	for(;;)
	{//先計算總共有幾位數
		in=in/2;
		//次方加一
		counter++;
		if(in==1)
			break;
	}
	//counter為最高次方
	//開始輸出
	in=buffer;//in恢復原始值
	for(;in>=0;)
	{
		if(in>=pow(2.0,counter))//如果數值大於二的最高次方
		{
			cout<<"1";
			in=in-pow(2.0,counter);
			counter-=1;
			continue;
		}
		if(in<pow(2.0,counter))//如果小於
		cout<<"0";
		if(counter==0)//判定
			break;
		counter-=1;
	}	
	return 0;
}
