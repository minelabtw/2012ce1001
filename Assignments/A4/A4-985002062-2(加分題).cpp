#include <iostream>
#include <math.h>
#include <cstdlib>
using namespace std;
int power(int,int); 
int main()
{  
   int total=0,m=1,i,n,a;
   cout<<"Enter a positive integer : ";
   if(cin>>n)
   {
      if(n<0)
      {   
          cout<<"Input can't be negative.";
          return 0;
      }         
      a=n;
      while(n!=0)
      {
        total=total+(n%2)*m;
        n=n/2;
        i=n%2;     //i表示除以2的餘數
        m=m*10;
      } 
      cout<<"Binary of "<<a<<" : "<<total<<endl;
   }
   else
      cout<<"Invalid input."<<endl;
    
//  system("pause");
    return 0;
}


