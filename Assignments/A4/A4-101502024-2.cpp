#include <iostream>
using namespace std;

int main()
{
    int x,y=0,z;
    cout<<"Enter a positive integer : ";//Show line.
    if( cin >> x == false || x < 0 )//Judge if the number inputed is valid, invalid, negative or zero.
    {
       if( x<0 )
       cout << "Input can't be negative.";//Show line.
       else
       cout << "Invalid input.";//Show line.
       return 0;
    }
    if( x > 0 )
    {
       z = x;//Keep the origin volume of x to z.
       cout<<"Binary of "<< z <<" : ";
           while( x >= 1 )//The process to change a demicial into a binary.
           {
             y = x % 2;
             x = x / 2;
             cout << y;
           }
    }
    return 0;
}
