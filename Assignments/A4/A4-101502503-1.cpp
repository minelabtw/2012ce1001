// 101502503
#include<iostream> // allows program to output data to the screen
#include<cmath>

using namespace std ;

// funtion main begins program execution
int main ()
{
    int a, b=0, c=0; // variable declarations
    bool isPrimeFlag = true ;

    cout << "Enter an positive integer : " ;

    if ( cin >> a == false || a < 0 )
    {
        if ( a <= 0 ) // determine whether a is smaller than 0 or equal to 0
        cout << "Input can't be negative or zero." << endl ; // display result
        else
        cout << "Invalid input." << endl ; // display result
        return -1 ;
    } // end if

    else
        cout << "positive prime divisors of " << a << " :" ; // display result
        for ( b=2 ; b<=a ; b++ ) // b的初始值為1,當b小於等於a時做下面指令,做完b+1
        {
            isPrimeFlag = true;
            if ( a%b==0 ) // 如果a除以b餘數為0,做下面指令
            {
                for ( int i=2 ; i <= sqrt( (float) b ) ; i++ ) //i的初始值為2,當i為小於開根號b的最大整數時做下面指令,做完i+1
                    if ( b%i==0 ) // 如果b除以i餘數為0,做下面指令
                    {
                        isPrimeFlag = false ; // 將isPrimeFlag設為false
                        break ; // 跳出if
                    } // end if
                if (isPrimeFlag) // 如果isPrimeFlag=ture,做下面指令
                    cout << " " << b ; // display result
            } // end if
        } // end for
    return 0 ; // indicate that program ended successfully
} // end main
