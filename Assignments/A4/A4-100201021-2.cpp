#include<iostream>
#include<math.h>
// (decimal)200 > (binary)11001000
using namespace std;
int num,i,j,k,ed;

int main()
{
    cout << "Enter an positive integer : ";
    cin >> num;
    if (num == false)   //debug
    {
        cout << "Invalid input" << endl;
        return 0;
    }
    if (num <= 0)       //debug
    {
        cout << "Input can't be negative or zero." << endl;
        return 0;
    }
    ed = 0;
    j=0;
    cout << "Binary of " << num << " : ";
    while(num!=0)
    {
        k = num%2;              //十進位轉二進位算法（短除法）
        for(i=1;i<=j;i++)       //
        {
            k *= 10;            //位數
        }
        ed += k;
        j++;
        num /=2;

    }
    cout << ed << endl;
    return 0;
}
