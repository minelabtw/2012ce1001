#include <iostream>
#include <cmath>

using namespace std ;

int main()
{
    int integer = 0 , traninteger = 0; // traninteger is later used to

    cout << "Enter a positive integer : ";

    if(cin >> integer == false)//detect illigle input
    {
        cout << "Invalid input." << endl;
        return -1;
    }
    else if(integer < 0)
    {
        cout << "Input can't be negative.";
        return -1;
    }
    else
    {
        while(integer != 0)
        {
            cout << integer%2 ;
            integer /= 2 ;
        }
        //below is unfinished bonus part.
        /****************************************************
        int i = 0 ;
        cout << "Binary of " << integer << " : ";

        if(integer%2 != 0)
        {
            while(integer != 0)
            {
                traninteger = traninteger*10 + (integer%2) ;
                integer /= 2 ;
            }
            cout << traninteger ;
        }
        else if(integer%2 == 0 && integer != 0)
        {
            traninteger = 10 ;
            while(integer != 0)
            {
                traninteger = traninteger*10 + (integer%2) ;
                integer /= 2 ;
            }
            cout << traninteger ;
        }
        else
        {
            cout << 0 ;
        }
        ****************************************************/
    }

    return 0;
}
