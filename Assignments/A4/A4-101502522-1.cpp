#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int number;
    bool isprimeflag = true;

    cout << "Enter an positive integer : ";
    cin >> number;

    if ( !cin ) //偵錯非數字輸入
    {
        cout << "Invalid input.";
        return 0;
    }

    if ( number < 0 ) //偵錯負數數值輸入
    {
        cout << "Input can't be negative or zero.";
        return 0;
    }

    cout << "positive prime divisors of " << number << " : ";

    for ( int i = 2 ; i <= number ; i++ ) //判斷所有小於number的數,即i
    {
        isprimeflag = true;

        for ( int j = 2 ; j <= sqrt( (float)i ) ; j++ ) //檢查i是否為質數
        {
            if ( i % j == 0 )
            {
                isprimeflag = false;
                break;
            }
        }

        if ( number % i == 0 and isprimeflag == true ) // 檢查i是否為number的質因數
            cout  << i << " ";
    }

    return 0;
}
