#include <iostream>
using namespace std;

int main()
{
    int binary,decimal;

    cout << "Enter a postive integer : ";
    cin >> decimal;

    if ( cin == false || decimal < 0 ) //偵測輸入字元或負數的錯誤
    {
        if ( decimal < 0 ) //若輸入負數則輸出以下錯誤訊息
        cout << "Input can't be negative." << endl;
        else //若輸入字元則輸出以下錯誤訊息
        cout << "Invalid input." << endl;

        return -1;
    }

    cout << "binary of " << decimal << " : " ;
    while ( decimal >= 1 ) //將輸入的十進位數轉換為二進位數
    {
        binary = decimal % 2;
        decimal = decimal / 2;
        cout << binary;
    }

    return 0;
}
