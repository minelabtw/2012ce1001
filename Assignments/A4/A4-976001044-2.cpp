// Assignment #4-2 
// A4-976001044-2.cpp 
// 可調整 flag 之值，以決定運算結果之輸出方式 
#include <iostream>
#include <cstdlib>
#include <cmath>

// 定義一個用來設定2的冪次方的值作為邊界值 
// 對應程式內宣告的變數(整數)，此處設預為20，預設可輸入的值之範圍：0 ~ 2^20-1 
// 此值最大可為 31，可輸入的值之範圍：0 ~ 2147483647 (2^31-1) 
#define boundary 20.0

// 定義一個標籤值，對應程式碼內所執行的 Function 
// 1 為執行 General  ( 反序輸出二進位值 ) 
// 0 為執行 Extra    ( 依序輸出二進位值 )
#define flag 0

using namespace std;
int General();
int Extra();

// 將 main 和兩個 function 都會用到的變數以全域變數宣告，並給定0為初始值 
// 其中變數 N 用於 pow(double,double) 函數，故直接宣告為 double
long input=0, output=0;
double N=0.0;

//  主程式部分，主要用途如下： 
//  1. input 數值，其中輸入值必須為整數，並介於 0 ~ (2^boundary -1) 之間 
//  2. 對 input 偵錯 
//  3. 呼叫副程式 
int main()
{
    cout << "Enter a positive integer : ";
    
    if ( cin >> input != false )  // 判斷 input 若"不為"字元，為有效輸入
    {
         // 判斷 input 在數值上是否為有效輸入，並輸出對應的錯誤訊息 
         if ( input < 0 ) 
         {
              cout << "Input can't be negative." << endl;
         }
         else if ( input > pow(2.0,boundary)-1 )
         {
              cout << "Input value too large.\n"
                   << "Change input value,\n" 
                   << "or the boundary value in procedure."
                   << endl;
         }
         else // input 為有效輸入 
         {
              cout << "Binary of " << input << " : ";
              
              // 依據前面設定的標籤值，呼叫對應的副程式 
              // 1 為執行 General  ( 反序輸出二進位值 ) 
              // 0 為執行 Extra    ( 依序輸出二進位值 )
              // 並且由副程式直接輸出主要結果 ( 二進位值 )
              if ( flag == 0 )
              {
                   Extra();
              }
              else if ( flag == 1 )
              {
                   General();
              }
         }
    }
    else     // 判斷 input 若"為"字元，則為無效輸入，輸出錯誤訊息 
    {
         cout << "Invalid input." << endl;
    }
    
    system("pause");
    return 0;
}

// 主要程序部分
// 將十進位的 input 轉換為二進位數字，倒反順序輸出結果 
int General()
{
    // 使用 While Loop 連續進行短除法運算
    // 將每次除法結果之餘數不換行輸出 
    while ( input > 1 )
    {
          output = input % 2;
          input = input / 2;
          cout << output;
    }
    
    // 短除法的最後 input 值遞減為 1 或 0 ( 不小於 1 ) 
    // 意義為除以 2 的零次方之餘數，直接將之輸出並換行 
    cout << input << endl;
    return 0;
}

// 主要程序部分
// 將十進位的 input 轉換為二進位數字，依照順序輸出結果
// 主要運算方法：將短除法每次除以2抓取餘數，改為每次除以2的冪次方以抓取商數 
//               目的為直接確認二進位數值的某一位數之值(降冪)
//               降冪除法抓取餘數後，在不使用陣列的情況下，直接輸出即為順序 
int Extra()
{
    // 確認 input 最零進的 2 的冪次方的數值
    // 除了用來設定 N 的知之外 
    // 可避免後面的 While Loop 執行多餘的運算，也可避免輸出不必要的結果 
    // 例如： 2 →000000000000001 的結果 
    for ( unsigned long i=1 ; i <= boundary ; i++ )
    {
        if ( input < pow(2.0,double(i)) )
        {
             N = i-1;
             break;
        }
    }
    
    // 將 input 值除以 2 的冪次方後，抓取並輸出商數的部分
    // 並將餘數指派回變數 input 直到 N 為 0 
    while ( N != 0 )
    {
          output = input / int(pow(2.0,N));
          input = input % int(pow(2.0,N));
          cout << output;
          N = N - 1;
    }
    
    // 運算法的最後 N 值遞減為 0 ( 不等於 0 ) 
    // 意義為除以 2 的零次方之餘數，直接將之輸出並換行
    cout << input << endl;
    return 0;
}
