#include <iostream>

using namespace std;
int main()
{
    int integer ;//設定一個變數

    cout<<"Enter a positive integer : ";
    cin>>integer;

    if(integer<0)//錯誤偵測，如果integer<0就輸出此段文字
    {
        cout<<"Input can't be negative.";
        return 0;
    }

    if( cin==false )//其餘的非法輸入則出現此段文字
    {
        cout<<"Invalid input.";
        return 0;
    }

    while(integer>0)//如果integer>0就進入迴圈中
    {
        cout<<(integer%2);//先輸出一個integer除2的餘數
        integer=(integer/2);//接著再用餘數除二
    }

    return 0;
}

