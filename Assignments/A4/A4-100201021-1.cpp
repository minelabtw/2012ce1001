#include<iostream>
#include<math.h>

using namespace std;
int num,i,j,k;

int main()
{
    cout << "Enter an positive integer : ";
    cin >> num;
    if (num == false)   //debug
    {
        cout << "Invalid input" << endl;
        return 0;
    }
    if (num <= 0)       //debug
    {
        cout << "Input can't be negative or zero." << endl;
        return 0;
    }
    for(i=2;i<=num;i++)
    {
        k=0;
        if(num%i==0)    //Factor
        {
            for(j=1;j<=sqrt(i);j++)     //Prime number
            {
                if(i%j==0)
                k++;
            }
            if(k==1)
            cout << i << " ";
        }
    }
    return 0;
}
