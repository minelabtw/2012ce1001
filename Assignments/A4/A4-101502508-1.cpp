#include<iostream>
#include<cmath>

using namespace std ;

int main()
{
    int integer , factor ;
    bool isPrimeFlag = true ;//isPrimeFlag is used for record whether this number is a prime number.

    cout << "Enter an positive integer : " ;

    if ( cin >> integer == false )//check wheather input is not integer
    {
        cout << "Invalid input." << endl ;
        return -1 ;
    }

    if ( integer <= 0 )//check input is negative
    {
        cout << "Input can't be negative or zero." << endl ;
        return -1 ;
    }

    cout << "positive prime divisors of " << integer << " : ";

    for ( int factor = 2 ; factor <= integer ; factor++ ) //used "for" statement to find all the factors
    {
        if ( integer % factor == 0 ) //confirm that the value is a factor
        {
            for ( int i = 2 ; i <= sqrt((float)factor) ; i++ ) //confirm that factors are prime numbers
            {
                isPrimeFlag = true ;
                if ( factor % i == 0 )//if factors can be divided then they aren't prime numbers
                {
                    isPrimeFlag = false ;
                    break ;
                }
            }

            if ( isPrimeFlag )
                cout << factor << " " ;
        }
    }

    return 0 ;
}
