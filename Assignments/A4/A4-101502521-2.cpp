#include <iostream>

using namespace std;

void print_binary(int);

int main()
{
    int n;
    char check;
    cout << "Enter a positive integer : ";

    if(!(cin >> n)||n<0)                            // Error detect
    {
        if(n<0)
            cout << "Input can't be negative.\n";
        else
            cout << "Invalid input.\n";
        return -1;
    }

    check = cin.get();
    if(check!=' ' && check!='\n' && check!=0)       //Detect invalid char after integer, like "123a"
    {
        cout << "Invalid input.\n";
        return -1;
    }

    cout << "Binary of " << n << " : ";
    if(n==0)
        cout << "0";
    else
        print_binary(n);                            //Function to change Dec to Binary and print it

    cout << endl;

    return 0;
}

void print_binary(int n)                            // 遞迴 先往下找 返回時在輸出
{
    if(n==0) return;                                // 到零為止
    print_binary(n/2);
    cout << n%2;
}
