#include <iostream>
using namespace std;
int main()
{
    int a , b = 0,c = 0,i = 1;
    cout << "Enter a positive integer : ";
    if( cin >> a == false || a < 0 )//輸入一個數值a,a<=0或a為字元
    {
        if( a < 0 )//若a<0則顯示"Input can't be negative."
            cout << "Input can't be negative.";
        else//若a為字元則顯示"Invalid input."
            cout << "Invalid input.";
        return 0;
    }
    cout << "Binary of " << a << " : ";
    while( a >= 1 )//當a>=1時進入迴圈
    {
        b = a % 2;//b等於a除以2的餘數
        a /= 2;//將a除以2

        b *= i;//使顯示出來的10進位為正常的
        i *= 10;
        c += b;
    }
        cout<<c;
    return 0;
}
