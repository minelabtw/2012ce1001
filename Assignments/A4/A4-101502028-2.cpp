#include <iostream>
#include <cstdlib>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int decimal, binary = 0, bit = 1, n;
    cout << "Enter a positive integer : ";
    if ((cin >> decimal == false) || (decimal < 0)) // check the input
    {
        if (decimal < 0) // if the input is negative
            cout << "Input can't be negative." << endl; // appears this message
        else // if the input is a character
            cout << "Invalid input." << endl; // appears this message
        return -1; // error
    }
    cout << "Binary of " << decimal << ": ";
    while ( decimal != 0 ) // a loop to check the input number to convert it to binary
    {
        n = decimal % 2; // take the decimal number to convert it to binary
        n = n * bit; // multiply the taken number by the first bit
        bit = bit * 10; // multiply the bit by 10 because of the decimal
        decimal = decimal / 2; // check to see the second binary
        binary = binary + n; // add all binaries
    }
    cout << binary << endl; // print out the added binary
    return 0; // end program
}
