#include <iostream>

using namespace std;

int main()
{
    int i=0,j=0;

    cout<<"Enter a positive integer : ";
    cin>>i;

    if(i==false)//當輸入i不為數字時,其進入條件
       cout<<"Invalid input.";

    if(i<0)//當輸入i小於0時,其進入條件
       cout<<"Input can't be negative.";

    if(i>0)
    {

       cout<<"Binary of "<<i<<" :";
           while(i>=1)
           {
             j=i%2;
             i=i/2;
             cout<<j;
            }
    }
    return 0;
}
