//有做加分題
#include <iostream>

using namespace std;

int main()
{
    int integer,sum=0,a=1;

    cout << "Enter an positive integer : ";
    if ( cin >> integer ==false || integer < 0 )//判斷integer是否為字元輸入或小於0
    {
        if ( integer < 0 )//判斷integer是否小於0
            cout << "Input can't be negative." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }

    cout <<"Binary of " << integer << " : ";
    while ( integer >0 )//integer大於0時進入迴圈
    {
        sum += ( integer % 2 ) * a;//sum等於sum加上integer除以2的餘數乘上a
        integer = integer / 2;//integer等於integer除以2
        a *= 10;//a等於a乘以10
    }
    cout << sum << endl;//輸出sum

    return 0;
}
