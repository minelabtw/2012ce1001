/*****************************************
* Assignment 4
* Name: �d�u��
* School Number: 986001511
* E-mail: lu19900213@gmail.com
* Course: 2012-CE1001
* Submitted: 2012/10/18
*
***********************************************/
/*****************************************
*
* find the INVERSE BINARY of a decimal number,
* example: 29's binary is 11101, the program will show 10111
*
***********************************************/

#include<iostream>
#include<cstdlib>

using namespace std;

int main(){

    int num1,num,temp;

    cout << "Enter a positive integer : " ;
    cin >> num1;

    // assign a new variable for initial number.
    num=num1;

    // if th number is non-number(characters)
    if(num==1972411956){
        cout << "Invalid number." << endl;
    }
    // if the number is negative
    else if (num <= 0 ){
        cout << "Input can't be negative or zero." << endl;
    }
    // if it is a normal number
    else{
        cout << "Bibary of " << num1 << " : ";
        // if the number greater than 1
        while(num >= 1){
            // output the remainders
            cout << num%2;
            // assign a new number replace the old one to do loops
            num=num/2;
            // if the number less than 1, means the last step, end program
            if (num < 1){cout << endl;}
            }
            }

    system("pause");
    return 0;
}
