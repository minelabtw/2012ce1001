#include <iostream>

using namespace std;

int main()
{
    int decimal , binary;

    cout << "Enter a positive integer : ";
    cin >> decimal;



    if ( !cin ) // 偵錯非數字輸入
    {
        cout << "Invalid input.";
        return 0;
    }

    if ( decimal < 0 ) // 偵錯負數輸入
    {
        cout << "Input can't be negative.";
        return 0;
    }

    cout << "Binary of " << decimal << " : ";

    while ( decimal > 0 ) //十進位數值轉換為2進位數值(反向輸出)
    {
        binary = decimal % 2;
        decimal = decimal / 2;

        cout << binary;
    }

    return 0;
}
