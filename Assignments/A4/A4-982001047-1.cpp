#include<iostream>

using namespace std ;

int main(){
    
    
    int po ;
        
    cout << "Enter a positive integer : ";
    
    if( cin >> po == false )  // 輸入字元 停止程式 
    {                          
      cout << "Invalid input." << endl; 
      return -1;
    }

    if( po < 0 )            // 輸入負數  停止程式 
    {
      cout << "Input can't be negative." << endl;
      return -1;
    }
    int i;
    cout << "positive prime divisors of " << po << " :" ;
    for( i = 2 ; i <=po ; i++){
         if( po%i == 0 )        //除以i 餘零  印出 因數i 
         {      
           cout << " " << i;
         }
         while( po%i ==0)       //把重複的因數消掉 
         {
           po /= i;
         }  
    }

    return 0;
}
