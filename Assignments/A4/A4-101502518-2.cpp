#include<iostream>
using namespace std;
int main()
{
    int integer;

    cout<<"Enter a positive integer : ";

    if(cin>>integer==0||integer<0)
    {
        if(integer<0)//檢查輸入是否為負數
        {
            cout<<"Input can't be negative.";
        }
        else//檢查輸入是否為非數字
        {
            cout<<"Invalid input.";
        }
        return -1;
    }

    cout<<"Binary of "<<"integer : ";

    if(integer==1)
        cout<<"1";

    while(integer/2!=0)
    {
        cout<<integer%2;
        integer=integer/2;

        if(integer/2==0)
            cout<<"1";
    }
    return 0;
}
