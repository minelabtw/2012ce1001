#include<iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int a;//宣告變數a
    cout <<"Enter a positive integer :";
    cin >> a;

    if( a < 0 )
    cout << "Input can't be negative." << endl;
    else if( cin == false )//若輸入的a不是數字型態
    cout << "Invalid input." << endl;
    else if ( a > 0 )
    {
      cout << "Binary of " << a << " : ";
      while ( a > 0 )
      {
         if(a%2==0)//若a除以2餘數為0則把0輸出
         cout << "0";
         else//若餘數不為0則輸出1
         cout << "1";
         a=a/2;//每次迴圈中的a為上一次的a除以2
      }

    }
   return 0;
}
