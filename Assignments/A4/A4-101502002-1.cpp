#include<iostream>
#include<cmath>
using namespace  std;//簡化std
int main()
{
    int a,b=2,i=2;//a為輸入數、b為用來判斷是否為因數、c為用來判斷b是否為質數
    bool isprimeflag=true;
    cout<<"Enter an positive integer : ";//輸出文字
    if (cin>>a)//若輸數為整數
    {
        if(a<=0)//若輸入小於等於零
            cout<<"Input can't be negative or zero."<<endl;//輸出文字並結束
        else//若輸入大於零
        {
            cout<<"positive prime divisors of "<<a<<" : ";
            for ( b=2; b<=a; b++)//迴圈判斷b是否為質數
            {
                isprimeflag=true;
                if( a == 1 )//a等於一時沒有任何質因數
                {
                    isprimeflag=false;
                    return 0;
                }
                else
                    for ( i=2; i<=sqrt((double)b); i++)//判斷b是否為質數
                    {
                        if (b%i==0)
                        {
                            isprimeflag=false;
                            break;//若整除就表示b不是質數，結束回圈
                        }
                    }
                if (isprimeflag and a%b==0 )//若b為質數且被a整除則其為a的質因數
                    cout<<b<<" ";

            }
        }
    }
    else
        cout<<"Invalid input."<<endl;//若非法輸入則輸出文字並結束
    return 0;
}
