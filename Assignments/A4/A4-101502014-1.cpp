#include <iostream>
#include <math.h>
using namespace std;
int main()
{
    int integer , c;
    cout << "Enter an positive integer : ";
    if(cin >> integer == false) //判斷輸入是否為字元,是的話則輸出錯誤訊息
        cout << "Invalid input." << endl;
    else if(integer < 0) //判斷輸入是否為負數,是的話則輸出錯誤訊息
        cout << "Input can't be negative." << endl;
    else
    {
        cout << "positive prime divisors of " << integer << " : ";
        for(int i = 2 ; i <= integer ; i++)
        {
            c = 1; //假設i為質數,則c等於1
            for(int j = 2 ; j <= sqrt(i) ; j++) //判斷i是否為質數
            {
                if(i % j == 0) //若j整除i的話,則i不是質數,c就等於0,並且跳出迴圈
                {
                    c = 0;
                    break;
                }
            }
            if(integer % i == 0 && c == 1) //若i為質數,且i整除integer,則i為integer的質因數
                cout << i << " ";
        }
        cout << endl;
    }
	return 0;
}
