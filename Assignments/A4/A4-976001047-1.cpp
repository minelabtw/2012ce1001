#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;

int main()

{
    int integer=0;
    int a=2;
    int b=0;
    
    cout << "Enter an positive integer : ";
    cin >> integer ;
    
    if (cin==false)                        //error message for input isn't a number.
    {
        cout << "Invalid input.\n";
        
        system ("PAUSE");
        return 0;             
    }
    
    if (integer<=0)                       // error message for input is negative.
    {
        cout << "Input can't be negative or zero.\n";
         
        system ("PAUSE");
        return 0;
    }     
    
    cout << "positive prime divisors of "<< integer <<" : ";
    
    while (a<=integer)              
    {          
        if(integer%a==0)              //find the fator for input and the inside loop is to judge the fator is a prime or not. 
        {                             
            b= true;             
            for(int i=2 ; i<a ; i++) 
            {
                if (a%i==0)          
                {                 
                    b=false;
                    break;
                }
            }
            if (b)
                cout << a <<" ";    // print all the prime fator for input.
        }
       
        a=a+1;
    }          
    
    system("PAUSE");
    return 0;
    
}             
