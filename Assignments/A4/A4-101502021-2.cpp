#include <iostream>

using namespace std;

int main()
{
    int interger ;
    int remainder ;
    int sum=0 ;
    int A=1 ;

    cout << "Enter a positive interger : " ;

    if (cin >> interger ==false) //如果輸入的不是數字
    {
        cout << "Invalid input." << endl ;
        return 0 ;
    }

    if (interger<0)
    {
        cout << "Input can't be negative." << endl ;
        return 0 ;
    }

    while (interger >= 1)
    {
        remainder=interger%2 ; //remainder是輸入的interger除以2的餘數
        sum+=remainder*A ; //總和是前一次總和加上餘數乘以A
        A*=10 ;
        interger/=2 ;
    }

    cout << "Binary of 200 : "  << sum << endl ;

    return 0 ;
}
