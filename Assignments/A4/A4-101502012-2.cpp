#include <iostream> //有做加分題 二進位不是反的顯示!!!!
#include <math.h> //數學式需要的資料庫
using namespace std;

int main()
{
    int integer,buffer,answer=0,number,count=0;
    cout << "Enter an positive integer : ";
    cin >> integer;

    buffer=integer;   //buffer用來存integer原本的值 while迴圈完之後 integer值就變零了 buffer就可以代替顯示

    if (cin==false)  //錯誤偵測:輸入非字元
        cout << "Invalid input.";
    else if (integer<=0)  //錯誤偵測:輸入不能小於等於0
        cout << "Input can't be negative or zero.";
    else
    {
        while (integer>0)
        {
            number=integer%2;
            answer=answer+number*pow(10,count); //10進位轉2進位 integer/2的餘數=各位數字 餘數在乘10的次方 加起來就變成2進位
            integer=integer/2;
            count++;
        }
        cout << "Binary of " << buffer << " : " << answer << endl;
    }
    return 0;
}  //end main
