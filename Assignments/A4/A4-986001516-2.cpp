//Assignment 4-2
//input : integer , positive decimal number input
//output : integer , binary of input (inverse)
//bonus output : interger , binary of input (usual)
#include <iostream>
#include <cstdlib>
#include <cmath>
using namespace std;

int main()
{
    int dec,bin=0,dtr=0,temp,i=0;/*相關變數的宣告與定義,dtr用來做程式執行或者跳躍的判斷標準,初始值為0*/
    cout << "Enter an positive integer : ";
    if (cin >> dec == false)/*以if...判斷輸入是否為整數數值,若非,則印出錯誤訊息,dtr=-1*/
    {
        cout << "Invalid input.\n" << endl;
        dtr=-1;
    }
    temp=dec;/*將輸入值先暫存*/
    if (dtr==0)/*若上面判定為整數數值,則dtr仍為初始值0,執行第二次檢驗,確認是否為正整數或0,若非,則印出錯誤訊息,dtr=-1*/
    {
        if (dec<0)
        {
            cout << "Input can't be negative.\n";
            dtr=-1;
        }
    }
    if (dtr==0)/*若通過上面兩項檢驗,則確認輸入的為整數數值,開始轉換至2進位表示法*/
    {
        cout << "Binary of " << dec << " : ";
        while(dec!=0)/*利用while迴圈,將輸入值對2連除並取其餘數按順序印出,此處完成的結果會是正確2進位表示法的反向排列*/
        {
            cout << dec%2;
            dec=(int)(dec/2);
        }
        cout << endl;
    }
    dec=temp;/*因為上面第一次回圈已經將變數dec覆蓋,這裡利用暫存值將它還原*/
    if (dtr==0)/*加分題,將輸入值轉為二進位表示法,且以"正序"方式顯示*/
    {
        cout << "(Bonus) Binary of " << dec << " : ";
        while(dec!=0)/*一樣利用while迴圈,但這次則是先將結果的二進位值想像為10進位,所以最小位即是10的0次方,依此類推,而原本求餘數的方法即是從最小位取起,所以只要乘上相對應的10的次方數並且累加,最後結果即為正序的表示法*/
        {
            bin=bin+(dec%2)*pow(10,i);
            i=i+1;
            dec=(int)(dec/2);
        }
        cout << bin << endl;
    }
    system("pause");
    return 0;
}
