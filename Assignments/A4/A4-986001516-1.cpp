//Assignment 4-1
//input : integer , positive number input
//output : integer , positive prime divisors of input
#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    int num,dtr=0,div=2;/*宣告併定義相關變數,div為除數,用以檢驗輸入值是否能夠整除此數,初始值為2即最小質因數,其中dtr用來做程式執行或者跳躍的判斷標準,初始值為0*/
    cout << "Enter an positive integer : ";
    if (cin >> num == false)/*以if...判斷輸入是否為整數數值,若非,則印出錯誤訊息,dtr=-1*/
    {
        cout << "Invalid input.\n" << endl;
        dtr=-1;
    }
    if (dtr==0)/*若上面判定為整數數值,則dtr仍為初始值0,執行第二次檢驗,確認是否為正整數,若非,則印出錯誤訊息,dtr=-1*/
    {
        if (num<=0)
        {
            cout << "Input can't be negative or zero.\n";
            dtr=-1;
        }
    }
    if (dtr==0)/*若通過上面兩項檢驗,則確認輸入的為正整數,開始執行求質因數程序*/
    {
        cout << "positive prime divisors of " << num << " :";
        while(num!=1)/*此處判定執行while迴圈的標準為num是否為1,因為執行過程中,num的值會重複被新的結果覆蓋*/
        {
            if (num%div!=0)/*若num無法整除除數div,則div以其值+1覆蓋,再重複檢驗,逐個檢查各個整數是否為num的因數*/
            {
                div=div+1;
                continue;
            }
            else
            {
                if ((num/div)%div==0)/*若num可整除除數div,則利用此組if... else...判斷div的高次是否亦能整除num,此步驟是為了在逐個檢驗的整數中,排除掉num的因數裡,非質數的部份*/
                {
                    num=num/div;
                    continue;
                }
                else/*只有在num無法被當前整數連續整除的情況下,才將此整數div印出,並檢驗下一個整數是否能被num整除*/
                {
                    num=num/div;
                    cout << " " << div;
                    div=div+1;
                }
            }
        }
        cout << endl;
    }
    system("pause");
    return 0;
}
