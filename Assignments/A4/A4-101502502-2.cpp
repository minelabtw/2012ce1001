#include <iostream>
using namespace std;

int main()
{
    int num1;
    int num2;

    cout << "Enter a positive integer : ";

    if ( cin >> num1 == false || num1 < 0 )
    {

        if ( num1 < 0 )//判斷是否為負數
            cout << "Input can't be negative." << endl;

        else//錯誤的輸入
            cout << "Invalid input." << endl;
    }//end if

    else
    {
        cout << "Binary of " << num1 << " : ";

        while ( num1 > 0 )
        {
            num2 = num1 % 2;//找出除以二的餘數
            num1 /= 2;

            if ( num2 == 1 || num2 == 0 )
                cout << num2;
        }//end while

    }//end else

    return 0;
}//end main
