#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

int main()
{
     int dec,item;
     string binary;
     cout <<"Enter a positive integer : ";
     if(cin >> dec==false)                    //如果輸入為字元，告知為錯誤訊息，並結束程式
     {
        cout <<"Invalid input."<<endl;
        return -1;
     }

     else if(dec<0)                           //如果輸入為負數或0，告知輸入必須為正數，並結束程式
     {
        cout <<"Input can't be negative."<<endl;
        return -1;
     }
     else
        cout <<"Binary of "<<dec<<" : ";
     /*while(dec>0)                            //將十進位換成二進位(題目一般要求)
     {
        item=dec%2;
        cout <<item;
        dec=dec/2;
     }*/
     while(dec>0)                             //將十進位換成二進位(加分版)，將新產生的餘數轉成字元放在字串最左邊
     {
        binary=char('0'+dec%2)+binary;
        dec=dec/2;
     }
     cout <<binary;

     system("pause");
     return 0;

}
