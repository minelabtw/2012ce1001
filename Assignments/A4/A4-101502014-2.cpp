#include <iostream> //加分題
using namespace std;
int main()
{
    int integer , count = 1 , sum = 0; //sum記錄integer轉二進位後的答案
        cout << "Enter a positive integer : ";
    if(cin >> integer == false) //判斷輸入是否為字元,是的話則輸出錯誤訊息
        cout << "Invalid input." << endl;
    else if(integer < 0) //判斷輸入是否為負數,是的話則輸出錯誤訊息
        cout << "Input can't be negative." << endl;
    else
    {
        while(integer > 0) //十進位轉二進位
        {
            sum += count * (integer % 2);
            integer /= 2;
            count *= 10; //每次跑完迴圈,則前進一位數
        }
        cout << sum << endl;
    }
	return 0;
}
