#include <iostream>

using namespace std;

int main()
{
    int a,b=2,n=1,k;

    cout<<"Enter an positive integer : ";

    if(cin>>a==false||a<=0)               //判斷是否為正確輸入值
    {
        if(a<=0)                          //小於等於零的情況
            cout<<"Input can't be negative or zero.";
        else                              //錯誤輸入的情況
            cout<<"Invalid input.";
    }

    else
    {
        cout<<"positive prime divisors of "<<a<<" : ";

        for(b=2;b<=a;b++)                 //檢驗每個小於a的質數
        {
            k=0;
            for(n=1;n<=b/2;n++)           //判斷值b是否為質數
            {
                if(b%n==0)
                    k=k+1;
            }

            if(k==1&&a%b==0)              //因質數除了本身的因數只有一個1，當k等於1且b被a整除，此b即為a的質因數
                cout<<b<<" ";

        }
    }
    return 0;
}
