#include<iostream>
#include<math.h>

using namespace std;

int main()
{
    int x,y=0,z=0; //x to store the integer entered
                       //y to store the sum of the remainder of x/2 times ten to the power of a number, of which the number starts from 0
                       //z to be the power of ten,and it starts from 0

    cout << "Enter a positive integer : ";
    cin >> x;

    if(cin==false) //if the value of x entered isn't an integer,then enter this section
    {
        cout << "Invalid input." << endl;
        return 0;
    }

    if(x<0) // if the value of x entered is smaller than 0,then enter this section
    {
        cout << "Input can't be negative." << endl;
        return 0;
    }

    if(x==0) // if the value of x entered is 0,then enter this section
    {
        cout << "Binary of 0 : 0";
    }

    cout << "Binary of " << x << " : ";

    while(x>0) //if the value of x entered is greater than 0,then start to transform x into a binary number
    {
        y=y+(x%2)*pow(10,z); //this is the formula to transform x into the binary number
        z++;
        x/=2;
    }
    cout << y;
    cout << endl;
return 0;
}
