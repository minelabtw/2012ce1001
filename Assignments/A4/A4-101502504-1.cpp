//101502504
#include <iostream>//allows program to output data to the screen
#include<cmath>
using namespace std;//program uses

int main()//function main begins program exection
{
    int a=1,b=0;//variable declaration
    bool isPrimeFlag;

    cout << "Enter an positive integer : ";//prompt user to data

    if ( cin >> b && b <= 0 )//b<=0
    {
        cout << "Input can't be negative or zero." << endl;
        return -1;
    }

    else if ( b == false )//b is not a number
    {
        cout << "Invalid input." << endl;
        return -1;
    }

    cout << "positive prime divisors of " << b << " : ";
    while( a <= b )// a = 1 to b
    {
        isPrimeFlag = true;
        if( a == 1 )//a != 0
            isPrimeFlag = false;
        else
            for( int i = 2 , end = sqrt(a) ; i <= end ; i++)//a can't be even
                if( a%i == 0 )
                {
                    isPrimeFlag = false;
                    break;
                }
        if( isPrimeFlag && b%a==0 )// a is a prime number and is the factor of b
            cout << a << " " ;//show the answer
        a++;
    }
    cout << endl;
    return 0;//end successfully
}//end program main
