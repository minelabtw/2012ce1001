#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    int x,y;
    cout << "Enter a positive integer : ";
    cin >> x;

    if ( cin == false )//錯誤偵測非數字
        cout << "Invalid input.";
    else if ( x <= 0 )//錯誤偵測小於等於0的數
        cout << "Input can't be negative.";
    else
    {
        cout << "Binary of " << x <<" : ";
        while ( x > 0 )//不斷除2的迴圈
        {
            if( x%2 == 0 )//整除則顯示0
                cout << "0";
            else
                cout << "1";//不整除則顯示1
            x = x/2;
        }
    }
    return 0;
}
