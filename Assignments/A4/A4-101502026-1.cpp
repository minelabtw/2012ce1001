#include <iostream>
#include <math.h> //declare "sqrt" in this scope
#include <cstdio> //declare "stdin" in this scope

using namespace std;

int main()
{
    while(1) //set a loop to let the user retype a variable
    {
        int x,y=2,z,p; //x to store the number entered
                       //y to find out divisors of x
                       //z to store the value of y because y increases in the "for" loop
                       //p to store the value of the square root of z

        cout << "Enter an positive integer : ";

        if( cin >> x == false) //if x entered isn't an integer,then enter this section
        {
            cout << "Invalid input." <<endl;
            rewind(stdin); //to clear the buffer
            cin.clear(); //to clear the buffer
            continue; //after cleaning the buffer ,continue the loop
        }

        if(x==-1) //here I set a number -1,if the user enter it,exit the program
        {
            break; //if the value of x entered is 1,then exit the "while" loop
        }

        if(x<=0)
        {
            cout << "Input can't be negative or zero." << endl; //if the value of x entered is smaller than 0,then print out an error message
            continue;
        }

        cout << "positive prime divisors of " << x <<" : ";

        for(x;x>1&&x>=y;y++) //this is the loop to find out the divisors of x
        {
            if(x%y==0) //if the remainder of x/y is 0,then execute the instructions below
            {
                z=y; //use z to store the value of y
                p=(float)sqrt(z); //use p to store the value of the square root of z
                while(p>1) //set a loop to check whether y is a prime number or not
                {
                    if(z%p!=0) //if the value of z/p isn't 0,that means z can't be divided exactly
                    {
                        p--; //if z can't be divide exactly,then increase the value of p by 1
                    }
                    else
                    break; //if the value of z/p is 0 ,then exit the while loop
                }
                if(p==1) //if the value of p already decreases to 1,that means that we can make sure that z is a prime number
                {
                    cout << y << " ";
                }
            }
        }
    cout << endl;
    }
return 0;
}
