#include<iostream>
using namespace std;
int main()
{
    int input,log=0,i=1,sum=0;//宣告變數
    cout<<"Enter an positive integer : ";
    if(cin>>input==false||input<=0)//錯誤偵測
    {
        if(input<=0)//若為負數之錯誤輸出
            cout<<"Input can't be negative.\n";
        else
            cout<<"Inalid input.\n";//若為字元之錯誤輸出
        return -1;
    }
    else
    {
        cout<<"Binary of "<<input<<" : ";//輸出結果
        while(input!=0)
        {
            if(input%2==0)//當變數整除2時
                cout<<0;//二進位末位數為0
            else
                cout<<1;//否則為1
            input/=2;//將變數以二進位方式進位
        }
    }
    return 0;
}
