/*****************************************
* Assignment 4
* Name: �d�u��
* School Number: 986001511
* E-mail: lu19900213@gmail.com
* Course: 2012-CE1001
* Submitted: 2012/10/18
*
***********************************************/
/*****************************************
*
* find the prime dividors of integer numbers.
*
***********************************************/

#include<iostream>
#include<cstdlib>

using namespace std;

int main()
{
    int num1,num,temp;

    cout << "Enter a positive integer : " ;
    cin >> num1;

    // assign a new variable for initial number.
    num=num1;

    // if th number is non-number(characters)
    if(num==1972411956){
        cout << "Invalid number." << endl;
    }
    // if the number is negative
    else if (num <= 0 ){
        cout << "Input can't be negative or zero." << endl;
    }
    // if it is a normal number
    else{
        // a initial dividor 2.
        temp=2;
        cout << "Positive prime dividors of " << num1 << " :" ;
        // if the number greater than the dividors, do loops
        while(temp <= num){
            // in the loops
            // if the dividors are finded, ouput the result, and divide the number into a new one
            if(num%temp==0){
                num=num/temp;
                cout << " " << temp;
                // if the number is 1, means the last step, end program
                if(num==1){
                    cout << endl;
                }
            }
            // if it has remainders, add the dividors.
            else{
                temp=temp+1;
            }
        }
    }

    system("pause");
    return 0;
}
