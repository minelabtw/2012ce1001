#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int i=0,j,k=0;

    cout<<"Enter an positive integer :";
    cin>>i;

    k=i;//使兩個值一個變動、一個保留，到最後可以檢驗答案

    if(i==false)//當a不為一個數字時，達成條件
    {
        cout<<"Invalid input.";
        return 0;
    }
    if(i<0)//當i小於0時,達成條件
    {
        cout<<"Input can't be negative or zero.";
        return 0;
    }


    cout<<"positive prime divisors of "<<i<<" : ";
    while(i>1)
    {


       j=1*pow(i,0.5);//算出i的根號
          while(j>1)
       {
            if(i%j!=0)
            j--;//i藉比其根號還小的數字逐一去找出是否有因數

            else//若找到相除餘數為0之數,其跳出迴圈
            break;


        }

    if(j==1&&k%i==0)//先找出i以下的質數,再去找藉由相除時餘數為0找出質因數

      cout<<i<<" ";

      i--;


    }
    return 0;
}
