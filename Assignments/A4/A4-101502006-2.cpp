#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    int x;
    cout<<"Enter an positive integer : ";
    if ((cin >> x == false) || (x<0))//error detection : if receive a character or a negative number , program is terminated.
    {
        if(x<0)
        cout << "Input can't be negative.";
        else
        cout << "Invalid input.";
        return 0;
    }
    cout << "Binary of " << x << " is : ";
    while (x>0)//used for transform decimal to binary.
    {
        cout << (x%2);
        x/=2;
    }
    return 0;
}
