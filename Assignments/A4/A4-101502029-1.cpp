#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int integer,b;
    cout << "Enter an positive integer : ";
    cin >> integer;
    if( cin == false || integer < 0 )//找integer是否錯誤或integer<=0
     {
        if( integer <= 0 )
            cout << "Input can't be negative or zero." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }
    cout << "positive prime divisors of " << integer << " : " << " ";

    for ( int i = 1 ; i <= integer ; i++ )
    {
        if ( integer % i == 0 ) //找i是否為integer因數
		{
			for ( int j = 1 ; j <= i ; j++ )
			{
				if ( i % j == 0 )//找j是否為i的因數
				{
					 b++; //因數的個數
				}
			}
			if ( b == 2 )//有兩個因數為質數
			{
                cout << i << " ";
			}
			b=0; //i的因數個數為零
		}
    }
    return 0;
}
