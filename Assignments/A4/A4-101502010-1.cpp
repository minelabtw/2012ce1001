#include<iostream>
#include<cmath>
using namespace std;
int check(int n)//製作檢驗質數的函數，若為質數則回傳1否則回傳0
{
    int i,flag=1;//宣告變數，flag(旗幟)預設為1
    if(n==1)//若輸入為1則回傳否
        flag=0;
    for(i=2;i<=sqrt(n);i++)//若大於1則進入迴圈
    {
        if(n%i==0)
            flag=0;//若有整數能整除則為非質數
    }
    return flag;//回傳旗幟
}
int main()
{
    int input;
    cout<<"Enter an positive integer : ";
    if(cin>>input==false||input<=1)//錯誤偵測
    {
        if(input<=0)
            cout<<"Input can't be negative or zero.\n";//0或負數之錯誤輸出
        else if(input==1)
            cout<<"1 is not a prime number.\n";//1之錯誤輸出
        else
            cout<<"Inalid input.\n";//字元錯誤輸出
        return -1;
    }
    else
    {
        cout<<"positive prime divisors of "<<input<<" : ";
        int n=2;
        while(n<=sqrt(input))//從正整數2開始檢驗
        {
            if(check(n)==1 && input%n==0)//檢驗是否為因數且為質數者
                cout<<n<<" ";//輸出結果
            n++;//檢驗下一個數
        }
    }
    return 0;
}
