#include <iostream>
using namespace std;

int main()
{
    int a; //"a" is what we input
    int m=1; //Used for converting decimal to binary
    int n=0; //Same as above
    cout<<"Enter a positive integer : ";
    while (cin>>a==false) //When input is not integer, tell the user that it's invalid
    {
        cout<<"Invalid input.";
        return -1;
    }
    while (a<0) //"a" can't be negative; otherwise we can't calculate
    {
        cout<<"Input can't be negative.";
        return -1;
    }
    if (a>0)
    {
        cout<<"Binary of "<<a<<" : ";
        while (a>0) //Calculate decimal to binary function
        {
            n+=(a%2)*m;
            a/=2;
            m*=10;
        }
        cout<<n;
    }
    return 0;
}
