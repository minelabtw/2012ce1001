#include <iostream>
#include<cmath>
#include <cstdlib>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int no1 = 2, number; // any integer, no1 = 2 -->> smallest prime divider
    cout << "Enter an positive integer : ";
    if ((cin >> number == false) || (number <= 0)) // check if the input is a character or a negative number
    {
        if (number <= 0) // if the input is a negativer number
            cout << "Input can't be negative or zero." << endl; // apears this message
        else // if the input is a character
            cout << "Invalid input." << endl; // appears this message
        return -1; // make an error if one of this two conditions is true
    }
    cout << "positive prime divisors of " << number << " : "; // appears this message to start counting all the positive prime divisors
    while (no1 <= number) // start a loop
    {
        if (number % no1 == 0) // if the remainder of the input number and no1 is 0, probably can be the prime divider
        {
            int prime = 0, no2 = 2; // prime = 0 -->>  prime divider, prime = 1 -->> non prime divider, no2 = 2 -->> the smallest prime number
            while (no2 < no1) // if this two number are the same, prime will be 0 and it's the divider
            {
                if (no1 % no2 == 0) // if the remainder of this two numbers is 0
                    prime++; // prime will add 1 that means non prime divider
                no2++; // no2 will always increase by 1 to get the same number as no1
            }
            if (prime == 0) // compare that if the no1 and  no2 are the same, means that they are a prime divider of the input number
                cout << no1 << " "; // print out the no
        }
        no1++; // the no1 will always increase by 1
    }
    cout << endl; // skip a line
    return 0; // ends the program
}
