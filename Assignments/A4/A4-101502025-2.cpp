#include <iostream>

using namespace std;

int main()
{   int num1,num2=0,num3;

    cout<<"Enter a positive integer : ";

    if(cin>>num1==false||num1<0) // if the input is not a positive integer, go the end
    {
        if(num1<0)
           cout << "Input can't be negative."<<endl; // if the integer is negative
        else
           cout << "Invalid input."<<endl; // if the input is not a integer
        return 0;
    }
    else if (num1>0)
        {
             num3=num1; // let the output number be the original input number
             cout<<"Binary of "<< num3 <<" : ";

           while(num1>=1) // out put the remain until it is equal to 0 or 1
           {
                num2=num1%2;
                num1=num1/2;
                cout<<num2;
           }
         }
    cout<<endl;// this is my habit
    return 0;
}
