#include <iostream>

using namespace std;

int main()
{
    int number; //定輸入值為整數
    bool real;  //定一個判斷值判斷是否為質數用
    cout << "Enter an positive integer : " ;
    /*若非數字則顯示錯誤
      若小於0或等於0顯示錯誤*/
    if ( cin >> number == false )
    {
        cout << "Invalid input. ";
        return 0;
    }
    if ( number <= 0 )
    {
        cout << "Input can't be negative or zero.";
        return 0;
    }
    cout << "positive prime divisors of " << number << " : ";
    /*設定一個迴圈，用來判斷是否為因數
      因為起始值設2所以另外判斷是否2的時候可以被整除，
      若可以則為因數。
      其他情況則判斷是否被整除，
      若可以被整除則進一步判斷是否為質數，
      否則i++，
      若可以被整除則進入另一個迴圈，
      再設一個起始值為2的數，
      逐步+1判斷是否i能除j，
      若可以則跳出迴圈且值為錯誤，
      所以不會印出，之後i+1
      若一直不行則代表此數為質數，
      輸出這個值                                            */
    for ( int i = 2; i <= number; i++)
    {
        if ( i == 2 && number % 2 == 0)
        {
            cout << i << " ";
        }
        if ( number % i == 0)
        {
            for (int j = 2; j < i ;j++)
            {
                if ( i % j == 0 )
                {
                    real = false;
                    break;
                }
                else
                    real = true;
            }
        }
        if ( real == true)
            cout << i << " ";
            real = false;
    }
    return 0;
}
