#include<iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int decimal,binary=0,bit=1;
    cout << "Enter a positive integer : ";
    cin >> decimal;
    if( cin == false )//檢查輸入是否錯誤
    {
        if( decimal < 0)//檢查輸入是否為負數
            cout << "Input can't be negative.";
        else
            cout << "Invalid input.";
        return -1;
    }
    cout << "Binary of " << decimal << " :";
    while( decimal != 0 )
  {
    binary += decimal % 2 * bit;
    bit *= 10;
    decimal /= 2;
  }
    cout << " " << binary ;
    return 0;
}
