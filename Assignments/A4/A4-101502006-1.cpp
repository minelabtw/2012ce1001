#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    int x;
    int n=2;
    bool isPrimeFlag;//used for record whether n is a prime number.
    bool isFactorFlag;//used for record whether n is a factor of x.
    cout<<"Enter an positive integer : ";
    if ((cin >> x == false) || (x<=0))//error detection : if receive a character,a negative number or zero , program is terminated.
    {
        if(x<=0)
        cout << "Input can't be negative or zero.";
        else
        cout << "Invalid input.";
        return 0;
    }
    while(n <= x)//loop from 2 to x (because 1 w'ont be the answer).
    {
        isPrimeFlag = true;
        isFactorFlag = false;
        for( int i = 2 ; i <= sqrt(n) ; i++ )//use this loop to check whether n is a prime number.
            if( n%i == 0 )
            {
                isPrimeFlag = false;
                break;
            }

        if(x%n==0)//used for check whether n is a factor of x.
            isFactorFlag = true;

        if(isPrimeFlag && isFactorFlag)
            cout << n << " ";
        n++;
    }

    return 0;
}
