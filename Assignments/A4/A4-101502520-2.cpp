#include <iostream>
using namespace std;


/*


此作業是正常顯示二進位數字


*/

void Show_Binary(int);

int main()
{
    int decimalnum,trash;//trash用來存取cin的非整數部分

    cout<<"Enter a positive integer : ";
    if(cin>>decimalnum==false)
    {
        cout<<"Invalid input.";
        return -1;
    }

    trash=cin.get();
    if(trash!=10)//檢查輸入數字之後是不是換行(ASCII 10)
    {
        cout<<"Invalid input.";
        return -1;
    }

    if(decimalnum<0)
    {
        cout<<"Input can't be negative.";
        return -1;
    }

    cout<<"Binary of "<<decimalnum<<" : ";
    if(decimalnum==0)
        cout<<"0";//如果輸入是0就輸出0
    else
        Show_Binary(decimalnum);
    return 0;
}

void Show_Binary(int binarynum)
{
    int counter=0;
    int temp;

    temp=binarynum;
    while(temp!=0)//先計算總共需要除幾次
    {
        temp=temp/2;
        counter++;
    }
    for(int i=counter;i>0;i--)
    {
        temp=binarynum;
        for(int j=1;j<i;j++)//從需要除最多次的開始，再依序往下印出各個除以2的餘數
            temp=temp/2;
        cout<<temp%2;
    }

    return;
}

