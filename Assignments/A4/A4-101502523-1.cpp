#include <iostream>
#include <cmath>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer,a=2;
    bool isPrimeFlag;//判斷是否為質數依據(true or false)

    cout << "Enter an positive integer : ";
    if( cin >> integer == false || integer < 0 )//負數與符號偵錯
    {
        if( integer < 0 )//負數偵錯
            cout << "Input can't be negative or zero." << endl;
        else //符號偵錯
            cout << "Invalid input." << endl;
        return -1;
    }
    cout << "positive prime divisors of " << integer << " : ";
    while( a <= integer )//從2檢驗至integer之間的質數
    {
        isPrimeFlag = true;
        for( int i = 2 ; i <= sqrt( (float) a ) ; i++  )//判斷是否為質數
            if( a%i == 0 )
            {
                isPrimeFlag = false;
                break;
            }
        if( isPrimeFlag )//如果為質數
            if( integer % a == 0 )//判斷是否為integer的質因數(整除)
                cout << a << " ";
        a++;
    }
    return 0;
}
