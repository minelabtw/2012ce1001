#include <iostream>

using namespace std;

int main()
{
    /*定輸入值、餘數、計算值、總值*/
    int number,mod;
    int count = 1 ;
    int total = 0 ;
    cout << "Enter a positive integer : ";
    /*輸入非數字則錯誤*/
    if ( cin >> number == false )
    {
        cout << "Invalid input. " ;
        return 0;
    }
    /*輸入負數則錯誤*/
    if ( number < 0 )
    {
        cout << "Input can't be negative. " ;
        return 0;
    }
    cout << "Binary of " << number ;
    /*設定一個迴圈若輸入的值不論計算與否>0則進行
      餘數等於輸入值/2的餘數
      總值等於原本的+上餘數*計算值(預設1)
      輸入值/2且計算值*10                         */
    for (; number >0;)
    {
        mod = number % 2;
        total = total + mod * count;
        number = number /2;
        count = count *10;
    }
    cout <<  " : " << total ;
    return 0;
}
