#include <iostream>

using namespace std; //下面可以省略"std::"

int main()
{
    int a,b,c,d;//設定變數,a=輸入值,b=a/2的餘數,c=10的倍數,d=總和
    c = 1; //c一開始為10的0次方
    d = 0; //總和一開始為零
    cout << "Enter a positive integer : "; //輸出文字
    if (cin >> a == false || a < 0)
    {
        if (a<0) //如果a<0,顯示下列文字
            cout << "Input can't be negative." << endl;
        else//如果a為非字元,顯示下列文字
            cout << "Invalid input." << endl;
        return -1;
    }
    cout << "Binary of " << a << " "; //輸出文字 如果把這串用到最後的話a會一直=0 ...
    while (a>0)
    {
        b=a%2; //算出餘數等於多少,在*10的倍數
        d = d +b*c;
        a = a / 2; //a/2繼續計算
        c = c * 10; // c*10讓下一個餘數*10的下一個次方數

    }
    cout<<"is :" << d << endl; //輸出總和

    return 0;
}
