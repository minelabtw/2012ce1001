#include <iostream>
#include<cmath>
using namespace std;

int main()
{
    int a,b=2;
    bool c;
    cout << "Enter an positive integer : ";
    if(cin >> a==false ||a<=0)//錯誤偵測
    {
        if(a<=0)
            cout <<"Input can't be negative or zero."<<endl;
        else
            cout <<"Invalid input."<<endl;
        return 0;
    }
    cout <<"positive prime divisors of "<<a<<" : ";
     while(b<=a)
    {
        c=true;
        if(b==1)
            c=false;
        else
            for( int i=2,end=sqrt(b);i<=end;i++)//先偵測是不是質數
                if(b%i==0)
                {
                    c=false;
                    break;
                }
        if(c&& a%b==0)//是質數又是a的因數就可以輸出
          cout <<b<<" ";
        b++;
    }
    return 0;
}
