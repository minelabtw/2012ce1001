#include<iostream>
#include <cstdlib>
using namespace std;

int main()
{
    int a,b,c,total;
    total=0;
    b=1;
    cout<<"Enter a binary number: ";
    cin>>a;
    if(cin==false||a<0)//不能輸入英文數字或負數.
    {
        if(a<0)//輸入負數的結果
        {
            cout<<"Input can't be negative."<<endl;
            return -1;
        }
        else//輸入非數字的字元的結果
        {
             cout<<"Invalid input."<<endl;
             return -1;
        }
    }
        if(a>0)
        {
            while(a>=0)//轉換二進位成十進位
            {
                c=(a%10);
                if(c!=1&&c!=0)//餘數不能等於除了1和0的其他數字
                {
                    cout<<"Can't be binary input."<<endl;
                    return -1;
                }
                c=c*b;//使數字乘於對應的二的次方
                a=a/10;
                b=b* 2;
                total = total+c;
                if(a==0)//當a變到0的時候,讓程式出來
                break;
            }

        }
        cout<<"Decimal is: " <<total<<endl;

return 0;

}
