#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int l,t,sum=0;

    //輸入
    cout<<"Enter an positive integer : ";
    cin>>l;

    if(l<=0)
    {//輸入小於0或等於0,顯示錯誤訊息
        cout<<"Input cannot be negative or zero .";
        return -1 ;
    }
    if(!cin)
    {//輸入錯誤時,顯示錯誤訊息
        cout<<"Invalid input . ";
        return -1 ;
    }
    for(int u=1;u<=l;u++)
    {//判斷所有正質因數
        if(l%u==0)
        {
            for(int i=1;i<=u;i++)
            {
                t = u % i;
                if(t==0)
                {
                    sum = sum + 1 ;
                }
            }
            if(sum==2)
            {//輸出
                cout<<u<<" ";
            }
            sum=0;
        }
    }
    return 0 ;
}
