#include <iostream>
#include <math.h>
using namespace std; 

int main()
{
	int numc=0;
	int k=0;
	int counter=0;
	int in;
	
	cout<<"Enter an positive integer : ";
	
	if(cin>>in==false)
	cout<<"Invalid input.";

	else if(in<=0)
	cout<<"Input can't be negative or zero.";
	
	else

	cout<<"positive prime divisors of "<<in<<" : "; 
	for(counter=1;counter<=in;counter++)
	{
		
		
		if(in%counter==0)//是否為因數
		{//counter是in的因數
			for(numc=1;numc<=counter;numc++)//counter是否為質數
			{
				if(counter%numc==0)
					k=k+1;
			}
			if(k==2)//是質數
			{
				cout<<counter<<" ";
				k=0;//k值初始化
			}
			else
				k=0;//k值初始化
		}
	}
	return 0;
}
