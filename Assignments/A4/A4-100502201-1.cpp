#include<iostream> //Enable cin cout function
#include<cmath> //Enable sqrt function
using namespace std;

int main()
{
    int a,i,j; //"a" is what we input, i and j are used for loop function
    bool b; //Used for telling the prime number
    cout<<"Enter an positive integer : "; //@TA:Shouldn't that be "Enter a positive integer" ?
    while (cin>>a==false) //When input is not integer, tell the user that it's invalid
    {
        cout<<"Invalid input.";
        return -1;
    }
    while (a<1) //"a" can't be negative or zero; otherwise we can't calculate
    {
        cout<<"Input can't be negative or zero.";
        return -1;
    }
    if (a>0)
    {
        if (a==1) //Number 1 doesn't has any prime divisors
        {
            b=false;
            cout<<"No positive prime divisors found.";
            return -1;
        }
        else
        {
            cout<<"positive prime divisors of ";
            cout<<a;
            cout<<" : ";
            for (i=2;i<=a;i++) // Tell the divisors (except 1)
            {
                if (a%i==0)
                {
                    for (j=2;j<=sqrt(i);j++) //Tell the prime numbers
                    {
                        if (i%j==0)
                        {
                            b=false;
                            break;
                        }
                    }
                    if (b)
                    {
                        cout<<i<<" ";
                    }
                }
            }
        }
    }
    return 0;
}
