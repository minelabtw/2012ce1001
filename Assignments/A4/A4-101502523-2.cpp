//********************//
//加分題              //
//十進位轉二進位正確版//
//********************//
#include <iostream>
#include <cmath>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int decimal,n,i=0;

    cout << "Enter a positive integer : ";

    if( cin >> decimal == false || decimal < 0 )
    {
        if( decimal < 0 )//to avoid negative input
        {
            cout << "Input can't be negative." << endl;
            return -1;
        }
        else //to avoid character (such as "abc")
        {
            cout << "Invalid input." << endl;
            return -1;
        }
    }

    cout << "Binary of " << decimal << " : ";

    if( decimal == 0 )//if input is 0, the binary is 0.
        cout << "0";

    while( decimal > 0 )//change decimal to binary
    {
        while( decimal < pow(2,i+1) && i>=0 )//當decimal小於2的i+1次方時，且i大於等於0
        {
            cout << "1";
            decimal = decimal - pow(2,i);//decimal減掉2的i次方(such as 72=200-2^7)
            i = i - 1;//次方減1(為降一個2的次方作檢驗↓)
            n = i;
            while( decimal < pow(2,n) && n>=0 )//decimal如果小於2的n次方時(such as 8<2^4=16 that output "0")
            {
                cout << "0";
                i--;
                n--;
            }
        }
        i++;
    }
    return 0;
}
