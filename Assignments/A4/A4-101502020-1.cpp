//A4-1 by J
//with nothing but basic function
#include <iostream>
#include <cmath>
using namespace std ;
int main()
{
    int inp=0 , cr1=2 , cr2=2 ;
    //inp as input , cr as checker : cr1 for checking a divisor , cr2 for checking a prime

    cout << "Enter an positive integer : " ;
    if ( cin>>inp==false )      //check for false input
    {
        cout << "Invalid input." ;
        return -1 ;
    }
    else if ( inp<=0 )      //check for none-positive input
    {
        cout << "Input can't be negative or zero." ;
        return -1 ;
    }

    cout << "positive prime divisors of " << inp << " : " ;

    while ( cr1<=sqrt(inp) )     //major function loop
    {
        if ( inp%cr1==0 )       //if cr1 is a factor of inp , check that cr1 is a prime or not
        {
            cr2=sqrt(cr1) ;
            while ( cr2>1 )     //cr2's work starts here
            {
                if ( cr1%cr2!=0 )       //if cr2 isn't a factor of cr1 , check the next number smaller
                    cr2-- ;
                else        //if cr1 has a factor(cr2) , i.e. cr1%cr2==0 , we don't have to check anymore
                    break ;
            }

            if ( cr2==1 )       //cr2==1 means that cr1 has no factor besides 1 and cr1 , so it's a prime
                cout << cr1 << " " ;

            cr2=2 ;     //to make cr2 go back to the original for starting next check
        }

        cr1++ ;     //to check the next number larger
    }

    return 0 ;

}
