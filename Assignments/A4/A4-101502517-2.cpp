//A4-2：將十進位轉成2進位(不加分版)
#include<iostream>
using namespace std;
int main()
{
    int a;
    cout<<"Enter a positive integer : ";
    if((cin>>a==false)||(a<0))
    {//偵錯，若輸入為負或字元
        if(a<=0)//偵錯，若輸入為負
            cout<<"Input can't be negative or zero."<<endl;
        else//偵錯，若輸入為字元
            cout<<"Invalid input."<<endl;
        return -1;//回傳-1
    }
    for(int c=1;a>0;a=a/2)//當a大於0時進入回圈，每次處理完時a除以2
    {
        cout<<a%2;//輸出a除以2的餘數
    }
    return 0;//回傳0
}
