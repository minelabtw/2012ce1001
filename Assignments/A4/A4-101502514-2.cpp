#include <iostream>
using namespace std;
int main()

{
    int i,j;//宣告整數型態變數
    cout << "Enter a positive integer : ";
    if((cin>>i==false) || (i<0))//如果輸入的i不符合整數型態或是小於0
    {
        if(i<0)
            cout << "Input can't be negative." ;//小於等於零輸出Input can't be negative.
        else
            cout << "Invalid input.";//型態錯誤則輸出Invalid input.
        return -1;//回傳-1結束
    }
    cout <<"Binary of "<<i<<" : ";
    while(i>0)//當i大於0進入迴圈
    {
        j=i%2;//取i/2的餘數存到j裡面
        i=i/2;//每次執行i/2
        cout << j;//把j印出
    }
    return 0;//回傳
}
