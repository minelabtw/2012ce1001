// Assignment #4-1
// A4-976001044-1.cpp
#include <iostream>
#include <cstdlib>
#include <cmath>

// 定義一個用來設定2的冪次方的值作為邊界值 
// 對應程式內宣告的變數(整數)，此處設預為20，預設可輸入的值之範圍：0 ~ 2^20-1 
// 此值最大可為 31，可輸入的值之範圍：0 ~ 2147483647 (2^31-1) 
#define boundary 20.0

long gcd(long,long);    // 宣告副程式 gcd， 輸入引數為兩個長整數 
bool PrimeTest(long);   // 宣告副程式 PrimeTest， 輸入引數為一個長整數 
using namespace std;

int main()
{
    long input=0;

    cout << "Enter an positive integer : ";

    if ( cin >> input != false )  // 判斷 input 若"不為"字元，為有效輸入
    {
         // 判斷 input 在數值上是否為有效輸入，並輸出對應的錯誤訊息
         if ( input < 1 )
         {
              cout << "Input can't be negative or zero." << endl;
         }
         else if ( input > pow(2.0,boundary)-1 )
         {
              cout << "Input value too large.\n"
                   << "Change input value,\n" 
                   << "or the boundary value in procedure."
                   << endl;
         } 
         else  // input 為有效輸入 
         {
              cout << "positive prime divisors of " << input << " :";
              
              // 完整掃描 1~ input 的每個數值 
              for ( long i=1 ; i <= input ; i++ )
              {
                  // 若該數值為質數，且不與 input 互質
                  // 則該數為 input 的質因數，判斷式成立，則將該值輸出 
                  if ( (PrimeTest(i) == true) && (gcd(input,i) != 1) )
                  {
                       input = input / i;   // 避免執行過多次數的迴圈 
                       cout << " " << i;
                  }
              }
              cout << endl;
         }
    }
    else     // 判斷 input 若"為"字元，則為無效輸入，輸出錯誤訊息
    {
         cout << "Invalid input." << endl;
    }

    system("pause");
    return 0;
}

// 布林數型態之副程式 PrimeTest
// 測試任一符合 main() 之中的輸入條件的數字是否為質數
// 傳回值為一個布林數 
bool PrimeTest(long number)
{
     // 設定 flag 標籤之初始狀態為真 
     bool flag=true;
     
     // 若輸入引數為 1， 則該數部為質數 
     if ( number == 1 )
     {
          flag = false;
     }
     else
     {
         // 若輸入引數不為 1 且為質數，
         // 則該引數皆不整除此引數開根號到 2 的範圍內的任意整數 
          for (long i=2 ; i <= long(sqrt(double(number))) ; i++ )
          {
              // 若範圍內有任何一個滿除整除該引數的數 
              // 則將標籤值設為否後直接跳出迴圈 
              if ( number % i == 0 )
              {
                   flag = false;
                   break;
              }
          }
     }
     // 直接回傳該引數是否為質數 
     return flag;
}

// 長整數型態的副程式 gcd
// 計算兩個引數的最大公因數，傳回值為長整數型態的最大公因數
// 計算方法為：輾轉相除法
//   (x,n)=(n,r) , r = x mod n
//   gcd(x,n)=(x,n)=(n,r)=...=(r',0)=r'
long gcd(long input, long test_number)
{
    long r=1;

    while ( test_number != 0 )
    {
          r = input % test_number;
          if ( r == 0 )
          {
               break;
          }
          input = test_number;
          test_number = r;
    }

    return test_number;
}
