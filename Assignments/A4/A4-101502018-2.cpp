#include <iostream>

using namespace std;

int main()
{
    int a,b;
    cout <<"Enter a positive integer : ";
    if(cin >>a==false ||a<0)//錯誤偵測
    {
        if(a<0)
            cout <<"Input can't be negative."<<endl;
        else
            cout <<"Invalid input."<<endl;
        return 0;
    }
    if(a>0)
    {
        cout <<"Binary of "<<a<<" : ";
        while(a>=1)//10進位轉2進位
        {
            b=a%2;
            a=a/2;
            cout <<b;
        }
    }
    return 0;
}
