#include <iostream>

using namespace std;

int main()
{
    int integer,a,b,x;//設定變數
    bool prime;//判斷是否為質數之變數
    cout << "Enter an positive integer : ";//第一行顯示的字串
    cin >> integer;//輸入第一個變數

    if(cin==false)//錯誤偵測，若變數不屬於數字
        {
            cout << "Invalid input." << endl;//要顯示的字串
            return -1;//到此即可結束
        }

    if(integer<=0)//錯誤偵測，若變數小於等於0
        {
            cout << "Input can't be negative or zero." << endl;//要顯示的字串
            return -1;//到此結束
        }

    if(integer>0)//若變數符合大於0此條件
    {
        cout << "positive prime divisors of " << integer << " : ";//先顯示此字串

        int a=2,b=0;//初始化
        while(a<=integer)//此為找出所有小於integer的因數，a設置為因數
        {
            prime=true;//初始化
            b=integer%a;//b為integer除以a之餘數
            if(b==0)//若b等於0，即可判斷出a為integer之因數
            {
                for(int x=2;x<a;x++)//此迴圈用來判斷是否為質數
                    if(0==a%x)//若因數a除以x等於0
                    {
                        prime=false;//即判斷此數不屬於質數，為false
                        break;//即跳出迴圈
                    }
                if(prime)//若判斷為質數
                    cout << a << " ";//即輸出因數a
            }
            a++;//a繼續+1判斷因數
        }
    }

    return 0;
}
