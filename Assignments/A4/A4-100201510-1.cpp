#include <iostream>
#include <math.h>

using namespace std ;

int main()
{
    int number , divisor = 2 ;//"number" will later entered by user
    double fdivisor ;

    cout << "Enter an positive integer : ";

    if(cin >> number == false)//detect illigle input
    {
        cout << "Invalid input." << endl;
        return -1;
    }
    else if(number <= 0)
    {
        cout << "Input can't be negative or zero.";
        return -1;
    }
    else
    {
        cout << "positive prime divisors of " << number << " : ";

        while(divisor <= number)
        {
            if(number%divisor == 0)//check the divisor is prime or not , copy and modified from quiz last time
            {
                int i = 0 ;
                int checker = 2 ;
                fdivisor = divisor ;
                while(checker <= sqrt(fdivisor))
                {
                    if(divisor%checker == 0)
                    {
                        i++;
                    }
                    checker++;
                }
                if(i == 0)
                {
                    cout << divisor << " ";
                }
            }//end if
            divisor++;
        }//end while
        return 0 ;
    }
}
