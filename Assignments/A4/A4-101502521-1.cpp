#include <iostream>
#include <math.h>

using namespace std;

bool is_prime(int n);

int main()
{
    int n;
    char check;
    cout << "Enter a positive integer : ";

    if(!(cin >> n)||n<0)                            // Error detect
    {
        if(n<0)
            cout << "Input can't be negative.\n";
        else
            cout << "Invalid input.\n";
        return -1;
    }

    check = cin.get();
    if(check!=' ' && check!='\n' && check!=0)       //Detect invalid char after integer, like "123a"
    {
        cout << "Invalid input.\n";
        return -1;
    }

    if(n==1||n==0)
        cout << "number " << n << " don't have any positive prime divisors\n";
    else
    {
        cout << "positive prime divisors of " << n << " : ";

        for(int i=2;i<=n;i++)
            if(n%i==0 && is_prime(i))               //如果是因數 且 也是質數
                cout << i << " ";
    }

    return 0;
}

bool is_prime(int n)
{
    if(n==0 || n==1)                                //0 1 都不是質數
        return false;
    for(int i=2;i<=sqrt(n);i++)                     //sqrt(n)取根號n
        if(n%i==0)
            return false;                           //若能被整除 即不是質數 直接回傳
    return true;                                    //都不能被<=跟號n的數整除 即為質數
}

