#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <string.h>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer=0;
    int a=0;                
    int i=0;
    float t=0;
     
    cout << "Enter a positive integer : ";       
    cin >> integer ; 
    
    if (cin==false)                        //error message for input isn't a number.
    {
        cout << "Invalid input.\n";
        
        system ("PAUSE");
        return 0;             
    }
    if (integer<0)                        //error message for input is negative.
    {
        cout << "Input can't be negative.\n";
        
        system ("PAUSE");
        return 0;
        
    }
    
    i=integer;
    cout <<"Binary of "<< integer <<" : "; 
    while(i/2!=1)                          //loop for Decimal transfer to binary number
    {        
        if (i%2==0)
        {   
            cout <<"0";
        }
        
        if (i%2==1)
        {
            cout<<"1";
            i=i-1;
        }
        i=i/2;              
        a=a+1;
    } 
    
    if (i%2==1)                           //when i/2 == 1 , the loop be over , we have to count the last two power
    {
        cout<<"1";
    }
    
    if (i%2==0)
    {   
        cout<<"0";
    }
    
    cout<<"1";          
    
    system("PAUSE");
    return 0;
}    
    
                                      
