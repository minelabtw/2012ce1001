#include<iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int a,b,c,n;
    cout<<"Enter a positive integer : ";
    cin>>n;
    if(cin==false||n<0)
    {
        if(cin==false)                //不可輸入非數字.
        cout<<"Invalid input."<<endl;
        else                          //不可輸入負數.
            cout<<"Input can be negative or zero."<<endl;
        return -1;
    }
    cout<<"positive prime divisors of "<<n<<" : ";
    for(int a=2;a<=n;a++)         //求n的因數.
    {
        if(n%a==0)                //若a為n的因數,以下開始求n的質因數.
        {
            c=0;                  //變數.
            for(b=2;b<a;b++)      //求a的因數.
                {
                    if(a%b==0)    //若b為a的因數,因數個數加1.
                    c++;
                }
             if(c==0)             //若無因數,則為質因數.
                cout<<b<<" ";
        }
    }

return 0;
}

