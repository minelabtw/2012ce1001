#include <iostream>
#include <math.h>

using namespace std;


int main()
{
    int x,i,j;//x為輸入值,i為x之因數,j為i之因數,k為j之個數,所以當k之值為0時,i為質數,i就為x的質因數
    int k=0;
    int l=0;//l為i的個數

    cout << "Enter an positive interger: " ;
    cin >> x;

    if( x <= 0 || x == false )
    {
        if( x <= 0 )
            cout << "Input can't be negative or zero." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }
    cout << "positive prime divisors of " << x << " : ";

    for( i=2 ; i<x ; i++)
    {
        if( x%i ==0)//判斷b是否為a之因數,再確認是否為質數
        {
            l=0;
            k=0;//初始值為0
            for( j=2 ; j<i ; j++)
            {
                if( i%j==0 )
                k++;//若是可整除,則因數的個數加1,但此因數就不是質數
            }
            if( k==0 )
            cout << i << " " ;
            l++;
        }
    }
    if( l==0 )
    cout << x << " has no positive prime divisor.";


    return 0;
}
