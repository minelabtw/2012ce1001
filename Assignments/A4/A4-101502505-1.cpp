#include <iostream>
#include <cmath>
using namespace std;//使用名稱空間std下的名稱
int main()
{
    int a,b,c = 0;
    cout << "Enter an positive integer : ";
    cin >> a;

    if ( cin == false )//非數字
        cout << "Invalid input.";
    else if ( a <= 0 )
        cout << "Input can't be negative or zero.";
    else if ( a == 1 )//錯誤偵測 1不為質數
        return 0;
    else
    {
        cout <<"positive prime divisors of "<< a << " : ";
        for( b=2; 1 < a ; b++ )//迴圈
        {
            if( a%b == 0 )//找出a的因數
            {
                if( c == 0 )//上面有設必成立
                {
                    cout << b;//找到最大因數
                    c = 1;//跳出
                }
                else
                    cout << " " << b;
                while( a % b == 0 )
                    a /= b;//連除找出質因數
            }
        }
    }
    return 0;//回歸0
}
