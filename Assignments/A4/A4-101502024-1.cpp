#include <iostream>
#include<cmath>
using namespace std;

int main()
{
    int x,y=2;
    bool prime;
    cout << "Enter an positive integer : ";//Show line.
    if( cin >> x == false || x <= 0 )//Judge if the number entered is valid ,negetive, zero or invalid.
    {
        if( x <= 0 )//The case that x <= 0.
            cout << "Input can be negative or zero." << endl;
        else//The case that the number entered is invalid.
            cout << "Invalid input." << endl;
        return -1;
    }
    cout << "Positive prime divisors of " << x << " : ";//show the first part of line.
    while( y <= x )//Process after entering a valid number.
    {
        prime = true;
        if( y == 1 )
            prime = false;
            else
            for( int i = 2 , end = sqrt( y ) ; i <= end ; i++ )
                if( y % i == 0 )//Take out the numbers that are not prime number.
                {
                    prime =false;
                    break;
                }
        if( prime && x % y == 0 )//Show only the number that matches the both conditions.
          cout << y <<" ";
        y++;
    }
    return 0;
}
