#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int n1,n2,n3=0,n4=0,n5;
    cout<<"Enter a positive integer : ";
    if (cin>>n1==false||n1<0)
    {
        if (n1<0)
          cout<<"Input can't be negative. ";
        else
          cout<<"Invalid input.";
        return -1;
    }//the progrom before line 16 is used to debug.
    n5=n1;
    while (n1>=1)
    {
        n2=n1%2;//after this step n1 isn't n1/2.
        n1=n1/2;//let n1 become n1/2.
        n3=n3+n2*pow(10,n4);//to count the final answer.
        n4++;
    }
    cout<<"Binary of "<<n5<<" : "<<n3;
    return 0;
}
