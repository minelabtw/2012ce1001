#include<iostream>
using namespace std;
int main()
{
    int number,remainer,total=0,i=1;
    cout<<"Enter a positive integer : ";
    cin>>number;
    if(cin==false)//偵錯若為字元則輸出
    {
        cout<<"Invalid input."<<endl;
        return -1;
    }
    if(number<0)//偵錯若為負數則輸出
    {
        cout<<"Input can't be negative."<<endl;
        return -1;
    }
    cout<<"Binary of "<<number<<" is : ";
    while(number>=1)//判斷是否大於等於一若成立則進入迴圈
    {
        remainer=number%2;
        total=total+remainer*i;//將輸入之十進位用一個數字轉成二進位
        i=i*10;
        number=(number-remainer)/2;
    }
    cout<<total<<endl;//輸出所要求之二進位
    return 0;
}
