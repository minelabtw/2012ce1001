//Enter a positive integer and change it to binary number
#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int num;//decimal number entered by user
    int i = 0;//the power of 10
    int sum = 0;//change decimal to binary

    cout << "Enter a positive integer : ";

    if( cin >> num == false || num <= 0)//check input is invalid input or negative
    {
        if( num < 0 )//num can't be negative
        cout << "Input can't be negative." << endl;
        else////num can't be invalid input
        cout << "Invalid input." << endl;
        return -1;
    }//if end

    cout << "Binary of " << num << " : ";

    while( num > 0 )//num is positive
    {
        sum += ( num % 2 )*pow( 10,i );//change the decimal to binary
        num = ( num - num % 2 )/ 2;//num/2
        i++;//i+1
    }//while end

    cout << sum << endl;//output the binary

    return 0;
}//main end
