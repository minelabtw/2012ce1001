#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int integer;
    cout << "Enter a positive integer : ";//輸出一個問句


    if(cin >> integer == false)//若輸入一個字元
    {
        cout << "Invalid input." << endl;//則無法輸出答案
        return -1;
    }
    if( integer < 0 )//若輸入一個負數
    {
        cout << "Input can't be negative." << endl;//則無法輸出答案
        return -1;
    }
    if(integer > 0)//若輸入一個正數
    {
        cout << "Binary of " << integer << ": ";
        while (integer != 0)//若integer不等於0,則做while迴圈
        {
            cout << integer%2;
            integer = integer/2;
        }
        return 0;
    }

}
