#include <iostream>
using namespace std;

int main()
{

    int i,t;
    //輸入
    cout<<"Enter a positive integer : ";
    cin>>i;

    if(i<0)
    {//輸入小於0或等於0,顯示錯誤訊息
        cout<<"Input cannot be negative .";
        return -1 ;
    }
    if(!cin)
    {//輸入錯誤時,顯示錯誤訊息
        cout<<"Invalid input . ";
        return -1 ;
    }
    for(int u=2;i>0;)
    {//將十進位換成二進位
        cout<<i%u;
        i = i / 2;
    }
    return 0;
}
