//有做加分題!!

#include <iostream>
using namespace std;

int main()
{
    int number,number1,n=1,sum1=0;//設定變數

    cout << "Enter a positive integer : ";
    if ( cin >> number == false || number < 0)//判斷是否為負數以及字元
    {
        if ( number < 0)//負數時輸出結束
        {
            cout << "Input can't be negative.";
            return 0;
        }
        else//字元時結束
        {
            cout << "Invalid input.";
            return 0;
        }
    }
    number1 = number;//保留輸入的數字
    while ( number != 0)
    {
        sum1 += (number%2)*n;//加總轉換的結果
        number = number/2;//計算下一位
        n = n*10;//讓輸出結果不顛倒,所以依序乘以10
    }
    cout <<"Binary of "<<number1<<" : "<<sum1<<endl;//輸出結果

    return 0;
}
