#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int interger ;
    int A=2 ;
    bool B=true ; //B是一個非0的數
    int C ;

    cout << "Enter an positive integer : " ;

    if (cin>>interger==false) //如果輸入的不是數字
    {
        cout << "Invalid input." << endl ;
        return 0 ;
    }

    if (interger>0)
    {
        cout << "positive prime divisors of " <<  interger << " : " ;
        while (A<=interger)
        {
            if (interger%A==0) //如果輸入的數字除以A的餘數是0
            {
                for (int i=2 ; i<=sqrt((float)A) ; i++) //當i小於等於A的開根號，就執行下列程序
                {
                    if (A%i==0)
                    {
                        B=false ;
                        break; //強制結束for迴圈跳至第36行
                    }
                }
                if (B)
                    cout << A << " " ;
            }
            A++ ; //A=A+1
        }
    }

    else //如果A不大於0
    {
        cout << "Input can't be negative or zero." << endl ;
        return 0 ;
    }

    cout << endl ;

    return 0 ;
}
