# include<iostream>

using namespace std;

int main()
{
    int integer;

    cout << "Enter an positive integer : ";

    if((cin>>integer==false)||(integer<=0))//排除字元,0和負數
    {
        if(integer<=0)
        {
            cout << "Input can't be negative or zero.";
            return 0;
        }
        else
        {
            cout << "Invalid input.";
            return 0;
        }
    }

    for(int i=2;i<=integer;i++)
    {


        if((integer%i)==0)//進入此迴圈之i為因數
        {
            bool flag=true;
            for(int j=2;j<i;j++)//判斷因數i是否為質數
            {
                if((i%j)==0)
                {
                    flag=false;
                    break;
                }
            }
            if(flag)
            {
            cout << i << " ";
            }
        }
    }
}
