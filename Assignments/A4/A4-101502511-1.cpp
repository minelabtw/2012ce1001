#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int number;//設定變數
    bool isprimeflag;//變數

    cout << "Enter an positive integer : ";
    if (cin >> number ==false || number <=0)//判斷是否為字元及負數或等於零
    {
        if ( number <=0 )//小於等於0時,結束程式
        {
            cout << "Input can't be negative or zero.";
            return 0;
        }
        else//字元時結束程式
        {
            cout << "Invalid input.";
            return 0;
        }
    }
    cout << "positive prime divisors of "<<number<<" : ";

    for ( int j=2; j<=number; j++)//尋找質數
    {
        isprimeflag=true;//為了讓迴圈結束時,使isprimeflag恢復成true
        if( number == 1 )//輸入為1的話結束程式
        {
            isprimeflag=false;
            return 0;
        }
        else
            for (int i=2; i<=sqrt(j); i++)//判斷是否為質數
            {
                if (j%i==0)//j%i==0時,j非質數,isprimeflag為false
                {
                    isprimeflag=false;
                    break;
                }
            }
        if (isprimeflag and number%j==0 )//輸出number的質因數
            cout<<j<<" ";//　↑判斷質數是否為number的因數
    }
    return 0;
}
