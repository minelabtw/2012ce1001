// 101502503
#include<iostream> // allows program to output data to the screen

using namespace std ;

// funtion main begins program execution
int main ()
{

    int decimal, a=0, b=1, c=0, sum=0 ; // variable declarations

    cout << "Enter a positive integer : " ;

    if ( cin >> decimal == false || decimal < 0 )
    {
        if ( decimal<0 ) // 如果decimal小於0
            cout << "Input can't be negative." << endl ; // display result
        else
            cout << "Invalid input." << endl ; // display result
        return -1 ;
    } // end if

    else
        cout <<"Binary of "<< decimal << " : ";
        while ( decimal > 0 ) // 當decimal大於0,做下面指令
        {
             a = decimal%2 ; // a為decimal除以2的餘數
             c = a*b ; // c為a乘以b
             sum += c ; // sum等於c+sum
             decimal = decimal/2 ; // decimal等於decimal除以2
             b = b*10 ; // b等於b乘以10

        } // end while
        cout << sum << endl ; // display result

    return 0 ; // indicate that program ended successfully
}// end main
