#include <iostream>

using namespace std;

int main()
{
    int num;
    cout <<"Enter a positive integer : ";
    if (cin>>num==false || num<0) //error detection : if receive a character or a negative number , program is terminated.
    {
        if (num<0)
        {
            cout <<"Input can't be negative.";
            return -1;
        }
        else
        {
            cout <<"Invalid input.";
            return -1;
        }
    }
    cout <<"Binary of "<<num<<" : ";
    do{
        if (num%2==1)
        cout <<1;
        else
        cout <<0;
        num/=2;
        if (num==1) //除到最後商式等於1時,要先輸出1,否則回圈結束,無法輸出最後一個1
        cout <<1;
    }while (num/2!=0) ;
    return 0;
}
