#include <iostream>
using namespace std;

// 程式由此開始
int main()
{
    int a,d,e; // 假設變數
    int b = 2;

    cout << "Enter an positive integer : "; // display message

    if ( cin >> a == false ) // 如果a不等於整數，則顯示錯誤訊息，結束並回報
    {
        cout << "Invalid input.";
        return -1;
    }
    if ( a < 0 )
    {
        cout << "Input can't be negative or zero."; // 如果a小於或等於0，則顯示錯誤訊息，結束並回報
        return -1;
    }

    for ( ; a >= b ; b++ ) // 如果符合條件a小於等於b且b遞增時，進入迴圈
    {

        if ( a % b == 0 ) // 當b可以整除a
        {
            int f = 0; // 變數f等於0
            d = b;

            for ( int c = 2; d > c; c++ ) // 如果符合條件c等於2，d大於c且c遞增時，進入迴圈
            {
                if ( d % c == 0 ) // 如果c可以整除d，f等於1
                {
                    f = 1;
                }
            }
            if ( f == 0 ) // 如果f的值等於0，則輸出d
                cout << d << " ";
        }
    }
    return 0; // 結束並回報
}
