#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    int num1;
    bool num2 = true;//宣告 假設num2是質數
    int i;
    int j;

    cout << "Enter an positive integer : ";

    if ( cin >> num1 == false || num1 <= 0 )
    {
        if ( num1 <= 0 )
            cout << "Input can't be negative or zero." << endl;//不可為負數或零
        else
            cout << "Invalid input." << endl;//不符合的輸入
    }//end if


    else if ( num1 > 0 )
    {
        cout << "positive prime divisors of " << num1 << " : ";

        for ( i=2 ; i <= num1 ; i++ )//判斷輸入值的因數
        {
            num2 = true;//避免第二個for迴圈break掉之後,所產生的錯誤
            if ( num1 % i == 0 )
            {
                for ( j=2 ; j <= sqrt( (float)i ) ; j++ )//判斷輸入值的因數是否為質數
                {
                    if ( i % j == 0 )
                    {
                        num2 = false;//因數不是質數
                        break;
                    }//end if
                }//end for

                if(num2)
                    cout << i << " ";
            }//end if
        }//end for

    }//end else if

    return 0;
}//end main
