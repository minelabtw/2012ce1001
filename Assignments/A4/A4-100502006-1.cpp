/*  重複輸入
    輸入 1 ∼2147483647 的正整數
    輸入 -1 結束程式
    輸入大於 2147483647 的整數會擋掉，以防止之後運算溢位
    若任何錯誤輸入，程式皆會擋掉(ex: -abc123, 567def, 10.675, -123, 0, 大於 2147483647 的整數)
*/
#include <iostream>
#include <string>
#include <cstdlib>
#include <cmath>
using namespace std;

class CPrimeDiviser//定義 CPrimeDiviser類別
{
    public:
        void PrimeDiviser();//質因數函式
    private:
        bool Input();//輸入函式
        unsigned int iInteger;//用以儲存輸入整數
        bool PrimeNumber(int a);//質數判斷函式
};
void CPrimeDiviser::PrimeDiviser()//定義質因數函式
{
    while(Input());//呼叫輸入函式
    cout<<"positive prime divisors of "<<iInteger<<" : ";
    for(int i=2;i<=iInteger;i++)//判斷是否為因數
    {
        if(iInteger%i==0)
        {
            if(PrimeNumber(i))//呼叫質數判斷函式
            {
                cout<<i<<" ";
            }
        }
    }
    cout<<endl;
}
bool CPrimeDiviser::Input()//定義輸入函式
{
    string strInput;
    cout<<"Enter an positive integer : ";
    cin>>strInput;
    if(strInput=="-1")//若輸入為 -1，結束程式
    {
        exit(0);
    }
    if(strInput.c_str()[0]!=45)//判斷首字是否為負號
    {
        if( strInput=="0" )//判斷輸入是否為 0
        {
            cout<<"Input can't be negative or zero."<<endl;
            return true;
        }
        for(int j = 0; j < strInput.length(); j++)//逐一檢查字串字元
        {
           if( strInput.c_str()[j] < '0' || strInput.c_str()[j] > '9' )//逐一檢查字串所有字元是否為數字
           {
               cout<<"Invalid input."<<endl;
               return true;
           }
        }
        iInteger=strtoul(strInput.c_str(),NULL,10);//將字串型別轉換為整數儲存
        if(iInteger>2147483647)//輸入數字必須小於等於2147483647，否則之後運算會溢位
        {
            cout<<"Input must be in the range of 0 ∼2147483647."<<endl;
            return true;
        }
        return false;//回傳 false
     }
     else
     {
       for(int j = 1; j < strInput.length(); j++)//逐一檢查字串第一字元後字元
        {
           if( strInput.c_str()[j] < '0' || strInput.c_str()[j] > '9' )//逐一檢查字串第一字元後字元是否為數字
           {
               cout<<"Invalid input."<<endl;//顯示輸入錯誤
               return true;
           }
        }

       cout<<"Input can't be negative or zero."<<endl;
       return true;
     }
}
bool CPrimeDiviser::PrimeNumber(int a)//定義質數判斷函式
{
    for(int j=2;j<=sqrt(a);j++)//判斷是否為質數
    {
       if(a%j==0)
       {
           return false;
       }
    }
    return true;
}
int main()
{
    CPrimeDiviser PD;//命名CPrimeDiviser類別
    while(1)
    {
        PD.PrimeDiviser();//呼叫類別中質因數函式
    }
    return 0;
}
