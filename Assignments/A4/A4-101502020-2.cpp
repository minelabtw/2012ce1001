//A4-2 by J
/*It's a plus version. The main idea is to find n so that 2^(n-1)<input<2^n
After finding n , cout the digits in a different order from the biggest.*/
#include <iostream>
using namespace std ;
int main()
{
    int inp=0 , n=1 ;       //inp for input , n for the power of 2.

    cout << "Enter a positive integer : " ;

    if ( cin>>inp==false )      //check for letter input
    {
        cout << "Invalid input." ;
        return -1 ;
    }
    else if ( inp<0 )       //check for negative input
    {
        cout << "Input can't be negative." ;
        return -1 ;
    }

    cout << "Binary of " << inp << " : " ;

    //a loop for finding n
    for ( n=1 ; inp/n>1 ; n*=2)       //this for loop can judge n for situation below : 2^(n-1)<inp<2^n
    {
        //I just try to write a for loop ,because I seldom do a for loop.
    }

    while ( n>1 )       //This loop is the major function containing how to cout digits correctly
    {
        cout << inp/n ;     //ex:200/128=1
        inp%=n ;        //ex:200%128=72
        n/=2 ;      //for the next n , ex:128/2=64
    }

    cout << inp/n ;     //This statement is for the fact that I don't know how to cout the last digit in the former while loop

    return 0 ;

}
