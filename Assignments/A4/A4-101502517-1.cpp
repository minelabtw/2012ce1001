//A4-1：尋找質因數
#include<iostream>
using namespace std;
int main()
{
    int a,d;
    cout<<"Enter an positive integer : ";
    if((cin>>a==false)||(a<=0))//偵錯，若輸入為負或零或字元
    {
        if(a<=0)//偵錯，若輸入為負或零
            cout<<"Input can't be negative or zero."<<endl;
        else//偵錯，若輸入為字元
            cout<<"Invalid input."<<endl;
        return -1;//回傳-1
    }
    for(int b=2;b<=a;b++)//尋找因數
    {
        if(a%b==0)//若為因數
        {
            d=0;//判斷數歸零
            for(int c=2;c<b;c++)//判斷此因數是否為質數
            {
                if(b%c==0)//此因數若有因數，非質數
                {
                    d=1;//讓判斷數不等於0
                    break ;//跳出確認因數是否為質數的迴圈
                }
            }
            if(d==0)//若判斷數為0，則為因數中的質數
            {
                cout<<b<<"  ";//輸出此數
            }
        }
    }
    return 0;//回傳
}
