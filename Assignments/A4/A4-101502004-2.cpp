#include <iostream>

using namespace std;

int main()
{
    int integer,a=0,b=1,total=0;//設定變數
    cout << "Enter a positive integer : ";//第一行要顯示的字串
    cin >> integer;//輸入變數

    if(cin==false)//錯誤偵測，輸入非數字
    {
        cout << "Invalid input.";//輸出字串
        return -1;//到此結束
    }

    if(integer<0)//錯誤偵測，輸入負數
    {
        cout << "Input can't be negative.";//輸出字串
        return -1;//到此結束
    }

    cout << "Binary of " << integer << " : ";//先輸出此字串
    while(integer!=0)//迴圈開始將十進位轉成二進位，限制在integer不等於0
    {
        a=integer%2;//a為integer除以2之餘數
        total+=a*b;//total是把a乘上b之總和，當作位數計算
        b*=10;//b一直乘上十
        integer/=2;//integer除以2後之商再回去做迴圈
    }
    cout << total;//最後輸出total

    return 0;
}
