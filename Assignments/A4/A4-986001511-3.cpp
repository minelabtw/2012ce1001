/*****************************************
* Assignment 4
* Name: �d�u��
* School Number: 986001511
* E-mail: lu19900213@gmail.com
* Course: 2012-CE1001
* Submitted: 2012/10/18
*
***********************************************/
/*****************************************
*
* find the NORMAL BINARY of a decimal number,
* example: 29's binary is 11101, the program will show 11101
*
***********************************************/

#include<iostream>
#include<cstdlib>
#include<math.h>

using namespace std;

int main(){

    int num1,temp;
    float num,c;

    cout << "Enter a positive integer : " ;
    cin >> num1;

    // assign a new variable for initial number.
    num=num1;

    // if th number is non-number(characters)
    if(num==1972411956){
        cout << "Invalid number." << endl;
       }
    // if the number is negative
    else if (num <= 0 ){
        cout << "Input can't be negative or zero." << endl;
    }

    // if it is a normal number
    else{
        //find the most great index the number has.
        for (float i=0;i<10;i++){
        if ( (num<pow(2,i+1)) && (num >=pow(2,i)) ){
        c=i;
        }
        }

        cout << "Binary of " << num1 << " : " ;

        //if the number greater than 0
        while (num > 0){

            // if the numebr is greater than the index of 2.
            if( (num>=pow(2,c))){
            // output the result and assign a new value replace the old one and do loops
            cout << "1";
            num=num-pow(2,c);
            // decrease the index and try again
            c=c-1;
            }
            // if the number is smaller
            else{
            // output the result 0, and decrease the index value and try again the loop
            cout << "0";
            c=c-1;
            }
        }
    }
    //end program
    cout << endl;
    system("pause");
    return 0;
}
