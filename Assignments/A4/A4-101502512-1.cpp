#include<iostream>
#include<cmath>
using namespace std;
int main()
{
    int l=2,i;

    cout << "Enter an positive integer : ";

    if( cin >> i == false || i < 0 )//不屬於正數
    {
        if( i < 0 )
            cout << "Input can be negative." << endl;
        else
            cout << "Invalid input." << endl;//無效的數字
        return -1;
    }
    if(i==1)
    {
        cout << "1 doesn't have positive prime divisor.\n";
    return 0;
    }

    cout << "positive prime divisors of " << i <<" : ";
    while(l <= i)
    {
        bool p;
        p = true;
        if(i == 1)
        {
            p = false;
            return 0;
        }
        else
            for(int j=2; j <= sqrt(l); j++)
                if( l%j == 0 )
                {
                    p = false;
                    break;
                }
        if( p and i % l == 0)
            cout << l <<" ";
            l++;
    }
    cout<<endl;
    return 0;
}


