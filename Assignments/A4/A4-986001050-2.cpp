//input : integer , decimal input
//output : integer , binary of input
#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int decimal;
    int mod;

    cout << "Enter a positive integer: ";

    if( cin >> decimal == false )// prompt for and read in a decimal number
    {                           // and check whether input is not integer.
        cout << "Invalid input." << endl;
        return -1;
    }

    if( decimal < 0 )// check input is negative
    {
        cout << "Input can't be negative." << endl;
        return -1;
    }
    cout << "Binary of " << decimal << " : ";
    while ( decimal != 0 )// convert to binary equivalent
    {
        mod = decimal % 2;
        cout << mod;
        decimal /= 2;
    }

    return 0;
}
