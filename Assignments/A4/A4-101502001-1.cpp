#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int integer , a;
    bool isPrimeFlag ;



    cout << "Enter an positive integer : ";

    if( cin >> integer == false || integer <= 0 )
    {
        if( integer <= 0 )
        cout << "Input can't be negative or zero." << endl;

        else
        cout << "Invalid input." << endl;
        return -1;
    }

    else
    cout << "positive prime divisors of " << integer << " : ";

    while ( integer > 0 && a<=integer )
    {
        isPrimeFlag = true;
        a=1;
        integer%a ==0;
        for( int i = 2 , end = sqrt(a) ; i <= end ; i++ )
                if( a%i == 0 )
                {
                    isPrimeFlag = false;
                    break;
                }
                if(isPrimeFlag)

            cout << a << " ";
        a++;
    }

    return 0;
}
