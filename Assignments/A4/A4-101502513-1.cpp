#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    int input ,prime = 2;
    bool isPrimeFlag; //used determine that prime factor of input

    cout<< "Enter an positive integer : ";

    if( cin >> input == false || input <= 0 )
    {
        if( input <= 0 )
            cout<< "Input can't be negative or zero." << endl; //input is greater than zero
        else
            cout << "Invalid input." << endl; //input can't be character
        return 0;
    }

    cout << "positive prime divisors of " << input << " : ";
    while( prime <= input )
    {
        isPrimeFlag = true;

        for( int a = 2 ; a <= sqrt(prime) ; a++ )//check prime of input
        {
            if( prime % a == 0 )
            {
                isPrimeFlag = false;
                break;
            }
        }
        if( input % prime == 0 )//if prime is prime factor of input
        {
            if(isPrimeFlag)
            {
                cout << prime << " ";
            }
        }
        prime++;
    }
    cout << endl;
    return 0;
}
