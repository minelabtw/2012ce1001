#include <iostream>

using namespace std;

int main()
{
    int a;

    cout << "Enter a posituve interger : ";
    cin >> a;
    cout << "Binary of " << a << " : ";

    if( a < 0 || a == false )
    {
        if( a < 0 )
            cout << "Input can't be negative." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }
    if (a == 0)
    {
        cout << a;
        return 0;
    }

    while( a!=0 )//除到小於1時,程式取整數0即跳出迴圈
    {
        if( a%2 == 1)
            cout << "1";
        if( a%2 == 0)
            cout << "0";
        a /= 2;
    }

    return 0;
}
