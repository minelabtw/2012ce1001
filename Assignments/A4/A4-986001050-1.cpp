//input : integer
//output : 該正整數的所有正質因數。
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int integer;
    bool isPrimeFlag ;//isPrimeFlag is used for record whether this number is a prime number.
    int lowerBound = 1 ;

    cout << "Enter an positive integer : ";

    if( cin >> integer == false )// prompt for and read in an integer
    {                           // and check whether input is not integer.
        cout << "Invalid input." << endl;
        return -1;
    }

    if( integer <= 0 )// check input is negative or zero
    {
        cout << "Input can't be negative or zero." << endl;
        return -1;
    }
    cout << "positive prime divisors of " << integer << " : ";

    while( lowerBound <= integer )  //find out the prime numbers.
    {
        isPrimeFlag = true;
        if( lowerBound == 1 )
            isPrimeFlag = false;
        else
            for( int i = 2 , end = sqrt(lowerBound) ; i <= end ; i++ )
            if( lowerBound%i == 0 )
            {
                isPrimeFlag = false;
                break;
            }

        if( isPrimeFlag )           //if it is a prime number, print it out. the integer divide by it.
        {
            if( integer%lowerBound == 0 )
                cout << lowerBound << " ";

            while( integer%lowerBound == 0)
                integer /= lowerBound;
        }
        lowerBound++;
    }
    return 0;
}
