//extra credit problems
//enter -1 to exit

#include<iostream>
#include<stdlib.h>
#include<limits>
using namespace std;


bool inputcheck(int in);


int main()
{
    //initial
    int decimal=0,pow=0;


    while(1)
    {
        //input
        do
        {
            cout<<"Enter a positive integer : ";
            cin>>decimal;
        }while(!inputcheck(decimal));


        //exit
        if(decimal==-1)
            break;


        //compute & output
        cout<<"Binary of "<<decimal<<" : ";

        for(pow=1;;pow*=2)//find the largest 2^n number smaller than input
            if(decimal<pow*2)
                break;
        for(;pow>0;pow/=2)//check if the number include 2^n(which is pow).
            if(decimal>=pow)//if include, put 1.
            {
                cout<<'1';
                decimal-=pow;
            }
            else//if not, put 0.
                cout<<'0';

        cout<<"\n\n";
        //compute & output above
    }


    //end
    system("pause");
    return 0;
}




bool inputcheck(int in)
{
    //exit
    if(in==-1)
        return true;


    cin.clear();//reset cin;
    cin.ignore(numeric_limits<streamsize>::max(),'\n');//clear cin buffer
    if(cin.gcount()!= 1)//not integer
    {
        cout<<"Invalid input.\n\n";
        return false;
    }
    else if(in<0)//negative
    {
        cout<<"Input can't be negative.\n\n";
        return false;
    }
    //no error
    return true;
}
