#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int integer,number=1;
    bool isPrimeFlag;//宣告判斷是否為質數

    cout << "Enter an positive integer : ";
    if ( cin >> integer ==false || integer <= 0 )//判斷integer是否為字元輸入或小於等於0
    {
        if ( integer <= 0 )//判斷integer是否小於等於0
            cout << "Input can't be negative or zero." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }

    cout << "positive prime divisors of " << integer << " : ";

    while ( number <= integer )//當number小於等於integer時進入迴圈
    {
        isPrimeFlag = true;
        if ( number == 1 )//判斷number是否為1
            isPrimeFlag = false;
        else
            for ( int i = 2, end = sqrt( number ); i<=end ; i++ )
                if( number % i ==0 )
                {
                    isPrimeFlag = false;
                    break;
                }
        if ( isPrimeFlag && integer % number==0 )//判斷是否為質數且同時為integer的因數
            cout << number << " ";
            number++;
    }
    return 0;
}
