//In this solution I've corrected wrong output ( meaning 6 will be 110 not 011 ).
#include<iostream>

using namespace std ;

int main ()
{
    int integer , binary , sum = 0 , expotent = 1 ;

    cout << "Enter a positive integer : " ;

    if ( cin >> integer == false )//check wheather input is not integer
    {
        cout << "Invalid input." << endl ;
        return -1 ;
    }

    if ( integer < 0 )//check input is negative
    {
        cout << "Input can't be negative." << endl ;
        return -1 ;
    }

    cout << "Binary of " << integer << " is : " ;


    while ( integer > 0 )
    {
        binary = integer % 2 ;
        integer = integer / 2 ;

        sum = sum + binary * expotent ; //to let it be a correct binary
        expotent = expotent * 10 ;
    }

    cout << sum ;

    return 0 ;
}
