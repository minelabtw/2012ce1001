# include<iostream>
using namespace std;

int main()
{
    int decimal;

    cout <<  "Enter an positive integer : ";
    if((cin>>decimal==false)||(decimal<0))
    {
        if(decimal<0)
        {
            cout << "Input can't be negative.";
            return 0;
        }
        else
        {
            cout << "Invalid input.";
            return 0;
        }
    }
    while(decimal>0)
    {
        cout << (decimal%2);
        decimal=decimal/2;
    }
    return 0;
}
