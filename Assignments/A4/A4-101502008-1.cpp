#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    int number=1;


    cout<<"Enter an integer : ";
    cin>>number;

    if (number<=0)//錯誤偵測   小於0的不正確
    {
        cout<<"Input can't be negative or zero.";
        return 0;
    }
    if (number==1)//字串與1皆為無意義的輸入
    {
        cout<<"Invalid input.";
        return 0;
    }

    for (int i=2;number>=i;i++)
    {//for up {
        if ((number%i)==0)
        {//if up {
            bool set = true;//設定是否為質數的假設

            for(int j=2;i>j;j++)//對因數做質數判斷
            {//for up {
                if ((i%j) ==0 )//整除的狀況不為質數
                {
                    set = false;
                    break;
                }
            }//for down }
            if(set)//正確時輸出因數
            {
                    cout<<i<<" ";
            }
        }//if down }
    }//for down }


    return 0;
}
