#include <iostream>

using namespace std;//可以不用打std::

int main()
{
    int a,b,c=0; //設變數a=輸入值 b=質因數
    cout << "Enter an positive integer : ";
    if (cin>>a ==false || a<=0)
    {
        if (a<=0) //如果a為負數或等於零,出現下列文字
        {
            cout << "Input can't be negative or zero." << endl;
            return -1;
        }
        else //如果為非字元,輸出下列文字
        {
            cout << "Invalid input." << endl;
            return -1;
        }
    }
    for (b=2;a>1;b++) //因為1不是質數,所以直接從2開始,且數必須大於零
    {
        if(a%b==0)//找出因數
        {
            if (c==0)
            {
                cout << b;
                c =1; //c=1的話將會停止這個if
            }
            else
                cout << " " <<b; //其他則輸入空格
            while (a%b==0)
                a /= b;//除以b在找下一個因數
        }
    }
    return 0;
}
