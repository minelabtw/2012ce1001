#include <iostream>

using namespace std;

const int rows = 5;
const int columns = 5;
int box[rows][columns];
int bingo = 0;//if bingo biger than 0, if means Bingo!

bool input ()// if input is not 1 or 0, return false.
{

    for (int y = 0; y < rows ; y++)
    {
        for (int x = 0; x < columns; x++)
        {
            int a=0;//if input is an integer than a is 1.
            if (cin >> box[y][x])  a=1;

            if ( box[y][x] >1  || box[y][x]<0 || a!=1) return false;
        }
    }
    return true;
}

void determinecolums()  //to determine whether colums is bingo.
{
    for ( int j=0; j<columns ;j++ )
    {
        for ( int i=0; i<rows ;i++ )
        {
            if (box[i][j]==0)
                break;
            else if (i==(rows-1))
            {
                bingo++;
                j=columns;
                break;
            }
        }
    }
}

void determinerows()    //to determine whether rows is bingo.
{
    for ( int j=0; j<rows ;j++ )
    {
        for ( int i=0; i<columns ;i++ )
        {
            if (box[j][i]==0)
                break;
            else if (i==(columns-1))
            {
                bingo++;
                j=rows;
                break;
            }
        }
    }
}

void determinedigonal() //to determine whether digonal is bingo.
{
    for (int i=0; i<rows; i++)
    {
        if (box[i][i]==0)
            break;
        else if (i==(rows-1))
            bingo++;
    }

    for (int i=0; i<rows; i++)
    {
        if (box[i][columns-i-1]==0)
            break;
        else if (i==(rows-1))
            bingo++;
    }
}

int main()
{
    while(input())
    {
        determinerows();
        determinecolums();
        determinedigonal();
        if (bingo != 0)
        {
            cout << "Bingo!" << endl;
            bingo = 0;//return to 0 again
        }
        else
            cout << "You lose!" << endl;
    }
    return 0;
}
