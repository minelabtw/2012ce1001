#include <iostream>

using namespace std;

//兩個常數
const int rows=5;
const int columns=5;

int main()
{
    int array[ rows ][ columns ];
    int round;

    do
    {
        //輸入5*5的陣列
        //如果輸入非數字或非0非1則結束程式
        for (int i=0; i<rows; i++)
        {
            for (int j=0; j<columns; j++)
            {
                if (cin >> array[i][j]==false)
                    return 0;

                if (array[i][j]!=0 && array[i][j]!=1 )
                    return 0;
            }
        }

        round=true;
        if (round)
        {
            //判斷列
            for (int i=0; i<rows; i++)
            {
                for (int j=0; 1==array[i][j]&&array[i][j]==array[i][j+1]; j++)
                {
                    if (j==columns-2)
                        round=false;
                }
                if (round==false)
                {
                    cout << "Bingo!" << endl ;
                    break;
                }
            }

            //如果列没有Bingo才進入判斷行
            if (round)
            {
                for (int j=0; j<columns; j++)
                {
                    for (int i=0; 1==array[i][j]&&array[i][j]==array[i+1][j]; i++)
                    {
                        if (i==rows-2)
                            round=false;
                    }
                    if (round==false)
                    {
                        cout << "Bingo!" << endl;
                        break;
                    }
                }
            }

            //如果行沒有Bingo才進入判斷左上到右下
            if (round)
            {
                for (int i=0,j=0; 1==array[i][j]&&array[i][j]==array[i+1][j+1]; i++,j++)
                {
                    if(i==rows-2&&j==columns-2)
                    {
                        cout << "Bingo!" << endl;
                        round=false;
                    }
                }
            }

            //如果左上到右下沒有Bingo才進入判斷左下到右上
            if (round)
            {
                for (int i=rows-1,j=0; 1==array[i][j]&&array[i][j]==array[i-1][j+1]; i--,j++)
                {
                    if(i==1&&j==columns-2)
                    {
                        cout << "Bingo!" << endl;
                        round=false;
                    }
                }
            }

            //如果都没Bingo才進入輸了
            if (round)
                cout << "You lose!" << endl;
        }
    }while (1);

    return 0;
}

