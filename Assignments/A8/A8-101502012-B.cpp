#include <iostream>
using namespace std;
int main()
{
    int n;
    int check(int,int);
    while(cin>>n)
    {
        int right=0;//判斷是否有賓果
        int bingo[n][n];//用二維陣列方便判斷
        for (int c=0;c<n;c++)
        {
            for(int d=0;d<n;d++)//陣列數字輸入
            {
                cin>> bingo[c][d];
                if (bingo[c][d]!=0&&bingo[c][d]!=1)
                    return -1;
            }
        }
    for (int i=0;i<n;i++)//判斷橫線賓果
    {
        for(int j=0;j<n;j++)
        {
            if(bingo[i][j]==bingo[i][j+1]&&bingo[i][j]==bingo[i][j+2]&&bingo[i][j]==bingo[i][j+3]&&bingo[i][j]==bingo[i][j+4]&&bingo[i][j]==1)
            {
                cout << "Bingo!"<<endl;
                right=1;
            }
        }
    }
    for (int a=0;a<n;a++)//判斷直線賓果
    {
        for(int b=0;b<n;b++)
        {
            if(bingo[a][b]==bingo[a+1][b]&&bingo[a][b]==bingo[a+2][b]&&bingo[a][b]==bingo[a+3][b]&&bingo[a][b]==bingo[a+4][b]&&bingo[a][b]==1)
            {
                cout << "Bingo!" << endl;
                right=1;
            }
        }
    }
    for (int x=0;x<n;x++)//判斷斜線賓果
    {
        for(int y=0;y<n;y++)
        {
            if(bingo[x][y]==bingo[x+1][y+1]&&bingo[x][y]==bingo[x+2][y+2]&&bingo[x][y]==bingo[x+3][y+3]&&bingo[x][y]==bingo[x+4][y+4]&&bingo[x][y]==1)
            {
                cout << "Bingo!" << endl;
                right=1;
            }
        }
    }
    if (right==0)//如果right=0代表沒賓果 顯示你輸了
        cout << "You lose!" << endl;
    }
    return 0;
}
