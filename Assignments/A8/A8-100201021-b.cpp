#include<iostream>

using namespace std;

int main()
{
    int n,bin=0;                        //輸入數字n,產生陣列b[n][n],  bin=1 代表bingo
    while(cin  >> n)
    {
        int b[n][n],cor[4]={0,0,0,0};   //cor為判定用，cor[r]=5 bingo, cor[r]為不同線同時判定
        ////輸入陣列////
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<n;j++)
            {
                cin >> b[i][j];
            if (b[i][j] !=0 && b[i][j]!=1)
                return 0;
            }
        }
        //n<5，不可能bingo
        if(n<5)
            cout << "You lose!" << endl;
        else    //n>=5
        {
            bin = 0;    //bin 歸零
            for(int i=2;i<n-2;i++)
            {
                for(int j=2;j<n-2;j++)
                {
                    //代點 判斷b[2][2]~b[n-3][n-3]所有數字的直橫斜5個(4條線)
                    cor[0]=0,cor[1]=0,cor[2]=0,cor[3]=0;
                    for(int k=-2;k<=2;k++)
                    {
                        if(b[i+k][j]==1)
                            cor[0]++;
                        if(b[i][j+k]==1)
                            cor[1]++;
                        if(b[i+k][j+k]==1)
                            cor[2]++;
                        if(b[i+k][j-k]==1)
                            cor[3]++;
                    }
                    if(cor[1]==5 || cor[2]==5 || cor[3]==5 || cor[0]==5)
                    {
                        bin=1;
                        break;
                    }

                }
                if(bin==1)
                    break;
            }
            /////if b[2][2]~b[n-3][n-3] 尚未bingo
            if(bin!=1)
            {
                //判斷b[0,1][],b[n-2,n-1][],b[][0,1],b[][n-2,n-1]
                for(int i=0;i<2;i++)
                {
                    for(int j=0;j<n-4;j++)
                    {
                        cor[0]=0,cor[1]=0,cor[2]=0,cor[3]=0;
                        for(int k=0;k<=4;k++)
                        {
                            if(b[i][j+k]==1)
                                cor[0]++;
                            if(b[n-2+i][j+k]==1)
                                cor[1]++;
                            if(b[j+k][i]==1)
                                cor[2]++;
                            if(b[j+k][n-2+i]==1)
                                cor[3]++;
                        }
                        if(cor[1]==5 || cor[2]==5 || cor[3]==5 || cor[0]==5)
                        {
                            bin=1;
                            break;
                        }
                    }
                    if(bin==1)
                        break;
                }
            }
            ////
            if(bin==1)
                cout << "Bingo!" << endl;
            else
                cout << "You lose!" << endl;
        }
    }
    return 0;
}
