#include <iostream>
using namespace std;

int main()
{
    int n,flag,check;//declare the size variable of the matrix, and two flag to decide how to continue this program, flag is to let the computer knows that there is a bingo situation, check is to let the computer knows that user is lose
    for (int k=0;k>=0;k++)
    {
        if (cin >> n==false)//if user input the wrong size of the matrix, the program will end
        return -1;
        int bingo[n][n];//set the matrix size to the input n
        for (int i=0;i<=n-1;i++)
        {
            for (int j=0;j<=n-1;j++)
            {
                if (cin >> bingo[i][j]==false||bingo[i][j]!=1&&bingo[i][j]!=0)//if the elements is not an integer or 0 or 1, this program will end
                return -1;
            }
        }
        flag=0;
        check=0;//reset two flag variables to 0
        for (int i=0;i<=n-1;i++)
        {
            for (int j=0;j<=n-1;j++)
            {
                if(bingo[i][j]==1)//check the whole matrix whether it is accord the bingo situation, if there is a 1 element, check whether the horizontal lin, vertical line, oblique lines are all 1. If it is, display the "bingo" message.

                {
                    if (bingo[i][j+1]==1&&bingo[i][j+2]==1&&bingo[i][j+3]==1&&bingo[i][j+4]==1)
                    {
                        cout << "Bingo!" << endl;
                        flag=1;//if there is any "Bingo!" situation, set both the "flag" and "check" to 1, when the flag is 1, the flowing judgment will break this checking loop 'cause the user is already win, there is no need to check other situation
                        check=1;//if the "check" variable is 1, it can prevent the flowing decide, that display a "You lose!" message.
                        break;
                    }
                    if(bingo[i+1][j]==1&&bingo[i+2][j]==1&&bingo[i+3][j]==1&&bingo[i+4][j]==1)
                    {
                        cout << "Bingo!" << endl;
                        flag=1;
                        check=1;
                        break;
                    }
                    if(bingo[i+1][j+1]==1&&bingo[i+2][j+2]==1&&bingo[i+3][j+3]==1&&bingo[i+4][j+4]==1)
                    {
                        cout << "Bingo!" << endl;
                        flag=1;
                        check=1;
                        break;
                    }
                    if(bingo[i-1][j-1]==1&&bingo[i-2][j-2]==1&&bingo[i-3][j-3]==1&&bingo[i-4][j-4]==1)
                    {
                        cout << "Bingo!" << endl;
                        flag=1;
                        check=1;
                        break;
                    }
                }
            }
            if (flag==1)//falg is 1, the user is already win, break all the checking loop, and redo the program
            break;
        }
        if (check==0)//if the "check" is still 0, that represent there is no "Bingo!" situation in this matrix, display the "You lose!" message
        cout << "You lose!" << endl;
    }
    return 0;
}
