#include<iostream>
using namespace std;

int main()
{
    int n,x,y,p,q;
    while(cin>>n && n>0)
    {
        x=0;
        y=0;
        p=0;
        q=0;
        int bingo[n][n];
        for (int rows=0 ; rows<n ; rows++) //輸入n乘n的陣列
        {
            for (int columns=0 ; columns<n ; columns++)
                {
                    cin >> bingo[rows][columns];
                    if (bingo[rows][columns]!=0 && bingo[rows][columns]!=1)
                        return -1;
                }
        }

        for (int rows=0 ; rows<n ; rows++) //判斷橫的賓果
        {
            int test=0;
            for (int columns=0 ; columns<n ; columns++)
            {
                if (bingo[rows][columns]==1)
                    test++;
                else if (bingo[rows][columns]==0)
                    test=0;
                if (test==5)
                    break;
            }
            if (test==5) //有賓果
            {
                x=1;
                break;
            }
        }

        for (int columns=0 ; columns<n ; columns++) //判斷直的賓果
        {
            int test=0;
            for (int rows=0 ; rows<n ; rows++)
            {
                if (bingo[rows][columns]==1)
                    test++;
                else if (bingo[rows][columns]==0)
                    test=0;
                if (test==5)
                    break;
            }
            if (test==5) //有賓果
            {
                y=1;
                break;
            }
        }

        for (int rows=0 ; rows<=n-5 ; rows++) //判斷左上至右下的斜線賓果
        {
            int test,save1;
            for (int columns=0 ; columns<=n-5 ; columns++)
            {
                int save2=columns;
                test=0;
                save1=rows;
                for (; save2<n ; save2++)
                {
                    if (bingo[save1][save2]==1)
                        test++;
                    else if (bingo[save1][save2]==0)
                        test=0;
                    save1++;
                    if (test==5) //有賓果
                        break;
                }
                if (test==5)
                    break;
            }
            if (test==5)
            {
                p=1;
                break;
            }
        }

        for (int rows=n-1 ; rows>=4 ; rows--) //判斷左下右上的斜線賓果
        {
            int test,save1;
            for (int columns=0 ; columns<=n-5 ; columns++)
            {
                int save2=columns;
                test=0;
                save1=rows;
                for (; save2<n ; save2++)
                {
                    if (bingo[save1][save2]==1)
                        test++;
                    else if (bingo[save1][save2]==0)
                        test=0;
                    save1--;
                    if (test==5) //有賓果
                        break;
                }
                if (test==5)
                    break;
            }
            if (test==5)
            {
                q=1;
                break;
            }
        }

        if (x==1 || y==1 || p==1 || q==1)
            cout << "bingo!" << endl;
        else if (x==0 && y==0 && p==0 && q==0)
            cout << "You lose!" << endl;
    }

    return 0;
}
