#include<iostream>
using namespace std;
int main()
{
    while(true)
    {
        int a;                                                   //幾成幾的陣列.
        int k = 0;                                               //拿來判定賓果或失敗的變數.
        cin >> a;
        if(cin == false || a < 0)                                //不能輸入負數,非數字的字元.
        {
            return 0;
        }
        int five[a][a];                                          //輸入陣列.
        for (int i = 0; i < a; i++)
        {
            for (int o = 0; o < a; o++)
            {
                int b;
                cin >> b;
                if(cin == false || b < 0 || 1 < b)
                {
                    return 0;
                }
                five[i][o] = b;
            }
        }
        for(int i = 0; i <= a - 4; i++)                           //判斷.
        {
            for(int o = 1; o <= a - 4;o++)
            {
                if(five[i][o] == 1 && five[i+1][o+1] == 1 && five[i+2][o+2] == 1
                   && five[i+3][o+3] == 1 && five[i+4][o+4] == 1)                   //斜線.
                {
                    cout << "Bingo!" << endl;
                    k = 1;
                    break;
                }
                if(five[a-o][i] == 1 && five[a-o-1][i+1] == 1 && five[a-o-2][i+2] == 1
                   && five[a-o-3][i+3] == 1 && five[a-o-4][i+4] == 1)               //逆斜線
                {
                    cout << "Bingo!" << endl;
                    k = 1;
                    break;
                }
                if(five[i][o] == 1 && five[i][o+1] == 1 && five[i][o+2] == 1
                   && five[i][o+3] == 1 && five[i][o+4] == 1)                       //橫線.
                {
                    cout << "Bingo!" << endl;
                    k = 1;
                    break;
                }

                if(five[o][i] == 1 && five[o+1][i] == 1 && five[o+2][i] == 1        //直線.
                   && five[o+3][i] == 1 && five[o+4][i] == 1)
                {
                    cout << "Bingo!" << endl;
                    k = 1;
                    break;
                }
            }
            if(k == 1)                                                              //因為k=1已經賓果,所以在break一次.
            {
                break;
            }
        }
        if(k != 1)                                                                  //k沒有等於1,代表都失敗.
        {
            cout << "You lose!" << endl;
        }
    }
    return 0;
}

