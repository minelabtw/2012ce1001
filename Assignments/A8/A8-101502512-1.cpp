#include <iostream>
using namespace std;

int main()
{
    int sum1;//在橫排上為1的數
    int sum2;//在直排上為1的數
    int sum3;//在斜排上為1的數(左上到右下)
    int sum4;//在斜排上為1的數(左下到右上)
    int a[5][5];

    for(;;)//重複輸入
    {
        for(int i=0; i<5; i++)//陣列的輸入
        {
            for(int j=0; j<5; j++)
            {
                if(cin>>a[i][j]==false)//錯誤偵測
                    return 0;

                if(a[i][j]!=1 and a[i][j]!=0)
                    return 0;
            }
        }

        for(int i=0; i<5; i++)//檢查橫排為1的數
        {
            sum1=0;
            for(int j=0; j<5; j++)
                if(a[i][j]==1)
                    sum1++;
            if(sum1==5)//bingo
                break;
        }

        for(int i=0; i<5; i++)//檢查直排為1的數
        {
            sum2=0;
            for(int j=0; j<5; j++)
                if(a[j][i]==1)
                    sum2++;
            if(sum2==5)
                break;
        }

        sum3=0;
        for(int i=0; i<5; i++)//檢查斜排為1的數(左上到右下)
        {
            if(a[i][i]==1)
                sum3++;
        }

        int j=0;
        sum4=0;
        for(int i=4; i>=0; i--)//檢查斜排為1的數()左下到右上
        {
            if(a[i][j]==1)
                sum4++;
            j++;
        }

        if(sum1==5 or sum2==5 or sum3==5 or sum4==5)
            cout<<"Bingo!"<<endl;
        else
            cout<<"You lose!"<<endl;
    }
    return 0;
}
