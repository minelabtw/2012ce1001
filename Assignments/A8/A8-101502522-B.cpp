#include <iostream>
using namespace std;
int main()
{
    int n;
    while(cin >> n && n>0) //輸入陣列
    {
        int a[n][n],t=0,flag=0; //陣列,計算1的次數,有Bingo的標記

        for(int i=0 ; i<n ;  i++)
            for(int j=0 ; j<n ; j++)
            {
                cin >> a[i][j];
                if(a[i][j]!=1 && a[i][j]!=0) //偵錯非0,1輸入
                    return -1;
            }
        for(int v=0 ; v < n ; v++) //檢查橫向Bingo
            for(int h=0 ; h < n ; h++)
            {
                for(int l=0 ; a[v][h+l]==1 ; l++,t++)
                    if(t==4)
                        flag++;
                t=0;
            }
        for(int h=0 ; h < n ; h++) //檢查直向Bingo
            for(int v=0 ; v < n ; v++)
            {
                for(int l=0 ; a[v+l][h]==1 ; l++,t++)
                    if(t==4)
                        flag++;
                t=0;
            }
        for(int v=0 ; v<n ; v++) //檢查左上到右下的Bingo
            for(int h=0 ; h<n ; h++)
            {
                for(int l=0 ; a[v+l][h+l]==1 ; l++,t++)
                    if(t==4)
                        flag++;
                t=0;
            }
        for(int v=0 ; v<n ; v++) //檢查右上到左下的Bingo
            for(int h=n-1 ; h>=0 ; h--)
            {
                for(int l=0 ; a[v+l][h-l]==1 ; l++,t++)
                    if(t==4)
                        flag=1;
                t=0;
            }
        if(flag==0) //沒Bingo標記就輸了
            cout << "You lose!\n";
        else
            cout << "Bingo!\n";
    }
    return 0;
}
