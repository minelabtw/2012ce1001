#include<iostream>
#include<cstdlib>
using namespace std;

int main()
{
    int array[5][5];//設定一個二維陣列
    while(array>=0)//迴圈不斷運算直到陣列元素小於0跳出
    {
        for(int i=0;i<5;i++)
            for(int j=0;j<5;j++)//這兩個迴圈都是輸入變數
            {
                cin >> array[i][j];

                if(cin==false)//錯誤偵測，輸入字元即跳出
                {
                    return 0;
                    break;
                }
                if(array[i][j]!=0 && array[i][j]!=1)//錯誤偵測，輸入非1,0即跳出
                {
                    return 0;
                    break;
                }
            }

        int bingorow=0;//此變數為計算同行之中有幾個1
        for(int i=0;i<5;i++)//一行行固定後去檢測
        {
            for(int j=0;j<5;j++)//開始檢測列
            {
                if (array[i][j]==1)//當出現1即變數+1
                    bingorow++;
            }
            if(bingorow==5)//滿5個即可判斷有一行已bingo，輸出
                break;
            else
                bingorow=0;//若無則歸0繼續檢測下一行
        }
        if(bingorow==5)
        {
            cout << "Bingo!" << endl;
            continue;
        }

        int bingocolumns=0;//跟上面差不多，只是變成固定列檢視行
        for(int j=0;j<5;j++)
        {
            for(int i=0;i<5;i++)
            {
                if(array[i][j]==1)
                    bingocolumns++;
            }
            if(bingocolumns==5)
                break;
            else
                bingocolumns=0;
        }
        if(bingocolumns==5)
        {
            cout << "Bingo!" << endl;
            continue;
        }

        int ibingo=0,jbingo=0;
        for(int n=0;n<5;n++)//這個迴圈是來檢測斜角的兩種特殊情況
        {
            if(array[n][n]==1)//左上到右下
                ibingo++;
            if(array[n][abs(n-4)]==1)//右上到左下
                jbingo++;
        }
        if(ibingo==5 || jbingo==5)//有5個1即bingo
            cout << "Bingo!" << endl;

        if(bingorow!=5 && bingocolumns!=5 && ibingo!=5 && jbingo!=5)//全部都沒有bingo即輸出
            cout << "You lose!" << endl;
    }
}
