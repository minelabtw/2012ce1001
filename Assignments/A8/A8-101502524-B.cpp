#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int n,a,b,check;
    while(cin>>n && n>0)
    {
        check=0;

        int bingo[n][n];
        int x=0,y=0;

        //輸入
        for(b=0;b<n;b++)
        {
            for(a=0;a<n;a++)
            {
                cin>>bingo[a][b];
                if(bingo[a][b]!=0 && bingo[a][b]!=1)
                {
                    return -1;
                }
            }
        }


        //判斷直行是否有Bingo
        for(x=0;x<n;x++)
        {
            if(bingo[x][y]==1)
            {
                for(y=0;y<n-4;y++)
                {
                    for(int len=0;len<5;len++)
                    {
                        if(bingo[x][y+len]!=1)
                        {
                            break;
                        }
                        if(len==4)
                        {
                            cout<<"Bingo!"<<endl;
                            check = 1;
                        }
                    }
                }
            }
        }
        if(check==1)
        {
            continue;
        }


        x = 0;
        y = 0;

        //判斷橫列是否有Bingo
        for(y=0;y<n;y++)
        {
            if(bingo[x][y]==1)
            {
                for(x=0;x<n-4;x++)
                {
                    for(int len=0;len<5;len++)
                    {
                        if(bingo[x+len][y]!=1)
                        {
                            break;
                        }
                        if(len==4)
                        {
                            cout<<"Bingo!"<<endl;
                            check = 1;
                        }
                    }
                }
            }
        }
        if(check==1)
        {
            continue;
        }

        //判斷是否有右下斜Bingo
        for(x=0;x<n-4;x++)
        {
            for(y=0;y<n-4;y++)
            {
                for(int len=0;len<5;len++)
                {
                    if(bingo[x+len][y+len]!=1)
                    {
                        break;
                    }
                    if(len==4)
                    {
                        cout<<"Bingo!"<<endl;
                        check=1;
                    }
                }
            }
        }
        if(check==1)
        {
            continue;
        }


        //判斷是否有左下斜Bingo
        for(x=n;x>=0;x--)
        {
            for(y=n;y>=0;y--)
            {
                for(int len=0;len<5;len++)
                {
                    if(bingo[x-len][y-len]!=1)
                    {
                        break;
                    }
                    if(len==4)
                    {
                        cout<<"Bingo!"<<endl;
                        check = 1;
                    }
                }
            }
        }

        //沒有Bingo
        if(check==0)
        {
            cout<<"You lose!"<<endl;
        }
    }
    return 0;
}
