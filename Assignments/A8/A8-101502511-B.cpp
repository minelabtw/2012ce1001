#include <iostream>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

int main()
{
    int n;//nxn的方格
    while(cin>>n)//重複輸入
    {
        int a[n][n];
        int b=0,c=0,d=0,e=0,f=0,g=0,h=0,m=0,p=0;
        for(int i=0; i<n; i++)//判斷是否條件題目輸入
        {
            for(int j=0; j<n; j++)
                if(cin>>a[i][j]==false||a[i][j]>1)
                    return 0;
        }
        for(int i=4; i<n-1; i++)//判斷右上往左下,中線上方的線
        {
            int f1=0;
            for(int j=0,k=i; k>=1; j++,k--)
            {
                if(a[j][k]==1&&a[j][k]==a[j+1][k-1])//前項和下一項相比,相同且為1的話加一次
                    f1++;
                if(a[j+1][k-1]==0)//有0的話f1為0
                    f1=0;
                if(f1>=4)//如果f1為4或以上代表有bingo,f加一次
                    f++;
            }
        }
        for(int i=1; i<n-4; i++)
        {
            int h1=0;//右上往左下,下方
            for(int j=i,k=n-1; k>=i+1; j++,k--)
            {
                if(a[j][k]==1&&a[j][k]==a[j+1][k-1])
                    h1++;
                if(a[j+1][k-1]==0)
                    h1=0;
                if(h1>=4)
                    h++;
            }
        }
        for(int i=1; i<n-5; i++)
        {
            int m1=0,p1=0;//m1左上到右下(上方),p1是下方
            for(int j=0,k=i,q=1; k<n-1; j++,k++)
            {
                if(a[j][k]==1&&a[j][k]==a[j+1][k+1])
                    m1++;
                if(a[j+1][k+1]==0)
                    m1=0;
                if(m1>=4)
                    m++;
                if(a[k][j]==1&&a[k][j]==a[k+1][j+1])
                    p1++;
                if(a[k+1][j+1]==0)
                    p1=0;
                if(m1>=4)
                    p++;
            }
        }

        int e1=0,c1=0;
        for(int i=0; i<n-1; i++)//左上往右下,中線
        {
            if(a[i][i]==a[i+1][i+1]&&a[i][i]==1)
                e1++;
            if(a[i+1][i+1]==0)
                e1=0;
            if(e1>=4)
                e++;
            if(a[i][n-1-i]==1&&a[i][n-1-i]==a[i+1][n-2-i])
                c1++;
            if(a[i+1][n-2-i]==0)
                c1=0;
            if(c1>=4)
                c++;
        }
        for (int i=0; i<n; i++)//橫的跟值的
        {
            int b1=0,d1=0;
            for(int j=0; j<n-1; j++)
            {
                if(a[i][j]==1&&a[i][j]==a[i][j+1])
                    b1++;
                if(a[i][j+1]==0)
                    b1=0;
                if(b1>=4)
                    b++;
                if(a[j][i]==1&&a[j][i]==a[j+1][i])
                    d1++;
                if(a[j+1][i]==0)
                    d1=0;
                if(d1>=4)
                    d++;
            }
        }
        if(d>0||b>0||c>0||e>0||f>0||g>0||h>0||m>0||p>0)//只要有一項為bingo就輸出bingo
            cout<<"Bingo!"<<endl;
        else
            cout<<"You lose!"<<endl;
    }
    return 0;
}

