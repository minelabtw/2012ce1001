//A8-B by J
//I think that the basic idea is the same and I want to try to do it by functions.
#include <iostream>
using namespace std;
int i=0 , j=0 , ctr=0 , n ;
int x[500][500]={} ;

int hori()
{
    for (int a=0 ; a<n ; a++ )      //check for every row
    {
        for ( int b=0 ; b<(n-2) ; b++ )
        {
            if ( x[a][b]==1 && x[a][b+1]==1 && x[a][b+2]==1 && x[a][b+3]==1 && x[a][b+4]==1  )
                ctr++ ;
        }
    }
}

int verti()
{
    for (int b=0 ; b<n ; b++ )      //check for every col
    {
        for ( int a=0 ; a<(n-2) ; a++ )
        {
            if ( x[a][b]==1 && x[a+1][b]==1 && x[a+2][b]==1 && x[a+3][b]==1 && x[a+4][b]==1  )
                ctr++ ;
        }
    }
}

int slant1()
{
    for ( int a=0 ; a<n-2 ; a++ )
    {
        for ( int b=0 ; b<n-2 ; b++)
        {
            if ( x[a][b]==1 && x[a+1][b+1]==1 && x[a+2][b+2]==1 && x[a+3][b+3]==1 && x[a+4][b+4]==1 )
                ctr++;
        }
    }
}

int slant2()
{
    for ( int a=0 ; a<n-2 ; a++ )
    {
        for ( int b=0 ; b<n-2 ; b++)
        {
            if ( x[a+4][b]==1 && x[a+3][b+1]==1 && x[a+2][b+2]==1 && x[a+1][b+3]==1 && x[a][b+4]==1 )
                ctr++;
        }
    }
}

int main()
{

    while ( cin>>n )        //deciding what kind of n*n game
    {
        while ( cin>>x[i][j] && ( x[i][j]==0 || x[i][j]==1 ) )      //input each element&0 or 1
        {
            j++ ;
            if ( j==n )     //time to change to the next row
            {
                j=0 ;       //restart column
                i++ ;       //next row
            }

            if ( i==n && j==0 )     //It's time to judge win or lose!
            {
                if ( n>=5)      //4 situation for winning the game
                {
                    hori() ;
                    verti() ;
                    slant1() ;
                    slant2() ;
                }

                if (ctr>0)
                    cout << "Bingo!" << endl ;
                else
                    cout << "You lose!" << endl ;

                i=0 ;
                j=0 ;
                ctr=0 ;
                x[n][n]={} ;        //reset velues for next round!
                break ;     //break to cin the next n*n game
            }
        }
    }

    return 0;       //Thank you for your visit! Program ends!
}
