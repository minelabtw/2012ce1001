#include <iostream>
using namespace std;
void hvdidline (int [], int); // hvdidline function prototype
int line = 0; // line counter
int hcounter = 0, vcounter = 0, dcounter = 0, idcounter = 0; // counter of vertical, horizontal, diagonal, inversed diagonal counter
int time; // size of the game
int main()
{
    do // do while loop to play as many times as you want
    {
        cin >> time; // size of the game
        if (cin == false) // ends the game with a character
            return 0;
        else
        {
            int array [time][time]; // make a game according to the size
            for (int row = 0; row < time; row++) // for loop of row
            {
                for (int column = 0; column < time; column++) // for loop of column
                {
                    cin >> array [row][column]; // enter the game according to the size
                    if (array [row][column] != 1 && array [row][column] != 0) // error if there aren't zeros or ones
                        return -1;
                }
            }
            hvdidline(array[0], time); // apply the function on the made game
            if (line > 0) // if there's any line, bingo
                cout << "Bingo! \n" << endl;
            else // if there's not line, lose
                cout << "You lose! \n" << endl;
        }
        line = 0, hcounter = 0, vcounter = 0, dcounter = 0, idcounter = 0; // zeros the counter to play the next game
    }while (true);
    return 0;
}

void hvdidline(int a[], int b) // hvdidline function definition
{
    for (int i = 0; i < time; i ++) // row
    {
        for (int j = 0; j < time; j++) // column
        {
////////////////////////////////////////////// horizontal line
            if (a[i * b + j] == 1)
                hcounter ++;
            else
                hcounter = 0;
            if (hcounter == 5)
            {
                line ++;
                break;
            }
            if (j % b == b - 1)
                hcounter = 0;
/////////////////////////////////////////////// vertical line
            if (a[j * b + i] == 1)
                vcounter ++;
            else
                vcounter = 0;
            if (vcounter == 5)
            {
                line ++;
                break;
            }
            if (i % b == b - 1)
                vcounter = 0;
/////////////////////////////////////////////// diagonal line
            if (a[j * (b + 1) + i] == 1 || a[(j * (b + 1) + i) + b] == 1)
                dcounter ++;
            else
                dcounter = 0;
            if (dcounter == 5)
            {
                line ++;
                break;
            }
            if (i % (b + 1) == b - 2)
                dcounter = 0;
/////////////////////////////////////////////// inversed diagonal line
            if (a[j * (b - 1) + i] == 1 || a[(j * (b - 1) + i) + b] == 1)
                idcounter ++;
            else
                idcounter = 0;
            if (idcounter == 5)
            {
                line ++;
                break;
            }
            if (i % (b - 1) == b - 2)
                idcounter = 0;
/////////////////////////////////////////////// ends the counter
        }
        if (hcounter == 5 || vcounter == 5 || dcounter == 5 || idcounter == 5)
            break;
    }
}
