#include<iostream>
using namespace std;

void print( int x, int y, int z, int k )//print the final answer
{
    if ( x==5 || y==5 || z==5 || k==5 )
        cout << "Bingo!" << endl;
    else
        cout << "You lose!" << endl;
}

int main()
{
    int array[5][5];
    while(1)
    {
        int a=0,a1=0,b=0,b1=0,c=0,d=0;

        //user input the array
        for (int i=0;i<5;i++)
            for (int j=0;j<5;j++)
            {
                //if input is not 0 or 1, then return 0
                if ( cin >> array[i][j] == false || array[i][j] < 0 )
                    return 0;
                if ( array[i][j] !=0 && array[i][j] != 1 )
                    return 0;
            }

        //if the straight line all equal to 1
        for ( int i=0; i<5; i++)
        {
            a=0;
            b=0;
            for( int j=0; j<5; j++)
            {
                if ( array[i][j] == 1 )
                    a++;//if the row all equal to 1
                if ( array[j][i] == 1 )
                    b++;//if the column all equal to 1
            }
            //find the best line that has most "1"
            if ( a > a1 || b > b1 )
            {
                a1=a;
                b1=b;
            }
            if ( array[i][i] == 1)
                c++;//if a slant line all equal to 1 from left to right
            if ( array[i][4-i] == 1)
                d++;//if a slant line all equal to 1 from right to left
        }

        //print the result
        print(a1,b1,c,d);
    }
    return 0;
}
