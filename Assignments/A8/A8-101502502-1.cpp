#include<iostream>
using namespace std;

int main()
{
    const int count = 5;
    int square[count][count];//宣告一個五x五的方陣

    while(1)//重複輸入
    {
        int rslant = 0;
        int lslant = 0;
        int MaxColumn = 0;
        int MaxRow = 0;
        int MaxSlant = 0;


        for(int i=0 ; i < count ; i++)//以一排一排來做判斷
        {
            int row = 0;

            for(int j=0 ; j < count ; j++)
            {
                if(cin >> square[i][j] == false)//錯誤偵測
                    return 0;

                if (square[i][j] != 0 && square[i][j] != 1)//錯誤偵測
                    return 0;


                if (square[i][j] == 1)//判斷橫排的有幾個1
                    row++;

                if (square[i][j] == 1 && i == j)//判斷左斜的有幾個1
                    lslant++;

                if (square[i][j] == 1 && i + j == 4)//判斷右斜的有幾個1
                    rslant++;
            }


            if (row > MaxRow)//存取橫排的最大值
                MaxRow = row;

            if (lslant > MaxSlant)//存取斜線的最大值
                MaxSlant = lslant;

            if (rslant > MaxSlant)//存取斜線的最大值
                MaxSlant = rslant;
        }


        for (int j=0 ; j < count ; j++)//以一行一行的來做判斷
        {
            int column = 0;

            for (int i=0 ; i < count ; i++)
            {
                if (square[i][j] == 1)//判斷直行有幾個1
                    column++;
            }

            if (column > MaxColumn)//存取直行的最大值
                MaxColumn = column;
        }


        if (MaxColumn == 5 || MaxRow == 5 || MaxSlant == 5)//如果有一橫排或一直行或斜線的就輸出賓果
        {
            cout << "Bingo!" << endl;
        }

        else
        {
            cout << "You lose!" << endl;
        }

    }

    return 0;
}
