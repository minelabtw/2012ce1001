#include <iostream>
using namespace std;

int main()
{
    while(true)//loop for repeat input.
    {
        int n;//size of bingo board.
        bool BingoFlag = false;
        cin >> n;//determine the size of bingo board.
        int board[n][n];

        if(n <5)
        {
            for(int i = 1 ; i<=n*n ; i++)//input data
            {
                cin >> board[i/n][i%n];
                if(board[i/n][i%n] != 0 and board[i/n][i%n] != 1)
                    return 0;
            }
            BingoFlag = false;
        }
        else// if(n >= 5)
        {
            for(int i = 1 ; i<=n*n ; i++)//input data
            {
                cin >> board[i/n][i%n];
                if(board[i/n][i%n] != 0 and board[i/n][i%n] != 1)
                    return 0;
            }

            //check bingo or not
            for(int i = 1 ; i<=n*n ; i++)
            {
                if(board[i/n][i%n] == 1)
                {
                    if(board[i/n][i%n]+board[i/n][i%n +1]+board[i/n][i%n +2]+board[i/n][i%n +3]+board[i/n][i%n +4] == 5 and i%n +4 <= 4)//NS direction
                    {
                        BingoFlag = true;
                        break;
                    }

                    if(board[i/n][i%n]+board[i/n +1][i%n]+board[i/n +2][i%n]+board[i/n +3][i%n]+board[i/n +4][i%n] == 5 and i/n +4 <= 4)//EW direction
                    {
                        BingoFlag = true;
                        break;
                    }

                    if(board[i/n][i%n]+board[i/n +1][i%n +1]+board[i/n +2][i%n +2]+board[i/n +3][i%n +3]+board[i/n +4][i%n +4] == 5 and i%n <= 4)//NE direction
                    {
                        BingoFlag = true;
                        break;
                    }

                    if(board[i/n][i%n]+board[i/n +1][i%n -1]+board[i/n +2][i%n -2]+board[i/n +3][i%n -3]+board[i/n +4][i%n -4] == 5 and i%n >= 4)//SE direction
                    {
                        BingoFlag = true;
                        break;
                    }
                }
            }
        }

        //show result!!
        if(BingoFlag == true)
        {
            cout << "Bingo!";
        }
        else
        {
            cout << "You lose!";
        }
        cout << endl;
    }

    return 0;
}
