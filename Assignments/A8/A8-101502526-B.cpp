#include <iostream>

using namespace std;

int width(int number[100][100],int a)            //這是判斷橫的
{
    int count=0;
    for ( int i = 0 ; i < a ; i++)               //從第一行開始，若有連續5個1則回傳1代表贏
    {                                            //之後跳下一行，且跳行後計算值歸零
        for ( int j =0 ; j < a ; j ++)           //依此類推
        {
            if ( number[i][j] == 1)
                count += 1 ;
            else
                count = 0 ;
            if ( count == 5 )
                return 1;
        }
        count = 0;
    }

}

int side(int number[100][100],int a)              //這是判斷直的
{                                                 //從左邊數來第一列的第一個往下，若連續5個1則回傳1代表贏
    int count=0;                                  //否則跳到下一列，且計算值歸零
    for ( int i = 0 ; i < a ; i++)
    {
        for ( int j = 0 ; j < a ; j ++)
        {
            if ( number[j][i] == 1)
                count += 1 ;
            else
                count = 0 ;
            if ( count == 5 )
                return 1;
        }
        count = 0;
    }

}

int slash(int number[100][100], int a)                     //這是判斷左上到右下的斜線
{
    int count=0;
    for (int k = 0 ; k < a ; k ++)
    {
        for (int l = 0; l < a ; l ++ )
        {
            for (int i = l, j = k; i< a && j <a ; j ++ , i ++)    //先從左上角第一個開始往右下5個，若有連續5個1則回傳1代表贏，每5個判斷完計算值都歸零
            {                                                     //若沒有的話則改成從最上面左邊數來第二個往右下五個判斷，
                if ( number[j][i] == 1)                           //之後橫的做完都沒有，跳下一行，也就是從第二行左邊數來第一個往右下判斷
                    count += 1 ;                                  //以此類推
                else
                    count = 0 ;
                if ( count == 5 )
                    return 1;
            }
            count = 0;
        }
        count = 0;
    }
}

int slash2(int number[100][100], int a)                         //這是右上到左下的
{
    int count=0;
    for (int k = 0 ; k < a ; k ++)
    {
        for (int l = a; l > 0 ; l -- )
        {                                                       //先從最右上的往左下5個判斷，若連續5個1則回傳1代表贏，每五個判斷完計算值歸零
            for (int i = l, j = k; i> 0 && j <a ; j ++ , i --)  //否則往左一行，也就是從最上面右邊數來第二個往左下5個判斷
            {                                                   //一行判斷完後則往下行，也就是第二行右邊第一個開始往左下5個判斷
                if ( number[j][i] == 1)                         //以此類推
                    count += 1 ;
                else
                    count = 0 ;
                if ( count == 5 )
                    return 1;
            }
            count = 0;
        }
        count = 0;
    }
}

int main()
{
    int number[100][100]={},a;
    for ( ; cin >> a ;)                              //重複輸入邊界數
    {
        for ( int i = 0 ; i < a ; i ++)              //依次輸入每行的數字，之後跳行
        {
            for ( int j = 0 ; j < a ; j++)
            {
                cin >> number[i][j] ;
                if ( number[i][j] != 0 && number[i][j] != 1 || cin == false )     // 若不是1和0則結束程式
                    return 0;
            }
        }
        if ( width(number,a) == 1)                    //帶進函式判斷
            cout << "Bingo!" << endl ;
        else if ( side(number,a) == 1)
            cout << "Bingo!"<< endl;
        else if ( slash(number,a) == 1)
            cout << "Bingo!"<< endl;
        else if ( slash2(number,a) == 1)
            cout << "Bingo!"<< endl;
        else                                           // 若都不成立則輸
            cout << "You lose!"<< endl;
    }
    return 0;
}
