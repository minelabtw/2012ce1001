#include <iostream>
#include <cstdlib>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int x[5][5];
    int a=0;

    for (int i=0;i<=4;i++)           //input a 5*5 array.
    {
        for (int j=0;j<=4;j++)
        {
            cin>>x[i][j];

            if ((x[i][j]!=1)&&(x[i][j]!=0))  // input number is not 1 or 0 the program end.
            {
                return 0;
            }
        }
    }

    for (int i=0;i<=4;i++)              //the loop is to determine the first to 5th row
    {                                   //be bingo or not.
        if (x[i][0]==1)                 //ex : 1 1 1 1 1            0 0 0 0 0
        {                               //     0 0 0 0 0            0 0 0 0 0
            for (int j=1;j<=4;j++)      //     0 0 0 0 0            1 1 1 1 1
            {                           //     0 0 0 0 0            0 0 0 0 0
                if (x[i][j]==0)         //     0 0 0 0 0  => bingo  0 0 0 0 0 => bingo
                a=0;

                else
                a=a+1;
            }
            if (a==4)                // a=4 is mean all elements of the row are "1".
            {
                cout<<"Bingo!";
                return 0;
            }
        }
    }

    for (int i=0;i<=4;i++)            //the loop is to determine the first to 5th column
    {                                 //be bingo or not.
        if(x[0][i]==1)                //ex : 1 0 0 0 0            0 1 0 0 0
        {                             //     1 0 0 0 0            0 1 1 0 0
            for (int j=1;j<=4;j++)    //     1 0 0 0 0            0 1 0 0 1
            {                         //     1 0 0 0 0            0 1 0 1 0
                if(x[j][i]==0)        //     1 0 0 0 0 => bingo!  0 1 1 0 0 =>bingo (2nd row)
                a=0;

                else
                a=a+1;
            }
            if (a==4)                //a=4 is mean all elements of the column are "1".
            {
                cout<<"Bingo!";
                return 0;
            }
        }
    }

    if (x[0][0]==1)                             // the if statement is determent the right-up to left-down
    {                                           // be bingo or not.
        for (int i=1;i<=4;i++)                  // ex: 1 0 0 0 0
        {                                       //     0 1 0 0 0
            if (x[i][i]==0)                     //     0 0 1 0 0
            a=0;                                //     0 0 0 1 0
                                                //     0 0 0 0 1   =>bingo!
            else
            a=a+1;
        }
        if (a==4)                               //a=4 is mean all the elements of right-up to left-down are "1"
        {
            cout<<"Bingo!";
            return 0;
        }
    }

    if (x[0][4]==1)                           // the if statement is determent the left-up to right-down
    {                                         // be bingo or not.
        for (int i=1,j=3;i<=4,j>=0;i++,j--)   // ex : 0 0 0 0 1
        {                                     //      0 0 0 1 0
                                              //      0 0 1 0 0
            if (x[i][j]==0)                   //      0 1 0 0 0
            a=0;                              //      1 0 0 0 0 => bingo!
            else
            a=a+1;
        }
        if (a==4)                             //a=4 is mean all the elements of left-up to right-down are "1"
        {
            cout<<"Bingo!";
            return 0;
        }
    }

    cout<<"You lose!";                       // if those loops and statement couldn't be bingo , that means you lose.
    return 0;

}
