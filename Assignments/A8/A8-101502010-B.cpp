#include<iostream>
using namespace std;
int main()
{
    int n;
    while(cin>>n)
    {
        int flag=0,c=0;//flag用來記錄是否bingo，c用來記錄連續幾個1
        int s[n][n];//製作n*n的陣列
        for(int i=0;i<n;i++)//依次輸入陣列內容
            for(int j=0;j<n;j++)
            {
                cin>>s[i][j];
                if(s[i][j]!=1&&s[i][j]!=0)//若輸入非1或0則結束程式
                return 0;
            }
        for(int i=0;i<n;i++)//逐一檢查橫排是否有bingo
        {
            c=0;
            for(int j=0;j<n;j++)
            {
                if(s[i][j]==1)
                    c++;
                else
                    c=0;
                if(c==5)
                    flag=1;
            }
        }
        for(int i=0;i<n;i++)//逐一檢查直排是否有bingo
        {
            c=0;
            for(int j=0;j<n;j++)
            {
                if(s[j][i]==1)
                    c++;
                else
                    c=0;
                if(c==5)
                    flag=1;
            }
        }
        for(int i=0;i<n;i++)//逐一檢查斜排(左上至右下)是否有bingo
        {
            for(int j=0;j<n;j++)
            {
                c=0;
                for(int k=0;k<n;k++)
                {
                    if(s[i+k][j+k]==1)
                        c++;
                    else
                        c=0;
                    if(c==5)
                        flag=1;
                }
            }
        }
        for(int i=0;i<n;i++)//逐一檢查斜排(右上至左下)是否有bingo
        {
            for(int j=0;j<n;j++)
            {
                c=0;
                for(int k=0;k<n;k++)
                {
                    if(s[i+k][j-k]==1)
                        c++;
                    else
                        c=0;
                    if(c==5)
                        flag=1;
                }
            }
        }
        if(flag==1)//若標記為1則有bingo
            cout<<"Bingo!\n";
        else
            cout<<"You lose!\n";
    }
    return 0;//程式結束
}
