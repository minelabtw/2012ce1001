#include<iostream>
using namespace std;

int main()
{
    int check=0;
    int array[5][5];//設一個五乘五的二維陣列
    for(int r=0;r<5;r++)//輸入數字
    {
        for(int c=0;c<5;c++)
        {
            cin>>array[r][c];
            if(array[r][c]!=1 && array[r][c]!=0)//檢驗是否有非零或非一之數字
                return -1;
        }
        cout<<endl;
    }
    for(int r=0;r<5;r++)//若為橫的bingo
    {
        for(int c=0;c<5;c++)
        {
            if(array[r][c]==1)//連成直線
                check++;//計算幾個1連線
        }
        if(check==5)//若五個1連線
        {
            cout<<"Bingo!";
            return 0;
        }
        check=0;
    }
    for(int c=0;c<5;c++)//若為直的bingo
    {
        for(int r=0;r<5;r++)//連成直線
        {
            if(array[r][c]==1)//計算幾個1連線
                check++;
        }
        if(check==5)//若五個1連線
        {
            cout<<"Bingo!";
            return 0;
        }
        check=0;
    }
    for(int rc=0;rc<5;rc++)//若為斜由左上至右下的bingo
    {
        if(array[rc][rc]==1)
            check++;
        if(check==5)
        {
            cout<<"Bingo!";
            return 0;
        }
    }
    check=0;
    for(int rc=0;rc<5;rc++)//若為斜線由右上至左下的bingo
    {
        if(array[5-rc][rc]==1)
            check++;
        if(check==5)
        {
            cout<<"Bingo!";
            return 0;
        }
    }
    if(check!=5)//若無連成直線
        cout<<"You Lose!";
    return 0;
}
