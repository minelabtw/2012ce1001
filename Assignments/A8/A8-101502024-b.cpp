#include <iostream>

using namespace std;

int main()
{
    int SN;//Declare the integrals.

    while (cin >> SN)
    {
        int square[SN][SN];
        bool hue=true;//Reset the bool to true every time.

        for (int i = 0;i < SN;i++)
        {
            for (int j = 0;j < SN;j++)
            {
                int Num;
                cin >> Num;
                if (Num != 1 && Num !=0)
                {
                    return 0;
                }
                square[i][j] = Num;
            }
        }
        for (int i = 0;i < SN;i++)//Formula to judge the inputed array.
        {
            for(int j = 0;j < SN;j++)
            {
                if (square[i][j] == 1 && square[i][j+1] == 1 && square[i][j+2] == 1 && square[i][j+3] == 1 && square[i][j+4] == 1)
                {
                    cout << "Bingo!"<<endl;//Show line.
                    hue = false;
                }
                if (square[i][j] == 1 && square[i+1][j] == 1 && square[i+2][j] == 1 && square[i+3][j] == 1 && square[i+4][j] == 1)
                {
                    cout << "Bingo!"<<endl;//Show line.
                    hue = false;
                }
                if (square[i][j] == 1 && square[i+1][j+1] == 1 && square[i+2][j+2] == 1 && square[i+3][j+3] == 1 && square[i+4][j+4] == 1)
                {
                    cout << "Bingo!"<<endl;//Show line.
                    hue = false;
                }
                if (square[i][j] == 1 && square[i+1][j-1] == 1 && square[i+2][j-2] == 1 && square[i+3][j-3] == 1 && square[i+4][j-4] == 1)
                {
                    cout << "Bingo!"<<endl;//Show line.
                    hue = false;
                }
            }
            if (hue==false)
            {
                break;
            }
        }
        if (hue==false)
            continue;
        else
        {
            cout<<"You lose!"<<endl;//Show line.
            continue;
        }

    }
    return 0;
}
