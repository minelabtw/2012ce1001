#include <iostream>
#include <vector>
using namespace std;

void show_bingo();

int main()
{
    int n;

    //輸入n的數值，若輸入非數字則結束
    if(cin>>n==false)
        return -1;

    //根據輸入的n來建立雙層Vector
    vector<vector<int> > table(n,vector<int>(n));
    for(int i=0;i<table.size();i++)
        for(int j=0;j<table.size();j++)
        {
            //各別輸入Vector的值，若輸入非數字或是非0或非1則跳出
            if(cin>>table[i][j]==false || table[i][j]!=0 && table[i][j]!=1)
                return -2;
        }

    //針對Vector內的每個元素都對他做往右、往下、往右下、往左下的檢查
    for(int i=0;i<table.size();i++)
        for(int j=0;j<table.size();j++)
        {
            int count=0;
            int sum=0;

            //若該元素往右的空間足夠，則把它向右包含自己的五個元素加起來
            while(count<5 && table.size()-j>=5)
            {
                sum=sum+table[i][j+count];
                count++;
            }
            if(sum>=5)
            {
                show_bingo();
                return 0;
            }

            count=0;
            sum=0;

            //若該元素往下的空間足夠，則把它向下包含自己的五個元素加起來
            while(count<5 && table.size()-i>=5)
            {
                sum=sum+table[i+count][j];
                count++;
            }
            if(sum>=5)
            {
                show_bingo();
                return 0;
            }

            count=0;
            sum=0;

            //若該元素往右下的空間足夠，則把它向右下包含自己的五個元素加起來
            while(count<5 && i<=table.size()-5 && j<=table.size()-5)
            {
                sum=sum+table[i+count][j+count];
                count++;
            }
            if(sum>=5)
            {
                show_bingo();
                return 0;
            }

            count=0;
            sum=0;

            //若該元素往左下的空間足夠，則把它向左下包含自己的五個元素加起來
            while(count<5 && i<=table.size()-5 && j>=4)
            {
                sum=sum+table[i+count][j-count];
                count++;
            }
            if(sum>=5)
            {
                show_bingo();
                return 0;
            }
        }
    cout<<"You lose!";
    return 0;
}

void show_bingo()
{
    cout<<"Bingo!";
}
