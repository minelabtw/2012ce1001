////have done bonus/////
#include <iostream>
using namespace std;

bool rowtest(int,int,int[],int);
bool linetest(int,int,int[],int);
bool southeast(int,int,int[],int);
bool northeast(int,int,int[],int);

int count=0;  //count how many numbers does continus "1" have in any directions
int main()
{
    int size;
    while(cin >>size && size>0)  //input array size,if input is not an positive integer,the loop will break
    {
       bool result=false;  // assume no match line in any case at first
       int array[size][size];
       for(int i=0;i<size;i++)
           for(int j=0;j<size;j++)
           {
               cin >>array[i][j]; //input value of each array item
               if(array[i][j]!=0 && array[i][j]!=1 )   //if value is not 0 or 1,the program wil exit
                  return -1;
           }

       for(int i=0;i<size;i++)
       {
           for(int j=0;j<size;j++)  //check from array[0][0]
           {
               if(array[i][j]==1)   //if this point value is 1,check lines in any directions,or go to next point
               {
                  if(rowtest(i,j,array[0],size)==true  ||   //check horizontal line
                     linetest(i,j,array[0],size)==true   ||  //check vectical line
                     southeast(i,j,array[0],size)==true ||  //check northwest-southeast slash
                     northeast(i,j,array[0],size)==true)    //check southwest-northeast slash
                  {
                     cout<<"Bingo!"<<endl;  //if one of this four condition is satisfied,print Bingo
                     result=true;           //and result is true,you win the game,the loop will break
                     break;
                  }
               }
           }
           if(result==true)
               break;
       }
       if(result==false)    //if result is false,you lose the game
           cout<<"You lose!"<<endl;
    }
    return 0;
}

bool rowtest(int a,int b,int w[],int bsize)  //define rowtest function
{
    if(b>=bsize || w[a*bsize+b]==0)  //detect array boundary and the value of item
    {
       count=0;
       return false;
    }
    else
    {
       count++;
       if(count==5)   //if five continus 1 in the direction, it means that match a line
       {
          count=0;
          return true;
       }
       else return rowtest(a,b+1,w,bsize);   //check next point
    }
}

bool linetest(int a,int b,int w[],int asize)  //define linetest function
{
    if(a>=asize || w[a*asize+b]==0)  //detect array boundary and the value of item
    {
       count=0;
       return false;
    }
    else
    {
       count++;
       if(count==5)   //if five continus 1 in the direction, it means that match a line
       {
          count=0;
          return true;
       }
       else return linetest(a+1,b,w,asize);   //check next point
    }
}

bool southeast(int a,int b,int w[],int sesize)  //define southeast function
{
    if(a>=sesize || b>=sesize || w[a*sesize+b]==0)
    {
       count=0;
       return false;
    }
    else
    {
       count++;
       if(count==5)
       {
          count=0;
          return true;
       }
       else return southeast(a+1,b+1,w,sesize);
    }
}

bool northeast(int a,int b,int w[],int nesize)   //define northeast function
{
    if(a<0 || b>=nesize || w[a*nesize+b]==0)
    {
       count=0;
       return false;
    }
    else
    {
       count++;
       if(count==5)
       {
          count=0;
          return true;
       }
       else return northeast(a-1,b+1,w,nesize);
    }
}
