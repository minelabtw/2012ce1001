#include <iostream>
using namespace std;
int main()
{
    int n1;
    cin >> n1;
    if(cin == false || n1 <= 0)
        return 0;
    else
    {
        do
        {
            int n2,o = 0,a[n1][n1];

            for(int i = 0 ; i < n1 ; i++)//將數字存入陣列
            {
                for(int j = 0 ; j < n1 ; j++)
                {
                    cin >> n2;
                    if(n2 == 0 || n2 == 1)
                        a[i][j] = n2;
                    else//非0和1的字元
                        return 0;
                }
            }
            for(int k = 0 ; k < n1-2 ; k++)//第1到5列相加
            {
                int buffer = 0;
                for(int l = 0 ; l < n1-2 ; l++)
                    buffer += a[k][l];

                if(buffer == 5)//判別是否連線
                    o++;
            }
            for(int w = 0 ; w < n1-2 ; w++)//第1到5行相加
            {
                int buffer = 0;
                for(int e = 0 ; e < n1-2 ; e++)
                    buffer += a[e][w];

                if(buffer == 5)//判別是否連線
                    o++;
            }
            for(int q = 0 ; q <= n1-5 ; q++)//slant1負斜率的斜線
            {
                int buffer = 0;
                for(int r = 0 ; r <= n1-5 ; r++)
                {
                    buffer = a[q][r] + a[q+1][r+1] + a[q+2][r+2] + a[q+3][r+3] + a[q+4][r+4];

                    if(buffer == 5)//判別是否連線
                        o++;
                }
            }
            for(int s = n1-1 ; s >= 4 ; s--)//slant2正斜率的斜線
            {
                int buffer = 0;
                for(int t = 0 ; t <= n1-5 ; t++)
                {
                    buffer = a[s][t] + a[s-1][t+1] + a[s-2][t+2] + a[s-3][t+3] + a[s-4][t+4];

                    if(buffer == 5)//判別是否連線
                        o++;
                }
            }
            if(o > 0)
                cout << "Bingo!" << endl;
            else
                cout << "You lose!" << endl;

            cin >> n1;
        }while(cin != false && n1 > 0);
    }
}
