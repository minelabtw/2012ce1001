#include <iostream>
#include <iomanip>
#include <memory.h>
using namespace std;

int main()
{
    int n;

    while ( cin >> n && n > 0 )
    {
        int bingo[n][n], counter=0;
        bool isBingo;
        isBingo = false;

        for ( int i = 0 ; i < n ; i++ )
            for ( int j = 0 ; j < n ; j++ )
            {
                cin >> bingo[i][j];

                if ( bingo[i][j] != 0 && bingo[i][j] != 1 )     //陣列有非0和非1的輸入時結束程式
                    return -1;
            }

        //判斷每一橫列是否有符合賓果條件
        for ( int t = 0 ; t < n ; t++ )
        {
            for ( int s = 0 ; s < n && counter < 5 ; s++ )
            {
                if( bingo[t][s] == 1 )
                    counter++;
                else                    //判斷是否有5個1相連(賓果條件)
                    counter = 0;        //假使 1 1 1 0 0 ，遇到1時counter++，遇到0時counter歸0
            }                           //     →
            if( counter >= 5 )
            {
                counter = 0;
                isBingo = true;//符合賓果條件 → true
                break;         //跳出迴圈
            }
            else
                counter = 0;
        }

        //判斷每一直行是否有符合賓果條件
        for ( int t = 0 ; t < n ; t++ )
        {
            for ( int s = 0 ; s < n && counter < 5 ; s++ )
            {
                if( bingo[s][t] == 1 )  //                                      假使 1↓
                    counter++;          //                                           1
                else                    //判斷是否有5個1相連(賓果條件)               1
                    counter = 0;        //遇到1時counter++，遇到0時counter歸0        0
             }                          //                                           0
            if( counter >= 5 )
            {
                counter = 0;
                isBingo = true;
                break;
            }
            else
                counter = 0;
        }

        //判斷每一斜線是否有符合賓果條件
        for ( int t = 0 ; t < n ; t++ )     //判斷左上右下斜線
        {
            for ( int s = 0 ; s < n && counter < 5 ; s++ )
                if( bingo[t][s] == 1 )
                    for ( int e = s, r = t ; e < n && counter < 5 ; e++ , r++ ) //檢驗方向↘
                    {
                        if( bingo[r][e] == 1 )
                            counter++;
                        else
                        {
                            counter = 0;
                            break;
                        }
                    }
            if( counter >= 5 )
            {
                counter = 0;
                isBingo = true;
                break;
            }
            else
                counter = 0;
        }

        for ( int t = 0 ; t < n ; t++ )     //判斷左下右上斜線
        {
            for ( int s = n-1 ; s >= 0 && counter < 5 ; s-- )
                if( bingo[t][s] == 1 )
                    for ( int e = s, r = t ; e >= 0 && counter < 5 ; e-- , r++ ) //檢驗方向↙
                    {
                        if( bingo[r][e] == 1 )
                            counter++;
                        else
                        {
                            counter = 0;
                            break;
                        }
                    }
            if( counter >= 5 )
            {
                counter = 0;
                isBingo = true;
                break;
            }
            else
                counter = 0;
        }

        //結果輸出
        if ( isBingo == true )
            cout << "Bingo!" << endl;
        else
            cout << "You lose!" << endl;

        //清空bingo陣列
        memset( bingo , 0 , n*n );
    }
    return 0;
}

