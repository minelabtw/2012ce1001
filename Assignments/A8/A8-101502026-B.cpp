/* if the array isn't consistent with the array size entered,then ask the user to re-enter
   if the array entered contains elements that aren't 0 and 1,then ask the user to re-enter  */
#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    int x,p;
    string z;
    while(cin >> x)
    {
        cin.ignore();
        bool check=true,ArrayCheck=true;
        int Array[x][x];

        for(int i=0;i<x;i++)
        {
            getline(cin,z);
            if(z.length()<2*x-1||z.length()>2*x)
            {
                cout << "The array entered isn't qualified,please enter a new array size and a correct array." << endl;
                ArrayCheck=false;
                break;
            }
            else
            {
                int number=z.size(),j=0,n1;
                for(int k=0;k<number;k++)
                {
                    if(isdigit(z[k]))
                    {
                        Array[i][j]=z[k]-48;
                        j++;
                    }
                }
            }
        }

        if(ArrayCheck==true)
        {
            for(int i=0;i<x;i++)
            {
                for(int j=0;j<x;j++)
                {
                    if(Array[i][j]!=0&&Array[i][j]!=1)
                    {
                        cout << "The array entered isn't available,please enter a new and valid one." << endl;
                        ArrayCheck=false;
                        break;
                    }
                }
                if(ArrayCheck==false)
                    break;
            }
        }
        if(ArrayCheck==false)
            continue;
        if(x<5)
        {
            cout << "You lose!" << endl;
            continue;
        }
        for(int i=0;i<x;i++)
        {
            for(int j=0;j<x;j++)
            {
                if(Array[i][j]==1)
                {
                    p=0;
//check horizontal lines----------------------------------------------
                    for(int jreplace=j;jreplace<j+5&&j+4<x;jreplace++)
                        p+=Array[i][jreplace];
                    if(p>=5)
                    {
                        cout << "Bingo!" << endl;
                        check=false;
                        break;
                    }
//--------------------------------------------------------------------
//check vertical lines------------------------------------------------
                    p=0;
                    for(int ireplace=i;ireplace<i+5&&i+4<x;ireplace++)
                        p+=Array[ireplace][j];
                    if(p>=5)
                    {
                        cout << "Bingo!" << endl;
                        check=false;
                        break;
                    }
//--------------------------------------------------------------------
//check slant lines---------------------------------------------------
                    p=0;
                    for(int ireplace=i,jreplace=j;ireplace<i+5&&jreplace<j+5&&i+4<x&&j+4<x;ireplace++,jreplace++)
                        p+=Array[ireplace][jreplace];
                    if(p>=5)
                    {
                        cout << "Bingo!" << endl;
                        check=false;
                        break;
                    }

                    p=0;
                    for(int ireplace=i,jreplace=j;ireplace>=0&&jreplace<j+5&&i+4<x&&j+4<x;ireplace--,jreplace++)
                        p+=Array[ireplace][jreplace];
                    if(p>=5)
                    {
                        cout << "Bingo!" << endl;
                        check=false;
                        break;
                    }
//--------------------------------------------------------------------
                }
            }
            if(check==false)
                break;
        }
        if(check==true)
        {
            cout << "You lose!" << endl;
            continue;
        }
    }
    return 0;
}
