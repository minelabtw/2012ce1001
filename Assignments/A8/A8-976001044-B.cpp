#include <iostream>
#include <cstdlib>
using namespace std;
bool RowCheck(int*,int);
bool ColumnCheck(int*,int);
bool SkewCheck(int*,int);

// Bingo Game !!
// Enter a positive integer as the size of matrix (two dimension), and then input 0 or 1 as the data of matrix.
// Enter a character to terminated anytime.
// The data of matrix should be 0 or 1, another value will terminated too.
int main()
{
    int MatrixSize=0;
    int *inputMatrix=NULL;

    while ( cin >> MatrixSize && MatrixSize > 0 )
    {
        inputMatrix = new int [MatrixSize*MatrixSize];

        // Use one-dimension array to storage the two-dimension input data
        // Beacuse the subroutine can not recive multiple-dimension
        // So the following matrix expression will be : Matrix[i][j] --> Matrix[i+n*j]
        for ( int i=0 ; i <= MatrixSize-1 ; i++ )
        {
            for ( int j=0 ; j <= MatrixSize-1 ; j++ )
            {
                if ( (cin >> inputMatrix[i+MatrixSize*j] == false) || (inputMatrix[i+MatrixSize*j]!=0 && inputMatrix[i+MatrixSize*j]!=1)  )
                {
                    return 0;
                }
            }
        }

        // If matrix size !> 4, then the round is impossible to win
        // Only when input size > 4 need to call the Check function
        if ( MatrixSize > 4 )
        {
            if ( RowCheck(inputMatrix,MatrixSize) || ColumnCheck(inputMatrix,MatrixSize) || SkewCheck(inputMatrix,MatrixSize) )
            {
                cout << "Bingo!" << endl;
            }
            else
            {
                cout << "You lose!" << endl;
            }
        }
        else
        {
            cout << "You lose!" << endl;
        }

        delete [] inputMatrix;
        inputMatrix = NULL;
    }
}

bool RowCheck(int *Matrix, int n)
{
    int key=1;
    bool flag=false;
    for ( int i=0 ; i <= n ; i++ )
    {
        for ( int k=0 ; k <= n-4 ; k++ )
        {
            key = Matrix[i+n*(0+k)] * Matrix[i+n*(1+k)] * Matrix[i+n*(2+k)] * Matrix[i+n*(3+k)] * Matrix[i+n*(4+k)];
            if ( key == 1 )
            {
                flag = true;
            }
        }
    }
    return flag;
}

bool ColumnCheck(int *Matrix, int n)
{
    int key=1;
    bool flag=false;
    for ( int i=0 ; i <= n ; i++ )
    {
        for ( int k=0 ; k <= n-4 ; k++ )
        {
            key = Matrix[(0+k)+n*i] * Matrix[(1+k)+n*i] * Matrix[(2+k)+n*i] * Matrix[(3+k)+n*i] * Matrix[(4+k)+n*i];
            if ( key == 1 )
            {
                flag = true;
            }
        }
    }
    return flag;
}

bool SkewCheck(int *Matrix, int n)
{
    int key_1=1, key_2=1;
    bool flag=false;
    for ( int i=0 ; i <= n-4 ; i++ )
    {
        for ( int j=0 ; j <= n-4 ; j++ )
        {
            // Slope = -1
            key_1 = Matrix[i+n*j] * Matrix[(i+1)+n*(j+1)] * Matrix[(i+2)+n*(j+2)] * Matrix[(i+3)+n*(j+3)] * Matrix[(i+4)+n*(j+4)];
            // Slope = 1
            key_2 = Matrix[(4+i)+n*j] * Matrix[(3+i)+n*(j+1)] * Matrix[(2+i)+n*(j+2)] * Matrix[(1+i)+n*(j+3)] * Matrix[i+n*(j+4)];
            if ( key_2 == 1 || key_1 == 1 )
            {
                flag = true;
            }
        }
    }
    return flag;
}
//7 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0
//7 0 0 0 0 0 1 0 0 1 0 0 1 0 0 0 0 1 1 0 0 0 0 0 1 1 1 0 0 1 1 1 1 1 0 0 0 0 0 0 1 0 0 0 1 0 0 1 0 0
