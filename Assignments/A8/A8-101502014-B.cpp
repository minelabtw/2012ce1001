#include <iostream>
using namespace std;
int map[100][100] , bingo , n;
void horizon(int y , int x) //從map[y][x]開始向右找有沒有bingo
{
    int sum = 0; //sum記錄有幾格是1

    while(map[y][x] == 1 && sum < 5) //直到碰到0(無法連線)或找到一條線 , 才中止迴圈
    {
        sum++;
        x++;
    }
    if(sum == 5) //5格都是1 , 則bingo
        bingo = 1;
}
void vertical(int y , int x) //從map[y][x]開始向下找有沒有bingo
{
    int sum = 0; //sum記錄有幾格是1

    while(map[y][x] == 1 && sum < 5) //直到碰到0(無法連線)或找到一條線 , 才中止迴圈
    {
        sum++;
        y++;
    }
    if(sum == 5) //5格都是1 , 則bingo
        bingo = 1;

}
void slant1(int y , int x) //從map[y][x]開始向右下找有沒有bingo
{
    int sum = 0; //sum記錄有幾格是1

    while(map[y][x] == 1 && sum < 5) //直到碰到0(無法連線)或找到一條線 , 才中止迴圈
    {
        sum++;
        x++;
        y++;
    }
    if(sum == 5) //5格都是1 , 則bingo
        bingo = 1;

}
void slant2(int y , int x) //從map[y][x]開始向左下找有沒有bingo
{
    int sum = 0; //sum記錄有幾格是1

    while(map[y][x] == 1 && sum < 5) //直到碰到0(無法連線)或找到一條線 , 才中止迴圈
    {
        sum++;
        x--;
        y++;
    }
    if(sum == 5) //5格都是1 , 則bingo
        bingo = 1;

}
int main()
{
    while(cin >> n)
    {
        bingo = 0; //初始bingo為0代表沒bingo
        for(int i = 0 ; i < n ; i++) //輸入bingo盤的資訊
        {
            for(int j = 0 ; j < n ; j++)
            {
                if(cin >> map[i][j] == false) //若有字元輸入 , 則結束程式
                    return -1;
                if(map[i][j] != 1 && map[i][j] != 0) //若有非0 1輸入 , 則結束程式
                    return -1;
            }
        }
        if(n < 5) //若n小於5 , 則無法連線 , 輸出失敗訊息
        {
            cout << "You lose!" << endl;
            break;
        }
        for(int k = 0 ; k < n ; k++)
        {
            for(int l = 0 ; l < n ; l++)
            {
                if(map[k][l] == 1) //若找到某一點為1 , 則判斷它有沒有連線
                {
                    if(l + 5 <= n)
                        horizon(k , l);
                    if(k + 5 <= n)
                        vertical(k , l);
                    if(l + 5 <= n && k + 5 <= n)
                        slant1(k , l);
                    if(l - 5 >= 0 && k + 5 <= n)
                        slant2(k , l);
                }
                if(bingo) //bingo為1代表bingo了 , 則跳出迴圈
                    break;
            }
        }
        if(bingo) //bingo為1 , 則輸出勝利訊息
            cout << "Bingo!" << endl;
        else //bingo為0 , 則輸出失敗訊息
            cout << "You lose!" << endl;
    }
	return 0;
}
