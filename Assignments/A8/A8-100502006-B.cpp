#include<iostream>
#include<vector>

using namespace std;

bool horizontalCheck(vector<vector<int> >);//function to do the horizontal check
bool verticalCheck(vector<vector<int> >);//function to do the vertical check
bool leftSlantCheck(vector<vector<int> >);//function to do the left-slant check
bool rightSlantCheck(vector<vector<int> >);//function to do the right-slant check

int main()
{
    while(1)
    {
        int n;
        if(cin>>n==false)//Input size of matrix
        {
            return 0;
        }
        const int sizeY=n;
        const int sizeX=n;
        vector<vector<int> > vBingo(sizeY,vector<int>(sizeX));//declaration of matrix
        for(int y=0;y<sizeY;y++)//Input matrix's members
        {
            for(int x=0;x<sizeX;x++)
            {
                int b;
                cin>>b;
                if(cin==false||(b!=0&&b!=1))
                {
                    return 0;
                }
                vBingo[y][x]=b;
            }
        }
        if((sizeY>4||sizeX>4)&&(horizontalCheck(vBingo)||verticalCheck(vBingo)||leftSlantCheck(vBingo)||rightSlantCheck(vBingo)))//do the checks
        {
            cout<<"Bingo!"<<endl;
        }
        else
        {
            cout<<"You lose!"<<endl;
        }
    }
    return 0;
}
bool horizontalCheck(vector<vector<int> > a)
{
    for(int y=0;y<a.size();y++)//vertical counts
    {
        int counts=0;
        for(int x=0;(x<a[y].size()-5)||(a[y][x]==1);x++)//horizontal counts
        {
            if(a[y][x]==1)
            {
                counts++;
            }
            else
            {
                counts=0;
            }
            if(counts==5)
            {
                return true;
            }
        }
    }
    return false;
}
bool verticalCheck(vector<vector<int> > b)
{
    for(int x=0;x<b[0].size();x++)//horizontal counts
    {
        int counts=0;
        for(int y=0;(y<b.size()-5)||(b[y][x]==1);y++)//vertical counts
        {
            if(b[y][x]==1)
            {
                counts++;
            }
            else
            {
                counts=0;
            }
            if(counts==5)
            {
                return true;
            }
        }
    }
    return false;
}
bool leftSlantCheck(vector<vector<int> > c)
{
    for(int y=0;y<c.size()-4;y++)//vertical counts
    {
        int counts=0;
        for(int j=y,i=0;(j<c.size()-5)&&(i<c[j].size()-5)||(c[j][i]==1);j++,i++)//left-slant counts
        {
            if(c[j][i]==1)
            {
                counts++;
            }
            else
            {
                counts=0;
            }
            if(counts==5)
            {
                return true;
            }
        }
    }
    for(int x=1;x<c[0].size()-4;x++)//horizontal counts
    {
        int counts=0;
        for(int j=0,i=x;(j<c.size()-5)&&(i<c[j].size()-5)||(c[j][i]==1);j++,i++)//left-slant counts
        {
            if(c[j][i]==1)
            {
                counts++;
            }
            else
            {
                counts=0;
            }
            if(counts==5)
            {
                return true;
            }
        }
    }
    return false;
}
bool rightSlantCheck(vector<vector<int> > d)
{
    for(int y=0;y<d.size()-4;y++)//vertical counts
    {
        int counts=0;
        for(int j=y,i=d.size()-1;(j<d.size()-5)&&(i>4)||(d[j][i]==1);j++,i--)//right-slant counts
        {
            if(d[j][i]==1)
            {
                counts++;
            }
            else
            {
                counts=0;
            }
            if(counts==5)
            {
                return true;
            }
        }
    }
    for(int x=d[0].size()-2;x>3;x--)//horizontal counts
    {
        int counts=0;
        for(int j=0,i=x;(j<d.size()-5)&&(i>4)||(d[j][i]==1);j++,i--)//right-slant counts
        {
            if(d[j][i]==1)
            {
                counts++;
            }
            else
            {
                counts=0;
            }
            if(counts==5)
            {
                return true;
            }
        }
    }
    return false;
}
