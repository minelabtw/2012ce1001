#include<iostream>
using namespace std;

int main()
{
    while( cin ) //repetitively
    {
        int H = 0, V = 0, PS = 0, NS = 0; //initialize
        const int n = 5;
        int array[n][n];

        for( int i = 0; i < n; i++ ) //input array
        {
            for( int j = 0; j < n; j++ )
            {
                if( cin >> array[i][j] == false ) //if array is a character, end
                    return 0;
                if(array[i][j] != 0 && array[i][j] != 1 ) //if array is not equal to 0 or 1, end
                    return 0;
            }
        }

        for( int x = 0; x < n; x++ ) //calculate sum
        {
            int T1 = 0, T2 = 0; //initialize, and prepare to store (temporal)

            for( int y = 0; y < n; y++ )
            {
                T1 += array[x][y]; //store the sum into T1 (row)
                T2 += array[y][x]; //store the sum into T2 (column)
            }
            PS += array[x][x]; //calculate sum of number of positive slope(diagonal)
            NS += array[x][(n-1)-x]; //calculate sum of number of negetive slope(diagonal)

            if( T1 == 5 || T2 == 5 ) //if ther are five number 1, stop searching
            {
                H = T1;
                V = T2;
                break;
            }
        }

        //if there are five number 1 at every row or column or diagonal
        if( H == 5 || V == 5 || PS == 5 || NS == 5 )
            cout << "Bingo!" << endl;
        else
            cout << "You lose!" << endl;
    }
    return 0;
}
