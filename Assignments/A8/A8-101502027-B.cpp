#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int i;
    while (cin>>i)
    {
        bool choose=1,choose2=1;
        int a[i][i];
        for (int x=0;x<i;x++)
        {
            for (int y=0;y<i;y++)
            {
                cin>>a[x][y];
                if (a[x][y]!=0&&a[x][y]!=1)
                    return 0;
            }
        }//輸入陣列
        if (i<5)
        {
            cout <<"You lose!"<<endl;
            continue;
        }//不滿5列的錯誤偵測
        for (int x=0;x<i;x++)
        {
            for (int y=0;y<i;y++)
            {
                if (a[x][y]==1)
                {
                    if (a[x+1][y]==1&&a[x+2][y]==1&&a[x+3][y]==1&&a[x+4][y]==1)
                        choose=false;
                    else if (a[x][y+1]==1&&a[x][y+2]==1&&a[x][y+3]==1&&a[x][y+4]==1)
                        choose=false;
                    else if (a[x+1][y+1]==1&&a[x+2][y+2]==1&&a[x+3][y+3]==1&&a[x+4][y+4]==1)
                        choose=false;
                    else if (a[x-1][y+1]==1&&a[x-2][y+2]==1&&a[x-3][y+3]==1&&a[x-4][y+4]==1)
                        choose=false;
                }//檢查直橫斜上以及斜上
                if(choose==false)
                {
                    cout<<"Bingo!"<<endl;
                    break;
                }//ok則跳出for迴圈
            }
            if (choose==false)
            {
                choose2=false;
                break;
            }
        }
        if(choose2==false)
            continue;
        else
        {
            cout<<"You lose!"<<endl;
            continue;
        }//都沒檢查ok則輸出你輸了!!
    }
    return 0;
}

