#include<iostream>
using namespace std;

int main()
{
    int count;
    int square[100][100];//宣告一個方陣

    while(1)//重複輸入
    {
        if((cin >> count) == false || count <= 0)
            return 0;

        int rslant = 0;
        int lslant = 0;
        int MaxColumn = 0;
        int MaxRow = 0;
        int MaxSlant = 0;

        for(int i=0 ; i < count ; i++)//以一排一排來做判斷
        {
            int row = 0;

            for(int j=0 ; j < count ; j++)
            {
                if((cin >> square[i][j]) == false)//錯誤偵測
                    return 0;

                if (square[i][j] != 0 && square[i][j] != 1)//錯誤偵測
                    return 0;


                if (square[i][j] == 1)//判斷橫排的有幾個1
                {
                    row++;

                    if (row >= 5)
                    {
                        MaxRow = 5;
                    }
                }
                else
                    row = 0;

                if (row > MaxRow)//存取橫排的最大值
                    MaxRow = row;

            }
        }

        for (int j=0 ; j < count ; j++)//以一行一行的來做判斷
        {
            int column = 0;

            for (int i=0 ; i < count ; i++)
            {
                if (square[i][j] == 1)//判斷直行有幾個1
                {
                    column++;

                    if (column >= 5)
                    {
                        MaxColumn = 5;
                    }
                }
                else
                    column = 0;

                if (column > MaxColumn)//存取直行的最大值
                    MaxColumn = column;
            }
        }

        for (int k=0 ; k < (count-1)+(count-1) ; k++)//判斷向右傾斜的斜線
        {
            int rslant = 0;

            for (int i=0 ; i < count ; i++)
            {
                for(int j=0 ; j < count ; j++)
                {
                    if (square[i][j] == 1 && i + j == k)//相加都是等於固定的數
                    {
                        rslant++;

                        if(rslant >= 5)
                        {
                            MaxSlant = 5;
                        }
                    }

                    if (rslant > MaxSlant)//存最大的數
                        MaxSlant = rslant;
                }
            }
        }

        for (int k = 0 ; k < count ; k++)//判斷向左傾斜的斜線的右半部
        {
            int lslant = 0;

            for(int i = 0 ; i < count ; i++)
            {
                if (square[i][i+k] == 1)//判斷有幾個1
                {
                    lslant++;

                    if(lslant >= 5)
                    {
                        MaxSlant = 5;
                    }
                }
                else
                    lslant = 0;

                if(lslant > MaxSlant)//存最大值
                    MaxSlant = lslant;
            }
        }

        for (int k = 0 ; k < count ; k++)//判斷向左傾斜的斜線的左半部
        {
            int lslant = 0;

            for(int i = 0 ; i < count ; i++)
            {
                if (square[i+k][i] == 1)//判斷有幾個1
                {
                    lslant++;

                    if(lslant >= 5)
                    {
                        MaxSlant = 5;
                    }
                }
                else
                    lslant = 0;

                if(lslant > MaxSlant)//存最大值
                    MaxSlant = lslant;
            }
        }

        if (MaxColumn >= 5 || MaxRow >= 5 || MaxSlant >= 5)//如果有一橫排或一直行或斜線的五個連成一條線就輸出賓果
        {
            cout << "Bingo!" << endl;
        }

        else
        {
            cout << "You lose!" << endl;
        }

    }

    return 0;
}
