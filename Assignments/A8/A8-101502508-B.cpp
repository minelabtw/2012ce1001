#include<iostream>

using namespace std ;

int main()
{
    int n, m ;

    while ( cin >> n )//輸入多少層
    {
        int win1=0, win2=0, win3=0, win4=0 ;

        if ( cin==false )//對層數的錯誤偵測
            return 0 ;

        m=n ;

        int game[n][m] ;
///////////////////進行每一個位置的輸入/////////////////////
        for ( int k=0 ; k<n ; k++ )
        {
            for( int l=0 ; l<m ; l++ )
            {
                cin >> game[k][l] ;

                if ( cin==false || game[k][l]>1 )//對輸入的錯誤偵測
                    return 0 ;
            }
        }
///////////////////偵測column是否有bingo///////////////////////
        for ( int u=0 ; u<n ; u++ )
        {
            for ( int v=0 ; v<m-4 ; v++ )
            {
                if ( game[u][v]+game[u][v+1]+game[u][v+2]+game[u][v+3]+game[u][v+4]==5  )
                    win1=1 ;
            }
        }
///////////////////偵測row是否有bingo///////////////////////
        for ( int v=0 ; v<m ; v++ )
        {
            for ( int u=0 ; u<n-4 ; u++ )
            {
                if ( game[u][v]+game[u+1][v]+game[u+2][v]+game[u+3][v]+game[u+4][v]==5 )
                    win2=1 ;
            }
        }
///////////////////偵測左上斜向右下的斜線是否有bingo///////////////////////
        for ( int u=0 ; u<n-4 ; u++ )
        {
            for ( int v=0 ; v<n-4 ; v++ )
            {
                if ( game[u][v]+game[u+1][v+1]+game[u+2][v+2]+game[u+3][v+3]+game[u+4][v+4]==5 )
                    win3=1 ;
            }
        }
///////////////////偵測右上斜向左下的斜線是否有bingo///////////////////////
        for ( int u=0 ; u<n-4 ; u++ )
        {
            for ( int v=n ; v>3 ; v-- )
            {
                if ( game[u][v]+game[u+1][v-1]+game[u+2][v-2]+game[u+3][v-3]+game[u+4][v-4]==5 )
                    win4=1 ;
            }
        }
///////////////////輸出輸出輸出///////////////////////
        if ( win1==1 || win2==1 || win3==1 || win4==1 )
            cout << "Bingo!" << endl ;
        else
            cout << "You lose!" << endl ;
    }
}


