#include<iostream>
using namespace std;
int main()
{
    int n,z=0;
    while(cin>>n)
    {
        int a[n][n];
        for(int x=0;x<n;x++)//二維陣列輸入
        {
            for(int y=0;y<n;y++)
            {
                cin>>a[x][y];
                if(a[x][y]!=0&&a[x][y]!=1)//判斷非01退出整個while迴圈 用z來當判斷變數
                {
                    z=1;
                    break;
                }
            }
            if(z==1)
                break;
        }
        if(z==1)
                break;
        int bingo;
        for(int x=0;x<n;x++)//橫向  遇到1開始累加到5 bingo 退出 遇到0則bingo歸零
        {
            bingo=0;
            for(int y=0;y<n;y++)
            {
                if(a[x][y]==1)
                    bingo++;
                else
                    bingo=0;
                if(bingo==5)
                {
                    cout<<"Bingo!"<<endl;
                    break;
                }
            }
            if(bingo==5)
                break;
        }
        if(bingo==5)
            continue;

        for(int x=0;x<n;x++)//直向  遇到1開始累加到5 bingo 退出 遇到0則bingo歸零
        {
            bingo=0;
            for(int y=0;y<n;y++)
            {
                if(a[y][x]==1)
                    bingo++;
                else
                    bingo=0;
                if(bingo==5)
                {
                    cout<<"Bingo!"<<endl;
                    break;
                }
            }
            if(bingo==5)
                break;
        }


        for(int x=0;x<=n-5;x++)//最外兩層迴圈是左上到右下的第一個
        {
            for(int y=0;y<=n-5;y++)
            {
                for(z=0;z<5;z++)//計算5個數 有0退出
                    if(a[x+z][y+z]==0)
                        break;
                if(z==5)//5個全跑完代表bingo 退出
                {
                    cout<<"Bingo!"<<endl;
                    break;
                }
            }
            if(z==5)
                break;
        }
        if(z==5)
            continue;
        for(int x=0;x<=n-5;x++)//最外兩層迴圈是右上到左下的第一個
        {
            for(int y=4;y<n;y++)
            {
                for(z=0;z<5;z++)//計算5個數 有0退出
                    if(a[x+z][y-z]==0)
                        break;
                if(z==5)//5個全跑完代表bingo 退出
                {
                    cout<<"Bingo!"<<endl;
                    break;
                }
            }
            if(z==5)
                break;
        }
        if(z==5)
            continue;
        cout<<"You lose!"<<endl;//全跑完則  lose
    }
    return 0;
}
