#include<iostream>
using namespace std;

int rows;
int colums;

int main()
{
    while (cin >> rows)                 //used for enter again.
    {

        /////  error detection  /////
        if (rows<5)
        {
            return 0;
        }
        /////////////////////////////

        colums=rows;
        int array[rows][colums];        //used for create a rows*rows array.
        bool winmark=false;             //used for record win or lose.


        ///// enter the value of array  /////
        for (int i=0;i<rows;i++)
        {
            for (int j=0;j<colums;j++)
            {
                cin >> array[i][j];
            }
        }
        /////////////////////////////////////


        for (int i=0;i<rows;i++)
        {
            for (int j=0;j<colums;j++)
            {
                //////////////  to detect bingo  //////////////
                if ((array[i][j]==1&&array[i+1][j]==1&&array[i+2][j]==1&&array[i+3][j]==1&&array[i+4][j]==1)
                    ||(array[i][j]==1&&array[i][j+1]==1&&array[i][j+2]==1&&array[i][j+3]==1&&array[i][j+1]==1)
                    ||(array[i][j]==1&&array[i+1][j+1]==1&&array[i+2][j+2]==1&&array[i+3][j+3]==1&&array[i+4][j+4]==1))
                {
                    winmark=true;
                }
                ///////////////////////////////////////////////

                /////////////  error detection  //////////////
                if (array [i][j] <0 || array [i][j]>1)
                    return 0;
                //////////////////////////////////////////////
            }
        }

        ///////  show the result  ///////
        if (winmark==true)
            cout << "Bingo!" <<endl;
        else
            cout << "You lose!" <<endl;
        ////////////////////////////////

    }

    return 0;
}

