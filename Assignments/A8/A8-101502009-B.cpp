# include<iostream>

using namespace std;

int main()
{
    while(1)
    {   bool flag=false;
        int n,time=0;
        if((cin >> n==false)||(n<=0))//排除字元及負數及0
        {
            return 0;
        }
        int field[n][n];



        for( int i=1 ; i<=n ; i++ )//列
        {
            for( int j=1 ; j<=n ; j++ )//行
            {
                if( (cin >> field[i][j]==false) || ((field[i][j]!=0)&&(field[i][j]!=1)) )//輸入
                {
                    return 0;
                }
            }
        }

        for( int i=1 ; i<=n ; i++ )//檢驗列是否bingo
        {
            for( int j=1 ; j<=n ; j++ )
            {
                if(field[i][j]==1)//找到起始點
                {
                    time=time+1;
                    if(field[i][j]==0)
                    {
                        time=0;
                    }
                    if(time==5)
                    {
                         flag=true;
                    }
                }

            }
            time=0;

        }
        for( int j=1 ; j<=n ; j++ )//檢驗行是否bingo
        {
            for( int i=1 ; i<=n ; i++ )
            {
                if(field[i][j]==1)//找到起始點
                {
                    time=time+1;
                    if(field[i+1][j]==0)
                    {
                        time=0;
                    }
                    if(time==5)
                    {
                        flag=true;
                    }

                }
            }
            time=0;
        }
        for( int i=1 ; i<=n ; i++ )//檢驗右下斜
        {
            for( int j=1 ; j<=n ; j++ )
            {
                if(field[i][j]==1)//找到起始點
                {
                    time=time+1;
                    for( int k=1 ; k<=4 ; k++ )
                    {
                        if(field[i+k][j+k]==1)
                        {
                            time=time+1;
                        }
                        if(time==5)
                        {
                            flag=true;
                        }
                    }
                }
                time=0;

            }
        }
        for( int i=1 ; i<=n ; i++ )//檢驗左下斜
        {
            for( int j=1 ; j<=n ; j++ )
            {
                if(field[i][j]==1)//找到起始點
                {
                    time=time+1;
                    for( int k=1 ; k<=4 ; k++ )
                    {
                        if(field[i+k][j-k]==1)
                        {
                            time=time+1;
                        }
                        if(time==5)
                        {
                            flag=true;
                        }
                    }
                }
                time=0;

            }
        }
        if(flag==true)
        {
            cout << "Bingo !" <<endl;
        }
        if(flag==false)
        {
            cout << "You lose !" <<endl;
        }
    }
    return 0;
}
