#include <iostream>
using namespace std;
int main()
{
    int x[5][5];
    while ( 1 )//重複輸入
    {
        int a, b, c = 0, d = 0, a_max = 0, b_max = 0;
        for ( int i = 0; i < 5; i++ )//陣列x輸入
        {
            for ( int j = 0; j < 5; j++ )
            {
                if ( cin >> x[i][j] == false )//輸入若為字元,則結束程式
                    return -1;
                if ( x[i][j] != 0 && x[i][j] != 1 )//輸入若非0和1,結束程式
                    return -1;
            }
        }
        for ( int i = 0; i < 5; i++ )
        {
            a = 0;//將a,b的初始值0定在迴圈中
            b = 0;
            for ( int j = 0; j < 5; j++ )
            {
                if ( x[i][j] == 1 )//同行(直的)不同列
                    a++;//計算有幾個1
                if ( a > a_max )//判斷a的最大值
                    a_max = a;
                if ( x[j][i] == 1 )//同列(橫的)不同行
                    b++;//計算有幾個1
                if ( b > b_max )//判斷b的最大值
                    b_max = b;
            }
        }
        for ( int i = 0; i < 5; i++ )
        {
            if ( x[i][i] == 1 )//左上到右下的斜線
                c++;//計算有幾個1
            if ( x[i][4-i] == 1 )//右上到左下的斜線
                d++;//計算有幾個1
        }
        if ( a_max==5 || b_max==5 || c==5 || d==5 )//若有5個1,輸出Bingo!
            cout << "Bingo!" << endl;
        else//其餘則輸出You lose!
            cout << "You lose!" << endl;
    }
    return 0;
}
