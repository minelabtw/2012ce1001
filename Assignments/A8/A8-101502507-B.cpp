//輸入nxn矩陣 若有連續5個1則bingo
#include<iostream>

using namespace std;

int main()
{
    int n, i, j, k, sum;//sum用來計算有無連續5個1

    while(1)//要能重複輸入
    {
        cin >> n;//n代表之後輸入的矩陣是nxn

        int a[n][n], win=0;//win用來判斷是否有bingo(win=1 Bingo!, win=0 You lose!)

        for(i=0;i<n;i++)//輸入二維陣列
        {
            for(j=0;j<n;j++)
            {
                if(cin >> a[i][j] == false)    return -1;//偵錯! 若輸入非數字則結束程式
                if(a[i][j]!=0 && a[i][j]!=1)   return -1;//偵錯! 若輸入非0或1的數則結束程式
            }
        }

        for(i=0;i<n;i++)//判斷橫向bingo
        {
            sum = 0;

            for(j=0;j<n;j++)
            {
                if(a[i][j] == 1)//若下一項=1則累加
                {
                    sum++;
                    if(sum >= 5) win = 1;//若累加至超過5則bingo
                }

                else if(sum < 5) sum = 0;//若下一項不為1且累加未達5,則歸零,不bingo
            }
        }

        for(j=0;j<n;j++)//判斷直向bingo
        {
            sum = 0;

            for(i=0;i<n;i++)
            {
                if(a[i][j] == 1)//若下一項=1則累加
                {
                    sum++;
                    if(sum >= 5) win = 1;//若累加至超過5則bingo
                }

                else if(sum < 5) sum = 0;//若下一項不為1且累加未達5,則歸零,不bingo
            }
        }

        for(k=(n-5);k>=-(n-5);k--)//判斷斜向bingo(左上至右下斜) nxn矩陣共可能有-(n-5)到(n-5)條斜線有可能bingo
        {
            sum = 0;

            for(i=0;i<n;i++)
            {
                if(a[i][i+k] == 1)//若下一項=1則累加
                {
                    sum++;
                    if(sum >= 5) win = 1;//若累加至超過5則bingo
                }

                else if(sum < 5) sum = 0;//若下一項不為1且累加未達5,則歸零,不bingo
            }
        }

        for(k=(n-5);k>=-(n-5);k--)//判斷斜向bingo(右上至左下斜)
        {
            sum = 0;

            for(i=0;i<n;i++)
            {
                if(a[i][4-i] == 1)//若下一項=1則累加
                {
                    sum++;
                    if(sum >= 5) win = 1;//若累加至超過5則bingo
                }

                else if(sum < 5) sum = 0;//若下一項不為1且累加未達5,則歸零,不bingo
            }
        }

        if(win) cout << "Bingo!" << endl;//若win不為0則bingo
        else    cout << "You lose!" << endl;//若win為0則lose
    }

    return 0;

}
