#include<iostream>

using namespace std ;

int main()
{
    int n,i,j,check;

    cin >>  n;   //輸入指定的n
    int a[n+1][n+1];//多一個比較好看 下標為 0~n

    for( i = 1 ; i <= n ; i++)
    {
        for( j = 1 ; j <= n ; j++)
        {
            cin >> a[i][j];
            if(a[i][j]>1 || a[i][j]<0)//檢測輸入
            return 0;
        }
    }


    for( i = 1 ; i <= n ; i++ )//檢測每一列
    {
        check = 0;//每一列都要先歸零
        for( j =1 ; j <= n ; j++)
        {
            if(a[i][j]==1) //1的時候累計+1 為零0時累計歸零
            {
                check++;
                if(check==5)//累計到5則Bingo
                {
                    cout << "Bingo!";
                    return 0;
                }

            }
            else
            {
                check = 0;
            }
        }
    }

    for( i = 1 ; i <= n ; i++ )//檢測每一行  其餘類似
    {
        check = 0;
        for( j =1 ; j <= n ; j++)
        {
            if(a[j][i]==1)
            {
                check++;
                if(check==5)
                {
                    cout << "Bingo!";
                    return 0;
                }
            }
            else
            {
                check = 0;
            }

        }
    }


    for( i = 1 ; i <= n ; i++)//檢測對角線
    {
        check = 0 ;
        if(a[i][i]==1)
        {
            check++;
            if(check==5)
            {
                cout << "Bingo!";
                return 0;
            }
        }
        else
        {
            check = 0;
        }

    }


    for( i = 1 ; i <= n ; i++)//檢查另一個斜線
    {
        check = 0;
        if(a[i][n-i+1]==1)
        {
            check++;
                if(check==5)
                {
                    cout << "Bingo!";
                    return 0;
                }
        }

        else
        {
            check = 0;
        }
    }

    cout << "You lose!";//如果都沒有連線 才會輸出

    return 0;
}
