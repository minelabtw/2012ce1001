#include <iostream>
#include <vector>

using namespace std;

int main()
{
    while(1)
    {
        int n,a,b,x=0;
        if(cin>>n==false)           //輸入範圍並做錯誤偵測
            return -1;

        vector< vector<int> > bingo( n , vector<int>(n) );      //宣告二維陣列

        for(a=0;a<n;a++)
        {
            for(b=0;b<n;b++)
            {
                if(cin>>bingo[a][b]==false || (bingo[a][b]!=0 && bingo[a][b]!=1) )    //輸入陣列值並做錯誤偵測
                    return -1;
            }
        }

        for(a=0;a<n;a++)
        {
            for(b=0;b<=n-5;b++)                           //橫向賓果判斷
            {
                if(bingo[a][b]==1 && bingo[a][b+1]==1 && bingo[a][b+2]==1 && bingo[a][b+3]==1 && bingo[a][b+4]==1)   //連五個則賓果
                {
                    x=x+1;
                    if(x==1)                    //有一條賓果則輸出
                        cout<<"Bingo!\n";
                }
            }
        }

        for(b=0;b<n;b++)
        {
            for(a=0;a<=n-5;a++)                           //直向賓果判斷
            {
                if(bingo[a][b]==1 && bingo[a+1][b]==1 && bingo[a+2][b]==1 && bingo[a+3][b]==1 && bingo[a+4][b]==1)    //連五個則賓果
                {
                    x=x+1;
                    if(x==1)                   //有一條賓果則輸出
                        cout<<"Bingo!\n";
                }
            }
        }

        for(a=0;a<=n-5;a++)
        {
            for(b=0;b<=n-5;b++)                          //斜下向賓果判斷
            {
                if(bingo[a][b]==1 && bingo[a+1][b+1]==1 && bingo[a+2][b+2]==1 && bingo[a+3][b+3]==1 && bingo[a+4][b+4]==1)    //連五個則賓果
                {
                    x=x+1;
                    if(x==1)                   //有一條賓果則輸出
                        cout<<"Bingo!\n";
                }
            }
        }

        for(a=4;a<=n-1;a++)
        {
            for(b=0;b<=n-5;b++)                          //斜上向賓果判斷
            {
                if(bingo[a][b]==1 && bingo[a-1][b+1]==1 && bingo[a-2][b+2]==1 && bingo[a-3][b+3]==1 && bingo[a-4][b+4]==1)    //連五個則賓果
                {
                    x=x+1;
                    if(x==1)                   //有一條賓果則輸出
                        cout<<"Bingo!\n";
                }
            }
        }
        if(x==0)
            cout<<"You lose!\n";
    }
    return 0;
}
