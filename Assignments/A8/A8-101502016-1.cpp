#include <iostream>

//打a會無限bingo
using namespace std;

const int rows = 5;
const int columns = 5;

int main()
{
    while (1)//始可重複輸入
    {
        int array[ rows ][columns];
        int rowsbingo=0;
        int columnsbingo=0;
        int slantbingo=0;

        for ( int i=0 ; i<rows ; i++)
        {
            for ( int j=0 ; j<columns ; j++)
            {
                if(cin >> array[i][j] == false)
                    return -1;
                if ( array[i][j] != 1 && array[i][j] != 0)
                    return -1;
            }
        }

        for ( int i=0 ; i<rows ; i++)//橫排的判斷
        {
            int rowscount=0;

            for ( int j=0 ; j<columns ; j++)
            {
                if ( array[i][j] == 1 )
                    rowscount += 1;

            }

            if ( rowscount == 5)
                rowsbingo += 1;

        }

        for ( int i=0 ; i< rows ; i++)//直排的判斷
        {
            int columnscount=0;

            for ( int j=0 ; j<columns ; j++ )
            {
                if ( array[j][i] == 1)
                {
                    columnscount += 1;
                }

            }

            if ( columnscount == 5)
                columnsbingo +=1;

        }

        int slantcount=0;//歸零
        for ( int i=0,j=0 ; i<5 ; i++,j++ )//斜排左上至右下的判斷
        {
            if (array[i][j]==1)
                slantcount += 1;

            if ( slantcount == 5)
                slantbingo += 1;

        }
        slantcount=0;//歸零
        for (int i=4,j=0 ; i<0,j<5 ; i--,j++ )//斜排由左下至右上的判斷
        {
            if ( array[i][j]==1 )
                slantcount += 1;

            if ( slantcount == 5)
                slantbingo += 1;

        }

        if ( rowsbingo > 0 || columnsbingo > 0 || slantbingo >0 )
            cout << "Bingo!" << endl;
        else
            cout << "You lose!" << endl;
    }
    return 0;
}
