#include<iostream>
using namespace std;
int main()
{
    int p[5][5];
    while(1)
    {
        int x=0,y=0;

        for(int i=0; i<5; i++)
        {
            for(int j=0; j<5; j++)
            {
                cin>>p[i][j];
                if(p[i][j]!=1&&p[i][j]!=0)//If p[i][j] !=0 && p[i][j]!=1, end the program
                    return -1;
                if(cin==false)//If input character, end the program
                    return -1;
            }
        }
        //////////////////////////////
        int m=0,n=0;

        while(m<5&&n!=5)//To test the row if has five 1
        {
            n=0;//Reset the n
            while(p[m][n]==1)
            {
                n++;
            }
            m++;
        }
        //////////////////////////////
        int a=0,b=0;

        while(b<5&&a!=5)//To test the column if has five 1
        {
            a=0;//Reset the a
            while(p[a][b]==1)
            {
                a++;
            }
            b++;
        }
        //////////////////////////////
        int s=0,t=0;

        while(p[s][t]==1)//To test the oblique if has five 1
        {
            s++;
            t++;
            x++;
        }
        //////////////////////////////
        int h=0,k=4;

        while(p[h][k]==1)//To test the oblique if has five 1
        {
            h++;
            k--;
            y++;
        }
        //////////////////////////////
        if(n==5||a==5||x==5||y==5)
            cout<<"Bingo!"<<endl;
        else
            cout<<"You lose!"<<endl;
    }
    return 0;
}
