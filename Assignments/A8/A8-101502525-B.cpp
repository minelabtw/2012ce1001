#include<iostream>
using namespace std;

int main()
{
    int n;
    while(cin>>n&&n>0)//input check
    {
        //initialize
        int board[n+2][n+2];
        for(int y=0;y<n+2;y++)
            for(int x=0;x<n+2;x++)
                board[x][y]=0;

        //input
        for(int y=1;y<=n;y++)
            for(int x=1;x<=n;x++)
                if(!(cin>>board[x][y])||(board[x][y]!=1&&board[x][y]!=0))//input check
                    return 0;//exit

        //find bingo
        int bingo=0,dx[4]={1,1,1,0},dy[4]={-1,0,1,1};//flag, directions: /,-,\,|
        for(int y=1;y<=n;y++)
            for(int x=1;x<=n;x++)
                if(board[x][y]==1)//search a piece. If a piece exists, check aruond
                    for(int d=0;d<4;d++)//four directions
                        for(int t=1,l=1;(board[x+(dx[d]*l)][y+(dy[d]*l)]==1);l++,t++)//l=length from the first piece
                            if(t==4)//besides the first one, four pieces at the direction
                                bingo++;
        //judge
        cout<<(bingo>0?"Bingo!\n":"You lose!\n");
    }
    return 0;//end
}
