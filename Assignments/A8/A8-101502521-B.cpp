//bonus version
#include <iostream>

using namespace std;

int n;
int dx[] = {0,1,1,1}; // 枚舉起始點 從左上角開始 只有可能出現4種方向
int dy[] = {1,1,0,-1}; // 偏移量
bool G[600][600] = {0};
bool bingo; // is find?

// gogo(now position x, now y, now length, now Direction)
void gogo(int x, int y, int len, int dir);

int main()
{
    while(cin >> n)
    {
        bingo = false;
        int tmp;
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++)
            {
                if(!(cin >> tmp)||(tmp!=0&&tmp!=1))
                    return 0;
                G[i][j] = tmp;
            }

        for(int i=0;i<n&&!bingo;i++)
            for(int j=0;j<n&&!bingo;j++) // enumerate point
                if(G[i][j]==1)
                    for(int direction=0;direction<4&&!bingo;direction++)
                        gogo(i, j, 1, direction);
        if(!bingo)
            cout << "You lose!" << endl;
    }
    return 0;
}

void gogo(int x, int y, int len, int dir)
{
    if(len==5)
    {
        bingo = true;
        cout << "Bingo!" << endl;
        return;
    }
    int nx = x+dx[dir];
    int ny = y+dy[dir];
    if(nx<0||nx>=n||ny<0||ny>=n) // if out of range
        return;
    if(G[nx][ny]==1)
        gogo(nx, ny, len+1, dir); // gogogo
}
