#include<iostream>
using namespace std;

int main()
{
    int n;
    while (cin >>n && n>0)
    {
        int t,s=0;
        int a[n][n];
        for (int x=0;x<n;x++)
        {
            for (int y=0;y<n;y++)
            {
                if (cin >>t && t>=0 && t<=1)
                a[x][y]=t;
                if (t<0 || t>1) //若輸入超過0,1範圍的數字,則做出錯誤偵測
                return -1;
            }
        }

        for (int v=0;v<n;v++) //當Bingo為橫線的情況
        {
            for (int w=0;w<=n-5;w++)
            {
                for (int z=0;z<=4;z++)
                {
                    if (a[v][w+z]==1)
                    s++;
                }
                if (s==5) //只要偵測到有一條Bingo,就跳出迴圈,沒有必要繼續做下去
                {
                    cout <<"Bingo!"<<endl;
                    break;
                }
                else
                s=0;
            }
            if (s==5)
            break;
        }
        if (s==5) //若上述條件成立,則輸入下一個n
        continue;
        else //若沒有成立,則s歸0,進入下一個測試
        s=0;
        for (int c=0;c<n;c++) //當Bingo為直線的情況
        {
            for (int d=0;d<=n-5;d++)
            {
                for (int e=0;e<=4;e++)
                {
                    if (a[d+e][c]==1)
                    s++;
                }
                if (s==5) //只要偵測到有一條Bingo,就跳出迴圈,沒有必要繼續做下去
                {
                    cout <<"Bingo!"<<endl;
                    break;
                }
                else
                s=0;
            }
            if (s==5)
            break;
        }
        if (s==5) //若上述條件成立,則輸入下一個n
        continue;
        else //若沒有成立,則s歸0,進入下一個測試
        s=0;
        for (int q=0;q<=n-5;q++) //當Bingo為由左上-右下的斜線情況
        {
            for (int r=0;r<=n-5;r++)
            {
                for (int o=0;o<=4;o++)
                {
                    if (a[q+o][r+o]==1)
                    s++;
                }
                if (s==5) //只要偵測到有一條Bingo,就跳出迴圈,沒有必要繼續做下去
                {
                    cout <<"Bingo!"<<endl;
                    break;
                }
                else
                s=0;
            }
            if (s==5)
            break;
        }
        if (s==5) //若上述條件成立,則輸入下一個n
        continue;
        else //若沒有成立,則s歸0,進入下一個測試
        s=0;
        for (int h=1;h<=n-4;h++) //當Bingo為左下-右上的斜線情況
        {
            for (int i=0;i<=n-5;i++)
            {
                for (int j=0;j<=4;j++)
                {
                    if (a[n-h-j][i+j]==1)
                    s++;
                }
                if (s==5) //只要偵測到有一條Bingo,就跳出迴圈,沒有必要繼續做下去
                {
                    cout <<"Bingo!"<<endl;
                    break;
                }
                else
                s=0;
            }
            if (s==5)
            break;
        }
        if (s==5)
        continue;
        else //若以上4種情況皆沒有發生,則判定玩家輸了
        cout <<"You lose!"<<endl;
    }
    return 0;
}
