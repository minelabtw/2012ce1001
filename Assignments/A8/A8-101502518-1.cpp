#include<iostream>
using namespace std;
int main()
{
    const int length = 5;
    const int width = 5;
    int n[length][width];

    for(int i=0;i<length;i++)
    {
        for(int j=0;j<width;j++)
            if(cin>>n[i][j]==false||n[i][j]>1||n[i][j]<0)//除錯
            {
                return -1;
            }
    }

    if(n[0][0]==1 && n[1][1]==1 && n[2][2]==1 && n[3][3]==1 && n[4][4]==1)//檢查左上右下對角線
        cout<<"Bingo!";
    else if(n[0][4]==1 && n[1][3]==1 && n[2][2]==1 && n[3][1]==1 && n[4][0]==1)//檢查左下右上對角線
        cout<<"Bingo!";
    else
    {
        for(int i=0;i<5;i++)
        {
            if(n[i][0]== 1 && n[i][0]==n[i][1] && n[i][1]==n[i][2] && n[i][2]==n[i][3] && n[i][3]==n[i][4])//檢查橫排
            {
                cout<<"Bingo!";
                break;
            }
            else if(n[0][i]== 1 && n[0][i]==n[1][i] && n[1][i]==n[2][i] && n[2][i]==n[3][i] && n[3][i]==n[4][i])//檢查直排
            {
                cout<<"Bingo!";
                break;
            }
            else//完全沒中
            {
                cout<<"You lose!";
                break;
            }
        }
    }

    return 0;
}
