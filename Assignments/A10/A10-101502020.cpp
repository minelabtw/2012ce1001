//A10 by J
//I want to use the same idea in my Q3 and modify it a little better.
#include<iostream>
#include<string>
using namespace std;

//you must design a class for following main function
class number
{
 public:
    number();       //constructor
    void setNumber( string ) ;
    void plusplus() ;
    string getNumber() ;        //functions used &  mentioned by problem
 private:       //2 variables used
    string str ;        //for change
    int ctr ;       //same for input style 99999
};

number::number()
{
        //an empty constructor
}

void number::setNumber( string inp )
{
    str=inp ;   //set str as input value.
    ctr=0 ;     //reset ctr
}

void number::plusplus()
{
    str[str.length()-1]+=1 ;     //add 1 to the rightest digit

    for ( int i=(str).length()-1 ; i>=0 ; i-- )        //exam all the digits from right.
    {
        if ( ((int)(str)[i])==58 )      //it means the char in string is "10"(:).
        {
            (str)[i]='0';      //set it to '0'
            (int)((str)[i-1]+=1) ;     //the next digit +1
            ctr++ ;     //only if there's a "9+1->0" , ctr++.
        }
    }

    string k( (str).length()+1 , '0' ) ;       //for resetting str.

    if ( ctr==(str).length() )     //it deals with cases need 1 more digit (like 999+1=1000)
    {
        str=k ;        //reset the string length to 1 more.
        (str)[0]='1' ;     //set the first digit as 1.
    }
}

string number::getNumber()
{
    return str ;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
