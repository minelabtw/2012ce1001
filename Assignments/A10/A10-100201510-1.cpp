#include <iostream>
#include <string>
using namespace std;

//you must design a class for following main function
class number
{
public:
    number()
    {
    }

    void setNumber(string num)
    {
        n = num;
    }

    void plusplus()//the code in this function is copy from Q3-100201510.cpp
    {
        //檢查所有位數皆為九的情況，如果每個位數都是都是九，則位數加一
        bool nineflag = 1;
        for(int i = n.length() -1 ; i >= 0 ; i--)
        {
            if(n[i]  == 57)
                nineflag *= 1;
            else
                nineflag *= 0;
                    break;
        }

        if(nineflag)
            n = '0' + n;


        n[n.length() -1]++; //個位數字加一

        for(int i = n.length() -1 ; i >= 0 ; i--)
        {
            /* 57 為 "9" 的 ASCII 碼，但+1後的編碼卻是 ":" 的編碼，
            需要先將冒號修正回 "0" 的編碼，也就是48，才可繼續做下一位數的計算。*/
            if( n[i]  == 58)
            {
                n[i] = 48;
                n[i-1]++;
            }
        }
    }

    string getNumber()
    {
        return n;
    }

private:
    string n;
};

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
