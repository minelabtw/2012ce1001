#include<iostream>
#include<string>
using namespace std;

class number // class number definition
{
    public: // public data
        number(); // number constructor
        void setNumber(string); //setNumer function
        void plusplus(); // plusplus function
        string getNumber(); // return the got number
    private: // private data
        string n;
};

number::number() // constructor
{
    n = " ";
}

void number::setNumber (string no) // set a number to plusplus
{
    n = no;
}

void number::plusplus() // plusplus function definition
{
    bool nine = false;
    for (int i = 0; i <= n.length() - 1; i++) // check the set number
    {
        if (n[i] != '9') // if it's a not 9 number, true and end
        {
            nine = true;
            break;
        }
    }
    if (nine == true) // if it's true
    {
        for (int k = n.length() - 1; k >= 0; k--)
        {
            if (n[k] == '9') // change all 9 to 9
                n[k] = '0';
            else
            {
                n[k] = n[k] + 1; // change the first not 9 number from right to left + 1 and break
                break;
            }
        }
    }
    if (nine == false) // if it's all 9
    {
        for (int j = 0; j < n.length(); j++)
            n[j] = '0'; // change all to 0
        n = '1' + n; // put a 1 in front of all 0
    }

}

string number::getNumber()
{
    return n; // return the new number
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
