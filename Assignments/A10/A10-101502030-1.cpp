#include<iostream>
#include<string>
using namespace std;

class number
{
public:
    number()
    {
        o=0;
        i=0;
        win=true;
        p=1;
    }
    void setNumber(string value)                            //設一個變數來使用.
    {
        num=value;
    }
    void plusplus()                                 //計算過程.
    {
        i=num.length()-1;
        win=true;
        for(o=0;o<i+1;o++)                 //先分類成是否都是9跟有其他數字.
        {
            if(num[o]!='9')
            {
                win=false;
                break;
            }
        }
        if(win==false)                              //有其他數字.
        {
            for(i;i>=0;i--)          //從最後一項開始檢查.
            {
                if(num[i]!='9')                     //一不是9就加1.
                {
                    num[i]=num[i]+1;
                    break;
                }
                if(num[i]=='9')                     //若後面是9則變成0.
                {
                    num[i]='0';
                }
            }
        }
        if(win==true)                               //如果都是9.
        {
            string kda(i+2,'1');         //做一個多原本字串一位的字串,並且都是1.
            for(p=1;p<i+2;p++)
            {
                kda[p]='0';              //第一項之後通通改為0.
            }
            num=kda;
        }
    }
    string getNumber()
    {
        return num;
    }


private:
    int o,i,p;
    string num;
    bool win;
};

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}

