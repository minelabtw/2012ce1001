#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

//you must design a class for following main function
class number
{
    public:
        int digit_in, digit_out, index;
        string input, carry_out;

        // Initialization :
        // input data by the following function
        // and asign data value to the class variable
        void setNumber(string num)
        {
            input = num;
            carry_out = "1";
            digit_in = num.length();
            digit_out = digit_in + 1;
            return;
        }

        // Addition value 1 to the input data
        void plusplus()
        {
            bool carry=true;

            while (carry)
            {
                if ( digit_in > 0 )
                {
                    if ( (input[digit_in-1] < '9') && (input[digit_in-1] >= '0') )
                    {
                        // don't to carry
                        input[digit_in-1] = char(int(input[digit_in-1]) + 1);
                        carry = false;
                    }
                    else if ( input[digit_in-1] == '9' )
                    {
                        // needs carry to next digit
                        input[digit_in-1] = '0';
                        digit_in--;
                        carry = true;
                    }
                    else
                    {
                        // input data is not a number
                        input = "Invalid input!!";
                        carry = false;
                    }
                }
                else if ( digit_in == 0 )
                {
                    input = carry_out.append(input);
                    carry = false;
                }
            }
            return;
        }

        // return the result to this function
        string getNumber()
        {
            return input;
        }
};

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
