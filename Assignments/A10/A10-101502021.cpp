#include <iostream>
#include <string>
using namespace std;

class number
{
public:
    void setNumber(string);
    void plusplus();
    string getNumber();//要取出Str的值
private:
    string Str;//將傳入的字串存入Str內
    bool A;
};

void number::setNumber(string str)
{
    Str=str;
}

void number::plusplus()//讓數字+1
{
    A=true;

    for (int i=0 ; i<Str.length() ; i++)
    {
        if (Str[i]=='9')
            A=false;
        else
        {
            A=true;
            break;
        }
    }

    if (A)
    {
        for(int j=0;j<Str.length();j++)
        {
            if(Str[Str.length()-1]=='9')
            {
                for(int i=Str.length()-1;i>=0;i--)
                {
                    if(Str[i]=='9')
                    {
                        Str[i]='0';
                    }
                    else
                    {
                        Str[i]=Str[i]+1;
                        break;
                    }
                }
            }
            else
            {
               Str[Str.length()-1]=Str[Str.length()-1]+1;
               break;
            }
            break;
        }
    }
    else
    {
        string ptr(Str.length(),'0');
        Str='1'+ptr;
    }
}

string number::getNumber()//輸出的值
{
    return Str;
}


int main()
{
    string num;
    number plusCalculator;

    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
