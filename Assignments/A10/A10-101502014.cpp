#include<iostream>
#include<string>
using namespace std;
class number //number型態用來使大數加一
{
    public:
        void setNumber(string n) //bignum用來記錄輸入的大數 , len記錄此大數的長度
        {
            bignum = n;
            len = bignum.length();
        }
        void plusplus() //plusplus函式把大數加一
        {
            string bignum_plus(len + 1 , '0');
            int j = len;
            for(int i = 0 ; i < len ; i++)
                bignum_plus[i + 1] = bignum[i]; //bignum_plus記錄把大數平移右邊一位後的結果
            bignum_plus[len] += 1; //大數的第一位加一
            while(bignum_plus[j] > '9' && j >= 0) //判斷是否需要進位
            {
                bignum_plus[j] = '0';
                bignum_plus[j - 1] += 1;
                j--;
            }
            bignum = bignum_plus; //把加一後的結果丟回bignum
            if(bignum_plus[0] == '0') //若最大位為0(代表位數不變) ,則不必輸出
            {
                for(int k =0 ; k < len ; k++)
                    bignum[k] = bignum[k + 1];
                bignum[len] = '\0';
            }
        }
        string getNumber() //getNumber傳回大數加一的結果
        {
            return bignum;
        }
    private:
        string bignum;
        int len;
};
int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
