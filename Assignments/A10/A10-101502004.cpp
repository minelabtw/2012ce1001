#include<iostream>
#include<string>
using namespace std;

//you must design a class for following main function
class number//class裡面是放自己的屬性
{
public:
    number();//建構子constructor
    void setNumber(string);
    void plusplus();
    string getNumber();
private:
    string num;//用public去控制private裡面的東西
};

number::number(){}//constructor

void number::setNumber(string n)//設置字串
{
    num=n;
}

void number::plusplus()//進行運算
{
    int n=num.length(),check=0;//n是輸入的字串長度，check是要計算幾個9
    if(num[n-1]!='9')//當個位數不是9時，直接+1
        num[n-1]++;
    else
    {
        for(int i=n-1;i>=0;i--)//不然就進入迴圈
        {
            if(num[i]=='9')//只要是9。就把它變成0
            {
                num[i]='0';
                check++;
            }
            else//不是9的話就直接+1完成進位
            {
                num[i]++;
                break;
            }
        }
        if(check==n)//要是check算出全部都是9
        {
            string str(n+1,'0');//設置一個新陣列多一位數
            str[0]='1';//第一個變成1
            num=str;//取代原字串
        }
    }
}

string number::getNumber()//回傳出來
{
    return num;
}

int main()
{
    string num;
    number plusCalculator;//建立class物件
    while( cin >> num )
    {
        plusCalculator.setNumber(num);//傳入class設置字串
        plusCalculator.plusplus();//開始運算
        cout << plusCalculator.getNumber() << endl;//回傳後cout
    }
    return 0;
}
