#include<iostream>
#include<string>

using namespace std;

class number
{
public:
    number();
    void setNumber(string);
    void plusplus();
    string getNumber();
private:
    string num ;
};

number::number()                                      //初始值
{
    num = ' ';
}

void number::setNumber(string a)                      //設定輸入的值
{
    num =  a;
}

void number::plusplus()                               //運算過程
{
    int count = 0;
    for ( int i = 0 ; i <= num.length()- 1 ; i++ )          //計算是否全為9
    {
        if ( num.at(i) == '9' )
            count += 1 ;
    }
    if (count == num.length())                              //若全為9則將第一位數變0
    {                                                          //後面全為0且+補1個0
        num.at(0) = '1';
        for ( int j = 1 ; j <= num.length()-1 ; j++)
            num.at(j) = '0';
        num = num + '0';
    }
    else if (num.at(num.length()-1) == '9')              //否則若最後一個是9
    {                                                          //則將9變0
        for ( int i = num.length()-1 ; i > 0 ; i -- )       //從最後倒回來判斷
        {                                                      //遇到9都變為0若不是9則+1且跳出
            if ( num.at(i) == '9')
            {
                num.at(i) = '0';
                if (num.at(i-1) != '9' )
                {
                    num.at(i-1) += 1 ;
                    break;
                }

            }
        }
    }
    else                                                      //若以上都不是則直接+1
    {
        int length = num.length();
        num.at(length-1) += 1;
    }
}

string number::getNumber()                                    //回傳運算後的值
{
    return num;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
