#include<iostream>
#include<string>
using namespace std;

class number
{
public:
    void setNumber(string);
    void plusplus(void);
    string getNumber(void);

private:
    string digits;
    int len;

};

void number::setNumber(string s)
{
    digits = s;
}

string number::getNumber(void)
{
    return digits;
}

void number::plusplus(void)
{
    /////////// Create Backeard String And Init ///////////////
    int len = digits.length();                    // get the input string length
    string backward;                            // convert snum to backward string 123 => 321
    for(int i=len-1;i>=0;i--)
        backward += digits.at(i);
    backward += "0";                            //prevent length+1

    ////////// Process Add One ////////////////
    backward[0]++;                              // add one
    for(int i=0;i<=len;i++)
        if((backward[i]-'0')>=10)               // if carry
        {
            backward[i] -= 10;
            backward[i+1]++;                    // carry next pos
        }

    ///////// Store Back /////////////////
    digits.clear();                               // clear num
    int idx = len;
    while(backward[idx]=='0') idx--;            // find first index of non-zefo digit.such if 00123 => 123
    for(int i=idx;i>=0;i--)
        digits += backward.at(i);               // store back
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
