#include <iostream>
#include <string>

using namespace std;

class number
{
public:
    number()
    {

    }

    void setNumber(string target)
    {
        numstore=target;
    }

    void plusplus()
    {
        int length=numstore.length();
        int ninemark=-1;//設定9的標記初始值-1

        for(int i=length-1;i>=0;i--)//先從後面標記字串中連續的9到哪個位置
        {
            if(numstore[i]=='9')
                ninemark=i;
            else
                break;
        }

        if(ninemark!=-1 && ninemark!=0)
        {
            for(int i=length-1;i>=ninemark;i--)//把9通通改成0，前一個數在+1
                numstore[i]='0';
            numstore[ninemark-1]+=1;
        }
        else if(ninemark==0)
        {
            for(int i=length-1;i>=ninemark;i--)
                numstore[i]='0';
            numstore="1"+numstore;//全部改成0之後再跟1接起來
        }
        else
        {
            numstore[length-1]=numstore[length-1]+1;
        }
    }

    string getNumber()
    {
        return numstore;
    }
private:
    string numstore;
};

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
