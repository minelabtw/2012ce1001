#include<iostream>
using namespace std;


class number
{
    public:
        void setNumber(string in)
        {
            num=in;
        }
        void plusplus()
        {
            int tmp=0,i=num.length()-1;//tmp save the carry, compute from i, which is the end

            for(num[i]+=1;i>=0;num[--i]+=tmp)//if larger than 10, carry becomes 1. if not, becomes 0
                tmp=num[i]>'9'&&(num[i]-=10);//number larger than 10 will become its last digit

            if(tmp)//there's still carry means there must be one more digit
                num.insert(0,"1");//insert 1 at begin
        }
        string getNumber()
        {
            return num;
        }
    private:
        string num;
};


int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
