#include<iostream>
#include<string>
using namespace std;

class number
{
public:
    void setNumber(string n)  // 設定字串num
    {
        num = n;
    }
    void plusplus()//num++
    {
        num.at(num.length()-1) += 1; //先將數字+1
        for(int i=1 ; i <= num.length()-1 ; i++ ) //若有進位，將原數變為0，下一位數+1
            if(num.at(num.length()-i)>'9')
            {
                num.at(num.length()-i-1) += 1;
                num.at(num.length()-i) = '0';
            }
        if(num.at(0) > '9') //第一位若有進位，進位後變為0，多一字串設為1加在原字前
        {
            string num2="1";
            num.at(0) = '0';
            num2+=num;
            num=num2;
        }
    }
    string getNumber() //回傳+1後的字串
    {
        return num;
    }
private:
    string num;
}; // end class number
int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
