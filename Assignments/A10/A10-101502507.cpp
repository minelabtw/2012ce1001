#include<iostream>
#include<string>
using namespace std;

class number
{
public:

    string setNumber(string);
    string plusplus();
    string getNumber();

private:

    string _num;
};

string number::setNumber(string num)
{
    _num = num;
    return _num;
}

string number::plusplus()
{
    bool c = true;

    for(int k=_num.length()-1;k>=0;k--)
    {
        if(_num[k]=='9')//若9的前一位亦為9則往前一位+1
        {
            _num[k]='0';//自己變0(因為進位)
        }
        else
        {
            _num[k]+=1;//該位+1
            c = false;
            break;
        }
    }

    if(c) _num="1"+_num;//第一數字為9則多出一位數且第一數字為9

    return _num;
}

string number::getNumber()
{
    return _num;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {

        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }

    return 0;
}
