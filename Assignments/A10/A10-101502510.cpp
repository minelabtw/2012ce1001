#include<iostream>
#include<string>
using namespace std;
class number
{
public:
    void setNumber(string);
    void plusplus();
    string getNumber();
private:
    int L;
    string Big;
};

void number::setNumber(string b)
{
    Big=b;
    L=b.length();
}

void number::plusplus()
{
    string str(L+1,' ');//let str be bigger than Big
    int number=0;
    for(int i=0; i<L; i++)//pass Big to str
        str[i]=Big[i];
    for(int i=L-1; i>=0; i--)
    {
        if(str[i]=='9')//check the last character if 9
        {
            str[i]='0';//if the last character is 9 change it to be 0
            number++;
            if(number==L)//check if every character is 9
            {
                str[0]='1';
                str[L]='0';
            }
        }
        else
        {
            str[i]++;//if the last character is not 9, let the last charater +1
            break;
        }
    }
    Big=str;//pass str to Big
}

string number::getNumber()
{
    return Big;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
