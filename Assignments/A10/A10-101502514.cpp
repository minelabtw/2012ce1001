#include<iostream>
#include<string>
using namespace std;

class number
{
public:
    number();//建構子
    void setNumber(string);
    void plusplus();
    string getNumber();
private:
    string n;//宣告變數

};

number::number()//初始值
{
    n='0';
}

void number::setNumber(string n1)
{
    n=n1;
}

string number::getNumber()
{
    return n;
}

void number::plusplus()
{
    int b=n.length();
    for(int a=0;b>a;b--)
    {
        if(n[b-1]=='9')//最後一位為9時將9變為0
            n[b-1]='0';
        else
        {
            n[b-1]=n[b-1]+1;//如果不是9的話，+1
            break;
        }
        if(n[0]=='0')//如果為皆為9情況，第一位是0
        {
             n='1'+n;//進位，在前面多+一個1
        }
    }
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
