
//計算機實習作業
//資工系四年B班_965002086_張凱閔
//------------------------------------------- 

/*
3  大數加一 again
 
由於 int 的範圍只限於-20億~20億而已，而若是輸入的位數過多，則會造成 overflow 的問題，所以要請您寫一個簡單的程式，能夠實現某大數+1的功能。
 
輸入：
 
大數。要做重複輸入，但不必作錯誤偵測及判斷程式結束。
 
輸出：
 
大數+1。
 
請按照以下格式：(不可以改main function)
#include<iostream>
#include<string>
using namespace std;

//you must design a class for following main function

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
範例結果：
 1999999999999999999999999             <----輸入
2000000000000000000000000             <----輸出
9999999999999999999999999999999999999
10000000000000000000000000000000000000
5
6
*/

//-----------------------------------------------
#include<iostream>
#include<string>
using namespace std;

//you must design a class for following main function
class number{
      string str;
      public:
      string setNumber(string &num);
      string plusplus();
      string getNumber();
}; 

string number:: setNumber(string &num)
{
     str=num;
      return str;           
}; 
                  
string number::plusplus()
{
     
    string s="1";
    reverse(str.begin(),str.end());
    reverse(s.begin(),s.end());

    int temp=0;
    int lenth=str.length()>s.length()?str.length()+1:s.length()+1;
    string aa=str+string(lenth-str.length(),'0');
    string ss=s+string(lenth-s.length(),'0');
    for(int i=0;i<lenth;i++)
    {
       temp+=aa[i]-'0'+ss[i]-'0';
       aa[i]=temp%10+'0';
       temp/=10;
    }
    str=aa;
    return str;               
}; 
                 
string number::getNumber()
{
    reverse(str.begin(),str.end());
return str.substr(str.find_first_not_of('0'));             
                 }; 
                 
int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
