#include<iostream>
#include<string>
using namespace std;

//you must design a class for following main function

class number
{
public:
    number();
    void setNumber(string);
    void plusplus();
    string getNumber();
private:
    int s;
    string num1;
};

number::number()
{
    s=0;
}

void number::setNumber(string num)//此函式用來傳遞字串
{
    s=num.length();
    num1=num;
}

void number::plusplus()//此函式用來加一計算
{
    if(num1[0]!='-')
    {
        string str(s+1,'0');//宣告一字串陣列
        for(int i=1;i<s+1;i++)//傳遞陣列
        {
            str[i]=num1[i-1];
        }
        str[s]=(int)str[s]+1;//陣列加一
        for(int i=s;i>0;i--)//若進位的算法
        {
            if((int)str[i]==58)//迴圈演算
            {
                str[i]='0';
                (int)str[i-1]++;
            }
        }
        num1=str;
        if(str[0]=='0')//若有進位
        {
            for(int i=0;i<s;i++)//移位
            {
                num1[i]=num1[i+1];
            }
            num1[s]='\0';//加最後一位設為空字元
        }
    }
    if(num1=="-1")
        num1='0';
    else if(num1[0]=='-')//若為負數
    {
        string str(s,'0');
        for(int i=0;i<s;i++)//移到另一個字串陣列
        {
            str[i]=num1[i];
        }
        str[s-1]=(int)str[s-1]-1;//進行加一
        for(int i=s-1;i>0;i--)//退位運算
        {
            if((int)str[i]==47)
            {
                str[i]='9';
                (int)str[i-1]--;
            }
        }
        num1=str;
        if(str[1]=='0')//若退位
        {
            for(int i=1;i<s;i++)//移位
            {
                num1[i]=num1[i+1];
            }
            num1[s-1]='\0';
        }
    }
}

string number::getNumber()//取值
{

    return num1;
}
int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )//輸入字串數字
    {
        plusCalculator.setNumber(num);//呼叫函式
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;//輸出加一後結果
    }
    return 0;
}
