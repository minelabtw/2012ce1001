#include<iostream> //It's include negative number plus
#include<string> //有做負數的加1
using namespace std;

//you must design a class for following main function
class number // 自定義類別
{
    public:
        void setNumber( string num )
        {
            plusCalculator=num;
        }
        void plusplus() // 加數字1
        {
            int n=plusCalculator.length();
            if (plusCalculator[0]!='-') //題目是-20億~20億 所以先判斷大數的正負
            {
                for(int i=n-1;i>=0;i--)
                {
                    if (plusCalculator[i]!='9') //如果i+1位數非9 直接+1
                    {
                        plusCalculator[i]=plusCalculator[i]+1;
                        break;
                    }
                    else if (plusCalculator[i]==57) //如果i+1位數為9 轉變為0
                    {
                        plusCalculator[i]=48;
                    }
                }
                if (plusCalculator[0]=='0') //如果最後一位為0時 把數字直接顯示在num前
                {
                    plusCalculator='1'+plusCalculator;
                }
            }
            else if (plusCalculator[0]=='-') //負數在這執行
            {
                if (plusCalculator=="-1")//若數字為-1 則直接轉正數0
                    plusCalculator='0';
                else
                {
                    for(int i=n-1;i>=0;i--)
                    {
                        if (plusCalculator[i]!='0') //如果i位數非0 直接-1
                        {
                            plusCalculator[i]=plusCalculator[i]-1;
                            break;
                        }
                        else if (plusCalculator[i]==48) //如果i位數為0 轉變為9
                        {
                            plusCalculator[i]=57;
                        }
                    }
                    if (plusCalculator[1]=='0')
                    //當顯示數字num[1]有0時 代表-100就會顯示成-099
                    //為了解決中間卡0 先把數字往左移 最後面的數字直接等於空白 即可解決問題
                    {
                        for(int j=1;j<n-1;j++)
                        plusCalculator[j]=plusCalculator[j+1];

                        plusCalculator[n-1]=' ';
                    }
                }
            }
        }
        string getNumber() //要回傳數值 否則無法cout
        {
            return plusCalculator;
        }
    private:
        string plusCalculator;
};
int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
