#include<iostream>
#include<string>
using namespace std;

class number
{
    public:
        void setNumber(string);
        void plusplus();
        string getNumber();
    private:
        int i;//the length of len
        string len;
        string addlength;
};

void number::setNumber(string num)
{
    len = num;
}

void number::plusplus()
{
    i = len.length();
    string addlength(i+1,'0');
    for (int j = i-1;j >= 0;j--)//check the numbers from the unit number
    {
        if (len[j]=='9')
        {
            len[j] = '0';
        }
        else
        {
            len[j] = len[j] + 1;
            break;
        }
    }
    if (len[0] == '0')//if len is composed by several 0, it can be adjusted by addlength
    {
        addlength[0] = '1';
        len = addlength;
    }

}

string number::getNumber()
{
    return len;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
