#include<iostream>
#include<string>
using namespace std;

//you must design a class for following main function
class number
{
 public:
     void setNumber(string);
     void plusplus();
     string getNumber();

 private:
     string k;
};
void number::setNumber(string a)
{
    k = a;//把a存到k裡面存起來
}
void number::plusplus()
{
    bool c =true;//假設原本bool=true
    for(int p=k.length()-1;p>=0;p--)//從個位數往前檢查
    {
        if(k[p]=='9')//如果是9,要進位,所以用0代替
            k[p]='0';
        else
        {
            k[p]+=1;//如果不是9,就加1
            c=false;//如果不是9,把bool改成false
            break;
        }
    }
    if(c==true)
        k="1"+k;//把字串前面加上一個1
}
string number::getNumber()
{
    return k;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
