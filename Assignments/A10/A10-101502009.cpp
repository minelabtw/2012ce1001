#include<iostream>
#include<string>
using namespace std;

class number
{
public:
    void setNumber(string num);
    void plusplus();
    string getNumber();
private:
    string s;
};

void number::setNumber(string num)
{
    s=num;
}
void number::plusplus()
{
    int g=s.size(),time=0;
    /////////特例 都9
    for(int i=0;i<g;i++)
    {
        if(s[i]=='9')
        {
            time=time+1;
        }
        if(time==g)//進位
        {
            for(int i=0;i<g;i++)//換零
            {
                s[i]='0';
            }
            s='1'+s;//補一
            return;
        }
    }
    /////////

    /////////一般情況
    s[g-1]++;
    if(s[g-1]==':')//有9
    {
        for(int i=0;i<g;i++)
        {
            if(s[g-1-i]==':')
            {
                s[g-1-i]='0';
                s[g-2-i]=s[g-2-i]+1;
            }
        }
    }

}
string number::getNumber()
{
    return s;
}

//you must design a class for following main function

int main()
{
    string num;
    number plusCalculator;//使用class
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
