//A10-100502201
//Use class to do plus calculator
//No need to use pointer
#include<iostream>
#include<string>
using namespace std;

class number
{
    public:
        number(); //Constructor
        number(string);
        string num(); //Show input
        string getNumber(); //Get result
        void setNumber(string); //Get input
        void plusplus(); //A method to plus one to a big integer
    private:
        string _num; //Used to save string
};
number::number()
{
    _num="0";
}
number::number(string num)
{
    _num=num;
}
string number::getNumber()
{
    return _num;
}
void number::setNumber(string num)
{
    _num=num;
}
void number::plusplus()
{
    //If [1] to [n-1] is 9, flag is true
    //And use different methods to the string whose [0] is 9 or isn't 9 respectively
    bool flag=true;
    for (int l=1;l<_num.length();l++)
    {
        if (_num[l]=='9')
            continue;
        else
            flag=false;
    }
    if (_num[0]=='9'&&flag==true)
    {
        string str="1";
        for (int i=1;i<=_num.length();i++)
            str.insert(i,"0");
        _num=str;
    }
    else if(_num[0]!='9'&&flag==true)
    {
        _num[0]=_num[0]+1;
        for (int i=1;i<_num.length();i++)
            _num[i]='0';
    }
    //Normal add method, carry overs are counted in a variable
    if (flag==false)
    {
        int j=_num.length()-1;
        int count=0;
        int t=j-1;
        if (_num[j]!='9')
            _num[j]=_num[j]+1;
        else
        {
            _num[j]='0';
            count=1;
        }
        for (;t>=0;t--)
        {
            if (_num[t]==9&&count==1)
            {
                _num[t]='0';
            }
            else if (_num[t]!=9&&count==1)
            {
                _num[t]=_num[t]+1;
                count=0;
            }
        }
    }
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
