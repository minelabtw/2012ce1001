#include<iostream>
#include<string>
using namespace std;

class number
{
    public:
        void setNumber( string num )
        {
            n=num;
        }
        void plusplus()
        {
            int L=n.length();//L為輸入的長度
            string str(L,'0');//製造一個string讓長度為L而且數字都是0(要進位時使用)
            int count = 0;//用來計算有幾個9(要進位時使用)

            if ( n[L-1] != '9' )//最後一位不是9的情況
                n[L-1]++;//最後一位數加1

            if ( n[L-1] == '9' && L == 1 )//輸入是9的情況
                n = "10";//輸出10

            else
                for( int i = L ; i > 0 ; i-- )//讓迴圈從個位數開始跑
                {
                    if( n[i]=='9' )//如果位數是9
                    {
                        n[i]='0';//讓該位數變0
                        if ( n[i-1] != '9' )//如果下一位不是9
                        {
                            n[i-1]++;//讓下一位數加1,並跳出迴圈
                            break;
                        }
                        count++;//count加1
                    }
                    if( (count) == L-1 )//都是9(要進位的情況)
                        n= '1'+str;
                }
        }

        string getNumber()
        {
            return n;//回傳值
        }
    private:
        string n;
};

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
