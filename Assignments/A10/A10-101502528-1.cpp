#include<iostream>
#include<string>
using namespace std;

class number
{
public:
    void setNumber(string);
    void plusplus();
    string getNumber();
private:
    string num;
};

void number::setNumber(string n)
{
    num=n;
}

void number::plusplus()
{
    int test; //利用test的數值測試是否全數字均為9

    for (int a=num.length()-1 ; a>=0 ; a--) //檢測是否全數字均為9
    {
        test=0;

        if (num.at(a)=='9')
            test=1;

        else if (num.at(a)!='9')
        {
            test=0;
            break;
        }
    }

    if (test==1) //若全數字均為9則提前輸出1
        cout << '1';


    for (int n=num.length()-1 ; n>=0 ; n--)
    {
        if(num.at(n)=='9') //因進位故將9轉換成0
            num.at(n)='0';

        else if (num.at(n)!='9') //不必進位故加1後跳出迴圈
        {
            num.at(n)=num.at(n)+1;
            break;
        }
    }
}

string number::getNumber()
{
    return num;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
