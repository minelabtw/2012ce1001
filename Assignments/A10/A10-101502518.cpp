#include<iostream>
#include<string>
using namespace std;

//you must design a class for following main function

class number
{
public:
    void setNumber(string);
    void plusplus();
    string getNumber();
private:
    string n;
};

void number::setNumber(string k)
{
    n = k;
}

void number::plusplus()
{
    int l=n.size();
    bool flag=false;
    n[l-1]++;

    for(int i=l-1;i>=0;i--)
    {
        if(n[i]=='9'+1)
        {
            if(i!=0)
                n[i-1]++;
            else
            {
                n[0]='0';
                flag=true;
            }
            n[i]='0';
        }
    }
    if(flag==true)
    {
        string str(l+1,' ');
        str[0]='1';
        for(int i=0;i<=l;i++)
            str[i+1]=n[i];
        n=str;
    }
}

string number::getNumber()
{
    return n;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);

        plusCalculator.plusplus();

        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
