#include<iostream>
#include<string>
using namespace std;

class number//
{
public:

    void setNumber(string value)
    {
        x=value;//取得值
    }
    void plusplus()//運算加一的函式
    {
        int l=x.length();//數字共有幾位數
        if(x[l-1]=='9')//末位為九
        {
            for(int i=l-1;i>=0;i--)//由個位數開始看每個位數是否為九
            {
                if(x[i]!='9')//若不為九，加一且結束回圈
                {
                    x[i]++;
                    break;
                }
                else//為九，則變為0且繼續迴圈
                    x[i]='0';
                if(i==0)//若每個位子皆為九，則要增加一位數
                {
                    string str(l+1,' ');//先令一個字串，其位數為原字串加一
                    str='1'+x;//其值為1加上前面的結果
                    x=str;//將其值給予x
                }
            }
        }
        else//若不是九，則直接加一
            x[l-1]++;

    }
    string getNumber()
    {
        return x;//回傳結果
    }
private:
    string x;
};//class結束

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
