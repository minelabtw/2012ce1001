#include<iostream>
#include<string>

using namespace std;

class number
{
    public:
        number();
        void setNumber(string);
        void plusplus();
        string getNumber();
    private:
        string cnum;

};

number::number()
{
    cnum=("");
}

void number::setNumber(string n)
{
    cnum=n;
}

void number::plusplus()
{
    int x;
    x=cnum.length()-1; //先給定長度
    if(cnum[0]=='-')  //如果是負數
    {
        do
        {
            cnum.at(x)-=1;//因為負數+1 數字部分減少  先從個位數開始減
            if(cnum.at(x)<'0')//需要借位的話
               {
                    cnum.at(x)='9';
                    x--;
               }
            else
                break;
        }while(x>=0);
        if(x==1&&cnum.length()>2)//如果有 -100 -1000 的情況 防止變成 -099  -0999
        {
            string a(cnum.length()-1,'9');
            a.at(0)='-';
            cnum=a;
        }
        if(cnum.length()==2&&cnum.at(1)=='0')//如果是-1  防止變成-0  輸出0
        cnum='0';
    }

    else{
        x=cnum.length()-1;
        do////////////個位數開始累加 和進位
        {
            cnum.at(x)+=1;
            if(cnum.at(x)>'9')
               {
                    cnum.at(x)='0';
                    x--;
               }
            else
                break;
        }while(x>=0);
        if(x==-1)//防止9999 999999 情況  防止變成0000  000000
            {
                string a(cnum.length()+1,'0');
                a.at(0)='1';
                cnum=a;
            }
        }
}

string number::getNumber()
{
    return cnum;
}

int main()

{

string num;

number plusCalculator;

while( cin >> num )

{

plusCalculator.setNumber(num);

plusCalculator.plusplus();

cout << plusCalculator.getNumber() << endl;

}

return 0;

}
