#include<iostream>
#include<string>
using namespace std;
class number
{
    public:
        number(string="");// default constructor
        void setNumber(string);// set string
        void plusplus();// plus 1
        string getNumber();// return string
    private:
        string numberString;// store the string
};
number::number(string str)
{
    setNumber(str);// validate and set string
}
void number::setNumber(string setNumStr)
{
    numberString=setNumStr;// set private field string
}
void number::plusplus()
{
    bool ninesFlag=true;//to record if it is all nines
    for(int i=numberString.length()-1;i>=0;i--)
    {
        if(numberString[i]=='9')//turn nine into zero
        {
            numberString[i]='0';
        }
        else//if it is not nine, plus 1
        {
            numberString[i]+=1;
            ninesFlag=false;
            break;
        }
    }
    if(ninesFlag)//if it is all nine, insert '1' infront of the string
    {
        numberString="1"+numberString;
    }
}
string number::getNumber()
{
    return numberString;// return string value
}
int main()
{
    string num;
    number plusCalculator;// create number object
    while( cin >> num )
    {
        plusCalculator.setNumber(num);// set string to valid value
        plusCalculator.plusplus();// plus 1
        cout << plusCalculator.getNumber() << endl;// print string
    }
    return 0;
}
