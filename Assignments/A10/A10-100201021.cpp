#include<iostream>
#include<string>
using namespace std;

//you must design a class for following main function
 class number
 {
 public:
    number();
    void setNumber(string);
    void plusplus();
    string getNumber();
 private:
    string n;
 };
number::number()
{

}

void number::setNumber(string num)
{
    n = num;
}
void number::plusplus()
{
    int l = n.length(),p=0,b=0; //p >> 判斷正負號,  b >> 判斷進位
    //正負號判斷
    if(n[0]=='-')
        p=1;
    else
        p=0;
    //
    if(p==0) //n>=0 ，遇到9則進位
    {
        if(n[l-1]=='9')
        {
            n[l-1]='0';
            b=1;
            for(int i=l-2;i>=0;i--)
            {
                if(n[i]=='9' && b==1)
                    n[i]='0',b=1;
                else if(b==1)
                    n[i]+= 1,b=0;
                else
                    b=0;
            }
            if(b==1)    //開頭為9且要進位
            {
                n = '1'+n;
                b=0;
            }
        }
        else
            n[l-1]+= 1;
    }
    else    //n<0 (p==1) 遇到0則進位
    {
        b=0;
        for(int i=1;i<l;i++)    //負號後的0改為空白直到遇到非0
        {
            if(n[i]=='0')
                n[i]=0,b++;
            else
                break;
        }
        if(b==l-1)              //n==0 >> n = 2-1 = 1
            n=" 2",b=0,l=2;
        if(n[l-1]=='0')         //是否進位
        {
            n[l-1]='9';
            b=1;
            for(int i=l-2;i>0;i--)
            {
                if(n[i]=='0' && b==1)
                    n[i]='9',b=1;
                else if(b==1)
                    n[i]-= 1,b=0;
                else
                    b=0;
            }
        }
        else
            n[l-1]-= 1;
        if(n[1]=='0')   //負號開頭如果=0則去除
            n[1]=0;
    }
}
string number::getNumber()
{
    return n;                   //傳回計算後的n (n = num+1)
}
int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
