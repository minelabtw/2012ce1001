#include<iostream>
#include<string>
using namespace std;

class number
{
    public:
    void setNumber(string a)
    {//設定字串a
        num=a;
    }
    void plusplus()
    {
        int m=0;
        for(int k=num.length()-1;k>=0;k--)//將輸入的數字加一
        {
            if(num.at(k)=='9')
            {//數字為九，則進位
                (num).at(k) = '0';
            }
            else if(num.at(k)!=9)
            {//不是九，則加一並跳出迴圈
                num.at(k) = num.at(k) + 1;
                m = m + 1;
                break;
            }
        }
        if((num).at(0)=='0' && m==0)
        {//如果數字均為九的運算

            string change((num).length()+1,' ');
            for(int n=0;n<(num).length();n++)
            {
                (change).at(n+1)=(num).at(n);
            }
            (change).at(0)='1';
            (num) = change;
        }
    }
    string getNumber()
    {
        return num;
    }
    private:
    string num;
};

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
