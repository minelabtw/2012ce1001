#include<iostream>
#include<string>
using namespace std;

class number
{
public:
    void setNumber(string);
    string getNumber();
    void plusplus();

private:
    string numstore;
    int x; //use variable "x" to store the length plus 1 of the string "num"
};

void number::setNumber(string num)
{
    x=num.length()+1;
    numstore=" "+num; //use the string "numstore" to store a blank
                      //at first position and then store the whole string "num"
}

void number::plusplus() //this is the function to add 1 to the big number
{
    for(int i=x-1;i>=0;i--)
    {
        if(numstore[i]=='9') //to find the character "9" and replace it with 0
        {
            numstore[i]='0';
            continue;
        }
        if(numstore[i]!='9'&&numstore[i]!=' ')
        {
            numstore[i]=numstore[i]+1;
            break;
        }
        if(i==0)
            numstore[0]='1';
    }
}

string number::getNumber()
{
    if(numstore[0]==' ') //if the first character is the blank,then delete it
        numstore=numstore.substr(1,x-1);
    return numstore;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
