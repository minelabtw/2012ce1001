#include<iostream>
#include<string>
using namespace std;

class number//製作number class
{
    public:
        void setNumber(string num);//將外部變數導入內部變數
        void plusplus();//執行+1
        string getNumber();//回傳結果字串
    private:
        string plusCalculator;//內部變數
};
void number::setNumber(string num)
{
    plusCalculator=num;
}
void number::plusplus()
{
    plusCalculator[plusCalculator.length()-1]++;
    for(int i=plusCalculator.length()-1;i>=0;i--)//檢查進位
        if(plusCalculator[i]==':')
        {
            plusCalculator[i]='0';
            if(i!=0)
                plusCalculator[i-1]++;
            else
                plusCalculator='1'+plusCalculator;
        }
}
string number::getNumber()
{
    return plusCalculator;
}
int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
