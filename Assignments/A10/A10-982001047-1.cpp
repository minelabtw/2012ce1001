#include<iostream>
#include<string>
using namespace std;

//you must design a class for following main function


class number
{
public:

    void setNumber( string num)   // 輸入數字到x
    {
        x = num ;
    }

    void plusplus()
    {
        string y = "0";                    // 設多一位數的y
        y += x ;
        y.at(y.length()-1) += 1;           // 在尾項加1
        for( int i = y.length()-1 ; i > 0 ; i--)   //檢查每一項是否大於9則進位
        {
            if( y.at(i) > '9' )
            {
                y.at(i) = '0';
                y.at(i-1) += 1;
            }

        }
        x = y;              //  代回到x
    }

    string getNumber()
    {
        if(x.at(0) == '0' )    // 去掉最前面的0
        {
            return x.substr(1) ;
        }
        else
        {
            return x;
        }

    }

private:
    string x,y;

};

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
