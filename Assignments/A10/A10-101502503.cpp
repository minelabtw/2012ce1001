#include<iostream>
#include<string>
using namespace std;

//you must design a class for following main function
class number
{
public:
    void setNumber(string);
    void plusplus();
    string getNumber();
private:
    string *big;
    int count;
    int length;

};

void number::setNumber(string n)//將big指標指向n的記憶體位置
{
    big=&n;
}
void number::plusplus()
{
    int count=0 , length=(*big).length();//count計算是否全為;length為字串長度
    for(int i=0 ; i<length ; i++)//判斷是否全為9
    {
        if( (*big)[i] == '9' )
            count++;
    }
    if(count==length)//全為9
    {
        string new1(1+length,'0');
        new1[0]='1';
        (*big)=new1;
    }
    else if(count!=length)//不是全為9
    {
        for(int  i=length-1 ; i>=0 ;i--)//從個位數開始判斷
        {
           if((*big)[i]=='9')//
           {
               (*big)[i]='0';
               if((*big)[i-1]!='9')//如果後面的位數皆為9
               {
                   (*big)[i-1]++;
                   break;
               }
               else
                    (*big)[i]='0';
           }
           else//個位數不是9,個位數+1
           {
               (*big)[i]++;
               break;
           }
        }
    }
}
string number::getNumber()//回傳結果
{
    return *big;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
