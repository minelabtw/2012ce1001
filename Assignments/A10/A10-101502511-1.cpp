#include<iostream>
#include<string>
#include<iomanip>
using namespace std;
class number
{
public:
    number();
    void setNumber(string);
    void plusplus();
    string getNumber();
    int a;
private:
    string str;
    int n;
};
number::number()//建構子
{
    str[0]='\0';//重製字串
}
void number::setNumber(string str2)
{
    n=str2.length();
    str.resize(n+1);//新字串長度+1
    for(int  i=0; i<n; i++)
        str[i]=str2[i];//複製字串
}
void number::plusplus()
{
    a=0;//計算是否全部為9
    for(int  k=0; k<n; k++)
    {
        if(str[n-1-k]=='9')//判斷是否個位數為9
        {
            str[n-1-k]='0';//是的話+1進位成0
            a++;//
            if(a==n)//全部9的話
            {
                str[0]='1';
                str[n]='0';
            }
        }
        else
        {
            str[n-1-k]+=1;
            str[n]='\0';//最後一位為\0
            break;
        }
    }
}
string number::getNumber()
{
    return str;//回傳字串
}
int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
