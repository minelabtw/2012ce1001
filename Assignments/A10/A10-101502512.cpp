#include<iostream>
#include<string>
using namespace std;
//you must design a class for following main function
class number
{
public:
    number();//constructor
    void setNumber(string);
    void plusplus();
    string getNumber();
private:
    string n;
    int i;//the length of string
};
number::number()
{
    n ='0';
};
void number::setNumber(string num)
{
    n=num;
};
void number::plusplus()
{
    i=n.length();
    while(i>0)//run I times
    {
        if(n[i-1]=='9')//進位
        {
            n[i-1]='0';
        }
        else
        {
            n[i-1]+=1;
            break;
        }
        i--;
    }
    if(i==0)//當進位到第一位變成零
        cout<<'1';
};
string number::getNumber()
{
    return n;
};

int main()
{
    string num;
    number plusCalculator;//instantiate object plusCalculator of class number
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
