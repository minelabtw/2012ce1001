#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

class number
{
public:
    number();
    void setNumber( string& );
    void plusplus();
    string getNumber();
private:
    string str;
};

number::number()//初始化
{
    str.empty();
}

void number::setNumber( string &number )//set number.
{
    str = number;
}

void number::plusplus()//num++
{
    string oneStr( 1 , '1');
    int length = str.length();

    str[str.length()-1]++;

    for( int i = 1 ; i <= length ; i++ )//數字為9而加1時變成0
    {
        if( str.at(length-i) <= '9' )
            break;
        if( str.at(length-i) > '9' )//進位
        {
            str.at(length-i) = '0';
            if( length-1-i >= 0 )
                str.at(length-1-i)++;
            else    //超出原本字串長度的進位
            {
                oneStr = oneStr + str;
                str = oneStr;
                break;
            }
        }
    }
}

string number::getNumber()//回傳字串
{
    return str;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber( num );
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
