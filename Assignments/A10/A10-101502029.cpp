#include<iostream>
#include<string>
using namespace std;

class number
{
public:
    number();
    void setNumber( string );
    void plusplus();
    string getNumber();

private:
    string num;
};

number::number()
{
    num = " ";//將字串為空白
}
void number::setNumber(string num1)//將Nnumber在setNumber裡顯示出來
{
    num = num1;
}
//將Nnumber在plusplus裡顯示出來
void number::plusplus()//將數字加一的函式
{
    for( int i=0 ; i < num.length(); i++ )
    {
        if ( num[num.length()-1-i]=='9' )//如果數字是九，就轉為零
        {
            num[num.length()-1-i]='0';
        }
        else if ( num[num.length()-1-i]!='9' )//如果數字不是九，就加一
        {
            num[num.length()-1-i]++;
            break;
        }
        if( num[0]=='0')//首相為零時，陣列加一
        {
            num='1'+num;
            break;
        }
    }
}
string number::getNumber()//將Nnumber在getNumber裡顯示出來
{
    return num;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
