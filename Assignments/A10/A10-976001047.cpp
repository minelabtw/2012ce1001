#include<iostream>
#include<string>
using namespace std;

class number                   // build a class named number
{
public:
   number();
   void setNumber(string);
   void plusplus();
   string getNumber();
private:
   int n;
   int m;
   string a;
   string b;
};

number::number()
{
    n=0;
    a="0";
    b="0";
    m=0;
}

void number::setNumber(string num)
{
    n=num.length();                 //set some numbers and a string for use in plusplus.
    a=num;                          //n is the length of input .
    b="0";                          //string b is as long as string a,but its all elements is '0'.
    m=a.rfind('9');
    for(int i=0;i<n-1;i++)
    {
        b=b+"0";
    }

}

void number::plusplus()                //function for input plus one.
{
     int i=0;
     if(a[n-1]!='9')                   //plus one if  digit in ones is not nine.
     {
         a[n-1]=int(a[n-1])+1;
     }
     else                              //if digit in ones is nine ,the loop is for carry.
     {
         while (m<string::npos)
         {
             a.replace(m,1,"0");
             if (a[m-1]!='9')
             {
                 break;
             }
             else

             m=a.rfind('9');

         }
           a[i-1]=int(a[i-1])+1;
     }

     i=a.find('0');                     //if the hightest digit is zero,plus a string "1" to a.
     if (i==0)
     {
         a="1"+b;
     }

     a[i-1]=int(a[i-1])+1;

}

string number::getNumber()           //retrun string a to main.
{
    return a;
}

int main()
{
    string num;
    number plusCalculator;
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
