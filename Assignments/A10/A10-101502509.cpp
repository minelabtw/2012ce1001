#include<iostream>
#include<string>
using namespace std;
class number
{
public:
    number(string = " ");  //default constructor

    void setNumber(string );   //set cnum
    string getNumber();   //return cnum

    void plusplus();     //cnum++
private:
    string cnum;
};// end class number

number::number(string cm)  //initialize cnum to " "
{
    setNumber(cm);
}

void number::setNumber(string cn)  //set private cnum
{
    cnum=cn;
}
string number::getNumber()   //return cnum value
{
    return cnum;
}

void number::plusplus()    //let cnum add 1
{
    int size=cnum.length();
    bool flag=false;
    for(int i=size-1;i>=0;i--)
    {
        if(cnum[i]<'9')
        {
            cnum[i]=cnum[i]+1;  // check from the last item,
                                //  if one item smaller than '9',let the item value add 1
            for(int j=i+1;j<size;j++)
              (cnum)[j]='0';
            flag=true;
            break;
        }
    }
    if(flag==false)   //if each item of num is '9',put '1' in front of the first item,
                       //  and let other items become '0'
    {
        for(int i=0;i<size;i++)
            (cnum)[i]='0';
        (cnum)='1'+(cnum);
    }
}
int main()
{
    string num;
    number plusCalculator;  //build plusCalculator
    while( cin >> num )
    {
        plusCalculator.setNumber(num);
        plusCalculator.plusplus();
        cout << plusCalculator.getNumber() << endl;
    }
    return 0;
}
