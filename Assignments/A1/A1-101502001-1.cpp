#include <iostream>

using namespace std;

int main()
{
    int number1;
    int number2;
    int Perimeter;
    int Area;

    std::cout << "Please enter the rectangle length:";
    std::cin >> number1;

    std::cout << "Please enter the rectangle width:";
    std::cin >> number2;

    Perimeter = number1*2 + number2*2;
    Area = number1*number2;

    std::cout << "Perimeter is:" << Perimeter << std::endl;
    std::cout << "Area is:" << Area << std::endl;

}
