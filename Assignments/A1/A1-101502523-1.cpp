/*****************************************
* Assignment 1-1
* Name: 吳駿劭
* School Number: 101502523
* E-mail: kick2007sky@yahoo.com.tw
* Course: 2012-CE1001
* Submitted: 2012/09/27
*
***********************************************/

#include <iostream>

int main()
{
    int number1;
    int number2;
    int sum;

    std::cout << "Please enter the rectangle length:";
    std::cin  >> number1;

    std::cout << "Please enter the rectangle width:";
    std::cin  >> number2;

    sum = number1 * number2;

    std::cout << "Area is:" << sum;
    return 0;
}
