#include <iostream> 
#include <cstdlib> 
using namespace std;

int main()
{
    int input;
    
    cout << "Enter an integer: ";
    cin >> input;
    
    if ( (input % 2) == 0 )
    {
         cout << "The integer " << input << " is even." << endl;
    }
    else
    {
        cout << "The integer " << input << " is odd." << endl;
    }
    
    system("pause");
    return 0;
}
