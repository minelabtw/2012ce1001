#include <iostream>

using namespace std;

int main()
{
    int number1;
    int number2;
    int ans1;
    int ans2;

    cout << "Please enter the rectangle length:";
    cin >> number1;

    cout << "Please enter the rectangle width:";
    cin >> number2;

    ans1 = 2 * (number1 + number2);
    ans2 = number1 * number2;

    cout << "Perimeter is:" << ans1 << endl;
    cout << "Area is:" << ans2 << endl;

    return 0;
}
