 #include<iostream>//Enable cout/cin function
 using namespace std;

int main() //Start of program
{
    //Variable declaration
    int a,b,c,d; //Use "a", "b", "c", "d" as variables
    cout<<"Please enter the rectangle length:"; //Show the text on the screen to tell user to input a integer
    cin>>a; //We take this integer as variable "a"
    cout<<"Please enter the rectangle width:"; //Show the text on the screen to tell user to input another integer
    cin>>b; //We take this integer as variable "b"
    c=a*b; //Indicate that variable "c" is the area of rectangle
    d=a*2+b*2;//Indicate that variable "d" is the perimeter of rectangle
    cout<<"Perimeter is:"<<d<<"\n";//Show the text on the screen to tell the user the perimeter of the rectangle
    cout<<"Area is:"<<c<<endl; //Show the text on the screen to tell the user the area of the rectangle
    return 0; //End of program
}
