#include<iostream>
using std::cin;
using std::cout;
using std::endl;
int main()
{
    int len,wid;
    cout << "Please enter the rectangle length: ";
    cin >> len;
    cout << "Please enter the rectangle width: ";
    cin >> wid;
    cout << "Perimeter is: " << (len+wid)*2 <<endl;
    cout << "Area is: " << len*wid <<endl;
    return 0;
}
