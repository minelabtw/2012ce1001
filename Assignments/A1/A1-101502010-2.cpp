#include<iostream>
using std::cin;
using std::cout;
using std::endl;
int main()
{
    int num;
    cout << "Enter an integer: ";
    cin >> num;
    if(num%2!=0)
        cout << "The integer " << num << " is odd." << endl;
    else
        cout << "The integer " << num << " is even." << endl;
        return 0;
}
