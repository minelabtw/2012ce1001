//Assignment1-1 by J

#include <iostream>

using namespace std;

int main()
{
    double len ;
    double wid ;
    double area ;
    double peri ;

    //anouncing all the variables used in the program ,including length(len).width(wid).area.perimeter(peri)

    cout << "Please enter the rectangle length: " ;
    cin >> len ;
    cout << "Please enter the rectangle width: " ;
    cin >> wid ;
    area = len * wid ;
    peri = 2*(len + wid) ;
    cout << "Perimeter is: " << peri << endl ;
    cout << "Area is: " << area ;

    //defining all the variables and the arithmetic function , setting all the function of this program

    return 0;
}
