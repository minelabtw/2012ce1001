#include<iostream>
using namespace std;

int main()
{
    start:

    int x;

    cout<<"Enter an integer: ";
    cin>>x;

    if(!cin)
    {
        cout<<"Error!";
        cin.clear();
        cin.ignore();
        cout<<endl;
        goto start;
    }

    if(x%2==0)
        cout<<"The integer "<<x<<" is even.\n";
    else
        cout<<"The integer "<<x<<" is odd.\n";

    return 0;

}
