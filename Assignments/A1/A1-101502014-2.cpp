#include <iostream>
using namespace std;
int main()
{
    int n;
    cout << "Enter an integer: ";
    cin >> n;
    cout << "The integer " << n << " is ";
    if (n%2==0)
        cout << "even.";
    else
        cout << "odd.";
    return 0;
}
