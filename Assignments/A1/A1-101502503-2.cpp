#include<iostream>

int main()
{
    int number1;
    int sum1;

    std::cout << "Enter an integer:\t";
    std::cin >> number1;

    sum1 = number1 % 2 ;

    if ( sum1 == 0 )
    std::cout << "The integer\t" << number1 << "\tis even" << std::endl;

    if ( sum1 == 1 )
    std::cout << "The integer\t" << number1 << "\tis odd" << std::endl;

    return 0 ;
}
