#include <iostream>

using namespace std;

int main()
{
    int number1;
    int number2;
    int perimeter;
    int area;

    cout << "Please enter the rectangle length:";
    cin >> number1;
    cout << "Please enter the rectangle width:";
    cin >> number2;

    perimeter = (number1 + number2)*2;
    area = number1 * number2;
    cout << "Perimeter is:" << perimeter << endl ;
    cout << "Area is:" << area << endl ;

return 0;
}
