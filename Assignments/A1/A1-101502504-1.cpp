#include <iostream>
int main()
{
    double number1;
    double number2;
    double sum1;
    double sum2;

    std::cout << "Please enter the rectangle length: " ;
    std::cin >> number1;

    std::cout << "Please enter the rectangle width: " ;
    std::cin >> number2;

    sum1 = 2*(number1 + number2);
    std::cout << "Perimeter is: " << sum1 << std::endl;

    sum2 = number1 * number2;
    std::cout << "Area is: " << sum2 << std::endl;
}
