#include <iostream>

int main()
{
    int number1;
    int odd;


    std::cout<<"Enter an integer:";
    std::cin>>number1;

    odd = number1 % 2;

    if  (odd == 1)
        std::cout<<"The integer "<<number1<<"is odd.";

    else
        std::cout<<"The integer "<<number1<<" is even.";

    return 0;
}
