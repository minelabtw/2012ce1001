#include <iostream>

int main()
{
    int number1;
    int sum;

    std::cout << "Enter an integer: ";
    std::cin >> number1;

    sum = number1 % 2;

    if ( sum == 1 )
    {
        std::cout << "The integer " << number1 << " is odd.";
    }
    else
    {
        std::cout << "The integer " << number1 << " is even." << std::endl;
    }

    return 0;
 }
