#include <iostream>
int main()
{
    int number1;
    int number2;
    int answer1;
    int answer2;

    std::cout << "Please enter the rectangle length: ";
    std::cin >> number1;

    std::cout << "Please enter the rectangle width: ";
    std::cin >> number2;

    answer1 = ( number1 + number2 ) *2;

    answer2 = number1 * number2;
    std::cout << "Perimeter is: " << answer1 << std::endl;
    std::cout << "Area is: " << answer2 << std::endl;

    return 0;
}
