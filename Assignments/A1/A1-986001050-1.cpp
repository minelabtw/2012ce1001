#include <iostream>

using namespace std;

int main()
{
    int a;
    int b;
    int perimeter;
    int area;

    cout << "Please enter the rectangle length: " ;
    cin  >> a;

    cout << "Please enter the rectangle width: " ;
    cin  >> b;

    perimeter = a+b+a+b;
    area = a*b;
    cout << "perimeter is: " << perimeter << endl;
    cout << "area is: " << area << endl;

    return 0;
}
