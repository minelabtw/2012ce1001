#include <iostream>

using namespace std;

int main()
{
    int iInt;
    cout << "Enter an integer:"  ;
    cin >>iInt;
    if(iInt%2==0)
    {
        cout<<"The integer "<<iInt<<" is even";
    }
    else
    {
        cout<<"The integer "<<iInt<<" is odd";
    }
    return 0;
}
