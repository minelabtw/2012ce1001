#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1;
    int number2;
    int sum1;
    int sum2;

    cout << "Please enter the rectangle length: ";
    cin >> number1;

    cout << "Please enter the rectangle width: ";
    cin >> number2;

    sum1 = ( number1 + number2 ) * 2;
    sum2 = number1 * number2;

    cout << "Perimeter is: " << sum1 << endl;

    cout << "Area is: " << sum2 << endl;
}
