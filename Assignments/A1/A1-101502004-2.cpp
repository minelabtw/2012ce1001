#include <iostream>

using namespace std;

int main()
{
    int number;
    cout << "Enter an integer: ";
    cin >> number;

    if (1 == number % 2)
    cout << "The integer " << number << " is odd." << endl;

    if (0 == number % 2)
    cout << "The integer " << number << " is even." << endl;
}
