#include <iostream>

int main()
{
    double x;
    double y;
    double P;
    double A;

    std::cout << "Please enter the rectangle length: ";
    std::cin >> x;

    std::cout << "Please enter the rectangle width: ";
    std::cin >> y;

    P = 2*x + 2*y;
    std::cout << "Perimeter is: " << P <<std::endl;

    A = x*y;
    std::cout << "Area is: " << A <<std::endl;

    return 0;
}
