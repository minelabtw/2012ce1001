#include <iostream>
int main()
{
    int number1;

    std::cout << "Enter an integer: ";
    std::cin >> number1;

    if (number1%2==0) //number1%2==0 means that the entered number divided by 2.
    std::cout << "This integer " << number1 << " is even." << std::endl; //and if the remainder is equal to 0, the "number1" is even.
    else
    std::cout << "This integer " << number1 << " is odd." << std::endl; //and if the reaminder isn't equal to 0, the "number1" is odd

    return 0;
}
