#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int n;
    cin >> n;
    if(n%2==1)
        cout << "The integer " << n << " is odd.";
    else
        cout << "The integer " << n << " is even.";
    return 0;
}
