/*****************************************
* Assignment 1-2
* Name: ���a��
* School Number: 101303053
* E-mail: george1108zzz@hotmail.com
* Course: 2012-CE1001
* Submitted: 2012/09/29
*
***********************************************/

#include<iostream>

int main()
{
    int input;
    int ans;

    std::cout<<"Enter an integer:";
    std::cin>>input;

    ans=input%2;

    std::cout<<"The integer ";
    std::cout<<input;
    std::cout<<" is ";

    if(ans==0)
    {
        std::cout<<"even";
    }

    else
    {
        std::cout<<"odd";
    }
}
