#include <iostream>

using namespace std;

int main()
{
    int length;
    int width;
    cout << "Please enter the rectangle length:" ;
    cin >> length ;
    cout << "Please enter the rectangle width:" ;
    cin >> width;
    cout << "Area is :" << length * width << " \nPerimeter is : " << 2*length + 2*width ;

    return 0;
}
