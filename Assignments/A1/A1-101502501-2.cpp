#include <iostream>

using namespace std;

int main()
{
    int number1;
    int judge;

    cout << "Enter an integer: ";
    cin >> number1;

    judge = number1 % 2;

    if (judge == 0)
       cout << " The integer " << number1 << " is even. ";
    else
       cout << " The integer " << number1 << " is odd. ";

       return 0;

}
