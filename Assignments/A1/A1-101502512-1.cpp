#include<iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1;
    int number2;
    int Perimeter;
    int Area;

    cout << "Please enter rectangle length:";
    cin >> number1;

    cout << "Please enter rectangle width:";
    cin >> number2;

    Perimeter = (number1 + number2)*2;
    cout << "Perimeter is " << Perimeter << endl;

    Area = number1 * number2;
    cout << "Area is " << Area << endl;
}
