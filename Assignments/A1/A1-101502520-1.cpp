#include <iostream>

using namespace std;

int main()
{
    int a,b,ans1,ans2;
    cout<<"Please enter the rectangle length: ";
    cin>>a;
    cout<<"Please enter the rectangle width: ";
    cin>>b;
    ans1=a*b;
    ans2=2*(a+b);
    cout<<"Perimeter is: "<<ans2<<endl;
    cout<<"Area is: "<<ans1<<endl;

    return 0;
}
