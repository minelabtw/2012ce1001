#include <iostream>
int main()
{
    int length;
    int width;
    int sum;
    int area;

    std::cout << "Please enter the rectangle length: ";
    std::cin >> length;
    std::cout << "Please enter the rectangle width: ";
    std::cin >> width;

    sum = (length + width) * 2;
    std::cout << "Perimeter is: " << sum << std::endl;
    area = length * width;
    std::cout<< "Area is: " << area << std::endl;
}
