#include<iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int integer;

    cout << "Enter an integer:";
    cin >> integer;

    if ( integer % 2 == 0 )
       cout << "The integer " << integer << " is even." << endl;
    else
       cout << "The integer " << integer << " is odd." << endl;
}
