#include <iostream>

int main()
{
    int number1;
    int number2;
    int sum1;
    int sum2;

    std::cout << "Please enter the rectangle length:";
    std::cin>> number1;

    std::cout <<"Please enter the rectangle width:";
    std::cin >> number2;

    sum1 = number1 * number2;
    sum2 = ( number1 + number2 ) * 2;

    std::cout <<"Perimeter is:"<< sum2 << std::endl;
    std::cout <<"Area is:"<< sum1 << std::endl;

}
