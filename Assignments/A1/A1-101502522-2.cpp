#include <iostream>

int main()
{
    int number;

    std::cout << "Enter an integer: ";
    std::cin >> number;

    if (number%2==1)
    std::cout << "The integer " << number <<" is odd.";

    else
    std::cout << "The integer " << number <<" is even.";

    return 0;
}
