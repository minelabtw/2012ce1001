#include <iostream>

using namespace std;

int main()
{
    int x;
    int sum;

    cout << "Enter an integer:";
    cin >> x;

    sum = (x)%2;

    if (sum)
      cout << "The integer " << x << " is odd.";
    else
      cout << "The Integer " << x << " is even." << endl;
    return 0;
}
