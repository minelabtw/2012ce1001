#include <iostream>

int main()
{
 int a ;
 int b ;
 std::cout << "Enter an integer: " ;
 std::cin >> a ;
 b = a % 2 ;

 if (b==1)
    std::cout <<"The integer " << a <<" is odd.";
 else
    std::cout <<"The integer " << a <<" is even.";

}
