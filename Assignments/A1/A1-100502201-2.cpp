#include <iostream>

using namespace std;

int main()
{
    cout << "Enter an integer:";
    int a;
    cin>>a;
    if(0==a%2)
        cout<<"The integer "<<a<<" is even.";
    else
        cout<<"The integer "<<a<<" is odd.";
    return 0;
}
