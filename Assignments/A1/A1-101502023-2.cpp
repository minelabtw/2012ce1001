#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number;
    int sum;

    cout << "Enter an integer ";
    cin >> number;

    sum = number % 2;

    if ( sum == 0 )
    cout << "The integer " << number << " is even" << endl;
    if ( sum == 1 )
    cout << "The integer " << number << " is odd" << endl;
}

