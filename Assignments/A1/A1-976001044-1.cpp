#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    int num1, num2, res_peri, res_area;
    
    cout << "Please enter the rectangle length: ";
    cin >> num1;
    cout << "Please enter the rectangle width: ";
    cin >> num2;
    
    res_peri = (num1 + num2) * 2;
    res_area = num1 * num2;
    
    cout << "Perimeter is: " << res_peri << endl;
    cout << "Area is: " << res_area << endl;
    
    system("pause");
    return 0;
}
