#include <iostream>
#include <stdlib.h>
using std::cout;
using std::cin;
using std::endl;

int main()

{
    float length,width,Perimeter,Area;

    cout << "Please enter the rectangle length: ";
    cin >> length;
    cout << "Please enter the rectangle width: ";
    cin >> width;

    Perimeter = (length+width)*2;
    Area = length*width;

    cout << "Perimeter is: "<< Perimeter;
    cout << "\nArea is: "<< Area << endl;
   system("pause");
 }
