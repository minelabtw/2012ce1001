#include <iostream>

using namespace std;

int main()
{
    int ans;
    cout<<"Enter an integer: ";
    cin>>ans;
    if(ans%2==1) cout<<"The integer "<<ans<<" is odd.";
    else cout<<"The integer "<<ans<<" is even.";
    return 0;
}
