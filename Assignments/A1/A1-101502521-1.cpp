#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    int length, width;
    cout << "Please enter the rectangle length: ";
    cin >> length;
    cout << "Please enter the rectangle width: ";
    cin >> width;
    cout << "Perimeter is: " << 2*(length+width) << endl;
    cout << "Area is: " << length*width << endl;
    return 0;
}
