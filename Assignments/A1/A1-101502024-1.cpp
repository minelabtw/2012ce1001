#include <iostream>



int main()
{


    int n1;
    int n2;
    int sum1;
    int sum2;

    std::cout << "Please enter the rectangle length: ";
    std::cin >> n1;

    std::cout <<  "Please enter the rectangle width: ";
    std::cin >> n2;

    sum1= n1 * n2;
    sum2= 2 * n1 + 2 * n2;

    std::cout << "area is " << sum1 << std::endl;
    std::cout << "perimeter is " << sum2 << std::endl;
}
