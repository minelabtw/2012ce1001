#include <iostream>

using namespace std;

int main()
{
    int n1;
    int n2;

    cout << "Enter an integer: ";
    cin>>n1;

    n2=n1%2;

    if (n2==1)
       cout<<"The integer "<<n1<<" is odd."<<endl;
    else
       cout<<"The integer "<<n1<<" is even."<<endl;

}
