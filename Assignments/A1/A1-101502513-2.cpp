#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number;

    cout << "Enter an integer: ";
    cin >> number;

    if (number % 2 == 1)
        cout << "The integer " << number << " is odd." << endl;
    else
        cout << "The integer " << number << " is even." << endl;
}
