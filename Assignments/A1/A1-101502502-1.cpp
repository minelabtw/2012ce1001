#include <iostream>

int main()
{
    int number1;
    int number2;
    int sum1, sum2;

    std::cout << "Please enter the rectangle length: ";
    std::cin >> number1;

    std::cout << "Please enter the rectangle width: ";
    std::cin >> number2;

    sum1 = 2*( number1 + number2 );
    sum2 = number1 * number2 ;

    std::cout << "Perimeter is: " << sum1;
    std::cout << "\nArea is: " << sum2 << std::endl;

    return 0;
}
