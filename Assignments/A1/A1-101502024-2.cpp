#include <iostream>



int main()
{
    int n1;

    std::cout << "Enter an integer: ";
    std::cin >> n1;

    if ( n1 % 2 == 1 )
    std::cout << "The integer " << n1 << " is odd" << std::endl;

    if ( n1 % 2 == 0 )
    std::cout << "The integer " << n1 << " is even" << std::endl;
}
