//Assignment1-2 by J

#include <iostream>

using namespace std;

int main()
{
    int inp;
    int rmd;

    //define all the variables input as inp , remainder

    cout << "Enter an integer: " ;
    cin >> inp ;
    rmd = inp % 2 ;

    //define the arithmetic function and discribe the input page

    if (rmd == 0)
       cout << "The integer " << inp << " is even." ;
    if (rmd == 1)
       cout << "The integer " << inp << " is odd." ;
    return 0;
}
