#include <iostream>

using std::cout;
using std::cin;
using std::endl;


int main()
{
    int number1;
    int number2;
    int area;
    int Perimeter;

    cout << "Please enter the rectangle length : ";
    cin >> number1;

    cout << "Please enter the rectangle width : ";
    cin >> number2;

    area = number1 * number2;
    Perimeter = number1*2 + number2 * 2;

    cout << "Perimeter is : " << Perimeter << "\n";
    cout << "Area is : " << area << endl;


    return 0;
}
