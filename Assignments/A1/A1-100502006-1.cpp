#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include <string>
#include <ctype.h>
#include <stdio.h>

using namespace std;

class CRectangle
{
    public:
        string strRecLength;
        string strRecWidth;
        int iRecLength;
        int iRecWidth;
        char chReplay;
        void PrintGraph();
        void LengthInput();
        void WidthInput();
        void RecOutput();
    private:
        bool CheckInput(string str);

};

void CRectangle::PrintGraph()
{
    system("CLS");

    cout << "◢▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇◣" << endl;
    cout << "█                                                                        █" << endl;
    cout << "█    ▅▅▅▅▅▅▅            ◥                                        █" << endl;
    cout << "█    ▌                           ◥               ▅▅▅▅▅       ◢   █" << endl;
    cout << "█    ▌▅▅▅▅▅▅                  ◥              ▌    ▌     ◢     █" << endl;
    cout << "█    ▌                  ▅▅▅▅▅▅▅▅▅▅▅      ▌    ▌   ◢       █" << endl;
    cout << "█    ▌                          ▊                  ▌    ▌            █" << endl;
    cout << "█    ▌▅▅▅▅▅▅              ▊                  ▌    ▌            █" << endl;
    cout << "█    ▌                          ▊                  ▌    ▌         ◢ █" << endl;
    cout << "█    ▌                          ▊                  ▌    ▌     ◢◢   █" << endl;
    cout << "█  ▅▅▅▅▅▅▅▅▅          ◢     ◢◢◢◣  ▅▅▅▅▅▅▅ ◢        █" << endl;
    cout << "█    ▌                        ◢ ◢◢       ◣      ▌    ▌            █" << endl;
    cout << "█    ▌             ◢        ◢◢            ◢     ▌    ▌            █" << endl;
    cout << "█    ▌   ◣     ◢          ◢               ◢     ▌    ▌         ◢ █" << endl;
    cout << "█    ▌      ◣             ◢              ◢       ▌    ▌     ◢◢   █" << endl;
    cout << "█    ▌         ◣         ◢              ◢        ▌    ▌   ◢◢     █" << endl;
    cout << "█    ▌            ◣     ◢              ◢        ◢     ▌ ◢         █" << endl;
    cout << "█    ◤              ◣ ◢              ◢                 ▌            █" << endl;
    cout << "█                                                                        █" << endl;
    cout << "◥▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇▇◤" << endl;
}

bool CRectangle::CheckInput(string str)
{
    if(str.c_str()[0]!=45)
     {
        for(int i = 0; i < str.length(); i++)
        {
           if( str.c_str()[i] < '0' || str.c_str()[i] > '9' )
           { return false;}
         }
        return true;
     }
     else
     {
       for(int i = 1; i < str.length(); i++)
        {
           if( str.c_str()[i] < '0' || str.c_str()[i] > '9' )
           { return false;}
        }
       return true;
     }
}

void CRectangle::LengthInput()
{
    PrintGraph();
    while(1)
    {
        cout << "\n         請輸入 長方形 的 \"長度\" ：";
        cin>>strRecLength;
        if(CheckInput(strRecLength))
        {
             iRecLength=atoi(strRecLength.c_str());
             break;
        }
        else
        {
            PrintGraph();
            cout<<"\n         輸入錯誤, 請重新輸入！";
        }
    }
}


void CRectangle::WidthInput()
{
    PrintGraph();
    while(1)
    {
        cout << "\n         請輸入 長方形 的 \"長度\" ："<<iRecLength<<"              還有 \"寬度\" ：";
        cin>>strRecWidth;
        if(CheckInput(strRecWidth))
        {
             iRecWidth=atoi(strRecWidth.c_str());
             break;
        }
        else
        {
            PrintGraph();
            cout<<"\n         輸入錯誤, 請重新輸入！";
        }
    }
}


void CRectangle::RecOutput()
{
    system("CLS");

        cout << "\n";
        cout << "         你輸入長方形  的  長 ："<<iRecLength<<"       寬 ："<<iRecWidth<< endl;
        cout << "\n    ";
        //以下指令作長方形繪圖
        if(iRecLength>1)
        {
            for (int i =0;i<iRecLength;i++)
            {
                cout << "▃";
            }

            for (int i =0;i<iRecWidth;i++)
            {
                cout << "\n    ▌ ";

                for(int j =0; j<iRecLength-2; j++)
                {
                    cout << "  ";
                }

                cout << "▌";
            }
        }

        else
        {
            cout << "▁";
            for (int i =0;i<iRecWidth;i++)
                {
                    cout << "\n    ▏▏";
                }
        }

        cout << "\n    ";

        for (int i =0;i<iRecLength;i++)
        {
            cout << "￣";
        }
        //以上指令作長方形繪圖

        cout << "\n";
        cout<<"          周長 : "<< 2*(iRecLength+iRecWidth);
        cout <<"                面積 : "<<iRecLength*iRecWidth<<endl;

}




int main()
{
    CRectangle rec;
    while(1)
    {
        rec.LengthInput();
        rec.WidthInput();
        rec.RecOutput();
        cout << "\n\n";
        cout<<" 按 Enter鍵 返回, 按 q 離開......";
        do
        {
            rec.chReplay=getch();
            cin.clear();
        }while((rec.chReplay != 13)&&(rec.chReplay != 'q'));

        if(rec.chReplay=='q')
            {
                break;
            }
    }
    return 0;
}
