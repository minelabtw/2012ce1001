#include <iostream>

int main()
{
    int x;
    std::cout << "Enter an integer: ";
    std::cin >> x;

    if (x%2 == 1)
        std::cout << "The integer " << x << " is odd.";
    else
        std::cout << "The integer " << x << " is even.";

    return 0;
}
