#include <iostream>


int main()
{

    int number1;
    int number2;
    int perimeter;
    int area;

std::cout << "Please enter the rectangle length:";
std::cin >> number1;

std::cout << "Please enter the rectangle width:";
std::cin >> number2;

perimeter = (number1 + number2)*2;
area = number1 * number2;

std::cout << "Perimeter is:" << perimeter << std::endl;
std::cout << "Area is:" << area << std::endl;
}
