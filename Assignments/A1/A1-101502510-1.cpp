#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int number1;
    int number2;
    int sum;

    cout <<"Please enter the rectangle length:";
    cin  >>number1;

    cout <<"Please enter the rectangle width:";
    cin  >>number2;

    sum = 2*number1 + 2*number2;
    cout <<"Perimeter is:"<<sum<<endl;

    sum = number1 * number2;
    cout <<"Area is:"<<sum<<endl;
}
