#include <iostream>

using namespace std;

int main()
{
   int len = 0;
   int wid = 0;
   int per = 0;
   int are = 0;

   cout << "Please enter the rectangle length:";
   cin >> len;
   cout << "Please enter the rectangle width:";
   cin >> wid;
   per = (len+wid)*2;
   are = len*wid;
   cout << "Perimeter is:"<<per<<endl;
   cout << "Area is:"<<are<<endl;

   return 0;
}
