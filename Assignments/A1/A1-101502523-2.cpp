/*****************************************
* Assignment 1-2
* Name: 吳駿劭
* School Number: 101502523
* E-mail: kick2007sky@yahoo.com.tw
* Course: 2012-CE1001
* Submitted: 2012/09/27
*
***********************************************/

#include <iostream>

using namespace std;

int main()
{
    int number;

    cout << "Enter an integer:";
    cin  >> number;

    if (number%2==1)
    {
       cout << "The integer " << number << " is odd." << endl;
    }
    else if (number%2==0)
    {
       cout << "The integer " << number << " is even." << endl;
    }
}
