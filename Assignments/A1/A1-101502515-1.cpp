#include <iostream>

using namespace std;

int main()
{
    int int_length;
    int int_width;
    int int_area;
    int int_perimeter;

    cout << "Please enter the rectangle length: ";
    cin >> int_length;
    cout << "Please enter the rectangle width: ";
    cin>> int_width;

    int_perimeter = (int_length + int_width)*2;
    int_area = int_length * int_width;

    cout << "Perimeter is: "<<int_perimeter<<endl;
    cout << "Area is: "<<int_area<<endl;

}
