#include <iostream>

int main()
{
    int lengthx , lengthy ;
    std::cout << "Please enter the rectangle length: ";
    std::cin >> lengthx;
    std::cout << "Please enter the rectangle width: ";
    std::cin >> lengthy;
    std::cout << "Perimeter is: " << 2*(lengthx+lengthy) << std::endl;
    std::cout << "Area is: " << lengthx*lengthy;
    return 0;
}
