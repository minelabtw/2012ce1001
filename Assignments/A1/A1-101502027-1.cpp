#include <iostream>

using namespace std;

int main()
{
    int n1;
    int n2;
    int s1;
    int s2;

    cout << "Please enter the rectangle length: ";
    cin>>n1;

    cout<<"Please enter the rectangle width: ";
    cin>>n2;

    s1=n1*n2;
    s2=(n1+n2)*2;

    cout<<"Perimeter is: "<<s2<<endl;
    cout<<"Area is: "<<s1<<endl;

}
