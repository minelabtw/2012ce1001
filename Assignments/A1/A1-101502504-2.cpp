#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int number1;
    int modulus;

    cout << "Enter an integer: ";
    cin >> number1;
    modulus = number1%2;

    if (modulus == 1)
    {
       cout << "The integer " << number1 << " is odd" << std::endl;
    }
    else if (modulus == 0)
    {
       cout << "The integer " << number1 << " is even" << std::endl;
    }
}
