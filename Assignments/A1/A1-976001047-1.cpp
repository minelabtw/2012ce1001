#include <iostream>
#include <cstdlib>
int main()
{
    int x1;
    int x2;
    int P;
    int A;
    
    std::cout << "Please enter the rectangle length:";
    std::cin >> x1 ;

    std::cout << "Please enter the rectangle width:";
    std::cin >> x2 ;
     
    P = 2*(x1+x2);
    std::cout << "Perimeter is:" << P << std::endl;
    
    A = x1*x2;
    std::cout << "Area is:" << A << std::endl;
    
    system("PAUSE");
    return 0;
}
