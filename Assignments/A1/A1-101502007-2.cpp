#include <iostream>
using namespace std;

int main()
{
    int num;
    cout<<"Enter an integer:";
    cin>>num;
    if (num%2==1)
    cout<<"The integer "<<num<<" is odd.";
    else
    cout<<"The integer "<<num<<" is even.";
    return 0;
}
