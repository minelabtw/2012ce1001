#include <iostream>

using namespace std;

int main()
{
    int number1;

    std::cout <<"Enter an integer:";
    std::cin >>number1;

    if (number1%2 == 1)
    {
    std::cout <<"The integer " << number1 << " is odd.";
    }

    else
    {
    std::cout <<"The integer " << number1 << " is even.";
    }
}
