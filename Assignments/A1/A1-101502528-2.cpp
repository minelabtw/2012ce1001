#include <iostream>
int main()
{
    int number1;

    std::cout << "Enter an integer: ";
    std::cin >> number1;

    if (1 == number1 % 2)
        std::cout << "The integer " << number1 << " is odd.";
    else
        std::cout << "The integer " << number1 << " is even." << std::endl;
}
