#include <iostream>

using namespace std;

int main()
{
    int number1;
    int number2;

    cout << "Enter an integer:";
    cin >> number1;

    number2 = number1 % 2;

    if (number2 == 0)
        cout << "The integer " << number1 << " is even." << endl;
    else
        cout << "The integer " << number1 << " is odd." << endl;

    return 0;
}
