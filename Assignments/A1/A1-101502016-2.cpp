#include <iostream>
using namespace std;

int main()
{
    int number1;
    int answer;

    cout << "Enter an integer: ";
    cin >> number1;
    answer = number1 % 2;

    if (answer == 1)
    cout << "The integer " << number1 << " is odd\n";

    else
    cout << "The integer " << number1 << " is even";

    return 0;

}



