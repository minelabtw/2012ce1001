#include <iostream>

using std::cout ;
using std::cin ;
using std::endl ;

int main()
{
    int length ;
    int width ;
    int sum1 ;
    int sum2 ;

    cout << "Please enter the rectangle length:" ;
    cin >> length ;

    cout << "Please enter the rectangle width:" ;
    cin >> width ;

    sum1 = (length+width)*2 ;
    cout << "Perimeter is :" << sum1 << endl ;

    sum2 = length*width ;
    cout << "Area is :" << sum2 << endl ;



}
