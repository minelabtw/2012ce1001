#include<iostream>
using namespace std;
int main()
{
    int i,j,backup1,backup2,backupi,backupj,counter=1,max=1;
    while (cin>>i>>j)
    {
        backup1=i;//used for the while loop1.
        backup2=j;//used for the while loop2.
        backupi=i;//used for record the original i.
        backupj=j;//used for record the original j.
        if (backupi < 0 || backupi > 1000000 || backupj < 0 || backupj > 1000000) // error detection:negative or 1000000.
            return 0;
        while (backup1 <= backupj)//while loop1.(for i>j)
        {
            while (i != 1) // if i=1 break the loop.
            {
                if (i % 2 ==1) //if i is odd, i=3i+1.
                    i=3*i+1;
                else // if i is even, i=i/2.
                    i=i/2;
                counter++; //used for count the Cycle-length.
                if (counter>max)//record the max Cycle-length.
                    max=counter;
            }
            counter=1;
            backup1++;
            i=backup1;
        }
        while (backup2 <= backupi)//while loop2.(for j>i)
        {
            while (j != 1) // if j=1 break the loop.
            {
                if (j % 2 ==1) //if j is odd, j=3j+1.
                    j=3*j+1;
                else // if j is even, j=j/2.
                    j=j/2;
                counter++; //used for count the Cycle-length.
                if (counter>max)//record the max Cycle-length.
                    max=counter;
            }
            counter=1;
            backup2++;
            j=backup2;
        }
        cout<<backupi<<" "<<backupj<<" "<<max;
        max=1;
    }
    return 0;
}
