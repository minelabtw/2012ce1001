#include <iostream>
#include <limits>
#include <cstdlib>

using namespace std;

int main()
{
   int num1,num2,swap,item=0,max=0;
   do
   {
      cin >> num1>>num2;
      cin.clear(); //清除緩衝區資料
      cin.ignore(numeric_limits<int>::max(), '\n');  //忽略緩衝區錯誤資料

      if(cin==false || cin.gcount()!=1 || num1<=0 || num1>1000000)
          return -1;      // check num1,input character,negative number,and bigger than 1000000,finish the program
      else if(cin==false || cin.gcount()!=1 || num2<=0 || num2>1000000)
          return -1;      // check num2,input character,negative number,and bigger than 1000000,finish the program
      cout <<num1<<" "<<num2<<" ";

      if(num1>num2)  //if num1 is bigger than num2,exchange them
      {
          swap=num1;
          num1=num2;
          num2=swap;
      }

      while(num1<=num2)  //if num1 is bigger than num2,get out of the loop
      {
         int changenum=num1;

         while(changenum>1)  //Calculate  cycle-length of each number between num1 and num2
         {
             if(changenum%2==1)//if num is an odd number,let number =3*number+1 and let item add 1
             {
               changenum=3*changenum+1;
               item++;
             }
             else if(changenum%2==0)//if num is an even number,let number =number/2 and let item add 1
             {
               changenum=changenum/2;
               item++;
             }
         }
         item++;//Becuse when num=1,get out of the loop.We donn't calculate num=1 this item,we must calculate it.
         if(item>max)  // Find the largest cycle-length of each number between num1 and num2
             max=item;

         item=0;  //Restart item
         num1++;  //check next number smaller than num2
      }
      cout << max <<endl;

      num1=0;  //Restart num1
      num2=0;  //Restart num2
      item=0;  //Restart item
      max=0;   //Restart max
      //Restart again until both of the numbers are equal to 1
   }while(num1!=1 && num2!=1);

   system("pause");
   return 0;

}
