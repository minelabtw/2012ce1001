#include<iostream>

using namespace std ;

int main ()
{


    while(1)
    {

    int n , N , counter = 1 ;

    cout << "Enter positive integer n : " ;

    if ( cin >> n == false || n < 0 || n > 1000000 ) //if input is invalid(including not a number, negative or over 1000000), it will end.
        return 0 ;

    N = n ; //use N to maintain the original value(input)

    while ( n>=1 )
    {
        if ( n % 2 == 1 )//if n is odd
            n = n * 3 + 1 ;

        else if ( n % 2 == 0 )//if n is even
            n = n / 2 ;

        counter = counter + 1 ;//use counter to count cycle-length

        if ( n == 1 )//when n is 1, it will end.
            break ;
    }

    cout << "Cycle-length of " << N << " is " << counter << endl ;


    }
}
