#include <iostream>

using namespace std;

int main()
{
    int i,j,k,l=1,n,max;
    while(cin >>i && cin >>j)
    {
        max=0;//初始化
        if(i>j)
        {
            k=i;
            i=j;
            j=k;
        }
        if(i<=0 || j>1000000)
            return 0;
        cout<<i<<" "<<j<<" ";
        for(i;i<j;i++)
        {
            n=i;//儲存i的數在n
            while(n!=1)//算cycle-length
            {
                if(n%2==1)
                {
                    n=n*3+1;
                    l++;
                }
                if(n%2==0)
                {
                    n=n/2;
                    l++;
                }
            }
            if(l>max)//找出最大數
                max=l;
            l=1;
        }
        cout<<max<<endl;
    }
}

