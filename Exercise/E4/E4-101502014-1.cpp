#include<iostream>
using namespace std;
int main()
{
    int n;
    cout << "Enter positive integer n : ";
    while(cin >> n && n > 0 && n <= 1000000) //判斷n是否為負數,字元,或超過規定的範圍(1~1000000)
    {
        int sum = 1 , a = n; //sum紀錄n的cycle-lenth , a紀錄n的原始值
        while(n != 1) //n = 1時則跳出迴圈
        {
            if(n % 2 == 1) //判斷n是否為奇數
            {
                n = 3 * n + 1;
                sum++;
            }
            else //判斷n是否為偶數
            {
                n = n / 2;
                sum++;
            }
        }
        cout << "Cycle-length of " << a << " is " << sum << endl;
        cout << "Enter positive integer n : ";
    }
	return 0;
}
