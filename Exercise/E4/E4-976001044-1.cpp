#include <iostream>
#include <cstdlib>
#include <cmath>
int CycleLength(int);
using namespace std;

int main()
{
    int input=0, length=0;

    cout << "Enter positive integer n : ";
    while ( cin >> input != false )
    {
        if ( input <= 0 )
        {
            break;
        }
        else if ( input > 1000000 )
        {
            break;
        }
        else
        {
            length = CycleLength(input);
            cout << "Cycle-length of " << input << " is " << length << endl;
        }
        cout << "Enter positive integer n : ";
    }

    system("pause");
    return 0;
}

int CycleLength(int input)
{
    int count=0;
    while (input != 1)
    {
        if ( input %2 == 1 )
        {
            input = 3 * input + 1;
            count = count + 1;
        }
        else if ( input % 2 == 0 )
        {
            input = input / 2;
            count = count + 1;
        }
    }
    count = count + 1;
    return count;
}
