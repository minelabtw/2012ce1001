#include<iostream>
using namespace std;
int main()
{
    int number,counter1=0,counter=0,remainer;
    cout<<"Enter positive integer n : ";
    cin>>number;
    cout<<"Cycle-length of "<<number<<" is ";

    while(cin!=false && number>1)//當為正整數時進入迴圈
    {
        while(number>1)//若不等於一時進入小迴圈
        {
            remainer=number%2;
            if(remainer==0)//若為偶數時
            {
                number=number/2;
                counter1++;
            }
            else//為奇數時
            {
                number=3*number+1;
                counter1++;
            }
        }
        counter=counter1+1;
        cout<<counter<<endl;//輸出cycle-length
        counter=0;
        counter1=0;
        cout<<"Enter positive integer n : ";
        cin>>number;
        cout<<"Cycle-length of "<<number<<" is ";
    }
    if(cin==false || number<=1)//若為錯誤輸入則
        return -1;
    return 0;
}
