#include <iostream>

using namespace std;

int main()
{
    int n,cyclelength=1;//因為n=1時,while(n > 1)會少算一次所以cyclelength=1
    cout <<"Enter positive integer n : ";
    while(cin >> n)
    {
        if(n <= 0 || n >1000000 )//偵錯
        return 0;

        if(n == 1)//將1的另外算
        {
            cout <<"Cycle-length of 1 is 1";
            return 0;
        }

        cout <<"Cycle-length of "<< n <<" is ";
        while(n > 1)//執行演算法
        {
            if(n % 2 == 1)//奇數的算法
            {
                n = n * 3 + 1;
                cyclelength++;
            }
            else if(n % 2 == 0)//偶數的算法
            {
                n = n / 2;
                cyclelength++;
            }
        }
        cout << cyclelength <<endl;
        cout <<"Enter positive integer n : ";
        cyclelength = 1;//回歸初始值
    }
    return 0;
}
