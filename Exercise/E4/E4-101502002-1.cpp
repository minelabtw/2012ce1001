#include<iostream>
using namespace std;//簡化std
int main()
{
    int a;//a為輸入值
    cout<<"Enter positive integer n : ";
    while(cin>>a)//輸入整數a進入迴圈
    {
        if (a<1 or a>1000000)//錯誤輸入則結束
            return 0;
        int count=1;//count用來計算共有幾個數
        cout<<"Cycle-length of "<<a<<" is ";
        while(a>0)//用來計算count的迴圈
        {
            if(a==1)
                break;//a等於零食則跳出迴圈
            else if(a%2==0)//a為偶數時，進行運算
                a=a/2;
            else//a為奇數時，進行運算
                a=3*a+1;
            count=count+1;//數量加一

        }
        cout<<count<<endl;//輸出數量
        cout<<"Enter positive integer n : ";//重複輸入
    }
    return 0;
}
