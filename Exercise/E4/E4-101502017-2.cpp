#include <iostream>

using namespace std;

int main()
{
    int i,j,x,y=0,cycle=1;//x,y為儲存值

    while(cin >> i)
    {
        while(cin >> j)
        {
            if(i <= 0 || j <= 0)//偵錯
            return 0;

            cout <<i<<" "<<j<<" ";
            if(i > j)//大小代換
            {
                x = i;
                i = j;
                j = x;
            }

            for(i ; i <= j ; i++)//i到j的區間
            {
                x = i;//儲存值
               for(cycle = 1 ; i > 1 ; cycle++)//執行運算
                {
                    if(i % 2 == 1)
                        i = i * 3 + 1;
                    else if(i % 2 == 0)
                        i = i / 2;
                }
                if(cycle > y)//比大小代換
                    cycle = cycle;
                else
                    cycle = y;
                y = cycle;//代換,拿來做比較,儲存值
                i = x;
            }
            cout <<cycle<<endl;
            cycle = 1;
            y = 0;
            break;//這樣才能重複輸入i
        }
    }
    return 0;
}
