#include <iostream>

using namespace std;

int main()
{
    int n,c=0;
    cout << "Enter positive integer n : " ;
    if (cin >>n==false || n<0 )
    return -1;
    if (n>1000000 || n<1)
    return -1;

    while (n!=1)
    {
        cout <<"Cycle-length of "<<n<<" is ";
        while (n!=1) //若n=1則跳出回圈
        {
            if ((n%2)!=0)
            n=3*n+1;
            else
            n=n/2;
            c++; //每執行一次 cycle-length加1
            if (n==1)
            c=c+1;//當n=1時 也要算一次
        }
    cout <<c<<endl;
    c=0;
    cout << "Enter positive integer n : " ;
    cin >>n;
    }
    return 0;
}
