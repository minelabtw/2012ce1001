#include<iostream>
using namespace std;
int main()
{
    int num1;
    int num2;
    int i=1;

    cout << "Enter positive integer n : ";

    if ( cin >> num1 == false )//錯誤則結束程式
        return 0;

    else if ( num1 < 1 || num1 > 1000000 )//錯誤則結束程式
        return 0;

    num2 = num1; //把num1放到num2去運算

    while ( num2 > 1 )
    {
        int i = 1;


        while ( num2 > 1 )
        {
            if ( num2 % 2 == 1 ) //奇數情況
            {
                num2 = 3 * num2 + 1;
                i += 1;
            }//end if

            else if ( num2 % 2 == 0 ) //偶數情況
            {
                num2 = num2 / 2;
                i += 1;
            }//end else if

        }//end while
        cout << "Cycle-length of " << num1 << " is " << i << endl;
        cout << "Enter positive integer n : ";
        cin >> num1;
        num2 = num1;

    }//end while

    return 0;
}//end main
