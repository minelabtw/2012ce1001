#include<iostream>

using namespace std;

int main()
{
    while(1)
    {
        int n;
        int m;
        int c=1;
        cout<<"Enter positive integer n : ";
        if(cin>>m==false||m<1||m>1000000)//Checking input
        {
            return 0;
        }
        n=m;
        while(n!=1)//Calculating
        {
            if(n%2==1)
            {
                n=3*n+1;
            }
            else
            {
                n/=2;
            }
            c++;
        }
        cout<<"Cycle-length of "<<m<<" is "<<c<<endl;
    }
    return 0;
}
