//if what the user enters isn't an integer,then exit the progream
#include<iostream>

using namespace std;

int main()
{
    while(1)
    {
        int x,y,n=2; //n represents the cycle-length

        cout << "Enter positive integer n : ";
        cin >> x;

        y=x; //set y to store the value of x because the value of x changes below

        if(cin==false||x<=0||x>1000000)
        return 0;

        while(x>=1&&x<=1000000)
        {
            if(x%2==1)
            {
                x=3*x+1;
                if(x==1)
                break;
            }
            else
            {
                x=x/2;
                if(x==1)
                break;
            }
            n++; //whenever the loop runs once, n increases by 1
        }
        cout << "Cycle-length of " << y << " is " << n << endl;
    }
}
