//101502504
#include <iostream>//allows program to output data to the screen
using namespace std;
int main()
{
    int i,j,k,a=0,buffer=0,max=0;//variable declaration

    while ( cin >> i && 1 <= i && 1000000 >= i && cin >> j && 1 <= j && 1000000 >= j )//user input
    {
        max=0;
        cout << i << " " << j << " ";

        if ( i > j )//let i <= j
        {
            buffer = i;
            i = j;
            j = buffer;
        }

        for (; i<=j; i++)//loop from i to j
        {
            a = i;//avoid i change
            for ( k = 1; a > 1; k++)//let a go into the loop instead of i
            {
                if ( a % 2 == 1 )//a is odd
                    a = 3 * a + 1;
                else if ( a % 2 == 0 )//a is even
                    a /= 2;
            }//end loop

            if ( k >= max )//find the largest length
                max = k;
        }//end loop
            cout << max << endl;//show the answer
    }

    return 0;
}//end main program
