#include <iostream> //exit by -1
using namespace std;

int m,i,j,l,n;
main()
{
    l=0;
    while(l>=0) //重複輸入  //輸入錯誤則結束程式
    {
        l=1;
        m=1;        //重置max
        cin >> i;
        cin >> j;
        if(i ==-1 || j == -1)            //正常結束迴圈
            break;
        if(cin==false)                   //字元
            return -1;

        if(i<=0 || j<=0)                 //0或負數
            return -1;

        if(i>1000000 || j>1000000)       //超過範圍
            return -1;

        if(i>=j)    //設為i < j
        {
            n = i;
            i = j;
            j = n;
        }
////////////////////////////////////以上偵錯/////////////////////////////////
        cout << i << " " << j << " ";    //輸出i,j
        for(i;i<=j;i++) //從i ~ j
        {
            l=1; //重置loop
            n=i;
            while(n!=1) //3n+1演算法
            {
                if(n%2==0)
                    n/=2;
                else
                    n=3*n+1;
                l++;
            }
            if(l > m)  //更新最大loop
                m=l;
        }
        cout << m << endl;
    }
    return 0;
}
