#include <iostream>

using namespace std;

int main()
{
    int i,j,buffer;//設一變數讓i>j時兩數位置調換,
    int max=0,a,b;//max為最大值,a為計算cycle-length的代數
                  //b用來跳出迴圈及計算cycle-length時所需的整數
    cin >> i;
    cin >> j;
    cout << i << " " << j << " ";

    if ( i>j)
    {
        buffer=i;
        i=j;
        j=buffer;
    }//調換i,j位子以便計算

    while ( i<=j )
    {
        /////////////////錯誤偵測區///////////////

        if ( i<0 || j<0)
        return -1;

        else if ( cin==false)
        return -1;

        else if ( i>1000000 || j>1000000)
        return -1;

        //////////////////////////////////////////
        b=i;//設b起始值為i之值,用來計算cycle-length
        a=1;//讓每次迴圈開始時,cycle-length重新計算

        while ( b!=1)
        {
            if ( b%2 == 0)
            {
            b /= 2;
            a++;
            }
            else if ( b%2 == 1)
            {
            b = 3*b+1;
            a++;
            }

            if ( a>max )//若大於max值,則將計算結果的值當作max
            max = a;
        }
        i++;//換下一個整數計算
    }

    cout << max;

    return 0;
}
