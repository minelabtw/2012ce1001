#include <iostream>

using namespace std;

int count_cycle_len(int);
int get_valid_positive_int(void);

int main()
{
    int a, b;
    while(1)
    {
        if((a = get_valid_positive_int())==-1 || (b = get_valid_positive_int())==-1)
            return -1;                          //-1 means invalid input
        int aa=a, bb=b;                         //Let bb biger than aa
        if(aa>bb) swap(aa, bb);
        int ans = 0;
        for(int i=aa;i<=bb;i++)
            ans = max(ans, count_cycle_len(i));
        cout << a << " " << b << " " << ans+1 << endl;
    }
    return 0;
}

int count_cycle_len(int n)
{
    int c=0;
    while(n>1)
    {
        if(n%2==1)
            n = 3*n+1;
        else
            n /= 2;
        c++;
    }
    return c;
}

int get_valid_positive_int(void)
{
    int n;
    char check;
    if(!(cin >> n)||n<1||n>1000000)                 // Error detect
        return -1;                                  //-1 means invalid input
    check = cin.peek();                             // get next char in istream
    if(check!=' ' && check!='\n' && check!=0)       //Detect invalid char after integer, like "123a"
        return -1;
    return n;
}
