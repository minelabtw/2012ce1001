#include<iostream>
using std::cin;
using std::cout;
using std::endl;
int main()
{
    int number,tal=0;
    cout << "Enter positive integer n : ";
    cin >> number;
    if( cin == false )//找輸入是否錯誤
    {
        return -1;
    }
    if( number <= 1 || number > 1000000 )//找number是否在1與1000000之間，但number=1則結束
    {
        return -1;
    }
    while ( number > 1 )
    {
        if( number % 2 == 1 )//判斷基數
        {
            number = 3 * number + 1;
            tal++;
        }
        else if( number % 2 == 0 )//判斷偶數
        {
            number = number / 2;
            tal++;
        }
    }
    tal++;
    cout << "Cycle-length of 1000 is " << tal << endl;

    return 0;
}
