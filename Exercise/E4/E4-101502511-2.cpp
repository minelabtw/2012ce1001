#include <iostream>
using namespace std;

int main()
{
    int i,j,n=1,max=0,buffer,k;

    while (cin >>i>>j)//進入迴圈
    {

        if ( i<0 || j<0)//輸入負數時結束程式
        {
            return 0;
        }
        if ( i > 1000000 || j>1000000)//i or j超過1000000結束程式
        {
            return 0;
        }
        cout << i << " " <<j;
        if ( i > j)// 如果i > j 交換數字
        {
            buffer = i;
            i = j;
            j = buffer;
        }
        max=0;//max返回初始值
        for ( i; i<=j; i++)//尋找i,j之間的cycle-length
        {
            k =i;//避免i重複計算成1
            n=1;//返回初始值
            while ( k>0)//計算cycle-length
            {
                if (k%2 ==0 )
                {
                    k = k/2;
                    n=n+1;
                }
                else if ( k%2 !=0  )
                {
                    k=3*k+1;
                    n=n+1;
                }

                if ( k == 1)
                {
                    break;
                }
            }
            if( n > max )//儲存n的值,尋找max
                max = n;
        }
        cout <<" "<<max<<endl;
    }
    return 0;
}
