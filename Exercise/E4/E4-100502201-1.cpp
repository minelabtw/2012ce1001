#include<iostream>
using namespace std;

int main()
{
    int n;
    int count=0; //Count the number of cycle-length
    while(1) //Repetitive input
    {
        //Input
        cout<<"Enter positive integer n : ";
        cin>>n;
        if (n<0||cin==false)
        break;
        //Satisfy the condition, we'll calculate the cycle-length
        else
        {
            cout<<"Cycle-length of "<<n<<" is ";
            //Algorithm of 3n+1
            while (n!=1)
            {
                if (n%2==1)
                {
                    n=3*n+1;
                    count++;
                }
                else
                {
                    n=n/2;
                    count++;
                }
            }
            cout<<count+1<<endl; //Count+1 due to that number 1 doesn't count in variable "count"
            count=0;
            //Initialize "count" due to repetitive input
        }
    }
    return 0;
}
