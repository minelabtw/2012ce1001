#include <iostream>

using namespace std;

int main()
{  int n,m=1,k,r,max=1,t;
   while(cin>>n>>r)
   {
        if(n==false||n<1||n>1000000||r==false||r<1||r>1000000) //錯誤偵測
            return 0;
        cout<<n<<" "<<r; //輸出
        if(n>=r) //使大小排列正確
        {
         k=n;
         n=r;
         r=k;
        }
        while(n<=r) //進入cycle-length
        {
            t=n;
            while(t!=1)
            {
                if(t%2==1)
                    t=3*t+1;
                else
                    t=t/2;
                    m++;

            }
            if(m>max) //使輸出值為最大
            {
                max=m;
            }
            m=1;
            n++;
        }
    cout<<" "<<max<<endl;
   }return 0;
}


