#include<iostream>

using namespace std ;

int main()
{
    int i , I , j , max=0 , counter=1 , buffer ;

    while ( 1 )
    {
        cin >> i ;
        cin >> j ;

        I=i ;

        if ( i == false || j == false || i < 0 || j < 0 || i > 1000000 ||j > 1000000 )//if input is invalid(including not a number, negative or over 1000000), it will end.
            return 0 ;

        if ( i>j )//use number to change number
        {
            buffer=i ;
            i=j ;
            j=buffer ;
        }

        while ( i<=j )
        {
            I=i ;
            counter = 1 ;

            while ( I>=1 )//to find cycle-length
            {
                if ( I % 2 == 1 )
                    I = I * 3 + 1 ;

                else if ( I % 2 == 0 )
                    I = I / 2 ;

                counter = counter + 1 ;

                if ( I == 1 )
                    break ;
            }

            i = i + 1 ;

            if( counter > max )
                max = counter ;
        }

        cout << I << " " << j << " " << max << endl ;


    }

}
