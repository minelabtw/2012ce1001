#include <iostream>
using namespace std;

int main()
{
    int n, n1, i = 1 , j, max = 0 , buffer;
    cin >> n;//enter two number
    cin >> n1;

    if( n == false || n1 == false ) return 0;//number entered by user should not be invalid input
    if( n <= 0 || n1 <= 0 ) return 0;//number entered by user should be positive
    if( n > 1000000 || n1 > 1000000 ) return 0;//n,n1 can't greater than 1000000

    cout << n <<" "<< n1 << " ";

    if( n > n1 )//if n>n1 then change them
    {
        buffer = n;
        n = n1;
        n1 = buffer;
    }
    else
    for( int i = 1; n <= n1 ; n++ )//range should between n and n1
    {
        j = n;
        while( j > 1 )//j should >1
        {
            if( j % 2 != 0 )//odd
            {
                j = 3*j + 1;
                i = i + 1;
            }
            else//even
            {
                j /= 2;
                i = i + 1;
            }
            if( i > max ) max = i;//find max i
        }
    }

    cout << max;
    return 0;
}
