#include<iostream>
using namespace std;
int main()
{
    int n,length;
    cout<<"Enter positive integer n : ";
    cin>>n;

    if(cin==false || n<=0 || n>100000)
      return 0;

    while(n>1 && n<1000000)//if n greater than one and less than one million
    {
        length=1;//fix length equal to one
        cout<<"Cycle-length of "<<n;
        while(n>1 && n<1000000)
        {
            if(n%2==0)
            {
                n=n/2;
            }
            else
            {
                n=n*3+1;
            }
            length=length+1;//through every loop the length will pius one

        }
        cout<<" is "<<length<<endl;
        cout<<"Enter positive integer n : ";
        cin>>n;
     }

    return 0;
}
