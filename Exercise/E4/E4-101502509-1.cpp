#include <iostream>
#include <limits>
#include <cstdlib>

using namespace std;

int main()
{
   int num,item=0;
   do
   {
      cout <<"Enter positive integer n : ";
      cin >> num;
      cin.clear(); //清除緩衝區資料
      cin.ignore(numeric_limits<int>::max(), '\n');  //忽略緩衝區錯誤資料

      if(cin==false || cin.gcount()!=1 || num<=1 || num>1000000)
           return -1;       // 輸入字元,負數,1 and大於1000000的數字結束程式
      else
         cout <<"Cycle-length of "<<num<<" is ";  //輸出Cycle-length
///////////////////////////////Start Calculate item /////////////////////////
      while(num>1)  /*Because the last number of the sequence is always 1,so when num=1,
                      it will get out of the loop */
      {
         if(num%2==1)  //if num is an odd number,let number =3*number+1 and let item add 1
         {
           num=3*num+1;
           item++;
         }
         else if(num%2==0) //if num is an even number,let number =number/2 and let item add 1
         {
           num=num/2;
           item++;
         }
      }
      item++; //Becuse when num=1,get out of the loop.We donn't calculate num=1 this item,we must calculate it.
      cout << item<<endl; //print item.
//////////////////////////////Finish  Calculate item///////////////////////
      num=0;  //Restart num
      item=0; //Restart item

   }while(num!=1);//when num==1,get out of the loop

   system("pause");
   return 0;

}
