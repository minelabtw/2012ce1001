//E4-2 by J
#include <iostream>
using namespace std ;
int main()
{
    int i=0 , j=0 , max=1  , ctr=1 , lb=0 , ub=0 , chr=0 ;
    //i j for input , i.e. the range of all , ctr as counter for cycle-length , max for storing max cycle-length , lb for lower bound of i&j and to add until lb==ub ,ub as upper bound means the end of all count , chr means checker for checking all the positive integer between lb&up

    while ( cin>>i>>j )
    {
        if ( i>1000000 || j>1000000 || i<1 || j<1)      //end situstion
            return -1 ;

        if ( j>i )      //these if instruction is to indicate lower bound&upper bound's value
        {
            lb=i ; ub=j ;
        }
        else
        {
            lb=j ; ub =i ;
        }

        chr=lb ;        //checker's start value

        while ( lb<=ub )        //here we starts our examine from the smallest number to the largest
        {
            while ( chr!=1 )        //we use a new variable chr to check all the integers between input values
            {
                if ( chr%2==1 )     //2 algorithms
                    chr=3*chr+1 ;
                else
                    chr/=2 ;
                ctr+=1 ;        //the counter
            }

            if ( max<ctr )      //this statement find for the maximum of counter
                max=ctr ;

            ctr=1 ;     //reset counter
            lb+=1 ;     //1.
            chr=lb ;        //2.        1.&2.is to check the next integer
        }

        cout << i << " " << j << " " << max << endl ;       //show the result
        max=1 ;     //max has to reset for the next inputs

    }

    return 0 ;
}
