#include <iostream>

using namespace std;

int main()
{
    int interger,A ;
    int length ;

    cout << "Enter positive integer n : " ;

    while (cin>>interger) //輸入兩個數字，while使其可以重複輸入
    {
        A=interger ; //將interger的值先存進A，再用A做運算
        length = 1 ; //每做完一個迴圈後length要返回初始值1

        if (cin == false || interger < 1 || interger > 1000000) //偵錯，如果輸入的不是數字或輸入的小於1，或輸入的>1000000
            return 0 ;

        cout << "Cycle-length of " << interger ;

        while (A>1)
        {
            if (A%2==0) //如果interger是偶數
            {
                A = A / 2 ;
            }
            else //如果interger是奇數
            {
                A = A*3+1 ;
            }
            length = length +1 ; //每做完一次迴圈後會多一個數字，如此可以計算cycle-length的值
        }
        cout << " is " << length <<endl ;
        cout << "Enter positive integer n : " ;
    }

}
