#include<iostream>

using namespace std ;

int main ()
{
    int n ; // 宣告整數n

    cout << "Enter positive integer n : " ;

    while ( cin >> n && n>0 && n <= 1000000 ) // 輸入n,如果n大於且n小於等於1000000,進入while迴圈
    {
        int c = 1; // 宣告c初始值為1
        cout << "Cycle-length of " << n <<" is " ;

        while ( n != 1 ) // 當n不等於1,進入while迴圈
        {
            if ( n%2 == 1 ) // 如果n為奇數,執行以下動作
            {
                n = ( n*3 ) +1 ; //n等於n乘以3加1
                c++ ; // c加1
            } // end if
            else if ( n%2 != 1)
            {
                n = n/2 ;// n等於n除以2
                c++ ; // c加1
            } // end else if
        } // end while
        cout << c << endl;
        cout << "Enter positive integer n : " ;
    } // end while
    return 0 ;
} // end main

