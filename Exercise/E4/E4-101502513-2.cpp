#include <iostream>
using namespace std;
int main()
{
    int a, b, i = 1;
    while( cin >> a, cin >> b ) //input a and b repetitiously
    {
        int max = 0; //let max initialize
        if( a < 1 || a > 1000000 )
        {
            return 0;
        }

        if( b < 1 || b > 1000000 )
        {
            return 0;
        }

        cout << a << " " << b << " ";

        if( a > b ) //if a is greater than b, exchange a and b
        {
            int n;
            n = a;
            a = b;
            b = n;
        }

        while( a <= b )
        {
            int c = a;
            i = 1; //let i initialize

            while( c != 1 ) //enter a loop as n isn't equal to 1
            {
                if( c % 2 == 1 ) //when n is odd
                {
                    c = 3 * c + 1;
                    i++;
                }
                else //when n is even
                {
                    c = c / 2;
                    i++;
                }
            }
            if( i > max )
                max = i; //put i into max(memory)
            a++;
        }
        cout << max << endl;
    }
    return 0;
}
