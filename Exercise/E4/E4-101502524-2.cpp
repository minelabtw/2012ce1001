#include <iostream>
using namespace std;

int main()
{
    int i,i1,j,b,k,m,a=0;

    while(1)//一個無線迴圈
    {
        cin>>i1;
        if(!cin || i1<0 || i1>1000000)//如果輸入為負值,字元或大於一百萬,則跳出迴圈
            break;
        cin>>j;
        if(!cin || j<0 || j>1000000)//如果輸入為負值,字元或大於一百萬,則跳出迴圈
            break;
        cout<<i1<<" "<<j<<" ";
        if(i1>j)//如果前者大於後者,則交換兩者數值
        {
            b=i1;
            i1=j;
            j=b;
        }
        for(;i1<j;i1++)//如果i1<j則進入迴圈,每次i接增加1
        {
            i = i1;
            k=1;
            while(i1>1)//計算i1的Cycle-length
            {
                if(i1%2!=0)
                {
                    i1 = 3 * i1 + 1;
                    k = k + 1;
                }
                else if(i1%2==0)
                {
                    i1 = i1 / 2;
                    k = k + 1;
                }
            }
            i1=i;
            if(k>a)//如果Cycle-length比上一個大,則存取
            {
                a = k;
            }
        }
        cout<<a<<endl;
        a=0;
    }
    return 0;
}
