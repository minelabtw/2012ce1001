#include <iostream>

using namespace std;

int main()
{
    int a,b; //a為輸入值,b是算總共有幾個cycle

    cout << "Enter positive integer n : ";

    while(cin>>a) //如果輸入為整數則進入迴圈
    {
        if(a<0||a>1000000) //如果a<0 or a>1000000 直接結束
            return 0;
        b=1; //一開始B=1(因為要算到自己本身)
        cout << "Cycle-length of " << a << " is ";

        while (a>1) //如果a>1,進入迴圈
        {

            if (a%2==0) //a/2整除的話 a直接除以2,且個數加1
            {
                a=a/2;
                b++;
            }
            else //a/2餘一時,a*3+1,且個數+1
            {
                a=3*a+1;
                b++;
            }
        }

        cout << b << endl; //輸出個數
        cout << "Enter positive integer n : "; //重複輸入
    }
    return 0;
}
