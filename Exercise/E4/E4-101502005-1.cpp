#include <iostream>
using namespace std;
int main()
{
    int n,a;//設定兩個變數
    cout<<"Enter positive integer n : ";
    while(cin>>n)//輸入n進進入迴圈
    {
        if (cin!=false)//如果輸入的數不等於整數則跳出
            return 0;
        else if (n<1 && n>1000000)//錯誤偵測
            return 0;
        a=1;
        cout<<"Cycle-length of "<<n<<" is ";


        while(n!=1)//如果n不等於1則進入迴圈
        {

             if(n%2==1)//如果n除以2的餘數等於1則進入迴圈
             {
              n=3*n+1;
              a++;
             }

             else if(n%2==0)//其餘如果n除以2的餘數等於0則進入此迴圈
             {
                 n=n/2;
                 a++;
             }


        }
        cout<<a<<endl;
        cout<<"Enter positive integer n : ";
    }
    return 0;
}
