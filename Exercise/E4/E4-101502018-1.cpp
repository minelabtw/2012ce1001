#include <iostream>

using namespace std;

int main()
{
    int n,a=1;
    cout << "Enter positive integer n : ";
    while(cin>>n)
    {
        if(n<=0 || n>1000000)//如果n<=0或>1000000結束
            return 0;
        if(n==1)
        {
            cout <<"Cycle-length of 1 is 1";
        return 0;
        }
        cout <<"Cycle-length of "<<n<<" is ";
        while(n!=1)//當n不等於1,進入迴圈
        {
            if(n%2==1)
            {
                n=n*3+1;
                a++;
            }
            if(n%2==0)
            {
                n=n/2;
                a++;
            }
        }
        cout <<a<<endl;
        a=1;
        cout <<"Enter positive integer n : ";//重複輸入第一行
    }
    return 0;
}
