#include<iostream>
using namespace std;
int main()
{
    int a = 0;
    cout<<"Enter positive integer n : ";
    while(cin>>a)
    {
        int b = 1,c = 0;
        c = a;
        if(cin==false || a<0 || a>1000000)//如果出現錯誤輸入( 字元、負數、或超出範圍的數 )則結束程式
        {
            return 0;
        }

        while(c != 1)//如果c不等於1,進入迴圈
        {
            {
                if(c % 2 == 0)//若c為偶數,c=c/2

                    c /= 2;

                else//若c為奇數,c=3*c+1

                    c = 3 * c + 1 ;
            }

            b += 1;//計算共有幾個數
        }
        cout<<"Cycle-length of "<<a<<" is "<<b<<endl;
        cout<<"Enter positive integer n : ";
    }
    return 0;
}
