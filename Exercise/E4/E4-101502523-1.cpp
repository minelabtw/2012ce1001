#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int n,i=1;

    cout << "Enter positive integer n : ";

    if( !cin )//input為字元時結束程式
        return -1;

    while( cin >> n )//使得程式可重複輸入
    {
        if( n > 1000000 )//限定input介於1~1000000
            return -1;

        cout << "Cycle-length of " << n << " is ";

        while( n > 1 )// 3n + 1演算法的迴圈
        {
            if( n % 2 == 1 )
                n = 3 * n + 1;
            else
                n/=2;
            if( n == 1 )//如果n值最後為1時，結束迴圈
            {
                i++;
                cout << i << endl;
                break;
            }
            i++;
        }
        i = 1;

        cout << "Enter positive integer n : ";
        cin >> n;

        if( !cin )//input為字元時結束程式
            return -1;
        if( n <= 0 )//檢查input是否為正整數
            return -1;
    }
    return 0;
}
