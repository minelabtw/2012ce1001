#include<iostream>
using namespace std;
int main(){
    int a;
    while(1){
        cout<<"Enter positive integer n : ";
        cin>>a;
        if(cin==false||a<0||a>1000000)//錯誤判斷
            break;
        cout<<"Cycle-length of "<<a<<" is ";
        int x;
        for(x=1;a!=1;x++){//做運算 x計算cycle-length
            if(a%2==1)
                a=3*a+1;
            else
                a/=2;
        }
        cout<<x<<endl;//輸出
    }
    return 0;
}
