#include<iostream>
using namespace std;
int main()
{
    int i = 0 , j = 0,a = 0,m = 0,max=0;

    while(cin>>i,cin>>j)
    {
        if(i<0 || i>1000000 )//如果出現錯誤輸入( 字元、負數、或超出範圍的數 )則結束程式
        {
            return 0;
        }
        if(j<0 || j>1000000 )//如果出現錯誤輸入( 字元、負數、或超出範圍的數 )則結束程式
        {
            return 0;
        }
        cout<<i<<" "<<j<<" ";
        if(i>j)//若i>j,則i,j互換
        {
            m=i;
            i=j;
            j=m;
        }
        while(i<=j)//當i小於等於j時進入迴圈
        {
            int now = 1;
            a=i;
            while(a != 1)//當a不等於1時進入迴圈
            {
                {
                    if(a % 2 == 0)//若a為偶數,a=a/2

                        a /= 2;

                    else//若a為奇數,a=3*a+1

                        a = 3 * a + 1 ;
                }
                now += 1;//計算共有幾個數
                if(now>max)//儲存最大的數
                {
                    max=now;
                }
            }
            i++;//i+1
        }
        cout<<max<<endl;
        max=0;//重製max
    }
    return 0;
}
