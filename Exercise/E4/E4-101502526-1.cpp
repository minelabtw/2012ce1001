#include <iostream>

using namespace std;

int main()
{
    int n = 1;  //定輸入的值為整數
    int count = 1;  //定計算值為1 (因為n=1時不執行，少算1個)
    cout << "Enter positive integer n : " ;
    for (cin >> n ; n != 1;)  //重複輸入，當n不等於1時執行
    {
        //若值為字元或負數或大於1000000則結束程式
        if (cin == false || n < 0 || n > 1000000)
        {
        return 0 ;
        }
        cout << "Cycle-length of " << n << " is : " ;
        /*當n不論計算後不為1則執行迴圈，
          假如為奇數則n=3*n+1，計算值+1，繼續迴圈。
          若為偶數則n=n/2，計算值+1，繼續迴圈
          直到n=1結束迴圈，
          輸出計算值且重複輸入
        */
        for ( ; n != 1 ; )
        {
            if ( n % 2 == 1 )
            {
                n = 3 * n + 1 ;
                count = count + 1;
                continue;
            }
            if ( n % 2 == 0 )
            {
                n = n / 2 ;
                count = count + 1;
                continue;
            }
            if ( n == 1)
                break;
        }
        cout << count << endl;
        count = 1;
        cout << "Enter positive integer n : " ;
        cin >> n ;
    }

    return 0;
}
