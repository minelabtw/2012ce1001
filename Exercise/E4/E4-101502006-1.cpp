#include<iostream>
using namespace std;
int main()
{
    int n,backup,counter=1;
    cout << "Enter positive integer n : ";
    while (cin>>n) //used for enter again.
    {
        backup = n; //used for backup n,and show it later.
        if (backup < 0 || backup > 1000000) // error detection:negative or >1000000.
            return 0;
        while (n != 1) // if n=1 break the loop.
        {
            if (n % 2 ==1) //if n is odd, n=3n+1.
            {
                n=3*n+1;
            }
            else // if n is even, n=n/2.
            {
                n=n/2;
            }
            counter++; //used for count the Cycle-length.
        }
        cout << "Cycle-length of "<< backup <<" is "<<counter<<endl;
        cout << "Enter positive integer n :";
        counter=1; //prevent coutner +1 countinuosly.
    }
    return 0;
}
