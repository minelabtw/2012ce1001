#include<iostream>

using namespace std;

int main()
{
    int n,a;//輸入變數
    cout << "Enter positive integer n : ";//輸入第一行字串
    cin >> n;//輸入要計算的數字n

    while(n>=1 && n<=1000000)//限制n之範圍進入迴圈，否則退出程式
    {
        cout << "Cycle-length of " << n;//先輸出前面部分要顯示的字串
        a=1;//初始化，a為數列個數
        while(n!=1)//此為計算數列的迴圈，停止於n等於1
        {
            if(1==n%2)//當n是奇數
                n=3*n+1;//做此運算
            else if(0==n%2)//當n是偶數
                n=n/2;//做此運算
            a++;//計算a
        }

        cout << " is "<< a << endl;//輸出a
        cout << "Enter positive integer n : ";//輸出新字串
        cin >> n;//輸入新變數
    }

    return 0;

}
