#include<iostream>
using namespace std;
int check(int n)//made a check fuction
{
    int c=1;//the initial length counter is 1
    while(n!=1)//if n equals 1 then end the function
    {
        if(n%2==0)//when n is even
            n/=2;//
        else//when n is odd
            n=3*n+1;
        c++;//increase the counter
    }
    return c;//return the length
}
int main()
{
    int input;
    cout<<"Enter positive integer n : ";
    while((cin>>input)!=false && input>0 && input<1000000)//input must be a number and between 1 and 1000000
    {
        cout<<"Cycle-length of "<<input<<" is "<<check(input)<<endl;//output the length
        cout<<"Enter positive integer n : ";//ask to enter next number
    }
    return 0;//end of the program
}
