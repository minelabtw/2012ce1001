#include <iostream>
using namespace std;

int main()
{
    int a,b=1,max=0,x,y,w,buffer;
    while( cin >> x >> y )
    {
        if ( x < 1 || y < 1 || x > 1000000 || y > 1000000 )
        {
            return 0;
        }
        cout << x << " "<< y;//Show line.
        if ( x > y )//Exchange x, y if x > y.
        {
            buffer = y;
            y = x;
            x = buffer;
        }
        w = x;
        while ( x <= y )//The formula.
        {
            a = x;//keep the volume of x.
            while ( a > 1 )
            {
                if ( a % 2 == 1 )
                {
                    a = 3 * a + 1;
                }
                else
                {
                    a = a / 2;
                }
                b++;
            }
            if ( b > max )
            {
                max = b;
            }
            b = 1;//reset the cycle length.
            x++;
        }
        cout <<" "<< max << endl;//Show line.
    }
    return 0;
}
