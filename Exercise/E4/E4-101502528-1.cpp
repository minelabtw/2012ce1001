#include<iostream>
using namespace std;

int main()
{
    int n,save,count;

    while (1)//一個無窮迴圈
    {
        cout << "Enter positive integer n : ";
        cin >> n;
        save=n;
        count=1;

        if (cin==false||n<0||n>1000000)//若輸入負數或字元或大於一百萬則跳出迴圈
            break;
        else
            while (n>1)//計算n的cycle-length
            {
                if (n%2!=0)
                    n=3*n+1;
                else if (n%2==0)
                    n=n/2;
                count=count++;
            }
            n=save;
            cout << "Cycle-length of " << n << " is " << count << endl;
    }

    return 0;
}
