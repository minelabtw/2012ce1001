#include <iostream>
#include <math.h>
#include <cstdlib>
using namespace std;

int main()
{   
    int a,count=1;
    cout<<"Enter positive integer n : ";
    while(cin>>a && a>0)//判斷是否合法輸入 
    {
      count=1;
       while(a!=1)
       {
            if(a%2==1)//奇數
            {
                a=(3*a)+1;
                count=count+1;   
            }      
            else//偶數 
            {
                a=a/2;
                count=count+1;  
            } 
       } 
       cout<<"Cycle-length is "<<count<<endl; 
    }
 // system("pause");
    return 0;
}
