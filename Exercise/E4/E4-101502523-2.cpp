#include <iostream>
using std::cout;
using std::cin;
using std::endl;

int main()
{
    int i,j,buffer,length,max,a;
    length=1;

    while( 1 )//重複執行
    {
        max=0;
        cin >> i;
        cin >> j;

        if( i <= 0 || j <= 0)//檢查input是否為正整數
            return -1;
        if( !cin )//input為字元時結束程式
            return -1;
        if( i > 1000000 || j > 1000000 )//限定input介於1~1000000
            return -1;

        cout << i << " " << j << " ";

        if( i > j )//如果i>j，互相交換值
        {
            buffer = i;
            i = j;
            j = buffer;
        }
        while( i <= j )//運算至i為j值
        {
            a=i;
            while( a != 1 )
            {
                if( a % 2 == 1 )
                    a = 3 * a + 1;
                else
                    a/=2;
                length++;
            }
            if( length > max )//判斷介於i與j之間cycle-length 中的最大值
                max = length;
            i++;
            length=1;
        }
        cout << max << endl;
    }
    return 0;
}
