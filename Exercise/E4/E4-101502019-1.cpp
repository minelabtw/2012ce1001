#include <iostream>

using namespace std;

int main()
{
    int n,t = 0;

    cout << "Enter positive interger n : ";
    cin >> n;

    if (n == 1)//n=1直接結束程式
        return 0;
    else
    cout << "Cycle-length of " << n << " is ";

    while (cin != false && n > 1)//判斷錯誤並使程式能夠繼續輸入
    {
        while (n != 1)
        {
            if (n % 2 != 0)//奇數計算
            {
                n = 3 * n + 1;
                t++;//改變計數器的值
            }
            else//偶數計算
            {
                n = n / 2;
                t++;//改變計數器的值
            }
        }
    cout << t + 1;//因為t的初始值設為0 所以計數器要+1
    t = 0;

    cout << "\nEnter positive interger n : ";
    cin >> n;

    if (n==1)
        return 0;
    else
    cout << "Cycle-length of "<< n <<" is ";
    }
    return 0;
}
