#include <iostream>
using namespace std;

int main()
{
    int n;//the number entered by user
    cout << "Enter positive integer n : ";

    if( cin >> n == false || n <= 0 ) return 0;//number entered by user should be positive and not invalid input

    if(n > 1000000)return 0;//n can't greater than 1000000

    while(n>1)
    {
        int i = 1;//Cycle-length of n
        cout << "Cycle-length of " << n;

        if(n%2!=0)//odd
        {
            n = 3 * n + 1;
            i = i + 1;

            if(n==1) break;

            while(n%2==0)//even
            {
                n = n / 2;
                i = i + 1;
                while(n%2!=0)//odd
                {
                    if(n==1) break;
                    n = 3 * n + 1;
                    i = i + 1;
                }
            }
            while(n%2!=0)//odd
            {
                if(n==1) break;
                n = 3 * n + 1;
                i = i + 1;
                while(n%2==0)//even
                {
                    n = n / 2;
                    i = i + 1;
                }
            }
        }
        if(n%2==0)//even
        {
            n = n / 2;
            i = i + 1;

            while(n%2!=0)//odd
            {
                if(n==1) break;
                n = 3 * n + 1;
                i = i + 1;
                while(n%2==0)//even
                {
                    n = n / 2;
                    i = i + 1;
                }
            }
            while(n%2==0)//even
            {
                n = n / 2;
                i = i + 1;
                while(n%2!=0)//odd
                {
                    if(n==1) break;
                    n = 3 * n + 1;
                    i = i + 1;
                }
            }
        }

        cout << " is " << i << endl;
        cout << "Enter positive integer n : ";
        cin >> n;
}

return 0;
}
