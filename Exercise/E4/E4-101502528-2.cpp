#include<iostream>
using namespace std;

int main()
{
    int a,b,buffer,count,max,save;

    while (1)//一個無窮迴圈
    {
        max=0;
        cin >> a;
            if (cin==false||a<0||a>1000000)//輸入負數或字元或大於一百萬則跳出迴圈
                break;
        cin >> b;
            if (cin==false||b<0||b>1000000)//輸入負數或字元或大於一百萬則跳出迴圈
                break;
        cout <<a<<" "<<b<<" ";

        if (a>b)//若a大於b則將兩數交換
        {
            buffer=a;
            a=b;
            b=buffer;
        }

        while (a<b)//若a小於b則進入此迴圈
        {
            save=a;
            count=1;
            while (a>1)//計算a的cycle-length
            {
                if (a%2!=0)
                    a=3*a+1;
                else if (a%2==0)
                    a=a/2;
                count=count++;
            }
            a=save;
            a=a++;
            if (count>max)//找出最大的cycle-length
                max=count;
        }
        cout << max <<endl;
    }

    return 0;
}
