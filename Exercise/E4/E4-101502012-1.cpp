#include <iostream>
using namespace std;

int main()
{
    long int n,count=1,buffer;
    cout << "Enter positive integer n : ";

    while (cin >> n != false && ((n>=1)&&(n<=1000000)) ) // 錯誤偵測: n必須在數字範圍內 且非字元
    {
        buffer=n; //留住原本n的值

        while (n!=1) //當n=1結束迴圈
        {
            if (1==n%2) //偶數運算
                n = 3 * n + 1;
            else if (0==n%2) //基數運算
                n = n / 2; //計算數列個數
            count++;
        }
        cout << "Cycle-length of " << buffer << " is " << count << endl;
        count=1;//重新計算數列個數

        cout << "Enter positive integer n : ";
    }
    return 0;

}//end main
