#include <iostream>

using namespace std;

int main()
{
    int interger,A,X,Y,Z;
    int length ;
    int max=0 ;
    int buffer ;

    while (cin >> interger >> A) //輸入兩個數字，while使其可以重複輸入
    {
        max=0;
        Y=interger ; //先將interger的數值存到Y裡面
        Z=A ; // 先將A的數值存到Z裡面

        if (cin == false || interger < 1 || interger > 1000000 || A < 1 || A > 1000000)
            return 0 ;

        if (interger>=A) //如果第一個輸入的數字大於第二個輸入的數字，將兩數字值交換
        {
            buffer=interger;
            interger=A;
            A=buffer;
        }

        while (interger<=A)
        {
            X=interger ; // 先將interger的數值存到X裡面再用X做計算
            length = 1 ; //每做完一個迴圈後length要返回初始值1

            while (X>1)
            {
                if (X%2==0) //如果X是偶數
                {
                    X = X / 2 ;
                }
                else //如果X是奇數
                {
                    X = X*3+1 ;
                }
                length = length +1 ; //每做完一次迴圈後會多一個數字，如此可以計算cycle-length的值
            }

            if (length>max) //max是一個暫存值，如果每次做完的length大於max就將max替換掉
            {
                max=length ;
            }
            interger++;
        }
        cout << Y << " " << Z << " " << max << endl ; //列印出一開始輸入的兩個數字以及兩數字間cycle-length最大的值
    }

    return 0 ;
}
