//Enter EOF to exit

#include <iostream>
#include <stdio.h>
using namespace std;

int main()
{
    int n,temp,counter=1;
    cout<<"Enter positive integer n : ";

    do{
        if(cin>>n==false || n<1 || n>1000000)//Error detect
            return -1;

        temp=n;
        counter=1;

        while(n>1)
        {
            if(n%2==1)
                n=3*n+1;
            else
                n=n/2;
            counter++;
        }

        cout<<"Cycle-length of "<<temp<<" is "<<counter<<endl;
        cout<<"Enter positive integer n : ";
    }while((cin.get())!=EOF);// Enter EOF to exit
    return 0;
}
