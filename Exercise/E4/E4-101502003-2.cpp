#include <iostream>
using namespace std;//省略std::
int main()
{
  int a1,a2,c,max,b,d;//變數:a1=輸入1,a2=輸入2,c=個數,max=最多的數

  while (cin >> a1 && cin >> a2) //如果輸入的是整數,進入迴圈
  {

      max =0;
      cout << a1 << " " << a2 << " ";
      if (a2>a1) //先把大小順序列成一致,a1要是最大的
      {
            b=a2;
            a2=a1;
            a1=b;
      }
      while (a2<=a1) //如果a2<=a1,進入迴圈
      {
           c=1; //c=1是因為要算本身
           d=a2;  //因為d進入迴圈會一直=1,所以要讓d=a2,才不會無限迴圈
           while (d>1)
           {
               if (d%2==0)
                   d = d / 2;
               else
                   d = d*3 +1;
               c++;
           }
           a2++; //a2+1算下個數
           if (c>max)
               max = c; //讓max=c,找出當中最大的數
     }
     cout <<max<<endl;
  }

  return 0;
}
