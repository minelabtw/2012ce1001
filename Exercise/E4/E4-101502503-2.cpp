#include<iostream>

using namespace std ;

int main ()
{
    int i , j ,n=0 , max=0 ,buffer ; // 各種宣告

    // 輸入i和j,當i與j皆大於0且皆小於等於1000000,進入while迴圈
    while ( cin >> i >> j && i > 0 && j > 0 && i <=1000000 && j <= 1000000)
    {
        int c = 1 ; // 宣告整數c等於1
        cout << i << " " << j << " ";

        if ( i > j ) // 如果i大於j, 將i和j互換
        {
            buffer = i ;
            i = j ;
            j = buffer ;
        }

        while ( i <= j ) // 當i小於等於j,進入while迴圈
        {
            n=i ;
            c=1 ;
            while ( n != 1 ) // 當n不等於1,進入while迴圈
            {
                if ( n%2 == 1 ) // 如果n為奇數,執行以下動作
                {
                    n = ( n*3 ) +1 ; //n等於n乘以3加1
                    c++ ; // c加1
                } // end if
                else if ( n%2 != 1)
                {
                    n = n/2 ;// n等於n除以2
                    c++ ; // c加1
                } // end else if

            } // end while
            i++ ;
            if ( c > max ) // 如果c大於max
                max = c ; // max等於c
        } // end while
        cout << max << endl ;
        max = 0; // 初始化max
    } // end while
    return 0 ;
} // end main
