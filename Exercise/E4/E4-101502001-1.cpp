#include <iostream>

using namespace std;

int main()
{
    int integer,n;//integer為一正整數,n為integer的cycle-length


    while(1)
    {
        n=1;//n從1開始算起
        cout << "Enter positive integer n : ";//輸出問題
        cin >> integer;//鍵盤輸入數字

        if (cin==false || integer<=0 || integer>1000000)//若integer為字元,負數,或大於1000000
        {
            return -1;//則結束程式
        }
        if (integer==1)//若integer等於1
        {
            return 0;//則結束程式
        }
        else
        {
            while(integer!=1)//迴圈
            {

                if (integer%2==0)//若integer等於偶數
                {
                    integer=integer/2;
                    n++;//n遞增
                }

                else//integer是奇數
                {
                    integer=3*integer+1;
                    n++;//n遞增
                }

            }

        }
        cout << "Cycle-length of " << integer << " is " << n << "\n";//輸出cycle-length
    }


    return 0;
}
