#include <iostream>
using namespace std;

int main()
{
    int a,b=1,c;

    cout << "Enter positive integer n : ";//Show line.
    while( cin >> a )
    {
        if ( a <= 1 || a > 1000000 )
        return 0;
        c = a;//Keep the volume of a.
        while ( a > 1 )//The formula.
        {
            if ( a % 2 == 1 )
            {
                a = 3 * a + 1;
            }
            else
            {
                a = a / 2;
            }
            b++;
        }
        cout << "Cycle-length of " << c <<  " is " << b << "\n";//Show line.
        cout << "Enter positive integer n : ";//Show line.
        b = 1;
    }
    return 0;
}
