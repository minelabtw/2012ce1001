#include <iostream>
//其中可能有一堆無意義的假設，因為我把E4-1中的程式複製過來
using namespace std;
using std::cin;
int main()
{
    int i,j,max,m,store;
    while(i!=0 &&j!=0)//純粹為了重複輸入而寫的while
    {
        i=0;//為了判斷字串
        j=0;//為了判斷字串
        m=0;//m在每一次計算後必須重設
        cin>>i;
        cin>>j;
        if(i>100000 ||j>100000)//太大不行
        {
            return 0;
        }
        if(i==0||j==0)//為了判斷字串
        {
            return 0;
        }
        if(i<0||j<0)//負數不行
        {
            return 0;
        }
        cout<<i<<" "<<j<<" ";//輸出

        if(j<i)//預設i<j,若i>j則換位
        {
        store=i;
        i=j;
        j=store;
        }
        max=i;//一開始max=i
        while(j>=max)//j<max則迴圈結束
        {
            int n=0,count=1;

            for ( n=max;n!=1;count++)//3n+1的運算
                {
                    if(n % 2 ==0)
                    {
                        n=n/2;
                    }
                    else
                        n=(n*3)+1;
                }
            if (count>m)//助教教的最大值換位
                {
                    m=count;
                }
            max++;//max逐漸加到=max
        }
        cout<<m<<"\n";
    }
    return 0;
}
