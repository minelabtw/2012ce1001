#include <iostream>

using namespace std;
using std::cin;

int main()
{
    int n;//n為輸入值,a為迴圈執行的次數,即為cycle-length
    int a;


    while(n>=0)
    {
    a=1;

    cout << "Enter positive integer n : " ;
    cin >> n;
    cout << "Cycle-length of " << n << " " << "is ";

    if( n<=0 || cin==false)
    return -1;//錯誤偵測

    if( n==1 )
    return -1;

    while ( n != 1 )//執行一次迴圈,cycle-length的長度+1
    {
        if( n%2==0 )
        {
            n /= 2;
            a++;
        }
        else if( n%2==1 )
        {
            n = 3*n+1;
            a++;
        }
    }

    cout << a << endl;



    }

    return 0;
}
