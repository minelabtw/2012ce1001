//enter not integer to exit

#include<iostream>
#include<stdlib.h>
#include<limits>
using namespace std;


bool inputcheck(int in);
void swap(int &a,int &b);
int _3nplus1(int n);


int main()
{
    //initial
    int i=1,j=1,max=1;


    while(1)
    {
        //input & exit
        cin>>i;
        if(!inputcheck(i))
            break;
        cin>>j;
        if(!inputcheck(i))
            break;


        //compute & output//
        cout<<i<<' '<<j;

        //exception
        if(i>j)
            swap(i,j);

        for(max=1;i<=j;i++)
            if(_3nplus1(i)>_3nplus1(max))
                max=i;
        cout<<' '<<_3nplus1(max)<<endl;
        //compute & output above//
    }


    //end
    system("pause");
    return 0;
}




int _3nplus1(int n)
{
    static int cyclelength=0;
    cyclelength++;

    if(n==1)
    {
        int re=cyclelength;
        cyclelength=0;//reset
        return re;
    }
    else if(n%2==1)//odd number
        return _3nplus1(n*3+1);
    else//even number
        return _3nplus1(n/2);
}


bool inputcheck(int in)
{
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    if(cin.gcount()!=1)//not integer
        return false;
    else if(in<1)//negative or zero
        return false;
    else if(in>1000000)//too large
        return false;
    //no error
    return true;
}


void swap(int &a,int &b)
{
    int t;
    t=a;
    a=b;
    b=t;
}
