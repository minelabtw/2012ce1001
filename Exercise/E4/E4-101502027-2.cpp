#include <iostream>
using namespace std;

int main()
{
    int n1,n3=1,n2,n5,n6,max=0;
    while(cin>>n1>>n2&&n1>=0&&n1<=1000000&&n2>=0&&n2<=1000000)
    {
        cout<<n1<<" "<<n2<<" ";
        if(n1>n2)
        {
           n5=n1;
           n1=n2;
           n2=n5;
        }//這是替換 i>j 的情形
        while(n1<=n2)
        {
           n6=n1;
           while (n6>1)
           {
              if(n6%2==1)
                 n6=n6*3+1;
              else
                 n6=n6/2;
              n3++;
           }//輸出一個cycle-length
           if(max<n3)
             max=n3;//比較cycle-length(即N3),讓其可以輸出最大值
           n3=1;
           n1++;
        }
        cout<<max<<"\n";
    }
    return 0;
}
