#include <iostream>

using namespace std;

int count_cycle_len(int);
int get_valid_positive_int(void);

int main()
{
    int n;
    while(1)
    {
        cout << "Enter a positive integer : ";
        if((n = get_valid_positive_int())==-1)          //-1 means invalid input
            return -1;
        cout << "Cycle-length of " << n << " is " << count_cycle_len(n) << endl;
    }
    return 0;
}

int count_cycle_len(int n)
{
    int c=0;
    while(n>1)
    {
        if(n%2==1)
            n = 3*n+1;
        else
            n /= 2;
        c++;
    }
    //if(n!=1) cout << n << endl;
    return c+1;                                     //last number is 1, count++
}

int get_valid_positive_int(void)
{
    int n;
    char check;
    if(!(cin >> n)||n<1||n>1000000)                 // Error detect
        return -1;                                  //-1 means invalid input
    check = cin.peek();                             // get next char in istream
    if(check!=' ' && check!='\n' && check!=0)       //Detect invalid char after integer, like "123a"
        return -1;
    return n;
}
