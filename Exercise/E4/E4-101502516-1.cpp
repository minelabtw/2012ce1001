#include <iostream>
using namespace std;

// 程式由此開始
int main()
{
    int a; // 假設變數
    int x = 1;

    while ( x = 1 ) // 如果x等於1，進入迴圈
    {
        cout <<"Enter positive integer n : "; // display message
        if ( cin >> a == false || a <= 0 || a > 1000000 ) // 當輸入的a錯誤、小於等於0或是a大於1000000，結束並回報
        {
            return -1;
        }
        int b = 1; // 假設變數
        int c = 1;
        int d = 0;
        int f = a;
        while ( a != 1 ) // 如果a不等於1，則進入迴圈
        {
            if ( a % 2 != 0 ) // 當a除於2的餘數不為0
            {
                a = 3 * a + 1; // 運算公式
                b++; // b遞增
            }
            if ( a % 2 == 0 ) // 當2可以整除a
            {
                a = a / 2;
                b++;
            }
        }

        d = d + b; // 運算公式
        cout << "Cycle-length of " << f << " is " << d << endl; // 輸出最後結果
    }

    return 0; // 結束並回報
}
