//E4-1 by J     the 3n+1 prob.
#include <iostream>
using namespace std ;
int main()
{
    int inp=0 , n=0 ;
    //inp as input . Due to the fact that we have to show input again , I want to create the other variable to do the math.

    cout << "Enter positive integer n : " ;
    while ( cin>>inp )      //for repeating input
    {
        if ( inp<1 || inp>1000000 )        //cause letter input will stop automatically , I just do an end instruction for none positve integer
            return -1 ;

        n=inp ;
        int ctr=1 ;     //ctr as counter for counting cycle-length , it has to reset its value.
        while ( n!=1 )        //major algorithm loop
        {
            ctr+=1 ;

            if ( n%2==0 )
                n/=2 ;
            else
                n=3*n+1 ;
        }

        cout << "Cycle-length of " << inp << " is " << ctr << endl ;
        cout << "Enter positive integer n : " ;
    }

    return 0 ;
}
