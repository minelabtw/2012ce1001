#include <iostream>

using namespace std;

int main()
{
    int a,b,t,i,max = 0,trans;

    while (cin >> a >> b && a > 0 && b > 0)
    {
        if (a > b)
        {
            trans = a;
            a = b;
            b = trans;
        }
        trans = a;

        while (a < b)
        {
            i = a;
            t = 0;

            while (i != 1)
            {
                if (i % 2 != 0)
                {
                    i = 3 * i + 1;
                    t++;
                }
                else
                {
                    i = i / 2;
                    t++;
                }
            }

            if (max < t)
                max = t + 1;
            a++;
        }
        cout << trans << " " << b << " " << max <<endl;
    }
    return 0;
}
