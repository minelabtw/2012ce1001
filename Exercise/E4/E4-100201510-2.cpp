//input character to exit.

#include <iostream>

using namespace std ;

int main()
{
    int i=0 , i2=1 , j=0 , counter = 1 , max = 0; // let i2 != j to enter the while loop , i2 will later be the input data.

    while(i2 != j)//this loop is used to repeat input data.
    {
        cin >> i ;
        cin >> j ;
        cout << i << " " << j << " " ;
        i2 = i ;
        max = 0 ;

        //check illigel input//
        if(cin == false)
        {
            return -1 ;
        }
        else if(i < 0 || j < 0)
        {
            return -1 ;
        }
        else if(i < 1 || i > 1000000 || j < 1 || j > 1000000)
        {
            return -1 ;
        }
        //check illigel input//

        else if(i > j) // if i > j , swich i and j.
        {
            max = i2 ;
            i2 = j ;
            j = max ;
            max = 0 ;
        }

        while(i2 <= j)
        {
            counter = 1 ;
            while(i >=1)
            {
                if(i == 1)//若 n 等於 1 則結束
                {
                    break;
                }
                else if(i%2 == 1)//n 為奇數，則 n ← 3 * n + 1
                {
                    i = 3*i + 1;
                    counter++ ;
                }
                else if(i%2 == 0)//n 為偶數，則 n ← n / 2
                {
                    i /= 2;
                    counter++ ;
                }
            }
            if(counter > max)
            {
                max = counter;
            }
            i2++;
            i = i2 ;
        }
        cout << max << endl;
    }
}
