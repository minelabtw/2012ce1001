#include<iostream>
using namespace std;//
int main()
{
    int i,j,buffer,mix,count;

    while(cin>>i>>j)//輸入整數i,j進入迴圈
    {
        if (i<1 or j<1 or i>1000000 or j>1000000)
            return 0;//若為非法輸入則結束
        cout<<i<<" "<<j<<" ";
        if(i>j)//若i>j則交換位置
        {
            buffer=i;
            i=j;
            j=buffer;
        }
        int c=i;
        while(c>0)//先進行第一個數的迴圈，計算Cycle-length
        {
            count=1;
            if(c==1)
                break;
            else if(c%2==0)
                c=c/2;
            else
                c=3*c+1;
            count=count+1;
        }
        mix=count;//先將第一個數令為最大值
        i=i+1;//從第二個數開始
        while(i<=j)//從第二個數開始迴圈
        {
            count=1;
            int d=i;
            while(d>0)//計算各個數的Cycle-length
            {
                if(d==1)
                    break;
                else if(d%2==0)
                    d=d/2;
                else
                    d=3*d+1;
                    count=count+1;
            }
            if(count>mix)//比較誰大
                mix=count;
            i=i+1;
        }
        cout<<mix<<endl;//輸出結果
    }
    return 0;
}
