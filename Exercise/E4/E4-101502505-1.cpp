#include <iostream>
using namespace std;
int main()
{
    int a,z=1;//z=1因為要加上n也是一個數

    while (1)
    {
        z=1;
        cout << "Enter positive integer n : " ;
        cin >> a;

        if ( cin == false || a <= 1 || a > 1000000 )//錯誤偵測
            return -1;
        else
        {
            cout << "Cycle-length of " << a << " is " ;
                while ( a != 1 )//最後算到a=1會跳出
                {
                    if ( a % 2 == 1)//奇數
                    {
                        a = a * 3 + 1;
                        z++;//紀錄做了一次計算
                    }
                    else//偶數
                    {
                        a = a / 2;
                        z++;//紀錄做了一次計算
                    }
                }
                cout << z << endl;//總共有多少個數
        }
    }
}

