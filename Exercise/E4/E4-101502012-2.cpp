#include <iostream>
using namespace std;
int main()
{
    long int i,j,x,buffer1,buffer2=1,count=1;
    while (cin >> i != false && ((i>=1)&&(i<=1000000)) ) //錯誤偵測: i必須在數字範圍內 且非字元
        while (cin >> j != false && ((j>=1)&&(j<=1000000)) ) //錯誤偵測: j必須在數字範圍內 且非字元
        {
            cout << i << " " << j;
            if (i>j) //如果i>j 交換彼此數值
            {
                buffer1=i;
                i=j;
                j=buffer1;
            }
            while(i<=j)
            {
                x=i;//紀錄原本i值
                while (i!=1) //當i=1結束迴圈
                {
                    if (1==i%2) //偶數運算
                        i = 3 * i + 1;
                    else if (0==i%2) //基數運算
                        i = i / 2; //計算數列個數
                    count++;
                }
                if (buffer2<count) //buffer2負責記錄count裡最大的值
                    buffer2=count;
                i=x+1;
                count=1;
            }
            cout << " " << buffer2 << endl;
            buffer2=1;
            break;
        }
    return 0;
}//end main
