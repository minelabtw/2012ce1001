#include <iostream>

using namespace std;

int main()
{
     int a,n,c;

     while(1)
     {
         cin>>a;
         n=1;
         if(cin==false||a<=0)         //錯誤輸入的判定
             return -1;
         else
         {
             c=a;                     //儲存a之值
             cout<<"Enter positive integer n : "<<a<<"\n";
             while(a!=1)              //迴圈在a=1時停止
             {
                 if(a%2==0)           //a若為偶數時執行
                     a=a/2;
                 else                 //a若為奇數時執行
                     a=3*a+1;
                 n=n+1;               //計算執行次數
             }
             cout<<"Cycle-length of "<<c<<" is : "<<n<<"\n";    //輸出次數
          }
     }
        return 0;
}
