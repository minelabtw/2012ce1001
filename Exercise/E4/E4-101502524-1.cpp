#include <iostream>
using namespace std;

int main()
{
    int n,n1,k=1;

    while(1)//一個無線迴圈
    {
        cout<<"Enter positive integer n : ";
        cin>>n1;
        n = n1;
        if(n1<0 || !cin || n1>1000000)//如果輸入為負值,字元或大於一百萬,則跳出迴圈
        {
            break;
        }
        while(n1>1)//n1>1時,則進入迴圈
        {
            if(n1%2!=0)//計算n1的Cycle-length
            {
                n1 = 3 * n1 + 1;
                k = k + 1;
            }
            else if(n1%2==0)
            {
                n1 = n1 / 2;
                k = k + 1;
            }
        }
        cout<<"Cycle-length of "<<n<<" is "<<k<<endl;
    }
    return 0;
}
