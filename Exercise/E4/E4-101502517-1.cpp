//E4-101502517-1
#include <iostream>
using namespace std;
int main()
{
    int a,b=1;
    do//大迴圈，重複執行
    {
        cout<<"Enter positive integer n : ";
        if ((cin>>a==false)||(a<=0)||(a>1000000))//偵錯：錯誤輸入、負或零、超出範圍
            return -1;//回傳-1
        while(1)//小迴圈，找出Cycle-length
        {
            if (a==1)//a=1，達成目標，輸出並跳出小迴圈
                {
                cout<<"Cycle-length of 1000 is "<<b<<endl;
                break ;
                }
            if (a%2==0)//若a為偶數
                a=a/2;
            else//若a為奇數
                a=a*3+1;
            b++;//記錄Cycle-length
        }
        b=1;//小迴圈結束，Cycle-length歸一
    }while(1);//大迴圈，重複執行
}
