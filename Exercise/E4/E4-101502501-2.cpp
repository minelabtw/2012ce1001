#include <iostream>

using namespace std;

int main()
{
    int i,j,buffer;//宣告整數i,j,buffer

    while ( cin >> i >> j != false && i>0 && j>0 && i<=1000000 && j<=1000000 )//當輸入為整數,且i及j介於1及1000000間時進入迴圈
    {
        cout << i << " " << j;
        if ( i > j )//如果i大於j,交換兩者
        {
            buffer = i;
            i = j;
            j = buffer;
        }
        int n=0,a,max=0;//宣告整數

        while( i <= j )//當i小於等於j時進入迴圈
        {
            a=1;
            n=i;
            while ( n != 1 )//當n不等於1時進入迴圈
            {
                if ( n % 2 == 1 )//判斷n是否為奇數
                {
                    n = (3 * n) + 1;//n等於3乘n加上1
                    a++;//a加一
                }
                else//n為偶數時
                {
                    n /= 2;//n等於n除以2
                    a++;//a加一
                }
            }
            i++;
            if ( a > max )//當a大於max時,max代為a
                max = a;

        }

        cout << " " << max << endl;
    }

    return 0;
}
