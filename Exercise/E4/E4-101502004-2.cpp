#include<iostream>

using namespace std;

int main()
{
    int i,j,buffer,n,a,max=0;//設定變數
    cin >> i;//輸入第一個數字i
    cin >> j;//輸入第二個數字j

    while(i>=1 && i<=1000000 && j>=1 && j<=1000000)//限制i跟j之範圍進入迴圈，否則退出程式
    {
        cout << i << " " << j;//先輸出i跟j

        if(i>j)//若是i比j大，以下可進行交換
        {
            buffer=i;
            i=j;
            j=buffer;
        }

        for(;i<=j;i++)//此迴圈為找出i到j之間的所有數字
        {
            n=i;//設定一個變數n代入要計算數列的數字

            a=1;//初始化，a為數列個數
            while(n!=1)//此為計算數列的迴圈，停止於n等於1
            {
                if(n%2==1)//當n是奇數
                    n=3*n+1;//做此運算
                else if(n%2==0)//當n是偶數
                    n=n/2;//做此運算
                a++;//計算a
            }

            if(a>max)//當a大於變數max
                max=a;//則存為max
        }

        cout << " "<< max << endl;//輸出最大值
        cin >> i;//再次輸入i跟j繼續做運算
        cin >> j;
        max=0;//max初始化
    }
}
