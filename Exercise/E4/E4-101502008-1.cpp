#include <iostream>

using namespace std;

int main()
{
    int m=0;
    while(m>=0)//重複輸入所使用的迴圈while
    {
        int count=1;//算有幾個3n+1運算時的因子count

        cout<<"Enter positive integer n : ";
        cin>>m;
        if(m<=0 || m>=1000000)//錯誤輸入
        {
            return 0;
        }
        for (int n=m;n!=1;count++)//3n+1運算的規則與count的判斷
        {
            if(n % 2 ==0)
            {
                n=n/2;
            }
            else
                n=(n*3)+1;
        }
        cout<<"Cycle-length of "<<m<<" is "<<count<<"\n";//輸出cycle-length
    }
    return 0;
}
