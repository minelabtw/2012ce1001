#include <iostream>
#include <cstdlib>
#include <cmath>
int CycleLength(int);
using namespace std;

int main()
{
    int input_i=0, input_j=0, output=0, temp=0, max=0;

    while ( (cin >> input_i != false) && (cin >> input_j != false) )
    {
        if ( (input_i <= 0) || (input_j <= 0) )
        {
            break;
        }
        else if ( (input_i > 1000000 ) || (input_j > 1000000) )
        {
            break;
        }
        else
        {
            if ( input_i >= input_j )
            {
                temp = input_i;
                input_i = input_j;
                input_j = temp;
            }

            max = CycleLength(1);
            for ( int i=input_i ; i <= input_j ; i++ )
            {
                if ( max < CycleLength(i) )
                {
                    max = CycleLength(i);
                }
            }

            cout << input_i << " " << input_j << endl;
            cout << input_i << " " << input_j << " " << max << endl;
        }
    }

    system("pause");
    return 0;
}

int CycleLength(int input)
{
    int count=0;
    while (input != 1)
    {
        if ( input %2 == 1 )
        {
            input = 3 * input + 1;
            count = count + 1;
        }
        else if ( input % 2 == 0 )
        {
            input = input / 2;
            count = count + 1;
        }
    }
    count = count + 1;
    return count;
}
