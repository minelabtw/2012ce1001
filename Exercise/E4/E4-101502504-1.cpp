//101502504
#include <iostream>//allows program to output data to the screen
using namespace std;
int main()
{
    int n,k;//variable declaration
    cout << "Enter positive integer n : ";//prompt user to data

    while ( cin >> n && 1 <= n && 1000000 >= n)//to keep user input
    {
        cout << "Cycle-length of " << n << " is ";
        for ( k = 1; n > 1; k++)//determine how long is the cycle
        {
            if ( n % 2 == 1 )//n is odd
                n = 3 * n + 1;
            else if ( n % 2 == 0 )//n is even
                n /= 2;
        }
            cout << k << endl;
            cout << "Enter positive integer n : ";
    }

    return 0;
}//end function main
