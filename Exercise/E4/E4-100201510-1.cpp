#include <iostream>

using namespace std ;

int main()
{
    int integer , counter = 1 ;
    cout << "Enter positive integer n : ";
    cin >> integer ;

    //detect illigle input//
    if(cin == false)
    {
        return -1 ;
    }
    else if(integer < 0)
    {
        return -1 ;
    }
    else if(integer < 1 || integer > 1000000)
    {
        return -1 ;
    }
    //detect illigle input//
    else
    {   //do the the 3n+1 algorithm
        while(integer >=1)
        {
            if(integer == 1)//若 n 等於 1 則結束
            {
                cout << "Cycle-length of " << integer << " is " << counter ;
                break;
            }
            else if(integer%2 == 1)//n 為奇數，則 n ← 3 * n + 1
            {
                integer = 3*integer + 1;
                counter++ ;
            }
            else if(integer%2 == 0)//n 為偶數，則 n ← n / 2
            {
                integer /= 2;
                counter++ ;
            }
        }
        return 0 ;
    }
}
