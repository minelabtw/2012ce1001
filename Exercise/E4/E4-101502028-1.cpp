#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    long n, count = 0;
    do // first do once the program then the while loop
    {
        cout << "Enter positive integer n : "; // appears this message
        cin >> n; // input something
        if (cin == false || n <= 1 || n > 1000000 ) // (if the input is a character, negative */
            return -1; // error                         number, zero or more than 1 million)   else // if input is a positive number less than 1 million
        cout << "Cycle-length of " << n << " is "; // appears this message
        while (n > 1) /* (do a while loop for the input number, so when the program get n */
        {             /* equals to 1, ends the loop) */
            if (n % 2 == 1) // if the input is an odd number
            {
                n = n * 3 + 1; // number multiply by 3 plus 1
                count = count + 1; // the counter will plus 1
            }
            else if (n % 2 == 0) // if the input is an even number
            {
                n = n / 2; // number divide by 2
                count = count + 1; // the counter will plus 1
            }
        }
        count++; // at the beginning, n equals to 1, i didn't count the "1", so here i added it
        cout << count << endl; // print out all added count
        n = 0; // to start again until the last number is 1
        count = 0; // to start again until the las number is 1
    }while (n != 1); // loop ends until the last number is 1
    return 0; // ends program
}
