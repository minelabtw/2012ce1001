//E4-101502517-2
#include <iostream>
using namespace std;
int main()
{
    int a,b,c,d,f,max=0;
    do//大迴圈，重複執行
    {
        if ((cin>>a==false)||(a<=0)||(a>1000000))//a除錯
            return -1;//回傳-1
        if ((cin>>b==false)||(a<=0)||(a>1000000))//b除錯
            return -1;//回傳-1
        if(a>b)//若a>b，順序互調
        {
            c=b;
            d=a;
        }
        else//a<b，依舊
        {
            c=a;
            d=b;
        }
        for(int e=1;c<=d;c++)//中迴圈，執行3n+1找出Cycle-length，直至下限等於上限
        {
            f=c;
                while(1)//小迴圈，找出Cycle-length
            {
                if (f==1)
                    {
                    if(max<e)
                        max=e;//若Cycle-length比先前最大值還大，存入
                    break ;//跳出小迴圈，找下一個Cycle-length
                    }
                if (f%2==0)//3n+1演算法
                    f=f/2;
                else
                    f=f*3+1;
                e++;//紀錄Cycle-length
            }
            e=1;//小迴圈執行完畢，紀錄Cycle-length之數歸一
        }//中迴圈執行完畢，下限已達上限
        cout <<a<<" "<<b<<" "<<max<<endl;//輸出Cycle-length中的MAX
        max=0;//MAX歸零，等待進入下次中迴圈
    }while(1);//大迴圈，重複執行
}
