#include <iostream>

using namespace std;

int main()
{
    int n , x;
    x = 1;

    cout << "Enter positive integer n : ";
    cin >> n;

    if ( n > 1000000 ) // 設定n值在1000000以內,超出則結束程式
        return -1;

    if ( n <= 1 ) // 設定n值大於1 ,不和則結束程式
        return -1;

    if ( !cin ) // 偵錯非數字輸入
        return -1;

    while ( n > 0 ) // 重複輸入
    {
        cout << "Cycle-length of " << n << " is ";

        while ( n > 0 ) //計算3n+1演算法
        {

            if ( n % 2 == 1 ) //若n為偶數
                n = 3 * n + 1;

            else //若n為奇數
                n = n / 2;

            if ( n == 1 ) //當n計算到1時,跳出迴圈
            {
                x++;
                break;
            }

            x++;
        }
        cout << x << endl;
        cout << "Enter positive integer n : ";
        x = 1; //重新定義Cycle-length值
        cin >> n;
    }
    return 0;
}
