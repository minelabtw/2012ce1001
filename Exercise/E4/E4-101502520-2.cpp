//Enter EOF to exit

#include <iostream>
#include <stdio.h>
using namespace std;

int cyclenum(int n)
{
    int counter=1;
    while(n>1)
    {
        if(n%2==1)
            n=3*n+1;
        else
            n=n/2;
        counter++;
    }
    return counter;
}

int main()
{
    int n1,n2,n1temp,n2temp,result=0;

    do{
        if(cin>>n1==false || n1<1 || n1>1000000)//Error detect
            return -1;
        if(cin>>n2==false || n2<1 || n2>1000000)//Error detect
            return -1;

        n1temp=n1;
        n2temp=n2;
        result=0;

        if(n2<n1)
            swap(n1,n2);//exchange, if n2<n1

        for(int i=n1;i<=n2;i++)
            result=max(result,cyclenum(i));

        cout<<n1temp<<" "<<n2temp<<" "<<result<<endl;
    }while((cin.get())!=EOF);//Enter EOF to exit

    return 0;
}
