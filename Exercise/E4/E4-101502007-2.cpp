#include <iostream>

using namespace std;

int main()
{
    int a=1;
    while (a!=0) //為了重複輸入
    {
        int i,j,x=0,b,c=0,max=0,a=1;
        if (cin >>i==false || i<0)
        return -1;
        if (cin >>j==false || j<0)
        return -1;
        cout <<i<<" "<<j<<" ";
        if (i>j) //當i>j時 令i<j
        {
            x=j;
            j=i;
            i=x;
        }
        if (i==1) //若此時i=1 因為1無法進入迴圈 故將i替換成2 才能進入迴圈
        i=2;
        while (i!=1)
        {
            b=i;
            while (i!=1)
            {
                if ((i%2)!=0)
                i=3*i+1;
                else
                i=i/2;
                c++; //每執行一次 cycle-length加1
                if (i==1)
                c=c+1; //當i=1時 也要算一次
            }
            b++;
            i=b;
            if (i>j) //當i>j 則跳出迴圈
            break ;
            if (c>max)
            max=c;
            c=0; //令c=0 以備進入下一個迴圈
        }

        cout <<max;
    }
    return 0;
}
