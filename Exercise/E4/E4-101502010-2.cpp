#include<iostream>
using namespace std;
int check(int n)//made a check fuction
{
    int c=1;//the initial length counter is 1
    while(n!=1)//if n equals 1 then end the function
    {
        if(n%2==0)//when n is even
            n/=2;//
        else//when n is odd
            n=3*n+1;
        c++;//increase the counter
    }
    return c;//return the length
}
int main()
{
    int i,j;
    while((cin>>i)!=false && i>0 && i<1000000)// check whether the number is invilad
    {
        if((cin>>j)!=false && j>0 && j<1000000)// check whether the number is invilad
        {
            cout<<i<<" "<<j<<" ";//output the inputs first
            int max;
            if(i>j)//if i is bigger than j, they exchange
            {
                max=i;
                i=j;
                j=max;
            }
            max=0;//let the initial maximum be 0
            for(int c=i;c<=j;c++)//check from i to j
            {
                if(check(c)>max)//when the new lenth is bigger than maximum
                    max=check(c);//replace the original maximum
            }
            cout<<max<<endl;//output the maximum
        }
        else//if j is invilad input
            return 0;//end the program
    }
    return 0;//end the program
}
