//enter -1 to exit

#include<iostream>
#include<stdlib.h>
#include<limits>
using namespace std;


bool inputcheck(int in);
int _3nplus1(int n);


int main()
{
    //initial
    int n=0;


    while(1)
    {
        //input
        cout<<"Enter positive integer n : ";
        cin>>n;


        //exit
        if(n==-1)
            break;


        //exception
        if(!inputcheck(n))
            return -1;


        //output
        cout<<"Cycle-length of 1000 is "<< _3nplus1(n)<<"\n";
    }


    //end
    system("pause");
    return 0;
}




int _3nplus1(int n)
{
    static int cyclelength=0;
    cyclelength++;

    if(n==1)
    {
        int re=cyclelength;
        cyclelength=0;//reset
        return re;
    }
    else if(n%2==1)//odd number
        return _3nplus1(n*3+1);
    else//even number
        return _3nplus1(n/2);
}


bool inputcheck(int in)
{
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(),'\n');
    if(cin.gcount()!=1)//not integer
        return false;
    else if(in<1)//negative or zero
        return false;
    else if(in>1000000)//too large
        return false;
    //no error
    return true;
}
