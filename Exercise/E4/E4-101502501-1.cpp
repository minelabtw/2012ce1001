#include <iostream>

using namespace std;

int main()
{
    int n;//宣告一整數n
    cout << "Enter positive integer n : ";

    while( cin >> n && n > 0 && n <=1000000 )//當輸入為整數且大於0小於等於1000000時進入迴圈
    {
        int a=1;//宣告一整數a=1
        cout <<"Cycle-length of "<< n << " is ";

        while ( n != 1 )//當n不等於1時進入迴圈
        {
            if ( n % 2 == 1 )//判斷n是否為奇數
            {
                n = ( 3 * n ) + 1;//n等於3乘n加上1
                a++;//a加一
            }
            else//n為偶數時
            {
                n /= 2;//n等於n除以2
                a++;//a加一
            }
        }
        cout << a << endl;
        cout << "Enter positive integer n : ";
    }
    return 0;
}
