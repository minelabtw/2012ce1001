#include <iostream>
using namespace std;

int main()
{
    int i = 0 , j = 0 , k , buffer , c = 1 ,max = 0;

    while ( 1 ) // 重複輸入
    {
        cin >> i; // 偵錯i無效範圍輸入,非數字輸入
        if ( i < 1 || !cin || i > 1000000 )
            return -1;

        cin >> j; // 偵錯j無效範圍輸入,非數字輸入
        if ( j < 1 || !cin || j > 1000000)
            return -1;

        cout << i << " " << j << " ";

        if ( i > j ) // 排序,小數在前大數在後
        {
            buffer = i;
            i = j;
            j = buffer;
        }

        while ( i <= j ) // 當i小於j時進入迴圈
        {
            k = i;
            while ( k != 1 ) //計算3n+1演進法,並計算出cycle-lenth值
            {
                if ( k % 2 == 0 )
                    k = k / 2;
                else
                    k = 3 * k + 1;
                c++;
            }

            if ( c > max ) //取cycle-lenth最大值
            {
                max = c;
            }
            i++;

            c = 1; //重新定義cycle-lenth值\

        }
        cout << max << endl;
    }
    return 0;
}
