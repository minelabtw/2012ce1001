#include <iostream>
using namespace std;
int main()
{
    int n;
    cout << "Enter positive integer n : ";

    while( cin >> n ) //enter a loop
    {
        int i = 1; //let i initialize

        if( n == false || n < 0 )
        {
            return 0;
        }

        if( n >= 1 && n <= 1000000 )
            cout << "Cycle-length of " << n << " is ";
        else
            return 0;

        while( n != 1 ) //enter a loop as n isn't equal to 1
        {
            if( n % 2 == 1 ) //when n is odd
            {
                n = 3 * n + 1;
                i++;
            }
            else //when n is even
            {
                n = n / 2;
                i++;
            }
        }
        cout << i << endl;
        cout << "Enter positive integer n : ";
        //output " Enter positive integer n : " repetitiously
    }
    return 0;
}
