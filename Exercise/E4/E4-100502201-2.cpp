#include<iostream>
using namespace std;

int main()
{
    int a,b,tmp; //tmp=temporary variable, used in loop function and so on
    int count = 0; //Initialize "count"
    int max = 0; //Initialize "max"
    while (1)
    {
        cin>>a;
        if (a<0||cin==false)
        break;
        cin>>b;
        if (b<0||cin==false)
        break;
        cout<<a<<" "<<b<<" ";
        //If a>b, exchange a and b, let a is always smaller than b
        if (a>b)
        {
            tmp=a;
            a=b;
            b=tmp;
        }
        tmp=a; //tmp is used in loop function
        while(tmp<=b)
        {
            while (a!=1)
            {
                if (a%2==1)
                {
                    a=3*a+1;
                    count++;
                }
                else
                {
                    a=a/2;
                    count++;
                }
            }
            // Every time we count a number's cycle-length, compare to the last number and record the maximum number
            if (count+1>max)
            {
                max=count+1;
            }
            count=0;
            tmp++;
            a=tmp;
        }
        cout<<max<<endl;
        max=0;
    }
    return 0;
}
