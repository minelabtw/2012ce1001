#include <iostream>
using namespace std;

int main()
{
    int a,b,buffer,d; // 假設變數
    int x = 1;
    int max;

    while ( x = 1 ) // 如果符合條件x等於1，進入迴圈
    {
        max = 0;
        if ( cin >> a == false || a <= 0 || a > 1000000 ) // 當輸入的a錯誤、小於等於0或是a大於1000000，結束並回報
        {
            return -1;
        }
        if ( cin >> b == false || b <= 0 || b > 1000000 ) // 當輸入的b錯誤、小於等於0或是b大於1000000，結束並回報
        {
            return -1;
        }

        cout << a << " " << b << " "; // display message

        if ( a > b ) // 當a大於b的時候，交換a、b的位子
        {
            buffer = a;
            a = b;
            b = buffer;
        }

        for (; a <= b; a++ ) // 如果符合條件a小於等於b且a遞增時，進入迴圈
        {
            int f = a;
            int c = 1;
            d = 0;
            while ( f != 1 ) // 如果符合條件f不等於1時，進入迴圈
            {
                if ( f % 2 != 0 ) // 如果f不是2的倍數
                {
                    f = f * 3 + 1; // 運算公式
                    c++; // c遞增
                }
                if ( f % 2 == 0 )
                {
                    f = f / 2;
                    c++;
                }
            }
            d += c; // 運算公式
            if ( d > max ) // 當d大於max
                max = d;
        }

        cout << max << endl; // 輸出最後結果
    }

    return 0; // 結束並回報
}
