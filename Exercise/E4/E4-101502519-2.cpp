#include <iostream>

using namespace std;

int main()
{
    int a,b,n,c,d,x;
    /*a.b為輸入值,n用來計算次數,c儲存最大次數,
    d用來交換a,b的大小,x用來存a以計算*/
    while(1)                             //可重複輸入
    {
        if(cin>>a==false||a<=0)          //錯誤輸入的判定
             return -1;
        else if(cin>>b==false||b<=0)     //錯誤輸入的判定
             return -1;
        else
        {
            c=1;                         //讓最大值回歸1
            cout<<a<<" "<<b<<" ";        //輸出a,b
            if(a>b)                      //若a>b則a與b交換
            {
                d=a;
                a=b;
                b=d;
            }
            x=a;                         //存一x值等於a
            for(x>0;x<=b;x++)
            {
                a=x;                     //讓a之值回歸x
                n=1;
                while(a!=1)              //迴圈在a=1時停止
                {
                     if(a%2==0)          //a若為偶數時執行
                         a=a/2;
                     else                //a若為奇數時執行
                         a=3*a+1;
                     n=n+1;              //計算執行次數
                }
                if(x==1)                 //1的cycle-length為4
                    n=4;
                if(n>c)                  //以c儲存最大的n
                    c=n;
            }
            cout<<c<<"\n";               //輸出最大的cycle-length
        }
    }
    return 0;
}
