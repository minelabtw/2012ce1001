#include <iostream>
using namespace std;

int main()
{
    int a = 1;
    int b = 0;

    while ( b < 20)
    {
        b = b + 1;

        cout << a << " * " << b << " = " << a * b << endl;
    }

    return 0;
}
