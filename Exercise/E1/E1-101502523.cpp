#include <iostream>

using namespace std;

int main()
{
    int i = 0;

    while ( i < 20 )
    {
        i = 1 * i+1;
        cout << "1 * " << i << " = " << i << endl;
    }
    return 0;
}
