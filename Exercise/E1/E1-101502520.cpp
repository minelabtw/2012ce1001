#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    for(int i=1;i<=20;i++)
    {
        cout<<"1 "<<"* "<<i<<" = "<<setw(2)<<1*i<<" ";
        cout<<endl;
    }
    return 0;
}
