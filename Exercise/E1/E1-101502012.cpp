#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int i = 1;
    int ans;
    while ( i <= 20 )
{
    ans = 1*i;
    cout << "1 * " << i << " = " << ans << endl;

    i = i + 1;

}

    return 0 ;

}
