#include <iostream>

using namespace std;

int main()
{
    int number = 1;
    while (number <= 20)
    {
        cout << 1 << " * " << number << " = " << number << endl;
        number = number + 1;
    }
    return 0;
}
