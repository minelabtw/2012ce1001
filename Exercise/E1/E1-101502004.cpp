#include <iostream>

using namespace std;

int main()
{
    int product = 0;

    while ( product <= 19 )
    {
        product = 1 + product;
        cout << "1 * " << product << " = " << product << endl;
    }
    return 0;
}
