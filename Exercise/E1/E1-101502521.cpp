#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    /*for(int i=1;i<=9;i++, cout << endl)
        for(int j=1;j<=9;j++)
            cout << i << "*" << j << "=" << setw(2) << i*j << " ";*/
    int i=1, j=1;
    while(i<=20)
    {
        cout << j << " * " << i << " = " << j*i << endl;
        i++;
    }

    return 0;
}
