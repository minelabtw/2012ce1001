#include<iostream>
#include<iomanip>

using std::cout;
int main()
{
    int product = 1;
    while (product <= 20)
    {
        cout << 1 << " * " << product << " = " << product << "\n";
        product = product + 1;
    }

    return 0;
}
