#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int side1 , side2 , side3 , buffer ;

    cout << "Enter the side1 : ";      //��detect input is negative.
    if( ( cin >> side1 == false ) || ( side1 < 0 ) )//error detection : if receive a character or a negative number , program is terminated.
    {      //��detect the input is character.
        if( side1 < 0 )
            cout << "Side can't be a negative number." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }


    cout << "Enter the side2 : ";
    if( cin >> side2 == false || side2 < 0 )//error detection : if receive a character or a negative number , program is terminated.
    {
        if( side2 < 0 )
            cout << "Side can't be a negative number." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }

    cout << "Enter the side3 : ";
    if( cin >> side3 == false || side3 < 0 )//error detection : if receive a character or a negative number , program is terminated.
    {
        if( side3 < 0 )
            cout << "Side can't be a negative number." << endl;
        else
            cout << "Invalid input." << endl;
        return -1;
    }

    if( side1 > side2 )//if side1 is greater than side2, exchange side1 and side2
    {
        buffer = side1;
        side1 = side2;
        side2 = buffer;
    }

    if( side1 > side3 )//if side1 is greater than side3, exchange side1 and side3
    {
        buffer = side1;
        side1 = side3;
        side3 = buffer;
    }

    if( side2 > side3 )//if side2 is greater than side3, exchange side2 and side3
    {
        buffer = side2;
        side2 = side3;
        side3 = buffer;
    }

    if( side1 + side2 > side3 )
        cout << "Must be a trangle.";
    else
        cout << "Can't be a trangle.";

    return 0;
}
