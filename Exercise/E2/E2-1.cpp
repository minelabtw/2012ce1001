#include <iostream>
using std::cout;
using std::cin;
using std::endl;
int main()
{
    int input , sum , count ;
    count = 0;
    sum = 0;
    cout << "Enter an integer (if you want to quit, enter any character) : ";
    while( cin >> input ) // if cin encounter an invalid input( such as character ), it will return false then break the loop
    {
        count++;
        sum += input ;
        cout << "Average : " << sum / ( double ) count << endl;
        cout << "Enter an integer : ";
    }
    return 0;
}
